-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: happinate
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `happinate_application`
--

DROP TABLE IF EXISTS `happinate_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_application` (
  `application_id` int(11) NOT NULL AUTO_INCREMENT,
  `application_user_id` int(11) NOT NULL,
  `application_job_id` int(11) NOT NULL,
  `application_date` datetime NOT NULL,
  `application_status` tinyint(1) NOT NULL DEFAULT '0',
  `application_accepted_date` datetime DEFAULT NULL,
  `application_cv` int(11) DEFAULT NULL,
  PRIMARY KEY (`application_id`),
  KEY `application_user_id` (`application_user_id`,`application_job_id`),
  KEY `application_job_id` (`application_job_id`),
  CONSTRAINT `happinate_application_ibfk_5` FOREIGN KEY (`application_user_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `happinate_application_ibfk_6` FOREIGN KEY (`application_job_id`) REFERENCES `happinate_job` (`job_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1741 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_application_note`
--

DROP TABLE IF EXISTS `happinate_application_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_application_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(400) COLLATE utf8_bin NOT NULL,
  `employeer` int(11) NOT NULL,
  `note` mediumtext COLLATE utf8_bin NOT NULL,
  `date` date NOT NULL,
  `rate_min` decimal(5,2) NOT NULL,
  `rate_max` decimal(5,2) NOT NULL,
  `vacancies` int(11) NOT NULL,
  `group_id` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `trade_id` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_application_note_user`
--

DROP TABLE IF EXISTS `happinate_application_note_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_application_note_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `friend` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_application_oneclick`
--

DROP TABLE IF EXISTS `happinate_application_oneclick`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_application_oneclick` (
  `oneclick_id` int(11) NOT NULL AUTO_INCREMENT,
  `oneclick_employeer_id` int(11) DEFAULT NULL,
  `oneclick_employee_id` int(11) DEFAULT NULL,
  `oneclick_token` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `oneclick_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `oneclick_last_job` int(11) DEFAULT NULL,
  `oneclick_last_cv` int(11) DEFAULT NULL,
  `oneclick_note` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`oneclick_id`),
  KEY `oneclick_employeer_id` (`oneclick_employeer_id`,`oneclick_employee_id`),
  KEY `oneclick_employee_id` (`oneclick_employee_id`),
  CONSTRAINT `happinate_application_oneclick_ibfk_5` FOREIGN KEY (`oneclick_employeer_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `happinate_application_oneclick_ibfk_6` FOREIGN KEY (`oneclick_employee_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_application_oneclick_group`
--

DROP TABLE IF EXISTS `happinate_application_oneclick_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_application_oneclick_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeer` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_bin NOT NULL,
  `opis` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_application_oneclick_group_user`
--

DROP TABLE IF EXISTS `happinate_application_oneclick_group_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_application_oneclick_group_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group` (`group_id`),
  KEY `user` (`user`),
  CONSTRAINT `happinate_application_oneclick_group_user_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `happinate_application_oneclick_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `happinate_application_oneclick_group_user_ibfk_2` FOREIGN KEY (`user`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_city`
--

DROP TABLE IF EXISTS `happinate_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_province_id` int(11) NOT NULL,
  `city_text` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`city_id`),
  KEY `city_province_id` (`city_province_id`),
  CONSTRAINT `happinate_city_ibfk_1` FOREIGN KEY (`city_province_id`) REFERENCES `happinate_province` (`province_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_email`
--

DROP TABLE IF EXISTS `happinate_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `body` mediumtext NOT NULL,
  `template` varchar(200) NOT NULL,
  `fromName` varchar(200) NOT NULL,
  `fromEmail` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_job`
--

DROP TABLE IF EXISTS `happinate_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_job` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_added_by` int(11) NOT NULL,
  `job_contract_type_id` int(11) DEFAULT NULL,
  `job_trade_type_id` int(11) DEFAULT '18',
  `job_start_date` date NOT NULL,
  `job_time` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `job_work_hours_per_week` int(11) DEFAULT NULL,
  `job_work_shifts_per_day` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `job_logo` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `job_www` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `job_fanpage` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `job_publish_date` datetime NOT NULL,
  `job_publish_date_end` datetime NOT NULL,
  `job_position` varchar(300) COLLATE utf8_bin NOT NULL,
  `job_url_text` varchar(300) COLLATE utf8_bin NOT NULL,
  `job_rate_min` decimal(5,2) NOT NULL,
  `job_rate_max` decimal(5,2) NOT NULL,
  `job_volunteering` tinyint(1) NOT NULL DEFAULT '0',
  `job_hide_salary` tinyint(1) NOT NULL DEFAULT '0',
  `job_additional_salary_info` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `job_employeer_name` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `job_employeer_place_direct` tinyint(1) NOT NULL DEFAULT '0',
  `job_employeer_place_direct_address` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `job_employeer_place_direct_city` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `job_employeer_place_direct_zip_code` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `job_employeer_place_direct_place_tips` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `job_employeer_place_area` tinyint(1) NOT NULL DEFAULT '0',
  `job_employeer_place_area_tips` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `job_employeer_place_telework` tinyint(1) NOT NULL DEFAULT '0',
  `job_employeer_place_without` tinyint(1) NOT NULL DEFAULT '0',
  `job_contact_name` varchar(300) COLLATE utf8_bin NOT NULL,
  `job_contact_email` varchar(300) COLLATE utf8_bin NOT NULL,
  `job_contact_phone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `job_status` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `job_main_page` int(2) NOT NULL DEFAULT '0',
  `job_delete` tinyint(1) NOT NULL DEFAULT '0',
  `job_views` int(11) NOT NULL DEFAULT '0',
  `job_control` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `job_dotpay_tid` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `job_city_id` int(11) DEFAULT NULL,
  `job_created_date` datetime NOT NULL,
  `job_vacancies` int(11) DEFAULT NULL,
  `job_lat` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `job_long` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `job_rabat_id` int(11) DEFAULT NULL,
  `job_need` mediumtext COLLATE utf8_bin,
  `job_duty` mediumtext COLLATE utf8_bin,
  `job_offer` mediumtext COLLATE utf8_bin,
  `job_province_id` int(11) DEFAULT NULL,
  `job_additional_oneclick_info` varchar(600) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`job_id`),
  KEY `FK_user_id` (`job_added_by`),
  KEY `FK_job_contract_type` (`job_contract_type_id`),
  KEY `FK_job_trade_type` (`job_trade_type_id`),
  KEY `job_city_id` (`job_city_id`),
  CONSTRAINT `happinate_job_ibfk_11` FOREIGN KEY (`job_added_by`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `happinate_job_ibfk_14` FOREIGN KEY (`job_contract_type_id`) REFERENCES `happinate_job_contract_types` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `happinate_job_ibfk_15` FOREIGN KEY (`job_trade_type_id`) REFERENCES `happinate_tag` (`tag_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2883 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_job_contract_types`
--

DROP TABLE IF EXISTS `happinate_job_contract_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_job_contract_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_job_duty`
--

DROP TABLE IF EXISTS `happinate_job_duty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_job_duty` (
  `duty_id` int(11) NOT NULL AUTO_INCREMENT,
  `duty_job_id` int(11) NOT NULL,
  `duty_text` varchar(300) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`duty_id`),
  KEY `FK_job_id` (`duty_job_id`),
  CONSTRAINT `happinate_job_duty_ibfk_2` FOREIGN KEY (`duty_job_id`) REFERENCES `happinate_job` (`job_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1438 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_job_need`
--

DROP TABLE IF EXISTS `happinate_job_need`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_job_need` (
  `need_id` int(11) NOT NULL AUTO_INCREMENT,
  `need_job_id` int(11) NOT NULL,
  `need_text` varchar(300) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`need_id`),
  KEY `need_job_id` (`need_job_id`),
  CONSTRAINT `happinate_job_need_ibfk_2` FOREIGN KEY (`need_job_id`) REFERENCES `happinate_job` (`job_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2367 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_job_oneclick`
--

DROP TABLE IF EXISTS `happinate_job_oneclick`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_job_oneclick` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `happinate_job_oneclick_ibfk_3` FOREIGN KEY (`job_id`) REFERENCES `happinate_job` (`job_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `happinate_job_oneclick_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_job_rabat`
--

DROP TABLE IF EXISTS `happinate_job_rabat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_job_rabat` (
  `rabat_id` int(11) NOT NULL AUTO_INCREMENT,
  `rabat_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `rabat_percent` int(11) NOT NULL DEFAULT '0',
  `rabat_code` varbinary(256) NOT NULL,
  `rabat_count` int(11) NOT NULL DEFAULT '1',
  `rabat_used` int(11) NOT NULL DEFAULT '0',
  `rabat_date` datetime NOT NULL,
  PRIMARY KEY (`rabat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_job_trade_types`
--

DROP TABLE IF EXISTS `happinate_job_trade_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_job_trade_types` (
  `trade_id` int(11) NOT NULL AUTO_INCREMENT,
  `trade_name` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`trade_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_province`
--

DROP TABLE IF EXISTS `happinate_province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_province` (
  `province_id` int(11) NOT NULL AUTO_INCREMENT,
  `province_text` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_session`
--

DROP TABLE IF EXISTS `happinate_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_session` (
  `session_id` varchar(45) COLLATE utf8_bin NOT NULL,
  `value` mediumtext COLLATE utf8_bin NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_tag`
--

DROP TABLE IF EXISTS `happinate_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `tag_active` int(11) NOT NULL DEFAULT '0',
  `tag_sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_tag_job`
--

DROP TABLE IF EXISTS `happinate_tag_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_tag_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_job` (`job_id`),
  KEY `FK_tag` (`tag_id`),
  CONSTRAINT `happinate_tag_job_ibfk_3` FOREIGN KEY (`job_id`) REFERENCES `happinate_job` (`job_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `happinate_tag_job_ibfk_4` FOREIGN KEY (`tag_id`) REFERENCES `happinate_tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2319 DEFAULT CHARSET=latin2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_tag_oneclick`
--

DROP TABLE IF EXISTS `happinate_tag_oneclick`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_tag_oneclick` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oneclick_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user`
--

DROP TABLE IF EXISTS `happinate_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(11) NOT NULL,
  `user_profile_name` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `user_salt` varchar(64) COLLATE utf8_bin NOT NULL,
  `user_fb_id` varbinary(128) DEFAULT NULL,
  `user_email` varbinary(256) NOT NULL,
  `user_password` varbinary(256) NOT NULL,
  `user_logo` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `user_name` varbinary(256) DEFAULT NULL,
  `user_surname` varbinary(256) DEFAULT NULL,
  `user_nip` varbinary(256) DEFAULT NULL,
  `user_regon` varbinary(256) DEFAULT NULL,
  `user_pesel` varbinary(256) DEFAULT NULL,
  `user_date_of_birth` date DEFAULT NULL,
  `user_adress_country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `user_adress_city` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `user_adress_zip_code` varbinary(256) DEFAULT NULL,
  `user_adress_street` varbinary(256) DEFAULT NULL,
  `user_contact_country` int(11) DEFAULT NULL,
  `user_contact_city` int(11) DEFAULT NULL,
  `user_contact_zip_code` varbinary(256) DEFAULT NULL,
  `user_contact_street` varbinary(256) DEFAULT NULL,
  `user_www` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `user_fanpage` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `user_phone` varbinary(256) DEFAULT NULL,
  `user_cellphone` varbinary(256) DEFAULT NULL,
  `user_organizations` mediumtext COLLATE utf8_bin,
  `user_additional_mail` varbinary(256) DEFAULT NULL,
  `user_student` tinyint(1) NOT NULL DEFAULT '0',
  `user_uncapable` tinyint(1) NOT NULL DEFAULT '0',
  `user_customer_id` int(11) DEFAULT NULL,
  `user_register_date` datetime DEFAULT NULL,
  `user_register_token` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `user_ip` varbinary(128) DEFAULT NULL,
  `user_active` tinyint(1) NOT NULL DEFAULT '0',
  `user_password_reminder_token` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `user_password_reminder_exp_date` datetime DEFAULT NULL,
  `user_push` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `user_worknow` tinyint(1) NOT NULL DEFAULT '0',
  `user_old` tinyint(1) NOT NULL DEFAULT '0',
  `user_profile_complete` int(11) NOT NULL DEFAULT '0',
  `user_sms` tinyint(1) NOT NULL DEFAULT '0',
  `user_profile_url` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `user_info` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `user_profile_img` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `user_profile_img_info` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `user_profile_yt_movie` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `user_main_tag_id` int(11) DEFAULT NULL,
  `user_hobby` mediumtext COLLATE utf8_bin,
  `user_cv_color` varchar(200) COLLATE utf8_bin DEFAULT '#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF	',
  `user_bgcv_color` varchar(200) COLLATE utf8_bin DEFAULT '#4F5257,#4F5257,#4F5257,#4F5257,#4F5257,#4F5257,#4F5257',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `EMAIL` (`user_email`),
  KEY `FK_user_type` (`user_type`),
  CONSTRAINT `happinate_user_ibfk_1` FOREIGN KEY (`user_type`) REFERENCES `happinate_user_type` (`type_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4097 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user_cv`
--

DROP TABLE IF EXISTS `happinate_user_cv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user_cv` (
  `cv_id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_user_id` int(11) NOT NULL,
  `cv_date` date NOT NULL,
  `cv_src` varchar(128) COLLATE utf8_bin NOT NULL,
  `cv_name` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `cv_application_id` int(11) DEFAULT NULL,
  `cv_public` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cv_id`),
  KEY `cv_user_id` (`cv_user_id`),
  CONSTRAINT `happinate_user_cv_ibfk_2` FOREIGN KEY (`cv_user_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1106 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user_education`
--

DROP TABLE IF EXISTS `happinate_user_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user_education` (
  `education_id` int(11) NOT NULL AUTO_INCREMENT,
  `education_user_id` int(11) NOT NULL,
  `education_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `education_start` date NOT NULL,
  `education_end` date DEFAULT NULL,
  `education_till_now` tinyint(1) DEFAULT '0',
  `education_field` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`education_id`),
  KEY `FK_user_id` (`education_user_id`),
  CONSTRAINT `happinate_user_education_ibfk_2` FOREIGN KEY (`education_user_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user_experience`
--

DROP TABLE IF EXISTS `happinate_user_experience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user_experience` (
  `experience_id` int(11) NOT NULL AUTO_INCREMENT,
  `experience_user_id` int(11) NOT NULL,
  `experience_company` varchar(64) COLLATE utf8_bin NOT NULL,
  `experience_start` date NOT NULL,
  `experience_end` date DEFAULT NULL,
  `experience_till_now` tinyint(1) NOT NULL DEFAULT '0',
  `experience_position` varchar(64) COLLATE utf8_bin NOT NULL,
  `experience_opis` varchar(2000) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`experience_id`),
  KEY `experience_user_id` (`experience_user_id`),
  CONSTRAINT `happinate_user_experience_ibfk_3` FOREIGN KEY (`experience_user_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user_fav_job`
--

DROP TABLE IF EXISTS `happinate_user_fav_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user_fav_job` (
  `fav_id` int(11) NOT NULL AUTO_INCREMENT,
  `fav_user_id` int(11) NOT NULL,
  `fav_job_id` int(11) NOT NULL,
  PRIMARY KEY (`fav_id`),
  KEY `FK_user` (`fav_user_id`),
  KEY `FK_job` (`fav_job_id`),
  CONSTRAINT `happinate_user_fav_job_ibfk_3` FOREIGN KEY (`fav_user_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `happinate_user_fav_job_ibfk_4` FOREIGN KEY (`fav_job_id`) REFERENCES `happinate_job` (`job_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user_friend`
--

DROP TABLE IF EXISTS `happinate_user_friend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user_friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) DEFAULT NULL,
  `token` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `user_recommendation` tinyint(1) NOT NULL DEFAULT '0',
  `accepted` tinyint(1) NOT NULL,
  `email` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `friend_id` (`friend_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user_lang`
--

DROP TABLE IF EXISTS `happinate_user_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user_lang` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_user_id` int(11) NOT NULL,
  `lang_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `lang_level` varchar(300) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user_other_skill`
--

DROP TABLE IF EXISTS `happinate_user_other_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user_other_skill` (
  `skill_user_id` int(11) NOT NULL,
  `skill_driving_a` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_a1` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_b` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_b1` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_c` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_c1` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_d` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_d1` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_be` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_c1e` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_ce` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_d1e` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_de` tinyint(1) NOT NULL DEFAULT '0',
  `skill_driving_t` tinyint(1) NOT NULL DEFAULT '0',
  `skill_sanel_owner` tinyint(1) NOT NULL DEFAULT '0',
  `skill_sanel_not_expired` tinyint(1) NOT NULL DEFAULT '0',
  `skill_welding_tig` tinyint(1) NOT NULL DEFAULT '0',
  `skill_welding_mig` tinyint(1) NOT NULL DEFAULT '0',
  `skill_welding_mag` tinyint(1) NOT NULL DEFAULT '0',
  `skill_electricity_to_1kv` tinyint(1) NOT NULL DEFAULT '0',
  `skill_electricity_more_1kv` tinyint(1) NOT NULL DEFAULT '0',
  `skill_forklift` tinyint(1) NOT NULL DEFAULT '0',
  `skill_forklift_big` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`skill_user_id`),
  CONSTRAINT `happinate_user_other_skill_ibfk_2` FOREIGN KEY (`skill_user_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user_skill`
--

DROP TABLE IF EXISTS `happinate_user_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user_skill` (
  `skill_id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_user_id` int(11) NOT NULL,
  `skill_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `skill_opis` varchar(600) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`skill_id`),
  KEY `skill_user_id` (`skill_user_id`),
  CONSTRAINT `happinate_user_skill_ibfk_2` FOREIGN KEY (`skill_user_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user_tag`
--

DROP TABLE IF EXISTS `happinate_user_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_user_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tag_user_id` (`tag_user_id`,`tag_id`),
  KEY `FK_tag` (`tag_id`),
  CONSTRAINT `happinate_user_tag_ibfk_2` FOREIGN KEY (`tag_user_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `happinate_user_tag_ibfk_3` FOREIGN KEY (`tag_id`) REFERENCES `happinate_tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2830 DEFAULT CHARSET=latin2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user_training`
--

DROP TABLE IF EXISTS `happinate_user_training`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user_training` (
  `training_id` int(11) NOT NULL AUTO_INCREMENT,
  `training_user_id` int(11) NOT NULL,
  `training_end` date NOT NULL,
  `training_name` varchar(500) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`training_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user_type`
--

DROP TABLE IF EXISTS `happinate_user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user_type` (
  `type_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`type_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `happinate_user_watch`
--

DROP TABLE IF EXISTS `happinate_user_watch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happinate_user_watch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `watch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `watch_id` (`watch_id`),
  CONSTRAINT `happinate_user_watch_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `happinate_user_watch_ibfk_2` FOREIGN KEY (`watch_id`) REFERENCES `happinate_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-14 13:44:28
