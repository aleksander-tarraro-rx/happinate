<!doctype html>
<html lang="pl">
<head>
<meta charset="utf-8">
<title>{block name=metatitle}Praca dorywcza, tymczasowa oraz sezonowa - happinate{/block}</title>
<meta name="description" content="{block name=metadescription}Zapraszamy wszystkie osoby szukające pracy nie zależnie czy ma być ona sezonowa czy dorywcza. Na Naszej stronie znajdują się oferty pracy tymczasowej wśród których każdy znajdzie coś dla siebie.{/block}" />
<meta name="keywords" content="{block name=metakeywords}Agencja pracy tymczasowej,Praca tymczasowa,Praca dorywcza,Oferty pracy,Agencja pracy,Dodatkowa praca,Praca sezonowa,Praca MIASTO (Poznań),Praca,Wolontariat,Wolontariat Poznań{/block}" />
<meta name="robots" content="index,follow,all" />
<meta name="revisit-after" content="2 days" />
<meta name="publisher" content="modernfactory.pl" />
<meta name="rating" content="general" />

<meta property="og:title" content="happinate – praca dorywcza i tymczasowa" />
<meta property="og:image" content="{$smarty.const.HOST}view/images/happinate-face-img.png" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="320" />
<meta property="og:image:height" content="320" />
<meta property="og:description" content="" />

<link rel="stylesheet" href="{$smarty.const.HOST}view/css/libs/html5reset-1.6.1.css">
<link rel="stylesheet" href="{$smarty.const.HOST}view/css/mapa.css">
{block name=additionalCSS}{/block}
{if $smarty.const.DEBUG}<link rel="stylesheet" href="{$smarty.const.HOST}view/css/debug.css">{/if}
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <style>
        .underline-header-content{
            display: none;
        }
    </style>
<![endif]-->
<link rel="shortcut icon" href="{$smarty.const.HOST}view/images/favicon.png">
</head>
<body>
    
<section>
    
    <section class="center">
        <div class="logo-main"><a href="{$smarty.const.HOST}"><img src="{$smarty.const.HOST}view/images/logo-happinate.png" alt="Happinate"></a></div>

        <div class="cities">
            <a href="#poznan" class="active">Poznań</a>&nbsp; |&nbsp; 
            <a href="#lodz">Łódź</a>&nbsp; |&nbsp; 
            <a href="#warszawa">Warszawa</a>
        </div>

        <div class="clear"></div>

        <nav class="main-nav">
            <ul>
                <li class="home"><a href="{$smarty.const.HOST}"><i class="icon-house"></i></a></li>
                <li><a href="{$smarty.const.HOST}jak-to-dziala">Jak to działa?</a></li>
                <li><a href="{$smarty.const.HOST}oferty-pracy/1/">Oferty pracy</a></li>
                <li><a href="{$smarty.const.HOST}daj-ogloszenie">Daj ogłoszenie</a></li>
                <li{if !empty($smarty.get.url) && $controllerName=='mapaController'} class="active"{/if}><a href="{$smarty.const.HOST}mapa">Mapa</a></li>
                <li><a href="http://happichill.com/" target="_blank">Blog</a></li>
                {if $_user}
                    <li><a href="{$smarty.const.HOST}moje-konto">Profil</a></li>
                {else}
                    <li><a href="{$smarty.const.HOST}zaloguj-sie">Zaloguj się</a></li>
                {/if}
            </ul>
        </nav>
    </section>
    
    <section class="map">
        <div class="gmap"></div>
    </section>
    <aside>
        <div class="watch">
            <p class="img"><img src="{$smarty.const.HOST}view/images/logo-happinate.png" alt="logo pracodawcy"></p>
            <p class="motto">happinate - praca od zaraz</p>
            {*<a href="{$smarty.const.HOST}">Obserwuj</a>*}
        </div>
        <div class="address">
            {*<a href="{$smarty.const.HOST}" target="_blank" class="see-all">zobacz więcej</a>*}
            <div class="clear"></div>
            <p>ul.Baraniaka 88e (bud. F)<br>61-131 Poznań<br>tel.: <a href="tel:+48 506-052-552">+48 506-052-552</a></p>
        </div>
        <div class="filter">
            <div class="address" style="height: 20px;">
                <h2><strong>Szukaj oferty w kategori:</strong></h2>
                </div>
            <ul>
                <li class="all-icon on" data-value="all">Wszystkie oferty</li>
                {foreach from=$tags item=tag}               
                <li class="icon{$tag.tag_id}" data-value="{$tag.tag_name}">{$tag.tag_name}</li>
                {/foreach}
            </ul>
        </div>
        <div class="offers">
            <h2>Lista ofert pracy:</h2>
            <a href="{$smarty.const.HOST}oferty-pracy" class="see-all">zobacz więcej</a>
            <div class="clear"></div>
            <div class="scroll"></div>
        </div>
    </aside>
</section>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>if (typeof (jQuery) == 'undefined') document.write(unescape("%3Cscript " + "src='js/jquery.min.js' %3E%3C/script%3E"));</script>
<script src="{$smarty.const.HOST}view/js/markerclusterer.min.js"></script>
<script src="{$smarty.const.HOST}view/js/jquery.ui.map.js"></script>
<script src="{$smarty.const.HOST}view/js/mapa.js?date=20140218"></script>
{if !$smarty.const.DEBUG}
<script type="text/javascript">
	var _gaq = _gaq || []; var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
	_gaq.push(['_require', 'inpage_linkid', pluginUrl]);
	_gaq.push(['_setAccount', 'UA-39041418-1']);
	_gaq.push(['_trackPageview']);
	(function () {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
{/if}
{block name=additionalJavaScript}{/block}
{if $smarty.const.DEBUG}{include file="debug.tpl"}{/if}
</body>
</html>