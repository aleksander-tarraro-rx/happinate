{extends file="index.tpl"}

{block name=metatitle}happinate Sp. z o.o. {/block}
{block name=metadescription}Zapraszamy  do  kontaktu  z  Naszą  firmą,  a  postaramy  się  udzielić odpowiedzi na wszystkie nurtujące Państwa pytania.{/block}

{block name=body}

    <div class="left-column">
        <div id="map_canvas" class="contact-map"></div>
            <div style="color:#515254;line-height:20px;margin-top:20px;">
            <p><strong>happinate sp. z o.o.</strong><br>
            ul. Abpa Antoniego Baraniaka 88E (budynek F)<br>
            61-131 Poznań</p><br>
            <p>email: <a href="mailto:hi@happinate.com" target="_blank">hi@happinate.com</a><br>
            telefon: +48 506-052-552</p>
        </div>
    </div>
    <div class="right-column">
        <div class="help-phone-box" style="padding:20px 30px 0px 30px;margin:0px;height:90px;">
            <p class="phone"><strong>506-052-552</strong></p>
            <p><span class="call">Zadzwoń !</span> Chętnie pomożemy.</p>
        </div>
        <form action="" method="post" id="contact_form">
            <input type="hidden" name="contact_submit" value="1" />
            <div class="input-box right" style="margin:0px auto;">
                <label>
                    <span class="input-box-label">Imię i nazwisko</span>
                    <input type="text" name="name" value="{if isset($_user) && !empty($_user)}{$_user->getUserName()}{if $_user->getUserSurname()} {$_user->getUserSurname()}{/if}{/if}" autocomplete="off">
                </label>
            </div>
            <div class="input-box right" style="margin:0px auto;">
                <label>
                    <span class="input-box-label">Email <span class="red-text">(wymagane)</span></span>
                    <input type="text" name="email" value="{if isset($_user) && !empty($_user)}{$_user->getUserEmail()}{/if}" autocomplete="off">
                </label>
            </div>
            <div class="input-box right" style="margin:0px auto;">
                <label>
                    <span class="input-box-label">Wiadomość <span class="red-text">(wymagane)</span></span>
                    <textarea rows="3" name="message"></textarea>
                </label>
            </div>
            <a href="#" onClick="$('#contact_form').submit()" class="happi-btn" style="float:right;margin-top:10px;">Wyślij <i class="icon-chevron-right icon-white"></i></a>
        </form>
    </div>
    <div class="clear"></div>

{/block}