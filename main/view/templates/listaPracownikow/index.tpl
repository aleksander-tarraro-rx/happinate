{extends file="index.tpl"}

{block name=body}
    <div class="left-column">
        <h2>Lista pracodawców</h2>
        {foreach from=$list item=l}
            {if !isset($firstLetter)}<h3 style="text-transform: uppercase;">{$l.user_profile_name.0}</h3>{assign var=firstLetter value=strtolower($l.user_profile_name.0)}{/if}
            {if isset($firstLetter) && strtolower($firstLetter) != strtolower($l.user_profile_name.0)}<h3 style="text-transform: uppercase;">{$l.user_profile_name.0}</h3>{assign var=firstLetter value=$l.user_profile_name.0}{/if}
            <p style="padding: 5px;">{if $l.user_logo}<a href="{$smarty.const.HOST}profil/p/{$l.user_id|md5}" target="_blank"><img src="{$smarty.const.HOST}upload/userLogo/{$l.user_logo}" height="19px" width="19px" alt="" valign="absmiddle" /></a> {else}<span style="display: inline-block; width: 20px; height: 10px;"></span> {/if}<a href="{$smarty.const.HOST}profil/p/{$l.user_id|md5}" target="_blank">{$l.user_profile_name}</a></p>
        {/foreach}
    </div>

    <div class="right-column">
        <div class="simple-fb-like">
            <div class="fb-like-box" data-href="https://www.facebook.com/happinate" data-width="292" data-height="500" data-show-faces="true" data-stream="false" data-show-border="false" data-header="false"></div>
        </div>
    </div>
    
    <div class="clear"></div>
{/block}