{extends file="index.tpl"}

{block name=prebody}
<div class="center">
    <nav class="login-nav">
        {if $_user->getUserType()==1}
        <ul>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>
        {elseif $_user->getUserType()==2}
        <ul>
            <li{if $actionName=='dodajOgloszenieAction'} class="active"{/if}><a href="{$smarty.const.HOST}daj-ogloszenie"><strong>+</strong> Dodaj ogłoszenie</a></li>
            <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
            <li{if $actionName=='mojeGrupyAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-grupy">Moje grupy</a></li>
            <li{if $actionName=='moiKandydaciAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Moi kandydaci</a></li>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>            
        {/if}
    </nav>
</div>
{/block}

{block name=body}
<section class="main-content">

    <form class="main-form" action="" method="post" enctype="multipart/form-data">
        <input type="hidden" name="ogloszenie-edit" value="1" />
        
        <h2>EDYTUJ OGŁOSZENIE "{$job.job_position}"<span class="underline-header-content" style="width:243px;"></span></h2>
        <div class="clear" style="height:25px;"></div>
        
        <div class="row-fluid">
                <div class="span3"><label for="3">Dodatkowe informacje o wynagrodzeniu</label></div>
                <div class="span7">
                        <div class="textarea-input" style="width: 420px;">
                                <textarea id="3" name="additional_salary_info"  rows="3"{if isset($errorArray.additional_salary_info) && $errorArray.additional_salary_info} style="background:#EED3D7"{/if}>{$job.job_additional_salary_info}</textarea>
                        </div>
                </div>
        </div>
        <hr>
        <div class="row-fluid">
                <div class="span3"><label for="4">Wymagania</label></div>
                <div class="span7">
                    <div class="textarea-input" style="margin-bottom: 10px;width: 420px;">
                        <textarea class="ckeditor2" id="4" name="need" name="need" rows="3">{$job.job_need}</textarea>
                    </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="5">Obowiązki *</label></div>
                <div class="span7">
                    <div class="textarea-input" style="margin-bottom: 10px;width: 420px;">
                        <textarea class="ckeditor3" id="5" name="duty" rows="3">{$job.job_duty}</textarea>
                    </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="6">Oferujemy</label></div>
                <div class="span7">
                    <div class="textarea-input" style="margin-bottom: 10px;width: 420px;">
                        <textarea class="ckeditor4" id="6" name="offer" rows="3">{$job.job_offer}</textarea>
                    </div>
                </div>
        </div>
        <hr>
        <div class="row-fluid">
                <div class="span3"><label for="11">Osoba kontaktowa</label></div>
                <div class="span7">
                        <div class="text-input">
                                <input type="text" id="11"  value="{$job.job_contact_name}" name="contact_name">
                        </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="13">Telefon</label></div>
                <div class="span7">
                        <div class="text-input">
                                <input type="text" id="13" value="{$job.job_contact_phone}" name="contact_phone">
                        </div>
                </div>
        </div>
                        
        <hr>

                    
        <div class="row-fluid">
                <div class="span3">Data rozpoczęcia pracy</div>
                <div class="span7">
                        <div class="data-input">
                            <input type="text" name="start_date" value="{$job.job_start_date}">
                            <i class="icon-calendar-input"></i>
                        </div>
                </div>
        </div>

        <div class="row-fluid">
                <div class="span3"><label for="1">Ilość godzin pracy tygodniowo</label></div>
                <div class="span7">
                        <div class="text-input">
                                <input type="text" name="hours_per_week" id="1" value="{$job.job_work_hours_per_week}">
                        </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="2">Ilość zmian w ciągu dnia</label></div>
                <div class="span7">
                        <div class="text-input">
                                <input type="text" name="shifts" id="2" value="{$job.job_work_shifts_per_day}">
                        </div>
                </div>
        </div>

        <hr>
        <div class="row-fluid">
                <div class="span3"><label for="8">Strona www</label></div>
                <div class="span7">
                        <div class="text-input">
                                <input type="text" {if isset($errorArray.www) && $errorArray.www} style="background:#EED3D7"{/if} name="www" id="8" value="{$job.job_www}">
                        </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="9">Fanpage</label></div>
                <div class="span7">
                        <div class="text-input">
                                <input type="text" {if isset($errorArray.fanpage) && $errorArray.fanpage} style="background:#EED3D7"{/if} name="fanpage" id="9" value="{$job.job_fanpage}">
                        </div>
                </div>
        </div>

        <hr>    
            
        <a href="#" onclick="$('.main-form').submit();" class="preview-next">EDYTUJ <i class="icon-chevron-right icon-white"></i></a>
        <div class="clear"></div>
	</form>
</section>
{/block}

{block name=additionalJavaScript}
<script type="text/javascript">
    $('.ckeditor2').ckeditor();
    $('.ckeditor3').ckeditor();
    $('.ckeditor4').ckeditor();
</script>
{/block}