{extends file="index.tpl"}

{block name=prebody} 
    <div class="preview-header">
        <div class="preview-tabs">
            <div class="preview-tab active">
                <div class="preview-tab-step">1</div>
                <div class="preview-tab-label">Wybierz typ ogłoszenia</div>
            </div>

            <div class="preview-tab-arrow active">
                <i class="arrow-right"></i>
                <i class="arrow-right"></i>
            </div>

            <div class="preview-tab">
                <div class="preview-tab-step">2</div>
                <div class="preview-tab-label">Wybierz formę płatności</div>
            </div>

            <div class="preview-tab-arrow">
                <i class="arrow-right"></i>
                <i class="arrow-right"></i>
            </div>

            <div class="preview-tab">
                <div class="preview-tab-step">3</div>
                <div class="preview-tab-label">Wznów ogłoszenie</div>
            </div>
        </div>

        <div class="clear"></div>
    </div>
{/block}
 
{block name=body}
    <form method="post" action="" id="wznowOgloszenie1" style="margin-top: -15px;">

        <input type="hidden" id="step" name="step" value="0" />
        
        <div class="preview-body">
            <input type="hidden" name="formula" value="0" />
            <input type="hidden" name="max" value="0" />
            <input type="hidden" name="rabat" value="0" />
            <div class="preview-formula mini">
                <label class="input-checkbox">
                    <input type="checkbox" name="formula" value="1" {if isset($smarty.post.formula) && $smarty.post.formula==1}checked="checked"{/if}>
                    <div class="checkbox-bg">
                        <div class="check-bg"></div>
                    </div>

                    <div class="preview-formula-header">
                        <i class="logo-formula"></i>
                        formula mini
                    </div>
                </label>
                <div class="clear"></div>
                <div class="preview-formula-description">
                    <span class="green-text">3 dni</span> = 9,00 zł + vat
                </div>
            </div>
            <div class="clear"></div>
            <div class="preview-formula happi">
                <label class="input-checkbox">
                    <input type="checkbox" name="formula" value="2" {if isset($smarty.post.formula) && $smarty.post.formula==2}checked="checked"{/if}>
                    <div class="checkbox-bg">
                        <div class="check-bg"></div>
                    </div>

                    <div class="preview-formula-header">
                        <i class="logo-formula"></i>
                        formula happi
                    </div>
                </label>
                <div class="clear"></div>
                <div class="preview-formula-description">
                    <span class="green-text">14 dni</span> = 29,00 zł + vat
                </div>
            </div>
            <div class="clear"></div>
            <div class="preview-formula maxi">
                <label class="input-checkbox">
                    <input type="checkbox" name="max" value="1" {if isset($smarty.post.max) && $smarty.post.max==1}checked="checked"{/if}>
                    <div class="checkbox-bg">
                        <div class="check-bg"></div>
                    </div>

                    <div class="preview-formula-header">
                        <i class="logo-formula"></i>
                        formula maxi
                    </div>
                </label>
                <div class="clear"></div>
                <div class="preview-formula-description">
                    <span class="green-text">emisja na stronie głównej</span> = + 49,00 zł + VAT
                </div>
            </div>
            <div class="clear"></div>
            <div class="preview-formula rabat">
                <label class="input-checkbox">
                    <input type="checkbox" name="rabat" value="1" {if isset($smarty.post.rabat) && $smarty.post.rabat==1}checked="checked"{/if}>
                    <div class="checkbox-bg">
                        <div class="check-bg"></div>
                    </div>

                    <div class="preview-formula-header">
                        <i class="logo-formula"></i>
                        posiadam kod rabatowy
                    </div>
                </label>
                <div class="clear"></div>
                <div class="preview-formula-description"{if isset($smarty.post.rabat) && $smarty.post.rabat==1} style="display: block;"{/if}>
                    <input type="text" placeholder="wpisz kod rabatowy" name="rabat_text" value="{if isset($smarty.post.rabat_text) && $smarty.post.rabat_text}{$smarty.post.rabat_text}{/if}">
                </div>
            </div>
            <div class="clear"></div>
        </div>		

        <div class="preview-footer">
            <a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia" class="preview-back">WRÓĆ DO LISTY OGŁOSZEŃ</a>
            <a href="#" onClick="$('#wznowOgloszenie1').submit();" class="preview-next">DALEJ <i class="icon-chevron-right icon-white"></i></a>
            <div class="clear"></div>
        </div>
    </form>
{/block}