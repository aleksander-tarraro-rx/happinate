<div class="center">
    <nav class="login-nav">
        {if $_user->getUserType()==1}
        <ul>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>
        {elseif $_user->getUserType()==2}
        <ul>
            <li{if $actionName=='dodajOgloszenieAction'} class="active"{/if}><a href="{$smarty.const.HOST}daj-ogloszenie"><strong>+</strong> Dodaj ogłoszenie</a></li>
            <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
            <li{if $actionName=='mojeGrupyAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-grupy">Moje grupy</a></li>
            <li{if $actionName=='moiKandydaciAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Moi kandydaci</a></li>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>            
        {/if}
    </nav>
</div>