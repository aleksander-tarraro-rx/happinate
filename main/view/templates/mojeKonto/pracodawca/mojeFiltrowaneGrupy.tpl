<table class="table candidate" border="0" cellspacing="0" cellpadding="0">
    <thead>
    <tr>
        <th class="column-name">Nazwa grupy</th>
        <th>Opis grupy</th>
        <th class="column-phone">Ilość kandydatów</th>
        <th class="column-action-small" style="width: 60px">Akcje</th>
    </tr>
    </thead>
    <tbody>
    {if $groups}
        {foreach from=$groups item=group}
            <tr>
                <td class="column-name"><a href="{$smarty.const.HOST}moje-konto/moje-grupy/podglad,{$group.id}">{$group.name}</a></td>
                <td>{$group.opis}</td>
                <td class="column-phone">{$model->countOneclickInGroup($group.id)}</td>
                <td class="column-action-small">
                    <a href="{$smarty.const.HOST}moje-konto/moje-grupy/podglad,{$group.id}" class="tooltip-item btn btn-success btn-mini" title="Podgląd"><i class="icon-eye-open icon-white"></i></a>
                    <a class="tooltip-item btn btn-danger btn-mini" onClick="$('#delLink').attr('href', '{$smarty.const.HOST}moje-konto/moje-grupy/usun,{$group.id}');" href="javascript:;" data-toggle="modal" data-target="#deleteGroup" data-original-title="Usuń grupę"><i class="icon-remove-sign  icon-white"></i></a>
                </td>
            </tr>
        {/foreach}
    {/if}
    </tbody>
</table>