{extends file="index.tpl"}

{block name=prebody}
<div class="center">
    <nav class="login-nav">
        {if $_user->getUserType()==1}
        <ul>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>
        {elseif $_user->getUserType()==2}
        <ul>
            <li{if $actionName=='oneclick'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/oneclick">1-click</a></li>
            <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
            <li{if $actionName=='mojeGrupyAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-grupy">Moje grupy</a></li>
            <li{if $actionName=='moiKandydaciAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Ulubieni kandydaci</a></li>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>            
        {/if}
    </nav>
</div>
    <div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Czy na pewno usunąć te ogłoszenie?</h3>
        </div>
        <div class="modal-body" style="line-height: 150%;">
            <p>Jeśli usuniesz to ogłoszenie automatycznie usunięci zostaną wszyscy kandydaci którzy aplikowali na to stanowisko.</p>
            <p>Aby tego uniknąć <b>dodaj kandydatów na ulubionych</b>.</p>
        </div>
        <div class="modal-footer">
            <a href="" id="delLink" class="btn btn-danger">Usuń</a>
            <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>
    
    <div id="inviteFriend" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Poleć nas innym pracodawcom</h3>
      </div>
     <div class="modal-body">
        <form action="" method="post" id="inviteFriendForm">
            <p><input type="text" name="friendEmail" placeholder="adres email..." style="width: 500px;" value="{if isset($smarty.post.friendEmail) && $smarty.post.friendEmail}{$smarty.post.friendEmail}{/if}" /></p>
        </form>
     </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <a href="#" onclick="$('#inviteFriendForm').submit();" class="btn btn-success">Wyślij zaproszenie</a>
      </div>
    </div>  
{/block}

{block name=body}
    
    <a href="javascript:;" data-toggle="modal" data-target="#inviteFriend" class="btn btn-happinate add-candidate no-border-radius" style="float: right;"><i class="icon-heart icon-white"></i> Poleć nas innym pracodawcom</a>
    
    <div style="height:15px; width: 100%;"></div>

    <table class="table notice" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th class="column-name">Nazwa</th>
                <th>Koniec</th>
                <th>Aplikacje</th>
                <th>1-click</th>
                <th>Przyjęte</th>
                <th>Wyświetleń</th>
                <th class="column-action">Akcje</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$jobsList item=job}
                <tr>
                    <td class="column-name"><a {if $job.job_status!=1} style="font-style: italic; color: #CCC;"{/if} href="{$smarty.const.HOST}moje-konto/moje-ogloszenia/{$job.job_id}">{$job.job_position}</a></td>
                    <td>{assign var=date value=" "|explode:$job.job_publish_date_end} {$date.0}</td>
                    <td>{$model->countApplications($job.job_id)}</td>
                    <td>{$model->countOneClick($job.job_id)}</td>
                    <td>{$model->countAcceptedApplications($job.job_id)}</td>
                    <td>{$job.job_views}</td>
                    <td class="column-action">
                        <a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia/{$job.job_id}" class="tooltip-item btn btn-mini" title="Lista kandydatów"><i class="icon-user"></i></a>
                        {if $job.job_publish_date_end < $smarty.now|date_format:"%Y-%m-%d"}
                            <span hidden="">wstrzymany</span>  
                            <a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia/wznow,{$job.job_id}" class="tooltip-item btn btn-mini btn-primary" data-original-title="Wznów"><i class="icon-repeat icon-white"><span hidden="">wstrzymany</span></i></a>
                        {else}
                            {if $job.job_status==1}
                                <a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia/wylacz,{$job.job_id}" class="tooltip-item btn btn-mini btn-success" title="Wstrzymaj publikacje"><i class="icon-pause icon-white"></i></a>
                            {elseif $job.job_status==3}
                                <span hidden="">wstrzymany</span>  
                                <a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia/wznow,{$job.job_id}" class="tooltip-item btn btn-mini btn-primary" data-original-title="Wznów ogłoszenie"><i class="icon-repeat icon-white"><span hidden="">wstrzymany</span></i></a>
                            {else}
                                <span hidden="">wstrzymany</span>  
                                <a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia/wlacz,{$job.job_id}" class="tooltip-item btn btn-mini btn-warning" title="Wznów publikację"><i class="icon-play icon-white"><span hidden>wstrzymany</span></i></a>
                            {/if}
                        {/if}
                        <a href="{$smarty.const.HOST}praca/{$job.job_id}/{$job.job_url_text}" target="_blank" class="tooltip-item btn btn-mini" title="Podgląd"><i class="icon-eye-open"></i></a>
                        <a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia/edytuj,{$job.job_id}" class="tooltip-item btn btn-mini" title="Edytuj"><i class="icon-pencil"></i></a>
                        <a href="#deleteModal" onClick="$('#delLink').attr('href', '{$smarty.const.HOST}moje-konto/moje-ogloszenia/usun,{$job.job_id}');" role="button" data-toggle="modal" class="tooltip-item btn btn-mini" title="Usuń"><i class="icon-trash"></i></a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
        
    <div class="clear"></div>
{/block}