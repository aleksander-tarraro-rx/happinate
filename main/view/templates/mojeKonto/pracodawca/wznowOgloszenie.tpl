{extends file="index.tpl"}
 
{block name=body}
    <section class="main-content">
    <form class="main-form" action="" method="post">
        
            <input type="hidden" id="formula" name="formula" value="0">
            
            <h2>FINALIZACJA<span class="underline-header-content" style="width:176px;"></span></h2>
            
            {if !$_user}
                <div class="clear" style="height:25px;"></div>
                <a href="{$smarty.const.HOST}zaloguj-sie">Zaloguj się</a> lub <a href="" onClick="if($('#noweKonto').css('display') == 'none') { $('#noweKonto').slideDown() } else { $('#noweKonto').slideUp() } ;return (false);">dodaj nowe konto</a>. Zajmie to zaledwie 5 sekund!

                <div class="clear" style="height:20px;"></div>
                <div {if !isset($smarty.post.step)}style="display: none;"{/if} id="noweKonto">
                    <div class="row-fluid">
                            <div class="span3"><label for="1">Adres email</label></div>
                            <div class="span7">
                                    <div class="text-input">
                                            <input type="text" name="email" id="1" {if isset($errorArray.email) && $errorArray.email} style="background:#EED3D7"{/if} value="{if isset($smarty.post.email) && $smarty.post.email}{$smarty.post.email}{elseif isset($smarty.session.basket.contact_email) && $smarty.session.basket.contact_email}{$smarty.session.basket.contact_email}{/if}">
                                    </div>
                            </div>
                    </div>
                    <div class="row-fluid">
                            <div class="span3"><label for="2">Hasło</label></div>
                            <div class="span7">
                                    <div class="text-input">
                                            <input type="password" name="pass" id="2" {if isset($errorArray.pass) && $errorArray.pass} style="background:#EED3D7"{/if}>
                                    </div>
                            </div>
                    </div>
                    <div class="row-fluid">
                            <div class="span3"><label for="3">Powtórz hasło</label></div>
                            <div class="span7">
                                    <div class="text-input">
                                            <input type="password" name="pass2" id="3" {if isset($errorArray.pass2) && $errorArray.pass2} style="background:#EED3D7"{/if}>
                                    </div>
                            </div>
                    </div>
                </div>

            {/if}
            <div class="clear" style="height:0px;"></div>
            <div class="formuly" style="margin-top: 10px;">
                <div class="formula free">
                    <div class="formula-header">
                        <i class="formula-logo"></i>
                        <p class="formula-header-text">formuła free</p>
                    </div>
                    <div class="formula-body">
                        <p class="formula-price"><small>bezpłatne</small></p>
                    </div>
                    <div class="formula-footer">
                        ogłoszenie bezpłatne na 14 dni
                    </div>
                    <a class="happi-btn" onclick="$('#formula').val(1);$('.main-form').submit();" href="#">WYBIERZ <i class="icon-chevron-right icon-white"></i></a>
                </div>

                <div class="formula mini">
                    <div class="formula-header">
                        <i class="formula-logo"></i>
                        <p class="formula-header-text">formuła mini *</p>
                    </div>
                    <div class="formula-body">
                        <p class="formula-price">9,<small>00 zł</small></p>
                    </div>
                    <div class="formula-footer">
                        wyróżnienie ogłoszenia na 7 dni
                    </div>
                    <a class="happi-btn" onclick="$('#formula').val(2);$('.main-form').submit();" href="#">WYBIERZ <i class="icon-chevron-right icon-white"></i></a>
                </div>

                <div class="formula happi">
                    <div class="formula-header">
                        <i class="formula-logo"></i>
                        <p class="formula-header-text">formuła happi *</p>
                    </div>
                    <div class="formula-body">
                        <p class="formula-price">29,<small>00 zł</small></p>
                    </div>
                    <div class="formula-footer">
                        wyróżnienie ogłoszenia na 14 dni
                    </div>
                    <a class="happi-btn" onclick="$('#formula').val(3);$('.main-form').submit();" href="#">WYBIERZ <i class="icon-chevron-right icon-white"></i></a>
                </div>
                <div class="clear"></div>
            </div>
                
            <p class="price-description">* Powyższe ceny są cenami netto i należy do nich doliczyć podatek VAT.</p>

            <hr>
            <a href="#" onclick="$('#step').val(2);$('.main-form').submit();" class="preview-back">WSTECZ</a>
            <div class="clear"></div>
    </form>
</section>
 
{/block}