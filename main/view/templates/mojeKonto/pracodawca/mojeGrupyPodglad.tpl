{extends file="index.tpl"}

{block name=prebody}
<div class="center">
    <nav class="login-nav">
        {if $_user->getUserType()==1}
        <ul>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>
        {elseif $_user->getUserType()==2}
        <ul>
            <li{if $actionName=='oneclick'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/oneclick">1-click</a></li>
            <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
            <li{if $actionName=='mojeGrupyAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-grupy">Moje grupy</a></li>
            <li{if $actionName=='moiKandydaciAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Ulubieni kandydaci</a></li>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>            
        {/if}
    </nav>
</div>
    
<div id="deleteMember" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Czy na pewno usunąć tego kandydata z grupy?</h3>
    </div>
    <div class="modal-footer">
        <a href="" id="delLink" class="btn btn-danger">Usuń</a>
        <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Anuluj</button>
    </div>
</div> 

{/block}

{block name=body}
<div class="data-table">
    
    <a class="btn btn-normal no-border-radius" href="/moje-konto/moje-grupy">&lt;&lt; Wróc do listy grup</a>

    <h2 style="margin: 0;">Kandydaci w grupie <b>{$group.name}</b></h2>
    
    <a href="{$smarty.const.HOST}moje-konto/moje-grupy/nowy,{$group.id}" class="btn btn-happinate btn-large add-candidate no-border-radius" style="float: left; font-size: 16px; margin-bottom: 10px;"><i class="icon-user icon-white"></i> Dodaj nowego kandydata do tej grupy</a>

    <table class="table candidate" border="0" cellspacing="0" cellpadding="0">
        <thead>
                <tr>
                    <th style="width: 700px" class="column-name">Imię i nazwisko (adres email)</th>
                    <th style="width: 100px">Telefon</th>
                    <th class="column-action-small" style="width: 60px">Akcje</th>
                </tr>
        </thead>
        <tbody>
            {if $members}
            {foreach from=$members item=member}
                <tr>
                    <td class="column-name"><a href="{$smarty.const.HOST}profil/k/{$member.user_id|md5}" target="_blank">{$member.user_name} {$member.user_surname} ({$member.user_email})</a></td>
                    <td>{$member.user_phone}</td>
                    <td class="column-action-small">
                        <a class="tooltip-item btn btn-danger btn-mini" onClick="$('#delLink').attr('href', '{$smarty.const.HOST}moje-konto/moje-grupy/usun-kandydata,{$group.id},{$member.user_id}');" href="javascript:;" data-toggle="modal" data-target="#deleteMember" data-original-title="Usuń z grupy"><i class="icon-remove-sign  icon-white"></i></a>
                    </td>
                </tr>
            {/foreach}
            {/if}
        </tbody>
    </table>
</div>
{/block}
