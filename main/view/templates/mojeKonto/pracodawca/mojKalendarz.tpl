{extends file="index.tpl"}

{block name=prebody}
	{if $usertype == 1}
            <div class="center">
                <nav class="login-nav">
                    <ul>
                        <li{if $actionName=='znajomiAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/znajomi">Znajomi</a></li>
                        <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
                        <li{if $actionName=='mojeCvAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-cv">Moje CV</a></li>
                        <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
                    </ul>            
                </nav>
            </div>
	{else}
            <div class="center">
                <nav class="login-nav">
                    {if $_user->getUserType()==1}
                    <ul>
                        <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
                    </ul>
                    {elseif $_user->getUserType()==2}
                    <ul>
                        <li{if $actionName=='dodajOgloszenieAction'} class="active"{/if}><a href="{$smarty.const.HOST}daj-ogloszenie"><strong>+</strong> Dodaj ogłoszenie</a></li>
                        <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
                        <li{if $actionName=='moiKandydaciAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Moi kandydaci</a></li>
                        <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
                    </ul>            
                    {/if}
                </nav>
            </div>
	{/if}
{/block}

{block name=body}

	<h2 style="margin-bottom:10px;">MÓJ KALENDARZ<span class="underline-header-content" style="width:240px;"></span></h2>

	<div class="calendar-header">
		<form action="" method="post">
			<div>
				Wybierz miesiąc
				<select name="m">
					<option value="1" {if $month=='1'}selected="selected"{/if}>Styczeń</option>
					<option value="2" {if $month=='2'}selected="selected"{/if}>Luty</option>
					<option value="3" {if $month=='3'}selected="selected"{/if}>Marzec</option>
					<option value="4" {if $month=='4'}selected="selected"{/if}>Kwiecień</option>
					<option value="5" {if $month=='5'}selected="selected"{/if}>Maj</option>
					<option value="6" {if $month=='6'}selected="selected"{/if}>Czerwiec</option>
					<option value="7" {if $month=='7'}selected="selected"{/if}>Lipiec</option>
					<option value="8" {if $month=='8'}selected="selected"{/if}>Sierpień</option>
					<option value="9" {if $month=='9'}selected="selected"{/if}>Wrzesień</option>
					<option value="10" {if $month=='10'}selected="selected"{/if}>Październik</option>
					<option value="11" {if $month=='11'}selected="selected"{/if}>Listopad</option>
					<option value="12" {if $month=='12'}selected="selected"{/if}>Grudzień</option>
				</select>				
			</div>	
			<div>
				Wybierz rok
				<select name="y">
					{foreach from=$yearsList item=v}
						<option value="{$v}" {if $year==$v}selcted="selected"{/if}>{$v}</option>
					{/foreach}
				</select>				
			</div>
			<div>
				<button type="submit" class="happi-btn">Pokaż</button>
			</div>				
		</form>
	</div>
	<div class="clear"></div>

	<input type="hidden" class="calendar-month" value="{$month}">
	<input type="hidden" class="calendar-year" value="{$year}">
	<input type="hidden" class="calendar-day" value="{$day}">

    <div class="calendar-big-left">
		<table class="calendar-table">
			{foreach from=$userCalendar item=month key=k}
				<thead>
					<tr>
					<th colspan="7">
						<div class="header">{$k}</div>
					</th>
					</tr>
				</thed>
				<tbody>
					<tr>
						<td>Pn</td>
						<td>Wt</td>
						<td>Śr</td>
						<td>Cz</td>
						<td>Pi</td>
						<td>So</td>
						<td>Nd</td>
					</tr>				
					<tr>
					{assign var="i" value=1}
					{foreach from=$month item=day key=k}					
						<td class="row {$day.type}">
							<a href="#" class="calendar-day-edit" data-day="{$day.label}">{$day.label}</a>
						</td>
					    {if $i%7==0}
					    	</tr><tr>
					    {/if}
						{assign var="i" value=$i+1}
					{/foreach}
					</tr>								
				</tbody>
			{/foreach}                     
		</table> 
	</div>

	<div class="calendar-big-right">

		<div class="calendar-day-header">
			Wybrany dzień: <span class="calendar-dayinfo"></span>
		</div>
		<div class="calendar-progress"></div>

		<br>

        <div class="tabbable tabs-right">
          <ul class="nav nav-tabs calendar-day-posistions">

          </ul>
          <div class="tab-content calendar-day-posistions-content">

          </div>
        </div>

		<hr>		
		<br>	
		<form method="post" class="calendar-day-edit-form">
			<input type="hidden" name="act" value="add">	
			<input type="hidden" name="date" class="form-date" value="">	
			<table>
				<tr>
					<td><span>Dodaj nową pozycję</span><br><br></td>
				<tr>
					<td>Godzina:</td>
					<td><input type="text" required name="time"><span class="help-inline">np: 18:00:00</span></td>
				</tr>
					<td>Opis:</td>
					<td><input type="text" required name="information"></label></td>
				</tr>
			</table>
			<br>
			<br>
			<button type="submit" class="happi-btn calendar-button-save">Zapisz zmiany</button>
		</form>


	</div>	

	<div class="clear"></div>




{/block}