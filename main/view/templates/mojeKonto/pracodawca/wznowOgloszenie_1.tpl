{extends file="index.tpl"}

{block name=prebody} 
    <div class="preview-header">
        <div class="preview-tabs">
            <div class="preview-tab">
                <div class="preview-tab-step">1</div>
                <div class="preview-tab-label">Wybierz typ ogłoszenia</div>
            </div>

            <div class="preview-tab-arrow">
                <i class="arrow-right"></i>
                <i class="arrow-right"></i>
            </div>

            <div class="preview-tab active">
                <div class="preview-tab-step">2</div>
                <div class="preview-tab-label">Wybierz formę płatności</div>
            </div>

            <div class="preview-tab-arrow active">
                <i class="arrow-right"></i>
                <i class="arrow-right"></i>
            </div>

            <div class="preview-tab">
                <div class="preview-tab-step">3</div>
                <div class="preview-tab-label">Wznów ogłoszenie</div>
            </div>
        </div>

        <div class="clear"></div>
    </div>
{/block}
 
{block name=body}
  <form method="post" action="" id="dodajOgloszenieStep3">

    <input type="hidden" id="step" name="step" value="1" />

    <div class="preview-body" style="width:700px;">
        <div class="preview-pay-choice">
                {if isset($smarty.session.resume.formula) && $smarty.session.resume.formula==1 && (!isset($smarty.session.resume.max) || $smarty.session.resume.max!=1)}
                <div class="pay-choice-phone{if isset($smarty.session.resume.payment_type) && $smarty.session.resume.payment_type==1} active{/if}">
                    <label>
                        <input type="radio" name="payment_type" onclick="$('#rabat').css('text-decoration', 'line-through');" value="1"{if isset($smarty.session.resume.payment_type) && $smarty.session.resume.payment_type==1} checked="checked"{/if}>
                        <i class="phone-icon"></i>
                        SMS
                    </label>
                </div>
                {/if}
                <div class="pay-choice-online{if isset($smarty.session.resume.payment_type) && $smarty.session.resume.payment_type==2} active{/if}">
                    <label>
                        <input type="radio" name="payment_type" onclick="$('#rabat').css('text-decoration', 'none');" value="2"{if isset($smarty.session.resume.payment_type) && $smarty.session.resume.payment_type==2} checked="checked"{/if}>
                        <i class="online-icon"></i>
                        Przelew online
                    </label>
                </div>
                {*
                <div class="pay-choice-card{if isset($smarty.session.resume.payment_type) && $smarty.session.resume.payment_type==3} active{/if}">
                    <label>
                        <input type="radio" name="payment_type" value="3"{if isset($smarty.session.resume.payment_type) && $smarty.session.resume.payment_type==3} checked="checked"{/if}>
                        <i class="card-icon"></i>
                        Karta kredytowa
                    </label>
                </div>
                *}
        </div>
        <div class="preview-summation">
            {if isset($smarty.session.resume.rabat) && $smarty.session.resume.rabat}
                {assign var="rabat" value={$model->getRabatPercent({$smarty.session.resume.rabat_text})}}
            {/if}
            <div class="your-choice">
                <h2>Twój wybór to:</h2>
                <ul>
                    <li>formuła {if $smarty.session.resume.formula==1}mini{else}happi{/if}</li>
                    {if $smarty.session.resume.max}<li>formuła maxi</li>{/if}
                    {if isset($smarty.session.resume.rabat) && $smarty.session.resume.rabat}<li id="rabat">rabat {$rabat}%</li>{/if}
                </ul>
            </div>
            <div class="your-sms"{if isset($smarty.session.resume.payment_type) && $smarty.session.resume.payment_type==1} style="display:block;"{/if}>
                <p>Wyślij SMS o treści <span class="green-text">AP.HI</span> pod numer <span class="green-text">79068</span>. Poniżej wpisz otrzymany sms’em kod.</p>
                <input type="text" placeholder="wprowadź kod" name="sms" value="">
            </div>
            <div class="your-pay">
                <h2>Do zapłaty:</h2>
                <span class="price">
                    {if $smarty.session.resume.formula==1 && $smarty.session.resume.max==0}{if $smarty.session.resume.rabat && $rabat>0}{assign var="price" value={math equation="x - (x * (y/100))" x=9 y=$rabat}}{else}{assign var="price" value=9}{/if}{/if}
                    {if $smarty.session.resume.formula==2 && $smarty.session.resume.max==0}{if $smarty.session.resume.rabat && $rabat>0}{assign var="price" value={math equation="x - (x * (y/100))" x=29 y=$rabat}}{else}{assign var="price" value=29}{/if}{/if}
                    {if $smarty.session.resume.formula==1 && $smarty.session.resume.max==1}{if $smarty.session.resume.rabat && $rabat>0}{assign var="price" value={math equation="x - (x * (y/100))" x=58 y=$rabat}}{else}{assign var="price" value=58}{/if}{/if}
                    {if $smarty.session.resume.formula==2 && $smarty.session.resume.max==1}{if $smarty.session.resume.rabat && $rabat>0}{assign var="price" value={math equation="x - (x * (y/100))" x=78 y=$rabat}}{else}{assign var="price" value=78}{/if}{/if}
                    <input type="hidden" name="amount" value="{$price}" />
                    {$price}
                    zł
                </span>
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>		

    <div class="preview-footer">
        <a href="#" onClick="$('#step').val(0);$('#dodajOgloszenieStep3').submit();" class="preview-back">WRÓĆ</a>
        <a href="#" onClick="$('#step').val(2);$('#dodajOgloszenieStep3').submit();" class="preview-next">DALEJ <i class="icon-chevron-right icon-white"></i></a>
        <div class="clear"></div>
    </div>
</form>
{/block}