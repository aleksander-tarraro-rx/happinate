{extends file="index.tpl"}

{block name=prebody}
<div class="center">
    <nav class="login-nav">
        {if $_user->getUserType()==1}
        <ul>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>
        {elseif $_user->getUserType()==2}
        <ul>
            <li{if $actionName=='oneclick'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/oneclick">1-click</a></li>
            <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
            <li{if $actionName=='mojeGrupyAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-grupy">Moje grupy</a></li>
            <li{if $actionName=='moiKandydaciAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Ulubieni kandydaci</a></li>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>
        {/if}
    </nav>
</div>

    <div id="addGroup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="addCandidateLabel">Dodaj nową grupę</h3>
        </div>
        <div class="modal-body">
            <form action="{$smarty.const.HOST}moje-konto/moje-grupy" method="post" id="addGroupForm">
                <input type="hidden" name="addGroup" value="1" />
                <input type="hidden" name="returnTo" value="moje-konto/moi-kandydaci"/>
                <p><label><b>Nazwa grypy</b>
                        <input type="text" name="name" placeholder="nazwa grupy..." style="width: 500px; margin-top: 5px; margin-bottom: 15px;" value="" /></label></p>
                <p><label><b>Opis grypy</b>
                        <input type="text" name="opis" placeholder="opis grupy..." style="width: 500px; margin-top: 5px; margin-bottom: 5px;" value="" /></label></p>
            </form>
        </div>
        <div class="modal-footer">
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
            <a href="javascript:;" onclick="$('#addGroupForm').submit();" class="btn btn-success">Dodaj</a>
        </div>
    </div>

    <div id="deleteGroup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Czy na pewno usunąć zaznaczone grupy?</h3>
        </div>
        <form action="{$smarty.const.HOST}moje-konto/moje-grupy" method="post" id="deleteGroupForm">
            <input type="hidden" name="deleteGroup" value="1" />
            <input type="hidden" name="returnTo" value="moje-konto/moi-kandydaci" />
            <input type="hidden" name="groupsIds" value="[{implode(',',array_keys($employerGroups))}]" class="groupsIds" />
        </form>
        <div class="modal-footer">
            <a href="javascript:;" onclick="$('#deleteGroupForm').submit();" id="delLink" class="btn btn-danger">Usuń</a>
            <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>

    <div id="addCandidate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Dodaj nowego kandydata do ulubionych</h3>
        <p>Możesz dodawać nowych kandydatów po przecinku.</p>
      </div>
      <div class="modal-body">
        <form id="addCandidateForm" class="main-form" action="{$smarty.const.HOST}moje-konto/moi-kandydaci/nowy" method="post">
            <input type="text" name="email" placeholder="Podaj email kandydata" style="width:500px;"><br>
            <textarea name="message" placeholder="Wiadomość" style="width:500px; resize:none;"></textarea>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <button class="btn btn-primary" onClick="$('#addCandidateForm').submit();">Wyślij zaproszenie</button>
      </div>
    </div>
            
    <div id="removeOneClick" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Czy na pewno usunąć tę osobę z listy ulubionych?</h3>
        </div>
        <div class="modal-footer">
            <a href="" id="removeOneClickHref" class="btn btn-success">Tak</a>
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Nie</button>
        </div>
    </div> 
                
    <div id="userTags" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <p id="myModalLabel"><strong>Rodzaje pracy dla użytkownika <span style="color:#2a85a0" id="tagEmail"></span></strong></p>
            <p id="userTagsText"></p>
        </div>
        <div class="modal-body">
            <div class="label label-important">Przypisz kandydatowi rodzaje prac które odpowiadają jego kwalifikacjom.</div><br/><br/>
          <form id="editUserTags" action="{$smarty.const.HOST}moje-konto/moi-kandydaci/edytuj-tagi" method="post">
                <input type="hidden" name="user" id="userid" value="" />
                <div class="row"> 
                {if $tagList}
                    {foreach from=$tagList item=tag}
                        <div class="span2" style="width: 200px;"><label><input type="checkbox" name="tags[]" value="{$tag.tag_id}" /> {$tag.tag_name}</label></div>
                    {/foreach}
                {/if}
                </div>
          </form>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Anuluj</button>
            <button class="btn btn-success" onClick="$('#editUserTags').submit();">Zmień rodzaje prac <i class="icon-white icon-chevron-right"></i></button>
        </div>
    </div>
                       
    <div id="editNoteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <p id="myModalLabel"><strong>Edytuj notatkę użytkownika <span style="color:#2a85a0" id="tagEmailNote"></span></strong></p>
            <p id="userTagsText"></p>
        </div>
        <div class="modal-body">
            <div class="label label-info"><i class="icon-white icon-info-sign" ></i> Twoja notatka nie będzie widoczna dla użytkownika.</div><br/><br/>
            <form id="editNote" class="main-form" action="" method="post">
                <input type="hidden" name="editNote" value="1" />
                <input type="hidden" name="user" id="useridNote" value="" />
                <textarea name="note" placeholder="Notatka" style="width:500px; resize:none;" id="userNote"></textarea>
            </form>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Anuluj</button>
            <button class="btn btn-success" onClick="$('#editNote').submit();">Edytuj <i class="icon-white icon-chevron-right"></i></button>
        </div>
    </div>         
                
    <div id="waitingInvitation" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <p id="myModalLabel"><strong>Oczekujące zaproszenia</strong></p>
            <p style="margin-top: 10px; font-size: 12px;">Ci kandydaci nie utworzyli jeszcze konta w serwisie happinate.com</p>
        </div>
        <div class="modal-body">
            {foreach from=$waiting item=wait}
                <div>{$wait.oneclick_email}</div>
            {/foreach}
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Zamknij</button>
        </div>
    </div> 
{/block}

{block name=body}
 <div class="data-table">
        <a href="javascript:;" data-toggle="modal" data-target="#addCandidate" style="font-size: 16px;" class="btn btn-large add-candidate btn-happinate no-border-radius">+ Dodaj nowego kandydata do ulubionych</a>
        <a href="#" data-toggle="modal" data-target="#addGroup" style="font-size: 16px; margin-right: 10px;" class="btn btn-large add-candidate btn-happinate no-border-radius">+ Dodaj nową grupę</a>

        <h2>Kandydaci <strong>1-click</strong> (ulubieni)</h2>
        
        <div data-toggle="modal" data-target="#waitingInvitation" style="float:right; margin-top: -45px;" class="btn btn-normal disabled">Oczekujące zaproszenia: <strong>{$model->countSendOneClick()}</strong></div>

     <a class="groups-management-wrapper-toggler btn-happinate no-border-radius action-button btn-large" href="#">Filtruj listę grup</a>
     <div class="groups-management-wrapper">
            <div class="groups-management custom-popup custom-popup-big">
                <div class="input-box">
                    <input class="search" type="text" placeholder="filtruj grupy..." />
                </div>
                <div class="groups">
                    <span class="embedded">
                        <input type="checkbox" value="all" checked="checked" id="allGroupsCheckbox" /> Wszystkie
                    </span>
                    {foreach from=$employerGroups item=group}
                        <span>
                            <input data-name="{$group.name}" type="checkbox" value="{$group.id}" checked="checked" /> <label style="display:inline;">{$group.name}</label>
                        </span>
                    {/foreach}
                </div>
                <div class="actions">
                    <span class="action-wrapper">
                        <a href="#" class="btn-happinate no-border-radius action-button group-filter-btn">Filtruj</a>
                    </span>
                    <span class="action-wrapper">
                        <a href="#" id="deleteGroupsProxy" class="btn-happinate no-border-radius action-button {if count($employerGroups) === 0}disabled{/if}">- Usuń zaznaczone grupy</a>
                    </span>
                    <a href="#" data-toggle="modal" data-target="#deleteGroup" id="deleteGroupsExecutor" style="display:none;">&nbsp;</a>
                </div>
            </div>
        </div>
        
        <table class="table candidate" border="0" cellspacing="0" cellpadding="0">
                <thead>
                        <tr>
                            <th class="column-name">Imię i nazwisko</th>
                            <th>Grupy</th>
                            <th class="column-phone">Telefon</th>
                            <th>CV</th>
                            <th class="column-action-small" style="width: 100px">Akcje</th>
                        </tr>
                </thead>
                <tbody>
                    {if $oneClickList}
                    {foreach from=$oneClickList item=app}
                        <tr data-user="{$app.user_id}">
                            <td class="column-name"><a target="_blank" href="{$smarty.const.HOST}profil/k/{$app.user_id|md5}">{if !empty($app.user_name) && !empty($app.user_surname)}{$app.user_name} {$app.user_surname}{else}{$app.user_email}{/if}</a></td>
                            <td class="groups-col" data-groups="[{implode(',', $app.groupsIds)}]">{if $app.groups}{$app.groups}{else}---{/if}</td>
                            <td class="column-phone">{$app.user_phone}</td>
                            <td class="column-phone">{if $app.cv_src}<a href="{$smarty.const.HOST}upload/userCV/{$app.cv_src}" target="_blank">[ podgląd cv ]</a>{else}---{/if}</td>
                            <td class="column-action-small">
                                <a class="tooltip-item btn btn-warning btn-mini" onClick="$('#removeOneClickHref').attr('href', '{$smarty.const.HOST}moje-konto/moi-kandydaci/usun-z-ulubionych,{$app.user_id}');" href="javascript:;" data-toggle="modal" data-target="#removeOneClick" data-original-title="Usuń z ulubionych"><i class="icon-star icon-white"></i></a>
                                <a href="#" class="tooltip-item manage-user-groups-toggle btn btn-mini btn-info"
                                    title="Dodaj do grupy">
                                    <i class="icon-tag icon-white"></i>
                                </a>

                                <div class="manage-user-groups-wrapper" data-user="{$app.user_id}" style="display:inline-block;position:relative;">
                                    <div class="manage-user-groups-content custom-popup custom-popup-small" style="top:8px;right:-105px;">
                                        <h4>Dodaj użytkownika do grupy</h4>
                                        <div class="input-box embedded">
                                            <input type="text" class="filterUserGroups" placeholder="wyszukaj grupę">
                                        </div>
                                            <span class="embedded">
                                                <input {if count($app.groupsIds) == count($employerGroups)}checked="checked"{/if} type="checkbox" value="all" class="selectAllUserGroups" /> dodaj do wszystkich
                                            </span>
                                        {foreach from=$employerGroups item=group}
                                            <span>
                                                <input {if in_array($group.id, $app.groupsIds)}checked="checked"{/if}
                                                    type="checkbox" 
                                                    value="{$group.id}" /> {$group.name}
                                            </span>
                                        {/foreach}

                                        <span class="action-wrapper embedded">
                                            <a href="javascript:;" class="btn-happinate no-border-radius action-button assign-groups-to-user">zapisz</a>
                                        </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    {/foreach}
                    {/if}
                </tbody>
        </table>
{/block}

{block name=additionalJavaScript}
<script>
    jQuery(function() {
        var availableTags = [ {foreach from=$tagList item=tag}"{$tag.tag_name}",{/foreach} ];
        function split( val ) { return val.split( /,\s*/ ); }
        function extractLast( term ) { return split( term ).pop(); }
        jQuery(".tags").bind( "keydown", function( event ) { 
            if ( event.keyCode === $.ui.keyCode.TAB && jQuery( this ).data( "ui-autocomplete" ).menu.active ) {
                event.preventDefault();
            }
        }).autocomplete({
            minLength: 0,
            source: function( request, response ) {
                response( $.ui.autocomplete.filter(
                availableTags, extractLast( request.term ) ) );
            },
            focus: function() {
                return false;
            },
            select: function( event, ui ) {
                var terms = split( this.value );
                terms.pop();
                // add the selected item
                terms.push( ui.item.value );
                // add placeholder to get the comma-and-space at the end
                terms.push( "" );
                this.value = terms.join( ", " );
                return false;
            }
        });
    });

    $(document).ready(function() {
        var hostname = '{$smarty.const.HOST}';
        var $allGroupCheckboxes = $('.groups-management-wrapper .groups span input[type="checkbox"]');
        var $selectAllCheckbox = $('#allGroupsCheckbox');
        var groupsCount = {count($employerGroups)};

        // hiding popup when clicking on non-popup
        $('body').click(function(e) {
            var $target = $(e.target || e.srcElement);
            if (!($target.parents('.custom-popup').length > 0 && $target.is(':not(a)'))) {
                $('.custom-popup').hide();
            }
        });

        // toggling groups management panel
        $('.groups-management-wrapper-toggler').on('click', function () {
            $('.groups-management').toggle();
            return false;
        });

        // filtering groups management elements
        $('input.search').on('keyup', function() {
            var $this = $(this);
            var $groupsList = $this.parents('.groups-management-wrapper').find('div.groups span:not(.embedded)');

            var filterFactory = function (filter) {
                var _filter = filter;
                return function (index, value) {
                    var $value = $(value);
                    ($value.text().toLowerCase().indexOf(_filter.toLowerCase()) === -1) ? $value.hide() : $value.show();
                }
            };

            $.each($groupsList, filterFactory($this.val()));
        });

        // filtering applicants rows
        $allGroupCheckboxes.on('change', function(){
            var $this = $(this);

            if ($this.val() === 'all') {
                $.each($allGroupCheckboxes, function (index, element) {
                    var $element = $(element);
                    if ($element.parents('li').first().hasClass('embedded')) {
                        return ;
                    }
                    if ($this.attr('checked') === 'checked' && $element.attr('checked') !== 'checked') {
                        $element.attr('checked', 'checked');
                    } else if ($this.attr('checked') !== 'checked' && $element.attr('checked') === 'checked') {
                        $element.removeAttr('checked');
                    }
                });
            }

            var groupsToShow = getGroupsToShow($allGroupCheckboxes);
            if ($this.val() !== 'all') {
                (groupsToShow.length === groupsCount) ? $selectAllCheckbox.attr('checked', 'checked') : $selectAllCheckbox.removeAttr('checked')
                ;
            }
            (groupsToShow.length === 0) ? $('#deleteGroupsProxy').addClass('disabled') : $('#deleteGroupsProxy').removeClass('disabled');
            $('#deleteGroupForm input.groupsIds').val(JSON.stringify(groupsToShow));
        });

        $('.group-filter-btn').click(function() {
            $('.custom-popup').hide();
            var summary = userGroupsSummary($(this).parents('.groups-management-wrapper'));

            $.each($('table.candidate tbody tr'), function (index, element) {
                var $tableRow = $(element);
                var rowGroupIds = $tableRow.find('.groups-col').first().data('groups');
                var showRow = false;
                if (summary.checked.length > 0) {
                    $.each(summary.checked, function (idx, elem) {
                        if (rowGroupIds.indexOf(elem) !== -1) {
                            showRow = true;
                        }
                    });
                } else if (rowGroupIds.length === 0) {
                    showRow = true;
                }
                showRow ? $tableRow.show() : $tableRow.hide();
            });

            return false;
        });

        var getGroupsToShow = function ($checkboxes) {
            var groups = [];
            $.each($checkboxes, function (index, element) {
                var $element = $(element);
                if ($element.attr('checked') === 'checked') {
                    var parsedId = parseInt($element.val());
                    if (!isNaN(parsedId)) {
                        groups.push(parsedId);
                    }
                }
            });

            return groups;
        };

        $('#deleteGroupsProxy').on('click', function () {
            var visibleGroups = getGroupsToShow($allGroupCheckboxes);
            if (visibleGroups.length > 0) {
                $('#deleteGroupsExecutor').trigger('click');
            }

            return false;
        });

        // toggling tag management panel
        $('.manage-user-groups-toggle').on('click', function () {
            var $popup = $(this).parent().find('.manage-user-groups-content');
            if ($popup.is(':not(visible)')) {
                $('.custom-popup').hide();
            }
            $popup.toggle();

            return false;
        });

        // filtering tag list
        $('.filterUserGroups').on('keyup', function () {
            var $this = $(this);
            var $groupsList = $this.parents('div.manage-user-groups-wrapper').find('.manage-user-groups-content span:not(.embedded)');

            var filterFactory = function (filter) {
                var _filter = filter;
                return function (index, value) {
                    var $value = $(value);
                    if ($value.text().toLowerCase().indexOf(_filter.toLowerCase()) === -1) {
                        $value.hide();
                    } else {
                        $value.show();
                    }
                }
            };

            $.each($groupsList, filterFactory($this.val()));
        });

        var userGroupsSummary = function ($wrapperObj) {
            var $elements = $wrapperObj.find('span:not(.embedded) input[type="checkbox"]');
            {literal}
            var summary = {totalElements: $elements.length, checked: [], unchecked: []};
            {/literal}

            $.each($elements, function (index, value) {
                var $checkbox = $(value);
                (Boolean($checkbox.attr('checked')))
                        ? summary.checked.push(parseInt($checkbox.val()))
                        : summary.unchecked.push(parseInt($checkbox.val()))
                ;
            });

            return summary;
        };

        var markAllCheckboxesAs = function ($wrapperObj, checked) {
            var $checkboxes = $wrapperObj.find('span:not(.embedded) input[type="checkbox"]');
            (checked === true)
                    ? $checkboxes.each(function(index, element){ $(element).attr('checked', 'checked') })
                    : $checkboxes.each(function(index, element){ $(element).removeAttr('checked'); })
            ;
        };

        // toggling group subscription
        $('.manage-user-groups-content span input[type="checkbox"]').on('change', function () {
            var $this = $(this);
            var $wrapper = $this.parents('div.manage-user-groups-wrapper');

            if ($this.val() === 'all') {
                markAllCheckboxesAs($wrapper, this.checked);
                return true;
            }

            var summary = userGroupsSummary($wrapper);

            (summary.unchecked.length === 0)
                    ? $wrapper.find('.selectAllUserGroups').attr('checked', 'checked')
                    : $wrapper.find('.selectAllUserGroups').removeAttr('checked');
        });

        {literal}
        $('.assign-groups-to-user').click(function() {
            $('.custom-popup').hide();
            var $this = $(this);
            var $wrapper = $this.parents('div.manage-user-groups-wrapper');
            var userId = $wrapper.data('user');

            var summary = userGroupsSummary($wrapper);

            if (summary.checked.length > 0) {
                $.post(hostname + "moje-konto/grupy/dodaj-kandydata," + userId, {'groups': summary.checked}, function (response) {});
            }
            if (summary.unchecked.length > 0) {
                $.post(hostname + "moje-konto/grupy/usun-kandydata," + userId, {'groups': summary.unchecked}, function (response) {});
            }

            return false;
        });
        {/literal}
    });
</script>
{/block}