{extends file="index.tpl"}

{block name=prebody}
<div class="center">
    <nav class="login-nav">
        {if $_user->getUserType()==1}
        <ul>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>
        {elseif $_user->getUserType()==2}
        <ul>
            <li{if $actionName=='oneclick'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/oneclick">1-click</a></li>
            <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
            <li{if $actionName=='mojeGrupyAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-grupy">Moje grupy</a></li>
            <li{if $actionName=='moiKandydaciAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Ulubieni kandydaci</a></li>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>            
        {/if}
    </nav>
</div>
 
{/block}

{block name=body}
<div class="data-table">
    
    <a class="btn btn-normal no-border-radius" href="{$smarty.const.HOST}moje-konto/moje-grupy/podglad,{$group.id}">&lt;&lt; Wróc do podglądu grupy</a>

    <h2 style="margin-bottom: 20px;">Dodaj nowych kandydatów do grupy <b>{$group.name}</b></h2>
    
    <form action="" method="post">
        <input type="hidden" name="member" value="1" />
        <div class="multiple-select-input"  style="width:978px;">
            <select name="oneclick[]" data-placeholder="Wybierz ulubionych użytkowników" multiple="" class="chosen-select" style="width:968px;">
                <option></option>
                {if $oneclick}
                    {foreach from=$oneclick item=user}
                        <option {if isset($smarty.post.oneclick) && in_array($user.user_id, $smarty.post.oneclick)} selected="selected"{elseif isset($smarty.session.basket.oneclick) && in_array($user.user_id, $smarty.session.basket.oneclick)} selected="selected"{/if} value="{$user.user_id}">{if $user.user_name}{$user.user_name} {/if}{if $user.user_surname}{$user.user_surname} {/if}{$user.user_email}</option>
                    {/foreach}
                {/if}
            </select>
        </div>
            
        <a class="btn btn-large add-candidate btn-happinate no-border-radius" style="font-size: 16px" onclick="$(this).parent().submit();" href="javascript:;">Dodaj nowych kandydatów >></a>
        
        <div class="clear"></div>

    </form>

</div>
{/block}
