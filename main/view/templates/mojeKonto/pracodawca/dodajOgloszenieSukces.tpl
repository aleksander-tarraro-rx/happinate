{extends file="index.tpl"}

{block name=prebody}
    {include file="mojeKonto/pracodawca/header.tpl"}
{/block}

{block name=body}
<div class="preview-body" style="width:809px;">
    <img src="{$smarty.const.HOST}view/images/published-notice.jpg">
    <div style="width:350px;height:45px;margin:12px auto;">
    <a href="{$smarty.const.HOST}praca/{$job_id}" class="happi-btn brown" style="padding:12px 15px 14px 15px;float:left;">Zobacz ogłoszenie</a>
    <a href="{$smarty.const.HOST}moje-konto/dodaj-ogloszenie" class="happi-btn" style="padding:12px 15px 14px 15px;float:left;margin-left:14px;">Daj nowe ogłoszenie  <i class="icon-chevron-right icon-white"></i></a>
    </div>
    <div class="clear"></div>
</div>		
{/block}