{extends file="index.tpl"}

{block name=prebody}
<div class="center">
    <nav class="login-nav">
        {if $_user->getUserType()==1}
        <ul>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>
        {elseif $_user->getUserType()==2}
        <ul>
            <li{if $actionName=='oneclickAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/oneclick">1-click</a></li>
            <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
            <li{if $actionName=='mojeGrupyAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-grupy">Moje grupy</a></li>
            <li{if $actionName=='moiKandydaciAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Ulubieni kandydaci</a></li>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>            
        {/if}
    </nav>
</div>
{/block}

{block name=body}

    <a href="{$smarty.const.HOST}moje-konto/oneclick/nowe" class="btn btn-large btn-happinate add-candidate no-border-radius" style="float: left; font-size: 17px;"><i class="icon-plus icon-white"></i> Wyślij nową ofertę typu 1-click </a>
    
    <div style="height:15px; width: 100%;"></div>

    <table class="table notice" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th class="column-name">Nazwa</th>
                <th>Data</th>
                <th>Rozpatruje</th>
                <th>Przyjęte</th>
                <th>Rezerwowi</th>
                <th>Odrzucone</th>
                <th>Lista</th>
            </tr>
        </thead>
        <tbody>
            {if $oneclickList}
            {foreach from=$oneclickList item=job}
                {assign var=count value=$model->countOneClickNotes({$job.id})}
                <tr>
                    <td class="column-name"><a href="{$smarty.const.HOST}moje-konto/oneclick/{$job.id}">{$job.title}</a></td>
                    <td>{$job.date}</td>
                    <td>{if $count.0}{$count.0}{else}0{/if}</td>
                    <td>{if $count.1}{$count.1}{else}0{/if}</td>
                    <td>{if $count.2}{$count.2}{else}0{/if}</td>
                    <td>{if $count.3}{$count.3}{else}0{/if}</td>
                    <td>
                        <a href="{$smarty.const.HOST}moje-konto/oneclick/{$job.id}" class="tooltip-item btn btn-mini" title="Lista kandydatów"><i class="icon-user"></i></a>
                        <a href="{$smarty.const.HOST}moje-konto/oneclick/copy,{$job.id}" class="tooltip-item btn btn-mini" title="Wyślij jeszcze raz to powiadomienie"><i class="icon-repeat"></i></a>
                    </td>
                </tr>
            {/foreach}
            {/if}
        </tbody>
    </table>
        
    <div class="clear"></div>
{/block}