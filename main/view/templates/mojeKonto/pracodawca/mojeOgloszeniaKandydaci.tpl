{extends file="index.tpl"}

{block name=prebody}
    <div class="center">
        <nav class="login-nav">
            {if $_user->getUserType()==1}
                <ul>
                    <li{if $actionName=='indexAction'} class="active"{/if}>
                        <a href="{$smarty.const.HOST}moje-konto">Mój profil</a>
                    </li>
                </ul>
            {elseif $_user->getUserType()==2}
                <ul>
                    <li{if $actionName=='oneclick'} class="active"{/if}>
                        <a href="{$smarty.const.HOST}moje-konto/oneclick">1-click</a>
                    </li>
                    <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}>
                        <a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a>
                    </li>
                    <li{if $actionName=='mojeGrupyAction'} class="active"{/if}>
                        <a href="{$smarty.const.HOST}moje-konto/moje-grupy">Moje grupy</a>
                    </li>
                    <li{if $actionName=='moiKandydaciAction'} class="active"{/if}>
                        <a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Ulubieni kandydaci</a>
                    </li>
                    <li{if $actionName=='indexAction'} class="active"{/if}>
                        <a href="{$smarty.const.HOST}moje-konto">Mój profil</a>
                    </li>
                </ul>
            {/if}
        </nav>
    </div>

    <div id="addGroup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="addCandidateLabel">Dodaj nową grupę</h3>
        </div>
        <div class="modal-body">
            <form action="{$smarty.const.HOST}moje-konto/moje-grupy" method="post" id="addGroupForm">
                <input type="hidden" name="addGroup" value="1"/>
                <input type="hidden" name="returnTo" value="moje-konto/moje-ogloszenia/{$position.job_id}"/>

                <p>
                    <label>
                        <b>Nazwa grupy</b>
                        <input type="text" name="name" placeholder="nazwa grupy..."
                               style="width: 500px; margin-top: 5px; margin-bottom: 15px;" value=""/>
                    </label>
                </p>

                <p>
                    <label>
                        <b>Opis grupy</b>
                        <input type="text" name="opis" placeholder="opis grupy..."
                               style="width: 500px; margin-top: 5px; margin-bottom: 5px;" value=""/>
                    </label>
                </p>
            </form>
        </div>
        <div class="modal-footer">
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
            <a href="javascript:;" onclick="$('#addGroupForm').submit();" class="btn btn-success">Dodaj</a>
        </div>
    </div>

    <div id="addOneClick" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Czy na pewno dodać tę osobę do listy ulubionych?</h3>
        </div>
        <div class="modal-footer">
            <a href="" id="addOneClickHref" class="btn btn-success">Tak</a>
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Nie</button>
        </div>
    </div>

    <div id="removeOneClick" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Czy na pewno dodać tę osobę do listy ulubionych?</h3>
        </div>
        <div class="modal-footer">
            <a href="" id="removeOneClickHref" class="btn btn-success">Tak</a>
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Nie</button>
        </div>
    </div>

    <div id="changeStatus" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Zmień aktualny status kandydata</h3>

            <p>UWAGA! Zmiana statusu jest możliwa tylko raz i zostanie wysłana również na adres emailowy osoby
                kandydującej.</p>
        </div>
        <div class="modal-footer">
            <a href="" id="statusAccepted" class="btn btn-success">Zaakceptuj kandydata</a>
            <a href="" id="declinedAccepted" class="btn btn-danger">Odrzuć kandydata</a>
            <button class="btn " data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>

    <div id="oneClick" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Opcja 1-click!</h3>

            <p>Ten kandydat zostanie przeniesiony z listy rezerwowych na główną listę kandydatów 1-click.</p>

            <p><br></p>

            <p>UWAGA! Zmiana statusu jest możliwa tylko raz i zostanie wysłana również na adres emailowy osoby
                kandydującej.</p>

        </div>
        <div class="modal-footer">
            <a href="" id="oneClickUrl" class="btn btn-success"><i class="icon-ok-circle icon-white"></i> Zaakceptuj
                kandydata!</a>
            <button class="btn btn-warning" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>

    <div id="odrzuc" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Odrzucanie kandydata!</h3>

            <p>Ten kandydat zostanie odrzucony z listy 1-click.</p>

            <p><br></p>

            <p>UWAGA! Zmiana statusu jest możliwa tylko raz i zostanie wysłana również na adres emailowy osoby
                kandydującej.</p>

        </div>
        <div class="modal-footer">
            <a href="" id="odrzucUrl" class="btn btn-danger"><i class="icon-remove-sign icon-white"></i> Odrzuć
                kandydata!</a>
            <button class="btn btn-warning" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>

    <div id="zaakceptuj" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>UWAGA!</h3>

            <p>Zmiana statusu jest możliwa tylko raz i zostanie wysłana również i zostanie wysłane powiadomienie na
                adres emailowy osoby kandydującej.</p>
        </div>
        <div class="modal-footer">
            <a href="" id="zaakceptujUrl" class="btn btn-success"><i class="icon-ok-circle icon-white"></i> Zaakceptuj
                kandydata</a>
            <button class="btn btn-warning" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>

    <div id="odrzuc" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>UWAGA!</h3>

            <p>Zmiana statusu jest możliwa tylko raz i zostanie wysłana również i zostanie wysłane powiadomienie na
                adres emailowy osoby kandydującej.</p>
        </div>
        <div class="modal-footer">
            <a href="" id="odrzucUrl" class="btn btn-danger"><i class="icon-remove-sign icon-white"></i> Odrzuć
                kandydata!</a>
            <button class="btn btn-warning" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>
{/block}

{block name=body}
    <div class="data-table">
    <a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia" class="btn btn-normal no-border-radius"><< Wróc do listy
        ogłoszeń</a>
    <a target="_blank" style="margin: 30px 0px 0px 10px; font-size: 30px; display: block;"
       href="{$smarty.const.HOST}praca/{$position.job_id}/{$position.job_url_text}">{$position.job_position}</a>
    <h2>Kandydaci którzy aplikowali na to ogłoszenie:</h2>
    <table class="table candidate" border="0" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            <th align="center" style="width: 30px;"></th>
            <th class="column-name">Imię i nazwisko</th>
            <th>Miejscowość</th>
            <th class="column-phone">Telefon</th>
            <th>CV</th>
            <th class="column-action-small" style="width: 130px;">Akcje</th>
        </tr>
        </thead>
        <tbody>
        {if $appList}
            {foreach from=$appList item=app}
                <tr>
                    <td align="center" style="text-align:center;">
                        <span style="display: none;">{$app.application_status}</span>
                        {if $app.application_status==2}
                        <span class="label label-success tooltip-item" title="Kandydat zaakceptowany">
                            <i class="icon icon-white icon-ok-sign"></i>
                        </span>
                        {elseif $app.application_status==1}
                        <span class="label label-error tooltip-item" title="Kandydat odrzucony">
                            <i class="icon icon-white icon-remove-sign"></i>
                        </span>
                        {else}
                            <span class="label label-info tooltip-item" title="Kandydat rozpatrywany">
                                <i class="icon icon-white icon-question-sign"></i>
                            </span>
                        {/if}
                    </td>
                    <td class="column-name">
                        <a target="_blank" href="{$smarty.const.HOST}profil/k/{$app.user_id|md5}">
                            {if !empty($app.user_name) && !empty($app.user_surname)}
                                {$app.user_name} {$app.user_surname}
                            {else}
                                {$app.user_email}
                            {/if}
                        </a>
                    </td>
                    <td>{$app.user_city}</td>
                    <td class="column-phone">{$app.user_phone}</td>
                    <td>
                        {if $app.cv_src}
                            <a href="{$smarty.const.HOST}upload/userCV/{$app.cv_src}" target="_blank">[podgląd CV ]</a>
                        {else}
                            <i>-- brak --</i>
                        {/if}
                    </td>
                    <td class="column-action-small" style="width: 70px;">
                        <a href="{$smarty.const.HOST}profil/k/{$app.user_id|md5}" class="tooltip-item btn btn-mini" title="Podgląd">
                            <i class="icon-eye-open"></i>
                        </a>
                        {if $app.application_status==0}
                            <a onClick="$('#zaakceptujUrl').attr('href', '{$smarty.const.HOST}moje-konto/moi-kandydaci/przyjmij-kandydata,{$app.user_id},{$app.job_id}');"
                               href="javascript:;" data-toggle="modal" data-target="#zaakceptuj"
                               class="tooltip-item btn btn-mini btn-success" title="Zaakceptuj kandydata">
                                <i class="icon-ok-sign icon-white"></i>
                            </a>
                            <a onClick="$('#odrzucUrl').attr('href', '{$smarty.const.HOST}moje-konto/moi-kandydaci/odrzuc-kandydata,{$app.user_id},{$app.job_id}');"
                               href="javascript:;" data-toggle="modal" data-target="#odrzuc"
                               class="tooltip-item btn btn-mini btn-danger" title="Odrzuć kandydata">
                                <i class="icon-remove-sign icon-white"></i>
                            </a>
                            <a href="javascript:;" class="tooltip-item manage-group btn btn-mini btn-info" title="Dodaj do grupy">
                                <i class="icon-tag icon-white"></i>
                            </a>
                        {/if}

                        <div class="manage-group-wrapper" data-user="{$app.user_id}" style="display:inline-block;position:relative;">
                            <div class="manage-group-content custom-popup custom-popup-small" style="top:8px;right:-105px;">
                                <h4>Dodaj użytkownika do grupy</h4>
                                <div class="input-box">
                                    <input class="manage-group-filter" type="text" placeholder="wyszukaj grupę">
                                </div>
                                <span class="embedded">
                                    <input class="selectAllCheckbox" {if count($appGroups) == count($app.groups)}checked="checked"{/if}
                                           type="checkbox" value="all">
                                    dodaj do wszystkich
                                </span>
                                {foreach from=$appGroups item=appGroup}
                                    <span>
                                        <input {if in_array($appGroup.id, $app.groups)}checked="checked"{/if}
                                               type="checkbox" value="{$appGroup.id}">
                                        <label style="display: inline;">{$appGroup.name}</label>
                                    </span>
                                {/foreach}
                                <span class="action-wrapper embedded">
                                    <a href="javascript:;" class="btn-happinate no-border-radius action-button assign-groups-to-user">zapisz</a>
                                </span>
                                <span class="action-wrapper embedded">
                                    <a href="javascript:;" data-toggle="modal" data-target="#addGroup" class="btn-happinate no-border-radius action-button">dodaj nową grupę</a>
                                </span>
                            </div>
                        </div>
                    </td>
                </tr>
            {/foreach}
        {/if}
        </tbody>
    </table>
    {if $appOneClickList}
        <h2>Kandydaci 1-click - ulubieni: ({$model->getAcceptedOneClick($position.job_id)}/{$model->getVacancies($position.job_id)} wakatów)</h2>
        <table class="table candidate" border="0" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th align="center" style="width: 30px;"></th>
                <th class="column-name">Imię i nazwisko</th>
                <th>Miejscowość</th>
                <th class="column-phone">Telefon</th>
                <th class="column-action-small" style="width: 50px;">Akcje</th>
            </tr>
            </thead>
            <tbody>

            {foreach from=$appOneClickList item=app}
                <tr>
                    <td align="center" style="text-align:center;">
                        <span style="display: none;">{$app.status}</span>
                        {if $app.status==3}
                            <span class="label label-important tooltip-item" title="1-click odrzucony">
                                <i class="icon icon-white icon-remove"></i>
                            </span>
                        {elseif $app.status==2}
                            <span class="label label-warning tooltip-item" title="1-click rezerwowy">
                                <i class="icon icon-white icon-flag"></i>
                            </span>
                        {elseif $app.status==1}
                            <span class="label label-success tooltip-item" title="1-click przyjęty">
                                <i class="icon icon-white icon-ok-sign"></i>
                            </span>
                        {else}
                            <span class="label label-info tooltip-item" title="1-click rozpatrywany">
                                <i class="icon icon-white icon-question-sign"></i>
                            </span>
                        {/if}
                    </td>
                    <td class="column-name">
                        <a target="_blank" href="{$smarty.const.HOST}profil/k/{$app.user_id|md5}">{if !empty($app.user_name) || !empty($app.user_surname)}{$app.user_name} {$app.user_surname}{else}{$app.user_email}{/if}</a>
                    </td>
                    <td>{$app.user_city}</td>
                    <td class="column-phone">{$app.user_phone}</td>
                    <td class="column-action-small" style="width: 70px;">
                        <a href="{$smarty.const.HOST}profil/k/{$app.user_id|md5}" class="tooltip-item btn btn-mini" title="Podgląd">
                            <i class="icon-eye-open"></i>
                        </a>
                        {if $app.status==2}
                            <a onClick="$('#oneClickUrl').attr('href','{$smarty.const.HOST}moje-konto/moje-ogloszenia/oneclick,{$position.job_id},{$app.user_id}');"
                               href="javascript:;" data-toggle="modal" data-target="#oneClick"
                               class="tooltip-item btn btn-mini btn-success" title="Zaproś kandydata do pracy">
                                <i class="icon-ok-sign icon-white"></i>
                            </a>
                        {/if}
                        {if $app.status!=3}
                            <a onClick="$('#odrzucUrl').attr('href','{$smarty.const.HOST}moje-konto/moje-ogloszenia/odrzuc,{$position.job_id},{$app.user_id}');"
                               href="javascript:;" data-toggle="modal" data-target="#odrzuc"
                               class="tooltip-item btn btn-mini btn-danger" title="Odrzuć kandydata">
                                <i class="icon-remove-sign icon-white"></i>
                            </a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    {/if}
    <div class="legend">
        <h3>Legenda:</h3>

        <p>
            <span class="label label-info">
                <i class="icon icon-white icon-question-sign"></i>
            </span> - kandydat otrzymał zaproszenie ale jeszcze nie potwierdził, że stawi się w pracy
        </p>

        <p>
            <span class="label label-success">
                <i class="icon icon-white icon-ok-sign"></i>
            </span> - kandydat jest gotowy
            do pracy
        </p>

        <p>
            <span class="label label-warning">
                <i class="icon icon-white icon-flag"></i>
            </span> - kandydat jest gotowy do pracy ale ze względu na niewystarczającą ilość wakatów został przeniesiony na listę rezerwowych
        </p>

        <p>
            <span class="label label-important">
                <i class="icon icon-white icon-remove"></i>
            </span> - kandydat został odrzucony
        </p>
    </div>
    {if $smarty.const.MOD_CALENDAR}
        <h2>Kalendarz rekrutacji:</h2>
        <table class="table candidate2" border="0" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th class="column-name">Imię i nazwisko</th>
                <th>Nazwa ogłoszenia</th>
                <th>Data</th>
                <th>Godzina</th>
                <th class="column-action-small">Akcje</th>
            </tr>
            </thead>
            <tbody>
            {if $calendarAppList}
                {foreach from=$calendarAppList item=app}
                    <tr>
                        <td class="column-name">
                            <a target="_blank" href="{$smarty.const.HOST}profil/k/{$app.user_id|md5}">
                                {if !empty($app.user_name) && !empty($app.user_surname)}
                                    {$app.user_name} {$app.user_surname}
                                {else}
                                    {$app.user_email}
                                {/if}
                            </a>
                        </td>
                        <td>
                            <a href="{$smarty.const.HOST}praca/{$app.job_id}/{$app.job_url_text}" target="_blank">
                                {$app.job_position}
                            </a>
                        </td>
                        <td>{$app.date}</td>
                        <td>{$app.hour}:00</td>
                        <td class="column-action-small" style="width: 100px;">
                            {if $app.confirmation !=1}
                                <form action="" method="post">
                                    <input type="hidden" name="act" value="confirm">
                                    <input type="hidden" name="jid" value="{$app.job_id}">
                                    <input type="hidden" name="uid" value="{$app.user_id}">
                                    <input type="hidden" name="date" value="{$app.date}">
                                    <input type="hidden" name="hour" value="{$app.hour}:00:00">
                                    <input type="hidden" name="information"
                                           value="Spotkanie rekrutacyjne z {if !empty($app.user_name) && !empty($app.user_surname)}{$app.user_name} {$app.user_surname}{else}{$app.user_email}{/if}">
                                    <button type="submit" class="btn btn-mini">Akceptuj spotkanie</button>
                                </form>
                            {else}
                                Spotkanie potwierdzone
                            {/if}
                        </td>
                    </tr>
                {/foreach}
            {/if}
            </tbody>
        </table>
        </div>
    {/if}

{/block}

{block name=additionalJavaScript}
    <script>
        var oTable = $('.table.candidate2').dataTable({
            "bPaginate": false,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "oLanguage": {
                "oPaginate": {
                    "sFirst": "&laquo;",
                    "sLast": "&raquo;",
                    "sNext": "&rsaquo;",
                    "sPrevious": "&lsaquo;"
                },
                "sSearch": '<div class="input-append search-entries-input">' +
                '<input type="text" placeholder="np. imię, nazwisko..." autocomplete="off">' +
                '<span class="btn"><i class="icon-search"></i></span>' +
                '</div>',
                "sEmptyTable": 'Brak danych w tabeli',
                "sZeroRecords": 'Brak danych do wyświetlenia',
                "sInfoEmpty": '',
                "sInfo": '',
                "sInfoFiltered": '',
                "sLengthMenu": ''
            }
        });
    </script>
    <script type="text/javascript">
        var hostname = '{$smarty.const.HOST}';
        var groupsCount = {count($appGroups)};

        {literal}
        $(document).ready(function () {
            var $manageGroup = $('.manage-group');

            // toggling group panel
            $manageGroup.on('click', function () {
                var $popup = $(this).parent().find('div.manage-group-wrapper').find('div.manage-group-content');
                if ($popup.is(':not(visible)')) {
                    $('.custom-popup').hide();
                }
                $popup.toggle();

                return false;
            });

            $('body').click(function(e) {
                var $target = $(e.target || e.srcElement);
                if (!($target.parents('.custom-popup').length > 0 && $target.is(':not(a)'))) {
                    $('.custom-popup').hide();
                }
            });

            // filtering groups
            $('.manage-group-filter').on('keyup', function (e) {
                var $this = $(this);
                var $groupsList = $this.parents('div.manage-group-wrapper').find('.manage-group-content span:not(.embedded)');

                var filterFactory = function (filter) {
                    var _filter = filter;
                    return function (index, value) {
                        var $value = $(value);
                        ($value.text().toLowerCase().indexOf(_filter.toLowerCase()) === -1) ? $value.hide() : $value.show();
                    }
                };

                $.each($groupsList, filterFactory($this.val()));
            });

            var userGroupsSummary = function ($wrapperObj) {
                var $elements = $wrapperObj.find('span:not(.embedded) input[type="checkbox"]');
                var summary = {totalElements: $elements.length, checked: [], unchecked: []};

                $.each($elements, function (index, value) {
                    var $checkbox = $(value);
                    (Boolean($checkbox.attr('checked')))
                        ? summary.checked.push(parseInt($checkbox.val()))
                        : summary.unchecked.push(parseInt($checkbox.val()))
                        ;
                });

                return summary;
            };

            var markAllCheckboxesAs = function ($wrapperObj, checked) {
                var $checkboxes = $wrapperObj.find('span:not(.embedded) input[type="checkbox"]');
                (checked === true)
                    ? $checkboxes.each(function(index, element){ $(element).attr('checked', 'checked') })
                    : $checkboxes.each(function(index, element){ $(element).removeAttr('checked'); })
                ;
            };

            // toggling group subscription
            $('.manage-group-content span input[type="checkbox"]').on('change', function () {
                var $this = $(this);
                var $wrapper = $this.parents('div.manage-group-wrapper');

                var summary = userGroupsSummary($wrapper);

                if ($this.val() === 'all') {
                    markAllCheckboxesAs($wrapper, this.checked);
                    return true;
                }

                (summary.unchecked.length === 0)
                    ? $wrapper.find('.selectAllCheckbox').attr('checked', 'checked')
                    : $wrapper.find('.selectAllCheckbox').removeAttr('checked');
            });

            $('.assign-groups-to-user').click(function() {
                $('.custom-popup').hide();
                var $this = $(this);
                var $wrapper = $this.parents('div.manage-group-wrapper');
                var userId = $wrapper.data('user');

                var summary = userGroupsSummary($wrapper);

                if (summary.checked.length > 0) {
                    $.post(hostname + "moje-konto/grupy/dodaj-kandydata," + userId, {'groups': summary.checked}, function (response) {});
                }
                if (summary.unchecked.length > 0) {
                    $.post(hostname + "moje-konto/grupy/usun-kandydata," + userId, {'groups': summary.unchecked}, function (response) {});
                }

                return false;
            });
        });
        {/literal}
    </script>
{/block}