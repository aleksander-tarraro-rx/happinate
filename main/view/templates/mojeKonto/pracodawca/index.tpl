{extends file="index.tpl"}

{block name=prebody}
<div class="center">
    <nav class="login-nav">
        {if $_user->getUserType()==1}
        <ul>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>
        {elseif $_user->getUserType()==2}
        <ul>
            <li{if $actionName=='oneclick'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/oneclick">1-click</a></li>
            <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
            <li{if $actionName=='mojeGrupyAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-grupy">Moje grupy</a></li>
            <li{if $actionName=='moiKandydaciAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Ulubieni kandydaci</a></li>
            <li class="active"><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>            
        {/if}
    </nav>
</div>

    <div id="delete" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Czy na pewno chcesz zrezygnować z posiadania konta w serwisie happinate.com?</h3>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <a class="btn btn-danger" href="{$smarty.const.HOST}moje-konto/usun">Tak</a>
      </div>
    </div>

    <div id="inviteFriend" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Poleć nas innym pracodawcom</h3>
      </div>
     <div class="modal-body">
        <form action="" method="post" id="inviteFriendForm">
            <p><input type="text" name="friendEmail" placeholder="adres email..." style="width: 500px;" value="{if isset($smarty.post.friendEmail) && $smarty.post.friendEmail}{$smarty.post.friendEmail}{/if}" /></p>
        </form>
     </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <a href="#" onclick="$('#inviteFriendForm').submit();" class="btn btn-success">Wyślij zaproszenie</a>
      </div>
    </div>  
{/block}

{block name=body}

    <form class="main-form" method="post" action="" enctype="multipart/form-data" id="update">
        <input type="hidden" name="update-profile" value="" />
        <input type="hidden" name="changePass" value="0" />
        <div class="left-column">
            <h2>PODSTAWOWE DANE PROFILOWE<span style="width:386px;" class="underline-header-content"></span></h2>
            <div class="input-box left">
                <label>
                    <span class="input-box-label">Nazwa firmy  </span>{if isset($errorArray.profile_name) && $errorArray.profile_name}<span class="label-error"></span>{/if}
                    <input type="text" name="profile_name" placeholder="Wpisz nazwę profilu" value="{if isset($smarty.post.profile_name)}{$smarty.post.profile_name}{else}{$userProfile.user_profile_name}{/if}" autocomplete="off">
                </label>
            </div>
            <div class="input-box left">
                <label>
                    <span class="input-box-label">Adres email </span>{if isset($errorArray.profile_email) && $errorArray.profile_email}<span class="label-error"></span>{/if}
                    <input type="text" name="profile_email" placeholder="Wpisz adres email" value="{if isset($smarty.post.profile_email)}{$smarty.post.profile_email}{else}{$userProfile.user_email}{/if}" autocomplete="off">
                </label>
            </div>
            <div class="input-box left">
                <label class="input-box-half">
                    <span class="input-box-label">Miejscowość </span>
                    <input type="text" name="profile_city" placeholder="Miejscowość" value="{if isset($smarty.post.profile_city)}{$smarty.post.profile_city}{else}{$userProfile.user_adress_city}{/if}" autocomplete="off">
                </label>
                <label class="input-box-half right">
                    <span class="input-box-label">Kod pocztowy </span>{if isset($errorArray.profile_zip_code) && $errorArray.profile_zip_code}<span class="label-error"></span>{/if}
                    <input type="text" name="profile_zip_code" placeholder="00-000" value="{if isset($smarty.post.profile_zip_code)}{$smarty.post.profile_zip_code}{else}{$userProfile.user_adress_zip_code}{/if}" autocomplete="off">
                </label>
            </div>
            <div class="clear"></div>
            <div class="input-box left">
                <label>
                    <span class="input-box-label">Ulica i nr domu </span>
                    <input type="text" name="profile_street" placeholder="Wpisz ulice i nr domu" value="{if isset($smarty.post.profile_street)}{$smarty.post.profile_street}{else}{$userProfile.user_adress_street}{/if}" autocomplete="off">
                </label>
            </div>
            <div class="input-box left">
                <label class="input-box-half">
                    <span class="input-box-label">NIP</span>{if isset($errorArray.profile_nip) && $errorArray.profile_nip}<span class="label-error"></span>{/if}
                    <input type="text" name="profile_nip" placeholder="Wpisz NIP" value="{if isset($smarty.post.profile_nip)}{$smarty.post.profile_nip}{else}{$userProfile.user_nip}{/if}" autocomplete="off">
                </label>
                <label class="input-box-half right">
                    <span class="input-box-label">REGON</span>{if isset($errorArray.profile_regon) && $errorArray.profile_regon}<span class="label-error"></span>{/if}
                    <input type="text" name="profile_regon" placeholder="Wpisz REGON" value="{if isset($smarty.post.profile_regon)}{$smarty.post.profile_regon}{else}{$userProfile.user_regon}{/if}" autocomplete="off">
                </label>
            </div>
            <div class="clear"></div>
            <div class="input-box left">
                <label>
                    <span class="input-box-label">Telefon </span>
                    <input type="text" name="profile_phone" placeholder="Wpisz nr telefonu" value="{if isset($smarty.post.profile_phone)}{$smarty.post.profile_phone}{else}{$userProfile.user_phone}{/if}" autocomplete="off">
                </label>
            </div>
            <div class="input-box left">
                <label class="input-box-half">
                    <span class="input-box-label">Strona www </span>{if isset($errorArray.www) && $errorArray.www}<span class="label-error"></span>{/if}
                    <input type="text" name="www" placeholder="Wpisz adres www" value="{if isset($smarty.post.www)}{$smarty.post.www}{else}{$userProfile.user_www}{/if}" autocomplete="off">
                </label>
                <label class="input-box-half right">
                    <span class="input-box-label">Fanpage </span>{if isset($errorArray.fanpage) && $errorArray.fanpage}<span class="label-error"></span>{/if}
                    <input type="text" name="fanpage" placeholder="Wpisz adres fanpage" value="{if isset($smarty.post.fanpage)}{$smarty.post.fanpage}{else}{$userProfile.user_fanpage}{/if}" autocomplete="off">
                </label>
            </div>
        </div>
        <div class="right-column">
            <a href="javascript:;" data-toggle="modal" data-target="#inviteFriend" class="btn btn-normal  add-candidate no-border-radius" style="float: right; width: 216px; text-align: center;"><i class="icon-heart icon-white"></i> Poleć nas innym pracodawcom</a>

            <div class="clear"></div>
    
            {if $smarty.const.MOD_CALENDAR}
                {include file="mojeKonto/pracodawca/mojKalendarzBox.tpl"}
            {/if}

            <div class="file-input-box" style="margin-top:20px;">
                <h2>LOGO <span style="width:66px;" class="underline-header-content"></span></h2>{if isset($errorArray.file) && $errorArray.file}<span class="label-error"></span>{/if}
                <div class="file-box">
                    <div class="img-box-2">
                        <img id="imagePreview" />
                    </div>
                    {if $userProfile.user_logo}
                    <div class="img-box">
                        <img src="{$smarty.const.HOST}upload/userLogo/{$userProfile.user_logo}">
                        <input type="hidden" name="profile_file" value="{$userProfile.user_logo}" />
                    </div>
                    {/if}
                    <div class="action-file-box" {if !$userProfile.user_logo} style="margin-top: 0px;"{/if}>
                        <div class="btn-browser" {if !$userProfile.user_logo}style="width: 295px;"{/if}>
                            PRZEGLĄDAJ
                            <input type="file" id="uploadImage" name="file" accept="image/*" onchange="readURL(this);">
                        </div>
                        {if $userProfile.user_logo}
                            <input type="hidden" name="file_delete" id="file_delete" value="false" />
                            <span class="btn-erase tooltip-item" onClick="deleteFile();" title="Usuń"><i class="icon-trash icon-white"></i></span>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
        <div style="height:20px;" class="clear"></div>

        <div class="right-column">
            <a href="#" onClick="$('#update').submit();" class="happi-btn" style="float:right;margin-top:14px;padding:10px 10px 11px 15px;">ZAPISZ PODSTAWOWE DANE PROFILOWE<i class="icon-chevron-right icon-white"></i></a>
        </div>
        <div class="clear"></div>
    </form>
            
    <!-- Edycja profilu -->
    <form class="main-form" action="" method="post" enctype="multipart/form-data" id="profileEdit">
        <input type="hidden" name="profileEdit" value="1" />
        <h2 style="margin-top:30px;">DODATKOWE DANE PROFILOWE<span class="underline-header-content" style="width:367px;"></span></h2>
        <div class="left-column">

            <div class="input-box left">
                <span class="input-box-label">Dodaj informacje o firmie i zwiększ atrakcyjność profilu pracodawcy </span>
                <div class="textarea-input" style="margin-bottom: 10px;width: 600px; margin-top: 10px;">
                    <textarea class="ckeditor" placeholder="dddd" name="info" rows="5">{if isset($smarty.post.info)}{$smarty.post.info}{else}{$userProfile.user_info}{/if}</textarea>
                </div>
            </div>
                
            <div class="input-box left textarea-char-num">
                <span class="input-box-label">Dodaj tekst do zdjęcia profilowego </span>
                <div class="textarea-input" style="margin-bottom: 10px; width: 600px; margin-top: 10px;">
                    <textarea class="ckeditor2" placeholder="dddd" name="profile_img_info" onchange="" maxlength="120" rows="5">{if isset($smarty.post.profile_img_info)}{$smarty.post.profile_img_info}{else}{$userProfile.user_profile_img_info}{/if}</textarea>
                </div>
            </div>

        </div>
        <div class="right-column">
            <a href="{$smarty.const.HOST}profil" target="_blank" class="btn btn-normal" style="margin-top: 39px;"><i class="icon icon-white icon-eye-open"></i> zobacz jak wygląda Twój profil</a>
            <div class="file-input-box" style="margin-top:20px;">
                <span class="input-box-label">Zdjęcie profilowe </span>{if isset($errorArray.file2) && $errorArray.file2}<span class="label-error"></span>{/if}
                <div class="file-box">
                    <div class="img-box-22">
                        <img id="imagePreview2" />
                    </div>
                    {if $userProfile.user_profile_img}
                    <div class="img-box-222">
                        <img src="{$smarty.const.HOST}upload/userProfile/{$userProfile.user_profile_img}">
                        <input type="hidden" name="profile_file" value="{$userProfile.user_profile_img}" />
                    </div>
                    {/if}
                    <div class="action-file-box" {if !$userProfile.user_profile_img} style="margin-top: 0px;"{/if}>
                        <div class="btn-browser" {if !$userProfile.user_profile_img}style="width: 295px;"{/if}>
                            PRZEGLĄDAJ
                            <input type="file" id="uploadImage2" name="file" accept="image/*" onchange="readURL2(this);">
                        </div>
                        {if $userProfile.user_profile_img}
                            <input type="hidden" name="file_delete" id="file_delete2" value="false" />
                            <span class="btn-erase tooltip-item" onClick="deleteFile2();" title="Usuń"><i class="icon-trash icon-white"></i></span>
                        {/if}
                    </div>
                </div>
            </div>
            <div class="input-box right">
                <label>
                    <span class="input-box-label">Film z Youtube</span>{if (isset($errorArray.yt) && $errorArray.yt) || (isset($errorArray.yt) && $errorArray.yt)}<span class="label-error"></span>{/if}
                    <input type="text" name="yt" placeholder="Wpisz adres do filmu z Youtube" value="{if isset($smarty.post.yt)}{$smarty.post.yt}{else}{$userProfile.user_profile_yt_movie}{/if}" autocomplete="off">
                </label>
            </div>
                    
        </div>
        <div class="clear"></div>
        <div class="right-column">
            
            <button onClick="$('#profileEdit').submit();return(false);" class="happi-btn" style="float:right; margin-top:11px; padding:10px 10px 11px 15px;">ZAPISZ DODATKOWE DANE PROFILOWE<i class="icon-chevron-right icon-white"></i></button>
        </div>
        <div class="clear"></div>
    </form> 
            
    <!-- Zmiana hasła -->
    <form class="main-form" action="" method="post" id="changePassword">
        <h2 style="margin-top:30px;">ZMIEŃ HASŁO<span class="underline-header-content" style="width:160px;"></span></h2>
        <div class="input-box left">
            <input type="hidden" name="changePass" value="1" />
            <label class="input-box-half">
                <input type="password" name="password_old" placeholder="Stare hasło" value="" autocomplete="off">
            </label>
            <label class="input-box-half right">
                <input type="password" name="password_new" placeholder="Nowe hasło" value="" autocomplete="off">
            </label>
            <label class="input-box-half right">
                <input type="password" name="password_new2" placeholder="Powtórz nowe hasło" value="" autocomplete="off">
            </label>
        </div>
        <div class="clear"></div>
        <div class="left-column">
            <a href="#" data-toggle="modal" data-target="#delete"  onClick="//$('.main-form').submit();" class="happi-btn brown" style="float:left;margin-top:14px;padding:12px 10px 11px 10px;">USUŃ KONTO</a>
        </div>
        <div class="right-column">
            <button onClick="$('#changePassword').submit();return(false);" class="happi-btn" style="float:right; margin-top:11px; padding:10px 10px 11px 15px;">ZMIEŃ HASŁO <i class="icon-chevron-right icon-white"></i></button>
        </div>
        <div class="clear"></div>
    </form>  

    <div class="clear"></div>
{/block}

{block name=additionalJavaScript}
<script type="text/javascript">
    $('.ckeditor').ckeditor({
        wordcount: {
            showWordCount: true,
            showCharCount: true,
            countHTML: false,
            charLimit: '1000',
            wordLimit: 'unlimited' 
        }
    });
    $('.ckeditor2').ckeditor({
        wordcount: {
            showWordCount: true,
            showCharCount: true,
            countHTML: false,
            charLimit: '120',
            wordLimit: 'unlimited' 
        }
    });
</script>
{/block}