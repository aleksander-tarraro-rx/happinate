{extends file="index.tpl"}

{block name=prebody}
<div class="center">
    <nav class="login-nav">
        {if $_user->getUserType()==1}
        <ul>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>
        {elseif $_user->getUserType()==2}
        <ul>
            <li{if $actionName=='oneclick'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/oneclick">1-click</a></li>
            <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
            <li{if $actionName=='mojeGrupyAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-grupy">Moje grupy</a></li>
            <li{if $actionName=='moiKandydaciAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Ulubieni kandydaci</a></li>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>            
        {/if}
    </nav>
</div>
    <div id="deleteGroup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Czy na pewno usunąć te grupę?</h3>
        </div>
        <div class="modal-footer">
            <a href="" id="delLink" class="btn btn-danger">Usuń</a>
            <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>
    
    <div id="addGroup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Dodaj nową grupę</h3>
      </div>
     <div class="modal-body">
        <form action="" method="post" id="addGroupForm">
            <input type="hidden" name="addGroup" value="1" />
            <p><label><b>Nazwa grypy</b>
            <input type="text" name="name" placeholder="nazwa grupy..." style="width: 500px; margin-top: 5px; margin-bottom: 15px;" value="" /></label></p>
            <p><label><b>Opis grypy</b>
            <input type="text" name="opis" placeholder="opis grupy..." style="width: 500px; margin-top: 5px; margin-bottom: 5px;" value="" /></label></p>
        </form>
     </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <a href="javascript:;" onclick="$('#addGroupForm').submit();" class="btn btn-success">Dodaj</a>
      </div>
    </div>  
{/block}

{block name=body}

<div class="data-table">

    <h2 style="margin-bottom: 0px;">Grupy <strong>1-click</strong> (ulubieni)</h2>

    <a href="javascript:;" data-toggle="modal" data-target="#addGroup" class="btn btn-happinate btn-large add-candidate no-border-radius" style="float: left; font-size: 16px; margin-bottom: 10px; margin-right: 10px;"><i class="icon-plus icon-white"></i> Dodaj nową grupę</a>

    <div class="input-box">
        <input id="group-filter" type="text" style="width:775px;" placeholder="szukaj grupy">
    </div>

    <table class="table candidate" border="0" cellspacing="0" cellpadding="0">
            <thead>
                    <tr>
                        <th class="column-name">Nazwa grupy</th>
                        <th>Opis grupy</th>
                        <th class="column-phone">Ilość kandydatów</th>
                        <th class="column-action-small" style="width: 60px">Akcje</th>
                    </tr>
            </thead>
            <tbody>
                {if $groups}
                {foreach from=$groups item=group}
                    <tr>
                        <td class="column-name"><a href="{$smarty.const.HOST}moje-konto/moje-grupy/podglad,{$group.id}">{$group.name}</a></td>
                        <td>{$group.opis}</td>
                        <td class="column-phone">{$model->countOneclickInGroup($group.id)}</td>
                        <td class="column-action-small">
                            <a href="{$smarty.const.HOST}moje-konto/moje-grupy/podglad,{$group.id}" class="tooltip-item btn btn-success btn-mini" title="Podgląd"><i class="icon-eye-open icon-white"></i></a>
                            <a class="tooltip-item btn btn-danger btn-mini" onClick="$('#delLink').attr('href', '{$smarty.const.HOST}moje-konto/moje-grupy/usun,{$group.id}');" href="javascript:;" data-toggle="modal" data-target="#deleteGroup" data-original-title="Usuń grupę"><i class="icon-remove-sign  icon-white"></i></a>
                        </td>
                    </tr>
                {/foreach}
                {/if}
            </tbody>
    </table>
</div>
{/block}


{block name=additionalJavaScript}
    <script type="text/javascript">
        var $groupFilter = $('#group-filter');
        $groupFilter.on('keyup', function(e) {
            if (e.keyCode !== 8 && e.keyCode < 48 || e.keyCode > 90) {
                return ;
            }
            $.get("{$smarty.const.HOST}moje-konto/moje-grupy/filtr," + $groupFilter.val(), function (data) {
                var $wrapper = $('div.data-table');
                $wrapper.find('table').parent().remove();
                $wrapper.append(data);
                $wrapper.find('table').first().DataTable({
                    "bPaginate": false,
                    "bFilter": false,
                    "aaSorting": [[ 1, "desc" ]],
                    "oLanguage": {
                        "sEmptyTable": 'Brak danych w tabeli',
                        "sZeroRecords": 'Brak danych do wyświetlenia',
                        "sInfoEmpty": '',
                        "sInfo": '',
                        "sInfoFiltered": '',
                        "sLengthMenu":	''
                    }
                });
            });
        });
    </script>
{/block}