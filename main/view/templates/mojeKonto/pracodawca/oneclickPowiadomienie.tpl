{extends file="index.tpl"}

{block name=prebody}
<div class="center">
    <nav class="login-nav">
        {if $_user->getUserType()==1}
        <ul>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>
        {elseif $_user->getUserType()==2}
        <ul>
            <li{if $actionName=='oneclickAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/oneclick">1-click</a></li>
            <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
            <li{if $actionName=='mojeGrupyAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-grupy">Moje grupy</a></li>
            <li{if $actionName=='moiKandydaciAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Ulubieni kandydaci</a></li>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>            
        {/if}
    </nav>
</div>
{/block}

{block name=body}

    <div class="data-table">
        <h2 style="margin-bottom: 20px;">Oferta pracy typu <strong>1-click</strong></h2>

        {if !$oneclick}
            <div class="overflow" style="position: absolute; z-index: 20; width: 980px; height: 720px; background: #FFF; opacity: .6; filter: alpha(opacity=60);"></div>
            <div class="text" style="position: absolute; z-index: 21; border: 1px solid #39ADAD; background: #39ADAD; color:#FFF; padding: 10px; text-align: center; margin: 150px 200px;">
                <i class="icon icon-white icon-exclamation-sign"></i><br/><br/>
                <p>Pracodawco,  ofertę pracy typu 1-click możesz wysłać tylko do swoich ulubionych / rekomendowanych kandydatów. </p><br/>
                <p><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci" style="color:#FFF;">Dodaj kandydatów do ulubionych i wysyłaj do nich powiadomienia >></a></p>
            </div>
        {/if}
        <form action="" method="post">
            <input type="hidden" name="oneclick" value="1"/>
            
            <div class="row-fluid">
                    <div class="span3">Tytuł powiadomienia <font style="color: #cd0a0a; font-weight: bold;">*</font><br>(nazwa stanowiska)</div>
                    <div class="span7">
                        <div class="text-input input-char-num">
                            <input type="text" id="1" maxlength="120" value="{if isset($smarty.post.position)}{$smarty.post.position}{/if}" name="position">
                            <span class="char-num">Pozostało <span class="value">120</span> znaków</span>
                        </div>
                    </div>
            </div>
            
            <div class="row-fluid">
                <div class="span3">Ilość wakatów <font style="color: #cd0a0a; font-weight: bold;">*</font></div>
                <div class="span7">
                    <div class="text-input">
                        <input type="text" id="1" maxlength="120" value="{if isset($smarty.post.vacancies)}{$smarty.post.vacancies}{/if}" name="vacancies">
                    </div>
                </div>
            </div>
            
            <div class="row-fluid">
                    <div class="span3">Data rozpoczęcia pracy <font style="color: #cd0a0a; font-weight: bold;">*</font></div>
                    <div class="span7">
                        <div class="data-input">
                            <input type="text" value="{if isset($smarty.post.date)}{$smarty.post.date}{/if}" name="date">
                            <i class="icon-calendar-input"></i>
                        </div>
                    </div>
            </div>
            
            <div class="row-fluid">
                <div class="span3">Stawka <font style="color: #cd0a0a; font-weight: bold;">*</font></div>
                <div class="span7">
                        <div class="select-input stawka"{if isset($errorArray.paymant) && $errorArray.paymant} style="background:#EED3D7"{/if}>
                            <div class="select-label">
                                <p class="select-value">{if isset($smarty.post.paymant) && $smarty.post.paymant}{$smarty.post.paymant}{elseif isset($smarty.session.basket.paymant)}{$smarty.session.basket.paymant}{else}Wybierz stawkę{/if}</p>
                                <input type="hidden" name="paymant" value="{if isset($smarty.post.paymant)}{$smarty.post.paymant}{elseif isset($smarty.session.basket.paymant)}{$smarty.session.basket.paymant}{/if}">
                                <i class="icon-select-list"></i>
                            </div>
                            <ul>
                                <li>wolontariat, staż / praktyki bezpłatne</li>
                                <li>publikuj bez podawania stawki</li>
                                <li>stawka godzinowa</li>
                                <li>stawka miesięczna</li>
                            </ul>
                        </div>

                        <div class="feature-row stawka"{if isset($smarty.post.paymant) && $smarty.post.paymant=='stawka godzinowa'} style="display: block;"{elseif isset($smarty.session.basket.paymant)}{$smarty.session.basket.paymant=='stawka godzinowa'} style="display: block;"{/if}>
                            <div class="span12">
                                <div class="text-input small">
                                    <input type="text" name="rate" maxlength="120" value="{if isset($smarty.post.rate)}{$smarty.post.rate}{elseif isset($smarty.session.basket.rate)}{$smarty.session.basket.rate}{/if}" id="1"{if isset($errorArray.rate) && $errorArray.rate} style="background:#EED3D7"{/if}>
                                </div>
                            </div>

                            <div class="span12 text-right" style="margin-left: 0px;">
                                PLN na godzinę brutto
                            </div>

                            <div class="span12 text-right" style="margin-left: 0px;">
                                <hr>
                            </div>

                            <div class="span6 text-right" style="margin-left: 0px;">
                                <div class="text-input small">
                                    lub od <input type="text" name="rate_min" maxlength="120" value="{if isset($smarty.post.rate_min)}{$smarty.post.rate_min}{elseif isset($smarty.session.basket.rate_min)}{$smarty.session.basket.rate_min}{/if}" id="1"{if isset($errorArray.rate_min) && $errorArray.rate_min} style="background:#EED3D7"{/if}>
                                </div>
                            </div>
                            <div class="span6 text-right" style="margin-left: 0px;">
                                <div class="text-input small">
                                    do <input type="text" name="rate_max" maxlength="120" value="{if isset($smarty.post.rate_max)}{$smarty.post.rate_max}{elseif isset($smarty.session.basket.rate_max)}{$smarty.session.basket.rate_max}{/if}" id="1"{if isset($errorArray.rate_max) && $errorArray.rate_max} style="background:#EED3D7"{/if}>
                                </div>
                            </div>

                            <div class="span12 text-right" style="margin-left: 0px;">
                                PLN na godzinę brutto
                            </div>
                        </div>

                        <div class="feature-row miesieczna"{if isset($smarty.post.paymant) && $smarty.post.paymant=='stawka miesięczna'} style="display: block;"{elseif isset($smarty.session.basket.paymant) && $smarty.session.basket.paymant=='stawka miesięczna'} style="display: block;"{/if}>
                            <div class="span6">
                                <div class="text-input small">
                                    <input onkeyup="calculateMonthRate(this)" type="text" name="rateMonth" maxlength="120" value="{if isset($smarty.post.rateMonth)}{$smarty.post.rateMonth}{elseif isset($smarty.session.basket.rateMonth)}{$smarty.session.basket.rateMonth}{/if}" id="1"{if isset($errorArray.rateMonth) && $errorArray.rateMonth} style="background:#EED3D7"{/if}>
                                </div>
                            </div>
                            <div class="span6 text-right">
                                PLN brutto <br>(168 godzin miesięcznie)
                            </div>
                            <div class="span12 text-right" style="margin-left: 0;">
                                Jest to <span id="monthRate">0.00</span>zł na godzinę.
                            </div>
                        </div>
                        <div class="hand-tip tip-2"></div>
                </div>
            </div>

            <div class="row-fluid" style="margin-top: 10px;">
                <div class="span3" style="margin-top:0;"><label for="3">Szczegółowe informacje o ofercie pracy <br>(godzina rozpoczęcia, miejsca wykonywania pracy, obowiązki) <font style="color: #cd0a0a; font-weight: bold;">*</font></label></div>
                <div class="span7">
                    <div class="textarea-input">
                        <textarea id="3" name="oneclick_info"  style="width: 620px;" rows="3">{if isset($smarty.post.oneclick_info)}{$smarty.post.oneclick_info}{elseif isset($smarty.session.basket.oneclick_info)}{$smarty.session.basket.oneclick_info}{/if}</textarea>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span3">
                    Użytkownicy do powiadomienia <font style="color: #cd0a0a; font-weight: bold;">*</font>
                </div>
                <div class="span7">
                    <div class="select-input uzytkownicy"{if isset($errorArray.users) && $errorArray.users} style="background:#EED3D7;width:622px;" {else} style="width:622px;"{/if}>
                        <div class="select-label">
                            <p class="select-value">Wybierz kogo powiadomić o tym ogłoszeniu</p>
                            <i class="icon-select-list"></i>
                        </div>
                        <ul class="user-toggle">
                            <li data-target="user-list-favorite">Twoi ulubieni kandydaci</li>
                            <li data-target="user-list-job-type">Rodzaje przypisanych prac</li>
                            <li data-target="user-list-candidate-group">Twoje grupy kandydatów</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row-fluid user-list user-list-favorite">
                <div class="span3">
                    Wybierz do kogo chcesz wysłać ofertę pracy 1-click z listy ulubionych kandydatów
                </div>
                <div class="span7" style="margin-top: 10px;">
                    <div class="multiple-select-input"  style="width:631px;">
                        <select name="oneclick[]" data-placeholder="Wybierz ulubionych użytkowników" multiple="" class="chosen-select select-users-oneclick" style="width:620px;">
                            <option></option>
                            {if $oneclick}
                                {foreach from=$oneclick item=user}
                                    <option {if isset($smarty.post.oneclick) && in_array($user.user_id, $smarty.post.oneclick)} selected="selected"{elseif isset($smarty.session.basket.oneclick) && in_array($user.user_id, $smarty.session.basket.oneclick)} selected="selected"{/if} value="{$user.user_id}">{if $user.user_name}{$user.user_name} {/if}{if $user.user_surname}{$user.user_surname} {/if}{$user.user_email}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
            </div>
                        
            <div class="row-fluid user-list user-list-job-type">
                <div class="span3">
                    Wybierz rodzaje pracy do których przypisałeś swoich kandydatów i wyślij do nich ofertę pracy 1-click
                </div>
                <div class="span7" style="margin-top: 10px;">
                    <div class="multiple-select-input"  style="width:631px;">
                        <select name="tags[]" data-placeholder="Wybierz rodzaje pracy" multiple="" class="chosen-select select-users-tags" style="width:620px;">
                            <option></option>
                            {if $tags}
                                {foreach from=$tags item=tag}
                                    {assign var=count value=$model->countOneClickTags($tag.tag_id)}
                                    <option {if isset($smarty.post.tags) && in_array($tag.tag_id, $smarty.post.tags)} selected="selected"{/if} {if $count==0} disabled=""{/if} value="{$tag.tag_id}">{$tag.tag_name} ({$count})</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
            </div>              
                        
            <div class="row-fluid user-list user-list-candidate-group">
                <div class="span3">
                    Wybierz swoje grupy kandydatów i wyślij do nich ofertę pracy 1-click
                </div>
                <div class="span7" style="margin-top: 10px;">
                    {if $groups}
                    <div class="multiple-select-input"  style="width:631px;">
                        <select name="groups[]" data-placeholder="Wybierz grupy" multiple="" class="chosen-select select-users-groups" style="width:620px;">
                            <option></option>
                            
                                {foreach from=$groups item=group}
                                    {assign var=count value=$model->countOneclickInGroup($group.id)}
                                    <option {if isset($smarty.post.groups) && in_array($group.id, $smarty.post.groups)} selected="selected"{/if} {if $count==0} disabled=""{/if} value="{$group.id}">{$group.name} ({$count})</option>
                                {/foreach}
                            
                        </select>
                    </div>
                    {else}
                        Aby wybrać tę opcję musisz dodać grupy. <a target="_blank" href="/moje-konto/moje-grupy">Klinkij tutaj aby przejść do grup.</a> 
                    {/if}
                </div>
            </div>

            <div class="row-fluid">
                <div class="span3"></div>
                <div class="span7">
                    <span style="line-height:20px;">Powiadomienia wysłane zostaną do</span>
                    <ul id="users-summary"></ul>
                </div>
            </div>

            <a class="btn btn-large add-candidate btn-happinate no-border-radius" style="font-size: 16px; float: left;" href="{$smarty.const.HOST}moje-konto/oneclick"><< Powrót</a>
            
            <a class="btn btn-large add-candidate btn-happinate no-border-radius" style="font-size: 16px" onclick="$(this).parent().submit();" href="javascript:;">Wyślij powiadomienie 1-click >></a>

        </form>
        <div class="clear"></div>

    
    </div>

{/block}