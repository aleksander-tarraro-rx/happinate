{extends file="index.tpl"}

{block name=prebody}
<div class="center">
    <nav class="login-nav">
        {if $_user->getUserType()==1}
        <ul>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>
        {elseif $_user->getUserType()==2}
        <ul>
            <li{if $actionName=='oneclickAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/oneclick">1-click</a></li>
            <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
            <li{if $actionName=='mojeGrupyAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-grupy">Moje grupy</a></li>
            <li{if $actionName=='moiKandydaciAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Ulubieni kandydaci</a></li>
            <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
        </ul>            
        {/if}
    </nav>
</div>
    
    <div id="addOneClick" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Czy na pewno dodać tę osobę do listy ulubionych?</h3>
        </div>
        <div class="modal-footer">
            <a href="" id="addOneClickHref" class="btn btn-success">Tak</a>
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Nie</button>
        </div>
    </div>    
    
    <div id="removeOneClick" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Czy na pewno dodać tę osobę do listy ulubionych?</h3>
        </div>
        <div class="modal-footer">
            <a href="" id="removeOneClickHref" class="btn btn-success">Tak</a>
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Nie</button>
        </div>
    </div> 
    
    <div id="changeStatus" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Zmień aktualny status kandydata</h3>
            <p>UWAGA! Zmiana statusu jest możliwa tylko raz i zostanie wysłana również na adres emailowy osoby kandydującej.</p>
        </div>
        <div class="modal-footer">
            <a href="" id="statusAccepted" class="btn btn-success">Zaakceptuj kandydata</a>
            <a href="" id="declinedAccepted" class="btn btn-danger">Odrzuć kandydata</a>
            <button class="btn " data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>    
    
    <div id="oneClick" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Opcja 1-click!</h3>
            <p>Ten kandydat zostanie przeniesiony z listy rezerwowych na główną listę kandydatów 1-click.</p>
            <p><br></p>
            <p>UWAGA! Zmiana statusu jest możliwa tylko raz i zostanie wysłana również na adres emailowy osoby kandydującej.</p>
            
        </div>
        <div class="modal-footer">
            <a href="" id="oneClickUrl" class="btn btn-success"><i class="icon-ok-circle icon-white"></i> Zaakceptuj kandydata!</a>
            <button class="btn btn-warning" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>    
        
    <div id="odrzuc" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Odrzucanie kandydata!</h3>
            <p>Ten kandydat zostanie odrzucony z listy 1-click.</p>
            <p><br></p>
            <p>UWAGA! Zmiana statusu jest możliwa tylko raz i zostanie wysłana również na adres emailowy osoby kandydującej.</p>
            
        </div>
        <div class="modal-footer">
            <a href="" id="odrzucUrl" class="btn btn-danger"><i class="icon-remove-sign icon-white"></i> Odrzuć kandydata!</a>
            <button class="btn btn-warning" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>    
    
    <div id="zaakceptuj" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>UWAGA!</h3>
            <p>Zmiana statusu jest możliwa tylko raz i zostanie wysłana również i zostanie wysłane powiadomienie na adres emailowy osoby kandydującej.</p>
        </div>
        <div class="modal-footer">
            <a href="" id="zaakceptujUrl" class="btn btn-success"><i class="icon-ok-circle icon-white"></i> Zaakceptuj kandydata</a>
            <button class="btn btn-warning" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>
        
    
    <div id="odrzuc" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>UWAGA!</h3>
            <p>Zmiana statusu jest możliwa tylko raz i zostanie wysłana również i zostanie wysłane powiadomienie na adres emailowy osoby kandydującej.</p>
        </div>
        <div class="modal-footer">
            <a href="" id="odrzucUrl" class="btn btn-danger"><i class="icon-remove-sign icon-white"></i> Odrzuć kandydata!</a>
            <button class="btn btn-warning" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        </div>
    </div>
{/block}

{block name=body}
 <div class="data-table">
     
        <a href="{$smarty.const.HOST}moje-konto/oneclick" class="btn btn-normal no-border-radius"><< Wróc do listy powiadomień</a>
 
        <a target="_blank" style="margin: 0px 0px 0px 10px; font-size: 30px; display: block;" href="{$smarty.const.HOST}praca/{$position.job_id}/{$position.job_url_text}">{$position.job_position}</a>
        
        <h2>Kandydaci ({$model->getAcceptedOneClick($queryname, true)}/{$model->getVacancies($queryname, true)} wakatów)</h2>

        <table class="table candidate2" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th align="center" style="width: 30px;"></th>
                    <th class="column-name">Imię i nazwisko</th>
                    <th>Miejscowość</th>
                    <th class="column-phone">Telefon</th>
                    <th class="column-action-small" style="width: 130px;">Akcje</th>
                </tr>
            </thead>
            {if $appList}
            <tbody>
                {if $appList}
                {foreach from=$appList item=app}
                    <tr>
                        <td align="center" style="text-align:center;"><span style="display: none;">{if $app.status==5}3{elseif $app.status==4}4{elseif $app.status==3}2{elseif $app.status==2}1{elseif $app.status==1}0{else}3{/if}</span>{if $app.status==5}<span class="label label-warning tooltip-item" title="Kandydat czeka na akceptację"><i class="icon icon-white icon-thumbs-up"></i></span>{elseif $app.status==4}<span class="label label-important tooltip-item" title="Kandydat odrzucił tę ofertę pracy"><i class="icon icon-white icon-minus-sign"></i></span>{elseif $app.status==3}<span class="label label-important tooltip-item" title="Anuluj ofertę pracy dla tego kandydata"><i class="icon icon-white icon-remove"></i></span>{elseif $app.status==2}<span class="label label-warning tooltip-item" title="Kandydat rezerwowy - potwierdził gotowość do pracy, czeka na wolny wakat"><i class="icon icon-white icon-flag"></i></span>{elseif $app.status==1}<span class="label label-success tooltip-item" title="Zaakceptowany - kandydat potwierdził gotowość do pracy"><i class="icon icon-white icon-ok-sign"></i></span>{else}<span class="label label-info tooltip-item" title="Wysłano ofertę pracy - czeka na akceptację kandydata"><i class="icon icon-white icon-question-sign"></i></span>{/if}</td>
                        <td class="column-name"><a target="_blank" href="{$smarty.const.HOST}profil/k/{$app.user_id|md5}">{if !empty($app.user_name) && !empty($app.user_surname)}{$app.user_name} {$app.user_surname}{else}{$app.user_email}{/if}</a> {if $app.friend}(polecony przez <a target="_blank" href="{$smarty.const.HOST}profil/k/{$app.friend|md5}">{$model->getUserEmail($app.friend)}</a>){/if}</td>
                        <td>{$app.user_city}</td>
                        <td class="column-phone">{$app.user_phone}</td>
                        <td class="column-action-small" style="width: 70px;">
                            <a href="{$smarty.const.HOST}profil/k/{$app.user_id|md5}" class="tooltip-item btn btn-mini" title="Podgląd"><i class="icon-eye-open"></i></a>
                            {if $app.status!=3 && $app.status!=4}
                                <a onClick="$('#odrzucUrl').attr('href', '{$smarty.const.HOST}moje-konto/oneclick/odrzuc-kandydata,{$app.note},{$app.user_id}');" href="javascript:;" data-toggle="modal" data-target="#odrzuc" class="tooltip-item btn btn-mini btn-danger" title="Anuluj ofertę pracy dla tego kandydata"><i class="icon-remove-sign icon-white"></i></a>
                            {/if}
                            {if $app.status==2}
                                <a onClick="$('#oneClickUrl').attr('href','{$smarty.const.HOST}moje-konto/oneclick/akceptuj-kandydata,{$app.user_id},{$app.note}');" href="javascript:;" data-toggle="modal" data-target="#oneClick" class="tooltip-item btn btn-mini btn-success" title="Przenieś kandydata z listy rezerwowych na listę podstawową"><i class="icon-ok-sign icon-white"></i></a>
                            {/if}
                            {if $app.status==5}
                                <a onClick="$('#oneClickUrl').attr('href','{$smarty.const.HOST}moje-konto/oneclick/akceptuj-kandydata,{$app.user_id},{$app.note},1');" href="javascript:;" data-toggle="modal" data-target="#oneClick" class="tooltip-item btn btn-mini btn-success" title="Przenieś kandydata na listę podstawową"><i class="icon-ok-sign icon-white"></i></a>
                            {/if}
                        </td>
                    </tr>
                {/foreach}
                {/if}
            </tbody>
            {/if}
        </table>
        
        <div class="legend">
            <h3>Legenda:</h3>
            <p><span class="label label-info"><i class="icon icon-white icon-question-sign"></i></span> - kandydat otrzymał zaproszenie ale jeszcze nie potwierdził, że stawi się w pracy</p>
            <p><span class="label label-success"><i class="icon icon-white icon-ok-sign"></i></span> - kandydat jest gotowy do pracy</p>
            <p><span class="label label-warning"><i class="icon icon-white icon-flag"></i></span> - kandydat jest gotowy do pracy ale ze względu na niewystarczającą ilość wakatów został przeniesiony na listę rezerwowych</p>
            <p><span class="label label-warning"><i class="icon icon-white icon-thumbs-up"></i></span> - kandydat jest gotowy do pracy ale został zaproszony przez znajomego i wymaga Twojej akceptacji</p>
            <p><span class="label label-important"><i class="icon icon-white icon-remove"></i></span> - kandydat został odrzucony</p>
            <p><span class="label label-important"><i class="icon icon-white icon-minus-sign"></i></span> - kandydat odrzucił te stanowisko</p>
        </div>

{/block}

{block name=additionalJavaScript}
    <script>
    var oTable = $('.table.candidate2').dataTable({
            "bPaginate": false,
            "sPaginationType": "full_numbers",
            "aaSorting": [[ 0, "asc" ]],
            "oLanguage": {
                    "oPaginate": {
                            "sFirst": "&laquo;",
                            "sLast": "&raquo;",
                            "sNext": "&rsaquo;",
                            "sPrevious": "&lsaquo;",
                    },
                    "sSearch": '<div class="input-append search-entries-input">'+
                        '<input type="text" placeholder="np. imię, nazwisko..." autocomplete="off">'+
                        '<span class="btn"><i class="icon-search"></i></span>'+
                    '</div>',
                    "sEmptyTable": 'Brak danych w tabeli',
                    "sZeroRecords": 'Brak danych do wyświetlenia',
                    "sInfoEmpty": '',
                    "sInfo": '',
                    "sInfoFiltered": '',
                    "sLengthMenu":	'',
            }
    });
    </script>
{/block}