{extends file="index.tpl"}

{block name=prebody}
    <div class="center">
        <div class="powiadomienia">
        
        </div>
        <nav class="login-nav" style="float: right; width: 243px">
            <ul>
                <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
            </ul>            
        </nav>
        <div class="clear"></div>
    </div>
               
    <div id="inviteFriend" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Zaproś znajomych i pracujcie razem</h3>
      </div>
     <div class="modal-body">
        <form action="{$smarty.const.HOST}moje-konto/znajomi" method="post" id="inviteFriendForm">
            <input type="hidden" name="inviteFriend" value="1" />
            <p>Podaj adres email swojego znajomego!</p>
            <hr>
            <p><input type="text" name="friendEmail" placeholder="adres email..." style="width: 500px;" value="{if isset($smarty.post.friendEmail) && $smarty.post.friendEmail}{$smarty.post.friendEmail}{/if}" /></p>
        </form>
     </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <a href="#" onclick="$('#inviteFriendForm').submit();" class="btn btn-success">Wyślij zaproszenie</a>
      </div>
    </div>
                          
    <div id="zmienHaslo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Zmień hasło</h3>
        </div>
        <div class="modal-body">
            <form action="" method="post" class="main-form" >
                <input type="hidden" name="changePass" value="1" />
                <div class="row-fluid">
                    <div class="span4">
                        <label for="input1">Twoje stare hasło:</label>
                    </div>
                    <div class="span8">
                        <input id="input1" type="password" name="pass" value="" placeholder="" />
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <label for="input2">Nowe hasło:</label>
                    </div>
                    <div class="span8">
                        <input id="input2" type="password" name="npass" value="" placeholder="" />
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <label for="input3">Powtórz nowe hasło:</label>
                    </div>
                    <div class="span8">
                        <input id="input3" type="password" name="npass2" value="" placeholder="" />
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a href="javascript:;" onClick="$('#zmienHaslo form').submit();" class="btn btn-normal" data-dismiss="modal" aria-hidden="true">Zmień >></a>
            <button class="btn btn-normal" data-dismiss="modal" aria-hidden="true">Zamknij</button>
        </div>
    </div>
    
    <div id="usunKonto" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Usuń konto</h3>
        </div>
        <div class="modal-body">
            <form action="" method="post" class="main-form" >
                <input type="hidden" name="deleteUser" value="1" />
                <div class="row-fluid">
                    <div class="span4">
                        <label for="input1">Wpisz swoje hasło:</label>
                    </div>
                    <div class="span8">
                        <input id="input1" type="password" name="pass" value="" placeholder="" />
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a href="javascript:;" onClick="$('#usunKonto form').submit();" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Usuń konto >></a>
            <button class="btn btn-normal" data-dismiss="modal" aria-hidden="true">Zamknij</button>
        </div>
    </div>
            
    <div id="calendarDay" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"></h3>
        </div>
        <div class="modal-body">
            <p>Proszę czekać. Ładuje dane z kalendarza...</p>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Zamknij</button>
        </div>
    </div>     
                
                
    <div id="friendInvitations" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nowe zaproszenia od znajomych</h3>
        </div>
        <div class="modal-body">
            {if $friendInvitations}
            <table>
                {foreach from=$friendInvitations item=friend}
                <tr>
                    <td style="width: 450px;">{$friend.user_name} {$friend.user_surname} {$friend.user_email}</td>
                    <td>
                    <a href="{$smarty.const.HOST}moje-konto/znajomi/akceptacja,{$friend.id}" class="btn btn-success btn-mini" title="Przyjmij zaproszenie"><i class="icon-thumbs-up icon-white"></i></a>
                    <a href="{$smarty.const.HOST}moje-konto/znajomi/usun,{$friend.user_id}" class="btn btn-danger btn-mini" title="Odrzuć zaproszenie"><i class="icon-thumbs-down icon-white"></i></a>
                    </td>
                </tr>
                {/foreach}
            </table>
            {else}
                -- brak nowych powiadomień --
            {/if}
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Zamknij</button>
        </div>
    </div>
          
{/block}

{block name=body}
    <div class="new-candidate-profile">
        
        {* LEFT COLUMN *}
        
        <div class="left-column">
            <div class="box1">
                <div class="avatar">
                    {if $userProfile.user_logo}
                        <img src="{$smarty.const.HOST}upload/userLogo/{$userProfile.user_logo}" alt="Twoje zdjęcie profilowe!" />
                    {else} 
                        <img src="{$smarty.const.HOST}view/images/unknown_user.png" alt="Twoje zdjęcie profilowe!" />
                    {/if}
                </div>
                
                <div class="name">{if !empty($userProfile.user_name) || !empty($userProfile.user_surname)}{$userProfile.user_name} {$userProfile.user_surname}{else}<i>uzupełnij dane</i>{/if}</div>
                {if !empty($userProfile.user_adress_street) || !empty($userProfile.user_adress_city) || !empty($userProfile.user_adress_zip_code)}
                    <div class="adres">
                        {$userProfile.user_adress_street} {$userProfile.user_adress_city} {$userProfile.user_adress_zip_code}
                    </div>
                {/if}
                {if $userProfile.user_phone}<div class="email">{$userProfile.user_email}</div>{/if}
                {if $userProfile.user_phone}<div class="phone">{$userProfile.user_phone}</div>{/if}
                
                <div class="happirating">
                    
                    {*
					<div class="title">happirating</div>
                    <div class="stars tooltip-item" title="Jakiś tekst tłumaczący co to jest?"><div class="stars-complete" style="width: 30%;"></div></div>
                    *}
					
                    <a href="{$smarty.const.HOST}profil" class="btn btn-small btn-happinate" target="_blank" style="float: left;font-size: 12px; padding: 5px 7px 3px 11px; margin-top: 15px;"> ZOBACZ PROFIL <i class="icon icon-white icon-eye-open"></i></a>
                    <a href="{$smarty.const.HOST}moje-konto/edytuj" class="button" style="float: right;"> EDYTUJ PROFIL <i class="icon icon-white icon-pencil"></i></a>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="clear"></div>
            
            {*
            <div class="box2">
                <div class="title">Moja galeria zdjęć</div>
                <div class="galeria">
                    <img src="{$smarty.const.HOST}view/images/unknown_user.png" alt="Dodane: 13/02/2013" class="tooltip-item" title="Dodane: 13/02/2013" />
                    <img src="{$smarty.const.HOST}view/images/unknown_user.png" alt="Dodane: 13/02/2013" class="tooltip-item" title="Dodane: 13/02/2013" />
                    <img src="{$smarty.const.HOST}view/images/unknown_user.png" alt="Dodane: 13/02/2013" class="tooltip-item" title="Dodane: 13/02/2013" />
                    <div class="clear"></div>
                </div>

                <a href="#" class="button" style="float: right;"> EDYTUJ SWOJE ZDJĘCIA <i class="icon icon-white icon-camera"></i></a>

                <div class="clear"></div>

            </div>
            *}

            <div class="box3" style="height: 58px;">
                <a href="{$smarty.const.HOST}moje-konto/moje-cv" class="title-little"><span>MOJE CV</span> - DODAJ >></a>
            </div>
                
            <div class="box3" style="height: 58px;">
                <a href="javascript:;" data-toggle="modal" data-target="#zmienHaslo" class="title-little"><span>TWOJE HASŁO</span> - ZMIEŃ >></a>
            </div>
            
            <div class="box3" style="height: 58px;">
                <a href="javascript:;" data-toggle="modal" data-target="#usunKonto" class="title-little" style="background: #ce0225;">USUŃ KONTO >></a>
            </div>
                
            <div class="box3">
                <a href="{$smarty.const.HOST}moje-konto/edytuj" class="title"><span>Stwórz profesjonalne CV</span> w 3 krokach</a>
                <div class="text">
                    <b>1.</b> Podaj podstawowe dane<br/>
                    <b>2.</b> Uzupełnij swoje doświadczenie, edukację i umiejętności<br/>
                    <b>3.</b> Zapisz CV do pliku PDF<br/><br/>
                    Z nami szybko przygotujesz profesjonalne CV,
                    które wyróżni Twója kandydaturę.
                </div>
            </div>
            
        </div>
        
        {* MIDDLE COLUMN *}
        
        <div class="middle-column">
            
            <div class="box1">
                <form action="{$smarty.const.HOST}moje-konto?szukaj" method="post">
                    <input type="hidden" name="update-tags" value="1" />
                    <div class="title">Interesujące mnie rodzaje pracy</div>
                    <div class="text">
                        <label>KATEGORIA GŁÓWNA
                            <select data-placeholder="Wybierz główną kategorię" multiple="" name="main[]" class="chosen-select-one" style="width:410px;">
                                <option></option>
                                {if $tagList}
                                    {foreach from=$tagList item=tag}
                                        <option {if isset($userProfile.user_main_tag_id) && $userProfile.user_main_tag_id == $tag.tag_id} selected="selected"{/if} value="{$tag.tag_id}">{$tag.tag_name}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </label>
                        <small>wybierz jedną kategorię główną</small>
                        <label style="margin-top: 10px;">POZOSTAŁE
                            <select data-placeholder="Wybierz pozostałe kategorie" multiple="" name="tags[]" class="chosen-select" style="width:410px;">
                                <option></option>
                                {if $tagList}
                                    {foreach from=$tagList item=tag}
                                        <option {if isset($userTags) && is_array($userTags) && in_array($tag.tag_id, $userTags)} selected="selected"{/if} value="{$tag.tag_id}">{$tag.tag_name}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </label>
                        <small>wybierz przynajmniej 2 kategorie pozostałe</small>
                    </div>

                    <a href="javascript:;" onclick="$(this).parent().submit();" class="button szukaj-pracy" style="float: right; margin-top: 0;"> SZUKAJ PRACY <i class="icon icon-white icon-search"></i></a>

                    <div class="clear"></div>
                </form>
            </div>
            
            <div class="box2">
                <div class="title">
                    {if $calendarEvents && count($calendarEvents)}
                        <span class="badge badge-important calendarUnreadTrigger"> {count($calendarEvents)} </span>
                    {/if}
                    Oferty 1-click<a href="javascript:;"><span data=">> zwiń"> >> sprawdź </span></a>
                </div>
                <div class="one-click"></div>
                <div class="text watch">
                    {if $userOneclick}
                    <table>
                        {foreach from=$userOneclick item=oneclick}
                        <tr>
                            {if $oneclick.job_position}
                            <td><a href="{$smarty.const.HOST}praca/oneclick/{$oneclick.job_id}" target="_blank" class="name" style="width: 350px; display: block;">{$oneclick.job_position}</a></td>
                            {else}
                            <td><a href="{$smarty.const.HOST}moje-konto/oneclick/{$oneclick.job_id}" target="_blank" class="name" style="width: 350px; display: block;">{$oneclick.title}</a></td>
                            {/if}
                            <td>{$oneclick.job_start_date}</td>
                        </tr>
                        {/foreach}
                    </table>
                    {else}
                    <table>
                        <tr>
                            <td align="center">-- brak nowych ofert 1-click --</td>
                        </tr>
                    </table>
                    {/if}
                    <div style="text-align: center; padding-bottom: 10px;">
                        <a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia/oneclick" class="button"> ZOBACZ WSZYSTKIE <i class="icon icon-white icon-share-alt"></i></a>
                    </div>

                    <div class="clear"></div>
                </div>
            </div>
            <div class="box2">
                <div class="title">Sprawdź oferty pracy na dziś i na jutro <a href="javascript:;"><span data="<< zwiń">>> zobacz więcej</span></a></div>
                <div class="pracuj-teraz"></div>
                {if $workNowJobs}
                <div class="text">
                    <table>
                        {foreach from=$workNowJobs item=job}
                        <tr>
                            <td><div class="name"><a href="{$smarty.const.HOST}profil/p/{$job.job_added_by|md5}" target="_blank">{if $job.user_profile_name}{$job.user_profile_name}{else}{$job.user_email}{/if}</a></div></td>
                            <td><div class="desc"><a href="{$smarty.const.HOST}praca/{$job.job_id}/{$job.job_url_text}" target="_blank">{$job.job_position}</a></div></td>
                            {assign var=date value=" "|explode:$job.job_created_date}
                            <td class="date">{$date.1}<br/>{$date.0}</td>
                            <td><div class="city">{$job.job_employeer_place_direct_city}</div></td>
                        </tr>
                        {/foreach}
                    </table>

                    <div style="text-align: center; padding-bottom: 10px;">
                        <a href="{$smarty.const.HOST}oferty-pracy/1/" class="button"> ZOBACZ WSZYSTKIE <i class="icon icon-white icon-share-alt"></i></a>
                    </div>

                    <div class="clear"></div>
                </div>
                {else}
                <div class="text">
                    <table>
                        <tr>
                            <td align="center">-- brak ofert pracy na dziś i na jutro --</td>
                        </tr>
                    </table>

                    <div style="text-align: center; padding-bottom: 10px;">
                        <a href="{$smarty.const.HOST}oferty-pracy/1/" class="button"> ZOBACZ WSZYSTKIE <i class="icon icon-white icon-share-alt"></i></a>
                    </div>

                    <div class="clear"></div>
                </div>
                {/if}
            </div>
            
            
            {*   
            <div class="box2">
                <div class="title">Nowe oferty pracy dla moich tagów <a href="javascript:;"><span data="<< zwiń">>> sprawdź i aplikuj</span></a></div>
                <div class="text">
                    <table>
                        {for $foo=1 to 3}
                        <tr>
                            <td width="100"><div class="name"><a href="#" target="_blank">Dobry Wybór</a></div></td>
                            <td width="230"><div class="desc"><a href="#" target="_blank">Pozycjoner stron WWW</a></div></td>
                            <td class="date">12:32:43<br/>02/02/2013</td>
                            <td class="city">Poznań</td>
                        </tr>
                        {/for}
                    </table>

                    <div style="text-align: center; padding-bottom: 10px;">
                        <a href="{$smarty.const.HOST}oferty-pracy/1/{if $userTags && is_array($userTags)}{foreach from=$userTags item=tag name=tag}{($model->getTagName($tag)|lower)|replace:' ':'_'}{if !$smarty.foreach.tag.last},{/if}{/foreach}------{/if}" class="button"> ZOBACZ WSZYSTKIE <i class="icon icon-white icon-share-alt"></i></a>
                    </div>

                    <div class="clear"></div>
                </div>
            </div>
            *}
            
            <div class="box2">
                <div class="title url">Moje ogłoszenia <a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia"><span data="<< zwiń">>> zobacz wszystkie </span></a></div>
            </div>
            
            <div class="box2">
                <div class="title">Pracodawcy których obserwuję <a href="javascript:;"><span data="<< zwiń">>> sprawdź </span></a></div>
                <div class="text watch">
                    {if $userWatch}
                    <table>
                        {foreach from=$userWatch item=user}
                        <tr>
                            <td><div class="name" style="width: 350px;"><a href="{$smarty.const.HOST}profil/p/{$user.user_id|md5}" target="_blank">{if $user.user_profile_name}{$user.user_profile_name}{else}{$user.user_email}{/if}</a></div></td>
                            <td><a href="javascript:;" id="{$user.user_id}" class="usun-z-obserwowanych"><i class="icon-minus"></i> usuń</a></td>
                        </tr>
                        {/foreach}
                    </table>
                    {else}
                    <table>
                        <tr>
                            <td align="center">-- brak obserwowanych pracodawców --</td>
                        </tr>
                    </table>
                    {/if}
                    <div style="text-align: center; padding-bottom: 10px;">
                        <a class="button" href="/lista-pracodawcow"> ZOBACZ WSZYSTKICH PRACODACÓW <i class="icon icon-white icon-share-alt"></i></a>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
                
            <div class="box2">
                <div class="title">Pracodawcy którzy dodali mnie do ulubionych <a href="javascript:;"><span data="<< zwiń">>> sprawdź </span></a></div>
                <div class="text">
                    {if $userFav}
                    <table>
                        {foreach from=$userFav item=user}
                        <tr>
                            <td><div class="name" style="width: 450px;"><a href="{$smarty.const.HOST}profil/p/{$user.user_id|md5}" target="_blank">{if $user.user_profile_name}{$user.user_profile_name}{else}{$user.user_email}{/if}</a></div></td>
                        </tr>
                        {/foreach}
                    </table>
                    {else}
                    <table>
                        <tr>
                            <td align="center">-- brak pracodawców którzy dodali Cię do ulubionych --</td>
                        </tr>
                    </table>
                    {/if}
                </div>
            </div>
            
                        
        </div>
        
        {* RIGHT COLUMN *}
        
        <div class="right-column">
            <div class="box1">
                <div class="title">Twój kalendarz {if $calendarEvents}<a href="#calendarUnread" data-toggle="modal"><span class="badge badge-important">{$calendarEvents|count}</span></a>{/if}<div class="clear"></div></div>
                <div class="kalendarz">
                    <div class="loading"></div>
                </div>
            </div>
            
            <div class="box2">
                <div class="title">Znajomi {if $friendInvitations}<a href="#friendInvitations" data-toggle="modal"><span class="badge badge-important">{$friendInvitations|count}</span></a>{/if}</div>
                {if $userFriend}
                <div class="znajomi">
                    {foreach from=$userFriend item=friend}
                        <a target="_blank" href="{$smarty.const.HOST}profil/k/{$friend.user_id|md5}" class="imagebox tooltip-item" title="{if $friend.user_name || $friend.user_surname}{$friend.user_name} {$friend.user_surname}{else}{$friend.user_email}{/if}{if !$model->checkIfUserAcceptedFriendInvitation($friend.user_id)} (czeka na akceptację){/if}"><span></span>{if $friend.user_logo}<img src="{$smarty.const.HOST}upload/userLogo/{$friend.user_logo}" alt="" />{else}<img src="{$smarty.const.HOST}view/images/unknown_user.png" alt="" />{/if}</a>
                    {/foreach}
                    <div class="clear"></div>
                </div>
                {else}
                    <div class="znajomi">
                        nie masz jeszcze dodanych znajomych
                    </div>
                {/if}
                
                <a href="javascript:;" data-toggle="modal" data-target="#inviteFriend" class="button" style="float: left;"> DODAJ <i class="icon icon-white icon-plus"></i></a>
                
                <a href="{$smarty.const.HOST}moje-konto/znajomi" class="button" style="float: right;"> ZOBACZ ZNAJOMYCH <i class="icon icon-white icon-user"></i></a>

                <div class="clear"></div>

            </div>
            
             <div class="box3" style="text-align: left;">

                <span style="color:#c10101; font-size: 23px;">Pracuj już dziś <br/>
                i znajdż pracę na jutro!</span>
                 
                <div class="work-now" style="height: 140px;">

                    <label style="top: 20px;" class="input-checkbox">
                        <input type="checkbox" value="1" name="worknow"{if isset($smarty.post.worknow) && $smarty.post.worknow} checked{elseif $userProfile.user_worknow} checked{/if}>
                        <div class="checkbox-bg" style="left: 0px;">
                            <div style="margin: 4px 2px;" class="check-bg"></div>
                        </div>
                        <div style="color:#4f5252;font-size:12px;padding: 0px 30px; margin-top: -23px;" class="label-checkbox">Chciałbym otrzymywać powiadomienia o ofertach typu PRACUJ TERAZ.</div>
                    </label>

                    <label class="input-checkbox" style="top: 90px;">
                        <input type="checkbox" value="1" name="sms"{if isset($smarty.post.sms) && $smarty.post.sms} checked{elseif $userProfile.user_sms} checked{/if}>
                        <div class="checkbox-bg" style="left: 0px;">
                            <div style="margin: 4px 2px;" class="check-bg"></div>
                        </div>
                        <div style="color:#4f5252;font-size:12px;padding: 0px 30px;; margin-top: -23px;" class="label-checkbox">Bądź na bieżąco i otrzymuj powiadomienia SMS!</div>
                    </label>
                </div>
                        
                <div class="clear"></div>
            </div>
                
            <div class="box3">

                Używasz SmartPhona? Pobierz <br/>
                naszą aplikację <span>i szukaj pracy  <br/>
                gdziekolwiek jesteś.</span>
                
                <a class="android-btn" style="margin: 20px 0px 0px 25px" target="_blank" href="https://play.google.com/store/apps/details?id=com.moodup.happinate"></a>
                
                <div class="clear"></div>
            </div>
        </div>
        
        {* CLEAR *}
        
        <div class="clear"></div>
    </div>
{/block}

{block name=additionalJavaScript}
    <script>
    $(document).ready(function() {
        createCalendar('{$smarty.now|date_format:"%m-%Y"}');
    });
    
    function createCalendar(date) {
        $( ".kalendarz" ).prepend('<div class="loading"></div>');
        $.post("/moje-konto/createCalendar", { date: date }, function(data) {
            $( ".kalendarz" ).html( data );
        });
    }
    
    function createOneDayCalendar(date) {
        $( "#calendarDay #myModalLabel" ).html( '<p>Proszę czekać. Ładuje dane z kalendarza...</p>');
        $( "#calendarDay .modal-body" ).html( '' );
        $.post("/moje-konto/createOneDayCalendar", { date: date }, function(data) {
            var json = JSON.parse(data);
            $( "#calendarDay #myModalLabel" ).html( json[0] );
            $( "#calendarDay .modal-body" ).html( json[1] );
        });
    }
    </script>
{/block}