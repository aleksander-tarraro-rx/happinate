{extends file="index.tpl"}
{block name=prebody}
    {if $_user && !empty($_user) && $_user->getUserType()==1}
    <div id="inviteFriend" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel"><img  src="{$smarty.const.HOST}view/images/letsworktogether.png" width="200px" /></h3>
      </div>
     <div class="modal-body">
        <form action="" class="main-form"  method="post" id="inviteFriendForm">
            <input type="hidden" name="inviteFriend" value="1" />
            <input type="hidden" name="id" value="{$powiadomienie.id}" />
            <p>Zaproś znajomych i <strong>pracujcie razem!</strong></p>
            <hr>
            {assign var=friendList value=$_user->getFriendsList()}
            {if $friendList}    
                <div class="row">
                {foreach from=$friendList item=user}
                    <div class="span4"><label><input type="checkbox" name="friends[]" value="{$user.user_email}" /> {$user.user_email}</label></div>
                {/foreach}
                </div>
            {else}
                Niestety nie masz żadnych znajomych.
            {/if}
        </form>
     </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        {if $friendList}
            <a href="javascript:;" onclick="$('#inviteFriendForm').submit();" class="btn btn-success">Prześlij znajomym tą ofertę pracy 1-cick</a>
        {else}
            <a href="{$smarty.const.HOST}moje-konto/moi-znajomi " class="btn btn-success">Zaproś znajomych!</a>
        {/if}
      </div>
    </div>

    {/if}
{/block}
{block name=body}

    <div class="left-column">
        <h2 class="jobTitleH2">{$powiadomienie.title} </h2>
        
        <hr>
        
        <div class="notice-description">
            <h2>Pracodawca</h2>
            <p><a href="{$smarty.const.HOST}profil/p/{$powiadomienie.employeer|md5}"><button class="btn btn-large btn-happinate" style="font-size: 18px;">zobacz profil pracodawcy <i class="icon icon-white icon-eye-open"></i></button></a> </p> 
        </div>
        
        <hr>
        
        <div class="notice-description">
            <h2>Data rozpoczęcia pracy</h2>
            <p>{$powiadomienie.date}</p>
            <h2>Stawka godzinowa brutto</h2>
            <p>{$powiadomienie.rate_max}</p>
            <h2>Ilość wakatów</h2>
            <p>{$powiadomienie.vacancies}</p>     
            <h2>Wiadomość od pracodawcy</h2>
            <p>{$powiadomienie.note}</p>      
        </div>
    </div>

    <div class="right-column notice-more-column">
        <div style="text-align: center;">
            {if $powiadomienie.date > $smarty.now|date_format:"%Y-%m-%d"}
            <a data-toggle="modal" data-target="#inviteFriend" style="margin-top: 20px; margin-bottom: 20px; width: 50%; font-size: 13px;" class="btn btn-happinate" href="javascript:;"><i class="icon icon-white icon-user"></i> POLEĆ TĘ PRACĘ ZNAJOMEMU</a>
            
            {if $applied}
                <h3 style="margin-top: 20px; font-size: 15px;">
                {if $powiadomienie.status==3}
                    <span class="alert alert-danger" style="display: block;"><b>Oferta pracy została anulowana</b> <br><br>niestety pracodawca anulował tę ofertę pracy</span>
                {elseif $powiadomienie.status==2}
                    <span class="alert alert-warning" style="display: block;"><b>Lista rezerwowych</b> <br><br> po zaakceptowaniu 1-click'a znajdujesz się na liście rezerwowych<br>o zmiane statusu poinformujemy Cię mailowo</span>
                {elseif $powiadomienie.status==1}
                    <span class="alert alert-success" style="display: block;"><b>Przyjdź do pracy!</b> <br><br>gratulujemy! skontaktuj się z pracodawcą po dokładniejsze informacje</span>
                {elseif $powiadomienie.status==4}
                    <span class="alert alert-danger" style="display: block;"><b>Odrzuciłeś to zaproszenie</b></span>
                {else}
                    <span class="label label-info">Rozpatrywane <br><i style="font-weight: normal;"><a style="color: #FFF;" href="{$smarty.const.HOST}praca/oneclick/{$one.job_id}" target="_blank">przejdź do oferty i zaakceptuj zaproszenie</a></i></span>
                {/if}</h3>
                <a style="margin-top: 20px; width: 80%; font-size: 16px;" class="btn btn-happinate btn-large" href="{$smarty.const.HOST}">POWRÓT NA STRONĘ GŁÓWNĄ >></a>
            {else}
                {if $vacancies}
                    <a style="margin-top: 10px; width: 80%; font-size: 16px;" href="{$smarty.const.HOST}moje-konto/oneclick/{$powiadomienie.id},akceptuj"><img src="{$smarty.const.HOST}view/images/jaktodziala-oneclick-2.jpg" border="0" /></a>
                    <a style="margin-top: 10px; width: 80%; font-size: 16px;" class="btn btn-happinate btn-large" href="{$smarty.const.HOST}moje-konto/oneclick/{$powiadomienie.id},akceptuj">POTWIERDŹ, ŻE STAWISZ SIĘ W PRACY</a>
                    <a style="margin-top: 10px; font-size: 12px;" class="btn btn-danger" href="{$smarty.const.HOST}moje-konto/oneclick/{$powiadomienie.id},odrzuc">ODRZUĆ TO ZAPROSZENIE DO PRACY</a>
                {else}
                    <h3 style="margin-top: 20px; font-size: 15px;">Wszystkie wakaty zostały już zapełnione ale możesz zapisać się na listę rezerwowych dzięki czemu pracodawca będzie wiedział, że jesteś chętny z nim współpracować.</h3>
                    <a style="margin-top: 20px; width: 80%; font-size: 16px;" class="btn btn-happinate btn-large" href="{$smarty.const.HOST}moje-konto/oneclick/{$powiadomienie.id},akceptuj">ZAPISZ SIĘ JAKO REZERWOWY</a>
                    <a style="margin-top: 10px; width: 80%; font-size: 16px;" class="btn btn-happinate btn-large" href="{$smarty.const.HOST}">POWRÓT NA STRONĘ GŁÓWNĄ >></a>
                {/if}
            {/if}
            {else}
                <span class="alert alert-warning" style="display: block;"><b>Ogłoszenie wygasło</b></span>
                <a style="margin-top: 20px; width: 80%; font-size: 16px;" class="btn btn-happinate btn-large" href="{$smarty.const.HOST}">POWRÓT NA STRONĘ GŁÓWNĄ >></a>
            {/if}
        </div>
    </div>

    <div class="clear"></div>
{/block}
