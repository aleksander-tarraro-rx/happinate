{extends file="index.tpl"}

{block name=prebody}
    <div class="center">
        <div class="powiadomienia">
        
        </div>
        <nav class="login-nav" style="float: right; width: 243px">
            <ul>
                <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
            </ul>            
        </nav>
        <div class="clear"></div>
    </div>
{/block}

{block name=body}
    <h2>Ogłoszenia na które <strong>aplikowałaś/łeś</strong></h2>
    <hr style="margin-bottom: 60px;">
    <div class="data-table">
        <table class="table candidate-notice" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Nazwa stanowiska</th>
                    <th>Firma</th>
                    <th>Data aplikowania</th>
                    <th>Status</th>
            </tr>
            </thead>
            <tbody>
                {if $jobList}
                {foreach from=$jobList item=job}
                <tr>
                    <td><a href="{$smarty.const.HOST}praca/{$job.job_id}/{$job.job_url_text}" target="_blank">{$job.job_position}</a></td>
                    <td>{$job.job_employeer_name}</td>
                    <td>{$job.application_date}</td>
                    <td>
                        {if $job.application_status == 0}
                            Rozpatrywana
                        {elseif $job.application_status == 1}
                            Odrzucona
                        {elseif $job.application_status == 3}
                            1-click!
                        {else}
                            Przyjęta
                        {/if}
                    </td>
                </tr>
                {/foreach}
                {/if}
            </tbody>
        </table>
    </div>
            
            
    <h2>Twoje <strong>ulubione</strong> ogłoszenia</h2>
    <hr style="margin-bottom: 60px;">
    <div class="data-table">
        <table class="table candidate-notice" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Nazwa stanowiska</th>
                    <th>Firma</th>
                    <th>Koniec</th>
                    <th>Miejscowość</th>
            </tr>
            </thead>
            <tbody>
                {if $favJobList}
                {foreach from=$favJobList item=job}
                <tr>
                    <td><a href="{$smarty.const.HOST}praca/{$job.job_id}/{$job.job_url_text}" target="_blank">{$job.job_position}</a></td>
                    <td>{$job.job_employeer_name}</td>
                    <td>{$job.job_publish_date_end}</td>
                    <td>{$job.job_employeer_place_direct_city}<div id="{$job.job_id}" onClick="$(this).parent().parent().fadeOut('slow');" style="position: relative; float: right;" data-original-title="{if !$model->checkIfAddedToFav({$job.job_id})}Usuń z ulubionych{else}Dodaj do ulubionych{/if}"  class="fav tooltip-item {if !$model->checkIfAddedToFav({$job.job_id})} active{/if}"></div></td>   
                </tr>
                {/foreach}
                {/if}
            </tbody>
        </table>
    </div>
{/block}