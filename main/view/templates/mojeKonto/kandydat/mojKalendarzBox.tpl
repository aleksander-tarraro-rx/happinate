{if $userCalendar}
<h2 style="margin-top:30px;">MÓJ KALENDARZ<span class="underline-header-content" style="width:255px;"></span></h2>
<div class="input-box right">

	<table class="calendar-table">
		{foreach from=$userCalendar item=month key=k}
			<thead>
				<tr>
					<th colspan="7">
						<div class="header">{$k}</div>
					</th>
				</tr>
			</thed>
			<tbody>
				<tr>
					<td>Pn</td>
					<td>Wt</td>
					<td>Śr</td>
					<td>Cz</td>
					<td>Pi</td>
					<td>So</td>
					<td>Nd</td>
				</tr>				
				<tr>
				{assign var="i" value=1}
				{foreach from=$month item=day key=k}					
					<td class="row {$day.type}">
						{if !empty($day.info)}
							{foreach $day.info item=v1 key=k1}
								{assign var="info" value=$v1.time+" "+$v1.information}
							{/foreach}
							<span rel="tooltip" title="
							<ul>
							{foreach $day.info item=v1 key=k1}
								<li>{$v1.time} - {$v1.information}</li>
							{/foreach}
							</ul>
							">{$day.label}</span>
						{else}	
							<span>{$day.label}</span>
						{/if}
					</td>
				    {if $i%7==0 }
				    	</tr><tr>
				    {/if}
					{assign var="i" value=$i+1}
				{/foreach}
				</tr>								
			</tbody>
		{/foreach}                     
	</table>

	<div class="calendar-more">
		<a href="/moje-konto/moj-kalendarz">Kalendarz szczegółowy</a>
	</div>
</div>
{/if}