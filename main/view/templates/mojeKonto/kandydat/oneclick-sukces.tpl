{extends file="index.tpl"}

{block name=body}

    <div style="text-align: center;">
        <h2>Kandydacie gratulujemy! Twoja aplikacja została przyjęta. Jesteś wpisany na listę pracowników potrzebnych do pracy od zaraz!</h2>
        <a style="margin-top: 10px; width: 80%; font-size: 15px;" class="btn btn-happinate btn-large" href="{$smarty.const.HOST}">POWRÓT NA STRONĘ GŁÓWNĄ >> </a>
    </div>

    <div class="clear"></div>
{/block}