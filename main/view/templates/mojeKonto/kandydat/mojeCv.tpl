{extends file="index.tpl"}

{block name=prebody}
    <div class="center">
        <div class="powiadomienia">
        
        </div>
        <nav class="login-nav" style="float: right; width: 243px">
            <ul>
                <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
            </ul>            
        </nav>
        <div class="clear"></div>
    </div>
    
    <div id="addCV" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Dodaj CV</h3>
      </div>
      <div class="modal-body">
        <form id="addCVForm" action="{$smarty.const.HOST}moje-konto/moje-cv/nowe" enctype="multipart/form-data" method="post">
            <div class="avatar-input-box" style="background: #FFF; width: 500px; padding: 0px;">
                <div class="action-avatar-box" style="margin-top: 10px; width: 100%;">
                    <div class="btn-browser" style="width: 318px;">
                        WYBIERZ PLIK ZE SWOJEGO DYSKU <i class="icon-upload icon-white"></i>
                        <input type="file" accept="*" name="cv">
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <button class="btn btn-primary" onClick="$('#addCVForm').submit();">Dodaj CV</button>
      </div>
    </div>
        
    <div id="deleteCV" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Czy na pewno usunąć CV?</h3>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <a id="deleteCVHref" href="" class="btn btn-danger">Tak</a>
      </div>
    </div>
{/block}

{block name=body}
    <div class="data-table">
        <a href="javascript:;" data-toggle="modal" data-target="#addCV" style="font-size: 16px;" class="btn btn-large add-candidate btn-happinate no-border-radius">+ Dodaj nowe CV</a>

        <h2 style="margin-bottom: 10px;">Moje <strong>CV</strong></h2>

        <table class="table cv" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th class="column-name">Nazwa CV</th>
                    <th>Data zgłoszenia</th>
                    <th>Ogłoszenie</th>
                    <th style="width: 65px;" class="column-action">Akcje</th>
                </tr>
            </thead>
            <tbody>
                {if $cvList}
                    {foreach from=$cvList item=cv}
                        <tr>
                            <td><a href="{$smarty.const.HOST}upload/userCV/{$cv.cv_src}" target="_blank">{$cv.cv_name}</a>{if $cv.cv_public} <span style="font-size: 12px; color:#999;">[dodane ręcznie]</span>{/if}</td>
                            <td>{$cv.cv_date}</td>
                            <td>{if $cv.application_job_id}<a href="{$smarty.const.HOST}praca/{$cv.application_job_id}/" target="_blank">{$model->getJobName($cv.application_job_id)}</a>{else}---{/if}</td>
                            <td>                                
                                <a href="{$smarty.const.HOST}upload/userCV/{$cv.cv_src}" target="_blank" class="tooltip-item btn btn-mini" title="Podgląd"><i class="icon-eye-open"></i></a>
                                {if !$cv.application_job_id}<a class="tooltip-item btn btn-danger btn-mini" onClick="$('#deleteCVHref').attr('href', '{$smarty.const.HOST}moje-konto/moje-cv/usun,{$cv.cv_id}');" href="javascript:;" data-toggle="modal" data-target="#deleteCV" data-original-title="Usuń CV"><i class="icon-ban-circle icon-white"></i></a>{/if}
                            </td>
                        </tr>
                    {/foreach}
                {/if}
            </tbody>
        </table>
            
    </div>
    <div class="clear"></div>
{/block}