{extends file="index.tpl"}

{block name=prebody}
    <div class="center">
        <div class="powiadomienia">
        
        </div>
        <nav class="login-nav" style="float: right; width: 243px">
            <ul>
                <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
            </ul>            
        </nav>
        <div class="clear"></div>
    </div>

    <div id="delete" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Czy na pewno chcesz usunąć znajomego?</h3>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <a class="btn btn-danger" id="deleteHref" href="">Tak</a>
      </div>
    </div>
            
    <div id="inviteFriend" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Zaproś znajomych i pracujcie razem</h3>
      </div>
     <div class="modal-body">
        <form action="" method="post" id="inviteFriendForm">
            <input type="hidden" name="inviteFriend" value="1" />
            <p>Podaj adres email swojego znajomego!</p>
            <hr>
            <p><input type="text" name="friendEmail" placeholder="adres email..." style="width: 500px;" value="{if isset($smarty.post.friendEmail) && $smarty.post.friendEmail}{$smarty.post.friendEmail}{/if}" /></p>
        </form>
     </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <a href="#" onclick="$('#inviteFriendForm').submit();" class="btn btn-success">Wyślij zaproszenie</a>
      </div>
    </div>
{/block}

{block name=body}
    <a href="javascript:;" data-toggle="modal" data-target="#inviteFriend" style="float: right;"><img data-content="dsadsa" data-toggle="popover" data-original-title="dsd" data-placement="left" data-trigger="hover" style="float: right;" width="243px" src="{$smarty.const.HOST}view/images/letsworktogether.png" /></a>
    
    <div class="data-table">

        <h2 style="margin-bottom: 10px;">Moi <strong>znajomi</strong></h2>

        <table class="table friends" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th class="column-name">Adres email</th>
                    <th style="width: 65px;" class="column-action">Akcje</th>
                </tr>
            </thead>
            <tbody>
                {if $friends}
                    {foreach from=$friends item=friend}
                        <tr>
                            <td>{if !$friend.accepted}(oczekuje na akceptacje zaproszenia) {/if}<a href="{$smarty.const.HOST}profil/k/{$friend.user_id|md5}">{if $friend.user_name || $friend.user_surname}{if $friend.user_name}{$friend.user_name} {/if}{if $friend.user_surname}{$friend.user_surname} {/if}{else} <i>brak imienia i nazwiska </i>{/if}</a>(<a href="mailto:{$friend.user_email}">{$friend.user_email}</a>)</td>
                            <td>
                                {if $friend.accepted}
                                    <a href="javascript;:" data-toggle="modal" data-target="#delete" onclick="$('#deleteHref').attr('href', '{$smarty.const.HOST}moje-konto/znajomi/usun,{$friend.user_id}');" class="tooltip-item btn btn-danger btn-mini" title="Usuń znajomego"><i class="icon-trash icon-white"></i></a>
                                {else}
                                    <a href="{$smarty.const.HOST}moje-konto/znajomi/akceptacja,{$friend.id}" class="tooltip-item btn btn-success btn-mini" title="Przyjmij zaproszenie"><i class="icon-thumbs-up icon-white"></i></a>
                                    <a href="{$smarty.const.HOST}moje-konto/znajomi/usun,{$friend.user_id}" class="tooltip-item btn btn-danger btn-mini" title="Odrzuć zaproszenie"><i class="icon-thumbs-down icon-white"></i></a>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                {/if}
            </tbody>
        </table>
            
    </div>
    <div class="clear"></div>
{/block}