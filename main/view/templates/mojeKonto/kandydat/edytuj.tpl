{extends file="index.tpl"}

{block name=prebody}
    <div class="center">
        <div class="powiadomienia">
        
        </div>
        <nav class="login-nav" style="float: right; width: 243px">
            <ul>
                <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
            </ul>            
        </nav>
        <div class="clear"></div>
    </div>
                 
    <div id="usun" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3></h3>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn btn-danger">Tak</a>
            <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Nie</button>
        </div>
    </div>
          
{/block}

{block name=body}
    <div class="new-candidate-profile-edit">
        
        {* LEFT COLUMN *}
        
        <div class="left-column">
            
            <div class="krok">
                <b>KROK 1:</b> Uzupełnij podstawowe dane - zwiększ szanse na kontakt z <span>Pracodawcą</span>
            </div>
            
            <form action="" class="main-form" method="post" enctype="multipart/form-data">
                
                <input type="hidden" value="0" name="trash-file" id="trash-file" />
                <input type="hidden" value="1" name="edit-basic" />
                <input type="hidden" value="0" name="worknow" />
                <input type="hidden" value="0" name="sms" />
                
                <div class="avatar">
                    
                    <div class="img-box-2">
                        <img id="imagePreview">
                    </div>
                    
                    <div class="img-box">
                        {if $userProfile.user_logo}
                            <img id="imagePreview" src="{$smarty.const.HOST}upload/userLogo/{$userProfile.user_logo}">
                        {else}
                            <img id="imagePreview" src="{$smarty.const.HOST}view/images/unknown_user.png">
                        {/if}
                    </div>

                    <div class="action-avatar-box">
                        <div class="btn-browser">
                            {if $userProfile.user_logo}ZMIEŃ ZDJĘCIE{else}DODAJ ZDJĘCIE{/if} <i class="icon-camera icon-white" style="margin:3px 3px 0px 0px;"></i>
                            <input type="file" name="photo" accept="image/*" onchange="readURL(this);">
                        </div>
                        <span class="btn-erase" onclick="deleteFile();$('#trash-file').val(1);"><i class="icon-trash icon-white" style="margin:3px 3px 0px 0px;"></i></span>
                    </div>
                    
                </div>

                <div class="form">
                    <div class="row-fluid">
                        <div class="span4">
                            <label for="input1">Imię:</label>
                        </div>
                        <div class="span8">
                            <input id="input1" type="text" name="name" value="{if isset($smarty.post.name) && $smarty.post.name}{$smarty.post.name}{else}{$userProfile.user_name}{/if}" placeholder="" />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">
                            <label for="input2">Nazwisko:</label>
                        </div>
                        <div class="span8">
                            <input id="input2" type="text" name="surname" value="{if isset($smarty.post.surname) && $smarty.post.surname}{$smarty.post.surname}{else}{$userProfile.user_surname}{/if}" placeholder="" />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">
                            <label for="input3">Miasto:</label>
                        </div>
                        <div class="span8">
                            <input id="input3" type="text" name="city" value="{if isset($smarty.post.city) && $smarty.post.city}{$smarty.post.city}{else}{$userProfile.user_adress_city}{/if}" placeholder="" />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">
                            <label for="input4">Ulica:</label>
                        </div>
                        <div class="span8">
                            <input id="input4" type="text" name="street" value="{if isset($smarty.post.street) && $smarty.post.street}{$smarty.post.street}{else}{$userProfile.user_adress_street}{/if}" placeholder="" />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">
                            <label for="input5">Kod pocztowy:</label>
                        </div>
                        <div class="span8">
                            <input id="input5" type="text" maxlength="10" name="postal" value="{if isset($smarty.post.postal) && $smarty.post.postal}{$smarty.post.postal}{else}{$userProfile.user_adress_zip_code}{/if}" placeholder="" />
                        </div>
                    </div>
                        
                    <div class="row-fluid">
                        <div class="span4">
                            <label for="input6">Telefon:</label>
                        </div>
                        <div class="span8">
                            <input id="input6" type="text" maxlength="30" name="phone" value="{if isset($smarty.post.phone) && $smarty.post.phone}{$smarty.post.phone}{else}{$userProfile.user_phone}{/if}" placeholder="" />
                        </div>
                    </div>         
                        
                    <div class="row-fluid">
                        <div class="span4">
                            <label for="input7">Data urodzenia:</label>
                        </div>
                        <div class="span8  data-input-2">
                            <input id="input7" type="text" maxlength="30" name="birth" value="{if isset($smarty.post.birth) && $smarty.post.birth}{$smarty.post.birth}{else}{$userProfile.user_date_of_birth}{/if}" placeholder="" />
                        </div>
                    </div>

                </div>
                        
                <div class="clear"></div>

                <a href="javascript:;" onclick="$(this).parent().submit();" class="button" style="float: right;"> ZAPISZ SWOJE DANE <i class="icon icon-white icon-ok"></i></a>

                <div class="clear"></div>

            </form>
                    
            <div class="clear"></div>
            
            <div class="krok" style="margin-top: 10px">
                <b>KROK 2:</b> Uzupełnij swój <span>profil</span>, kiedy tylko masz czas
            </div>
            
            {* DOŚWIADCZENIE *}
            
            <div class="doswiadczenie">
                
                <a name="doswiadczenie"></a>

                <div class="header" id="0" style="{if isset($userBgCVColors.0)}background-color: {$userBgCVColors.0};{/if}{if isset($userCVColors.0)}color: {$userCVColors.0};{/if}">
                    DOŚWIADCZENIE
                    <div class="rozwin">rozwiń >></div>
                    <div class="zmien-kolor">
                        ZMIEŃ KOLORY <i class="icon icon-white icon-adjust"></i>
                        <div class="box">
                            <p>WYBIERZ KOLOR PASKA:</p>
                            <div class="color checked" style="background-color: #4f5252;"></div>
                            <div class="color" style="background-color: #FFF;"></div>
                            <div class="color" style="background-color: #f16437;"></div>
                            <div class="color" style="background-color: #c5c5c5;"></div>
                            <div class="color" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                            <p>WYBIERZ KOLOR CZCIONKI:</p>
                            <div class="fcolor" style="background-color: #4f5252;"></div>
                            <div class="fcolor checked" style="background-color: #FFF;"></div>
                            <div class="fcolor" style="background-color: #f16437;"></div>
                            <div class="fcolor" style="background-color: #c5c5c5;"></div>
                            <div class="fcolor" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                    
                <div class="box-do-rozwijania">
                    {if $userExperience}
                    {foreach from=$userExperience item=exp}
                    <div class="item">
                        <div class="info"{if isset($smarty.post.doswiadczenie) && isset($smarty.post.id) && $smarty.post.id=={$exp.experience_id}} style="display: none;"{/if}>
                            <div class="row-fluid">
                                <div class="span2 date">
                                    <small>od</small> {$exp.experience_start}<br/>
                                    <small>do</small> {if $exp.experience_till_now}teraz{else}{$exp.experience_end}{/if}
                                </div>
                                <div class="span4 pozycja">{$exp.experience_position}</div>
                                <div class="span4 firma">{$exp.experience_company}</div>
                            </div>
                            <div class="row-fluid opis">
                                <div class="span2"></div>
                                <div class="span10">{htmlspecialchars_decode($exp.experience_opis)}</div>
                            </div>
                            <div class="buttons">
                                <a href="#usun" id="1-{$exp.experience_id}" data-toggle="modal" class="button usun" style="float: right;"> USUŃ <i class="icon icon-white icon-trash"></i></a>
                                <a href="javascript:;" class="button edit" style="float: right;"> EDYTUJ <i class="icon icon-white icon-edit"></i></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="edytuj-doswiadczenie"{if isset($smarty.post.doswiadczenie) && isset($smarty.post.id) && $smarty.post.id=={$exp.experience_id}} style="display: block;"{/if}>
                            <form action="" method="post">
                                <input type="hidden" name="doswiadczenie" value="1" />
                                <input type="hidden" name="teraz" value="0" />
                                <input type="hidden" name="id" value="{$exp.experience_id}" />
                                <div class="row-fluid">
                                    <div class="span2">
                                        Od:
                                    </div>
                                    <div class="span3 data-input-3">
                                        <input type="text" name="od" value="{if isset($smarty.post.od) && $smarty.post.od}{$smarty.post.od}{else}{$exp.experience_start}{/if}" placeholder="" />
                                    </div>
                                    <div class="span1">
                                        Do:
                                    </div>
                                    <div class="span3 data-input-3">
                                        <input type="text" name="do" value="{if isset($smarty.post.do) && $smarty.post.do}{$smarty.post.do}{else}{$exp.experience_end}{/if}" placeholder="" />
                                    </div>
                                    <div class="span3">
                                       <label class="input-checkbox" style="margin: 0px 0px 5px 0px;">
                                           <div class="label-checkbox" style="margin: 0px 5px 5px 0px; padding: 0px; color: #A5A5A5">Do teraz:</div>
                                           <input type="checkbox" value="1" style="display: none;" name="teraz"{if isset($smarty.post.teraz) && $smarty.post.teraz} checked=""{elseif $exp.experience_till_now} checked=""{/if}>
                                           <div class="checkbox-bg" style="margin: 0px 0px 10px 0px;">
                                               <div class="check-bg"></div>
                                           </div>
                                       </label>
                                   </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span2">
                                        Stanowisko:
                                    </div>
                                    <div class="span8">
                                        <input style="width: 484px;" type="text" name="stanowisko" value="{if isset($smarty.post.stanowisko) && $smarty.post.stanowisko}{$smarty.post.stanowisko}{else}{$exp.experience_position}{/if}" placeholder="" />
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span2">
                                        Firma:
                                    </div>
                                    <div class="span8">
                                        <input style="width: 484px;" type="text" name="firma" value="{if isset($smarty.post.firma) && $smarty.post.firma}{$smarty.post.firma}{else}{$exp.experience_company}{/if}" placeholder="" />
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span2">
                                        Opis stanowiska:
                                    </div>
                                    <div class="span8">
                                        <textarea name="opis" class="editor" value="" placeholder="">{if isset($smarty.post.opis) && $smarty.post.opis}{$smarty.post.opis}{else}{htmlspecialchars_decode($exp.experience_opis)}{/if}</textarea>
                                    </div>
                                </div>
                                <a href="javascript:;" class="button anuluj" style="float: right; margin-left: 10px; margin-top: 10px;"> ANULUJ <i class="icon icon-white icon-warning-sign"></i></a>
                                <a href="javascript:;" onclick="$(this).parent().submit();" class="button sukces" style="float: right; margin-top: 10px;"> ZAPISZ <i class="icon icon-white icon-ok"></i></a>
                            </form>
                            <div class="clear"></div>
                        </div>
                    </div>
                    {/foreach}
                    {/if}

                    <div class="dodaj"{if isset($smarty.post.doswiadczenie) && !isset($smarty.post.id)} style="display: none;"{/if}>Dodaj{if $userExperience} kolejne{/if} doświadczenie <i class="icon icon-white icon-add-item"></i></div>

                    <div class="dodaj-doswiadczenie"{if isset($smarty.post.doswiadczenie) && !isset($smarty.post.id)} style="display: block;"{/if}>

                        <form action="" method="post">
                            <input type="hidden" name="teraz" value="0" />
                            <input type="hidden" name="doswiadczenie" value="1" />
                            <div class="row-fluid">
                                <div class="span2">
                                    Od:{if isset($smarty.post.doswiadczenie) && isset($errorArray.od) && $errorArray.od} <span class="label-error"></span>{/if}
                                </div>
                                <div class="span3 data-input-3">
                                    <input type="text" name="od" value="{if isset($smarty.post.doswiadczenie) && isset($smarty.post.od) && !isset($smarty.post.id)}{$smarty.post.od}{/if}" placeholder="" />
                                </div>
                                <div class="span1">
                                    Do:{if isset($smarty.post.doswiadczenie) && isset($errorArray.do) && $errorArray.do} <span class="label-error"></span>{/if}
                                </div>
                                <div class="span3 data-input-3">
                                    <input type="text" name="do" value="{if isset($smarty.post.doswiadczenie) && isset($smarty.post.do) && !isset($smarty.post.id)}{$smarty.post.do}{/if}" placeholder="" />
                                </div>
                                <div class="span3">
                                    <label class="input-checkbox" style="margin: 0px 0px 5px 0px;">
                                        <div class="label-checkbox" style="margin: 0px 5px 5px 0px; padding: 0px; color: #A5A5A5">Do teraz:</div>
                                        <input type="checkbox" value="1" style="display: none;" name="teraz"{if isset($smarty.post.doswiadczenie) && isset($smarty.post.teraz) && $smarty.post.teraz} checked=""{/if}>
                                        <div class="checkbox-bg" style="margin: 0px 0px 10px 0px;">
                                            <div class="check-bg"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    Stanowisko: {if isset($errorArray.stanowisko) && $errorArray.stanowisko}<span class="label-error"></span>{/if}
                                </div>
                                <div class="span8">
                                    <input style="width: 484px;" maxlength="50" type="text" name="stanowisko" value="{if isset($smarty.post.stanowisko) && !isset($smarty.post.id)}{$smarty.post.stanowisko}{/if}" placeholder="" />
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    Firma: {if isset($errorArray.firma) && $errorArray.firma}<span class="label-error"></span>{/if}
                                </div>
                                <div class="span8">
                                    <input style="width: 484px;" maxlength="50"  type="text" name="firma" value="{if isset($smarty.post.firma) && !isset($smarty.post.id)}{$smarty.post.firma}{/if}" placeholder="" />
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    Opis stanowiska: {if isset($errorArray.opis) && $errorArray.opis}<span class="label-error"></span>{/if}
                                </div>
                                <div class="span8">
                                    <textarea name="opis" class="editor">{if isset($smarty.post.opis) && !isset($smarty.post.id)}{$smarty.post.opis}{/if}</textarea>
                                </div>
                            </div>
                            <a href="javascript:;" class="button anuluj" style="float: right; margin-left: 10px; margin-top: 10px;"> ANULUJ <i class="icon icon-white icon-warning-sign"></i></a>
                            <a href="javascript:;" onclick="$(this).parent().submit();" class="button sukces" style="float: right; margin-top: 10px;"> DODAJ <i class="icon icon-white icon-ok"></i></a>
                        </form>

                        <div class="clear"></div>

                    </div>
                                    
                    <div class="zwin"><< zwiń</div>
                    
                    <div class="clear"></div>
                </div>
            </div>
                
            {* WYKRZTAŁCENIE *}
                
            <div class="doswiadczenie">
                
                <a name="wyksztalcenie"></a>

                <div class="header" id="1" style="{if isset($userBgCVColors.1)}background-color: {$userBgCVColors.1};{/if}{if isset($userCVColors.1)}color: {$userCVColors.1};{/if}">
                    WYKSZTAŁCENIE
                    <div class="rozwin">rozwiń >></div>
                    <div class="zmien-kolor">
                        ZMIEŃ KOLORY <i class="icon icon-white icon-adjust"></i>
                        <div class="box">
                            <p>WYBIERZ KOLOR PASKA:</p>
                            <div class="color checked" style="background-color: #4f5252;"></div>
                            <div class="color" style="background-color: #FFF;"></div>
                            <div class="color" style="background-color: #f16437;"></div>
                            <div class="color" style="background-color: #c5c5c5;"></div>
                            <div class="color" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                            <p>WYBIERZ KOLOR CZCIONKI:</p>
                            <div class="fcolor" style="background-color: #4f5252;"></div>
                            <div class="fcolor checked" style="background-color: #FFF;"></div>
                            <div class="fcolor" style="background-color: #f16437;"></div>
                            <div class="fcolor" style="background-color: #c5c5c5;"></div>
                            <div class="fcolor" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="box-do-rozwijania">
                    {if $userEducation}
                    {foreach from=$userEducation item=edu}
                    <div class="item">
                        <div class="info"{if isset($smarty.post.wyksztalcenie) && isset($smarty.post.id) && $smarty.post.id=={$edu.education_id}} style="display: none;"{/if}>
                            <div class="row-fluid">
                                <div class="span2 date">
                                    <small>od</small> {$edu.education_start}
                                </div>
                                <div class="span2 date">
                                    <small>do</small> {if $edu.education_till_now}teraz{else}{$edu.education_end}{/if}
                                </div>
                                <div class="span4 pozycja">{$edu.education_name}</div>
                                <div class="span4">{$edu.education_field}</div>
                            </div>
                            <div class="buttons">
                                <a href="#usun" id="2-{$edu.education_id}" data-toggle="modal" class="button usun" style="float: right;"> USUŃ <i class="icon icon-white icon-trash"></i></a>
                                <a href="javascript:;" class="button edit" style="float: right;"> EDYTUJ <i class="icon icon-white icon-edit"></i></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="edytuj-doswiadczenie"{if isset($smarty.post.wyksztalcenie) && isset($smarty.post.id) && $smarty.post.id=={$edu.education_id}} style="display: block;"{/if}>
                            <form action="" method="post">
                                <input type="hidden" name="wyksztalcenie" value="1" />
                                <input type="hidden" name="teraz" value="0" />
                                <input type="hidden" name="id" value="{$edu.education_id}" />
                                <div class="row-fluid">
                                    <div class="span2">
                                        Od:
                                    </div>
                                    <div class="span3 data-input-3">
                                        <input type="text" name="od" value="{if isset($smarty.post.wyksztalcenie) && isset($smarty.post.od) && $smarty.post.od}{$smarty.post.od}{else}{$edu.education_start}{/if}" placeholder="" />
                                    </div>
                                    <div class="span1">
                                        Do:
                                    </div>
                                    <div class="span3 data-input-3">
                                        <input type="text" name="do" value="{if isset($smarty.post.wyksztalcenie) && isset($smarty.post.do) && $smarty.post.do}{$smarty.post.do}{else}{$edu.education_end}{/if}" placeholder="" />
                                    </div>
                                    <div class="span3">
                                        <label class="input-checkbox" style="margin: 0px 0px 5px 0px;">
                                            <div class="label-checkbox" style="margin: 0px 5px 5px 0px; padding: 0px; color: #A5A5A5">Do teraz:</div>
                                            <input type="checkbox" value="1" style="display: none;" name="teraz"{if isset($smarty.post.wyksztalcenie) && isset($smarty.post.teraz) && $smarty.post.teraz} checked=""{elseif $edu.education_till_now} checked=""{/if}>
                                            <div class="checkbox-bg" style="margin: 0px 0px 10px 0px;">
                                                <div class="check-bg"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span2">
                                        Szkoła:
                                    </div>
                                    <div class="span8">
                                        <input style="width: 484px;" type="text" name="szkola" value="{if isset($smarty.post.szkola) && $smarty.post.szkola}{$smarty.post.szkola}{else}{$edu.education_name}{/if}" placeholder="" />
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span2">
                                        Kierunek:
                                    </div>
                                    <div class="span8">
                                        <input style="width: 484px;" type="text" name="kierunek" value="{if isset($smarty.post.kierunek) && $smarty.post.kierunek}{$smarty.post.kierunek}{else}{$edu.education_field}{/if}" placeholder="" />
                                    </div>
                                </div>
                                <a href="javascript:;" class="button anuluj" style="float: right; margin-left: 10px; margin-top: 10px;"> ANULUJ <i class="icon icon-white icon-warning-sign"></i></a>
                                <a href="javascript:;" onclick="$(this).parent().submit();" class="button sukces" style="float: right; margin-top: 10px;"> ZAPISZ <i class="icon icon-white icon-ok"></i></a>
                            </form>
                            <div class="clear"></div>
                        </div>
                    </div>
                    {/foreach}
                    {/if}

                    <div class="dodaj"{if isset($smarty.post.wyksztalcenie) && !isset($smarty.post.id)} style="display: none;"{/if}>Dodaj{if $userEducation} kolejne{/if} wykształcenie <i class="icon icon-white icon-add-item"></i></div>

                    <div class="dodaj-doswiadczenie"{if isset($smarty.post.wyksztalcenie) && !isset($smarty.post.id)} style="display: block;"{/if}>

                        <form action="" method="post">
                            <input type="hidden" name="teraz" value="0" />
                            <input type="hidden" name="wyksztalcenie" value="1" />
                            <div class="row-fluid">
                                <div class="span2">
                                    Od:{if isset($smarty.post.wyksztalcenie) && isset($errorArray.od) && $errorArray.od} <span class="label-error"></span>{/if}
                                </div>
                                <div class="span3 data-input-3">
                                    <input type="text" name="od" value="{if isset($smarty.post.od) && !isset($smarty.post.id)}{$smarty.post.od}{/if}" placeholder="" />
                                </div>
                                <div class="span1">
                                    Do:{if isset($smarty.post.wyksztalcenie) && isset($errorArray.do) && $errorArray.do} <span class="label-error"></span>{/if}
                                </div>
                                <div class="span3 data-input-3">
                                    <input type="text" name="do" value="{if isset($smarty.post.do) && !isset($smarty.post.id)}{$smarty.post.do}{/if}" placeholder="" />
                                </div>
                                <div class="span3">
                                    <label class="input-checkbox" style="margin: 0px 0px 5px 0px;">
                                        <div class="label-checkbox" style="margin: 0px 5px 5px 0px; padding: 0px; color: #A5A5A5">Do teraz:</div>
                                        <input type="checkbox" value="1" style="display: none;" name="teraz"{if isset($smarty.post.wyksztalcenie) && isset($smarty.post.teraz) && $smarty.post.teraz} checked=""{/if}>
                                        <div class="checkbox-bg" style="margin: 0px 0px 10px 0px;">
                                            <div class="check-bg"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    Szkoła: {if isset($errorArray.szkola) && $errorArray.szkola}<span class="label-error"></span>{/if}
                                </div>
                                <div class="span8">
                                    <input style="width: 484px;" maxlength="50" type="text" name="szkola" value="{if isset($smarty.post.szkola) && !isset($smarty.post.id)}{$smarty.post.szkola}{/if}" placeholder="" />
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    Kierunek: {if isset($errorArray.kierunek) && $errorArray.kierunek}<span class="label-error"></span>{/if}
                                </div>
                                <div class="span8">
                                    <input style="width: 484px;" maxlength="50"  type="text" name="kierunek" value="{if isset($smarty.post.kierunek) && !isset($smarty.post.id)}{$smarty.post.kierunek}{/if}" placeholder="" />
                                </div>
                            </div>
                            <a href="javascript:;" class="button anuluj" style="float: right; margin-left: 10px; margin-top: 10px;"> ANULUJ <i class="icon icon-white icon-warning-sign"></i></a>
                            <a href="javascript:;" onclick="$(this).parent().submit();" class="button sukces" style="float: right; margin-top: 10px;"> DODAJ <i class="icon icon-white icon-ok"></i></a>
                        </form>

                        <div class="clear"></div>

                    </div>
          
                    <div class="zwin"><< zwiń</div>
                    
                    <div class="clear"></div>
                </div>
            </div>
             
            {* JĘZYKI *}
                
            <div class="doswiadczenie">
                
                <a name="jezyki"></a>

                <div class="header" id="2" style="{if isset($userBgCVColors.2)}background-color: {$userBgCVColors.2};{/if}{if isset($userCVColors.2)}color: {$userCVColors.2};{/if}">
                    JĘZYKI
                    <div class="rozwin">rozwiń >></div>
                    <div class="zmien-kolor">
                        ZMIEŃ KOLORY <i class="icon icon-white icon-adjust"></i>
                        <div class="box">
                            <p>WYBIERZ KOLOR PASKA:</p>
                            <div class="color checked" style="background-color: #4f5252;"></div>
                            <div class="color" style="background-color: #FFF;"></div>
                            <div class="color" style="background-color: #f16437;"></div>
                            <div class="color" style="background-color: #c5c5c5;"></div>
                            <div class="color" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                            <p>WYBIERZ KOLOR CZCIONKI:</p>
                            <div class="fcolor" style="background-color: #4f5252;"></div>
                            <div class="fcolor checked" style="background-color: #FFF;"></div>
                            <div class="fcolor" style="background-color: #f16437;"></div>
                            <div class="fcolor" style="background-color: #c5c5c5;"></div>
                            <div class="fcolor" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                    
                <div class="box-do-rozwijania">
                    {if $userLangs}
                    {foreach from=$userLangs item=lang}
                    <div class="item">
                        <div class="info"{if isset($smarty.post.jezyki) && isset($smarty.post.id) && $smarty.post.id=={$lang.lang_id}} style="display: none;"{/if}>
                            <div class="row-fluid">
                                <div class="span2 pozycja">{$lang.lang_name}</div>
                                <div class="span7">{$lang.lang_level}</div>
                                <div class="span3">
                                    <div class="buttons">
                                        <a href="#usun" id="3-{$lang.lang_id}" data-toggle="modal" class="button usun" style="float: right;"> USUŃ <i class="icon icon-white icon-trash"></i></a>
                                        <a href="javascript:;" class="button edit" style="float: right;"> EDYTUJ <i class="icon icon-white icon-edit"></i></a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edytuj-doswiadczenie"{if isset($smarty.post.jezyki) && isset($smarty.post.id) && $smarty.post.id=={$lang.lang_id}} style="display: block;"{/if}>
                            <form action="" method="post">
                                <input type="hidden" name="jezyki" value="1" />
                                <input type="hidden" name="id" value="{$lang.lang_id}" />
                                <div class="row-fluid">
                                    <div class="span2">
                                        Język:
                                    </div>
                                    <div class="span8">
                                        <input style="width: 484px;" type="text" name="jezyk" value="{if isset($smarty.post.jezyk) && $smarty.post.jezyk}{$smarty.post.jezyk}{else}{$lang.lang_name}{/if}" placeholder="" />
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span2">
                                        Poziom:
                                    </div>
                                    <div class="span8">
                                        <input style="width: 484px;" type="text" name="poziom" value="{if isset($smarty.post.poziom) && $smarty.post.poziom}{$smarty.post.poziom}{else}{$lang.lang_level}{/if}" placeholder="" />
                                    </div>
                                </div>
                                <a href="javascript:;" class="button anuluj" style="float: right; margin-left: 10px; margin-top: 10px;"> ANULUJ <i class="icon icon-white icon-warning-sign"></i></a>
                                <a href="javascript:;" onclick="$(this).parent().submit();" class="button sukces" style="float: right; margin-top: 10px;"> ZAPISZ <i class="icon icon-white icon-ok"></i></a>
                            </form>
                            <div class="clear"></div>
                        </div>
                    </div>
                    {/foreach}
                    {/if}

                    <div class="dodaj"{if isset($smarty.post.jezyki) && !isset($smarty.post.id)} style="display: none;"{/if}>Dodaj{if $userLangs} kolejny{/if} język <i class="icon icon-white icon-add-item"></i></div>

                    <div class="dodaj-doswiadczenie"{if isset($smarty.post.jezyki) && !isset($smarty.post.id)} style="display: block;"{/if}>

                        <form action="" method="post">
                            <input type="hidden" name="jezyki" value="1" />
                            <div class="row-fluid">
                                <div class="span2">
                                    Język: {if isset($errorArray.jezyk) && $errorArray.jezyk}<span class="label-error"></span>{/if}
                                </div>
                                <div class="span8">
                                    <input style="width: 484px;" maxlength="50" type="text" name="jezyk" value="{if isset($smarty.post.jezyk) && !isset($smarty.post.id)}{$smarty.post.jezyk}{/if}" placeholder="" />
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    Poziom: {if isset($errorArray.poziom) && $errorArray.poziom}<span class="label-error"></span>{/if}
                                </div>
                                <div class="span8">
                                    <input style="width: 484px;" maxlength="50"  type="text" name="poziom" value="{if isset($smarty.post.poziom) && !isset($smarty.post.id)}{$smarty.post.poziom}{/if}" placeholder="" />
                                </div>
                            </div>
                            <a href="javascript:;" class="button anuluj" style="float: right; margin-left: 10px; margin-top: 10px;"> ANULUJ <i class="icon icon-white icon-warning-sign"></i></a>
                            <a href="javascript:;" onclick="$(this).parent().submit();" class="button sukces" style="float: right; margin-top: 10px;"> DODAJ <i class="icon icon-white icon-ok"></i></a>
                        </form>

                        <div class="clear"></div>

                    </div>
                                    
                    <div class="zwin"><< zwiń</div>
                    
                    <div class="clear"></div>
                </div>
            </div>
                                
            {* SZKOLENIA *}

            <div class="doswiadczenie">
                
                <a name="szkolenia"></a>

                <div class="header" id="3" style="{if isset($userBgCVColors.3)}background-color: {$userBgCVColors.3};{/if}{if isset($userCVColors.3)}color: {$userCVColors.3};{/if}">
                    SZKOLENIA
                    <div class="rozwin">rozwiń >></div>
                    <div class="zmien-kolor">
                        ZMIEŃ KOLORY <i class="icon icon-white icon-adjust"></i>
                        <div class="box">
                            <p>WYBIERZ KOLOR PASKA:</p>
                            <div class="color checked" style="background-color: #4f5252;"></div>
                            <div class="color" style="background-color: #FFF;"></div>
                            <div class="color" style="background-color: #f16437;"></div>
                            <div class="color" style="background-color: #c5c5c5;"></div>
                            <div class="color" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                            <p>WYBIERZ KOLOR CZCIONKI:</p>
                            <div class="fcolor" style="background-color: #4f5252;"></div>
                            <div class="fcolor checked" style="background-color: #FFF;"></div>
                            <div class="fcolor" style="background-color: #f16437;"></div>
                            <div class="fcolor" style="background-color: #c5c5c5;"></div>
                            <div class="fcolor" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                
                <div class="box-do-rozwijania">
                    {if $userTraining}
                    {foreach from=$userTraining item=tra}
                    <div class="item">
                        <div class="info"{if isset($smarty.post.szkolenia) && isset($smarty.post.id) && $smarty.post.id=={$tra.training_id}} style="display: none;"{/if}>
                            <div class="row-fluid">
                                <div class="span2 date">
                                    <small>Data zakończenia</small> {$tra.training_end}
                                </div>
                                <div class="span4 pozycja">{$tra.training_name}</div>
                            </div>
                            <div class="buttons">
                                <a href="#usun" id="4-{$tra.training_id}" data-toggle="modal" class="button usun" style="float: right;"> USUŃ <i class="icon icon-white icon-trash"></i></a>
                                <a href="javascript:;" class="button edit" style="float: right;"> EDYTUJ <i class="icon icon-white icon-edit"></i></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="edytuj-doswiadczenie"{if isset($smarty.post.szkolenia) && isset($smarty.post.id) && $smarty.post.id=={$tra.training_id}} style="display: block;"{/if}>
                            <form action="" method="post">
                                <input type="hidden" name="szkolenia" value="1" />
                                <input type="hidden" name="id" value="{$tra.training_id}" />
                                <div class="row-fluid">
                                    <div class="span2">
                                        Data zakończenia szkolenia:
                                    </div>
                                    <div class="span4 data-input-3">
                                        <input type="text" name="koniec" value="{if isset($smarty.post.szkolenia) && isset($smarty.post.koniec) && $smarty.post.koniec}{$smarty.post.koniec}{else}{$tra.training_end}{/if}" placeholder="" />
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span2">
                                        Nazwa szkolenia:
                                    </div>
                                    <div class="span8">
                                        <input style="width: 484px;" type="text" name="nazwa" value="{if isset($smarty.post.nazwa) && $smarty.post.nazwa}{$smarty.post.nazwa}{else}{$tra.training_name}{/if}" placeholder="" />
                                    </div>
                                </div>
                                <a href="javascript:;" class="button anuluj" style="float: right; margin-left: 10px; margin-top: 10px;"> ANULUJ <i class="icon icon-white icon-warning-sign"></i></a>
                                <a href="javascript:;" onclick="$(this).parent().submit();" class="button sukces" style="float: right; margin-top: 10px;"> ZAPISZ <i class="icon icon-white icon-ok"></i></a>
                            </form>
                            <div class="clear"></div>
                        </div>
                    </div>
                    {/foreach}
                    {/if}

                    <div class="dodaj"{if isset($smarty.post.szkolenia) && !isset($smarty.post.id)} style="display: none;"{/if}>Dodaj{if $userTraining} kolejne{/if} szkolenie <i class="icon icon-white icon-add-item"></i></div>

                    <div class="dodaj-doswiadczenie"{if isset($smarty.post.szkolenia) && !isset($smarty.post.id)} style="display: block;"{/if}>

                        <form action="" method="post">
                            <input type="hidden" name="szkolenia" value="1" />
                            <div class="row-fluid">
                                <div class="span2">
                                    Data ukończenia:{if isset($smarty.post.szkolenia) && isset($errorArray.koniec) && $errorArray.koniec} <span class="label-error"></span>{/if}
                                </div>
                                <div class="span7 data-input-3">
                                    <input type="text" name="koniec" value="{if isset($smarty.post.koniec) && !isset($smarty.post.id)}{$smarty.post.koniec}{/if}" placeholder="" />
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    Nazwa szkolenia: {if isset($errorArray.nazwa) && $errorArray.nazwa}<span class="label-error"></span>{/if}
                                </div>
                                <div class="span7">
                                    <input style="width: 484px;" maxlength="50" type="text" name="nazwa" value="{if isset($smarty.post.nazwa) && !isset($smarty.post.id)}{$smarty.post.nazwa}{/if}" placeholder="" />
                                </div>
                            </div>
                            <a href="javascript:;" class="button anuluj" style="float: right; margin-left: 10px; margin-top: 10px;"> ANULUJ <i class="icon icon-white icon-warning-sign"></i></a>
                            <a href="javascript:;" onclick="$(this).parent().submit();" class="button sukces" style="float: right; margin-top: 10px;"> DODAJ <i class="icon icon-white icon-ok"></i></a>
                        </form>

                        <div class="clear"></div>

                    </div>
                                    
                    <div class="zwin"><< zwiń</div>
                    
                    <div class="clear"></div>
                </div>
            </div>
             
            {* UMIEJĘTNOŚCI *}
                
            <div class="doswiadczenie">
                
                <a name="umiejetnosci"></a>

                <div class="header" id="4" style="{if isset($userBgCVColors.4)}background-color: {$userBgCVColors.4};{/if}{if isset($userCVColors.4)}color: {$userCVColors.4};{/if}">
                    UMIEJĘTNOŚCI
                    <div class="rozwin">rozwiń >></div>
                    <div class="zmien-kolor">
                        ZMIEŃ KOLORY <i class="icon icon-white icon-adjust"></i>
                        <div class="box">
                            <p>WYBIERZ KOLOR PASKA:</p>
                            <div class="color checked" style="background-color: #4f5252;"></div>
                            <div class="color" style="background-color: #FFF;"></div>
                            <div class="color" style="background-color: #f16437;"></div>
                            <div class="color" style="background-color: #c5c5c5;"></div>
                            <div class="color" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                            <p>WYBIERZ KOLOR CZCIONKI:</p>
                            <div class="fcolor" style="background-color: #4f5252;"></div>
                            <div class="fcolor checked" style="background-color: #FFF;"></div>
                            <div class="fcolor" style="background-color: #f16437;"></div>
                            <div class="fcolor" style="background-color: #c5c5c5;"></div>
                            <div class="fcolor" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                
                <div class="box-do-rozwijania">
                    {if $userOtherSkill}
                    {foreach from=$userOtherSkill item=skill}
                    <div class="item">
                        <div class="info"{if isset($smarty.post.umiejetnosci) && isset($smarty.post.id) && $smarty.post.id=={$skill.skill_id}} style="display: none;"{/if}>
                            <div class="row-fluid">
                                <div class="span2 pozycja">{$skill.skill_name}</div>
                                <div class="span7">{$skill.skill_opis}</div>
                                <div class="span3">
                                    <div class="buttons">
                                        <a href="#usun" id="5-{$skill.skill_id}" data-toggle="modal" class="button usun" style="float: right;"> USUŃ <i class="icon icon-white icon-trash"></i></a>
                                        <a href="javascript:;" class="button edit" style="float: right;"> EDYTUJ <i class="icon icon-white icon-edit"></i></a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edytuj-doswiadczenie"{if isset($smarty.post.umiejetnosci) && isset($smarty.post.id) && $smarty.post.id=={$skill.skill_id}} style="display: block;"{/if}>
                            <form action="" method="post">
                                <input type="hidden" name="umiejetnosci" value="1" />
                                <input type="hidden" name="id" value="{$skill.skill_id}" />
                                <div class="row-fluid">
                                    <div class="span2">
                                        Nazwa:
                                    </div>
                                    <div class="span8">
                                        <input style="width: 484px;" type="text" name="nazwa" value="{if isset($smarty.post.nazwa) && $smarty.post.nazwa}{$smarty.post.nazwa}{else}{$skill.skill_name}{/if}" placeholder="" />
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span2">
                                        Opis:
                                    </div>
                                    <div class="span8">
                                        <input style="width: 484px;" type="text" name="opis" value="{if isset($smarty.post.opis) && $smarty.post.opis}{$smarty.post.opis}{else}{$skill.skill_opis}{/if}" placeholder="" />
                                    </div>
                                </div>
                                <a href="javascript:;" class="button anuluj" style="float: right; margin-left: 10px; margin-top: 10px;"> ANULUJ <i class="icon icon-white icon-warning-sign"></i></a>
                                <a href="javascript:;" onclick="$(this).parent().submit();" class="button sukces" style="float: right; margin-top: 10px;"> ZAPISZ <i class="icon icon-white icon-ok"></i></a>
                            </form>
                            <div class="clear"></div>
                        </div>
                    </div>
                    {/foreach}
                    {/if}

                    <div class="dodaj"{if isset($smarty.post.umiejetnosci) && !isset($smarty.post.id)} style="display: none;"{/if}>Dodaj{if $userOtherSkill} kolejną{/if} umiejętność <i class="icon icon-white icon-add-item"></i></div>

                    <div class="dodaj-doswiadczenie"{if isset($smarty.post.umiejetnosci) && !isset($smarty.post.id)} style="display: block;"{/if}>

                        <form action="" method="post">
                            <input type="hidden" name="umiejetnosci" value="1" />
                            <div class="row-fluid">
                                <div class="span2">
                                    Nazwa: {if isset($errorArray.nazwa) && $errorArray.nazwa}<span class="label-error"></span>{/if}
                                </div>
                                <div class="span8">
                                    <input style="width: 484px;" maxlength="50" type="text" name="nazwa" value="{if isset($smarty.post.nazwa) && !isset($smarty.post.id)}{$smarty.post.nazwa}{/if}" placeholder="" />
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    Opis: {if isset($errorArray.opis) && $errorArray.opis}<span class="label-error"></span>{/if}
                                </div>
                                <div class="span8">
                                    <input style="width: 484px;" maxlength="50"  type="text" name="opis" value="{if isset($smarty.post.opis) && !isset($smarty.post.id)}{$smarty.post.opis}{/if}" placeholder="" />
                                </div>
                            </div>
                            <a href="javascript:;" class="button anuluj" style="float: right; margin-left: 10px; margin-top: 10px;"> ANULUJ <i class="icon icon-white icon-warning-sign"></i></a>
                            <a href="javascript:;" onclick="$(this).parent().submit();" class="button sukces" style="float: right; margin-top: 10px;"> DODAJ <i class="icon icon-white icon-ok"></i></a>
                        </form>

                        <div class="clear"></div>

                    </div>
                                    
                    <div class="zwin"><< zwiń</div>
                    
                    <div class="clear"></div>
                </div>
            </div>
                                  
            {* UPRAWNIENIA *}
            
            <div class="doswiadczenie">
                
                <a name="uprawnienia"></a>
                
                <div class="header" id="5" style="{if isset($userBgCVColors.5)}background-color: {$userBgCVColors.5};{/if}{if isset($userCVColors.5)}color: {$userCVColors.5};{/if}">
                    UPRAWNIENIA
                    <div class="rozwin">rozwiń >></div>
                    <div class="zmien-kolor">
                        ZMIEŃ KOLORY <i class="icon icon-white icon-adjust"></i>
                        <div class="box">
                            <p>WYBIERZ KOLOR PASKA:</p>
                            <div class="color checked" style="background-color: #4f5252;"></div>
                            <div class="color" style="background-color: #FFF;"></div>
                            <div class="color" style="background-color: #f16437;"></div>
                            <div class="color" style="background-color: #c5c5c5;"></div>
                            <div class="color" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                            <p>WYBIERZ KOLOR CZCIONKI:</p>
                            <div class="fcolor" style="background-color: #4f5252;"></div>
                            <div class="fcolor checked" style="background-color: #FFF;"></div>
                            <div class="fcolor" style="background-color: #f16437;"></div>
                            <div class="fcolor" style="background-color: #c5c5c5;"></div>
                            <div class="fcolor" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                
                <div class="box-do-rozwijania">
                    <form action="" method="post">
                        <input type="hidden" name="uprawnienia" value="1" />

                        <div class="item uprawnienia">

                            <input type="hidden" name="a1" value="0" />
                            <input type="hidden" name="a" value="0" />
                            <input type="hidden" name="b1" value="0" />
                            <input type="hidden" name="b" value="0" />
                            <input type="hidden" name="c1" value="0" />
                            <input type="hidden" name="c" value="0" />
                            <input type="hidden" name="d1" value="0" />
                            <input type="hidden" name="d" value="0" />
                            <input type="hidden" name="be" value="0" />
                            <input type="hidden" name="c1e" value="0" />
                            <input type="hidden" name="ce" value="0" />
                            <input type="hidden" name="d1e" value="0" />
                            <input type="hidden" name="de" value="0" />
                            <input type="hidden" name="t" value="0" />

                            <div class="row-fluid">
                                <div class="span3" style="line-height: 20px;">
                                    Prawo jazdy kategorii:
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_a1} active{/if}">
                                        <span class="driving-licence-kat">A1</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="a1"{if $userSkill.skill_driving_a1} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_a} active{/if}">
                                        <span class="driving-licence-kat">A</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="a"{if $userSkill.skill_driving_a} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_b1} active{/if}">
                                        <span class="driving-licence-kat">B1</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="b1"{if $userSkill.skill_driving_b1} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_b} active{/if}">
                                        <span class="driving-licence-kat">B</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="b"{if $userSkill.skill_driving_b} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_c1} active{/if}">
                                        <span class="driving-licence-kat">C1</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="c1"{if $userSkill.skill_driving_c1} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_c} active{/if}">
                                        <span class="driving-licence-kat">C</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="c"{if $userSkill.skill_driving_c} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_d1} active{/if}">
                                        <span class="driving-licence-kat">D1</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="d1"{if $userSkill.skill_driving_d1} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span3" style="line-height: 40px;"></div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_d} active{/if}">
                                        <span class="driving-licence-kat">D</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="d"{if $userSkill.skill_driving_d} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_be} active{/if}">
                                        <span class="driving-licence-kat">BE</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="be"{if $userSkill.skill_driving_be} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_c1e} active{/if}">
                                        <span class="driving-licence-kat">C1E</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="c1e"{if $userSkill.skill_driving_c1e} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_ce} active{/if}">
                                        <span class="driving-licence-kat">CE</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="ce"{if $userSkill.skill_driving_ce} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_d1e} active{/if}">
                                        <span class="driving-licence-kat">D1E</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="d1e"{if $userSkill.skill_driving_d1e} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_de} active{/if}">
                                        <span class="driving-licence-kat">DE</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="de"{if $userSkill.skill_driving_de} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                                <div class="span1">
                                    <label class="driving-licence-check{if $userSkill.skill_driving_t} active{/if}">
                                        <span class="driving-licence-kat">T</span>
                                        <span class="driving-licence-corner"></span>
                                        <input type="checkbox" name="t"{if $userSkill.skill_driving_t} checked="checked"{/if} value="1">
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="item">

                            <input type="hidden" name="tig" value="0" />
                            <input type="hidden" name="mig" value="0" />
                            <input type="hidden" name="mag" value="0" />

                            <div class="row-fluid">
                                <div class="span3" style="line-height: 55px;">
                                    Uprawnienia spawalnicze:
                                </div>
                                <div class="span3">
                                    <label class="input-checkbox">
                                        <input type="checkbox" name="tig"{if $userSkill.skill_welding_tig} checked="checked"{/if} value="1">
                                        <div class="checkbox-bg">
                                            <div class="check-bg"></div>
                                        </div>
                                        <div class="label-checkbox">TIG</div>
                                    </label>
                                </div>
                                <div class="span3">
                                    <label class="input-checkbox">
                                        <input type="checkbox" name="mig"{if $userSkill.skill_welding_mig} checked="checked"{/if} value="1">
                                        <div class="checkbox-bg">
                                            <div class="check-bg"></div>
                                        </div>
                                        <div class="label-checkbox">MIG</div>
                                    </label>
                                </div>
                                <div class="span3">
                                    <label class="input-checkbox">
                                        <input type="checkbox" name="mag"{if $userSkill.skill_welding_mag} checked="checked"{/if} value="1">
                                        <div class="checkbox-bg">
                                            <div class="check-bg"></div>
                                        </div>
                                        <div class="label-checkbox">MAG</div>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="item">

                            <input type="hidden" name="sanel_owner" value="0" />
                            <input type="hidden" name="sanel_not_expired" value="0" />

                            <div class="row-fluid">
                                <div class="span3" style="line-height: 40px;">
                                    Książeczka sanepidu:
                                </div>
                                <div class="span4">
                                    <label class="input-checkbox">
                                        <input type="checkbox" name="sanel_owner"{if $userSkill.skill_sanel_owner} checked="checked"{/if} value="1">
                                        <div class="checkbox-bg">
                                            <div class="check-bg"></div>
                                        </div>
                                        <div class="label-checkbox" style="padding: 10px 0px 0px 20px;">posiadam książeczkę</div>
                                    </label>
                                </div>
                                <div class="span4">
                                    <label class="input-checkbox">
                                        <input type="checkbox" name="sanel_not_expired"{if $userSkill.skill_sanel_not_expired} checked="checked"{/if} value="1">
                                        <div class="checkbox-bg">
                                            <div class="check-bg"></div>
                                        </div>
                                        <div class="label-checkbox" style="padding: 10px 0px 0px 20px;">książeczka jest ważna</div>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="item">

                            <input type="hidden" name="to_1kv" value="0" />
                            <input type="hidden" name="more_1kv" value="0" />

                            <div class="row-fluid">
                                <div class="span3" style="line-height: 40px;">
                                    Uprawnienia elektryczne:
                                </div>
                                <div class="span4">
                                    <label class="input-checkbox">
                                        <input type="checkbox" name="to_1kv"{if $userSkill.skill_electricity_to_1kv} checked="checked"{/if} value="1">
                                        <div class="checkbox-bg">
                                            <div class="check-bg"></div>
                                        </div>
                                        <div class="label-checkbox">do 1kV</div>
                                    </label>
                                </div>
                                <div class="span4">
                                    <label class="input-checkbox">
                                        <input type="checkbox" name="more_1kv"{if $userSkill.skill_electricity_more_1kv} checked="checked"{/if} value="1">
                                        <div class="checkbox-bg">
                                            <div class="check-bg"></div>
                                        </div>
                                        <div class="label-checkbox">powyżej 1kV</div>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="item">

                            <input type="hidden" name="forklift" value="0" />
                            <input type="hidden" name="forklift_big" value="0" />

                            <div class="row-fluid">
                                <div class="span3" style="line-height: 40px;">
                                    Wózki widłowe:
                                </div>
                                <div class="span4">
                                    <label class="input-checkbox">
                                        <input type="checkbox" name="forklift"{if $userSkill.skill_forklift} checked="checked"{/if} value="1">
                                        <div class="checkbox-bg">
                                            <div class="check-bg"></div>
                                        </div>
                                        <div class="label-checkbox">wózki widłowe</div>
                                    </label>
                                </div>
                                <div class="span4">
                                    <label class="input-checkbox">
                                        <input type="checkbox" name="forklift_big"{if $userSkill.skill_forklift_big} checked="checked"{/if} value="1">
                                        <div class="checkbox-bg">
                                            <div class="check-bg"></div>
                                        </div>
                                        <div class="label-checkbox">wysoki skład</div>
                                    </label>
                                </div>
                            </div>
                        </div>

                    </form>
                                        
                    <div class="zwin" style="margin-top: 10px;"><< zwiń</div>
                             
                    <a href="javascript:;" onclick="$(this).parent().find('form').submit();" class="button sukces" style="float: right; margin-top: 10px;"> ZAPISZ <i class="icon icon-white icon-ok"></i></a>
                    
                    <div class="clear"></div>      
                    
                </div>
            </div>
                        
            {* ZAINTERESOWANIA *}
                            
            <div class="doswiadczenie">
                
                <a name="zainteresowania"></a>

                <div class="header" id="6" style="{if isset($userBgCVColors.6)}background-color: {$userBgCVColors.6};{/if}{if isset($userCVColors.6)}color: {$userCVColors.6};{/if}">
                    ZAINTERESOWANIA
                    <div class="rozwin">rozwiń >></div>
                    <div class="zmien-kolor">
                        ZMIEŃ KOLORY <i class="icon icon-white icon-adjust"></i>
                        <div class="box">
                            <p>WYBIERZ KOLOR PASKA:</p>
                            <div class="color checked" style="background-color: #4f5252;"></div>
                            <div class="color" style="background-color: #FFF;"></div>
                            <div class="color" style="background-color: #f16437;"></div>
                            <div class="color" style="background-color: #c5c5c5;"></div>
                            <div class="color" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                            <p>WYBIERZ KOLOR CZCIONKI:</p>
                            <div class="fcolor" style="background-color: #4f5252;"></div>
                            <div class="fcolor checked" style="background-color: #FFF;"></div>
                            <div class="fcolor" style="background-color: #f16437;"></div>
                            <div class="fcolor" style="background-color: #c5c5c5;"></div>
                            <div class="fcolor" style="background-color: #38adad;"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                    
                <div class="box-do-rozwijania">

                    <form action="" method="post">
                        <input type="hidden" name="zainteresowania" value="1" />
                        <div class="row-fluid">
                            <div class="span12">
                                <textarea name="hobby" class="editor2">{if isset($smarty.post.hobby) && !isset($smarty.post.id)}{$smarty.post.hobby}{else}{$userProfile.user_hobby}{/if}</textarea>
                            </div>
                        </div>
                        
                    </form>
                            
                    <div class="zwin" style="margin-top: 10px;"><< zwiń</div>
                             
                    <a href="javascript:;" onclick="$(this).parent().find('form').submit();" class="button sukces" style="float: right; margin-top: 10px;"> ZAPISZ <i class="icon icon-white icon-ok"></i></a>
                    
                    <div class="clear"></div>
                    
                </div>

            </div>
                            
            <div class="krok" style="margin-top: 10px">
                <b>KROK 3:</b> zapisz swoje CV do pliku PDF <a href="{$smarty.const.HOST}pdf" class="button" style="float: right; margin-top: -3px; margin-right: 5px; cursor: pointer;">ZAPISZ >></a>
            </div>
                                           
        </div>
        
        {* RIGHT COLUMN *}
        
        <div class="right-column">
            
            <div class="uzupelnij-box">
                
                <a href="{$smarty.const.HOST}moje-konto" class="button" style="float: left; color: #FFF; text-transform: uppercase;"><< Powrót do profilu</a>
                
                <a href="{$smarty.const.HOST}profil" target="_blank" class="btn btn-small btn-happinate" style="float: right; font-size: 12px; padding: 3px 7px 3px 11px;">Zobacz swój profil <i class="icon icon-white icon-eye-open" style="margin-top: 2px;"></i></a>
                
                <div class="clear"></div>
                
                <h2>Twój profil jest uzupełniony w <b style="{if $completePercent<=20}color: #666;{elseif $completePercent<=50}color: #FBB450;{else}color: #39ADAD;{/if}">{$completePercent}%</b>:</h2>
                
                <div class="progress">
                    <div class="bar" style="width: {$completePercent}%;{if $completePercent<=20}background-color: #666;{elseif $completePercent<=50}background-color: #FBB450;{else}background-color: #39ADAD;{/if}"></div>
                </div>
                
                {if $completePercent<100}
                    <h3>Uzupełnij profil!</h3>

                    {if !$userExperience}
                        <a href="#doswiadczenie" class="uzupełnij">DOŚWIADCZENIE<span>+</span></a>
                    {/if}
                    {if !$userEducation}
                        <a href="#wyksztalcenie" class="uzupełnij">WYKSZTAŁCENIE<span>+</span></a>
                    {/if}
                    {if !$userLangs}
                        <a href="#jezyki" class="uzupełnij">JĘZYKI<span>+</span></a>
                    {/if}
                    {if !$userTraining}
                        <a href="#szkolenia" class="uzupełnij">SZKOLENIA<span>+</span></a>
                    {/if}
                    {if !$userOtherSkill}
                        <a href="#umiejetnosci" class="uzupełnij">UMIEJĘTNOŚCI<span>+</span></a>
                    {/if}
                {/if}
                
                <h3>Możesz również dodać:</h3>
                
                <a href="#uprawnienia" class="uzupełnij2">UPRAWNIENIA<span>+</span></a>
                
                {if !$userOtherSkill}
                    <a href="#zainteresowania" class="uzupełnij2">ZAINTERESOWANIA<span>+</span></a>
                {/if}
            </div>
                
        </div>
        
        {* CLEAR *}
        
        <div class="clear"></div>
    </div>
{/block}

{block name=additionalJavaScript}
<script type="text/javascript">
    $('textarea.editor').ckeditor({
        width: '496px',
        wordcount: {
            showWordCount: true,
            showCharCount: true,
            countHTML: false,
            charLimit: '1000',
            wordLimit: 'unlimited' 
        }
    });
    $('textarea.editor2').ckeditor({
        width: '648px',
        wordcount: {
            showWordCount: true,
            showCharCount: true,
            countHTML: false,
            charLimit: '1000',
            wordLimit: 'unlimited' 
        }
    });
</script>
{/block}