{extends file="index.tpl"}

{block name=prebody}
    <div class="center">
        <div class="powiadomienia">
        
        </div>
        <nav class="login-nav" style="float: right; width: 243px">
            <ul>
                <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
            </ul>            
        </nav>
        <div class="clear"></div>
    </div>
{/block}

{block name=body}
    <h2>Ogłoszenia <strong>1-click</strong></h2>
    <hr style="margin-bottom: 60px;">
    <div class="data-table">
        <table class="table candidate" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Nazwa stanowiska</th>
                    <th>Data rozpoczęcia pracy</th>
                    <th style="width: 150px;">Status</th>
                </tr>
            </thead>
            <tbody>
                {if $oneclick}
                {foreach from=$oneclick item=one}
                <tr>
                    {if $one.job_position}
                    <td><a href="{$smarty.const.HOST}praca/oneclick/{$one.job_id}" target="_blank">{$one.job_position}</a> <span style="font-size:10px; color: #999;">(oferta pracy)</span></td>
                    {else}
                    <td><a href="{$smarty.const.HOST}moje-konto/oneclick/{$one.job_id}" target="_blank">{$one.title}</a> <span style="font-size:10px; color: #999;">(powiadomienie 1-click)</span></td>
                    {/if}
                    <td>{$one.job_start_date}</td>
                    <td>
                        {if $one.status==3}
                            <span class="label label-important">Oferta pracy została anulowana <br><i style="font-weight: normal;">niestety pracodawca anulował tę ofertę pracy</i></span>
                        {elseif $one.status==2}
                            <span class="label label-warning">Lista rezerwowych <br><i style="font-weight: normal;">po zaakceptowaniu 1-click'a znajdujesz się na liście rezerwowych<br>o zmiane statusu poinformujemy Cię mailowo</i></span>
                        {elseif $one.status==1}
                            <span class="label label-success">Przyjdź do pracy! <br><i style="font-weight: normal;">gratulujemy! skontaktuj się z pracodawcą po dokładniejsze informacje</i></span>
                        {elseif $one.status==4}
                            <span class="label label-important">Odrziciłeś to zaproszenie <br><i style="font-weight: normal;"><a style="color: #FFF;" href="{$smarty.const.HOST}praca/oneclick/{$one.job_id}" target="_blank">odrzuciłeś to zaproszenie</a></i></span>
                        {else}
                            <span class="label label-info">Rozpatrywane <br><i style="font-weight: normal;"><a style="color: #FFF;" href="{$smarty.const.HOST}praca/oneclick/{$one.job_id}" target="_blank">przejdź do oferty i zaakceptuj zaproszenie</a></i></span>
                        {/if}
                    </td>
                </tr>
                {/foreach}
                {/if}
            </tbody>
        </table>
    </div>
            
{/block}