{extends file="index.tpl"}

{block name=prebody}
    <div class="center">
        <nav class="login-nav">
            <ul>
                <li{if $actionName=='znajomiAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/znajomi">Znajomi</a></li>
                <li{if $actionName=='mojeOgloszeniaAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
                <li{if $actionName=='mojeCvAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto/moje-cv">Moje CV</a></li>
                <li{if $actionName=='indexAction'} class="active"{/if}><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
            </ul>            
        </nav>
    </div>
        
    <div id="addCV" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Dodaj CV</h3>
      </div>
      <div class="modal-body">
        <form id="addCVForm" action="{$smarty.const.HOST}moje-konto/moje-cv/nowe" enctype="multipart/form-data" method="post">
            <div class="avatar-input-box" style="background: #FFF; width: 500px; padding: 0px;">
                <div class="action-avatar-box" style="margin-top: 10px; width: 100%;">
                    <div class="btn-browser" style="width: 318px;">
                        WYBIERZ PLIK ZE SWOJEGO DYSKU <i class="icon-upload icon-white"></i>
                        <input type="file" onChange="readCV(this)" accept="*" name="cv">
                    </div>
                </div>
                <div class="clear"></div>
                <div style="margin: 10px 0px; display: none; font-weight: normal; padding: 10px;" id="uploaded" class="label label-success"></div>
            </div>
            <div class="clear"></div>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <button class="btn btn-primary" onClick="$('#addCVForm').submit();">Dodaj CV</button>
      </div>
    </div>
            
    <div id="delete" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Czy na pewno chcesz zrezygnować z posiadania konta w serwisie happinate.com?</h3>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <a class="btn btn-danger" href="{$smarty.const.HOST}moje-konto/usun">Tak</a>
      </div>
    </div>
        
    <div id="inviteFriend" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel"><img  src="{$smarty.const.HOST}view/images/letsworktogether.png" width="200px" /></h3>
      </div>
     <div class="modal-body">
        <form action="" method="post" id="inviteFriendForm">
            <input type="hidden" name="inviteFriend" value="1" />
            <p>Zaproś znajomych i <strong>pracujcie razem!</strong></p>
            <hr>
            <p><input type="text" name="friendEmail" placeholder="adres email..." style="width: 500px;" value="{if isset($smarty.post.friendEmail) && $smarty.post.friendEmail}{$smarty.post.friendEmail}{/if}" /></p>
        </form>
     </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <a href="#" onclick="$('#inviteFriendForm').submit();" class="btn btn-success">Wyślij zaproszenie</a>
      </div>
    </div>
{/block}

{block name=body}
    
    <a href="javascript:;" data-toggle="modal" data-target="#inviteFriend" class="btn btn-large add-candidate no-border-radius" style="float: right;"><img data-content="dsadsa" data-toggle="popover" data-original-title="dsd" data-placement="left" data-trigger="hover" style="float: right;" width="190px;" src="{$smarty.const.HOST}view/images/letsworktogether.png" /></a>
    
    <form class="main-form" id="update" method="post" action="" enctype="multipart/form-data" style="margin-top: 30px;">

        <input type="hidden" name="student" value="0" />
        <input type="hidden" name="uncapable" value="0" />
        <input type="hidden" name="driving_a" value="0" />
        <input type="hidden" name="driving_a1" value="0" />
        <input type="hidden" name="driving_b" value="0" />
        <input type="hidden" name="driving_b1" value="0" />
        <input type="hidden" name="driving_c" value="0" />
        <input type="hidden" name="driving_c1" value="0" />
        <input type="hidden" name="driving_d" value="0" />
        <input type="hidden" name="driving_d1" value="0" />
        <input type="hidden" name="driving_be" value="0" />
        <input type="hidden" name="driving_c1e" value="0" />
        <input type="hidden" name="driving_ce" value="0" />
        <input type="hidden" name="driving_d1e" value="0" />
        <input type="hidden" name="driving_de" value="0" />
        <input type="hidden" name="driving_t" value="0" />
        <input type="hidden" name="sanel_owner" value="0" />
        <input type="hidden" name="sanel_not_expired" value="0" />
        <input type="hidden" name="welding_tig" value="0" />
        <input type="hidden" name="welding_mig" value="0" />
        <input type="hidden" name="welding_mag" value="0" />
        <input type="hidden" name="electricity_to_1kv" value="0" />
        <input type="hidden" name="electricity_more_1kv" value="0" />
        <input type="hidden" name="forklift" value="0" />
        <input type="hidden" name="forklift_big" value="0" />
        <input type="hidden" name="worknow" value="0" />
        <input type="hidden" name="sms" value="0" />
        
        <input type="hidden" name="photo" value="{$userProfile.user_logo}" /> 
        <input type="hidden" name="trashFile" id="trashFile" value="0" />
        <input type="hidden" name="update-profile" value="1" />
        
        <input type="hidden" name="profileEditType" id="profileEditType" value="3" />
        
        <h2 style="margin-bottom:10px;">TWOJA WIZYTÓWKA<span class="underline-header-content" style="width:240px;"></span></h2>
        <div class="big-tags" style="margin:0px 0px 1px 0px;">
            <div class="row-fluid">
                <div class="span3" style="color:#fff;"><label for="22">Rodzaj pracy <span class="red-stars">*</span></label></div>
                <div class="span7">
                    <div class="multiple-select-input">
                        <select data-placeholder="Wybierz tagi" multiple="" name="tags[]" class="chosen-select" id="22" style="width:410px;">
                            <option></option>
                            {if $tagList}
                                {foreach from=$tagList item=tag}
                                    <option {if isset($userTags) && is_array($userTags) && in_array($tag.tag_id, $userTags)} selected="selected"{/if} value="{$tag.tag_id}">{$tag.tag_name}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                    <p class="small-tip">Zaznacz przynajmniej 3 rodzaje prac.</p>
                </div>
            </div>
            <a href="#" id="basicBtnSave" onClick="$('#profileEditType').val(1);$('#update').submit();" class="preview-next" style="color:#000; background: #FFF; margin:15px 15px -15px 0px; float:right;"><i class="icon-check"></i> zapisz</a>
            <a style="margin:13px 15px -15px 15px;" href="{$smarty.const.HOST}oferty-pracy/1/{if $userTags && is_array($userTags)}{foreach from=$userTags item=tag name=tag}{($model->getTagName($tag)|lower)|replace:' ':'_'}{if !$smarty.foreach.tag.last},{/if}{/foreach}------{/if}" class="search-job-btn"><i class="icon-search icon-white" style="margin:5px 7px 0px 0px;"></i> szukaj pracy</a>
            <div class="clear"></div>
        </div>
        
        {if $userProfile.user_name && $userProfile.user_surname && $userProfile.user_adress_city && $userProfile.user_phone}
        <div id="basic">
            <div class="edit-candidat-small-window">
                <div class="img-box">
                    {if $userProfile.user_logo}
                    <img width="130" src="{$smarty.const.HOST}upload/userLogo/{$userProfile.user_logo}">  
                    {else}
                    <img width="130" src="{$smarty.const.HOST}view/images/unknown_user.png">
                    {/if}
                </div>
                <div class="candidat-data">
                    <p><strong>{if empty($userProfile.user_name) && empty($userProfile.user_surname)}<i>imię i nazwisko</i>{else}{$userProfile.user_name} {$userProfile.user_surname}{/if}</strong>, {if empty($userProfile.user_adress_city)}<i>miejscowość</i>{else}{$userProfile.user_adress_city}{/if}</p>
                    <p><i class="icon-envelope icon-white" style="margin:5px;"></i> <a href="mailto:{$userProfile.user_email}">{$userProfile.user_email}</a> &nbsp; | &nbsp; tel.: {if empty($userProfile.user_phone)}xxx-xxx-xxx{else}{$userProfile.user_phone}{/if}</p>
                </div>
                {if $model->checkUserCV()}
                    <a href="{$smarty.const.HOST}moje-konto/moje-cv" class="happi-btn-brown preview"><i class="icon-eye-open icon-white" style="margin:3px 7px 0px 0px;"></i> moje CV</a>
                {else}
                    <a href="javascript:;" data-toggle="modal" data-target="#addCV" class="happi-btn-brown preview"><i class="icon-star icon-white" style="margin:3px 7px 0px 0px;"></i> dodaj CV</a>
                {/if}
                <a href="" onclick="$('#basic').slideUp();$('#basicBtnSave').slideUp();$('#advance').slideDown();return (false);" class="happi-btn-brown edit"><i class="icon-edit icon-white" style="margin:3px 7px 0px 0px;"></i> edytuj</a>
                <div class="clear"></div>
            </div>

        </div>
        
        <div id="advance" style="display: none;">
            <div class="edit-candidat-window">
                <a href="javascript:;" onclick="fbLoginUser(); return false;" class="connect-face"></a>

                <div class="row-fluid candidat-data">
                    <div class="span7">
                            <div class="row-fluid">
                                <div class="span3" style="color:#fff;"><label for="1">Imię <span class="red-stars">*</span></label></div>
                                <div class="span7">
                                    <div class="text-input">
                                        <input type="text" name="name" id="1" value="{if isset($smarty.post.name)}{$smarty.post.name}{else}{$userProfile.user_name}{/if}">
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span3" style="color:#fff;"><label for="2">Nazwisko <span class="red-stars">*</span></label></div>
                                <div class="span7">
                                    <div class="text-input">
                                        <input type="text" name="surname" id="2" value="{if isset($smarty.post.surname)}{$smarty.post.surname}{else}{$userProfile.user_surname}{/if}">
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span3" style="color:#fff;"><label for="3">Miejscowość <span class="red-stars">*</span></label></div>
                                <div class="span7">
                                    <div class="text-input">
                                        <input type="text" id="3" name="user_adress_city" value="{if isset($smarty.post.user_adress_city)}{$smarty.post.user_adress_city}{else}{$userProfile.user_adress_city}{/if}">
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span3" style="color:#fff;"><label for="4">Telefon <span class="red-stars">*</span></label></div>
                                <div class="span7">
                                    <div class="text-input">
                                        <input type="text" id="4" name="phone" value="{if isset($smarty.post.phone)}{$smarty.post.phone}{else}{$userProfile.user_phone}{/if}">
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="span5">
                        <div class="avatar-input-box">
                            <div class="img-box">
                                {if $userProfile.user_logo}
                                    <img id="imagePreview" src="{$smarty.const.HOST}upload/userLogo/{$userProfile.user_logo}">
                                {else}
                                    <img id="imagePreview" src="{$smarty.const.HOST}view/images/unknown_user.png">
                                {/if}
                            </div>
                            <div class="action-avatar-box">
                                <div class="btn-browser">
                                    zdjęcie profilowe *
                                    <input type="file" name="photo" accept="image/*" onchange="readURL(this);">
                                </div>
                                <span class="btn-erase" onclick="deleteFile();$('#trashFile').val(1);"><i class="icon-trash icon-white" style="margin:3px 3px 0px 0px;"></i> usuń zdjęcie</span>
                            </div>
                            <div class="clear"></div>
                            <small style="font-size: 11px; color: #FFF;">* zdjęcie profilowe musi być formatu JPG, PNG lub GIF</small>
                            <div class="action-file-box candidate-profile-cv">
                                <div class="btn-browser" style="width:297px;">
                                    <a href="javascript:;" data-toggle="modal" data-target="#addCV" class="happi-btn-brown preview"><i class="icon-star icon-white" style="margin:3px 7px 0px 0px;"></i> dodaj CV</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                
                <div class="work-now" style="height:120px">
                    <label class="input-checkbox" style="top: 50px;">
                        <input type="checkbox" name="worknow" {if $userProfile.user_worknow} checked="checked"{/if} value="1">
                        <div class="checkbox-bg">
                            <div class="check-bg" style="margin: 4px 2px"></div>
                        </div>
                        <div class="label-checkbox" style="color:#c10101;font-size:13px;padding: 0px 12px;" >Chciałbym otrzymywać powiadomienia o ofertach typu PRACUJ TERAZ.</div>
                    </label>
                        
                    <label  style="top: 85px;" class="input-checkbox">
                        <input type="checkbox" name="sms" {if $userProfile.user_sms} checked="checked"{/if} value="1">
                        <div class="checkbox-bg">
                            <div class="check-bg" style="margin: 4px 2px"></div>
                        </div>
                        <div class="label-checkbox" style="color:#c10101;font-size:13px;padding: 0px 12px;" >Bądź na bieżąco i otrzymuj powiadomienia SMS!</div>
                    </label>
                </div>

                    <a href="#" onClick="$('#profileEditType').val(2);$('#update').submit();" class="preview-next" style="color:#000; background: #FFF; margin:0px 65px 0px 0px;"><i class="icon-check"></i> zapisz</a>
            
                <div class="clear"></div>
            </div>
            
        </div>
        {else}
        <div id="advance">
            <div class="edit-candidat-window">
                <a href="javascript:;" onclick="fbLoginUser(); return false;" class="connect-face"></a>

                <div class="row-fluid candidat-data">
                    <div class="span7">
                            <div class="row-fluid">
                                <div class="span3" style="color:#fff;"><label for="1">Imię <span class="red-stars">*</span></label></div>
                                <div class="span7">
                                    <div class="text-input">
                                        <input type="text" name="name" id="1" value="{if isset($smarty.post.name)}{$smarty.post.name}{else}{$userProfile.user_name}{/if}">
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span3" style="color:#fff;"><label for="2">Nazwisko <span class="red-stars">*</span></label></div>
                                <div class="span7">
                                    <div class="text-input">
                                        <input type="text" name="surname" id="2" value="{if isset($smarty.post.surname)}{$smarty.post.surname}{else}{$userProfile.user_surname}{/if}">
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span3" style="color:#fff;"><label for="3">Miejscowość <span class="red-stars">*</span></label></div>
                                <div class="span7">
                                    <div class="text-input">
                                        <input type="text" id="3" name="user_adress_city" value="{if isset($smarty.post.user_adress_city)}{$smarty.post.user_adress_city}{else}{$userProfile.user_adress_city}{/if}">
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span3" style="color:#fff;"><label for="4">Telefon <span class="red-stars">*</span></label></div>
                                <div class="span7">
                                    <div class="text-input">
                                        <input type="text" id="4" name="phone" value="{if isset($smarty.post.phone)}{$smarty.post.phone}{else}{$userProfile.user_phone}{/if}">
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="span5">
                        <div class="avatar-input-box">
                            <div class="img-box">
                                {if $userProfile.user_logo}
                                    <img id="imagePreview" src="{$smarty.const.HOST}upload/userLogo/{$userProfile.user_logo}">
                                {else}
                                    <img id="imagePreview" src="{$smarty.const.HOST}view/images/unknown_user.png">
                                {/if}
                            </div>
                            <div class="action-avatar-box">
                                <div class="btn-browser">
                                    zdjęcie profilowe *
                                    <input type="file" name="photo" accept="image/*" onchange="readURL(this);">
                                </div>
                                <span class="btn-erase" onclick="deleteFile();$('#trashFile').val(1);"><i class="icon-trash icon-white" style="margin:3px 3px 0px 0px;"></i> usuń zdjęcie</span>
                            </div>
                            <div class="clear"></div>
                            <small style="font-size: 11px; color: #FFF;">* zdjęcie profilowe musi być nie wieksze niż 100px na 100px<br/>oraz być formatu JPG, PNG lub GIF</small>
                            <div class="action-file-box candidate-profile-cv">
                                <div class="btn-browser" style="width:297px;">
                                    <a href="{$smarty.const.HOST}moje-konto/moje-cv" style="color:#FFF;"><span class="file-label">dołącz swoje CV</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                
                <div class="work-now" style="height:120px">
                    <label class="input-checkbox" style="top: 50px;">
                        <input type="checkbox" name="worknow" {if $userProfile.user_worknow} checked="checked"{/if} value="1">
                        <div class="checkbox-bg">
                            <div class="check-bg" style="margin: 4px 2px"></div>
                        </div>
                        <div class="label-checkbox" style="color:#c10101;font-size:13px;padding: 0px 12px;" >Chciałbym otrzymywać powiadomienia o ofertach typu PRACUJ TERAZ.</div>
                    </label>
                        
                    <label  style="top: 85px;" class="input-checkbox">
                        <input type="checkbox" name="sms" {if $userProfile.user_sms} checked="checked"{/if} value="1">
                        <div class="checkbox-bg">
                            <div class="check-bg" style="margin: 4px 2px"></div>
                        </div>
                        <div class="label-checkbox" style="color:#c10101;font-size:13px;padding: 0px 12px;" >Bądź na bieżąco i otrzymuj powiadomienia SMS!</div>
                    </label>
                </div>
                        
             <div class="clear"></div>
             
                <a href="#" onClick="$('#profileEditType').val(2);$('#update').submit();" class="preview-next" style="color:#000; background: #FFF; margin:0px 65px 0px 0px;"><i class="icon-check"></i> zapisz</a>
            
                <div class="clear"></div>
            </div>
            
        </div>
        {/if}
        </form>
        
        <p style="text-align:center;margin:20px 0px;"><img src="{$smarty.const.HOST}view/images/candidat-header.png"></p>
        <form class="main-form" action="" method="post" id="changePassword">
            <h2 style="margin-top:30px;">ZMIEŃ HASŁO<span class="underline-header-content" style="width:160px;"></span></h2>
            <div class="input-box left">
                <input type="hidden" name="profileEditType" value="4" />
                <label class="input-box-half">
                    <input type="password" name="password_old" placeholder="Stare hasło" value="" autocomplete="off">
                </label>
                <label class="input-box-half right">
                    <input type="password" name="password_new" placeholder="Nowe hasło" value="" autocomplete="off">
                </label>
                <label class="input-box-half right">
                    <input type="password" name="password_new2" placeholder="Powtórz nowe hasło" value="" autocomplete="off">
                </label>

                <div class="right-column">
                    <button onClick="$('#changePassword').submit();return(false);" class="happi-btn" style="float:right; margin-top:11px;">ZMIEŃ HASŁO <i class="icon-chevron-right icon-white"></i></button>
                </div>
            </div>
        </form>

        
        <div class="clear"></div>
        
        {if $smarty.const.MOD_CALENDAR}
            {include file="mojeKonto/kandydat/mojKalendarzBox.tpl"}
        {/if}
{*  
        <!-- Left column -->
        <div class="left-column">
            
            <h2 style="margin-top:30px;">DOŚWIADCZENIE ZAWODOWE<span class="underline-header-content" style="width:345px;"></span></h2>
            
            <div class="input-box left">
                <label class="input-checkbox">
                    <input type="checkbox" value="1" name="student"{if $userProfile.user_student} checked="checked"{/if}>
                    <div class="checkbox-bg">
                        <div class="check-bg"></div>
                    </div>
                    <div class="label-checkbox">jestem studentem</div>
                </label>
                <label class="input-checkbox">
                    <input type="checkbox" value="1" name="uncapable"{if $userProfile.user_uncapable} checked="checked"{/if}>
                    <div class="checkbox-bg">
                        <div class="check-bg"></div>
                    </div>
                    <div class="label-checkbox">mam orzeczenie o niepełnosprawdności</div>
                </label>
                <div class="clear"></div>	
            </div>

            {if !empty($userProfile.experiences)}
                {foreach from=$userProfile.experiences item=exp}            
                <div class="experience-box">
                        <div class="input-box left">
                            <label>
                                <span class="input-box-label">Nazwa firmy</span>
                                <input type="text" name="experience[{$exp.experience_id}][]" placeholder="Wpisz nazwę firmy" value="{$exp.experience_company}" autocomplete="off">
                            </label>
                        </div>
                        <div class="input-box left" style="margin:0px auto;padding:0px;">
                            <label class="calendar input-box-half">
                                <span class="input-box-label">Okres zatrudnienia</span>
                                <input type="text" name="experience[{$exp.experience_id}][]" class="experience-date" placeholder="od" value="{if strtotime($exp.experience_start)!=0}{$exp.experience_start}{/if}">
                                <i class="icon-calendar-input"></i>
                            </label>
                            <label class="calendar input-box-half right">
                                <span class="input-box-label"></span>
                                <input type="text" name="experience[{$exp.experience_id}][]" class="experience-date" placeholder="do" value="{if strtotime($exp.experience_end)!=0}{$exp.experience_end}{/if}">
                                <i class="icon-calendar-input"></i>
                            </label>
                        </div>
                        <div class="clear"></div>
                        <div class="input-box left">
                            <label>
                                <span class="input-box-label">Stanowisko</span>
                                <input type="text" name="experience[{$exp.experience_id}][]" placeholder="Wpisz stanowisko" value="{$exp.experience_position}" autocomplete="off">
                            </label>
                        </div>

                    <a href="javascript:;" onClick="addExperienceBox(this, {$exp.experience_id+1}); return false;" class="happi-btn add-btn"><strong>+</strong> DODAJ</a>
                    <a href="javascript:;" onClick="removeExperienceBox(this, {$exp.experience_id}); return false;" class="happi-btn brown remove-btn"><i class="icon-trash icon-white"></i> USUŃ</a>
                    <div class="clear"></div>
                </div>
                {/foreach}
            {else}
            <div class="experience-box">
                <div class="input-box left">
                    <label>
                        <span class="input-box-label">Nazwa firmy</span>
                        <input type="text" name="newexperience[0][]" placeholder="Wpisz nazwę firmy" value="" autocomplete="off">
                    </label>
                </div>
                <div class="input-box left" style="margin:0px auto;padding:0px;">
                    <label class="calendar input-box-half">
                        <span class="input-box-label">Okres zatrudnienia</span>
                        <input type="text" name="newexperience[0][]" class="experience-date" placeholder="od" value="">
                        <i class="icon-calendar-input"></i>
                    </label>
                    <label class="calendar input-box-half right">
                        <span class="input-box-label"></span>
                        <input type="text" name="newexperience[0][]" class="experience-date" placeholder="do" value="">
                        <i class="icon-calendar-input"></i>
                    </label>
                </div>
                <div class="clear"></div>
                <div class="input-box left">
                    <label>
                        <span class="input-box-label">Stanowisko</span>
                        <input type="text" name="newexperience[0][]" placeholder="Wpisz stanowisko" value="" autocomplete="off">
                    </label>
                </div>
                <a href="javascript:;" onClick="addExperienceBox(this, 1); return false;" class="happi-btn add-btn"><strong>+</strong> DODAJ</a>
                <a href="javascript:;" onClick="removeExperienceBox(this); return false;" class="happi-btn brown remove-btn"><i class="icon-trash icon-white"></i> USUŃ</a>
                <div class="clear"></div>
            </div>
            {/if}
           
            <h2 style="margin-top:30px;">WYKSZTAŁCENIE<span class="underline-header-content" style="width:195px;"></span></h2>
            {if !empty($userProfile.education)}
                {foreach from=$userProfile.education item=edu}
                    <div class="education-box">
                        <div class="input-box left">
                            <label>
                                <span class="input-box-label">Nazwa uczelni</span>
                                <input type="text" name="education[{$edu.education_id}][]" placeholder="Wpisz nazwę uczelni" value="{$edu.education_name}" autocomplete="off">
                            </label>
                        </div>
                        <div class="input-box left" style="margin:0px auto;padding:0px;">
                            <label class="calendar input-box-half">
                                <span class="input-box-label">Okres nauczania</span>
                                <input type="text" name="education[{$edu.education_id}][]" class="experience-date" placeholder="od" value="{if strtotime($edu.education_start)!=0}{$edu.education_start}{/if}">
                                <i class="icon-calendar-input"></i>
                            </label>
                            <label class="calendar input-box-half right">
                                <span class="input-box-label"></span>
                                <input type="text" name="education[{$edu.education_id}][]" class="experience-date" placeholder="do" value="{if strtotime($edu.education_end)!=0}{$edu.education_end}{/if}">
                                <i class="icon-calendar-input"></i>
                            </label>
                        </div>
                        <div class="clear"></div>
                        <div class="input-box left">
                            <label>
                                <span class="input-box-label">Kierunek</span>
                                <input type="text" name="education[{$edu.education_id}][]" placeholder="Wpisz kierunek" value="{$edu.education_field}" autocomplete="off">
                            </label>
                        </div>
                        <a href="javascript:;" onClick="addEducationBox(this, {$edu.education_id+1}); return false;" class="happi-btn add-btn"><strong>+</strong> DODAJ</a>
                        <a href="javascript:;" onClick="removeEducationBox(this, {$edu.education_id}); return false;" class="happi-btn brown remove-btn"><i class="icon-trash icon-white"></i> USUŃ</a>
                        <div class="clear"></div>
                    </div>
                {/foreach}
            {else}
            <div class="education-box">
                <div class="input-box left">
                    <label>
                        <span class="input-box-label">Nazwa uczelni</span>
                        <input type="text" name="neweducation[0][]" placeholder="Wpisz nazwę uczelni" value="" autocomplete="off">
                    </label>
                </div>
                <div class="input-box left" style="margin:0px auto;padding:0px;">
                    <label class="calendar input-box-half">
                        <span class="input-box-label">Okres nauczania</span>
                        <input type="text" name="neweducation[0][]" class="experience-date" placeholder="od" value="">
                        <i class="icon-calendar-input"></i>
                    </label>
                    <label class="calendar input-box-half right">
                        <span class="input-box-label"></span>
                        <input type="text" name="neweducation[0][]" class="experience-date" placeholder="do" value="">
                        <i class="icon-calendar-input"></i>
                    </label>
                </div>
                <div class="clear"></div>
                <div class="input-box left">
                    <label>
                        <span class="input-box-label">Kierunek</span>
                        <input type="text" name="neweducation[0][]" placeholder="Wpisz kierunek" value="" autocomplete="off">
                    </label>
                </div>
                <a href="javascript:;" onClick="addEducationBox(this, 1); return false;" class="happi-btn add-btn"><strong>+</strong> DODAJ</a>
                <a href="javascript:;" onClick="removeEducationBox(this); return false;" class="happi-btn brown remove-btn"><i class="icon-trash icon-white"></i> USUŃ</a>
                <div class="clear"></div>
            </div>
            {/if}

            <h2 style="margin-top:30px;">DODATKOWE UMIEJĘTNOŚCI<span class="underline-header-content" style="width:335px;"></span></h2>
            <div class="input-box left">
                {if !empty($userProfile.skills)}
                    {foreach from=$userProfile.skills item=skill}
                        <div class="add-item">
                            <input type="text" name="skills[{$skill.skill_id}]" placeholder="Wpisz umiejętności" value="{$skill.skill_text}">
                            <span class="delete-item-btn tooltip-item" title="Usuń"><i class="icon-delete-item"></i></span>
                            <span class="add-item-btn tooltip-item" title="Dodaj"><i class="icon-add-item"></i></span>
                        </div>
                    {/foreach}
                {else}
                    <div class="add-item">
                        <input type="text" name="newskills[]" placeholder="Wpisz umiejętności" value="">
                        <span class="delete-item-btn tooltip-item" title="Usuń"><i class="icon-delete-item"></i></span>
                        <span class="add-item-btn tooltip-item" title="Dodaj"><i class="icon-add-item"></i></span>
                    </div>
                {/if}
            </div>

            <h2 style="margin-top:30px;">ZMIEŃ HASŁO<span class="underline-header-content" style="width:160px;"></span></h2>
            <div class="input-box left">
                <label class="input-box-half">
                    <input type="password" name="password_old" placeholder="Stare hasło" value="" autocomplete="off">
                </label>
                <label class="input-box-half right">
                    <input type="password" name="password_new" placeholder="Nowe hasło" value="" autocomplete="off">
                </label>
            </div>
            <div class="clear"></div>
        </div>
        <div class="right-column">

            {include file="mojeKonto/kandydat/mojKalendarzBox.tpl"}

            <h2 style="margin-top:30px;">ADRES ZAMIESZKANIA<span class="underline-header-content" style="width:255px;"></span></h2>
            <div class="input-box right">
                <label>
                    <span class="input-box-label">Ulica</span>
                    <input type="text" name="adress_street" placeholder="Wpisz ulice" value="{$userProfile.user_adress_street}" autocomplete="off">
                </label>
            </div>
            <div class="input-box right">
                <label>
                    <span class="input-box-label">Miejscowość</span>
                    <input type="text" name="adress_city" placeholder="Wpisz miejscowość" value="{$userProfile.user_adress_city}" autocomplete="off">
                </label>
            </div>
            <div class="input-box right">
                <label>
                    <span class="input-box-label">Kod pocztowy</span>
                    <input type="text" name="adress_zip_code" placeholder="00-000" value="{$userProfile.user_adress_zip_code}" autocomplete="off">
                </label>
            </div>
			
                <h2 style="margin-top:30px;">UPRAWNIENIA<span class="underline-header-content" style="width:165px;"></span></h2>
                <div class="input-box right">
                        <span class="input-box-label">Prawo jazdy</span>
                </div>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">A1</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_a1" value="1"{if $userProfile.skill_driving_a1} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">A</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_a" value="1"{if $userProfile.skill_driving_a} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">B1</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_b1" value="1"{if $userProfile.skill_driving_b1} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">B</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_b" value="1"{if $userProfile.skill_driving_b} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">C1</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_c1" value="1"{if $userProfile.skill_driving_c1} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">C</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_c" value="1"{if $userProfile.skill_driving_c} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">D1</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_d1" value="1"{if $userProfile.skill_driving_d1} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">D</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_d" value="1"{if $userProfile.skill_driving_d} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">BE</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_be" value="1"{if $userProfile.skill_driving_be} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">C1E</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_c1e" value="1"{if $userProfile.skill_driving_c1e} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">CE</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_ce" value="1"{if $userProfile.skill_driving_ce} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">D1E</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_d1e" value="1"{if $userProfile.skill_driving_d1e} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">DE</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_de" value="1"{if $userProfile.skill_driving_de} checked="checked"{/if}>
                </label>
                <label class="driving-licence-check">
                        <span class="driving-licence-kat">T</span>
                        <span class="driving-licence-corner"></span>
                        <input type="checkbox" name="driving_t" value="1"{if $userProfile.skill_driving_t} checked="checked"{/if}>
                </label>
                
                <div class="clear" style="height:20px;"></div>
                
                <div class="input-box right">
                    <span class="input-box-label">Książeczka sanepidu</span>
                    <label class="input-checkbox">
                        <input type="checkbox" name="sanel_owner" value="1"{if $userProfile.skill_sanel_owner} checked="checked"{/if}>
                        <div class="checkbox-bg">
                            <div class="check-bg"></div>
                        </div>
                        <div class="label-checkbox">mam książeczkę</div>
                    </label>
                    <div class="clear"></div>
                    <label class="input-checkbox">
                        <input type="checkbox" name="sanel_not_expired" value="1"{if $userProfile.skill_sanel_not_expired} checked="checked"{/if}>
                        <div class="checkbox-bg">
                            <div class="check-bg"></div>
                        </div>
                        <div class="label-checkbox">książeczka jest ważna</div>
                    </label>
                </div>
                
                <div class="clear" style="height:20px;"></div>
                
                <div class="input-box right">
                    <span class="input-box-label">Uprawnienia spawalnicze</span>
                    <label class="input-checkbox">
                        <input type="checkbox" name="welding_tig" value="1"{if $userProfile.skill_welding_tig} checked="checked"{/if}>
                        <div class="checkbox-bg">
                            <div class="check-bg"></div>
                        </div>
                        <div class="label-checkbox">TIG</div>
                    </label>
                    <div class="clear"></div>
                    <label class="input-checkbox">
                        <input type="checkbox" name="welding_mig" value="1"{if $userProfile.skill_welding_mig} checked="checked"{/if}>
                        <div class="checkbox-bg">
                            <div class="check-bg"></div>
                        </div>
                        <div class="label-checkbox">MIG</div>
                    </label>
                    <div class="clear"></div>
                    <label class="input-checkbox">
                        <input type="checkbox" name="welding_mag" value="1"{if $userProfile.skill_welding_mag} checked="checked"{/if}>
                        <div class="checkbox-bg">
                            <div class="check-bg"></div>
                        </div>
                        <div class="label-checkbox">MAG</div>
                    </label>
                </div>
                
                <div class="clear" style="height:20px;"></div>
                
                <div class="input-box right">
                    <span class="input-box-label">Uprawnienia elektryczne</span>
                    <label class="input-checkbox">
                        <input type="checkbox" name="electricity_to_1kv" value="1"{if $userProfile.skill_electricity_to_1kv} checked="checked"{/if}>
                        <div class="checkbox-bg">
                            <div class="check-bg"></div>
                        </div>
                        <div class="label-checkbox">do 1kV</div>
                    </label>
                    <div class="clear"></div>
                    <label class="input-checkbox">
                        <input type="checkbox" name="electricity_more_1kv" value="1"{if $userProfile.skill_electricity_more_1kv} checked="checked"{/if}>
                        <div class="checkbox-bg">
                            <div class="check-bg"></div>
                        </div>
                        <div class="label-checkbox">powyżej 1kV</div>
                    </label>
                </div>
                
                <div class="clear" style="height:20px;"></div>
                
                <div class="input-box right">
                    <span class="input-box-label">Wózki widłowe</span>
                    <label class="input-checkbox">
                        <input type="checkbox" name="forklift" value="1"{if $userProfile.skill_forklift} checked="checked"{/if}>
                        <div class="checkbox-bg">
                            <div class="check-bg"></div>
                        </div>
                        <div class="label-checkbox">wózki widłowe</div>
                    </label>
                    <div class="clear"></div>
                        <label class="input-checkbox">
                        <input type="checkbox" name="forklift_big" value="1"{if $userProfile.skill_forklift_big} checked="checked"{/if}>
                        <div class="checkbox-bg">
                            <div class="check-bg"></div>
                        </div>
                        <div class="label-checkbox">wysoki skład</div>
                    </label>
                </div>
        </div>
        
        <div style="height:20px;" class="clear"></div>
        
        <h2>AKCJE<span class="underline-header-content" style="width:70px;"></span></h2>
        <div class="left-column">
            <a href="#" data-toggle="modal" data-target="#delete"  onClick="//$('.main-form').submit();" class="happi-btn brown" style="float:left;margin-top:14px;padding:12px 10px 11px 10px;">USUŃ KONTO</a>
        </div>
        <div class="right-column">
            <a href="#" onClick="$('.main-form').submit();" class="happi-btn" style="float:right;margin-top:14px;padding:10px 10px 11px 15px;">ZAPISZ <i class="icon-chevron-right icon-white"></i></a>
        </div>
        
        <div class="clear"></div>
 *}
    
{/block}