{extends file="index.tpl"}

{block name=prebody}
    {include file="mojeKonto/administrator/header.tpl"}

{/block}

{block name=body}
    <div class="data-table">
        <h2 style="margin-bottom: 10px;">Statystyki</h2>
    </div>
    
    <table style="width: 30%; float: left; margin-right: 20px;">
        <tr>
            <td align="right"><h2>Użytkownicy</h2></td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Wszyscy:</strong></td>
            <td width="50px" align="center">{$stats.users.all}</td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Kandydaci:</strong></td>
            <td width="50px" align="center">{$stats.users.kandydaci}</td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Pracodawcy:</strong></td>
            <td width="50px" align="center">{$stats.users.pracodawcy}</td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Aktywni:</strong></td>
            <td width="50px" align="center">{$stats.users.aktywni}</td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Nieaktywni:</strong></td>
            <td width="50px" align="center">{$stats.users.nieaktywni}</td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Zarejstrowani dzisiaj:</strong></td>
            <td width="50px" align="center">{$stats.users.dzisiaj}</td>
        </tr>       
    </table>
    <table style="width: 30%; float: left; margin-right: 20px;">
        <tr>
            <td align="right"><h2>Ogłoszenia</h2></td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Wszystkie:</strong></td>
            <td width="50px" align="center">{$stats.jobs.all}</td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Aktywne:</strong></td>
            <td width="50px" align="center">{$stats.jobs.aktywne}</td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Zaplacone:</strong></td>
            <td width="50px" align="center">{$stats.jobs.zaplacone}</td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Dodane dzisiaj:</strong></td>
            <td width="50px" align="center">{$stats.jobs.dzisiaj}</td>
        </tr>    
    </table>
    <table style="width: 30%; float: left; margin-right: 20px;">
        <tr>
            <td align="right"><h2>Aplikacje</h2></td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Wszystkie:</strong></td>
            <td width="50px" align="center">{$stats.apps.all}</td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Wysłane dzisiaj:</strong></td>
            <td width="50px" align="center">{$stats.apps.dzisiaj}</td>
        </tr>    
    </table>
    <div class="clear"></div>
    <table style="width: 30%; margin-top: 20px; float: left; margin-right: 20px;">
        <tr>
            <td align="right"><h2>One click</h2></td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Wszystkie:</strong></td>
            <td width="50px" align="center">{$stats.oneclick.all}</td>
        </tr>
    </table>
    <table style="width: 30%; margin-top: 20px; float: left; margin-right: 20px;">
        <tr>
            <td align="right"><h2>CV</h2></td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Wszystkie:</strong></td>
            <td width="50px" align="center">{$stats.cv.all}</td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Dodane dzisiaj:</strong></td>
            <td width="50px" align="center">{$stats.cv.dzisiaj}</td>
        </tr>
    </table>
    <table style="width: 30%; margin-top: 20px; float: left; margin-right: 20px;">
        <tr>
            <td align="right"><h2>Tagi</h2></td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Wszystkie:</strong></td>
            <td width="50px" align="center">{$stats.tag.all}</td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Aktywne:</strong></td>
            <td width="50px" align="center">{$stats.tag.aktywne}</td>
        </tr>
        <tr>
            <td width="200px" align="right"><strong>Nieaktywne:</strong></td>
            <td width="50px" align="center">{$stats.tag.nieaktywne}</td>
        </tr>
    </table>        
    <div class="clear"></div>
{/block}