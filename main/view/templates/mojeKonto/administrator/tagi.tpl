{extends file="index.tpl"}

{block name=prebody}
    {include file="mojeKonto/administrator/header.tpl"}
        
    <div id="deleteTAG" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Czy na pewno usunąć tag?</h3>
      </div>
      <div class="modal-footer">
        <a id="deleteTagHref" href="" class="btn btn-success">Tak</a>
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
      </div>
    </div>    
    
    <div id="acceptTag" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Czy na pewno zaakceptować tag?</h3>
      </div>
      <div class="modal-footer">
        <a id="acceptTagHref" href="" class="btn btn-success">Tak</a>
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
      </div>
    </div>
{/block}

{block name=body}
    <div class="data-table">
        <h2 style="margin-bottom: 10px;">Tagi <strong>nieaktywne</strong></h2>

        <table class="table tag" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th class="column-name">Nazwa</th>
                    <th class="column-action" style="width: 10px;">Akcje</th>
                </tr>
            </thead>
            <tbody>
                {if $tagsNotActive}
                    {foreach from=$tagsNotActive item=tag}
                        <tr>
                            <td>{$tag.tag_name}</td>
                            <td>     
                                <a class="tooltip-item btn btn-success btn-mini" onClick="$('#acceptTagHref').attr('href', '{$smarty.const.HOST}moje-konto/tagi/akceptuj,{$tag.tag_id}');" href="javascript:;" data-toggle="modal" data-target="#acceptTag" data-original-title="Akceptuj tag"><i class="icon-ok icon-white"></i></a>
                                <a class="tooltip-item btn btn-danger btn-mini" onClick="$('#deleteTagHref').attr('href', '{$smarty.const.HOST}moje-konto/tagi/usun,{$tag.tag_id}');" href="javascript:;" data-toggle="modal" data-target="#deleteTAG" data-original-title="Usuń tag"><i class="icon-ban-circle icon-white"></i></a>
                            </td>
                        </tr>
                    {/foreach}
                {/if}
            </tbody>
        </table>        
            
        <div class="clear"></div>
        
        <h2 style="margin-bottom: 10px;">Tagi <strong>aktywne</strong></h2>

        <table class="table tag" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th class="column-name">Nazwa</th>
                </tr>
            </thead>
            <tbody>
                {if $tagsActive}
                    {foreach from=$tagsActive item=tag}
                        <tr>
                            <td>{$tag.tag_name}</td>
                        </tr>
                    {/foreach}
                {/if}
            </tbody>
        </table>
            
    </div>
    <div class="clear"></div>
{/block}