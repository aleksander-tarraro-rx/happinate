{extends file="index.tpl"}

{block name=prebody}
    {include file="mojeKonto/administrator/header.tpl"}
    <div id="addRabat" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Wygeneruj nowe kody</h3>
      </div>
      <div class="modal-body">
        <form id="addRabatForm" action="{$smarty.const.HOST}moje-konto/rabaty/generuj" method="post">
            <label>
                <p>Nazwa rabat:</p>
                <input type="text" name="nazwa" placeholder="Nazwa kody" style="width:518px;"><br>
            </label>
            <label>
                <p>Procent rabatu:</p>
                <select name="procent">
                    <option value="10">10%</option>
                    <option value="25">25%</option>
                    <option value="50">50%</option>
                    <option value="100">100%</option>
                </select>
            </label>
            <label>
                <p>Ilość użyć rabatu:</p>
                <input type="text" name="count" placeholder="Ilość użyć rabatu" style="width:518px;"><br>
            </label>
            <label>
                <p>Ilość kodów do wygenerowania:</p>
                <input type="text" name="howmany" placeholder="Ilość kodów do wygenerowania" style="width:518px;"><br>
            </label>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        <button class="btn btn-success" onClick="$('#addRabatForm').submit();">Generuj</button>
      </div>
    </div>
{/block}

{block name=body}
    <a href="javascript:;" data-toggle="modal" data-target="#addRabat" class="btn btn-large add-candidate btn-success no-border-radius">+ Wygeneruj nowe kody rabatowe</a>
    
    <div class="data-table">
        <h2 style="margin-bottom: 10px;">Kody <strong>rabatowe</strong></h2>
        
        <table class="table rabat" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="width: 10px;" class="column-name"></th>
                    <th >Nazwa</th>
                    <th style="width: 30px;" class="column-name">%</th>
                    <th style="width: 200px;" class="column-name">Kod</th>
                    <th style="width: 100px;" class="column-name">Ilość</th>
                </tr>
            </thead>
            <tbody>
                {if $kodyRabatowe}
                    {foreach from=$kodyRabatowe item=rabat}
                        <tr>
                            <td>{$rabat.rabat_id}</td>
                            <td>{$rabat.rabat_name}</td>
                            <td>{$rabat.rabat_percent}</td>
                            <td>{$rabat.rabat_code}</td>
                            <td>{$rabat.rabat_count} ({$rabat.rabat_used})</td>
                        </tr>
                    {/foreach}
                {/if}
            </tbody>
        </table>        
            
        <div class="clear"></div>
    </div>
    
{/block}