<!doctype html>
<html lang="pl">
<head>
<meta charset="utf-8">
<title>Praca dorywcza, tymczasowa oraz sezonowa - happinate</title>
<meta name="description" content="Zapraszamy wszystkie osoby szukające pracy nie zależnie czy ma być ona sezonowa czy dorywcza. Na Naszej stronie znajdują się oferty pracy tymczasowej wśród których każdy znajdzie coś dla siebie." />
<meta name="keywords" content="Agencja pracy tymczasowej,Praca tymczasowa,Praca dorywcza,Oferty pracy,Agencja pracy,Dodatkowa praca,Praca sezonowa,Praca MIASTO (Poznań),Praca,Wolontariat,Wolontariat Poznań" />
<meta name="robots" content="index,follow,all" />
<meta name="revisit-after" content="2 days" />
<meta name="publisher" content="modernfactory.pl" />
<meta name="rating" content="general" />

<meta property="og:title" content="happinate – praca dorywcza i tymczasowa" />
<meta property="og:image" content="{$smarty.const.HOST}view/images/happinate-face-img.png" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="320" />
<meta property="og:image:height" content="320" />

<link rel="stylesheet" href="{$smarty.const.HOST}view/css/libs/bootstrap.min.css">
<link rel="stylesheet" href="{$smarty.const.HOST}view/css/libs/bootstrap-responsive.min.css">
<link rel="stylesheet" href="{$smarty.const.HOST}view/css/libs/html5reset-1.6.1.css">
<link rel="stylesheet" href="{$smarty.const.HOST}view/css/landingpage.css">
{if $smarty.const.DEBUG}<link rel="stylesheet" href="{$smarty.const.HOST}view/css/debug.css">{/if}
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <style>
        .underline-header-content{
            display: none;
        }
    </style>
<![endif]-->
<link rel="shortcut icon" href="{$smarty.const.HOST}view/images/favicon.png">
</head>
<body>

    <div class="znajdz3sposoby">
        <div class="bg"></div>
        
        <div class="page">
            <span class="logo"></span>
            
            <table class="table">
                <tr>
                    <td width="340px" style="padding-top: 40px;  line-height: normal;">
                        <p style="font-size: 35px;">Znajdź pracę na</p>
                        <p style="font-size: 46px; color: #38adad; font-weight: 700;">3 sposoby</p>
                    </td>
                    <td width="240px" style="text-align:center;">
                        <img width="140px" height="140px" src="{$smarty.const.HOST}view/images/landingpage/worknow.jpg" alt="worknow" />
                    </td>
                    <td style="text-align:center;">
                        <img width="140px" height="140px" src="{$smarty.const.HOST}view/images/landingpage/letsworktogether.jpg" alt="letsworktogether" />
                    </td>
                    <td width="250px" style="text-align:center;">
                        <img width="140px" height="140px" src="{$smarty.const.HOST}view/images/landingpage/oneclick.jpg" alt="oneclick" />
                    </td>
                </tr>
                <tr style="font-size: 17px; line-height: normal;">
                    <td>
                        <p>Oddajemy Wam do dyspozycji prosty</p>
                        <p>i przejrzysty serwis, dzięki któremu</p>
                        <p>to praca znajdzie Ciebie!</p>
                    </td>
                    <td style="text-align:center;">
                        <p>potwierdzasz i za godzinę</p>
                        <p style="color: #38adad; font-weight: 700;">jesteś w pracy!</p>
                    </td>
                    <td style="text-align:center;">
                        <p>pracuj i zarabiaj</p>
                        <p style="color: #38adad; font-weight: 700;">z przyjaciółmi</p>
                    </td>
                    <td style="text-align:center;">
                        <p style="color: #38adad; font-weight: 700;">klikasz i masz pracę!</p>
                        <p>bez zbędnych formalności</p>
                    </td>
                </tr>
            </table>
                    
            <div class="form">
                <div class="box">
                    <form action="" method="post">
                        <input type="text" name="email" placeholder="Twój adres e-mail" />
                        <input type="password" name="password1" placeholder="wpisz hasło" />
                        <input type="password" name="password2" placeholder="powtórz hasło" />
                        <input type="submit" name="submit" value="DOŁACZ DO NAS!" />
                    </form>
                    {if isset($error)}
                         <div class="alert alert-error" style="font-family: tahoma; font-size: 13px; width: 280px; margin-top: 10px;">
                             <button type="button" class="close" data-dismiss="alert">&times;</button>
                             {$error}
                         </div>
                    {/if}
                </div>
            </div>
        
        </div>
                        
        <div class="footer">
            <div class="text">
                <table width="100%">
                    <tr>
                        <td>&copy; 2013 HAPPINATE</td>
                        <td align="right">
                            <a href="https://www.facebook.com/sharer/sharer.php?src=bm&u=http://{$smarty.server.HTTP_HOST}/landingpage/znajdz3sposoby/facebook" target="_blank" class="facebook"><img src="{$smarty.const.HOST}view/images/landingpage/facebook.jpg" /></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
                        
    </div>
    
    
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>if (typeof (jQuery) == 'undefined') document.write(unescape("%3Cscript " + "src='{$smarty.const.HOST}view/js/libs/jquery.min.js' %3E%3C/script%3E"));</script>
    <script src="{$smarty.const.HOST}view/js/libs/bootstrap.min.js"></script>

    {if !$smarty.const.DEBUG}
    <script type="text/javascript">
            var _gaq = _gaq || []; var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
            _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
            _gaq.push(['_setAccount', 'UA-39041418-1']);
            _gaq.push(['_trackPageview']);
            {if isset($googleAnalytics)}
                _gaq.push(['_trackEvent', 'Landingpage', 'Mailing', '{$googleAnalytics}']);  
            {else}
                _gaq.push(['_trackEvent', 'Landingpage', 'Mailing', '-- brak --']); 
            {/if}
            (function () {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
    </script>
    {/if}
    {if $smarty.const.DEBUG}{include file="debug.tpl"}{/if}
</body>
</html>