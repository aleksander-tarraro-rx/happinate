<!doctype html>
<html lang="pl">
<head>
<meta charset="utf-8">
<title>{block name=metatitle}Praca dorywcza, tymczasowa oraz sezonowa - happinate{/block}</title>
<meta name="description" content="{block name=metadescription}Zapraszamy wszystkie osoby szukające pracy nie zależnie czy ma być ona sezonowa czy dorywcza. Na Naszej stronie znajdują się oferty pracy tymczasowej wśród których każdy znajdzie coś dla siebie.{/block}" />
<meta name="keywords" content="{block name=metakeywords}Agencja pracy tymczasowej,Praca tymczasowa,Praca dorywcza,Oferty pracy,Agencja pracy,Dodatkowa praca,Praca sezonowa,Praca MIASTO (Poznań),Praca,Wolontariat,Wolontariat Poznań{/block}" />
<meta name="robots" content="index,follow,all" />
<meta name="revisit-after" content="2 days" />
<meta name="publisher" content="modernfactory.pl" />
<meta name="rating" content="general" />

<meta property="og:title" content="happinate – praca dorywcza i tymczasowa" />
<meta property="og:image" content="{$smarty.const.HOST}view/images/happinate-face-img.png" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="320" />
<meta property="og:image:height" content="320" />
<meta property="og:description" content="" />

<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <style>
        .underline-header-content{
            display: none;
        }
    </style>
<![endif]-->
<link rel="shortcut icon" href="{$smarty.const.HOST}view/images/favicon.png">
</head>
<body>
<script type="text/javascript">
	var _gaq = _gaq || []; var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
	_gaq.push(['_require', 'inpage_linkid', pluginUrl]);
	_gaq.push(['_setAccount', 'UA-39041418-1']);
	_gaq.push(['_trackPageview']);
	(function () {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
</body>
</html>