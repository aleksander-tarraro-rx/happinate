{extends file="index.tpl"}

{block name=body}

    <div class="left-column">
    	<img src="{$smarty.const.HOST}view/images/login.png">
    </div>
    
    <div class="right-column" style="margin-top:15px;">
    	<h2 class="login-header" style="margin-bottom:15px;">Utwórz konto</h2>
        <p class="text-or">(lub <a href="{$smarty.const.HOST}zaloguj-sie">zaloguj się</a>)</p>
        <div class="clear"></div>
        <form action="" method="post">
            <div class="input-box right" style="padding:0px;">
                <label>
                    <span class="input-box-label">Email</span>
                    <input type="text" name="register-email" placeholder="Wpisz email" value="" autocomplete="off">
                </label>
            </div>
            <div class="input-box right" style="padding:10px 0px 0px 0px;">
                <label>
                    <span class="input-box-label">Hasło</span>
                    <input type="password" name="register-pass" placeholder="Wpisz hasło" value="" autocomplete="off">
                </label>
            </div>
            <div class="input-box right" style="padding:10px 0px 0px 0px;">
                <label>
                    <span class="input-box-label">Powtórz hasło</span>
                    <input type="password" name="register-pass-2" placeholder="Powtórz hasło" value="" autocomplete="off">
                </label>
            </div>
            <input type="submit" class="happi-big-btn" style="width:330px;margin-top:15px; margin-bottom: 15px;" name="register-submit" value="ZAREJESTRUJ SIĘ">
        </form>
    </div>
    
    <div class="clear"></div>

{/block}