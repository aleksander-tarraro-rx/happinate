{extends file="index.tpl"}

{block name=body}
    <div class="left-column">
    	<img src="{$smarty.const.HOST}view/images/registered.png">
    </div>
    
    <div class="right-column">
    	<h2 class="login-header">Gratulacje!</h2>
        <div class="clear"></div>
        <p style="font-size:17px;line-height:25px;text-align:justify; margin-bottom: 10px;">Na podany adres został wysłany mail z kodem aktywacyjnym. Kliknij na niego, aby aktywować konto.</p>
        <p style="font-size:15px;line-height:20px;text-align:justify;">
            <p style="margin-bottom: 10px; font-weight: bold;">Jeżeli w ciągu 5 min nie otrzymasz linku aktywacyjnego wykonaj jedną z opcji:</p>
            <ol style="font-size:12px;line-height:20px;text-align:justify;">
                <li>Sprawdź czy nasza wiadomość nie trafiła do SPAMu.</li>
                <li>Wyślij ponownie link aktywacyjny korzystając z formularza znajdującego się <a style="color: #c90c20; font-weight: bold;" href="{$smarty.const.HOST}rejestracja/wyslij-ponownie-link" target="_blank">TUTAJ</a>.</li>
                <li>Skontaktuj się z nami <a href="mailto:hi@happinate.com" target="_blank">hi@happinate.com</a>.</li>
            </ol>
        </p>
        <a href="{$smarty.const.HOST}zaloguj-sie" class="happi-big-btn" style="margin-top:20px;">ZALOGUJ SIĘ</a>
    </div>
    
    <div class="clear"></div>
{/block}