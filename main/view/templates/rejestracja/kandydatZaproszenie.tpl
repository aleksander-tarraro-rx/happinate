{extends file="index.tpl"}

{block name=body}

    <h2 style="margin:5px 15px 15px 20px;">ZAREJESTRUJ SIĘ</h2>
    <form action="" method="post" id="registerForm">
        <input type="hidden" value="1" name="register" />
        <div class="big-tags rejestracja">
                <div class="row-fluid" style="margin-bottom: 10px;">
                    <div class="span3" style="color:#fff;"><label for="1">KATEGORIA GŁÓWNA <span class="red-stars">*</span></label></div>
                    <div class="span7">
                        <div class="multiple-select-input">
                            <select data-placeholder="Wybierz główną kategorię" multiple="" name="main[]" class="chosen-select-one" id="1" style="width:410px;">
                                <option></option>
                                {if $tagList}
                                    {foreach from=$tagList item=tag}
                                        <option {if isset($smarty.post.tags) && in_array($tag.tag_id, $smarty.post.main)} selected="selected"{/if} value="{$tag.tag_id}">{$tag.tag_name}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </div>
                        <p class="small-tip">wybierz jedną kategorię główną</p>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3" style="color:#fff;"><label for="2">POZOSTAŁE <span class="red-stars">*</span></label></div>
                    <div class="span7">
                        <div class="multiple-select-input">
                            <select name="tags[]" data-placeholder="Wybierz pozostałe kategorie" multiple="" class="chosen-select" id="2" style="width:410px;">
                                <option></option>
                                {if $tagList}
                                    {foreach from=$tagList item=tag}
                                        <option {if isset($smarty.post.tags) && in_array($tag.tag_id, $smarty.post.tags)} selected="selected"{/if} value="{$tag.tag_id}">{$tag.tag_name}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </div>
                        <p class="small-tip">wybierz przynajmniej 2 kategorie pozostałe</p>
                    </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="1">Email <span class="red-stars">*</span></label></div>
                <div class="span7">
                        <div class="text-input">
                            <input type="text" name="register-email" disabled="" value="{$email}" id="1">
                        </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="2">Hasło <span class="red-stars">*</span></label></div>
                <div class="span7">
                    <div class="text-input">
                            <input type="password" name="register-pass" id="2">
                    </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="3">Powtórz hasło <span class="red-stars">*</span></label></div>
                <div class="span7">
                        <div class="text-input">
                                <input type="password" name="register-pass-2" id="3">
                        </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="offset3 span7">
                        <label class="input-checkbox">
            <input type="checkbox"  name="checkbox1" value="1" {if isset($smarty.post.checkbox1) && $smarty.post.checkbox1==1} checked{else} checked{/if}>
            <div class="checkbox-bg">
                <div class="check-bg"></div>
            </div>
            <div class="label-checkbox">Akceptuje <a href="{$smarty.const.HOST}informacje/regulamin" target="_blank">regulamin</a> serwisu</div>
        </label>
                        <div class="clear"></div>
                        <label class="input-checkbox">
            <input type="checkbox"  name="checkbox2" value="1" {if isset($smarty.post.checkbox2) && $smarty.post.checkbox2==1} checked{else} checked{/if}>
            <div class="checkbox-bg">
                <div class="check-bg"></div>
            </div>
            <div class="label-checkbox">Wyrażam zgodę na przetwarzanie moich danych<br>osobowych przez happinate z o.o. <a href="" onclick="if($('.hiddenText').css('display') == 'none') { $('.hiddenText').slideDown(); } else { $('.hiddenText').slideUp(); } return(false);">czytaj więcej »</a></div>
        </label>
            <div class="clear"></div>
            <div class="hiddenText" style="display: none; font-size: 12px; color: #666; line-height: 150%;">Wyrażam zgodę administratorowi danych osobowych - happinate Sp. z o.o., ul. Ratajczaka 42/13, 61-816 Poznań, na przetwarzanie moich danych osobowych, przekazanych za pomocą serwisu Happinate oraz na ich udostępnienie potencjalnym pracodawcom, w celu wykonania na moją rzecz usług pośrednictwa pracy i innych usług określonych w Regulaminie, a także na ich przetwarzanie w celu marketingu produktów lub usług administratora danych. Wyrażam też zgodę na nieodpłatne wykorzystanie i upublicznienie mojego wizerunku poprzez umieszczenie go na stronach internetowych serwisu Happinate i przekazanie potencjalnym pracodawcom. Podanie danych nastąpiło dobrowolnie, przysługuje mi prawo dostępu do nich oraz ich poprawiania. </div>
            <div class="clear"></div>
            </div>
        </div>
    </form>
    <hr>
    <a href="javascript:;" onclick="$('#registerForm').submit();" class="preview-next">UTWÓRZ KONTO <i class="icon-chevron-right icon-white"></i></a>
    <div class="clear"></div>

{/block}