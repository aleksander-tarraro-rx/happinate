{extends file="index.tpl"}

{block name=body}
    <div class="clear"></div>
    <h1 class="register-header">Utwórz darmowe konto</h1>
    <h2 class="register-header"><em>Wybierz typ konta, które chcesz utworzyć</em></h2>

    <a href="{$smarty.const.HOST}rejestracja/pracodawca" class="employer-btn">STREFA<br>PRACODAWCY<br><small>wybierz <i class="icon icon-white icon-chevron-right"></i></small></a>
    <a href="{$smarty.const.HOST}rejestracja/kandydat" class="candidate-btn">STREFA<br>KANDYDATA<br><small>wybierz <i class="icon icon-white icon-chevron-right"></i></small></a>

    <div class="clear"></div>
{/block}