{extends file="index.tpl"}

{block name=body}
{if $smarty.post['register-type']==2}
    <div class="left-column">
    	<img src="{$smarty.const.HOST}view/images/login.png">
    </div>
    
    <div class="right-column" style="margin-top:35px;">
    	<h2 class="login-header">Utwórz konto</h2>
        <p class="text-or">(lub <a href="{$smarty.const.HOST}zaloguj-sie">zaloguj się</a>)</p>
        <div class="clear"></div>
        <form action="" method="post">
            <input type="hidden" name="register-type" value="{$smarty.post['register-type']}" />
            <div class="input-box right" style="padding:0px;">
                <label>
                    <span class="input-box-label">Email</span>
                    <input type="text" name="register-email" placeholder="Wpisz email" value="" autocomplete="off">
                </label>
            </div>
            <div class="input-box right" style="padding:10px 0px 0px 0px;">
                <label>
                    <span class="input-box-label">Hasło</span>
                    <input type="password" name="register-pass" placeholder="Wpisz hasło" value="" autocomplete="off">
                </label>
            </div>
            <div class="input-box right" style="padding:10px 0px 0px 0px;">
                <label>
                    <span class="input-box-label">Powtórz hasło</span>
                    <input type="password" name="register-pass-2" placeholder="Powtórz hasło" value="" autocomplete="off">
                </label>
            </div>
            <input type="submit" class="happi-big-btn" style="width:330px;margin-top:15px; margin-bottom: 15px;" name="register-submit" value="ZAREJESTRUJ SIĘ">
        </form>
    </div>
    
    <div class="clear"></div>
{else}
    <h2 style="margin:5px 15px 15px 20px;">ZAREJESTRUJ SIĘ</h2>
    <form action="" method="post" id="registerForm">
        <input type="hidden" name="register-type" value="{$smarty.post['register-type']}" />
            <div class="big-tags">
                    <div class="row-fluid">
                        <div class="span3" style="color:#fff;"><label for="2">Rodzaj pracy <span class="red-stars">*</span></label></div>
                        <div class="span7">
                            <div class="multiple-select-input">
                                <select name="tags[]" data-placeholder="Wybierz tagi" multiple="" class="chosen-select" id="2" style="width:410px;">
                                    <option></option>
                                    {if $tagList}
                                        {foreach from=$tagList item=tag}
                                            <option {if isset($smarty.post.tags) && in_array($tag.tag_id, $smarty.post.tags)} selected="selected"{elseif isset($smarty.session.basket.tags) && in_array($tag.tag_id, $smarty.session.basket.tags)} selected="selected"{/if} value="{$tag.tag_id}">{$tag.tag_name}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </div>
                            <p class="small-tip">Zaznacz przynajmniej 3 opcje.</p>
                        </div>
                    </div>
            </div>
            <div class="row-fluid">
                    <div class="span3"><label for="1">Email <span class="red-stars">*</span></label></div>
                    <div class="span7">
                            <div class="text-input">
                                    <input type="text" name="register-email" value="{if isset($smarty.post['register-email']) && $smarty.post['register-email']}{$smarty.post['register-email']}{/if}" id="1">
                            </div>
                    </div>
            </div>
            <div class="row-fluid">
                    <div class="span3"><label for="2">Hasło <span class="red-stars">*</span></label></div>
                    <div class="span7">
                            <div class="text-input">
                                    <input type="password" name="register-pass" id="2">
                            </div>
                    </div>
            </div>
            <div class="row-fluid">
                    <div class="span3"><label for="3">Powtórz hasło <span class="red-stars">*</span></label></div>
                    <div class="span7">
                            <div class="text-input">
                                    <input type="password" name="register-pass-2" id="3">
                            </div>
                    </div>
            </div>
            <div class="row-fluid">
                    <div class="offset3 span7">
                            <label class="input-checkbox">
                <input type="checkbox"  name="checkbox1" value="1" {if isset($smarty.post.checkbox1) && $smarty.post.checkbox1==1} checked{else} checked{/if}>
                <div class="checkbox-bg">
                    <div class="check-bg"></div>
                </div>
                <div class="label-checkbox">Akceptuje <a href="{$smarty.const.HOST}informacje/regulamin" target="_blank">regulamin</a> serwisu</div>
            </label>
                            <div class="clear"></div>
                            <label class="input-checkbox">
                <input type="checkbox"  name="checkbox2" value="1" {if isset($smarty.post.checkbox2) && $smarty.post.checkbox2==1} checked{else} checked{/if}>
                <div class="checkbox-bg">
                    <div class="check-bg"></div>
                </div>
                <div class="label-checkbox">Wyrażam zgodę na przetwarzanie moich danych<br>osobowych przez Happinate z o.o. <a href="" onclick="if($('.hiddenText').css('display') == 'none') { $('.hiddenText').slideDown(); } else { $('.hiddenText').slideUp(); } return(false);">czytaj więcej »</a></div>
            </label>
                <div class="clear"></div>
                <div class="hiddenText" style="display: none; font-size: 12px; color: #666; line-height: 150%;">Wyrażam zgodę administratorowi danych osobowych - Happinate Sp. z o.o., ul. Ratajczaka 42/13, 61-816 Poznań, na przetwarzanie moich danych osobowych, przekazanych za pomocą serwisu Happinate oraz na ich udostępnienie potencjalnym pracodawcom, w celu wykonania na moją rzecz usług pośrednictwa pracy i innych usług określonych w Regulaminie, a także na ich przetwarzanie w celu marketingu produktów lub usług administratora danych. Wyrażam też zgodę na nieodpłatne wykorzystanie i upublicznienie mojego wizerunku poprzez umieszczenie go na stronach internetowych serwisu Happinate i przekazanie potencjalnym pracodawcom. Podanie danych nastąpiło dobrowolnie, przysługuje mi prawo dostępu do nich oraz ich poprawiania. </div>
                <div class="clear"></div>
                </div>
            </div>
    </form>
    <hr>
    <a href="#" onclick="$('#registerForm').submit();" class="preview-next">UTWÓRZ KONTO <i class="icon-chevron-right icon-white"></i></a>
    <div class="clear"></div>
{/if}
{/block}