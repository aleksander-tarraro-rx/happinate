{extends file="index.tpl"}

{block name=metatitle}Szukam pracowników - happinate{/block}
{block name=metadescription}Jeżeli szukasz pracowników do swojej firmy w związku z pojawieniem się dorywczej pracy to koniecznie zarejestruj się w Naszym serwisie. Dzięki bazie osób, które poszukują pracy znajdziemy idealnego kandytata.{/block}

{block name=body}
    <div class="left-column">
    	<img src="{$smarty.const.HOST}view/images/login.png">
    </div>
    
    <div class="right-column" style="margin-top: 15px">
    	<h2 class="login-header" style="margin-bottom: 15px">Zaloguj się</h2>
        <p class="text-or">(lub <a href="{$smarty.const.HOST}rejestracja">utwórz konto</a>)</p>
        <div class="clear"></div>
        <a href="#" class="login-with-facebook big" onClick="fbLoginUser(); return false;"></a>
        <hr style="margin-top:20px;">
        <p class="facebook-or">lub</p>

        <form action="" method="post">
        	<div class="input-box right" style="padding:0px;">
                <label>
                    <span class="input-box-label">Email</span>
                    <input type="text" name="login-email" placeholder="Wpisz email" value="">
                </label>
            </div>
            <div class="input-box right" style="padding:10px 0px 0px 0px;">
                <label>
                    <span class="input-box-label">Hasło</span>
                    <input type="password" name="login-pass" placeholder="Wpisz hasło" value="">
                </label>
            </div>
            <input type="submit" class="happi-big-btn" style="width:330px;margin-top:15px;" name="login-submit" value="ZALOGUJ SIĘ">
        </form>
    </div>
    
    <div class="clear"></div>
{/block}