{extends file="index.tpl"}

{block name=body}
	
    <div class="left-column simple-text">
    	<h2 style="text-align:center;">Regulamin</h2>

<p><strong>DEFINICJE</strong></p>

<ol>
	<li>Pojęcie &bdquo;serwis happinate&rdquo; i/lub &bdquo;happinate&rdquo; oznacza udostępniane przez nas funkcje i usługi, w tym:
	<ul>
		<li>naszą stronę internetową <a href="http://www.happinate.com/">www.happinate.com</a> i wszelkie inne strony internetowe oznaczone lub wsp&oacute;łoznaczone marką happinate (w tym poddomeny, wersje międzynarodowe, widżety i wersje dla telefon&oacute;w kom&oacute;rkowych),</li>
		<li>wszelkie funkcje dostarczane na w/w stronach internetowych w imieniu happinate,</li>
		<li>inne multimedia, oprogramowanie (takie jak pasek narzędzi), urządzenia i sieci, kt&oacute;re istnieją obecnie lub zostaną wprowadzone p&oacute;źniej.</li>
	</ul>
	</li>
	<li>Pojęcie &bdquo;informacje&rdquo; oznacza dane, zdarzenia i inne informacje na temat użytkownika, w tym czynności wykonane przez użytkownik&oacute;w i osoby niebędące użytkownikami, kt&oacute;re korzystają z serwisu happinate.</li>
	<li>Pojęcie &bdquo;treści&rdquo; oznacza wszystko, co użytkownik lub inne osoby publikują w serwisie happinate, a co nie mieści się w definicji terminu &bdquo;informacje&rdquo;.</li>
	<li>Pojęcie &bdquo;dane&rdquo;, &bdquo;dane użytkownik&oacute;w&rdquo; lub &bdquo;dane użytkownika&rdquo; odnosi się do wszelkich danych, w tym treści i informacji użytkownika, kt&oacute;re użytkownik i inne osoby mogą uzyskać z serwisu happinate lub przesłać do serwisu happinate za pośrednictwem Platformy.</li>
	<li>Pojęcie &bdquo;opublikowanie&rdquo; oznacza opublikowanie treści w serwisie happinate lub udostępnienie jej w inny spos&oacute;b za pomocą happinate.</li>
	<li>Pojęcie &bdquo;korzystanie&rdquo; oznacza wykorzystywanie, kopiowanie, publiczne odtwarzanie lub wyświetlanie, dystrybucję, modyfikowanie, tłumaczenie i tworzenie prac pochodnych.</li>
	<li>Ilekroć w regulaminie mowa o &bdquo;zatrudnieniu&rdquo;, &bdquo;pracy&rdquo; lub innych podobnych terminach, należy przez to rozumieć świadczenie pracy lub usług na podstawie r&oacute;żnych stosunk&oacute;w prawnych, w tym um&oacute;w cywilnoprawnych.</li>
	<li>Ilekroć w regulaminie mowa o &bdquo;pracodawcy&rdquo; należy przez to rozumieć użytkownika Happinate umieszczającego ogłoszenie w serwisie, poszukującego kandydat&oacute;w do pracy lub zatrudniającego innych użytkownik&oacute;w na zasadach określonych w ust. 7.</li>
	<li>Ilekroć w regulaminie mowa o &bdquo;pracowniku&rdquo;, &bdquo;kandydacie&rdquo; lub innych pojęciach pokrewnych, należy przez to rozumieć osobę poszukującą pracy przez Happinate, aplikującą na określone stanowisko lub wykonującą pracę w ramach umowy zawartej poprzez Happinate.</li>
</ol>

<p><strong>Art. 1</strong></p>

<p><strong>UDOSTĘPNIENIE TREŚCI I DANYCH UŻYTKOWNIK&Oacute;W</strong></p>

<p>Użytkownik jest właścicielem wszystkich treści i informacji publikowanych przez siebie w serwisie happinate i może określać spos&oacute;b udostępniania ich poprzez ustawienia prywatności oraz ustawienia aplikacji. W przypadku treści objętych prawem własności intelektualnej, takich jak zdjęcia i filmy, użytkownik przyznaje happinate poniższe uprawnienia zgodnie z wprowadzonymi przez siebie ustawieniami prywatności i ustawieniami aplikacji: użytkownik przyznaje happinate niewyłączną, zbywalną, obejmującą prawo do udzielania sublicencji, bezpłatną, światową licencję zezwalającą na wykorzystanie wszelkich treści objętych prawem własności intelektualnej publikowanych przez niego w ramach serwisu happinate lub w związku z nim. Licencja ta wygasa wraz z usunięciem przez użytkownika treści objętych prawami własności intelektualnej lub konta, o ile treści nie zostały udostępnione innym osobom, kt&oacute;re ich nie usunęły.</p>

<p>Usunięcie treści objętych prawem własności intelektualnej odbywa się w spos&oacute;b podobny do opr&oacute;żnienia kosza na komputerze. Użytkownik przyjmuje jednak do wiadomości, że kopia zapasowa usuniętych treści może być przechowywana przez uzasadniony okres czasu, nie będzie jednak w tym czasie udostępniana innym.</p>

<p>Podczas korzystania z aplikacji może ona poprosić o uprawnienie dostępu do treści i informacji użytkownika, a także treści i informacji, kt&oacute;re zostały mu udostępnione przez inne osoby. Aplikacje są zobowiązane szanować prywatność użytkownik&oacute;w. Ustalenia pomiędzy użytkownikiem a aplikacją będą regulować spos&oacute;b wykorzystywania, przechowywania i przekazywania wyżej wymienionych treści i informacji przez aplikację.</p>

<p>Gdy użytkownik publikuje treści lub informacje, korzystając z ustawienia Publiczne, zezwala on wszystkim, w tym osobom niebędącym użytkownikami serwisu happinate, na uzyskiwanie dostępu i wykorzystywanie informacji, a także wiązanie ich z użytkownikiem (tj. jego imieniem i nazwiskiem oraz zdjęciem profilowym).</p>

<p>Doceniamy wszelkie opinie i inne propozycje użytkownik&oacute;w dotyczące serwisu happinate. Przekazując opinię lub propozycję, użytkownicy przyznają happinate zezwolenie na wykorzystanie ich bez obowiązku wypłaty wynagrodzenia.</p>

<p><strong>Art. 2</strong></p>

<p><strong>BEZPIECZEŃSTWO</strong></p>

<p>Dokładamy wszelkich starań, aby serwis happinate był bezpieczny, ale nie możemy udzielić takiej gwarancji. Aby dbać o bezpieczeństwo serwisu happinate, potrzebujemy pomocy użytkownika, w tym przestrzegania przez niego następujących postanowień:</p>

<ol>
	<li>Zabronione jest publikowanie nieautoryzowanych komunikat&oacute;w komercyjnych (np. spamu) w serwisie happinate.</li>
	<li>Zabronione jest zbieranie treści i danych użytkownik&oacute;w oraz inne formy uzyskiwania dostępu do serwisu happinate z wykorzystaniem narzędzi automatycznych (np. bot&oacute;w zbierających, robot&oacute;w, pająk&oacute;w lub program&oacute;w kopiujących), bez uprzedniego uzyskania naszego pisemnego zezwolenia.</li>
	<li>Zabronione jest prowadzenie w serwisie happinate nielegalnego marketingu wielopoziomowego, np. piramid finansowych.</li>
	<li>Zabronione jest przesyłanie wirus&oacute;w i złośliwego kodu innego rodzaju.</li>
	<li>Zabronione jest wyłudzanie danych logowania oraz uzyskiwanie dostępu do konta należącego do innej osoby.</li>
	<li>Niedozwolone jest atakowanie, zastraszanie i nękanie innych użytkownik&oacute;w.</li>
	<li>Niedozwolone jest publikowanie treści, kt&oacute;re: promują nienawiść, zawierają groźby lub pornografię, nawołują do przemocy lub zawierają elementy nagości, a także drastyczną i lub nieuzasadnioną przemoc.</li>
	<li>Zabronione jest wykorzystywanie serwisu happinate do cel&oacute;w niezgodnych z prawem, wprowadzających w błąd, złośliwych lub dyskryminacyjnych.</li>
	<li>Zabronione jest wykonywanie jakichkolwiek czynności, kt&oacute;re mogłyby wyłączyć, przeciążyć lub ograniczyć prawidłowe funkcjonowanie oraz wygląd serwisu happinate, takich jak atak powodujący zablokowanie obsługi, ingerowanie w spos&oacute;b wyświetlania strony lub inne funkcje serwisu happinate.</li>
	<li>Zabronione jest ułatwianie naruszania niniejszego Regulaminu i naszych zasad, a także podżeganie do takiego postępowania.</li>
</ol>

<p><strong>Art. 3</strong></p>

<p><strong>REJESTRACJA I BEZPIECZEŃSTWO KONTA</strong></p>

<p>Użytkownicy serwisu happinate podają swoje prawdziwe imiona i nazwiska bądź nazwy w przypadku firm oraz prawdziwe dane. Potrzebujemy pomocy użytkownik&oacute;w, aby utrzymać ten stan rzeczy. Poniżej zawarto przyjęte przez użytkownika zobowiązania względem serwisu happinate, kt&oacute;re dotyczą rejestracji oraz troski o bezpieczeństwo konta:</p>

<ol>
	<li>Zabronione jest podawanie fałszywych danych osobowych w serwisie happinate i tworzenie konta dla innej osoby bądź firmy bez jej pozwolenia.</li>
	<li>Użytkownik może utworzyć tylko jedno konto.</li>
	<li>W przypadku zablokowania konta użytkownik nie ma prawa utworzyć kolejnego bez naszego uprzedniego pisemnego zezwolenia.</li>
	<li>Użytkownik jest zobowiązany dbać o rzetelność i aktualność swoich danych.</li>
	<li>Zabronione jest udostępnianie swojego hasła, zezwalanie innym na uzyskanie dostępu do własnego konta oraz wykonywanie innych czynności, kt&oacute;re mogą spowodować spadek poziomu bezpieczeństwa konta.</li>
	<li>Zabronione jest przekazywanie swojego konta (a także administrowanej przez siebie strony) innej osobie bez wcześniejszego uzyskania naszego zezwolenia w formie pisemnej.</li>
	<li>W przypadku wybrania przez użytkownika nazwy użytkownika lub podobnego identyfikatora dla konta lub strony rezerwujemy sobie prawo do usunięcia lub odebrania go w razie potrzeby (np. jeżeli właściciel znaku towarowego złoży skargę na nazwę użytkownika niezwiązaną z jego faktycznym imieniem/nazwiskiem/nazwą).</li>
</ol>

<p>Zakładanie konta nie jest wymagane aby można było korzystać z wyszukiwarki ofert pracy. Brak konta pozbawi jednak użytkownika możliwości aplikowania na oferty pracy oraz dostępu do historii ofert na kt&oacute;re aplikował, możliwości zdefiniowania powiadomień o dopasowanych ofertach pracy, uproszczenia aplikowania poprzez możliwość korzystania z przycisku &bdquo;1-click&rdquo; itp.</p>

<p>W przypadku aplikowania przez Użytkownika kt&oacute;ry nie posiada konta na jakąkolwiek ofertę pracy, Happinate tworzy temu Użytkownikowi konto w spos&oacute;b automatyczny oraz przesyła na podany w trakcie aplikacji adres e-mail hasło startowe. Od tego momentu użytkownik może zalogować się na swoje konto z użyciem podanego adresu e-mail oraz hasła startowego.</p>

<p>Konto jest r&oacute;wnież zakładane Użytkownikowi w przypadku dodania go do ulubionych przez osobę przeprowadzającą rekrutację. Użytkownik otrzymuje w takim wypadku e-mail z linkiem aktywacyjnym. Po kliknięciu w link Użytkownik może wprowadzić hasło i w ten spos&oacute;b uzyskać aktywację konta. Link aktywacyjny wygasa po upływie 30 dni.</p>

<p><strong>Art. 4</strong></p>

<p><strong>OCHRONA PRAW INNYCH OS&Oacute;B</strong></p>

<p>Szanujemy prawa innych os&oacute;b i tego samego oczekujemy od naszych użytkownik&oacute;w.</p>

<p>Zabronione jest publikowanie w serwisie happinate treści lub wykonywanie jakichkolwiek czynności, kt&oacute;re naruszają lub łamią prawa innej osoby, albo są w inny spos&oacute;b sprzeczne z prawem.</p>

<p>Zastrzegamy sobie prawo do usunięcia dowolnej treści lub informacji opublikowanej przez użytkownika, jeśli uznamy, że jest ona sprzeczna z niniejszym Regulaminem lub naszymi zasadami.</p>

<p>Jeżeli usuniemy treść użytkownika z powodu naruszenia praw autorskich innej osoby i użytkownik uzna nasze działanie za błędne, zapewniamy możliwość odwołania się od decyzji.</p>

<p>Wielokrotne naruszanie praw własności intelektualnej innych os&oacute;b może spowodować zablokowanie konta użytkownika.</p>

<p>Zabronione jest wykorzystywanie naszych praw autorskich lub znak&oacute;w towarowych, a także wszelkich łudząco podobnych oznaczeń, z wyjątkiem przypadk&oacute;w, gdy ich używanie zostało bezpośrednio dopuszczone w wydanym przez nas wcześniej zezwoleniu pisemnym.</p>

<p>W przypadku zbierania informacji od użytkownik&oacute;w, niezbędne jest: uzyskanie ich zgody, wyraźne poinformowanie, że to użytkownik a nie serwis happinate zbiera dane, oraz opublikowanie zasad ochrony prywatności zawierających om&oacute;wienie zbieranych informacji oraz sposobu ich wykorzystania.</p>

<p>Zabronione jest publikowanie w serwisie dowod&oacute;w tożsamości innych os&oacute;b, poufnych informacji o charakterze finansowym oraz innych poufnych informacji, w kt&oacute;rych posiadanie Użytkownik wszedł w związku z wcześniej wykonywaną pracą lub w inny spos&oacute;b się o nich dowiedział.</p>

<p><strong>Art. 5</strong></p>

<p><strong>TELEFONY KOM&Oacute;RKOWE I INNE URZĄDZENIA PRZENOŚNIE</strong></p>

<p>W chwili obecnej świadczymy usługi dla posiadaczy telefon&oacute;w kom&oacute;rkowych bezpłatnie, ale należy pamiętać, że są one obciążone standardowymi stawkami i opłatami operatora, takimi jak opłata za wiadomości SMS czy opłata za korzystanie z Internetu w telefonie.</p>

<p>W przypadku zmiany lub dezaktywacji numeru telefonu kom&oacute;rkowego użytkownik zobowiązuje się do zaktualizowania danych swojego konta na happinate w ciągu 3 dni, aby zapobiec wysyłaniu przeznaczonych dla niego wiadomości do nabywcy jego poprzedniego numeru. W przypadku niezaktualizowania numeru telefonu uważa się, iż wiadomość wysłana na numer poprzedni została skutecznie dostarczona Użytkownikowi.</p>

<p>Użytkownik wyraża zgodę na umożliwienie innym osobom synchronizowania (także za pośrednictwem aplikacji) urządzeń z wszelkimi danymi, do kt&oacute;rych owi użytkownicy mają dostęp za pośrednictwem happinate, i przyznaje wszelkie potrzebne do tego prawa.</p>

<p><strong>Art. 6</strong></p>

<p><strong>PŁATNOŚCI</strong></p>

<p>Dokonanie płatności w serwisie happinate oznacza przyjęcie naszych <a href="http://www.happinate.com/Home/index.php?page=pay-rules">Warunk&oacute;w płatności i publikacji ogłoszeń</a>.</p>

<p><strong>Art. 7</strong></p>

<p><strong>ODPOWIEDZIALNOŚĆ</strong></p>

<ol>
	<li>Strony zobowiązane są do naprawienia szkody, jaką poniosła druga strona na skutek niewykonania lub nienależytego wykonania przez nie obowiązk&oacute;w wynikających z Regulaminu, chyba że ich niewykonanie lub nienależyte wykonanie było następstwem okoliczności, za kt&oacute;re strona nie ponosi odpowiedzialności.</li>
	<li>Serwis happinate nie ponosi odpowiedzialności za jakiekolwiek szkody powstałe na skutek zaprzestania świadczenia usług. Happinate nie jest r&oacute;wnież odpowiedzialne za szkody powstałe na skutek zaprzestania świadczenia usług i usunięcia Konta Użytkownika naruszającego Regulamin.</li>
	<li>Happinate nie ponosi ponadto odpowiedzialności za:
	<ul>
		<li>jakiekolwiek szkody wyrządzone osobom trzecim, powstałe w wyniku korzystania przez Użytkownik&oacute;w z Platformy w spos&oacute;b sprzeczny z Regulaminem lub przepisami prawa,</li>
		<li>za treści udostępniane przez Użytkownik&oacute;w za pośrednictwem Platformy, kt&oacute;re to treści naruszają prawo lub chronione prawem dobra os&oacute;b trzecich,</li>
		<li>informacje oraz materiały pobrane, zamieszczone w Serwisach happinate lub wysyłane za pośrednictwem sieci Internet przez Użytkownik&oacute;w,</li>
		<li>utratę przez Użytkownika danych spowodowanych działaniem czynnik&oacute;w zewnętrznych (np. awaria sprzętu) lub też innymi okolicznościami niezależnymi od happinate (działanie os&oacute;b trzecich),</li>
		<li>szkody wynikłe na skutek braku ciągłości dostarczania Usług, będące następstwem okoliczności, za kt&oacute;re happinate nie ponosi odpowiedzialności (siła wyższa, działania i zaniechania os&oacute;b trzecich, itp.),</li>
		<li>podania przez Użytkownik&oacute;w nieprawdziwych lub niepełnych informacji przy rejestracji Konta,</li>
		<li>nieprzestrzegania przez Użytkownik&oacute;w warunk&oacute;w Regulaminu.</li>
	</ul>
	</li>
	<li>Użytkownik zobowiązuje się zapłacić Happinate r&oacute;wnowartość wszelkich odszkodowań i koszt&oacute;w, jakie na podstawie powszechnie obowiązujących przepis&oacute;w prawa Happinate poniesie na rzecz os&oacute;b trzecich, w związku z takim korzystaniem przez Użytkownika z Happinate, kt&oacute;re narusza niniejszy Regulamin lub przepisy powszechnie obowiązującego prawa.</li>
	<li>Sp&oacute;łka nie ponosi odpowiedzialności w przypadku niezatrudnienia użytkownika Serwisu, ani nie staje się podmiotem stosunku pracy w rozumieniu Kodeksu Pracy, jeśli do zatrudnienia nie doszło z powod&oacute;w niezależnych od happinate. W szczeg&oacute;lności sp&oacute;łka nie ponosi odpowiedzialności za zrezygnowanie z usług Serwisu przez użytkownika oferującego pracę. W tym przypadku sp&oacute;łka nie ponosi r&oacute;wnież żadnych koszt&oacute;w związanych z ewentualnymi wydatkami wiążącymi się z wszczętym procesem zatrudnienia tj. zwrot kwot wydanych na bilety czy inne wydatki związane z dotarciem na miejsce świadczenia pracy, tudzież na miejsce zaplanowanego spotkania w sprawie rozm&oacute;w wstępnych przed ewentualnym zatrudnieniem.</li>
	<li>Sp&oacute;łka ponosi odpowiedzialność za szkody związane z fałszywą treścią ofert zamieszczonych przez użytkownik&oacute;w tylko w sytuacji, gdy powiadomiona o niezgodnej z rzeczywistym stanem rzeczy treści oferty, nie usunęła jej niezwłocznie po otrzymaniu zgłoszenia. Termin niezwłocznie oznacza czas niezbędny do należytego zbadania zgłoszenia oraz dokonania innych niezbędnych działań technicznych w zwykłym toku czynności, nie kr&oacute;tszy niż 7 dni roboczych. W pozostałych przypadkach Sp&oacute;łka jest wolna od jakiejkolwiek odpowiedzialności.</li>
</ol>

<p>Art. 8</p>

<p>DANE OSOBOWE, PLIKI COOKIES I INFORMACJE PRZESYŁANE UŻYTKOWNIKOM</p>

<ol>
	<li>O zakresie danych osobowych udostępnionych Happinate decyduje Użytkownik, poprzez wprowadzanie danych do formularza znajdującego się na stronie internetowej oraz poprzez ustawienia prywatności konta użytkownika. Użytkownik może w każdej chwili rozszerzyć lub ograniczyć zakres udostępnionych danych osobowych.&nbsp; Użytkownik przyjmuje do wiadomości, że wykonanie na jego rzecz usług dostarczanych przez Hapinnate może wymagać wykorzystania jego danych osobowych, oraz że niewykonanie tych usług z powodu nieudostępnienia danych osobowych nie może powodować jakiejkolwiek odpowiedzialności ze strony Happinate.</li>
	<li>Użytkownik ma prawo wyłączyć w używanej przeglądarce internetowej możliwość zapisywania na swoim komputerze plik&oacute;w &bdquo;cookies&rdquo;. W takim wypadku użytkownik zgadza się na ograniczenie dostępności niekt&oacute;rych usług Happinate lub nawet brak możliwości korzystania z tych usług do kt&oacute;rych niezbędne jest wykorzystanie plik&oacute;w &bdquo;cookies&rdquo;.</li>
	<li>Użytkownik ma prawo decydowania o tym jakie wiadomości e-mail będzie otrzymywać od Happinate poprzez ustawienia konta użytkownika, jak r&oacute;wnież poprzez udzielanie oraz odwoływanie stosownych zg&oacute;d na otrzymywanie określonych wiadomości, w szczeg&oacute;lności zawierających informację handlową Happinate lub partner&oacute;w biznesowych.</li>
	<li>Happinate zastrzega sobie prawo do wysyłania Użytkownikom niezapowiedzianych wiadomości, takich jak informacje odnoszące się bezpośrednio do funkcjonowania Portalu (np. zmiany w funkcjonowaniu) bądź też komunikaty systemu (np. informacje o nowych wiadomościach oczekujących na poczcie wewnętrznej, komunikaty o przerwach w działaniu Portalu) i inne wiadomości istotne z punktu widzenia prawnego, technicznego bądź organizacyjnego, związane z funkcjonowaniem usług . W ramach wiadomości powyższych nie będzie przesyłana informacja handlowa.</li>
</ol>

<p><strong>Art. 9</strong></p>

<p><strong>INFORMACJE O REKLAMACH I INNYCH TREŚCIACH KOMERCYJNYCH W SERWISIE</strong></p>

<p>Naszym celem jest udostępnienie takich reklam i treści komercyjnych, kt&oacute;re będą stanowić realną wartość dla naszych użytkownik&oacute;w i reklamodawc&oacute;w. W tym celu użytkownik akceptuje następujące zapisy:</p>

<p>Za pomocą Ustawień prywatności użytkownik może określić, w jaki spos&oacute;b jego imię, nazwisko i zdjęcie profilowe mogą być powiązane z treściami komercyjnymi, sponsorowanymi lub powiązanymi (np. z firmą, kt&oacute;rą użytkownik polubi), kt&oacute;re są obsługiwane lub wzbogacane przez happinate. Użytkownik zezwala happinate na używanie swojego imienia i nazwiska oraz zdjęcia profilowego w połączeniu z tymi treściami zgodnie z określonymi przez niego limitami.</p>

<p>Nie przekazujemy treści ani danych użytkownika reklamodawcom bez jego zgody.</p>

<p><strong>Art. 10</strong></p>

<p><strong>SPECJALNE POSTANOWIENIA DOTYCZĄCE STRON I ZATRUDNIENIA</strong></p>

<p>&nbsp;</p>

<ol>
	<li>Wszyscy Użytkownicy Happinate obowiązani są przestrzegać postanowień niniejszego Regulaminu.</li>
	<li>Happinate świadczy wyłącznie usługi pośrednictwa pracy, polegające na udostępnianiu możliwości prezentowania ogłoszeń i innych ofert przez potencjalnych pracodawc&oacute;w oraz na dostarczeniu instrument&oacute;w komunikacji pomiędzy kandydatem z potencjalnym pracodawcą. Happinate nie staje się stroną um&oacute;w zawieranych przy wykorzystaniu funkcjonalności serwisu i odpowiada za bezprawne działania lub zaniechania stron zawieranych za pomocą serwisu um&oacute;w tylko w zakresie określonym w art. 7 &ndash; ODPOWIEDZIALNOŚĆ.</li>
	<li>Kandydat może aplikować na zawarte w Happinate ogłoszenia za pomocą formularza &bdquo;aplikuj&rdquo; lub za pomocą opcji &bdquo;1-click&rdquo;. Opcja &bdquo;1-click&rdquo; stanowi uproszczoną metodę zawarcia umowy z pracodawcą, z pominięciem procesu rekrutacji.</li>
	<li>Warunki skorzystania z opcji &bdquo;1-click&rdquo; są następujące:
	<ol>
		<li>Kandydat musi posiadać status &bdquo;ulubiony&rdquo;, nadany przez pracodawcę&nbsp; lub osobę dokonującą rekrutacji.</li>
		<li>Warunkiem nadania statusu &bdquo;ulubiony&rdquo; przez pracodawcę jest uprzednia praca dla tego pracodawcy na stanowisku zakwalifikowanym do tej samej kategorii, co stanowisko kt&oacute;rego dotyczy dane ogłoszenie. Pracodawca nadaje status &bdquo;ulubiony&rdquo; na podstawie oceny przebiegu poprzedniego zatrudnienia.</li>
		<li>Kategorie stanowisk określają oznaczenia - &bdquo;Tagi&rdquo;, kt&oacute;re są nadawane zar&oacute;wno ogłoszeniom, jak i kandydatom kt&oacute;rzy już wykonywali pracę na stanowisku należącym do kategorii objętej danym Tagiem. Kandydat uzyskuje status &bdquo;ulubiony&rdquo; tylko w zakresie nadanego mu Taga. Tagi stanowią zbi&oacute;r określony odg&oacute;rnie przez Happinate i zamknięty, składający się z następujących oznaczeń:
		<ul>
			<li>należy wypisać wszystkie przewidywane oznaczenia (praca fizyczna, praca magazyniera itp.)</li>
		</ul>
		</li>
		<li>Aby nawiązać umowę z&nbsp; pracodawcą, kandydat posiadający status &bdquo;ulubiony&rdquo; w zakresie Taga kt&oacute;rym oznaczono dane ogłoszenie, może po zapoznaniu się z warunkami zatrudnienia/świadczenia usług, zgłosić się do pracy z pominięciem procesu rekrutacji. W tym celu powinien wykorzystać dostępny przycisk &bdquo;1-click&rdquo;. Jeżeli przycisk &bdquo;1-click&rdquo; nie jest dostępny dla danego użytkownika, to oznacza, że nie zostały spełnione warunki wykorzystania tego trybu. Wykorzystanie przycisku &bdquo;1-click&rdquo; oznacza akceptację warunk&oacute;w zatrudnienia określonych w ogłoszeniu.</li>
		<li>Status &bdquo;ulubiony&rdquo; może być r&oacute;wnież nadany lub odebrany przez osobę przeprowadzającą rekrutację.</li>
		<li>Użytkownicy zamieszczający ogłoszenia oraz osoby dokonujące rekrutacji zobowiązują się do nienadawania statusu &bdquo;ulubiony&rdquo; z przyczyn innych niż obiektywne i rzeczywiście uzasadnione z perspektywy danego stanowiska pracy. Za przyczyny obiektywne i uzasadnione uważa się przede wszystkim doświadczenie zawodowe, przebieg zatrudnienia, oceny wystawiane przez pracodawc&oacute;w. Bezwzględnie niedozwolone jest nadawanie lub nienadawanie statusu &bdquo;ulubiony&rdquo; z przyczyn takich jak płeć, wiek, niepełnosprawność, rasa, religia, pochodzenie etniczne, narodowość, orientacja seksualną, przekonania polityczne i wyznanie, ani ze względu na przynależność związkową os&oacute;b, dla kt&oacute;rych poszukuje się zatrudnienia lub innej pracy zarobkowej. Naruszenie niniejszego postanowienia powoduje odpowiedzialność odszkodowawczą osoby dopuszczającej się naruszenia, kt&oacute;ra obejmuje wszelkie utracone korzyści&nbsp; oraz poniesione przez Happinate koszty z tego tytułu, w tym w ramach odpowiedzialności cywilnej, karnej, za wykroczenia czy administracyjnej.</li>
	</ol>
	</li>
	<li>O nadaniu/odebraniu statusu &bdquo;ulubiony&rdquo;, Użytkownik będzie każdorazowo powiadamiany drogą mailową, a po zainstalowaniu odpowiedniej aplikacji i skorzystaniu w tym celu z dostępnych opcji, także poprzez powiadomienia wysyłane na urządzenia mobilne.</li>
	<li>Użytkownik ma prawo upowszechniania i dalszego publikowania/przekazywania ofert zamieszczanych&nbsp; na Happinate, w tym przede wszystkim poprzez środki indywidualnego porozumiewania się na odległość lub ich publikację w serwisach społecznościowych.</li>
</ol>

<p>&nbsp;</p>

<p><strong>Art. 11</strong></p>

<p><strong>SPECJALNE POSTANOWIENIA DOTYCZĄCE OPROGRAMOWANIA</strong></p>

<p>W przypadku pobrania oprogramowania happinate użytkownik wyraża zgodę na pobieranie od nas od czasu do czasu nowych wersji, aktualizacji i dodatkowych funkcji przez oprogramowanie w celu jego usprawnienia, wzbogacenia lub rozwinięcia w inny spos&oacute;b.</p>

<p>Użytkownik nie będzie modyfikował, tworzył prac pochodnych i dekompilował oprogramowania ani w żaden inny spos&oacute;b nie będzie pr&oacute;bował wydobyć kodu źr&oacute;dłowego happinate, chyba że otrzyma od Sp&oacute;łki wyraźną zgodę w postaci licencji na oprogramowanie typu open source lub pisemnego zezwolenia.</p>

<p><strong>Art. 12</strong></p>

<p><strong>POPRAWKI</strong></p>

<p>W przypadku zmiany niniejszego Regulaminu użytkownicy zostaną powiadomieni o niej na siedem (7) dni przed jej planowanym wprowadzeniem (na przykład poprzez opublikowanie jej na stronie lub poprzez przesłanie nowej wersji Regulaminu na adres e-mail podany podczas rejestracji) i otrzymają możliwość przekazania happinate opinii na jej temat (z wyjątkiem zmian umotywowanych względami prawnymi lub administracyjnymi albo potrzebą skorygowania błędnej informacji).</p>

<p>Dalsze korzystanie z serwisu happinate po wprowadzeniu zmian jest r&oacute;wnoznaczne z akceptacją zmienionych warunk&oacute;w.</p>

<p><strong>Art. 13</strong></p>

<p><strong>ZAKOŃCZENIE ŚWIADCZENIA USŁUG</strong></p>

<p>W przypadku naruszenia przez użytkownika litery lub ducha niniejszego Regulaminu albo narażenia Sp&oacute;łki na ryzyko odpowiedzialności prawnej w inny spos&oacute;b zastrzegamy sobie prawo do zaprzestania udostępniania użytkownikowi całości lub części usług serwisu happinate. Użytkownik zostanie o tym powiadomiony pocztą e-mail lub w postaci komunikatu przy następnej pr&oacute;bie uzyskania dostępu do konta. Użytkownik r&oacute;wnież ma prawo do usunięcia swojego konta lub wyłączenia swojej aplikacji w dowolnej chwili.</p>

<p>Zakończenie świadczenia usług przez Happinate następuje po rozwiązaniu umowy przez Użytkownika. Rozwiązanie umowy jest dobrowolne i może nastąpić w każdym czasie za pomocą formularza dostępnego na stronie <a href="http://www.happinate.com/">www.happinate.com</a>. Rozwiązanie umowy o świadczenie usług drogą elektroniczną skutkuje usunięciem konta użytkownika.</p>

<p><strong>Art. 13</strong></p>

<p><strong>SPORY</strong></p>

<p>Użytkownik zobowiązany jest do rozwiązywania wszelkich roszczeń, podstaw pow&oacute;dztwa lub spor&oacute;w (roszczenia) w stosunku do nas wynikających z niniejszego Regulaminu wyłącznie we właściwym miejscowo Sądzie Rejonowym w Poznaniu. Zastrzeżenie powyższe nie dotyczy użytkownik&oacute;w korzystających z Happinate w celach nie związanych z prowadzoną działalnością gospodarczą lub zawodową. Zapisy niniejszego Oświadczenia, a także wszelkie spory pomiędzy serwisem a użytkownikiem, podlegają przepisom prawa polskiego, bez względu na przepisy prawa kolizyjnego.</p>

<p>Dokładamy wszelkich starań, aby serwis happinate działał prawidłowo, bezbłędnie i bezpiecznie, ale użytkownik korzysta z niego na własną odpowiedzialność. Udostępniany serwis happinate w takim stanie, w jakim jest, bez jakiejkolwiek bezpośredniej lub dorozumianej gwarancji, w tym domniemanej zbywalności, przydatności do określonego celu i nienaruszalności praw. Nie gwarantujemy, że serwis happinate zawsze będzie bezpieczny i wolny od błęd&oacute;w, ani że zawsze będzie działał bez zakł&oacute;ceń, op&oacute;źnień i nieprawidłowości happinate nie ponosi odpowiedzialności za czynności, treści, informacje i dane stron trzecich. Użytkownik zwalnia happinate, naszych Dyrektor&oacute;w, Przedstawicieli, Pracownik&oacute;w i Agent&oacute;w od wszelkiej odpowiedzialności za roszczenia i szkody jawne lub ukryte wynikające z jakichkolwiek roszczeń użytkownika wobec stron trzecich lub związane z takimi roszczeniami. Nie ponosimy odpowiedzialności za utratę przez użytkownika zysk&oacute;w ani inne straty wt&oacute;rne, specjalne, pośrednie lub przypadkowe wynikające z niniejszego oświadczenia lub korzystania z serwisu happinate lub związane z nimi, nawet jeżeli mieliśmy świadomość możliwości ich wystąpienia. Nasza łączna odpowiedzialność za szkody wynikające z niniejszego oświadczenia lub korzystania z serwisu happinate nie przekracza kwoty stu dolar&oacute;w (100 USD) lub kwoty, kt&oacute;rą Użytkownik zapłacił firmie happinate w ciągu ostatnich dwunastu miesięcy. Obowiązujące przepisy mogą nie zezwalać na ograniczenie lub wyłączenie odpowiedzialności lub odszkodowania za straty przypadkowe i wt&oacute;rne, więc powyższe ograniczenie lub wyłączenie może nie mieć zastosowania do Użytkownika. W takich przypadkach odpowiedzialność serwisu happinate jest ograniczona w maksymalnym dozwolonym przez stosowne przepisy zakresie.</p>

<p><strong>Art. 14</strong></p>

<p><strong>INNE</strong></p>

<p>Niniejszy Regulamin, wraz z &bdquo;Warunkami płatności i publikacji ogłoszeń w serwisie happinate&rdquo; stanowiącymi jego integralną część, stanowią całość umowy dotyczącej serwisu happinate pomiędzy stronami i zastępują wszelkie wcześniejsze uzgodnienia.</p>

<p>W przypadku uznania za nieobowiązującą jakiejkolwiek części niniejszego Regulaminu, pozostała część nadal obowiązuje.</p>

<p>Niemożność wyegzekwowania niniejszego Regulaminu nie jest uznawana za jego uchylenie.</p>

<p>Jakiekolwiek zmiany lub odstępstwa od niniejszego Regulaminu muszą być dokonane na piśmie i podpisane przez nas.</p>

<p>Zabronione jest przekazywanie jakichkolwiek praw lub obowiązk&oacute;w wynikających z niniejszego Regulaminu innym osobom bez naszej zgody.</p>

<p>Wszystkie nasze prawa i obowiązki wynikające z niniejszego Regulaminu mogą zostać przez&nbsp;Nas&nbsp;swobodnie przeniesione w przypadku fuzji, przejęcia lub sprzedaży zasob&oacute;w albo innej czynności.</p>

<p>Żadne postanowienia niniejszego Regulaminu nie zwalniają nas od przestrzegania przepis&oacute;w prawa.</p>

<p>Niniejszy Regulamin nie daje żadnych praw stronom trzecim.</p>

<p>Zastrzegamy wszelkie prawa, kt&oacute;re nie zostały wyraźnie przyznane użytkownikom.</p>

<p>Użytkownik jest zobowiązany stosować się do wszelkich przepis&oacute;w prawa podczas korzystania z serwisu happinate lub uzyskiwania dostępu do niego.</p>

<p>&nbsp;</p>

<hr />
	</div>
    
    <div class="right-column">
    	<!--<h2>Jak to działa?</h2>
   <a href="#videoModal" role="button" data-toggle="modal" class="video-works"><img src="img/video-bg.png" alt="Jak to działa?"></a>
   <h2 style="margin-top:34px;">Pobierz aplikację</h2>
   <a href="#" class="app-btn"></a>
   <a href="#" class="android-btn"></a>
   <div class="clear"></div>-->
		
		<div class="simple-fb-like">
			<div class="fb-like-box" data-href="https://www.facebook.com/happinate" data-width="292" data-height="500" data-show-faces="true" data-stream="false" data-show-border="false" data-header="false"></div>
		</div>
    </div>
    
    <div class="clear"></div>

{/block}