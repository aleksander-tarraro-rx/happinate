{extends file="index.tpl"}

{block name=body}
	
    <div class="left-column simple-text">
    	<h2 style="text-align:center;">Warunki płatności i publikacji ogłoszeń w serwisie happinate</h2>
    	<p>Publikacja ogłoszeń w serwisie happinate jest płatna zgodnie z cennikiem zamieszczonym na stronie <a href="http://www.happinate.com/Home/Prices">www.happinate.com/cennik</a>. Happinate zastrzega sobie prawo do wprowadzenia zmian w cenniku w każdym momencie. Zmienione warunki płatności będą obowiązywać Ogłoszeniodawców z momentem publikacji nowej wersji niniejszych Warunków na stronie <a href="http://www.happinate.com/Home/Prices">www.happinate.com/cennik</a>.</p>
    	<p>Chcemy mieć pewność, że happinate jest dla każdego z naszych Użytkowników serwisem możliwie jak najbardziej przejrzystym i praktycznym. Ogłoszenia, które nie spełniają zasad zamieszczania przedstawionych w niniejszych Warunkach, mogą zostać w każdym czasie usunięte z naszych stron.</p>
    	<p>Ogłoszeniodawca jest wyłącznie odpowiedzialny za wszelkie informacje, które publikuje w serwisie happinate i ewentualne skutki, które mogą wynikać z publikacji jego ogłoszenia. Ogłoszeniodawcy, którzy regularnie naruszają Warunki, mogą się spodziewać ograniczeń w dalszym korzystaniu z serwisu, w tym usunięcia konta.</p>
    	<p>Podawana w serwisie stawka musi być stawką brutto w przeliczeniu za godzinę pracy. Podawane w ogłoszeniu pozostałe istotne z punktu widzenia Użytkownika informacje (nazwa stanowiska, nazwa pracodawcy, zadania do wykonania na danym stanowisku, rodzaj umowy, adres miejsca pracy, dane kontaktowe do Ogłoszeniodawcy) bezwzględnie muszą odpowiadać faktycznym warunkom zatrudnienia oferowanym przez Ogłoszeniodawcę.</p>
    	<p><strong>Serwis happinate zastrzega sobie prawo do odmowy publikacji ogłoszenia lub usunięcia go zgodnie z poniższymi zasadami.</strong></p>
    	<p>Mogą nie zostać opublikowane, bądź mogą w każdym momencie zostać usunięte ogłoszenia, które naszym zdaniem są niezgodne z Warunkami, w szczególności ogłoszenia, które:</p>
    	<ul>
			<li>prezentują nieprawdziwe stawki oferowanych wynagrodzeń</li>
			<li>prezentują nieprawdziwe inne ważne informacje dotyczące oferty (nazwa stanowiska, nazwa pracodawcy, zadania do wykonania na danym stanowisku, rodzaj umowy, adres miejsca pracy, dane kontaktowe do Ogłoszeniodawcy)</li>
			<li>nie są ofertami pracy</li>
			<li>są anonsami naruszającymi dobre obyczaje</li>
			<li>są anonsami o podtekście erotycznym</li>
			<li>zawierają wulgaryzmy</li>
			<li>szerzą nienawiść, przemoc, propagują zasady niezgodne z ogólnie przyjętymi zasadami współżycia społecznego</li>
			<li>zawierają niestosowne zdjęcia</li>
			<li>wskazują na dyskryminację ze względu na wiek, płeć, wyznanie, narodowość, rasę</li>
    	</ul>
		<p>Zastrzegamy sobie również prawo wedle własnego uznania do czasowego lub stałego ograniczenia prawa Użytkownika, który publikuje tego rodzaju ogłoszenia, do korzystania ze strony lub odmówienia mu prawa do rejestracji w serwisie.</p>
    	<p>Prosimy Użytkowników o zgłaszanie nam potencjalnie nieuczciwych anonsów lub Ogłoszeniodawców przez formularz znajdujący się na stronie www.happinate.com, w zakładce Pomoc, podając numer referencyjny ogłoszenia.</p>
    	<p>W powyższych przypadkach Klientowi nie przysługuje zwrot ceny za publikację ogłoszenia.</p>
    	<p><strong>Serwis zastrzega sobie prawo do wstrzymania emisji ogłoszenia wedle własnego uznania.</strong></p>
    	<p>Emisja ogłoszenia może się opóźnić w czasie jeśli dane ogłoszenie zostało nam zgłoszone jako potencjalnie niezgodne z Warunkami.</p>
    	<p>Serwis happinate może też samodzielnie wstrzymać publikację ogłoszenia zawierającego informacje niedostosowane do opisów poszczególnych pól formularza (np. podanie numeru telefonu w nazwie stanowiska itp.).</p>
    	<p>W takich przypadkach anons może zostać czasowo zawieszony, aż do momentu sprawdzenia go przez moderatorów i późniejszego poprawienia przez Użytkownika. Zgłoszone anonse sprawdzamy najszybciej jak to możliwe. Jeżeli okaże się, że ogłoszenie nie złamało żadnego z Warunków, niezwłocznie pojawi się ono na stronie.</p>
    	<p>Jeżeli Twoje ogłoszenie nie zostało opublikowane, a Twoim zdaniem nie zaistniała żadna z powyższych okoliczności, prosimy o wysłanie maila ze swoim stanowiskiem na adres hi@happinate.com, a my przyjrzymy się sprawie i postaramy się niezwłocznie odpowiedzieć na Twoje wątpliwości.</p>
    	<p><strong>Postanowienia końcowe.</strong></p>
    	<p>W sprawach nieuregulowanych niniejszymi Warunkami płatności i publikacji ogłoszeń zastosowanie mają przepisy kodeksu cywilnego, kodeksu pracy oraz ustawy o świadczeniu usług drogą elektroniczną.</p>
    	<p>Zaakceptowanie Regulaminu i rejestracja w serwisie happinate oznacza akceptację warunków płatności i publikacji ogłoszeń.</p>
    	<p>Uiszczanie płatności określonych w niniejszych Warunkach będzie konieczne wyłącznie dla dokonania publikacji ogłoszenia w serwisie. Użytkownik nie dokonujący publikacji nie jest zobowiązany do uiszczania opłat za samo posiadanie konta.</p>
    	<p>W przypadku zaistnienia sporów związanych z wykonywaniem niniejszych Warunków, Użytkownicy serwisu, jak i administrator, czyli happinate, zobowiązują się do ich polubownego załatwienia, a jeśli i taki sposób nie przyniesie oczekiwanych rezultatów sądem właściwym do rozstrzygania sporów będzie sąd właściwy ze względu na siedzibę happinate, tj. Sąd Rejonowy w Poznaniu.</p>
    </div>
    
    <div class="right-column">
    	<!--<h2>Jak to działa?</h2>
        <a href="#videoModal" role="button" data-toggle="modal" class="video-works"><img src="img/video-bg.png" alt="Jak to działa?"></a>
        <h2 style="margin-top:34px;">Pobierz aplikację</h2>
        <a href="#" class="app-btn"></a>
        <a href="#" class="android-btn"></a>
        <div class="clear"></div>-->
		
		<div class="simple-fb-like">
			<div class="fb-like-box" data-href="https://www.facebook.com/happinate" data-width="292" data-height="500" data-show-faces="true" data-stream="false" data-show-border="false" data-header="false"></div>
		</div>
    </div>
    
    <div class="clear"></div>

{/block}