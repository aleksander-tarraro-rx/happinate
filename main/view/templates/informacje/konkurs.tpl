{extends file="index.tpl"}

{block name=body}
    <div style="line-height: 200%">
    <h1>Postanowienia ogólne</h1>

<p>1.Niniejszy Regulamin określa zasady oraz warunki przeprowadzenia oraz udziału w konkursie, którego 

celem jest wyłonienie najlepszej odpowiedzi w ocenie jury na zadane pytanie konkursowe. </p>

<p>2.Konkurs przeprowadzany jest w formie elektronicznej, za pomocą e-mail/formularza znajdującego 

się na stronie internetowej www.happinate.com, dostępnego z podstrony konta użytkownikaKandydata (Strona Internetowa).</p>

<p>3.Organizatorem konkursu jest Happinate Sp. z o.o., ul. Ratajczaka 42/13, 61-816 Poznań, wpisaną do 

rejestru przedsiębiorców Krajowego Rejestru Sądowego przez Sąd Rejonowy Poznań Nowe Miasto i Wilda 

w Poznaniu, VIII Wydział Gospodarczy, pod numerem KRS 0000444418.</p>

<p>4.Czas trwania konkursu zostaje określony od 11.09 2013 r. godz. 8:00 do 15.10.2013 r. godz. 

23:59. </p>

  <h1>Uczestnictwo w konkursie</h1>

<p>1.Uczestnikiem konkursu może zostać każda pełnoletnia osoba fizyczna posiadająca pełną zdolność do 

czynności prawnych. </p>

<p>2.Warunkiem udziału w konkursie jest dokonanie rejestracji na stronie internetowej Organizatora 

(www.happinate.com) poprzez założenie konta użytkownika jako „Kandydat”, czyli osoba 

poszukująca zatrudnienia. Po dokonaniu rejestracji, użytkownik otrzyma wiadomość e-mail zawierającą

wskazówki co do dalszego udziału w konkursie.</p>

<p>3.Z konkursu wykluczone są osoby, które po dokonaniu zgłoszenia swojej odpowiedzi, a przed 

rozstrzygnięciem konkursu wyrejestrują konto użytkownika.</p>

<p>4.Odpowiedź na zadanie konkursowe musi zostać przesłana do Organizatora za pomocą służącego do 

tego celu formularza dostępnego na Stronie Internetowej/za pomocą poczty elektronicznej na wskazany 

przez Organizatora adres e-mail oraz w sposób określony w otrzymanej przez użytkownika wiadomości 

informacyjnej. Odpowiedzi niespełniające tych warunków zostaną odrzucone, a odpowiedź Uczestnika nie 

zostanie zakwalifikowana do oceny jury. </p>

<p>5.Uczestnikami konkursu, nie mogą być pracownicy oraz zaangażowani w realizację konkursu 

współpracownicy Happinate sp. z o.o. jak również ich krewni i powinowaci w linii prostej oraz osoby 

pozostające w faktycznym pożyciu.</p>

<p>6. Uczestnik konkursu, dokonując rejestracji na stronie internetowej Organizatora, obowiązany jest 

zastosować się do postanowień znajdującego się tam Regulaminu, określającego zasady świadczenia 

usług przez Organizatora oraz prawa i obowiązki użytkowników. Stwierdzenie niezastosowania się przez 

Użytkownika do treści Regulaminu w procesie rejestracji lub użytkowania konta użytkownika stanowi 

podstawę do odmowy wydania nagrody.</p>

<h1>Zasady konkursu i głosowanie</h1>

<p>1.Konkurs polega na udzielenie odpowiedzi na pytanie konkursowe, które brzmi: </p>

<p>2.Zaprezentowana odpowiedź zostanie poddana ocenie jury. Oceniana będzie kreatywność Uczestnika 

oraz pomysłowość (nieszablonowość) odpowiedzi. Ocena jury ma charakter subiektywny. Jurorzy 

oceniają odpowiedzi zgodnie z własnym przekonaniem, mając na względzie wyżej wskazane kryteria.</p>

<p>3.Laureat konkursu musi zostać zidentyfikowany przed przekazaniem nagrody w oparciu o oryginalny 

dokument tożsamości, aby dowieść, że dane rejestracyjne są prawidłowe.</p>

<h1>Nagrody</h1>

<p>1.Laureaci nagród zostają wyłonieni przez Organizatora w dniu 15.10.2013r. O przyznaniu nagrody, 

laureaci zostaną poinformowani za pomocą poczty elektronicznej.</p>

<p>2.Nagrodą główną w konkursie jest Tablet MANTA MID802</p>

<p>3.Laureatom konkursu nie przysługuje prawo wymiany nagrody na ekwiwalent pieniężny.</p>

<p>4.W celu odebrania nagrody, Laureat zobowiązany jest postępować zgodnie ze wskazówkami 

Organizatora, które otrzyma drogą elektroniczną wraz z wiadomością o wygranej, oraz podjąć Nagrodę 

w wyznaczonym terminie nie krótszym niż 14 dni od dnia otrzymania w/w wiadomości. Jeżeli Laureat 

nie podejmie nagrody w terminie wyznaczonym, rozwiązanie Konkursu ulega unieważnieniu, a Nagroda 

uznawana jest za nieprzyznaną.</p>

<p>5.Organizator może odmówić przyznania nagrody, jeśli Uczestnik konkursu naruszył  warunki 

uczestnictwa w konkursie.</p>

<p>6.Organizator nie odpowiada za brak możliwości kontaktu z Laureatem, jeżeli okoliczności takie nie

wynikają z przyczyn leżących po stronie Organizatora.</p>

<p>7.Każda Nagroda obejmować będzie również nagrodę pieniężną w wysokości 11,11 % wartości nagrody w 

postaci tabletu Tablet MANTA MID802 (dodatkowa nagroda pieniężna), która przeznaczona będzie na zapłatę 

podatku dochodowego od wygranej w Konkursie obciążającego osobę nagrodzoną. Organizator jako 

podmiot wydający Nagrodę będzie płatnikiem zryczałtowanego podatku dochodowego od Nagrody i 

potrąci kwotę należnego podatku dochodowego z ww. dodatkowej nagrody pieniężnej przy wydaniu 

Nagrody. Obowiązek odprowadzenia kwoty podatku do właściwego urzędu skarbowego obciąża 

Organizatora.</p>

<h1>Przetwarzanie danych</h1>

<p>1.Dane osobowe każdego Uczestnika są zbierane i przetwarzane w zakresie i celach niezbędnych do 

przeprowadzenia konkursu, a w szczególności w celu komunikacji z Uczestnikami oraz wydawania Nagród 

Laureatom konkursu.</p>

<h1>Postanowienia końcowe</h1>

<p>1.Treść niniejszego Regulaminu dostępna będzie w siedzibie Organizatora, a dla zarejestrowanych 

użytkowników-Kandydatów, także na Stronie Internetowej.</p>

<p>2.Wyboru Laureata oraz wszelkich decyzji związanych z przebiegiem konkursu dokonuje jury w składzie: 

Aleksandra Tokarewicz, Jagoda Pieścicka, Piotr Chabzda. Decyzje jury są ostateczne, a uczestnikom 

konkursu nie przysługuje prawo odwołania.</p>

<p>3.Organizator zastrzega sobie prawo do zmiany Regulaminu. Zmiany Regulaminu będą dostępne w 

siedzibie Organizatora oraz za pośrednictwem Strony Internetowej. Zmiany Regulaminu w żadnym 

wypadku nie mogą wpływać na sytuację prawną uczestników, którzy wzięli udział w konkursie przed 

dokonaniem zmiany Regulaminu.</p>
</div>
{/block}