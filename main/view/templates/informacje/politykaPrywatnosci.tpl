{extends file="index.tpl"}

{block name=body}
   
    <div class="left-column simple-text">
    	<h2 style="text-align:center;">Polityka prywatności</h2>
    	<p style="text-align:center;"><strong>Art. 1</strong></p>
    	<p style="text-align:center;"><strong>DANE OSOBOWE</strong></p>
    	<p>Happinate Sp. z o.o. (zwana dalej &bdquo;Spółką&rdquo; lub &bdquo;Serwisem&rdquo;) dokłada wszelkich starań, aby chronić prywatność użytkowników posiadanych przez Spółkę serwisów (zwanych dalej &bdquo;Platformą&rdquo;), oferowanych pod adresem <a href="http://www.happinate.com/">www.happinate.com</a> oraz innymi adresami, które są własnością Spółki.</p>
    	<p>Spółka przestrzega stosownych przepisów prawa w zakresie ochrony danych osobowych oraz chroni dane użytkowników od strony technicznej. Spółka w każdym momencie służy poradami i wyjaśnieniami dotyczącymi zasad bezpieczeństwa podczas korzystania z jej serwisów. Spółka oferuje szeroki wachlarz ustawień prywatności i np. użytkownik będzie mógł zdefiniować kto widzi jego wpisy, jak często otrzymuje informacje itp. zachęca do zapoznania się z opcjami ustawień prywatności, które znajdują się w profilu Użytkownika oraz do określenia poziomu dostępu do publikowanych przez użytkownika informacji. W razie jakichkolwiek wątpliwości zapraszamy do kontaktu z Biurem Obsługi Klienta.</p>
    	<p>Spółka nigdy nie przekazuje danych Użytkowników żadnym podmiotom do tego nieuprawnionym.</p>
    	<p>Dane Użytkowników są składowane na odpowiedniej klasy sprzęcie w odpowiednio zabezpieczonych centrach przechowywania informacji, do których dostęp mają tylko upoważnione osoby. System techniczny Spółki jest zbudowany w taki sposób, by zminimalizować ryzyko utraty jakichkolwiek danych na wypadek awarii wywołanych czynnikami zewnętrznym.</p>
    	<p>W razie wykrycia nieprawidłowości lub zagrożeń powodowanych przez osoby nierespektujące przepisów prawa i regulacji obowiązujących w serwisach Spółki współpracujemy z policją, prokuraturą, sądami, by pomóc w ujęciu osób dopuszczających się przestępstw i zapewnić użytkownikom jeszcze większe bezpieczeństwo.</p>
    	<p>Happinate dba o bezpieczeństwo udostępnianych nam danych osobowych, w szczególności przed dostępem osób nieupoważnionych.</p>
    	<p>Happinate zapewnia wszystkim zarejestrowanym Użytkownikom realizację uprawnień wynikających z ustawy z dnia 29 sierpnia 1997 roku o ochronie danych osobowych (tekst jednolity Dz. U. z 2002 r. Nr 101, poz. 926 z późn. zm.), w szczególności prawo wglądu do własnych danych osobowych, prawo żądania aktualizacji i usunięcia danych osobowych oraz prawo wniesienia sprzeciwu w przypadkach określonych w przepisach tej ustawy. Zbiór danych osobowych prowadzony przez Happinate został zgłoszony do rejestracji Generalnemu Inspektorowi Ochrony Danych Osobowych. Administratorem danych (przekazanych w formularzu rejestracyjnym) w rozumieniu ustawy o ochronie danych osobowych jest Spółka .</p>
    	<p>Happinate nie udostępnia ani nie przekazuje wbrew prawu podmiotom trzecim (w szczególności reklamodawcom) jakichkolwiek danych osobowych posiadanych w ramach bazy danych osobowych Serwisu. Jeśli takie dane zostają udostępnione, dzieje się to jedynie po uprzednim uzyskaniu wyraźnej zgody użytkownika na przekazanie jego danych. Przekazanie ww. informacji następuje również na żądanie uprawnionych do tego organów państwa, w szczególności po przedstawieniu stosownych dokumentów potwierdzających potrzebę posiadania tych danych do prowadzonych postępowań.</p>
    	<p>Niezależnie od powyższego, jeżeli korzystasz z usług lub produktów płatnych oferowanych przez happinate, Twoje dane osobowe mogą zostać użyte w celu dokonania rozliczeń oraz rozstrzygania sporów z nimi związanych.</p>
    	<p>Zwracamy też uwagę na fakt, że Twoja aktywność w miejscach publicznych w Serwisie, np. na forum, sprawia, że udostępniasz informacje o sobie także osobom spoza Twojej listy kontaktów, a dokładniej - wszystkim, którzy te strony odwiedzają. Informacje udostępnione podczas tej aktywności nie podlegają Polityce Prywatności.</p>
    	<p style="text-align:center;"><strong>Art. 2</strong></p>
    	<p style="text-align:center;"><strong>ZBIERANE INFORMACJE</strong></p>
    	<p>Zbierane przez Spółkę informacje o użytkownikach serwisów Spółki służą jak najlepszemu zaspokojeniu oczekiwań użytkowników oraz ciągłemu podnoszeniu jakości oferowanych usług oraz wprowadzaniu nowych funkcjonalności. Zbieramy następujące rodzaje informacji:</p>
    	<ul>
    		<li>Informacje podane przez użytkownika:
				<p>Informacje, które użytkownik powierza Spółce, mogą zawierać dane osobowe (między innymi takie jak adres email, imię, nazwisko), dane o historii edukacji i przebiegu pracy zawodowej, dane dotyczące odbytych kursów oraz szkoleń, nabytych uprawnień, oraz informacje o wymaganiach odnośnie przyszłej pracy lub inne dane określone w formularzu rejestracyjnym lub podane przez użytkownika w trakcie korzystania z usług. Wszystkie te dane są podawane dobrowolnie a Spółka określa dane, których podanie jest niezbędne w celu świadczenia usług.</p>
			</li>
    		<li>Uwagi przekazane za pośrednictwem formularza:
				<p>użytkownik ma możliwość przekazania Spółce swoich opinii na temat funkcjonowania serwisów. Informacje te są wykorzystywane w celu usprawnienia działania Serwisów.</p>
			</li>
    		<li>Informacje zbierane automatycznie:
				<p>Spółka zbiera i przechowuje dane dotyczące sposobu korzystania przez użytkownika z serwisów Spółki. Zbierane dane są wykorzystywane do rozszerzenia funkcjonalności oferowanych przez Spółkę Użytkownikowi. Zbierane dane to:</p>
				<ol start="1">
					<li>adres IP używany przez użytkownika, lokalizacja, typ i wersja przeglądarki, system operacyjny, rozdzielczość ekranu, zainstalowane rozszerzenia – w celach statystycznych oraz do sprawniejszego rozwiązywania zgłaszanych problemów;</li>
					<li>odwiedzane przez użytkownika poszczególne strony Serwisu, ilość spędzanego na każdej z nich czasu – w celach statystycznych;</li>
					<li>wpisywane przez użytkownika w wyszukiwarkę ofert frazy i wybierane z filtrów wyszukiwania kryteria – w celu ułatwienia użytkownikowi przeprowadzania kolejnych wyszukiwań;</li>
					<li>oglądane przez użytkownika oferty oraz oferty, na które użytkownik wysłał aplikacje, w tym oferty, które użytkownik przyjął, korzystając z opcji 1-click – w celu rekomendowania użytkownikowi ofert jak najlepiej odpowiadających jego preferencjom.</li>
				</ol>
			</li>
    		<li>Informacje zbierane automatycznie przez inne systemy:
				<p>Spółka umożliwia zbieranie danych o aktywnościach użytkownika na stronach serwisów Spółki obcym systemom w celach statystycznych (np. Google Analytics), reklamowych (np. Google AdSense) lub oferowania dodatkowych funkcjonalności (np. przycisk &bdquo;Lubię to!&rdquo; serwisu facebook).</p>
			</li>
   		</ul>
    	<p style="text-align:center;"><strong>Art. 3</strong></p>
    	<p style="text-align:center;"><strong>PLIKI &bdquo;COOKIES&rdquo;</strong></p>
    	<p>Pliki &bdquo;cookies&rdquo; to pliki tekstowe zapisywane przez przeglądarkę na dysku komputera użytkownika, w celu przechowywania informacji służących do identyfikowania użytkownika bądź zapamiętywania historii działań podejmowanych przez użytkownika na stronach serwisu.</p>
    	<p>Pliki &bdquo;cookies&rdquo; są wykorzystywane w celu identyfikowania użytkownika i poprawnego przypisywania mu danych historycznych, zebranych podczas poprzednich wizyt w serwisach Spółki. W sekcji Pomocy większości przeglądarek można znaleźć dokładne instrukcje jak zablokować zapisywanie przez przeglądarkę tego rodzaju plików. Spółka rekomenduje nieblokowanie tej funkcji, ponieważ dzięki niej użytkownik może lepiej wykorzystywać oferowane przez serwisy Spółki funkcje.</p>
    	<p>Pliki Cookies happinate nie są szkodliwe ani dla Ciebie ani dla Twojego komputera i danych, dlatego zalecamy niewyłączanie ich obsługi w przeglądarkach.</p>
    	<p>Happinate nie odpowiada za zawartość plików cookies wysyłanych przez inne strony internetowe, do których linki umieszczone są na stronach portalu przez Użytkowników.</p>
    	<p>Korzystamy także ze standardowych plików dziennika serwera sieciowego do liczenia osób odwiedzających Portal oraz do oceny jego możliwości technicznych. Używamy tych informacji, aby ustalić, ile osób odwiedza Portal, by zaaranżować strony w sposób najbardziej przyjazny dla Użytkownika i sprawić, żeby Portal był prostszy i bardziej użyteczny w obsłudze. Danych zbieranych w ten sposób, tj. automatycznie, nie można ani zmienić ani usunąć.</p>
    	<p style="text-align:center;"><strong>Art. 4</strong></p>
    	<p style="text-align:center;"><strong>APLIKOWANIE NA OFERTY W TRYBIE &bdquo;APLIKUJ&rdquo;</strong></p>
    	<p>Spółka świadczy pracodawcom usługę polegającą na zbieraniu aplikacji kandydatów za pośrednictwem formularza Aplikuj. Korzystając z formularza Aplikuj użytkownik wyraża zgodę na przetwarzanie swoich danych osobowych przez Spółkę w celu wsparcia procesu rekrutacji oraz na przekazanie danych pracodawcy.</p>
    	<p>Spółka dokłada wszelkich starań, aby aplikacja użytkownika została dostarczona pracodawcy w sposób bezpieczny, pewny i szybki.</p>
    	<p>W trakcie trwania rekrutacji Spółka ma prawo przesyłać użytkownikowi – o ile wyrazi on zgodę – informacje handlowe. Zebrane dane (oferta, na którą aplikacja została wysłana, data aplikowania, kryteria wykorzystane do wyszukania oferty) będą wykorzystywane przez Spółkę do celów statystycznych, w celu budowania rekomendacji dla użytkowników o podobnym profilu oraz do określania preferencji użytkownika, który pozostanie rozpoznawalny dla serwisów Spółki dzięki plikom &bdquo;cookies&rdquo;.</p>
    	<p style="text-align:center;"><strong>Art. 5</strong></p>
    	<p style="text-align:center;"><strong>APLIKOWANIE NA OFERTY W TRYBIE &bdquo;1-CLICK"</strong></p>
    	<p>Spółka nie ponosi odpowiedzialności w przypadku niezatrudnienia użytkownika Serwisu, ani nie staje się podmiotem stosunku pracy w rozumieniu Kodeksu Pracy, jeśli do zatrudnienia nie doszło z powodów niezależnych od happinate. W szczególności spółka nie ponosi odpowiedzialności za zrezygnowanie z usług Serwisu przez użytkownika oferującego pracę. W tym przypadku spółka nie ponosi również żadnych kosztów związanych z ewentualnymi wydatkami wiążącymi się z wszczętym procesem zatrudnienia tj. zwrot kwot wydanych na bilety czy inne wydatki związane z dotarciem na miejsce świadczenia pracy, tudzież na miejsce zaplanowanego spotkania w sprawie rozmów wstępnych przed ewentualnym zatrudnieniem.</p>
    	<p style="text-align:center;"><strong>Art. 6</strong></p>
    	<p style="text-align:center;"><strong>INFORMACJE HANDLOWE</strong></p>
    	<p>W przypadku użytkowników, którzy podali swoje dane osobowe i wyrazili zgodę na ich przetwarzanie do celów marketingowych, Spółka ma prawo przetwarzania danych osobowych użytkownika w celu marketingu usług własnych oraz usług swoich partnerów biznesowych, stosując narzędzia marketingu realizowanego zarówno drogą elektroniczną, jak i metodami tradycyjnymi.</p>
    	<p>Spółka wysyła użytkownikowi informacje o dopasowanych do niego ofertach pracy (np. w postaci powiadomień na telefon komórkowy, z ofertami wyszukanymi na podstawie wybranych przez użytkownika kryteriów lub w postaci ofert sugerowanych, ustalonych przez algorytm na podstawie historii aktywności danego użytkownika) wyłącznie po wyrażeniu odpowiedniej zgody przez użytkownika. Zgoda ta może być w dowolnym momencie wycofana.</p>
    	<p>Dane osobowe użytkownika mogą być przekazywane przez Spółkę innym podmiotom należącym do grupy kapitałowej Spółki prowadzącym serwisy internetowe, w celach reklamowych, badania rynku oraz określania zachowań i preferencji użytkowników.</p>
    	<p>Spółka wykorzystuje dane osobowe użytkownika do celów przesyłania drogą elektroniczną informacji handlowych pochodzących od Spółki lub partnerów biznesowych wyłącznie po wyrażeniu przez użytkownika zgody na takie przetwarzanie. Zgoda ta może być w dowolnym momencie wycofana.</p>
    	<p>Spółka nie udostępnia żadnych danych osobowych użytkowników w innych przypadkach lub innym podmiotom, niż określone powyżej.</p>
    	<p>Spółka ma prawo przekazać dane osobowe instytucjom do tego uprawnionym, takim jak policja, prokuratura, sądy, działającym na mocy obowiązujących przepisów prawa.</p>
    	<p>Happinate zastrzega sobie prawo do wysyłania użytkownikom niezapowiedzianych wiadomości. Zalicza się do nich informacje odnoszące się bezpośrednio do funkcjonowania Portalu (np. zmiany w funkcjonowaniu) bądź też komunikaty systemu (np. informacje o nowych wiadomościach oczekujących na poczcie wewnętrznej, komunikaty o przerwach w działaniu Portalu). Każdy z Użytkowników Portalu ma prawo zastrzec, że nie chce otrzymywać tego typu informacji od Spółki.</p>
    	<p>Natomiast treści reklamowe związane z działalnością komercyjną happinate i jej kontrahentów mogą być przesyłane Użytkownikom wyłącznie za ich zgodą, zgodnie z treścią art. 10 ustawy z dnia 18 lipca 2002 roku o świadczeniu usług drogą elektroniczną (Dz. U. 2002, Nr 144, poz. 1204, z późn. zm.). Zgoda w tym zakresie ma charakter odwoływalny.</p>
    	<p style="text-align:center;"><strong>Art. 7</strong></p>
    	<p style="text-align:center;"><strong>BADANIA SPOŁECZNE I MARKETINGOWE</strong></p>
    	<p>Happinate może przeprowadzać za pośrednictwem Serwisu różne badania społeczne i marketingowe (w szczególności ankiety). Jeśli Użytkownicy zgodzą się w nich uczestniczyć, będziemy wykorzystywać odpowiedzi w celu opracowania anonimowych wyników tych ankiet. Przestrzegamy w tym zakresie wszystkich polskich i europejskich norm dotyczących prowadzenia badań społecznych, aby zapewnić użytkownikom Serwisu bezpieczeństwo i anonimowość.</p>
    	<p style="text-align:center;"><strong>Art. 8</strong></p>
    	<p style="text-align:center;"><strong>BEZPIECZEŃSTWO INFORMACJI</strong></p>
    	<p>Spółka dokłada wszelkich starań aby zapewnić bezpieczeństwo informacji zbieranych lub powierzanych przez użytkownika.</p>
    	<p>Serwisy Spółki używają szyfrowanej transmisji danych (SSL) podczas rejestracji i logowania, co zapewnia ochronę danych identyfikujących użytkownika i znacząco utrudnia przechwycenie dostępu do jego konta.</p>
    	<p style="text-align:center;"><strong>Art. 9</strong></p>
    	<p style="text-align:center;"><strong>ODPOWIEDZIALNOŚĆ SPÓŁKI</strong></p>
    	<p>Spółka nie ponosi odpowiedzialności za treści publikowane przez firmy i osoby trzecie, w szczególności w postaci ofert pracodawców, wizytówek użytkowników itp. Spółka nie ponosi również odpowiedzialności za treści znajdujące się na serwisach nie będących własnością Spółki, do których linki mogą znajdować się w treściach zawartych na stronach Spółki.</p>
    	<p>Użytkownicy Serwisu mają możliwość udostępniania przekierowania (link) do innych stron www. Treści zawarte na tych stronach są przedmiotem praw autorskich przysługujących ich twórcy i podlegają ochronie zgodnie z prawem autorskim. W związku z tym, że happinate nie sprawuje kontroli nad witrynami osób trzecich, nie zatwierdza, nie dokonuje przeglądu i nie jest odpowiedzialna w szczególności za dostępność witryn osób trzecich Użytkownik uznaje, że korzysta z Witryn osób trzecich na własną odpowiedzialność oraz że happinate nie jest odpowiedzialna za jakiekolwiek szkody lub krzywdy poniesione w związku z korzystaniem z witryn, do których przekierowują odsyłacze zamieszczane przez Użytkowników w Serwisie. Zachęcamy do zapoznania się z treściami oświadczeń o ochronie prywatności umieszczonymi na stronach gromadzących dane osobowe. Użytkownik ponosi pełną odpowiedzialność za treści znajdujące się na stronie, do której przekierowuje zamieszczany przez niego w Serwisie odsyłacz.</p>
    	<p style="text-align:center;"><strong>Art. 10</strong></p>
    	<p style="text-align:center;"><strong>ZARZĄDZANIE DANYMI</strong></p>
    	<p>Użytkownik ma w każdej chwili możliwość zdecydowania, jakie dane chce dodać lub usunąć, choć może to wpłynąć na jakość użytkowania Serwisu lub uniemożliwić korzystanie z niektórych usług.</p>
    	<p>Jeśli użytkownik ma założone konto, ale nie życzy sobie otrzymywać korespondencji mailowej, może wyłączyć wysyłanie poszczególnych typów informacji korzystając z odpowiedniej zakładki na swoim koncie. Niezależnie od dokonania przez użytkownika powyższego wyboru, Spółka zastrzega sobie prawo do przesyłania mu istotnych z punktu widzenia prawnego, technicznego bądź organizacyjnego informacji związanych z funkcjonowaniem usług.</p>
    	<p>Zakładanie konta nie jest wymagane aby można było korzystać z wyszukiwarki ofert pracy. Brak konta pozbawi jednak użytkownika możliwości aplikowania na oferty pracy oraz dostępu do historii ofert na które aplikował, możliwości zdefiniowania powiadomień o dopasowanych ofertach pracy, uproszczenia aplikowania poprzez możliwość korzystania z przycisku &bdquo;1-click&rdquo; itp.</p>
    	<p>Użytkownik ma również możliwość wyłączenia zapisywania plików &bdquo;cookies&rdquo;, co uniemożliwi jakiekolwiek automatyczne zbieranie informacji o użytkowniku. Korzystanie z wyszukiwarki ofert pracy i aplikowanie nadal będzie w pełni dostępne, użytkownik nie będzie jednak miał dostępu do swoich ulubionych ofert ani historii wyszukiwań.</p>
    	<p>Rozwiązanie umowy o świadczenie usług drogą elektroniczną przez Użytkownika Serwisu jest dobrowolne i może nastąpić w każdym czasie za pomocą formularza dostępnego na stronie <a href="http://www.happinate.com/">www.happinate.com</a>. Rozwiązanie umowy o świadczenie usług drogą elektroniczną skutkuje usunięciem konta.</p>
    	<p>Dane osobowe Użytkowników, których Konta zostały usunięte są przetwarzane przez happinate w terminie określonym w procedurze przetwarzania danych osobowych z kont usuniętych. W związku z koniecznością zapewnienia aktywnym Użytkownikom najwyższych standardów bezpieczeństwa, nie dopuszczamy jednak, aby sprawcy ewentualnych naruszeń zacierali ślady swojej działalności w Serwisie.</p>
    	<p style="text-align:center;"><strong>Art. 11</strong></p>
    	<p style="text-align:center;"><strong>KONTAKT</strong></p>
    	<p>Użytkownicy Serwisu mają możliwość bezpośredniego kontaktu mailowego z pracownikami Biura Obsługi Klienta poprzez formularz dostępny na stronie <a href="http://www.happinate.com/">www.happinate.com</a>.</p>
    	<p>Happinate przechowuje korespondencję ze swoimi Użytkownikami w celach statystycznych oraz jak najlepszej i najszybszej reakcji na pojawiające się zapytania, a także w zakresie rozstrzygnięć reklamacyjnych i podejmowanych na podstawie zgłoszeń ewentualnych decyzji o interwencjach administracyjnych we wskazane profile. Jednocześnie happinate gwarantuje, że adresy oraz dane w ten sposób gromadzone nie będą wykorzystywane do komunikacji z Użytkownikiem w celu innym niż realizacja zgłoszenia. Ewentualna komunikacja w innych kwestiach może się odbywać tylko za uprzednią zgodą Użytkownika.</p>
    	<p>Gdy kontaktujesz się z nami w celu dokonania danych czynności (np. złożenia reklamacji) za pomocą formularza na stronie internetowej <a href="http://www.happinate.com/">www.happinate.com</a>, możemy ponownie wymagać przekazania nam Twoich danych, w tym osobowych, np. w postaci imienia, nazwiska, adresu e-mail, itp., celem potwierdzenia Twojej tożsamości i umożliwienia zwrotnego kontaktu w danej sprawie. Powyższe dotyczy tych samych danych, w tym osobowych, które wcześniej Użytkownik podał, i na których przetwarzanie wyraził zgodę. Ponowne, podanie tych danych nie jest obowiązkowe lecz może być niezbędne do dokonania czynności lub uzyskania informacji, które Cię interesują.</p>
    	<p>Wszelkie pytania i wątpliwości dotyczące w szczególności Regulaminu Serwisu happinate oraz jego załączników, Polityki Prywatności oraz korzystania z Portalu prosimy kierować do Biura Obsługi Klienta za pośrednictwem formularza dostępnego na stronie <a href="http://www.happinate.com/">www.happinate.com</a>.</p>
    	<p style="text-align:center;"><strong>Art. 12</strong></p>
    	<p style="text-align:center;"><strong>POSTANOWIENIA KOŃCOWE</strong></p>
    	<p>Usługi i funkcje w ramach Serwisu będą z czasem ulegać rozszerzeniu. Oznacza to m.in., że w przyszłości happinate może wprowadzić zmiany w niniejszym dokumencie.</p>
    	<p>Ogłoszenie zmian nastąpi w ten sposób, że Spółka umieści w ramach Serwisu informację o zmianach w Polityce Prywatności. Wraz z każdą zmianą nowa wersja Polityki Prywatności będzie się pojawiać z nową datą. Wszelkie najistotniejsze z punktu widzenia Użytkownika zmiany będą odpowiednio wyróżnione.</p>
    	<p>Jakiekolwiek wprowadzane ewentualnie zmiany w Polityce Prywatności nie wpływają na podstawową zasadę: happinate nie udostępnia ani nie przekazuje wbrew prawu podmiotom trzecim (w szczególności reklamodawcom) jakichkolwiek danych osobowych posiadanych w ramach bazy danych osobowych Portalu.</p>
    	<p>W razie wątpliwości lub sprzeczności pomiędzy Polityką a udzielonymi przez Użytkownika zgodami, niezależnie od postanowień powyższych, podstawą do podejmowania oraz określenia zakresu działań przez happinate są udzielone przez Użytkowników zgody. Niniejszy dokument ma natomiast charakter informacyjny.</p>
	</div>
    
    <div class="right-column">
    	<!--<h2>Jak to działa?</h2>
   <a href="#videoModal" role="button" data-toggle="modal" class="video-works"><img src="img/video-bg.png" alt="Jak to działa?"></a>
   <h2 style="margin-top:34px;">Pobierz aplikację</h2>
   <a href="#" class="app-btn"></a>
   <a href="#" class="android-btn"></a>
   <div class="clear"></div>-->
		
		<div class="simple-fb-like">
			<div class="fb-like-box" data-href="https://www.facebook.com/happinate" data-width="292" data-height="500" data-show-faces="true" data-stream="false" data-show-border="false" data-header="false"></div>
		</div>
    </div>
    
    <div class="clear"></div>
{/block}