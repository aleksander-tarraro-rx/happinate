{extends file="index.tpl"}

{block name=body}
    <div class="left-column simple-text">
        
        <h2>W happinate rozumiemy, jak zmienia się rynek pracy. W związku z tym stworzyliśmy internetowe środowisko właśnie dla Ciebie! Z poszanowaniem Twojego czasu!</h2>

        <p style="margin: 10px;">Szukasz pracy na dzień, miesiąc, zlecenie, zastępstwo, po to aby zdobyć doświadczenie? Bez zbędnych formalności, stresujących rozmów i wielotygodniowego oczekiwania na odpowiedź? Dzięki nam Ty decydujesz, czy rano chcesz iść do pracy. Wystarczy jedno kliknięcie i wskakuj pod prysznic, bo zaraz wychodzisz!</p>
        
        <h2>Gdzie działamy?</h2>

        <p style="margin: 10px;">happinate to poznańska firma, która ma na celu uproszczenie procesu poszukiwania pracy, a tym samym stworzeniu miejsca, które będzie przyjazne dla kandydata oraz pracodawcy. Poznań i Warszawa to miasta, z których oferty pracy, są umieszczane na łamach Naszego serwisu.</p>
        
        <h2>Jak będzie w happinate? </h2>

        <p style="margin: 10px;">W happinate będzie bardzo tanio, bardzo prosto i bardzo szybko – zawsze.<br/>
        Dzięki unikatowym funkcjonalnością, znalezienie pracy, dla nikogo nie będzie już problemem.</p>
        
        <p style="margin: 10px;">Oferta <font style="color: #C81D1D;">PRACUJ TERAZ!</font> skierowana jest do osób poszukujących pracy na dziś i na jutro. Dzięki niej kandydat może szybko znaleźć pracę oraz zacząć pracować w tym samym dniu co aplikował na ogłoszenie typu: <font style="color: #C81D1D;">PRACUJ TERAZ!</font> Wystarczy, że pracodawca dodając ogłoszenie na stronie www.happinate.com, zaznaczy opcję <font style="color: #C81D1D;">PRACUJ TERAZ!</font> - jego ogłoszenie będzie promowane przez serwis. Tym samym będzie mógł w szybki sposób znaleźć potrzebnego pracownika.</p>
        <p style="margin: 10px;"><font style="color: #0066cc;">Let's work together</font> jest ofertą skierowaną dla osób pragnących znaleźć pracę dedykowaną dla większego grona osób. Kandydat może zaprosić swoich znajomych i wspólnie pracować oraz zarabiać.</p>
        <p style="margin: 10px;"><b>Celem serwisu</b> jest uproszczenie procesu rekrutacji pracownika. Poprzez opcję <font style="color: #499249">1-click</font> można szybko aplikować na dane ogłoszenie. Pracując wcześniej u danego pracodawcy, kandydat może bez zbędnych formalności np. rozmowy rekrutacyjnej aplikować na dodane przez niego ogłoszenie. </p>
        <p style="margin: 10px;">Pracodawca tworzy bazę sprawdzonych pracowników. Szybko i prosto może wysłać kandydatowi zaproszenie do oferty pracy. Dzięki kalendarzowi rekrutera, pracodawca może rozplanować proces rekrutacji w czasie.</p>
        <p style="margin: 10px;"><b>Oferty pracy</b> dostępne są również w aplikacji mobilnej, którą możemy pobrać na telefon. Dzięki usłudze geolokalizacji kandydat zobaczy wszystkie ogłoszenia znajdujące się w jego okolicy i sprawdzi najszybszą trasę dojazdu do pracy dzięki współpracy partnerskiej z <font style="color: #39ADAD;">jakdojade</font>. </p>
        <p style="margin: 10px;"><font style="color: #39ADAD;"><b>Plusy happinate:</b></font></p>
        <p style="margin: 10px;">
            <ul style="margin: 10px; list-style: square;">
                <li style="margin-left:40px;">Serwis pracy</li>
                <li style="margin-left:40px;">Własna baza sprawdzonych pracowników</li>
                <li style="margin-left:40px;">Aplikacja mobilna z geolokalizacją</li>
                <li style="margin-left:40px;">Współpraca partnerska z serwisem jakdojade</li>
                <li style="margin-left:40px;">Prosta formuła dodawania ogłoszeń</li>
                <li style="margin-left:40px;">Najszybsza możliwa forma aplikowania</li>
                <li style="margin-left:40px;">Przejrzyste filtry wyszukiwania ofert</li>
                <li style="margin-left:40px;">unikatowa opcja 1-click</li>
                <li style="margin-left:40px;">możliwość darmowego dodania ogłoszenia</li>
                <li style="margin-left:40px;">unikatowe funkcje: PRACUJ TERAZ!, 1-click, Let's work together!</li>
                <li style="margin-left:40px;">oferty praktyk, staży, wolontariatu</li>
                <li style="margin-left:40px;">kalendarz rekrutera</li>
            </ul>
        </p>
            
        <p style="margin: 10px;"><a href="{$smarty.const.HOST}view/images/happinate-praca-od-zaraz.pdf" target="_blank" class="btn btn-info btn-large">POBIERZ WSZYSTKIE INFORMACJE W PLIKU PDF</a></p>
    </div>
        
    
    <div class="right-column">
		
		<div class="simple-fb-like">
			<div class="fb-like-box" data-href="https://www.facebook.com/happinate" data-width="292" data-height="500" data-show-faces="true" data-stream="false" data-show-border="false" data-header="false"></div>
		</div>
    </div>
    
    <div class="clear"></div>
{/block}