{extends file="index.tpl"}

{block name=body}
    <div class="left-column simple-text">
    	<h2 style="text-align:center;">Dane rejestrowe</h2>
		<p><strong>happinate sp. z o.o.</strong></p>
		<p><strong>Adres rejestrowy:</strong><br>
		ul. Ratajczaka 42/13<br>
		61-816 Poznań</p>
		<p><strong>NIP</strong>: 7831694894<br>
		<strong>REGON</strong>: 302302718<br>
		<strong>KRS</strong>: 0000444418<br>
		<strong>PKD</strong>: 63.12.Z</p>
		<p><strong>Kapitał zakładowy</strong>: 10.000 PLN opłacony w całości</p>
		<p><strong>konto PLN</strong>: 04 1050 1520 1000 0091 3183 9939</p>
		<p><strong>Adres biura:</strong><br>
		ul. Abpa Baraniaka 88E <br>
		(budynek F)<br>
		61-131 Poznań</p>
	</div>
    
    <div class="right-column">
    	<!--<h2>Jak to działa?</h2>
   <a href="#videoModal" role="button" data-toggle="modal" class="video-works"><img src="img/video-bg.png" alt="Jak to działa?"></a>
   <h2 style="margin-top:34px;">Pobierz aplikację</h2>
   <a href="#" class="app-btn"></a>
   <a href="#" class="android-btn"></a>
   <div class="clear"></div>-->
		
		<div class="simple-fb-like">
			<div class="fb-like-box" data-href="https://www.facebook.com/happinate" data-width="292" data-height="500" data-show-faces="true" data-stream="false" data-show-border="false" data-header="false"></div>
		</div>
    </div>
    
    <div class="clear"></div>

{/block}