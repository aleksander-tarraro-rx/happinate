<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html> 
<head> 
<meta http-equiv="Content-Type" content="charset=utf-8" />
<title>Praca dla studenta - Poznań, Warszawa </title>
<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<style>
body { margin: 0; padding: 0; font-size: 14px; font-family: 'PT Sans Narrow', sans-serif; color: #999; background: #FFF; }
.clear { clear: both;  }
.cv { padding: 0; margin: 0; width: 980px; }
.box1 { width: 100%; height: 235px; border-bottom: #CCC 1px solid; margin-bottom: 10px; }
.avatar { }
.avatar img { height: 235px; }
.adress { margin-top: 40px; }
.name { font-size: 24px; color: #a4a7a7; margin-top: 5px; padding-left: 41px; margin-bottom: 5px; }
.adres { font-size: 15px; color: #4f5252; margin: 0px auto 0px auto; height: 34px; padding-left: 40px; background: url(/view/images/icon-adres-white.jpg) no-repeat; }
.email { font-size: 15px; color: #4f5252; margin: -5px auto 0px auto; height: 34px; padding-left: 40px; background: url(/view/images/icon-email-white.jpg) no-repeat; }
.phone { font-size: 15px; color: #4f5252; margin: -5px auto 0px auto; height: 34px; padding-left: 40px; background: url(/view/images/icon-phone-white.jpg) no-repeat; }
.urodz { font-size: 15px; color: #4f5252; margin: -5px auto 0px auto; height: 34px; padding-left: 40px; }
.box { background: none repeat scroll 0 0 #FFF; color: #848C8B; margin-bottom: 10px; padding-bottom: 10px; page-break-inside: avoid; page-break-before:auto; page-break-after:auto; }
.box .header { background: none repeat scroll 0 0 #505252; color: #FFFFFF; font-size: 20px; padding: 0px 20px 5px 20px; }
.box .item { padding: 10px 20px 0; }
.box .item span { color: #666; }
.box .item p { margin: 0px; }
.footer { padding: 10px; position: fixed; bottom: 0px; left: 0px; right: 0px; height: 50px; text-align: center; }
</style>
</head>
<body>
    <div class="box1">
        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td width="205">
                    {if $profile.user_logo}
                        <img src="http://{$smarty.server.SERVER_NAME}{$smarty.const.HOST}upload/userLogo/{$profile.user_logo}" alt="Twoje zdjęcie profilowe!" />
                    {else} 
                        <img src="http://{$smarty.server.SERVER_NAME}{$smarty.const.HOST}view/images/unknown_user.png" alt="Twoje zdjęcie profilowe!" />
                    {/if}
                </td>
                <td>
                    <div class="adress">
                        <div class="name">{if !empty($profile.user_name) || !empty($profile.user_surname)}{$profile.user_name} {$profile.user_surname}{else}<i>uzupełnij dane</i>{/if}</div>
                        {if !empty($profile.user_adress_street) || !empty($profile.user_adress_city) || !empty($profile.user_adress_zip_code)}
                            <div class="adres">
                                {$profile.user_adress_street} {$profile.user_adress_city} {$profile.user_adress_zip_code}
                            </div>
                        {/if}
                        {if $profile.user_email}<div class="email">{$profile.user_email}</div>{/if}
                        {if $profile.user_phone}<div class="phone">{$profile.user_phone}</div>{/if}
                        {if $profile.user_date_of_birth}<div class="urodz">ur. {$profile.user_date_of_birth}</div>{/if}
                    </div>
                </td>
            </tr>
        </table>
    </div>
            
    {if $userExperience}    
    <div class="box">

        <div class="header" style="{if isset($userBgCVColors.0)}background-color: {$userBgCVColors.0};{/if}{if isset($userCVColors.0)}color: {$userCVColors.0};{/if}">
            DOŚWIADCZENIE
        </div>

        {foreach from=$userExperience item=exp}
        <div class="item">
            <p><span style="font-size: 16px">{$exp.experience_company}</span><p>
            <p><span>{$exp.experience_position}</span> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span>&#8226;</span> {$exp.experience_start} - {if $exp.experience_till_now}teraz{else}{$exp.experience_end}{/if}</p>
            <p>{htmlspecialchars_decode($exp.experience_opis)}</p>
        </div>
        {/foreach}

    </div>
    {/if}
     
    {if $userEducation}
    <div class="box">

        <div class="header" style="{if isset($userBgCVColors.1)}background-color: {$userBgCVColors.1};{/if}{if isset($userCVColors.1)}color: {$userCVColors.1};{/if}">
            EDUKACJA
        </div>

        {foreach from=$userEducation item=edu}
        <div class="item">
            <p><span style="font-size: 16px">{$edu.education_name}</span><p>
            <p><span>{$edu.education_field}</span> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span>&#8226;</span> {$edu.education_start} - {if $edu.education_till_now}teraz{else}{$edu.education_end}{/if}</p>
        </div>
        {/foreach}

    </div>
    {/if}
    
    {if $userLangs}             
    <div class="box">

        <div class="header" style="{if isset($userBgCVColors.2)}background-color: {$userBgCVColors.2};{/if}{if isset($userCVColors.2)}color: {$userCVColors.2};{/if}">
            JĘZYKI
        </div>

        {foreach from=$userLangs item=lang}
        <div class="item">
            <p><span>{$lang.lang_name}</span> - {$lang.lang_level}<p>
        </div>
        {/foreach}

    </div>
    {/if}

    {if $userOtherSkill}           
    <div class="box">

        <div class="header" style="{if isset($userBgCVColors.4)}background-color: {$userBgCVColors.4};{/if}{if isset($userCVColors.4)}color: {$userCVColors.4};{/if}">
            UMIEJĘTNOŚCI
        </div>

        {foreach from=$userOtherSkill item=skill}
        <div class="item">
            <p><span>{$skill.skill_name}</span><p>
            {if $skill.skill_opis}<p><small>opis:</small> {$skill.skill_opis}</p>{/if}
        </div>
        {/foreach}

    </div>   
    {/if}

    {if $userTraining}
    <div class="box">

        <div class="header" style="{if isset($userBgCVColors.3)}background-color: {$userBgCVColors.3};{/if}{if isset($userCVColors.3)}color: {$userCVColors.3};{/if}">
            SZKOLENIA
        </div>

        {foreach from=$userTraining item=tra}
        <div class="item">
            <p><span>{$tra.training_name}</span><p>
            <p><small>data zakończenia</small> {$tra.training_end}</p>
        </div>
        {/foreach}

    </div>
    {/if}

    {if $userSkill}    
    <div class="box">

        <div class="header" style="{if isset($userBgCVColors.5)}background-color: {$userBgCVColors.5};{/if}{if isset($userCVColors.5)}color: {$userCVColors.5};{/if}">
            INFORMACJE DODATKOWE
        </div>

        <div class="item">
            {if $userSkill.skill_driving_a || $userSkill.skill_driving_a1 || $userSkill.skill_driving_b || $userSkill.skill_driving_b1 || $userSkill.skill_driving_c || $userSkill.skill_driving_c1 || $userSkill.skill_driving_d || $userSkill.skill_driving_d1 || $userSkill.skill_driving_be || $userSkill.skill_driving_c1e || $userSkill.skill_driving_ce || $userSkill.skill_driving_d1e || $userSkill.skill_driving_de || $userSkill.skill_driving_t}
            <p><span>Prawo jazdy kategorii:</span> {if $userSkill.skill_driving_a} A {/if}{if $userSkill.skill_driving_a1} A1 {/if}{if $userSkill.skill_driving_b} B {/if}{if $userSkill.skill_driving_b1} B1 {/if}{if $userSkill.skill_driving_c} C {/if}{if $userSkill.skill_driving_c1} C1 {/if}{if $userSkill.skill_driving_d} D {/if}{if $userSkill.skill_driving_d1} D1 {/if}{if $userSkill.skill_driving_be} BE {/if}{if $userSkill.skill_driving_c1e} C1E {/if}{if $userSkill.skill_driving_ce} CE {/if}{if $userSkill.skill_driving_de} D1E {/if}{if $userSkill.skill_driving_d1e} DE {/if}{if $userSkill.skill_driving_t} T {/if}<p>
            {/if}
            {if $userSkill.skill_sanel_owner}
            <p>Posiadam <span>książeczkę sanepidu</span>{if $userSkill.skill_sanel_not_expired} i jest ona ważna{/if}.</p>
            {/if}
            {if $userSkill.skill_welding_tig || $userSkill.skill_welding_mig || $userSkill.skill_welding_mag}
            <p><span>Uprawnienia elektryczne:</span>{if $userSkill.skill_welding_tig} TIG {/if}{if $userSkill.skill_welding_mig} MIG {/if}{if $userSkill.skill_welding_mag} MAG {/if}</p>
            {/if}
            {if $userSkill.skill_forklift || $userSkill.skill_forklift_big}
            <p><span>Wózki widłowe:</span>{if $userSkill.skill_forklift} wózki widłowe {/if}{if $userSkill.skill_forklift_big} wysoki skład {/if}</p>
            {/if}
        </div>

    </div>     
    {/if}    

    {if $profile.user_hobby}
    <div class="box">

        <div class="header" style="{if isset($userBgCVColors.6)}background-color: {$userBgCVColors.6};{/if}{if isset($userCVColors.6)}color: {$userCVColors.6};{/if}">
            ZAINTERESOWANIA
        </div>

        <div class="item">
            <p>{htmlspecialchars_decode($profile.user_hobby)}<p>
        </div>

    </div>
    {/if}
    
    <div class="footer">
        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td style="font-size: 10px;" align="center" colspan="3">Wyrażam zgodę na przetwarzanie moich danych osobowych dla potrzeb procesu rekrutacji (zgodnie z ustawą o ochronie danych osobowych z dnia 29.08.1997 Dz. U. Nr 133, Poz. 833) </td>
            </tr>
            <tr>
                <td style="font-size: 10px;" colspan="3"><hr style="border: 0;color: #CCC; background-color: #CCC; height: 1px;"></td>
            </tr>
            <tr>
                <td width="20" valign="middle" align="left">
                    <img src="http://{$smarty.server.SERVER_NAME}{$smarty.const.HOST}view/images/favicon.png" height="20" width="20" alt="happinate" />
                </td>
                <td valign="middle" align="left">
                     {$smarty.server.SERVER_NAME}{$smarty.const.HOST}profil/k/{$profile.user_id|md5}
                </td>
                <td align="right" valign="middle">HAPPINATE &copy; 2013</td>
            </tr>
        </table>
    </div>
</body>
</html>