{extends file="index.tpl"}

{block name=body}

  <div class="formuly">
        <div class="formuly">
                <div class="formula free">
                    <div class="formula-header">
                        <i class="formula-logo"></i>
                        <p class="formula-header-text">formuła free</p>
                    </div>
                    <div class="formula-body">
                        <p class="formula-price"><small>bezpłatne</small></p>
                    </div>
                    <div class="formula-footer">
                        ogłoszenie bezpłatne na 14 dni
                    </div>
                    <a class="happi-btn" href="{$smarty.const.HOST}daj-ogloszenie">DODAJ OGŁOSZENIE <i class="icon-chevron-right icon-white"></i></a>
                </div>

                <div class="formula mini">
                    <div class="formula-header">
                        <i class="formula-logo"></i>
                        <p class="formula-header-text">formuła mini *</p>
                    </div>
                    <div class="formula-body">
                        <p class="formula-price">9,<small>00 zł</small></p>
                    </div>
                    <div class="formula-footer">
                        wyróżnienie ogłoszenia na 14 dni
                    </div>
                    <a class="happi-btn" href="{$smarty.const.HOST}daj-ogloszenie">DODAJ OGŁOSZENIE <i class="icon-chevron-right icon-white"></i></a>
                </div>

                <div class="formula happi">
                    <div class="formula-header">
                        <i class="formula-logo"></i>
                        <p class="formula-header-text">PRACUJ TERAZ! *</p>
                    </div>
                    <div class="formula-body">
                        <p class="formula-price">29,<small>00 zł</small></p>
                    </div>
                    <div class="formula-footer">
                        ogłoszenie na 14 dni
                    </div>
                    <a class="happi-btn" href="{$smarty.const.HOST}daj-ogloszenie#pracujteraz">DODAJ OGŁOSZENIE <i class="icon-chevron-right icon-white"></i></a>
                </div>
                <div class="clear"></div>
            </div>
                
    <p class="price-description">* Powyższe ceny są cenami netto i należy do nich doliczyć podatek VAT.</p>
   
    
{/block}