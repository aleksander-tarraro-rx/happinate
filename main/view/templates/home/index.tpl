{extends file="index.tpl"}

{block name=prebody}
      
    <div id="WorkNow" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" style="width: 900px; margin-left: -480px;overflow: hidden;" aria-hidden="true">
      <div class="modal-body" style="overflow: hidden;">
          <img src="{$smarty.const.HOST}view/images/worknow-big.png" />
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Zamknij</button>
        {if isset($_user) && $_user}
            <a href="{$smarty.const.HOST}jak-to-dziala" class="btn btn-primary">Zobacz więcej</a>
        {else}
            <a href="{$smarty.const.HOST}rejestracja/kandydat" class="btn btn-primary">Zobacz więcej</a>
        {/if}
      </div>
    </div>
      
    <div class="main-apla">
        <div class="center">
            <img class="apla-img" src="{$smarty.const.HOST}view/images/apla-mens.png" style="height: 147px" alt="PRACA DORYWCZA OD ZARAZ!">
            <h1 class="apla-haslo">PRACA OD ZARAZ!</h1>
            <div class="searchMainBox">
               <form action="{$smarty.const.HOST}oferty-pracy/1/" method="post" class="main-form"  >
                   <input type="hidden" name="filter" value="1">
                    <div class="span7" style="width: 300px; margin-right: 10px; margin-left: 10px;">
                        <div class="text-input" style="width: 300px;">
                            <input type="text" style="width: 280px; height: 15px; border: 1px solid #C2C2C2;" placeholder="szukaj po nazwie..." name="search" value="" maxlength="120" id="1"{if isset($errorArray.vacancies) && $errorArray.vacancies} style="background:#EED3D7"{/if}>
                        </div>
                    </div>
                    <div class="select-input" style="width: 160px; float: left">
                        <div class="select-label">
                            <p class="select-value">Wybierz region</p>
                            <input type="hidden" name="province" value="1">
                            <i class="icon-select-list"></i>
                        </div>
                        <ul>
                           {if $provinces}
                                {foreach from=$provinces item=province}
                                    <li id="{$province.province_id}">{$province.province_text}</li>
                                {/foreach}
                            {/if}
                        </ul>
                    </div>
                    <button class="btn btn-danger btn-large overlayerButton" style="float: left; padding: 7px 15px; margin-left: 10px;">SZUKAJ <i class="icon-white icon-search"></i></button>
               </form>
           </div>
        </div>
    </div>    
{/block}
{block name=body}
    <div class="left-column">
        
        <h2 style="color: #666;">Oferty pracy na dziś i jutro!</h2>

        <div class="karusela-mask">
            <div class="karusela">
                {foreach from=$mainPageJobs item=job}
                <div class="one-notice{if $job.job_main_page==1} wyroznienie{elseif $job.job_main_page==2} wyroznienie2{/if}" {if $job.job_main_page} style="width: 588px;"{/if}>
                        <div class="one-notice-text-box">
                        <p class="notice-title"><a href="{$smarty.const.HOST}praca/{$job.job_id}/{$job.job_url_text}">{$job.job_position}</a></p>
                        <p class="notice-job-area">{$job.job_employeer_place_direct_city}</p>
                    </div>
                    <div class="one-notic-pay">
                        {if $job.job_rate_min>0}
                            {if $job.job_rate_min == $job.job_rate_max}
                                {assign var=rate value="."|explode:$job.job_rate_min}
                                <p class="value">{$rate.0},<small>{if isset($rate.1)}{$rate.1}{if strlen($rate.1)==1}0{elseif strlen($rate.1)==0}00{/if}{else}00{/if}</small></p>
                                <p class="rate">zł/godzinę</p>
                            {else}
                                {assign var=rate value=ceil(($job.job_rate_min + $job.job_rate_max) / 2)}
                                <p class="value">{$rate}<small>,00</small></p>
                                <p class="rate">zł/godzinę</p>
                            {/if}
                        {/if}
                    </div>
                    {if $job.job_main_page==2}
                    <div class="workNow">
                            <img width="50px" height="50px" style="margin-top: 10px; margin-right: 10px;" src="{$smarty.const.HOST}view/images/work-now-logo.png" />
                    </div>
                    {elseif $job.job_main_page==1}
                    <div class="workNow">
                            <img width="50px" height="50px" style="margin-top: 10px; margin-right: 10px;" src="{$smarty.const.HOST}view/images/super-oferta.png" />
                    </div>
                    {/if}
                </div>
                {/foreach}
            </div>
        </div>
        <div class="clear" style="height:3px"></div>

        <!--<a href="index.php?page=looking_job" class="karuzela-show-more">Zobacz więcej</a>-->
        <a href="{$smarty.const.HOST}oferty-pracy" class="happi-big-btn" style="width:180px;float:left;margin-top:10px;padding:6px 0px 8px 0px;">Wszystkie oferty <i class="icon-chevron-right icon-white" style="margin-top:3px;"></i></a>
    	<div class="clear"></div>

        <div class="happi-fb-like">
            <div class="fb-like" data-href="https://www.facebook.com/happinate" data-send="false" data-width="315" data-show-faces="true" data-font="tahoma"></div>
        </div>
        
        <div class="g-plusone" data-annotation="inline" data-width="300"></div>
    </div>
    
        <div class="right-column" style="text-align:center;">
            <a href="#WorkNow" role="button" data-toggle="modal" class="video-works pracujTerazImgBtn"></a>
            
            <a href="{$smarty.const.HOST}rejestracja" class="btn btn-danger btn-large">Załóż konto <i class="icon-chevron-right icon-white"></i></a>
            
            <a href="https://play.google.com/store/apps/details?id=com.moodup.happinate" target="_blank" style="margin: 30px 0px 0px 90px;" class="android-btn"></a>
            <div class="clear"></div>

        </div>
    </div>

    
    <div class="clear"></div>

    <div class="partnerzy">
        <hr> 
        <div class="text">Partnerzy:</div>
        
        <a href="http://jakdojade.pl/" target="_blank"><span class="jakdojade"></span></a>
        <a href="http://www.qpony.pl/" target="_blank"><span class="qpony"></span></a>
        <a href="http://www.zostanstudentem.pl/" target="_blank"><span class="zostanstudendem"></span></a>
        <a href="http://amu.edu.pl/" target="_blank"><span class="uam"></span></a>
        <a href="http://startinstartup.pl/" target="_blank"><span class="startstartup"></span></a>
        <a href="http://aiesec.pl/poznan/" target="_blank"><span class="aisec"></span></a>
        <a href="http://www.mamstartup.pl/" target="_blank"><span class="mamstartup"></span></a>
        <a href="http://www.aip.org.pl/" target="_blank"><span class="aip"></span></a>
        <a href="http://juicynews.pl/" target="_blank"><span class="juicynews"></span></a>
        <a href="http://kartaroznorodnosci.pl/pl" target="_blank"><span class="kartaroznorodnosci"></span></a>
        
        <div class="clear"></div>
    </div>

{/block}

{block name=additionalJavaScript}
<!-- Place this tag after the last +1 button tag. -->

<script type="text/javascript">
window.___gcfg = { lang: 'pl' };
(function() {
var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
po.src = 'https://apis.google.com/js/plusone.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
</script>
{/block}
