{extends file="index.tpl"}

{block name=metatitle}Praca dorywcza w domu, praca wakacje, staże, praktyki {/block}
{block name=metadescription}Jeżeli szukasz pracy na wakacje, stażu, praktyki lub nawet pracy którą można wykonywać w domu sprawdź Nasza ofertę. Zapraszamy do zapoznania się z Naszą ofertą, która została przygotowana specjalnie dla Państwa.{/block}

{block name=body}

    <div class="how-works">
        
        <div class="jaktodziala">
            <div id="overflow" style="position: absolute; top:0px; left: 0px; width: 980px; height: 447px; z-index: 110000; display: none;"></div>
            <div class="info">
                <div class="closeBtn"></div>
                <img id="pracujTerazImg" src="{$smarty.const.HOST}view/images/jaktodziala-pracujteraz-img.jpg" />
                <img id="letsWorkTogetherImg" src="{$smarty.const.HOST}view/images/jaktodziala-zapros-przyjaciol.png" />
                <img id="oneClickImg" src="{$smarty.const.HOST}view/images/jaktodziala-oneclick-img.jpg" />
            </div>
            
            <table style="width: 900px; margin: 0px   auto; padding-bottom: 20px;">
                <tr>
                    <td align="center" width="33%"><div class="pracujTerazBtn" id="pracujTeraz"></div></td>
                    <td align="center" width="33%"><div class="oneClickBtn" id="oneClick"></div></td>
                    <td align="center" width="33%"><div class="letsWorkTogetherBtn" id="letsWorkTogether"></div></td>
                </tr>
                <tr>
                    <td align="center" height="100px"><img src="{$smarty.const.HOST}view/images/jaktodziala-pracujteraz-text.jpg" /></td>
                    <td align="center" height="100px"><img src="{$smarty.const.HOST}view/images/jaktodziala-oneclick-text.jpg" /></td>
                    <td align="center" height="100px"><img src="{$smarty.const.HOST}view/images/jaktodziala-letsworktogether-text.jpg" /></td>
                </tr>
            </table>
        </div>
        
        <a href="{$smarty.const.HOST}rejestracja"><span class="btn btn-large btn-danger" style="margin: 10px auto; display: block; text-decoration: none; float: right;">Dołącz do nas i zacznij zarabiać! <i class="icon-white icon-chevron-right"></i></span></a>
                
        <div class="clear"></div>
        
        <h1 style="margin-top: 25px; font-weight: normal">Zobacz jak to działa?</h1>
        
        <div style="margin: 15px auto; width: 560px;">
            <iframe width="560" height="315" frameborder="0" allowfullscreen="" src="http://www.youtube.com/embed/yzA8D6_ZwQc?rel=0"></iframe>
        </div>

    </div>
{/block}

{block name=additionalJavaScript}
<script>
    
    $('#pracujTeraz').click(function() {
        $('.jaktodziala .info').show().find('#pracujTerazImg').stop().animate({ opacity: 1 });
        $('#overflow').show();
    });
    
    $('#oneClick').click(function() {
        $('.jaktodziala .info').show().find('#oneClickImg').stop().animate({ opacity: 1 });
        $('#overflow').show();
    });
    
    $('#letsWorkTogether').click(function() {
        $('.jaktodziala .info').show().find('#letsWorkTogetherImg').stop().animate({ opacity: 1 });
        $('#overflow').show();
    });

    $('#overflow').click(function() {
        $('#overflow').hide();
        $('.jaktodziala .info img').stop().animate({ opacity: 0 }, function() { $('.jaktodziala .info').hide(); });
    });
    
</script>
{/block}