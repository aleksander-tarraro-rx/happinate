{extends file="index.tpl"}

{block name=body}
    <div class="left-column">
    	<img src="{$smarty.const.HOST}view/images/login.png">
    </div>
    
    <div class="right-column">
        <form action="" method="post">
        	<div class="input-box right" style="padding:0px;">
                <label>
                    <span class="input-box-label">Twój adres email</span>
                    <input type="text" disabled="disabled" name="reminder-email" value="{if isset($user_email)}{$user_email}{/if}" autocomplete="off">
                </label>
                <label>
                    <span class="input-box-label">Proszę wpisać nowe hasło</span>
                    <input type="password" name="reminder-password" placeholder="Wpisz nowe hasło" value="" autocomplete="off">
                </label>
            </div>
            <input type="submit" class="happi-big-btn" style="width:330px; margin: 10px 0px 0px 0px;" name="reminder-submit" value="ZMIEŃ HASŁO">
        </form>
    </div>
    
    <div class="clear"></div>
{/block}