{extends file="index.tpl"}

{block name=body}
    <div class="left-column">
    	<img src="{$smarty.const.HOST}view/images/login.png">
    </div>
    
    <div class="right-column">
        <form action="" method="post">
        	<div class="input-box right" style="padding:0px;">
                <label>
                    <span class="input-box-label">Email</span>
                    <input type="text" name="reminder-email" placeholder="Wpisz email" value="" autocomplete="off">
                </label>
            </div>
            <input type="submit" class="happi-big-btn" style="width:330px; margin:10px 0px 0px 0px;" name="reminder-submit" value="PRZYPOMNIJ HASŁO">
        </form>
    </div>
    
    <div class="clear"></div>
{/block}