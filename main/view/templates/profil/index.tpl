{extends file="index.tpl"}

{block name=prebody}
    {if $_user && !empty($_user) && $_user->getUserType()==1}
    <div id="inviteFriend" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">

     <div class="modal-body">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <form action="" class="main-form"  method="post" id="inviteFriendForm">
            <input type="hidden" name="inviteFriend" value="1" />
            <p>Udostępnij tego pracodawcę znajomemu!</p>
            <hr>
            {assign var=friendList value=$_user->getFriendsList()}
            {if $friendList}    
                <div class="row">
                {foreach from=$friendList item=user}
                    <div class="span4"><label><input type="checkbox" name="friends[]" value="{$user.user_email}" /> {$user.user_email}</label></div>
                {/foreach}
                </div>
            {else}
                Niestety nie masz żadnych znajomych.
            {/if}
        </form>
     </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        {if $friendList}
            <a href="javascript:;" onclick="$('#inviteFriendForm').submit();" class="btn btn-success">Prześlij znajomym wizytówkę tego pracodawcy</a>
        {else}
            <a href="{$smarty.const.HOST}moje-konto/znajomi" class="btn btn-success">Zaproś znajomych!</a>
        {/if}
      </div>
    </div>
    {/if}
{/block}

{block name=body}
    <div class="profile">
        <div class="row">
            <div class="span10">
                <h2 style="min-height: 50px;">{$profile.user_profile_name}</h2>
            </div>
        </div>
        <div class="row">
            <div class="span3">
                {if $profile.user_logo}<p><img src="{$smarty.const.HOST}upload/userLogo/{$profile.user_logo}" class="logo" alt="{$profile.user_profile_name}" /></p>{/if}
            </div>
            <div class="span3 adres">
                {if $profile.user_phone}<p>telefon: {$profile.user_phone}</p>{/if}
                {if $profile.user_adress_street}<p><a href="">{$profile.user_adress_street}</a></p>{/if}
                {if $profile.user_adress_zip_code || $profile.user_adress_city}<p>{$profile.user_adress_city} {$profile.user_adress_zip_code}</p>{/if}
                {if isset($userType) && $userType==1}
                <div class="notice-watch">
                    {if !$model->checkWatch($profile.user_id, $uid)}
                        <a href="javascript:;" id="watch-{$profile.user_id}" class="happi-big-btn" style="padding:4px 2px;font-size:12px;width:170px;"><i class="icon-ok-sign icon-white"></i> &nbsp; Obserwuj pracodawcę</a>
                    {else}
                        <a href="javascript:;" id="unwatch-{$profile.user_id}" class="happi-big-btn" style="padding:4px 2px;font-size:12px;width:170px;"><i class="icon-minus icon-white"></i> &nbsp; Usuń z obserwowanych</a>
                    {/if}
                </div>
                <div style="margin-right: 350px; top: 10px;" class="notice-share">
                    <a style="padding:4px 2px;font-size:12px;width:160px;" class="happi-big-btn" data-target="#inviteFriend" data-toggle="modal" href="#"><i class="icon-user icon-white"></i> Udostępnij znajomemu!</a>
                </div>
                {/if}
                <div class="notice-share">
                    <a href="#" class="happi-big-btn" style="padding:4px 2px;font-size:12px;width:100px;"><i class="icon-share icon-white"></i> &nbsp; Udostępnij</a>
                    <ul>
                        {if $profile.user_profile_url}
                            {assign var="url" value=$profile.user_profile_url}
                        {else}
                            {assign var="url" value={$profile.user_id}|md5}  
                        {/if}
                        <li><a href="https://www.facebook.com/sharer/sharer.php?s=100&p[images][0]={"http://{$smarty.server.HTTP_HOST}/upload/userProfile/{$profile.user_logo}"|urlencode}&p[title]={"happinate.com - {$profile.user_profile_name}"|urlencode}&p[url]={"http://{$smarty.server.HTTP_HOST}/profile/p/{$url}"|urlencode}&p[summary]={(($profile.user_info|html_entity_decode)|strip_tags)|urlencode}" target="_blank" class="facebook"></a></li>
                        <li><a href="https://twitter.com/intent/tweet?source=webclient&text={"http://{$smarty.server.HTTP_HOST}/profile/p/{$url}"|urlencode}" target="_blank" class="twitter"></a></li>
                        <li><a href="mailto:podaj@email.pl?subject={"happinate.com - {$profile.user_profile_name}"|urlencode}&body=Sprawdź+tego+pracodawcę+{"http://{$smarty.server.HTTP_HOST}/profile/p/{$url}"}" target="_blank" class="mail"></a></li>
                    </ul>
                </div>
                    
                <div class="clear"></div>
            </div>
            
            <div class="span4">
                {if $profile.user_fanpage}
		<div class="simple-fb-like">
                    <div class="fb-like" data-href="https://{$profile.user_fanpage}" data-send="false" data-width="315" data-show-faces="true" data-font="tahoma"></div>
		</div>
                {/if}
                <h3>Obserwujących: {$watchers|count}</h3>
            </div>
        </div>
                    
        <div class="clear"></div>
        
        <div class="row" style="margin-top: 10px;">
            <div class="span6 profileimg" style="position: relative;">
                {if $profile.user_profile_img_info}<div class="profile_img_info">{$profile.user_profile_img_info|html_entity_decode}</div>{/if}
                {if $profile.user_profile_img}<table width="100%" height="100%"><tr><td align="center" valign="middle"><img src="{$smarty.const.HOST}upload/userProfile/{$profile.user_profile_img}" alt="{$profile.user_profile_name}" /></td></tr></table>{/if}
            </div>
            <div class="span4">
                {if $profile.user_www}
                    <a href="http://{$profile.user_www}" target="_blank" class="box link"></a>
                {else}
                    <span class="box link inactive"></span>
                {/if}
                {if $profile.user_email}
                    <a href="mailto:{$profile.user_email}" target="_blank" class="box email"></a>
                {else}
                    <span class="box email inactive"></span>
                {/if}
                {if $profile.user_fanpage}
                    <a href="http://{$profile.user_fanpage}" target="_blank" class="box fb"></a>
                {else}
                    <span class="box fb inactive"></span>
                {/if}
                
                <div class="clear"></div>
                <div class="bigBox">
                    <div class="text">{$profile.user_info|html_entity_decode}</div>
                    <div class="more">czytaj więcej <i class="icon icon-white icon-chevron-down"></i></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
                    
        <div class="clear"></div>
        
        <div class="row" style="margin-top: 10px;">
            {if isset($jobList) && $jobList}
            <div class="span6 oferty-pracy">
                <h2>Oferty dodane przez tego pracodawcę</h2>
                <div class="karusela-all" id="oferty-pracy">
                    {if isset($jobList) && $jobList}
                    {foreach from=$jobList item=job}
                    {if $job.job_publish_date_end|strtotime > {$smarty.now}}
                    <div class="one-notice{if $job.job_publish_date_end|strtotime < {$smarty.now}} off{/if}{if $job.job_main_page==1} wyroznienie{elseif $job.job_main_page==2} wyroznienie2{/if}{if isset($job.job_url)} zewnetrzne{/if}">

                        <div class="one-notice-text-box">
                            {if isset($job.job_url)}
                            <p class="notice-title"><a href="{$smarty.const.HOST}praca/agencja/{$job.job_id}/{$job.job_url_text}">{$job.job_position}</a></p>
                            {else}
                            <p class="notice-title"><a href="{$smarty.const.HOST}praca/{$job.job_id}/{$job.job_url_text}">{$job.job_position}</a></p>
                            {/if}

                            {if !isset($job.job_city)}
                                <p class="notice-job-area">{$job.job_employeer_place_direct_city}</p>
                            {else}
                                <p class="notice-job-area">{$job.job_city}</p>
                            {/if}
                        </div>

                        <div class="one-notice-logo">
                            {if $job.job_logo}
                                {if isset($job.job_url)}
                                    <img src="{$job.job_logo}" />
                                {else}
                                    {if $job.job_logo == 'user'}
                                        <img src="{$smarty.const.HOST}upload/userLogo/{$model->getUserLogo($job.job_added_by)}" />
                                    {else}
                                        <img src="{$smarty.const.HOST}upload/jobLogo/{$job.job_logo}" />
                                    {/if}
                                {/if}
                            {/if}
                        </div>

                        <div class="one-notic-pay">
                            {if isset($job.job_url)}

                            {else}
                            {if $job.job_hide_salary}

                            {elseif $job.job_volunteering}
                                <p class="value">0<small>,00</small></p>
                                <p class="rate">zł/godzine</p>
                            {else}
                                {if $job.job_rate_min == $job.job_rate_max}
                                    {assign var=rate value="."|explode:$job.job_rate_min}
                                    <p class="value">{$rate.0},<small>{if isset($rate.1)}{$rate.1}{if strlen($rate.1)==1}0{elseif strlen($rate.1)==0}00{/if}{else}00{/if}</small></p>
                                    <p class="rate">zł/godzinę</p>
                                {else}
                                    {assign var=rate value=ceil(($job.job_rate_min + $job.job_rate_max) / 2)}
                                    <p class="value">{$rate}<small>,00</small></p>
                                    <p class="rate">zł/godzinę</p>
                                {/if}
                            {/if}
                            {/if}
                      </div>

                      {if $job.job_main_page==2}
                      <div class="workNow">
                          <img width="50px" height="50px" style="margin-top: 10px; margin-right: 10px;" src="{$smarty.const.HOST}view/images/work-now-logo.png" />
                      </div>
                      {elseif $job.job_main_page==1}
                      <div class="workNow">
                          <img width="50px" height="50px" style="margin-top: 10px; margin-right: 10px;" src="{$smarty.const.HOST}view/images/super-oferta.png" />
                      </div>
                      {/if}

                      {if isset($job.job_url)}
                      <div class="offer-zewn"></div>
                      {/if}

                    </div>
                    {/if}
                    {/foreach}
                    {else}
                        -- brak ofert pracy --
                    {/if}
                </div>
                <div class="clear" style="height: 10px;"></div>
                {if isset($jobList) && $jobList && count($jobList)>3}<button class="btn btn-success" style="float: right;">Zobacz wszystkie oferty <i class="icon icon-chevron-down icon-white"></i></button>{/if}
            </div>
            {else}
            <div class="span6">
                <h3>Ten pracodawca nie posiada aktualnych ofert pracy.</h3>
            </div>
            {/if}
            <div class="span4">
                {if $profile.user_profile_yt_movie}
                    {assign var=yt value="?v="|explode:$profile.user_profile_yt_movie}
                    {if !isset($yt.1)}
                        {assign var=yt value="youtu.be/"|explode:$profile.user_profile_yt_movie}
                    {/if}
                    <iframe width="368" height="235" src="//www.youtube.com/embed/{$yt.1}" frameborder="0" allowfullscreen></iframe>
                {/if}
            </div>
        </div>     
    </div>
{/block}