{extends file="index.tpl"}

{block name=prebody}
    {if $_user && !empty($_user) && $_user->getUserType()==1}
    <div id="send" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">

     <div class="modal-body">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <form action="" class="main-form"  method="post" id="inviteFriendForm">
            <input type="hidden" name="inviteFriend" value="1" />
            <input type="hidden" name="name" value="{$profile.user_name} {$profile.user_surname}" />
            <input type="hidden" name="email" value="{$profile.user_email}" />
            <p>Poleć tego znajomego pracodawcy który Cię dodał do ulubionych!</p>
            <hr>
            {if isset($friendList) && $friendList}    
                <div class="row">
                {foreach from=$friendList item=user}
                    <div class="span4"><label><input type="checkbox" name="cand[]" value="{$user.user_email}" /> {if $user.user_profile_name}{$user.user_profile_name}{else}{$user.user_email}{/if}</label></div>
                {/foreach}
                </div>
                <div class="row" style="margin-top: 20px;">
                    <div class="span4">
                        <span class="input-box-label">Wpisz wiadomość dla pracodawców</span>
                        <textarea name="info" rows="5" style="resize: none; margin-bottom: 10px;width: 513px; height: 40px; margin-top: 10px;">{if isset($smarty.post.info)}{$smarty.post.info}{else}{$userProfile.user_info}{/if}</textarea>
                    </div>
                </div>
            {else}
                Niestety żadne pracodawca nie dodał Cię do listy kandydatów ulubionych.
            {/if}
        </form>
     </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        {if $friendList}
            <a href="javascript:;" onclick="$('#inviteFriendForm').submit();" class="btn btn-success">Poleć tego znajomego!</a>
        {/if}
      </div>
    </div>
    {/if}
{/block}

{block name=body}
    <div class="profile-cv">

        {if isset($profil) && $profil}
            <a href="{$smarty.const.HOST}moje-konto" class="button">Powrót do profilu >></a>
        {/if}
        
        {if $_user && !empty($_user) && $_user->getUserType()==1 && !$myownprofile}
            <a href="javascript:;" class="button" data-target="#send" data-toggle="modal">Poleć tego kandydata pracodawcy który dodał Cię do ulubionych</a>
        {/if}
        
        <div class="box1">

            <div class="avatar">
                {if $profile.user_logo}
                    <img src="{$smarty.const.HOST}upload/userLogo/{$profile.user_logo}" alt="Twoje zdjęcie profilowe!" />
                {else} 
                    <img src="{$smarty.const.HOST}view/images/unknown_user.png" alt="Twoje zdjęcie profilowe!" />
                {/if}
            </div>

            <div class="adress">
                <div class="name">{if !empty($profile.user_name) || !empty($profile.user_surname)}{$profile.user_name} {$profile.user_surname}{else}<i>uzupełnij dane</i>{/if}</div>
                {if !empty($profile.user_adress_street) || !empty($profile.user_adress_city) || !empty($profile.user_adress_zip_code)}
                    <div class="adres">
                        {$profile.user_adress_street} {$profile.user_adress_city} {$profile.user_adress_zip_code}
                    </div>
                {/if}
                {if $profile.user_email}<div class="email">{$profile.user_email}</div>{/if}
                {if $profile.user_phone}<div class="phone">{$profile.user_phone}</div>{/if}
                {if $profile.user_date_of_birth}<div class="urodz">ur. {$profile.user_date_of_birth}</div>{/if}
            </div>
            
            <div class="clear"></div>
            
        </div>

        {if $profile.user_main_tag_id}    
        <div class="doswiadczenie" style="padding-bottom: 0px;">

            <div class="header" style="float: left; ">
                WYBRANE PRZEZ UŻYTKOWNIKA RODZAJE PRAC
            </div>
            
            <div style="float: left; padding: 19px 19px 0px 19px; line-height: 150%;">
                <p><b>Kategoria główna: </b> {$model->getTagName($profile.user_main_tag_id)}</p>
                <p><b>Kategorie dodatkowe: </b> 
                {foreach from=$userTags item=tag name=tags}
                    {$model->getTagName($tag.tag_id)}{if !$smarty.foreach.tags.last}, {/if}
                {/foreach}
                </p>
            </div>
            
            <div class="clear"></div>
        </div>
        {/if}  
        
        {if $userExperience}    
        <div class="doswiadczenie">

            <div class="header" style="{if isset($userBgCVColors.0)}background-color: {$userBgCVColors.0};{/if}{if isset($userCVColors.0)}color: {$userCVColors.0};{/if}">
                DOŚWIADCZENIE
            </div>

            
            {foreach from=$userExperience item=exp}
            <div class="item">
                <p><b style="font-size: 16px">{$exp.experience_company}</b><p>
                <p><b>{$exp.experience_position}</b> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <b>&#8226;</b> {$exp.experience_start} - {if $exp.experience_till_now}teraz{else}{$exp.experience_end}{/if}</p>
                <p>{htmlspecialchars_decode($exp.experience_opis)}</p>
            </div>
            {/foreach}
            
        </div>
        {/if}
               
        {if $userEducation}
        <div class="doswiadczenie">

            <div class="header" style="{if isset($userBgCVColors.1)}background-color: {$userBgCVColors.1};{/if}{if isset($userCVColors.1)}color: {$userCVColors.1};{/if}">
                EDUKACJA
            </div>

            {foreach from=$userEducation item=edu}
            <div class="item">
                <p><b style="font-size: 16px">{$edu.education_name}</b><p>
                <p><b>{$edu.education_field}</b> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <b>&#8226;</b> {$edu.education_start} - {if $edu.education_till_now}teraz{else}{$edu.education_end}{/if}</p>
            </div>
            {/foreach}

        </div>
        {/if}
            
        {if $userLangs}             
        <div class="doswiadczenie">

            <div class="header" style="{if isset($userBgCVColors.2)}background-color: {$userBgCVColors.2};{/if}{if isset($userCVColors.2)}color: {$userCVColors.2};{/if}">
                JĘZYKI
            </div>
            
            {foreach from=$userLangs item=lang}
            <div class="item">
                <p><b>{$lang.lang_name}</b> - {$lang.lang_level}<p>
            </div>
            {/foreach}
            
        </div>
        {/if}
            
        {if $userOtherSkill}           
        <div class="doswiadczenie">

            <div class="header" style="{if isset($userBgCVColors.4)}background-color: {$userBgCVColors.4};{/if}{if isset($userCVColors.4)}color: {$userCVColors.4};{/if}">
                UMIEJĘTNOŚCI
            </div>
                
            {foreach from=$userOtherSkill item=skill}
            <div class="item">
                <p><b>{$skill.skill_name}</b><p>
                {if $skill.skill_opis}<p><small>opis</small> {$skill.skill_opis}</p>{/if}
            </div>
            {/foreach}
            
        </div>   
        {/if}
        
        {if $userTraining}
        <div class="doswiadczenie">

            <div class="header" style="{if isset($userBgCVColors.3)}background-color: {$userBgCVColors.3};{/if}{if isset($userCVColors.3)}color: {$userCVColors.3};{/if}">
                SZKOLENIA
            </div>
            
            {foreach from=$userTraining item=tra}
            <div class="item">
                <p><b>{$tra.training_name}</b><p>
                <p><small>data zakończenia</small> {$tra.training_end}</p>
            </div>
            {/foreach}
        
        </div>
        {/if}

        {if $userSkill}    
        <div class="doswiadczenie">

            <div class="header" style="{if isset($userBgCVColors.5)}background-color: {$userBgCVColors.5};{/if}{if isset($userCVColors.5)}color: {$userCVColors.5};{/if}">
                INFORMACJE DODATKOWE
            </div>
            
            <div class="item">
                {if $userSkill.skill_driving_a || $userSkill.skill_driving_a1 || $userSkill.skill_driving_b || $userSkill.skill_driving_b1 || $userSkill.skill_driving_c || $userSkill.skill_driving_c1 || $userSkill.skill_driving_d || $userSkill.skill_driving_d1 || $userSkill.skill_driving_be || $userSkill.skill_driving_c1e || $userSkill.skill_driving_ce || $userSkill.skill_driving_d1e || $userSkill.skill_driving_de || $userSkill.skill_driving_t}
                <p><b>Prawo jazdy kategorii:</b> {if $userSkill.skill_driving_a} A {/if}{if $userSkill.skill_driving_a1} A1 {/if}{if $userSkill.skill_driving_b} B {/if}{if $userSkill.skill_driving_b1} B1 {/if}{if $userSkill.skill_driving_c} C {/if}{if $userSkill.skill_driving_c1} C1 {/if}{if $userSkill.skill_driving_d} D {/if}{if $userSkill.skill_driving_d1} D1 {/if}{if $userSkill.skill_driving_be} BE {/if}{if $userSkill.skill_driving_c1e} C1E {/if}{if $userSkill.skill_driving_ce} CE {/if}{if $userSkill.skill_driving_de} D1E {/if}{if $userSkill.skill_driving_d1e} DE {/if}{if $userSkill.skill_driving_t} T {/if}<p>
                {/if}
                {if $userSkill.skill_sanel_owner}
                <p>Posiadam <b>książeczkę sanepidu</b>{if $userSkill.skill_sanel_not_expired} i jest ona ważna{/if}.</p>
                {/if}
                {if $userSkill.skill_welding_tig || $userSkill.skill_welding_mig || $userSkill.skill_welding_mag}
                <p><b>Uprawnienia elektryczne:</b>{if $userSkill.skill_welding_tig} TIG {/if}{if $userSkill.skill_welding_mig} MIG {/if}{if $userSkill.skill_welding_mag} MAG {/if}</p>
                {/if}
                {if $userSkill.skill_forklift || $userSkill.skill_forklift_big}
                <p><b>Wózki widłowe:</b>{if $userSkill.skill_forklift} wózki widłowe {/if}{if $userSkill.skill_forklift_big} wysoki skład {/if}</p>
                {/if}
            </div>
            
        </div>     
        {/if}    
        
        {if $profile.user_hobby}
        <div class="doswiadczenie">

            <div class="header" style="{if isset($userBgCVColors.6)}background-color: {$userBgCVColors.6};{/if}{if isset($userCVColors.6)}color: {$userCVColors.6};{/if}">
                ZAINTERESOWANIA
            </div>

            <div class="item">
                <p>{htmlspecialchars_decode($profile.user_hobby)}<p>
            </div>
            
        </div>
        {/if}
{/block}
