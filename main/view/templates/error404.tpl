<!doctype html>
<html lang="pl">
<head>
<meta charset="utf-8">
<title>{block name=metatitle}Praca dorywcza, tymczasowa oraz sezonowa - happinate{/block}</title>
<meta name="description" content="{block name=metadescription}Zapraszamy wszystkie osoby szukające pracy nie zależnie czy ma być ona sezonowa czy dorywcza. Na Naszej stronie znajdują się oferty pracy tymczasowej wśród których każdy znajdzie coś dla siebie.{/block}" />
<meta name="keywords" content="{block name=metakeywords}Agencja pracy tymczasowej,Praca tymczasowa,Praca dorywcza,Oferty pracy,Agencja pracy,Dodatkowa praca,Praca sezonowa,Praca MIASTO (Poznań),Praca,Wolontariat,Wolontariat Poznań{/block}" />
<meta name="robots" content="index,follow,all" />
<meta name="revisit-after" content="2 days" />
<meta name="publisher" content="modernfactory.pl" />
<meta name="rating" content="general" />

<meta property="og:title" content="happinate – praca dorywcza i tymczasowa" />
<meta property="og:image" content="{$smarty.const.HOST}view/images/happinate-face-img.png" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="320" />
<meta property="og:image:height" content="320" />
<meta property="og:description" content="" />

<link rel="stylesheet" href="{$smarty.const.HOST}view/css/libs/bootstrap.min.css">
<link rel="stylesheet" href="{$smarty.const.HOST}view/css/libs/bootstrap-responsive.min.css">
<link rel="stylesheet" href="{$smarty.const.HOST}view/css/libs/html5reset-1.6.1.css">
<link rel="stylesheet" href="{$smarty.const.HOST}view/css/libs/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="{$smarty.const.HOST}view/css/libs/chosen.css">
<link rel="stylesheet" href="{$smarty.const.HOST}view/css/happinate.tabels.css">
<link rel="stylesheet" href="{$smarty.const.HOST}view/css/happinate.forms.css">
<link rel="stylesheet" href="{$smarty.const.HOST}view/css/happinate.css">
<link rel="stylesheet" href="{$smarty.const.HOST}view/css/custom.css">
{block name=additionalCSS}{/block}
{if $smarty.const.DEBUG}<link rel="stylesheet" href="{$smarty.const.HOST}view/css/debug.css">{/if}
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <style>
        .underline-header-content{
            display: none;
        }
    </style>
<![endif]-->
<link rel="shortcut icon" href="{$smarty.const.HOST}view/images/favicon.png">
</head>
<body>

{include file="_partials/nieprzeczytaneKalendarz.tpl"}

{if isset($_user) && $_user && $_user->getUserType()==1 && !$_user->checkIfUserHasTags() && (!isset($smarty.session.popup) || !$smarty.session.popup)}
    <div class="overlayer"></div>
    <div class="overlayerPopup">
        <div class="close"></div>
        <form action="{$smarty.const.HOST}moje-konto" method="post">
            <input type="hidden" name="update-profile" value="1" />
            <input type="hidden" name="profileEditType" value="1" />
            <div class="small-tags">
                <div class="row-fluid">
                    <div class="span3" style="color:#fff;"><label for="2">Rodzaj pracy <span class="red-stars">*</span></label></div>
                    <div class="span7">
                        <div class="multiple-select-input">
                            <select data-placeholder="Wybierz tagi" multiple="" name="tags[]" class="chosen-select" id="2" style="width:410px;">
                                <option></option>
                                {foreach from=$_user->getTags() item=tag}
                                    <option {if isset($smarty.post.tags) && is_array($smarty.post.tags) && in_array($tag.tag_id, $smarty.post.tags)} selected="selected"{elseif isset($userTags) && is_array($userTags) && in_array($tag.tag_id, $userTags)} selected="selected"{/if} value="{$tag.tag_id}">{$tag.tag_name}</option>
                                {/foreach}
                            </select>
                        </div>
                        <p class="small-tip" style="margin: 10px -20px 0 0;">Zaznacz przynajmniej 3 rodzaje prac.</p>
                    </div>
                </div>
            </div>
            {if isset($error)}
                <div class="alert  alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {$error}
                </div>
            {/if}
            <input type="submit" class="overlayerButton" value="ZAPISZ" > 
        </form>
    </div>
{/if}
<section class="center">
    <div class="logo-main"><a href="{$smarty.const.HOST}"><img src="{$smarty.const.HOST}view/images/logo-happinate.png" {*style="height: 51px;"*} alt="Happinate"></a></div>
    
    {if !isset($_user) || empty($_user)}
        <div class="login-box" style="margin-left: 10px;">
            <a href="{$smarty.const.HOST}rejestracja"><span class="happi-btn">Rejestracja <i class="icon-plus icon-white"></i></span></a>
        </div>
        <div class="login-box">
            <span class="happi-btn">Zaloguj <i class="icon-chevron-right icon-white"></i></span>
            <div class="register-box">
                <a href="#" class="login-with-facebook" onClick="fbLoginUser(); return false;"></a>
                <div class="clear" style="height:5px;"></div>
                <hr>
                <p class="login-or">lub</p>
                <div class="clear" style="height:5px;"></div>
                <form class="login-form" action="{$smarty.const.HOST}zaloguj-sie" method="post">
                    <div class="input-text">
                        <i class="icon-user"></i>
                        <input type="text" name="login-email" placeholder="Email" value="">
                    </div>
                    <div class="input-text">
                        <i class="icon-lock"></i>
                        <input type="password" name="login-pass" placeholder="Hasło" value="">
                    </div>
                    <input type="submit" name="login-submit" class="happi-btn submit-login" value="Zaloguj">
                </form>
                <a href="{$smarty.const.HOST}rejestracja" class="register">Utwórz konto</a>
                <a href="{$smarty.const.HOST}przypomnij-haslo" class="register">Przypomnij hasło</a>
            </div>
        </div>
    {else}
        <div class="logout-box">
            <a class="happi-btn" href="{$smarty.const.HOST}moje-konto">
                Mój profil <i class="icon-user icon-white"></i>
                {if $calendarEvents}
                    <span class="badge badge-important calendarUnreadTrigger">{count($calendarEvents)}</span>
                {/if}
            </a>
            <a class="happi-btn" href="{$smarty.const.HOST}wyloguj">Wyloguj <i class="icon-off icon-white"></i></a>
        </div>       
    {/if}
    
    <div class="clear"></div>
    
    <nav class="main-nav">
    	<ul>
            <li class="home{if empty($smarty.get.url)} active{elseif $controllerName=='informacjeController'} active{/if}"><a href="{$smarty.const.HOST}"><i class="icon-house"></i></a></li>
            <li{if !empty($smarty.get.url) && $controllerName=='jakToDzialaController'} class="active"{/if}><a href="{$smarty.const.HOST}jak-to-dziala">Jak to działa?</a></li>
            <li{if !empty($smarty.get.url) && ($controllerName=='ofertyPracyController' || $controllerName=='pracaController')} class="active"{/if}><a href="{$smarty.const.HOST}oferty-pracy/1/">Oferty pracy</a></li>
            <li{if !empty($smarty.get.url) && ($controllerName=='dajOgloszenieController' || ($controllerName=='mojeKontoController' && $actionName=='dodajOgloszenieAction'))} class="active"{/if}><a href="{$smarty.const.HOST}daj-ogloszenie">Daj ogłoszenie</a></li>
            <li{if !empty($smarty.get.url) && $controllerName=='mapaController'} class="active"{/if}><a href="{$smarty.const.HOST}mapa">Mapa</a></li>
            <li><a href="http://happichill.com/" target="_blank">Blog</a></li>
            <li{if !empty($smarty.get.url) && $controllerName=='kontaktController'} class="active"{/if}><a href="{$smarty.const.HOST}kontakt">Kontakt</a></li>
        </ul>
    </nav>
</section>
    
<div class="underline-menu"></div>

<section class="main-content">

    <h1 class="error-404">Błąd 404</h1>
    <div class="error-404-img">
        <img alt="Error 404 - happinate" src="/view/images/error-404.png">
    </div>
    <div class="error-text">
        <p><em>Nic się nie stało...</em><br>wracaj na stronę główną i surfuj dalej!</p>
    </div>
    <a href="/" class="error-btn-home">Powrót</a>

</section>

<section class="main-content{if empty($smarty.get.url)} home-content{else}{if $controllerName=='zalogujSieController' || $controllerName=='rejestracjaController' || $controllerName=='informacjeController'} home-content{/if}{/if}">
    {if isset($flashMsg.success)}
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {$flashMsg.success}
        </div>
    {/if}
    {if isset($flashMsg.warning)}
        <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {$flashMsg.warning}
        </div>
    {/if}
    {if isset($flashMsg.error)}
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {$flashMsg.error}
        </div>
    {/if}
    {if isset($error)}
        <div class="alert  alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {$error}
        </div>
    {/if}
    {block name=body}{/block}
</section>


<footer>
    <hr>
    <div class="center">
        <ul class="footer-nav">
            <li><a href="{$smarty.const.HOST}informacje/o-nas">O nas</a></li>
            <li><a href="{$smarty.const.HOST}informacje/wartosci">Wartości</a></li>
            <li><a href="{$smarty.const.HOST}informacje/dla-mediow">Dla mediów</a></li>
            <li><a href="{$smarty.const.HOST}informacje/faq">FAQ</a></li>
            <li><a href="{$smarty.const.HOST}cennik">Cennik</a></li>
        </ul>
        <ul class="footer-nav">
            <li><a href="{$smarty.const.HOST}informacje/mapa-kategorii">Mapa kategorii</a></li>
            <li><a href="{$smarty.const.HOST}informacje/mapa-miejscowosci">Mapa miejscowości</a></li>
            <li><a href="http://bloghappi.com/" target="_blank">Blog happi</a></li>
            <li><a href="http://happichill.com/" target="_blank">Blog happichill</a></li>
        </ul>
        <ul class="footer-nav">
            <li><a href="{$smarty.const.HOST}informacje/polityka-prywatnosci">Polityka prywatności</a></li>
            <li><a href="{$smarty.const.HOST}informacje/regulamin">Regulamin</a></li>
            <li><a href="{$smarty.const.HOST}informacje/warunki-platnosci">Warunki płatności</a></li>
            <li><a href="{$smarty.const.HOST}informacje/dane-rejestrowe">Dane rejestrowe</a></li>
        </ul>
        <div class="happi-social">
            <a href="https://www.facebook.com/happinate" target="_blank" class="facebook"></a>
            <a href="https://twitter.com/HappinatePolska" target="_blank" class="twitter"></a>
            <a href="https://plus.google.com/u/0/101450529462539804482/about" target="_blank" class="google-plus"></a>
            <a href="http://www.linkedin.com/profile/view?id=232772238" target="_blank" class="linkedin"></a>
            <a href="http://www.youtube.com/user/happinate" target="_blank" class="youtube"></a>
            <a href="mailto:hi@happinate.com" class="email"></a>
            <div class="clear" style="height:10px;"></div>
            <p class="copy">happinate © 2013</p>
        </div>
        <div class="clear" style="height:20px;"></div>
        <p class="footer-info">Właścicielem serwisu jest firma happinate sp. z o.o. z siedzibą w Poznaniu (61-816), przy ul. Ratajczaka 42/13, wpisana do KRS pod numerem 0000444418</p>
        <p class="footer-info">Serwis SMS obsługiwany przez Dotpay, usługa SMS dostępna w sieciach Era, Orange, Plus GSM, Play, Heyah, Sami Swoi</p>
    </div>
</footer>
        
<div class="livechat"><ds:livechat></ds:livechat></div>

<div id="fb-root"></div>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>if (typeof (jQuery) == 'undefined') document.write(unescape("%3Cscript " + "src='{$smarty.const.HOST}view/js/libs/jquery.min.js' %3E%3C/script%3E"));</script>
<script src="{$smarty.const.HOST}view/js/libs/jquery.ui.js"></script>
<script src="{$smarty.const.HOST}view/js/libs/jquery.ui.map.js"></script>
<script src="{$smarty.const.HOST}view/js/libs/jquery.dataTables.min.js"></script>
<script src="{$smarty.const.HOST}view/js/libs/bootstrap.min.js"></script>
<script src="{$smarty.const.HOST}view/js/libs/jquery.form.js"></script>
<script src="{$smarty.const.HOST}view/js/libs/jquery.placeholder.min.js"></script>
<script src="{$smarty.const.HOST}view/js/libs/chosen.jquery.min.js"></script>
<script src="{$smarty.const.HOST}view/js/happinate.obj.js"></script>
<script src="{$smarty.const.HOST}view/js/happinate.fb.init.js"></script>
<script src="{$smarty.const.HOST}view/js/happinate.checkbox.js"></script>
<script src="{$smarty.const.HOST}view/js/happinate.tabels.js"></script>
<script src="{$smarty.const.HOST}view/js/happinate.forms.js"></script>
<script src="{$smarty.const.HOST}view/js/happinate.forms.new.js"></script>	
<script src="{$smarty.const.HOST}view/js/happinate.google.maps.js"></script>
<script src="{$smarty.const.HOST}view/js/happinate.forms.new.js"></script>
<script src="{$smarty.const.HOST}view/js/happinate.js"></script>
<script src="{$smarty.const.HOST}view/js/calendar.js"></script>
<script src="{$smarty.const.HOST}view/js/custom.js"></script>
<script src="{$smarty.const.HOST}app/libs/ckeditor/ckeditor.js"></script>
<script src="{$smarty.const.HOST}app/libs/ckeditor/adapters/jquery.js"></script>
{if !$smarty.const.DEBUG}
<script type="text/javascript">
	var _gaq = _gaq || []; var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
	_gaq.push(['_require', 'inpage_linkid', pluginUrl]);
	_gaq.push(['_setAccount', 'UA-39041418-1']);
	_gaq.push(['_trackPageview']);
	(function () {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
<script src="https://panel.denisesystems.com/static/web/js/api/isok/denise.js?x0AL4" id="dsScriptTag" type="text/javascript"></script>
{/if}
{block name=additionalJavaScript}{/block}
{if $smarty.const.DEBUG}{include file="debug.tpl"}{/if}
</body>
</html>