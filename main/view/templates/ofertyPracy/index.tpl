{extends file="index.tpl"}

{block name=metatitle}Aktualne  oferty  pracy dla studenta - praca dodatkowa,  na  wakacje,  sezonowa,  info  praca - happinate{/block}
{block name=metadescription}Skorzystaj z ofert pracy znajdujących się na stronie www.happinate.com. Codziennie  nowe oferty pracy dla studentów na terenie Poznania i Warszawy{/block}

{block name=body}
    <div class="left-column" style="float:right;">
        {if $noJobs}
        <div class="alert alert-info"><i class="icon-info-sign icon-white" style="float: left; display: block; padding-bottom: 10px; padding-right: 10px;"></i> Nie znaleźliśmy ofert pracy spełniających wszystkie Twoje kryteria, sprawdź oferty jakie wybraliśmy dla Ciebie!</div>    
        {/if}
        <div class="karusela-all" id="oferty-pracy">
            {if isset($jobList) && $jobList}
            {foreach from=$jobList item=job}
            <div class="one-notice{if $job.job_publish_date_end|strtotime < {$smarty.now}} off{/if}{if $job.job_main_page==1} wyroznienie{elseif $job.job_main_page==2} wyroznienie2{/if}{if isset($job.job_url)} zewnetrzne{/if}">
                
                {if $_user && !empty($_user) && $_user->getUserType()==1}
                    <div id="{$job.job_id}" data-original-title="{if !$_model->checkIfAddedToFav({$job.job_id})}Usuń z ulubionych{else}Dodaj do ulubionych{/if}"  class="fav tooltip-item {if $job.job_main_page} main{/if}{if !$_model->checkIfAddedToFav({$job.job_id})} active{/if}"></div>
                {/if}
                
                <div class="one-notice-text-box">
                    {if isset($job.job_url)}
                    <p class="notice-title"><a href="{$smarty.const.HOST}praca/agencja/{$job.job_id}/{$job.job_url_text}">{$job.job_position}</a></p>
                    {else}
                    <p class="notice-title"><a href="{$smarty.const.HOST}praca/{$job.job_id}/{$job.job_url_text}">{$job.job_position}</a></p>
                    {/if}

                    {if !isset($job.job_city)}
                        <p class="notice-job-area">{$job.job_employeer_place_direct_city}</p>
                    {else}
                        <p class="notice-job-area">{$job.job_city}</p>
                    {/if}
                </div>
                    
                <div class="one-notice-logo"{if !$job.job_logo} style="line-height: 65px;{/if}>
                    <a href="{$smarty.const.HOST}profil/p/{$job.job_added_by|md5}">{$job.user_profile_name}</a><br>
                    {if $job.job_logo}
                        {if isset($job.job_url)}
                            <img src="{$job.job_logo}" />
                        {else}
                            {if $job.job_logo == 'user'}
                                <img src="{$smarty.const.HOST}upload/userLogo/{$_model->getUserLogo($job.job_added_by)}" />
                            {else}
                                <img src="{$smarty.const.HOST}upload/jobLogo/{$job.job_logo}" />
                            {/if}
                        {/if}
                    {/if}
                </div>
                
                <div class="one-notic-pay">
                    {if isset($job.job_url)}

                    {else}
                    {if $job.job_hide_salary}

                    {elseif $job.job_volunteering}
                        <p class="value">0<small>,00</small></p>
                        <p class="rate">zł/godzine</p>
                    {else}
                        {if $job.job_rate_min == $job.job_rate_max}
                            {assign var=rate value="."|explode:$job.job_rate_min}
                            <p class="value">{$rate.0},<small>{if isset($rate.1)}{$rate.1}{if strlen($rate.1)==1}0{elseif strlen($rate.1)==0}00{/if}{else}00{/if}</small></p>
                            <p class="rate">zł/godzinę</p>
                        {else}
                            {assign var=rate value=ceil(($job.job_rate_min + $job.job_rate_max) / 2)}
                            <p class="value">{$rate}<small>,00</small></p>
                            <p class="rate">zł/godzinę</p>
                        {/if}
                    {/if}
                    {/if}
              </div>
              
              {if $job.job_main_page==2}
              <div class="workNow">
                  <img width="50px" height="50px" style="margin-top: 10px; margin-right: 10px;" src="{$smarty.const.HOST}view/images/work-now-logo.png" />
              </div>
              {elseif $job.job_main_page==1}
              <div class="workNow">
                  <img width="50px" height="50px" style="margin-top: 10px; margin-right: 10px;" src="{$smarty.const.HOST}view/images/super-oferta.png" />
              </div>
              {/if}
              
              {if isset($job.job_url)}
              <div class="offer-zewn"></div>
              {/if}
              
            </div>
            {/foreach}
            {else}
                -- brak ofert pracy --
            {/if}
        </div>
        <div class="clear" style="height:3px;"></div>
        <div class="pagination">
            <ul>
                {if isset($pageCount) && $pageCount>0}
                    <li{if !isset($integerAction) || (isset($integerAction) && $integerAction<=1)} class="disabled"{/if}>{if !isset($integerAction) || (isset($integerAction) && $integerAction<=1)}<a href="#">«</a>{elseif isset($integerAction)}<a href="{$smarty.const.HOST}oferty-pracy/{$integerAction-1}{if isset($queryName) && !empty($queryName)}/{$queryName}{/if}">«</a>{else}<a href="#">«</a>{/if}</li>
                    {if isset($integerAction) && $integerAction>=6}
                        {assign var=ppp value=$integerAction - 5}
                    {else}
                        {assign var=ppp value=1}
                    {/if}
                    {assign var=c value=1}
                    {for $i=$ppp to $pageCount}
                    {if $c<=10}
                    <li{if (isset($integerAction) && $i==$integerAction) || (!isset($integerAction) && $i==1)} class="active"{/if}>{if (isset($integerAction) && $i==$integerAction) || (!isset($integerAction) && $i==1)}<a href="#">{$i}</a>{else}<a href="{$smarty.const.HOST}oferty-pracy/{$i}{if isset($queryName) && !empty($queryName)}/{$queryName}{/if}">{$i}</a>{/if}</li>
                    {assign var=c value=$c+1}
                    {/if}
                    {/for}
                    <li{if $pageCount==1 || (isset($integerAction) && $integerAction>=$pageCount)} class="disabled"{/if}>{if $pageCount==1 || (isset($integerAction) && $integerAction>=$pageCount)}<a href="#">»</a>{elseif isset($integerAction)}<a href="{$smarty.const.HOST}oferty-pracy/{$integerAction+1}{if isset($queryName) && !empty($queryName)}/{$queryName}{/if}">»</a>{else}<a href="{$smarty.const.HOST}oferty-pracy/2{if isset($queryName) && !empty($queryName)}/{$queryName}{/if}">»</a>{/if}</li>
                {/if}
            </ul>
        </div>
    </div>

    <div class="right-column" style="float:left;padding:5px 5px 0px 5px;">
        <form action="" method="post" id="filterForm">
            <input type="hidden" name="filter" value="1" />
            <div class="filtry" style="border:1px solid #ededed;">
                <h2 style="margin-left: 5px; margin-top: 5px;">Szukaj po nazwie:</h2>
                <input type="text" style="width: 250px; text-align: left;" name="search" placeholder="szukaj..." value="{if isset($search)}{$search}{/if}" autocomplete="off">
                <h2 class="tagi" id="tagi"><span class="btn" style="width: 200px; text-align: left;">Rodzaje prac <i class="icon-arrow-down" style="vertical-align: baseline; float: right;"></i></span> <small>wyczyść filtry</small></h2>
                <ul class="tagi" id="tagiMore">
                    {foreach from=$tagsList item=tag}
                        <li{if isset($tags) && is_array($tags) && !empty($tags) && in_array(strtolower(str_replace(' ', '_', $tag.tag_name)), $tags)} class="active"{/if} value="{$tag.tag_id}">• &nbsp;{$tag.tag_name}<input type="hidden" name="tags[{$tag.tag_id}]" value="{if isset($tags) && is_array($tags) && !empty($tags) && in_array(strtolower(str_replace(' ', '_', $tag.tag_name)), $tags)}1{else}0{/if}">{if isset($tags) && is_array($tags) && !empty($tags) && in_array(strtolower(str_replace(' ', '_', $tag.tag_name)), $tags)}<span class="erase-filtr"><span class="erase-icon-bg"><i class="icon-ok-sign icon-white"></i></span></span>{/if}</li>
                    {/foreach}
                </ul>  
                <h2 class="region" id="wojewodztwo"><span class="btn" style="width: 200px; text-align: left;">Województwa <i class="icon-arrow-down" style="vertical-align: baseline; float: right;"></i></span> <small>wyczyść filtry</small></h2>
                <ul class="region" id="wojewodztwoMore">
                    {foreach from=$provinceList item=city}
                        <li{if isset($cities) && is_array($cities) && !empty($cities) && in_array(strtolower(str_replace(' ', '_', $city.province_text)), $cities)} class="active"{/if} value="{$city.province_id}">• &nbsp;{$city.province_text}<input type="hidden" name="province[{$city.province_id}]" value="{if isset($cities) && is_array($cities) && !empty($cities) && in_array(strtolower(str_replace(' ', '_', $city.province_text)), $cities)}1{else}0{/if}">{if isset($cities) && is_array($cities) && !empty($cities) && in_array(strtolower(str_replace(' ', '_', $city.province_text)), $cities)}<span class="erase-filtr"><span class="erase-icon-bg"><i class="icon-ok-sign icon-white"></i></span></span>{/if}</li>
                    {/foreach}
                </ul> 
                {*

                
                <h2 class="umowa" id="umowa"><span class="btn" style="width: 200px; text-align: left;">Rodzaj umowy <i class="icon-chevron-down" style="vertical-align: baseline; float: right;"></i></span> <small>wyczyść filtry</small></h2>
                <ul class="umowa" id="umowaMore">
                    {foreach from=$contractTypeList item=type}
                        <li{if isset($contract) && is_array($contract) && !empty($contract) && in_array(strtolower(str_replace(' ', '_', $type.type_name)), $contract)} class="active"{/if} value="{$type.type_id}">• &nbsp;{$type.type_name}<input type="hidden" name="type[{$type.type_id}]" value="{if isset($contract) && is_array($contract) && !empty($contract) && in_array(strtolower(str_replace(' ', '_', $type.type_name)), $contract)}1{else}0{/if}" />{if isset($contract) && is_array($contract) && !empty($contract) && in_array(strtolower(str_replace(' ', '_', $type.type_name)), $contract)}<span class="erase-filtr"><span class="erase-icon-bg"><i class="icon-ok-sign icon-white"></i></span></span>{/if}</li>                     {/foreach}      
                </ul>
                <h2 class="praca" id="time"><span class="btn" style="width: 200px; text-align: left;">Czas pracy <i class="icon-chevron-down" style="vertical-align: baseline; float: right;"></i></span><small>wyczyść filtry</small></h2>
                
                <ul class="praca" id="timeMore">
                    
                    <li value="1"{if isset($time) && is_array($time) && in_array('stały', $time)} class="active"{/if}>• &nbsp;Stały<input type="hidden" name="time[1]" value="{if isset($time) && is_array($time) && in_array('stały', $time)}1{else}0{/if}" />{if isset($time) && is_array($time) && in_array('stały', $time)}<span class="erase-filtr"><span class="erase-icon-bg"><i class="icon-remove icon-white"></i></span> usuń filtr</span>{/if}</li>
                    
                    <li value="2"{if isset($time) && is_array($time) && in_array('elastyczny', $time)} class="active"{/if}>• &nbsp;Elastyczny<input type="hidden" name="time[2]" value="{if isset($time) && is_array($time) && in_array('elastyczny', $time)}1{else}0{/if}" />{if isset($time) && is_array($time) && in_array('elastyczny', $time)}<span class="erase-filtr"><span class="erase-icon-bg"><i class="icon-remove icon-white"></i></span> usuń filtr</span>{/if}</li>
                    
                </ul>
                *}
                <h2 style="margin-left: 5px;">Tylko, gdy stawka powyżej:</h2>
                <input type="text" name="rate-from" class="rate-from" placeholder="0.00" value="{if isset($rateFrom)}{$rateFrom}{/if}" autocomplete="off">
            </div>
            <button class="btn btn-large btn-happinate" onclick="$(this).parent().parent().submit();" style="float: right;">Pokaż <i class="icon-white icon-ok-sign"></i></button>
        </form>
    </div>

    <div class="clear"></div>
{/block}