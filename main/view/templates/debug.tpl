<div class="debug">
    <div class="name">DEBUG <a href="https://serwer1366167.home.pl/websvn/" target="_blank">rev. {$smarty.const.REVISION}</a></div>    <div class="item">
        {if $controllerName=='no controller found' || $actionName=='no action found'}<b style="color: red;">!</b> {/if}CONTROLLER
        <div class="info">
            <table>
                {if $controllerName!='no controller found'}
                <tr>
                  <th align="right">Path</th>
                  <td align="center" width="20"> : </td>
                  <td>{$controllerNamespace}</td>
                </tr>
                {/if}
                <tr>
                  <th align="right"{if $controllerName=='no controller found'} style="color: red;"{/if}>{$controllerName}</th>
                  <td align="center" width="20"> :: </td>
                  <td{if $actionName=='no action found'} style="color: red;"{/if}>{$actionName}({$queryName})</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="item">
        MODEL
        <div class="info">
            {if $modelName}
                <table>
                    <tr>
                      <th align="right">Path</th>
                      <td align="center" width="20"> : </td>
                      <td>{$modelNamespace}</td>
                    </tr>
                    <tr>
                      <th align="right">Class</th>
                      <td align="center" width="20"> : </td>
                      <td>{$modelName}()</td>
                    </tr>
                </table>
            {else}
                <span{if $modelName==NULL} style="color: olive"{/if}>no model found</span>
            {/if}
        </div>
    </div>
    {if isset($smarty.get) && is_array($smarty.get) && !empty($smarty.get)}
    <div class="item">
        GET
        <div class="info">
            <table>
            {foreach $smarty.get as $key=>$var}
                <tr>
                  <th align="right">{$key}</th>
                  <td align="center" width="20"> : </td>
                  <td>{$var}</td>
                </tr>
            {/foreach}
            </table>
        </div>
    </div>
   {/if}
   {if isset($smarty.post) && is_array($smarty.post) && !empty($smarty.post)}
    <div class="item">
        POST
        <div class="info">
            <table>
            {foreach $smarty.post as $key=>$var}
                <tr>
                  <th align="right">{$key}</th>
                  <td align="center" width="20"> : </td>
                  <td>{if is_array($var)}{$var|print_r}{else}{$var}{/if}</td>
                </tr>
            {/foreach}
            </table>
        </div>
    </div>
    {/if}
    {if isset($smarty.session) && is_array($smarty.session) && !empty($smarty.session)}
    <div class="item" id="session">
        SESSION
        <div class="info">
            <table>
            {foreach  $smarty.session as $key=>$var}
                <tr>
                  <th align="right">{$key}</th>
                  <td align="center" width="20"> : </td>
                  <td>{if is_array($var)}<pre>{$var|print_r}</pre>{else}{$var}{/if}</td>
                </tr>
            {/foreach}
                <tr>
                  <td colspan="3" height="20" valign="bottom" align="center"><a href="/index.php?clearSession=1">destroy session</a></td>
                </tr>
            </table>
        </div>    
    </div>
    {/if}
    {if isset($_files) && is_array($_files) && !empty($_files)}
    <div class="item" id="files">
        FILES
        <div class="info">
            <table>
            {foreach $_files as $key=>$var}
                <tr>
                  <th align="right">{$key}</th>
                  <td align="center" width="20"> : </td>
                  <td>{if is_array($var)}<pre>{$var|print_r}</pre>{else}{$var}{/if}</td>
                </tr>
            {/foreach}
            </table>
        </div>    
    </div>
    {/if}
    {if isset($smarty.cookies) && is_array($smarty.cookies) && !empty($smarty.cookies)}
    <div class="item">
        COOKIES
        <div class="info">
            <table>
            {foreach  $smarty.cookies as $key=>$var}
                <tr>
                  <th align="right">{$key}</th>
                  <td align="center" width="20"> : </td>
                  <td>{$var}</td>
                </tr>
            {/foreach}
            </table>
        </div>  
    </div>
    {/if}
    <div class="item">
        {if $totalTime>1}<b style="color: red;">!</b> {/if}TIME
        <div class="info">
            <table>
                <tr>
                  <th align="right">Total time</th>
                  <td align="center" width="20"> : </td>
                  <td{if $totalTime>1} style="color: red;"{/if}>{$totalTime} sec.</td>
                </tr>
            </table>
        </div>
    </div>
    {if isset($debugError)}
    <div class="item">
        <b style="color: red;">!</b> <span style="color: red;">ERRORS</span>
        <div class="info">
            <table>
            {foreach from=$debugError item=error}
                <tr>
                  <th align="right">{$error.type}</th>
                  <td align="center" width="20"> : </td>
                  <td>{$error.msg}</td>
                  <td align="center" width="20"> : </td>
                  <td>{$error.file} line {$error.line}</td>
                </tr>
            {/foreach}
            </table>
        </div>
    </div>
    {/if}
    {if isset($debugSQLError)}
    <div class="item">
        <b style="color: red;">!</b> <span style="color: red;">SQL ERRORS</span>
        <div class="info">
            <ul style="list-style: disc; padding-left:  10px;">
            {foreach from=$debugSQLError item=error}
                <li>{$error}</li>
            {/foreach}
            </ul>
        </div>
    </div>
    {/if}
    {if isset($flashMsg) && is_array($flashMsg) && !empty($flashMsg)}
    <div class="item" id="session">
        FLASH MSG
        <div class="info">
            <table>
            {foreach  $flashMsg as $key=>$var}
                <tr>
                  <th align="right">{$key}</th>
                  <td align="center" width="20"> : </td>
                  <td>{$var}</td>
                </tr>
            {/foreach}
            </table>
        </div>    
    </div>
    {/if}
</div>