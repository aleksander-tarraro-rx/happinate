<div id="calendarUnread" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Kalendarz - nieprzeczytane powiadomienia</h3>
    </div>
    <div class="modal-body">
        {if $calendarEvents}
            <table>
                {foreach from=$calendarEvents item=event}
                    <tr>
                        <td style="width: 40px; height: 30px;"><span class="label label-info"><i class="icon icon-white icon-question-sign"></i></span></td>
                        <td style="width: 70px; height: 30px; font-size: 10px;">{$event.date}</td>
                        <td style="width: 370px;"><a href="/moje-konto/oneclick/{$event.id}">{$event.title}</a></td>
                        <td><a class="btn btn-small btn-happinate" target="_blank" href="/profil/p/{$event.employeer|md5}">pracodawca</a></td>
                    </tr>
                {/foreach}
            </table>
        {else}
            -- brak nowych powiadomień --
        {/if}
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Zamknij</button>
    </div>
</div>