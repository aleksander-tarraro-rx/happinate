{extends file="index.tpl"}

{block name=body}
    <div class="left-column">
        <img src="{$smarty.const.HOST}view/images/help.png" alt="Pomożemy Ci">
    </div>
    <div class="right-column">
    	<div class="help-phone-box" style="padding:20px 30px 0px 30px;margin:0px;height:90px;">
            <p class="phone"><strong>506-052-552</strong></p>
            <p><span class="call">Zadzwoń !</span> Chętnie pomożemy.</p>
        </div>
    
    	<form id="help-form">
            <div class="input-box right" style="margin:0px auto;">
                <label>
                    <span class="input-box-label">Imię i nazwisko</span>
                    <input type="text" name="name" placeholder="Wpisz imię i nazwisko" autocomplete="off">
                </label>
            </div>
            <div class="input-box right" style="margin:0px auto;">
                <label>
                    <span class="input-box-label">Email <span class="red-text">(wymagane)</span></span>
                    <input type="text" name="email" placeholder="Wpisz adres email" autocomplete="off">
                </label>
            </div>
            <div class="input-box right" style="margin:0px auto;">
                <label>
                    <span class="input-box-label">Treść <span class="red-text">(wymagane)</span></span>
                    <textarea rows="3" name="message" placeholder="Wpisz treść"></textarea>
                </label>
            </div>
            <a href="javascript:;" onClick="$('#help-form').submit();" class="happi-btn" style="float:right;margin-top:10px;">Wyślij <i class="icon-chevron-right icon-white"></i></a>
        </form>
    </div>
    <div class="clear"></div>
{/block}