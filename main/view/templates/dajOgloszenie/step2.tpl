{extends file="index.tpl"}

{block name=prebody}
    {if $_user && $_user->getUserType()==2}
        <div class="center">
            <nav class="login-nav">
                <ul>
                    <li class="active"><a href="{$smarty.const.HOST}daj-ogloszenie"><strong>+</strong> Dodaj ogłoszenie</a></li>
                    <li><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
                    <li><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Ulubieni kandydaci</a></li>
                    <li><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
                </ul>            
            </nav>
        </div>
    {/if}
{/block}

{block name=body}
<section class="main-content">
    <form class="main-form" action="" method="post" enctype="multipart/form-data">
        <input type="hidden" id="step" name="step" value="2">
        <input type="hidden" name="trashFile" value="0" id="trashFile" />
            <h2>DANE OPCJONALNE - możesz pominąć ten krok<span class="underline-header-content" style="width:223px;"></span></h2>
            <div class="clear" style="height:25px;"></div>
            <div class="row-fluid">
                <div class="span3">Rodzaj umowy</div>
                <div class="span7">
                    <div class="select-input">
                        <div class="select-label">
                            <p class="select-value">{if isset($smarty.post.contract) && $smarty.post.contract}{$smarty.post.contract}{elseif isset($smarty.session.basket.contract) && $smarty.session.basket.contract}{$smarty.session.basket.contract}{else}Wybierz rodzaj umowy{/if}</p>
                            <input type="hidden" name="contract" value="{if isset($smarty.post.contract) && $smarty.post.contract}{$smarty.post.contract}{elseif isset($smarty.session.basket.contract)}{$smarty.session.basket.contract}{/if}">
                            <i class="icon-select-list"></i>
                        </div>
                        <ul>
                            {if $contractType}
                                {foreach from=$contractType item=contract}
                                    <li>{$contract.type_name}</li>
                                {/foreach}
                            {/if}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                    <div class="span3">Data rozpoczęcia pracy{if $_user && $_user->getUserType()==2}{if $oneclick} *{/if}{/if}</div>
                    <div class="span7">
                            <div class="data-input">
                                <input type="text" name="start_date" value="{if isset($smarty.post.start_date) && $smarty.post.start_date}{$smarty.post.start_date}{elseif isset($smarty.session.basket.start_date) && $smarty.session.basket.start_date}{$smarty.session.basket.start_date}{/if}">
                                <i class="icon-calendar-input"></i>
                            </div>
                    </div>
            </div>
            {if $_user && $_user->getUserType()==2}{if $oneclick}
            <div class="row-fluid">
                    <div class="span3" style="min-height: 5px;"></div>
                    <div class="span7" style="font-size: 12px;min-height: 5px;">
                        * Jeżeli wybierzesz opcję 1-click data rozpoczęcia pracy stanie się polem wymaganym
                    </div>
            </div>
            {/if}{/if}
            <div class="row-fluid">
                    <div class="span3">Czas pracy</div>
                    <div class="span7">
                            <div class="select-input">
                                    <div class="select-label">
                                            <p class="select-value">{if isset($smarty.post.time) && $smarty.post.time}{$smarty.post.time}{elseif isset($smarty.session.basket.time) && $smarty.session.basket.time}{$smarty.session.basket.time}{else}Wybierz czas pracy{/if}</p>
                                            <input type="hidden" name="time" value="{if isset($smarty.post.time) && $smarty.post.time}{$smarty.post.time}{elseif isset($smarty.session.basket.time)}{$smarty.session.basket.time}{/if}">
                                            <i class="icon-select-list"></i>
                                    </div>
                                    <ul>
                                            <li>Elastyczny</li>
                                            <li>Stały</li>
                                    </ul>
                            </div>
                    </div>
            </div>
            <div class="row-fluid">
                    <div class="span3"><label for="1">Ilość godzin pracy tygodniowo</label></div>
                    <div class="span7">
                            <div class="text-input">
                                    <input type="text" name="hours_per_week" id="1" {if isset($errorArray.hours_per_week) && $errorArray.hours_per_week} style="background:#EED3D7"{/if} value="{if isset($smarty.post.hours_per_week) && $smarty.post.hours_per_week}{$smarty.post.hours_per_week}{elseif isset($smarty.session.basket.hours_per_week)}{$smarty.session.basket.hours_per_week}{/if}">
                            </div>
                    </div>
            </div>
            <div class="row-fluid">
                <div class="span3"><label for="2">Ilość zmian w ciągu dnia</label></div>
                <div class="span7">
                    <div class="select-input">
                        <div class="select-label">
                            <p class="select-value">{if isset($smarty.post.time) && $smarty.post.time}{$smarty.post.time}{elseif isset($smarty.session.basket.time) && $smarty.session.basket.time}{$smarty.session.basket.time}{else}Wybierz ilość zmian{/if}</p>
                            <input type="hidden" name="shifts" id="2" value="{if isset($smarty.post.shifts) && $smarty.post.shifts}{$smarty.post.shifts}{elseif isset($smarty.session.basket.shifts)}{$smarty.session.basket.shifts}{/if}">
                            <i class="icon-select-list"></i>
                        </div>
                        <ul>
                            <li>I zmiana</li>
                            <li>II zmiany</li>
                            <li>III zmiany</li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row-fluid">
                    <div class="span3">Miejsce wykonywania pracy</div>
                    <div class="span7">
                            <div class="select-input">
                                    <div class="select-label">
                                            <p class="select-value">{if isset($smarty.post.place) && $smarty.post.place}{$smarty.post.place}{elseif isset($smarty.session.basket.place) && $smarty.session.basket.place}{$smarty.session.basket.place}{else}Wybierz miejsce wykonywania pracy{/if}</p>
                                            <input type="hidden" name="place" value="{if isset($smarty.post.place) && $smarty.post.place}{$smarty.post.place}{elseif isset($smarty.session.basket.place)}{$smarty.session.basket.place}{/if}">
                                            <i class="icon-select-list"></i>
                                    </div>
                                    <ul>
                                        <li>praca w terenie</li>
                                        <li>praca zdalna</li>
                                        <li>bez podawania adresu</li>
                                    </ul>
                            </div>
                            <div class="hand-tip tip-4"></div>
                    </div>
            </div>
            <hr>
            <div class="row-fluid">
                    <div class="span3"><label for="8">Strona www</label></div>
                    <div class="span7">
                            <div class="text-input">
                                    <input type="text" {if isset($errorArray.www) && $errorArray.www} style="background:#EED3D7"{/if} name="www" id="8" value="{if isset($smarty.post.www) && $smarty.post.www}{$smarty.post.www}{elseif isset($smarty.session.basket.www)}{$smarty.session.basket.www}{elseif $_user}{$_user->getUserWWW()}{/if}">
                            </div>
                    </div>
            </div>
            <div class="row-fluid">
                    <div class="span3"><label for="9">Fanpage</label></div>
                    <div class="span7">
                            <div class="text-input">
                                    <input type="text" {if isset($errorArray.fanpage) && $errorArray.fanpage} style="background:#EED3D7"{/if} name="fanpage" id="9" value="{if isset($smarty.post.fanpage) && $smarty.post.fanpage}{$smarty.post.fanpage}{elseif isset($smarty.session.basket.fanpage)}{$smarty.session.basket.fanpage}{elseif $_user}{$_user->getUserFanpage()}{/if}">
                            </div>
                    </div>
            </div>
            <hr>
            <div class="row-fluid">
                    <div class="span3">Logo</div>
                    <div class="span7">
                        <div class="file-input-box">
                            <div class="file-box">
                                {if isset($smarty.session.basket.logo) && $smarty.session.basket.logo}
                                    {if $smarty.session.basket.logo=='user'}
                                    <div class="img-box"  id="normalPreview">
                                        <img width="250px" src="{$smarty.const.HOST}upload/userLogo/{$_user->getUserLogo()}"/>
                                        <input type="hidden" name="logo" value="user" />
                                    </div>
                                    {elseif $smarty.session.basket.logo}
                                    <div class="img-box"  id="normalPreview">
                                        <img width="250px" src="{$smarty.const.HOST}upload/tmp/{$smarty.session.basket.logo}"/>
                                        <input type="hidden" name="logo" value="{$smarty.session.basket.logo}" />
                                    </div>          
                                    {/if}
                                {elseif (!isset($smarty.session.basket.trashFile) || !$smarty.session.basket.trashFile) && $_user && $_user->getUserLogo()}
                                <div class="img-box"  id="normalPreview">
                                    <img width="250px" src="{$smarty.const.HOST}upload/userLogo/{$_user->getUserLogo()}"/>
                                    <input type="hidden" name="logo" value="user" />
                                </div>
                                {/if}    
                                <div class="img-box-2">
                                    <img id="imagePreview" />
                                </div>
                                <div class="action-file-box" style="margin-top: 0px;">
                                    <div class="btn-browser">
                                        WYBIERZ LOGO <i class="icon-white icon-upload"></i>
                                        <input type="file" name="logo" accept="image/*"  onchange="readURL(this);">
                                    </div>
                                    <span onClick="$('#trashFile').val(1);$('#normalPreview').hide();$('#imagePreview').attr('src','');" class="btn-erase tooltip-item" title="Usuń"><i class="icon-trash icon-white"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <hr>

            {if $smarty.const.MOD_CALENDAR}
            <div class="row-fluid">
                <div class="span3"><label>Kalendarz rekrutacji kandydatów</label></div>
                <div class="span7">
                    <div class="input-box" style="margin:0px auto;padding:0px;">
                        <label class="calendar" style="width:200px;">
                            <span class="input-box-label">Określ termin rekrutacji</span>
                            <input type="text" name="calendar[start]" class="experience-date" style="width:180px;{if isset($errorArray.calendar.start) && $errorArray.calendar.start}background:#EED3D7{/if}" placeholder="od" value="{if isset($smarty.post.calendar.start) && $smarty.post.calendar.start}{$smarty.post.calendar.start}{elseif isset($smarty.session.basket.calendar.start)}{$smarty.session.basket.calendar.start}{/if}">
                            <i class="icon-calendar-input"></i>
                        </label>
                        <label class="calendar" style="width:200px;">
                            <span class="input-box-label"></span>
                            <input type="text" name="calendar[stop]" class="experience-date input-medium" style="width:180px;{if isset($errorArray.calendar.stop) && $errorArray.calendar.stop}background:#EED3D7{/if}" placeholder="do" value="{if isset($smarty.post.calendar.stop) && $smarty.post.calendar.stop}{$smarty.post.calendar.stop}{elseif isset($smarty.session.basket.calendar.stop)}{$smarty.session.basket.calendar.stop}{/if}">
                            <i class="icon-calendar-input"></i>
                        </label>                
                    </div>
                </div> 
            </div> 
            <div class="row-fluid">
                <div class="span3"></div>
                <div class="span7">
                    <label class="input-checkbox">
                        <input type="checkbox" name="calendar[exludeSunSut]" value="1" {if isset($smarty.post.calendar.exludeSunSut) && $smarty.post.calendar.exludeSunSut} checked="checked"{/if}>
                        <div class="checkbox-bg">
                            <div class="check-bg"></div>
                        </div>
                        <div class="label-checkbox">Wyklucz soboty i niedziele</div>
                    </label>        

                </div>
            </div>
            <div class="row-fluid">            
                <div class="span3"></div>
                <div class="span7">
                    <br>
                    <span class="input-box-label">Określ godziny przyjęć kandydatów</span>
                    <label class="input-box" style="margin:0; padding:0;">
                        <input type="text" name="calendar[hoursFrom]" class="" style="width:100px; {if isset($errorArray.calendar.hoursFrom) && $errorArray.calendar.hoursFrom}background:#EED3D7{/if}" placeholder="od godziny" value="{if isset($smarty.post.calendar.hoursFrom) && $smarty.post.calendar.hoursFrom}{$smarty.post.calendar.hoursFrom}{elseif isset($smarty.session.basket.calendar.hoursFrom)}{$smarty.session.basket.calendar.hoursFrom}{/if}">
                    </label> 
                    <label class="input-box" style="margin:0">
                        <input type="text" name="calendar[hoursTo]" class="" style="width:100px; {if isset($errorArray.calendar.hoursTo) && $errorArray.calendar.hoursTo}background:#EED3D7{/if}" placeholder="do godziny" value="{if isset($smarty.post.calendar.hoursTo) && $smarty.post.calendar.hoursTo}{$smarty.post.calendar.hoursTo}{elseif isset($smarty.session.basket.calendar.hoursTo)}{$smarty.session.basket.calendar.hoursTo}{/if}">
                    </label>     
                </div>
            </div> 
            <div class="row-fluid">
                <div class="span3"></div>
                <div class="span7">
                    <br>
                    <span class ="input-box-label">Określ liczbę kandydatów, którą możesz przyjąć w ciągu jednej godziny</span>
                    <label class="input-box" style="margin:0; padding:0;">
                        <input type="text" name="calendar[candidatePerHour]" class="" style="width:100px; {if isset($errorArray.calendar.candidatePerHour) && $errorArray.calendar.candidatePerHour}background:#EED3D7{/if}" placeholder="np.2" value="{if isset($smarty.post.calendar.candidatePerHour) && $smarty.post.calendar.hoursFrom}{$smarty.post.calendar.candidatePerHour}{elseif isset($smarty.session.basket.calendar.candidatePerHour)}{$smarty.session.basket.calendar.candidatePerHour}{/if}">
                    </label>                     
                </div>
            </div>               
                    
            <hr> 
            {/if}
            
            {if $_user && $_user->getUserType()==2}
            <div class="row-fluid">
                <div class="span10" style="text-align: center;"><h3>Twoi ulubieni pracownicy</h3></div>
            </div>
            {if $oneclick}
            <div class="row-fluid">
                <div class="span3">Zaznacz który z Twoich ulubionych ma dostać tę ofertę 1-click</div>
                <div class="span7">
                    <label class="input-checkbox">
                        <input type="checkbox" name="oneclickAll" value="1" {if isset($smarty.post.oneclickAll) && $smarty.post.oneclickAll} checked="checked"{elseif isset($smarty.session.basket.oneclickAll) && $smarty.session.basket.oneclickAll} checked="checked"{/if} onchange="if($(this).is(':checked')) { $('#oneclickList').hide('slow'); } else { $('#oneclickList').show('slow'); }">
                        <div class="checkbox-bg">
                            <div class="check-bg"></div>
                        </div>
                        <div class="label-checkbox">Wyślij do wszystkich ulubionych z pasującymi rodzajami prac</div>
                    </label>
                </div>
            </div>
            <div class="row-fluid" id="oneclickList">
                <div class="span3"></div>
                <div class="span7">
                    <div class="multiple-select-input">
                        <select name="oneclick[]" data-placeholder="Wybierz ulubionych użytkowników" multiple="" class="chosen-select" style="width:410px;">
                            <option></option>
                            {if $oneclick}
                                {foreach from=$oneclick item=user}
                                    <option {if isset($smarty.post.oneclick) && in_array($user.user_id, $smarty.post.oneclick)} selected="selected"{elseif isset($smarty.session.basket.oneclick) && in_array($user.user_id, $smarty.session.basket.oneclick)} selected="selected"{/if} value="{$user.user_id}">{if $user.user_name}{$user.user_name} {/if}{if $user.user_surname}{$user.user_surname} {/if}{$user.user_email}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                    <div class="hand-tip tip-1"></div>
                </div>
            </div>
            <div class="row-fluid">
                    <div class="span3"><label for="3">Dodatkowe informacje dla ulubionych</label></div>
                    <div class="span7">
                        <div class="textarea-input" style="width: 420px;">
                            <textarea id="3" name="oneclick_info"  rows="3">{if isset($smarty.post.oneclick_info)}{$smarty.post.oneclick_info}{elseif isset($smarty.session.basket.oneclick_info)}{$smarty.session.basket.oneclick_info}{/if}</textarea>
                        </div>
                    </div>
            </div>
            {else}
            <div class="row-fluid">
                <div class="span10" style="text-align: center;">nie masz dodanych ulubionych, aby to zrobić wejdź w "Mój profil" -> "Moi kandydaci" i dodaj ulubionych którzy już u Ciebie pracowali lub nowych klikając w zielony przycisk.</div>
            </div>
            {/if}
            <hr>
            {/if}
            <a href="#" onclick="$('#step').val(1);$('.main-form').submit();" class="preview-back">WSTECZ</a>
            <a href="#" onclick="$('.main-form').submit();" class="preview-next">DALEJ <i class="icon-chevron-right icon-white"></i></a>
            <div class="clear"></div>
    </form>
</section>
{/block}