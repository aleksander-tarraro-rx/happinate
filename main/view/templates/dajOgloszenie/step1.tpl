{extends file="index.tpl"}


{block name=prebody}
    {if $_user && $_user->getUserType()==2}
        <div class="center">
            <nav class="login-nav">
                <ul>
                    <li class="active"><a href="{$smarty.const.HOST}daj-ogloszenie"><strong>+</strong> Dodaj ogłoszenie</a></li>
                    <li><a href="{$smarty.const.HOST}moje-konto/moje-ogloszenia">Moje ogłoszenia</a></li>
                    <li><a href="{$smarty.const.HOST}moje-konto/moi-kandydaci">Ulubieni kandydaci</a></li>
                    <li><a href="{$smarty.const.HOST}moje-konto">Mój profil</a></li>
                </ul>            
            </nav>
        </div>
    {/if}

    <div id="WorkNow" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" style="width: 900px; margin-left: -480px; overflow: hidden;" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" style="margin: 10px; overflow: hidden;" aria-hidden="true">×</button>
        <div class="modal-body" style=" overflow: hidden;">
            <img src="{$smarty.const.HOST}view/images/worknow-big-pracodawca.png" />
        </div>
        <div class="modal-footer">
            Ogłoszenie jest płatne 29,00 zł netto.
            <button class="btn" data-dismiss="modal" aria-hidden="true">Zamknij</button>
        </div>
    </div>
{/block}


{block name=body}
<section class="main-content">
    {if !$_user || ($_user && ($_user->getUserType()==2 || $_user->getUserType()==6 || $_user->getUserType()==5))}
    <form class="main-form" action="" method="post">
        
        <input type="hidden" name="step" value="1">
        <input type="hidden" name="worknow" value="0">
        <h2>DODAJ OGŁOSZENIE<span class="underline-header-content" style="width:174px;"></span></h2>
        <div class="clear" style="height:25px;"></div>
        {if $_user && ($_user->getUserType()==6 || $_user->getUserType()==5)}
        <div class="row-fluid">
            <div class="span3"><label for="1">Użytkownik (adres email) <font style="color: #cd0a0a;">*</font></label></div>
            <div class="span7">
                <div class="text-input input-char-num">
                    <input type="text" name="userEmail" value="{if isset($smarty.post.userEmail)}{$smarty.post.userEmail}{elseif isset($smarty.session.basket.userEmail)}{$smarty.session.basket.userEmail}{/if}" maxlength="120" id="1"{if isset($errorArray.userEmail) && $errorArray.userEmail} style="background:#EED3D7"{/if}>
                    <span class="char-num">Pozostało <span class="value">0</span> znaków</span>
                </div>
            </div>
        </div>
        {/if}
        <div class="row-fluid">
            <div class="span3"><label for="1">Nazwa stanowiska <font style="color: #cd0a0a; font-weight: bold;">*</font></label></div>
            <div class="span7">
                <div class="text-input input-char-num">
                    <input type="text" name="position" value="{if isset($smarty.post.position)}{$smarty.post.position}{elseif isset($smarty.session.basket.position)}{$smarty.session.basket.position}{/if}" maxlength="120" id="1"{if isset($errorArray.position) && $errorArray.position} style="background:#EED3D7"{/if}>
                    <span class="char-num">Pozostało <span class="value">0</span> znaków</span>
                </div>
            </div>
        </div>
        <div class="row-fluid">
                <div class="span3">Rodzaj pracy <font style="color: #cd0a0a; font-weight: bold;">*</font><br/><small style="font-size: 12px;">Wybierz kategorię główną</small></div>
                <div class="span7">
                    <div class="select-input"{if isset($errorArray.trade) && $errorArray.trade} style="background:#EED3D7;  padding: 12px 9px;"{else} style=" padding: 12px 9px;"{/if}>
                        <div class="select-label">
                            <p class="select-value"  style="font-size: 13px;">{if isset($smarty.post.trade) && $smarty.post.trade}{$smarty.post.trade}{elseif isset($smarty.session.basket.trade)}{$smarty.session.basket.trade}{else}Kategoria główna{/if}</p>
                            <input type="hidden" name="trade" value="{if isset($smarty.post.trade)}{$smarty.post.trade}{elseif isset($smarty.session.basket.trade)}{$smarty.session.basket.trade}{/if}">
                            <i class="icon-select-list"></i>
                        </div>
                        <ul>
                            {if $tagList}
                                {foreach from=$tagList item=tag}
                                    <li>{$tag.tag_name}</li>
                                {/foreach}
                            {/if}
                        </ul>
                    </div>
                </div>
        </div>       
        <div class="row-fluid">
            <div class="span3">Rodzaj pracy <font style="color: #cd0a0a; font-weight: bold;">*</font><br/><small style="font-size: 12px;">Wybierz kategorie dodatkowe</small></label></div>
            <div class="span7">
                <div class="multiple-select-input"{if isset($errorArray.tags) && $errorArray.tags} style="background:#EED3D7"{/if}>
                    <select name="tags[]" data-placeholder="Kategorie dodatkowe" multiple="" class="chosen-select dodaj-ogloszenie-select" id="2">
                        <option></option>
                        {if $tagList}
                            {foreach from=$tagList item=tag}
                                <option {if isset($smarty.post.tags) && in_array($tag.tag_id, $smarty.post.tags)} selected="selected"{elseif isset($smarty.session.basket.tags) && in_array($tag.tag_id, $smarty.session.basket.tags)} selected="selected"{/if} value="{$tag.tag_id}">{$tag.tag_name}</option>
                            {/foreach}
                        {/if}
                    </select>
                    <i class="icon-select-list"></i>
                </div>
                <div class="hand-tip tip-1" style="margin-top: -35px;"></div>
            </div>
        </div>
                            
        <div class="row-fluid">
            <div class="span3" style="padding-top: 50px;">Wybierz ofertę PRACUJ TERAZ!<br><small href="#WorkNow" role="button" data-toggle="modal" class="btn btn-info" style="margin-top: 5px;">dowiedz się więcej <i class="icon-white icon-question-sign"></i></small></div>
            <div class="span7">
                <label class="input-checkbox">
                    <div class="label-checkbox"><img width="100px" height="100px" src="{$smarty.const.HOST}view/images/work-now-logo.png" alt="Work now!"><br/><br/></div>
                    <input type="checkbox" name="worknow" id="worknowCheckbox" value="1">
                    <div  style="margin-top: 50px;" class="checkbox-bg">
                        <div class="check-bg"></div>
                    </div>
                </label>        
                <div class="hand-tip tip-5"></div>
            </div>
        </div>
                        
        <div class="row-fluid">
            <div class="span3">Ilość wakatów <font style="color: #cd0a0a; font-weight: bold;">*</font></div>
            <div class="span7">
                <div class="text-input">
                    <input type="text" name="vacancies" value="{if isset($smarty.post.vacancies)}{$smarty.post.vacancies}{elseif isset($smarty.session.basket.vacancies)}{$smarty.session.basket.vacancies}{/if}" maxlength="120" id="1"{if isset($errorArray.vacancies) && $errorArray.vacancies} style="background:#EED3D7"{/if}>
                </div>
            </div>
        </div>
                    
        <hr>
        <div class="row-fluid">
            <div class="span3">Stawka <font style="color: #cd0a0a; font-weight: bold;">*</font></div>
            <div class="span7">
                    <div class="select-input stawka"{if isset($errorArray.paymant) && $errorArray.paymant} style="background:#EED3D7"{/if}>
                        <div class="select-label">
                            <p class="select-value">{if isset($smarty.post.paymant) && $smarty.post.paymant}{$smarty.post.paymant}{elseif isset($smarty.session.basket.paymant)}{$smarty.session.basket.paymant}{else}Wybierz stawkę{/if}</p>
                            <input type="hidden" name="paymant" value="{if isset($smarty.post.paymant)}{$smarty.post.paymant}{elseif isset($smarty.session.basket.paymant)}{$smarty.session.basket.paymant}{/if}">
                            <i class="icon-select-list"></i>
                        </div>
                        <ul>
                            <li>wolontariat, staż / praktyki bezpłatne</li>
                            <li>publikuj bez podawania stawki</li>
                            <li>stawka godzinowa</li>
                            <li>stawka miesięczna</li>
                        </ul>
                    </div>

                    <div class="feature-row stawka"{if isset($smarty.post.paymant) && $smarty.post.paymant=='stawka godzinowa'} style="display: block;"{elseif isset($smarty.session.basket.paymant)}{$smarty.session.basket.paymant=='stawka godzinowa'} style="display: block;"{/if}>
                        <div class="span12">
                            <div class="text-input small">
                                <input type="text" name="rate" maxlength="120" value="{if isset($smarty.post.rate)}{$smarty.post.rate}{elseif isset($smarty.session.basket.rate)}{$smarty.session.basket.rate}{/if}" id="1"{if isset($errorArray.rate) && $errorArray.rate} style="background:#EED3D7"{/if}>
                            </div>
                        </div>
                            
                        <div class="span12 text-right" style="margin-left: 0px;">
                            PLN na godzinę brutto
                        </div>

                        <div class="span12 text-right" style="margin-left: 0px;">
                            <hr>
                        </div>
                            
                        <div class="span6 text-right" style="margin-left: 0px;">
                            <div class="text-input small">
                                lub od <input type="text" name="rate_min" maxlength="120" value="{if isset($smarty.post.rate_min)}{$smarty.post.rate_min}{elseif isset($smarty.session.basket.rate_min)}{$smarty.session.basket.rate_min}{/if}" id="1"{if isset($errorArray.rate_min) && $errorArray.rate_min} style="background:#EED3D7"{/if}>
                            </div>
                        </div>
                        <div class="span6 text-right" style="margin-left: 0px;">
                            <div class="text-input small">
                                do <input type="text" name="rate_max" maxlength="120" value="{if isset($smarty.post.rate_max)}{$smarty.post.rate_max}{elseif isset($smarty.session.basket.rate_max)}{$smarty.session.basket.rate_max}{/if}" id="1"{if isset($errorArray.rate_max) && $errorArray.rate_max} style="background:#EED3D7"{/if}>
                            </div>
                        </div>
                            
                        <div class="span12 text-right" style="margin-left: 0px;">
                            PLN na godzinę brutto
                        </div>
                    </div>

                    <div class="feature-row miesieczna"{if isset($smarty.post.paymant) && $smarty.post.paymant=='stawka miesięczna'} style="display: block;"{elseif isset($smarty.session.basket.paymant) && $smarty.session.basket.paymant=='stawka miesięczna'} style="display: block;"{/if}>
                        <div class="span6">
                            <div class="text-input small">
                                <input onkeyup="calculateMonthRate(this)" type="text" name="rateMonth" maxlength="120" value="{if isset($smarty.post.rateMonth)}{$smarty.post.rateMonth}{elseif isset($smarty.session.basket.rateMonth)}{$smarty.session.basket.rateMonth}{/if}" id="1"{if isset($errorArray.rateMonth) && $errorArray.rateMonth} style="background:#EED3D7"{/if}>
                            </div>
                        </div>
                        <div class="span6 text-right">
                            PLN brutto <br>(168 godzin miesięcznie)
                        </div>
                        <div class="span12 text-right" style="margin-left: 0;">
                            Jest to <span id="monthRate">0.00</span>zł na godzinę.
                        </div>
                    </div>
                    <div class="hand-tip tip-2"></div>
            </div>
        </div>
        <hr>
        <div class="row-fluid">
                <div class="span3"><label for="4">Wymagania</label></div>
                <div class="span7">
                    {*<span class="btn editorBtn" style="margin-top: 5px; margin-bottom: 5px;">Pokaż edytor &darr;</span>*}
                    <div class="textarea-input" style="margin-bottom: 10px;width: 420px;">
                        <textarea class="ckeditor2" id="4" name="need" name="need" rows="3"{if isset($errorArray.need) && $errorArray.need} style="background:#EED3D7"{/if}>{if isset($smarty.post.need)}{$smarty.post.need}{elseif isset($smarty.session.basket.need)}{$smarty.session.basket.need}{/if}</textarea>
                    </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="5">Obowiązki <font style="color: #cd0a0a; font-weight: bold;">*</font></label></div>
                <div class="span7">
                    {*<span class="btn editorBtn{if isset($errorArray.duty) && $errorArray.duty} btn-danger{/if}" style="margin-top: 5px; margin-bottom: 5px;">Pokaż edytor &darr;</span>*}
                    <div class="textarea-input" style="margin-bottom: 10px;width: 420px;">
                        <textarea class="ckeditor3" id="5" name="duty" rows="3">{if isset($smarty.post.duty)}{$smarty.post.duty}{elseif isset($smarty.session.basket.duty)}{$smarty.session.basket.duty}{/if}</textarea>
                    </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="6">Oferujemy</label></div>
                <div class="span7">
                    {*<span class="btn editorBtn" style="margin-top: 5px; margin-bottom: 5px;">Pokaż edytor &darr;</span>*}
                    <div class="textarea-input" style="margin-bottom: 10px;width: 420px;">
                        <textarea class="ckeditor4" id="6" name="offer" rows="3"{if isset($errorArray.offer) && $errorArray.offer} style="background:#EED3D7"{/if}>{if isset($smarty.post.offer)}{$smarty.post.offer}{elseif isset($smarty.session.basket.offer)}{$smarty.session.basket.offer}{/if}</textarea>
                    </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="3">Dodatkowe informacje</label></div>
                <div class="span7">
                        <div class="textarea-input" style="width: 420px;">
                            <textarea class="ckeditor5" id="3" name="additional_salary_info"  rows="3"{if isset($errorArray.additional_salary_info) && $errorArray.additional_salary_info} style="background:#EED3D7"{/if}>{if isset($smarty.post.additional_salary_info)}{$smarty.post.additional_salary_info}{elseif isset($smarty.session.basket.additional_salary_info)}{$smarty.session.basket.additional_salary_info}{/if}</textarea>
                        </div>
                </div>
        </div>      
        <hr>
        <div class="row-fluid">
                <div class="span3">Województwo <font style="color: #cd0a0a; font-weight: bold;">*</font></div>
                <div class="span7">
                        <div class="select-input"{if isset($errorArray.province) && $errorArray.province} style="background:#EED3D7"{/if}>
                            <div class="select-label">
                                <p class="select-value">{if isset($smarty.post.province) && $smarty.post.province}{$smarty.post.province}{elseif isset($smarty.session.basket.province)}{$smarty.session.basket.province}{else}Wybierz województwo{/if}</p>
                                <input type="hidden" name="province" value="{if isset($smarty.post.province)}{$smarty.post.province}{elseif isset($smarty.session.basket.province)}{$smarty.session.basket.province}{/if}" id="7">
                                <i class="icon-select-list"></i>
                            </div>
                            <ul>
                               {if $provinces}
                                    {foreach from=$provinces item=province}
                                        <li>{$province.province_text}</li>
                                    {/foreach}
                                {/if}
                            </ul>
                        </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="8">Miasto <font style="color: #cd0a0a; font-weight: bold;">*</font></label></div>
                <div class="span7">
                        <div class="text-input">
                                <input type="text" value="{if isset($smarty.post.city)}{$smarty.post.city}{elseif isset($smarty.session.basket.city)}{$smarty.session.basket.city}{elseif $_user}{$_user->getUserAdressCity()}{/if}" name="city" id="8" onBlur="codeAddress();"{if isset($errorArray.city) && $errorArray.city} style="background:#EED3D7"{/if}>
                        </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="9">Ulica, numer lokalu <font style="color: #cd0a0a; font-weight: bold;">*</font></label></div>
                <div class="span7">
                        <div class="text-input">
                                <input type="text" value="{if isset($smarty.post.street)}{$smarty.post.street}{elseif isset($smarty.session.basket.street)}{$smarty.session.basket.street}{elseif $_user}{$_user->getUserAdressStreet()}{/if}" name="street" id="9" onBlur="codeAddress();"{if isset($errorArray.street) && $errorArray.street} style="background:#EED3D7"{/if}>
                        </div>
                        <div class="hand-tip tip-6"></div>
                </div>
                
        </div>
        <div class="row-fluid">
                <div class="span3"></div>
                <div class="span8">
                        <input type="hidden" name="coords" value="{if isset($smarty.post.coords)}{$smarty.post.coords}{elseif isset($smarty.session.basket.coords)}{$smarty.session.basket.coords}{/if}">
                        <div class="position-map" id="map_canvas"></div>
                </div>
        </div>
        {if !$_user}
        <hr>
        <div class="row-fluid">
                <div class="span3"><label for="11">Nazwa pracodawcy <font style="color: #cd0a0a; font-weight: bold;">*</font></label></div>
                <div class="span7">
                        <div class="text-input">
                                <input type="text" id="11"  value="{if isset($smarty.post.contact_name)}{$smarty.post.contact_name}{elseif isset($smarty.session.basket.contact_name)}{$smarty.session.basket.contact_name}{/if}" name="contact_name"{if isset($errorArray.contact_name) && $errorArray.contact_name} style="background:#EED3D7"{/if}>
                        </div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="12">E-mail <font style="color: #cd0a0a; font-weight: bold;">*</font></label></div>
                <div class="span7">
                        <div class="text-input">
                                <input type="text" id="12" value="{if isset($smarty.post.contact_email)}{$smarty.post.contact_email}{elseif isset($smarty.session.basket.contact_email)}{$smarty.session.basket.contact_email}{/if}" name="contact_email"{if isset($errorArray.contact_email) && $errorArray.contact_email} style="background:#EED3D7"{/if}>
                        </div>
                        <div class="hand-tip tip-3"></div>
                </div>
        </div>
        <div class="row-fluid">
                <div class="span3"><label for="13">Telefon</label></div>
                <div class="span7">
                        <div class="text-input">
                                <input type="text" id="13" value="{if isset($smarty.post.contact_phone)}{$smarty.post.contact_phone}{elseif isset($smarty.session.basket.contact_phone)}{$smarty.session.basket.contact_phone}{/if}" name="contact_phone"{if isset($errorArray.contact_phone) && $errorArray.contact_phone} style="background:#EED3D7"{/if}>
                        </div>
                </div>
        </div>
        {/if}
        <hr><small>* pola wymagane</small>
        <a href="#" onclick="$('.main-form').submit();" class="preview-next">DALEJ <i class="icon-chevron-right icon-white"></i></a>
        <div class="clear"></div>
	</form>
        {else}
            <div style="text-align: center;">
                <img src="{$smarty.const.HOST}view/images/strefadlapracodawcow.jpg" alt="">
            </div>
            <div class="error-text">
                <p><em>Nic się nie stało...</em><br>wyloguj się i załóż sobie nowe konto jako pracodawca!</p>
            </div>
            <a class="error-btn-home" href="{$smarty.const.HOST}">Powrót</a>
        {/if}
</section>
{/block}
{block name=additionalJavaScript}
<script type="text/javascript">

    $('.ckeditor2').ckeditor();
    $('.ckeditor3').ckeditor();
    $('.ckeditor4').ckeditor();
    $('.ckeditor5').ckeditor();
    
    $(document).ready(function() {
        var hash = location.hash;
        
        {if $_user} codeAddress(); {/if}
            
        if(hash=='#pracujteraz') {
            $('#worknowCheckbox').attr('checked', 'checked');
            $('#worknowCheckbox').parent().find('.check-bg').addClass('active');
           
        }
    });
</script>
{/block}