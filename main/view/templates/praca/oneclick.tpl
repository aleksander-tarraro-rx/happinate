{extends file="index.tpl"}

{block name=prebody}
    {if $_user && !empty($_user) && $_user->getUserType()==1}
    <div id="inviteFriend" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel"><img  src="{$smarty.const.HOST}view/images/letsworktogether.png" width="200px" /></h3>
      </div>
     <div class="modal-body">
        <form action="" class="main-form"  method="post" id="inviteFriendForm">
            <input type="hidden" name="inviteFriend" value="1" />
            <input type="hidden" name="url" value="{$job.job_id}/{$job.job_url_text}" />
            <p>Zaproś znajomych i <strong>pracujcie razem!</strong></p>
            <hr>
            {assign var=friendList value=$_user->getFriendsList()}
            {if $friendList}    
                <div class="row">
                {foreach from=$friendList item=user}
                    <div class="span4"><label><input type="checkbox" name="friends[]" value="{$user.user_email}" /> {$user.user_email}</label></div>
                {/foreach}
                </div>
            {else}
                Niestety nie masz żadnych znajomych.
            {/if}
        </form>
     </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        {if $friendList}
            <a href="javascript:;" onclick="$('#inviteFriendForm').submit();" class="btn btn-success">Prześlij znajomym to ogłoszenie</a>
        {else}
            <a href="{$smarty.const.HOST}moje-konto/moi-znajomi " class="btn btn-success">Zaproś znajomych!</a>
        {/if}
      </div>
    </div>
    {/if}
{/block}

{block name=body}

    <div class="left-column">
        <div class="job-title">
            
            <h2 class="jobTitleH2{if $job.job_main_page==2} pracujTerazH2{elseif $job.job_main_page==1} wyroznienieH2{/if}">{$job.job_position} </h2>
            
            {if $_user && !empty($_user) && $_user->getUserType()==1}<a href="#" onclick="return(false);"><span data-original-title="{if !$_model->checkIfAddedToFav({$job.job_id})}Usuń z ulubionych{else}Dodaj do ulubionych{/if}"  id="{$job.job_id}" class="fav tooltip-item{if $job.job_main_page} main{/if}{if !$_model->checkIfAddedToFav({$job.job_id})} active{/if}"></span></a>{/if}
            
            {assign var="start_date" value=" "|explode:$job.job_start_date}
            
            {if isset($job.job_start_date) && !empty($job.job_start_date) && strtotime($job.job_start_date)>0}<p>praca od: <strong>{$start_date.0}</strong></p>{/if}
            
            {assign var="job_url" value="http://{$smarty.server.HTTP_HOST}/praca/{$job.job_id}/{$job.job_url_text}"}
            
            {if $_user && !empty($_user) && $_user->getUserType()==1}
            <div class="notice-share notice-watch" style="margin-right: 50px;">
                {if !$_model->checkWatch($job.job_added_by, $_user->getUserId())}
                    <a href="javascript:;" class="happi-big-btn" id="watch-{$job.job_added_by}" style="padding:4px 2px;font-size:12px;width:150px;"><i class="icon-ok-sign icon-white"></i> &nbsp; Obserwuj pracodawcę</a>
                {else}
                    <a href="javascript:;" class="happi-big-btn" id="unwatch-{$job.job_added_by}" style="padding:4px 2px;font-size:12px;width:150px;"><i class="icon-minus icon-white"></i> &nbsp; Usuń z obserwowanych</a>
                {/if}
            </div>
            {/if}
            
            {if $_user && !empty($_user) && $_user->getUserType()==1}
            <div class="notice-share" style="margin-right: 130px; top: 57px;;">
                <a href="#" data-toggle="modal" data-target="#inviteFriend" class="happi-big-btn" style="padding:4px 2px;font-size:12px;width:230px;"><i class="icon-user icon-white"></i> Udostępnij znajomemu i pracujcie razem!</a>
            </div>
            {/if}

        </div>
        <hr>
        <div class="employer-address">
            <h2><a href="#">{$job.job_employeer_name}</a></h2>
            {if isset($job.job_logo) && !empty($job.job_logo)}
                <div class="logo">
                    {if $job.job_logo == 'user'}
                        <img src="{$smarty.const.HOST}upload/userLogo/{$_model->getUserLogo($job.job_added_by)}" />
                    {else}
                        <img src="{$smarty.const.HOST}upload/jobLogo/{$job.job_logo}" />
                    {/if}
                </div>
            {/if}
            {if isset($job.job_employeer_place_direct) && $job.job_employeer_place_direct}
                {if $job.job_employeer_place_direct_address && $job.job_employeer_place_direct_zip_code && $job.job_employeer_place_direct_city}
                    <p><i class="icon-map-marker"></i> <a href="https://maps.google.pl/maps?q=do+{$job.job_employeer_place_direct_address},+{$job.job_employeer_place_direct_zip_code},+{$job.job_employeer_place_direct_city}&hl=pl&t=m&z=15" target="_blank">{$job.job_employeer_place_direct_address}, {$job.job_employeer_place_direct_zip_code}, {$job.job_employeer_place_direct_city}</a></p>
                {/if}
                {if isset($job.job_employeer_place_direct_place_tips) && !empty($job.job_employeer_place_direct_place_tips)}<p>{$job.job_employeer_place_direct_place_tips}</p>{/if}
            {/if}
            
            {if isset($job.job_employeer_place_area) && $job.job_employeer_place_area}<p><i class="icon-map-marker"></i> praca w terenie</p>{/if}
            {if isset($job.job_employeer_place_telework) && $job.job_employeer_place_telework}<p><i class="icon-map-marker"></i> praca zdalna</p>{/if}
            
            {if isset($job.job_www) && !empty($job.job_www)}<p><i class="icon-globe"></i> <a href="http://{$job.job_www}" target="_blank">{$job.job_www}</a></p>{/if}
            {if isset($job.job_fanpage) && !empty($job.job_fanpage)}<p><i class="icon-star"></i> <a href="http://{$job.job_fanpage}" target="_blank">{$job.job_fanpage}</a></p>{/if}
        </div>
        <hr>
        <div class="notice-description">
            {if $job.duties}
            <h2>Obowiązki</h2>
            <ul>
                {foreach from=$job.duties item=duty}
                <li>{$duty.duty_text}</li>
                {/foreach}
            </ul>
            {elseif $job.job_duty}
            <h2>Obowiązki</h2>
            <p>{htmlspecialchars_decode($job.job_duty)}</p>
            {/if}
            
            {if $job.needs}
            <h2>Wymagania</h2>
            <ul>
                {foreach from=$job.needs item=needs}
                <li>{$needs.need_text}</li>
                {/foreach}
            </ul>
            {elseif $job.job_need}
            <h2>Wymagania</h2>
            <p>{htmlspecialchars_decode($job.job_need)}</p>
            {/if}
            
            {if $job.job_offer}
            <h2>Oferujemy</h2>
            <p>{htmlspecialchars_decode($job.job_offer)}</p>
            {/if}
                
            {if isset($job.job_contract_type) && !empty($job.job_contract_type)}
                <h2>Rodzaj umowy</h2>
                <p>{$job.job_contract_type}</p>
            {/if}
            
            {if isset($job.job_trade_type) && !empty($job.job_trade_type)}
                <h2>Branża</h2>
                <p>{$job.job_trade_type}</p>
            {/if}
            
            {if isset($job.job_time) && !empty($job.job_time)}
                <h2>Czas pracy</h2>
                <p>{$job.job_time}</p>
            {/if}
            
            {if isset($job.job_work_hours_per_week) && !empty($job.job_work_hours_per_week)}
                <h2>Ilość godzin pracy tygodniowo</h2>
                <p>{$job.job_work_hours_per_week}</p>
            {/if}
            
            {if isset($job.job_work_shifts_per_day) && !empty($job.job_work_shifts_per_day)}
                <h2>Ilość zmian w ciągu dnia</h2>
                <p>{$job.job_work_shifts_per_day}</p>
            {/if}
            
            {if isset($job.job_city_id) && !empty($job.job_city_id)}
                <h2>Miasto</h2>
                <p>{$job.job_employeer_place_direct_city}</p>
            {/if}  
            
            {if isset($job.job_vacancies) && !empty($job.job_vacancies)}
                <h2>Ilość wakatów</h2>
                <p>{$job.job_vacancies}</p>
            {/if}     
            
            {if isset($job.job_additional_oneclick_info) && !empty($job.job_additional_oneclick_info)}
                <h2>Dodatkowe informacje od pracodawcy</h2>
                <p>{$job.job_additional_oneclick_info}</p>
            {/if}            
        </div>
    </div>
    <div class="right-column notice-more-column">

        <div class="notice-rate">
            <h3>STAWKA BRUTTO</h3>
            {if isset($job.job_hide_salary) && $job.job_hide_salary}
                <p>oferta bez podania stawki godzinowej</p>
            {elseif isset($job.job_volunteering) && $job.job_volunteering}
                <p>wolontariat, staż / praktyki bezpłatne</p>
            {else}
            <p class="rate">zł/godzinę</p>
            
            {assign var=rate_min value="."|explode:$job.job_rate_min}
            {assign var=rate_max value="."|explode:$job.job_rate_max} 
            
            <p class="value"><small>OD</small> <strong>{$rate_min.0}<small>,{if isset($rate_min.1)}{$rate_min.1}{if strlen($rate_min.1)==1}0{elseif strlen($rate_min.1)==0}00{/if}{else}00{/if}</small></strong> &nbsp; <small>DO</small> <strong>{$rate_max.0}<small>,{if isset($rate_max.1)}{$rate_max.1}{if strlen($rate_max.1)==1}0{elseif strlen($rate_max.1)==0}00{/if}{else}00{/if}</small></strong></p>
            {/if}
        </div>
        {if isset($job.job_additional_salary_info) && !empty($job.job_additional_salary_info)}
        <div class="notice-more-info">
            <h3>DODATKOWE INFORMACJE</h3>
            <div class="show-hide-icon"></div>
            <div class="show-hide-info">
                <p>{$job.job_additional_salary_info}</p>
            </div>
        </div>
        {/if}
        {if $job.job_publish_date_end|strtotime > $smarty.now}
            {if isset($job.job_contact_name) || isset($job.job_contact_email) || isset($job.job_contact_phone)}
            <div class="notice-contact-info">
                <h3>DANE KONTAKTOWE</h3>         
                {if isset($job.job_contact_phone) && !empty($job.job_contact_phone)}
                <div class="show-phone-number">
                    <p class="phone-number" data-phone="{$job.job_contact_phone}"></p>
                    <div class="show-phone-number-mask">
                        <i class="icon-corner-phone"></i>
                        <p>POKAŻ NUMER TELEFONU</p>
                    </div>
                </div>
                {/if}
            </div>
            {/if}
        {/if}
        <div style="text-align: center;">
            {if isset($applied) && $applied}
                <h3 style="margin-top: 20px; font-size: 15px;">Już aplikowałeś na to stanowisko.</h3>
                <a style="margin-top: 20px; width: 80%; font-size: 16px;" class="btn btn-happinate btn-large" href="{$smarty.const.HOST}">POWRÓT NA STRONĘ GŁÓWNĄ >></a>
            {else}
                {if $vacancies}
                    <a style="margin-top: 10px; width: 80%; font-size: 16px;" href="{$smarty.const.HOST}praca/oneclick/{$job.job_id},akceptuj"><img src="{$smarty.const.HOST}view/images/jaktodziala-oneclick-2.jpg" border="0" /></a>
                    <a style="margin-top: 10px; width: 80%; font-size: 16px;" class="btn btn-happinate btn-large" href="{$smarty.const.HOST}praca/oneclick/{$job.job_id},akceptuj">POTWIERDŹ, ŻE STAWISZ SIĘ W PRACY</a>
                {else}
                    <h3 style="margin-top: 20px; font-size: 15px;">Wszystkie wakaty zostały już zapełnione ale możesz zapisać się na listę rezerwowych dzięki czemu pracodawca będzie wiedział, że jesteś chętny z nim współpracować.</h3>
                    <a style="margin-top: 20px; width: 80%; font-size: 16px;" class="btn btn-happinate btn-large" href="{$smarty.const.HOST}praca/oneclick/{$job.job_id},akceptuj">ZAPISZ SIĘ JAKO REZERWOWY</a>
                    <a style="margin-top: 10px; width: 80%; font-size: 16px;" class="btn btn-happinate btn-large" href="{$smarty.const.HOST}">POWRÓT NA STRONĘ GŁÓWNĄ >></a>
                {/if}
            {/if}
        </div>
    </div>

    <div class="clear"></div>
{/block}