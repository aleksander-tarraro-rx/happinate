{extends file="index.tpl"}

{block name=metatitle}{$job.job_position}{if !empty($job.job_employeer_place_direct_city)} - {$job.job_employeer_place_direct_city}{/if} - Czas pracy: {$job.job_time} - happinate{/block}
{block name=metadescription}{/block}

{block name=prebody}
    {if $_user && !empty($_user) && $_user->getUserType()==1}
    <div id="inviteFriend" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel"><img  src="{$smarty.const.HOST}view/images/letsworktogether.png" width="200px" /></h3>
      </div>
     <div class="modal-body">
        <form action="" class="main-form"  method="post" id="inviteFriendForm">
            <input type="hidden" name="inviteFriend" value="1" />
            <input type="hidden" name="url" value="{$job.job_id}/{$job.job_url_text}" />
            <p>Zaproś znajomych i <strong>pracujcie razem!</strong></p>
            <hr>
            {assign var=friendList value=$_user->getFriendsList()}
            {if $friendList}    
                <div class="row">
                {foreach from=$friendList item=user}
                    <div class="span4"><label><input type="checkbox" name="friends[]" value="{$user.user_email}" /> {$user.user_email}</label></div>
                {/foreach}
                </div>
            {else}
                Niestety nie masz żadnych znajomych.
            {/if}
        </form>
     </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        {if $friendList}
            <a href="javascript:;" onclick="$('#inviteFriendForm').submit();" class="btn btn-success">Prześlij znajomym to ogłoszenie</a>
        {else}
            <a href="{$smarty.const.HOST}moje-konto/moi-znajomi " class="btn btn-success">Zaproś znajomych!</a>
        {/if}
      </div>
    </div>

    <div id="cvList" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="addCandidateLabel">Wybierz swoje <b>CV</b></h3>
      </div>
     <div class="modal-body" id="radioButtons">
         {if isset($userCVList) && $userCVList}
         <table width="100%">
             {foreach from=$userCVList item=cv}
                 <tr><td width="30px"><input id="radio{$cv.cv_id}" type="radio" name="cv" value="{$cv.cv_id}" /></td><td><label for="radio{$cv.cv_id}">{$cv.cv_name}</label></td><td align="right"><a href="{$smarty.const.HOST}upload/userCV/{$cv.cv_src}" target="_blank">zobacz</a></td></tr>
             {/foreach}
         </table>
         {else}
             Niestety nie masz dodanych żadnych CV.
         {/if}
     </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Anuluj</button>
        {if isset($userCVList) && $userCVList}<button onclick="getCV($(this));" class="btn btn-success" data-dismiss="modal" aria-hidden="true">Wybierz</button>{/if}
      </div>
    </div>

    {/if}
{/block}
{block name=body}

    <div class="left-column">
        <div class="job-title" style="min-height: 110px;">
            
            <h2 class="jobTitleH2{if $job.job_main_page==2} pracujTerazH2{elseif $job.job_main_page==1} wyroznienieH2{/if}">{$job.job_position} </h2>
            
            {if $_user && !empty($_user) && $_user->getUserType()==1}<a href="#" onclick="return(false);"><span data-original-title="{if !$_model->checkIfAddedToFav({$job.job_id})}Usuń z ulubionych{else}Dodaj do ulubionych{/if}"  id="{$job.job_id}" class="fav tooltip-item{if $job.job_main_page} main{/if}{if !$_model->checkIfAddedToFav({$job.job_id})} active{/if}"></span></a>{/if}
            
            {assign var="start_date" value=" "|explode:$job.job_start_date}
            
            {if isset($job.job_start_date) && !empty($job.job_start_date) && strtotime($job.job_start_date)>0}<p>praca od: <strong>{$start_date.0}</strong></p>{/if}
            
            {assign var="job_url" value="http://{$smarty.server.HTTP_HOST}/praca/{$job.job_id}/{$job.job_url_text}"}
            
            {if $_user && !empty($_user) && $_user->getUserType()==1}
            <div class="notice-share notice-watch" style="margin-right: 160px;">
                {if !$_model->checkWatch($job.job_added_by, $_user->getUserId())}
                    <a href="javascript:;" class="happi-big-btn" id="watch-{$job.job_added_by}" style="padding:4px 2px;font-size:12px;width:150px;"><i class="icon-ok-sign icon-white"></i> &nbsp; Obserwuj pracodawcę</a>
                {else}
                    <a href="javascript:;" class="happi-big-btn" id="unwatch-{$job.job_added_by}" style="padding:4px 2px;font-size:12px;width:150px;"><i class="icon-minus icon-white"></i> &nbsp; Usuń z obserwowanych</a>
                {/if}
            </div>
            {/if}
            
            {if $_user && !empty($_user) && $_user->getUserType()==1}
            <div class="notice-share" style="margin-right: 130px; top: 57px;">
                <a href="#" data-toggle="modal" data-target="#inviteFriend" class="happi-big-btn" style="padding:4px 2px;font-size:12px;width:230px;"><i class="icon-user icon-white"></i> Udostępnij znajomemu i pracujcie razem!</a>
            </div>
            {/if}
            
            <div class="notice-share">
                <a href="#" class="happi-big-btn" style="padding:4px 2px;font-size:12px;width:100px;"><i class="icon-share icon-white"></i> &nbsp; Udostępnij</a>
                <ul>
                    <li><a href="https://www.facebook.com/sharer/sharer.php?s=100&p[images][0]={"http://{$smarty.server.HTTP_HOST}/view/images/happinate-face-img.png"|urlencode}&p[title]={$job.job_position|urlencode}&p[url]={$job_url|urlencode}&p[summary]={(($job.job_duty|html_entity_decode)|strip_tags)|urlencode}" target="_blank" class="facebook"></a></li>
                    <li><a href="https://twitter.com/intent/tweet?source=webclient&text={$job_url}" target="_blank" class="twitter"></a></li>
                    <li><a href="mailto:podaj@email.pl?subject=happinate%20praca&body=Sprawdź%20tę%20ofertę%0D%0A{$job_url}" target="_blank" class="mail"></a></li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="employer-address">
            
            <div style="padding: 10px; background: #F4F4F4">
                <h2><a href="{$smarty.const.HOST}profil/p/{$job.job_added_by|md5}">{$_model->getUserProfileName($job.job_added_by)}</a></h2>
                <p><a href="{$smarty.const.HOST}profil/p/{$job.job_added_by|md5}"><button class="btn btn-large btn-happinate" style="font-size: 18px;">zobacz profil pracodawcy <i class="icon icon-white icon-eye-open"></i></button></a> </p>
                {if isset($job.job_logo) && !empty($job.job_logo)}
                    <div class="logo">
                        {if $job.job_logo == 'user'}
                            <img src="{$smarty.const.HOST}upload/userLogo/{$_model->getUserLogo($job.job_added_by)}" />
                        {else}
                            <img src="{$smarty.const.HOST}upload/jobLogo/{$job.job_logo}" />
                        {/if}
                    </div>
                {/if}
            </div>
            {if $job.job_employeer_place_direct_address && $job.job_employeer_place_direct_city}
                <p><i class="icon-map-marker"></i> <a href="https://maps.google.pl/maps?q=do+{$job.job_employeer_place_direct_address},+{$job.job_employeer_place_direct_city}&hl=pl&t=m&z=15" target="_blank">{$job.job_employeer_place_direct_address}, {$job.job_employeer_place_direct_zip_code}, {$job.job_employeer_place_direct_city}</a></p>
            {/if}
            
            {if isset($job.job_employeer_place_direct_place_tips) && !empty($job.job_employeer_place_direct_place_tips)}<p>{$job.job_employeer_place_direct_place_tips}</p>{/if}

            {if isset($job.job_employeer_place_area) && $job.job_employeer_place_area}<p><i class="icon-map-marker"></i> praca w terenie</p>{/if}
            {if isset($job.job_employeer_place_telework) && $job.job_employeer_place_telework}<p><i class="icon-map-marker"></i> praca zdalna</p>{/if}
            
            {if isset($job.job_www) && !empty($job.job_www)}<p><i class="icon-globe"></i> <a href="http://{$job.job_www}" target="_blank">{$job.job_www}</a></p>{/if}
            {if isset($job.job_fanpage) && !empty($job.job_fanpage)}<p><i class="icon-star"></i> <a href="http://{$job.job_fanpage}" target="_blank">{$job.job_fanpage}</a></p>{/if}
        </div>
        <hr>
        <div class="notice-description">
            <h2>Obowiązki</h2>
            <p>{htmlspecialchars_decode($job.job_duty)}</p>
            

            {if $job.job_need}
            <h2>Wymagania</h2>
            <p>{htmlspecialchars_decode($job.job_need)}</p>
            {/if}
            
            {if $job.job_offer}
            <h2>Oferujemy</h2>
            <p>{htmlspecialchars_decode($job.job_offer)}</p>
            {/if}
                    
            {if isset($job.job_additional_salary_info) && !empty($job.job_additional_salary_info)}
            <h2>Dodatkowe informacje</h2>
            <p>{htmlspecialchars_decode($job.job_additional_salary_info)}</p>
            {/if}

            
            {if isset($job.job_contract_type) && !empty($job.job_contract_type)}
                <h2>Rodzaj umowy</h2>
                <p>{$job.job_contract_type}</p>
            {/if}
            
            {if isset($job.job_tag_type) && !empty($job.job_tag_type)}
                <h2>Główny rodzaj wykonywanej pracy</h2>
                <p>{$job.job_tag_type}</p>
            {/if}
            
            {if isset($job.job_time) && !empty($job.job_time)}
                <h2>Czas pracy</h2>
                <p>{$job.job_time}</p>
            {/if}
            
            {if isset($job.job_work_hours_per_week) && !empty($job.job_work_hours_per_week)}
                <h2>Ilość godzin pracy tygodniowo</h2>
                <p>{$job.job_work_hours_per_week}</p>
            {/if}
            
            {if isset($job.job_work_shifts_per_day) && !empty($job.job_work_shifts_per_day)}
                <h2>Ilość zmian w ciągu dnia</h2>
                <p>{$job.job_work_shifts_per_day}</p>
            {/if}
            
            {if isset($job.job_city_id) && !empty($job.job_city_id)}
                <h2>Miasto</h2>
                <p>{$job.job_employeer_place_direct_city}</p>
            {/if}  
            
            {if isset($job.job_vacancies) && !empty($job.job_vacancies)}
                <h2>Ilość wakatów</h2>
                <p>{$job.job_vacancies}</p>
            {/if}             
            
            {if isset($job.job_additional_oneclick_info) && !empty($job.job_additional_oneclick_info)}
                <h2>Dodatkowe informacje dla kandydatów</h2>
                <p>{$job.job_additional_oneclick_info}</p>
            {/if}            
        </div>
    </div>
    <div class="right-column notice-more-column">

        {if $smarty.const.MOD_CALENDAR && $isCalendar}
            <a href="/praca/kalendarz/{$job.job_id}" class="notice-calendar-link">
                <div class="notice-calendar" style="margin-bottom: 5px;">   
                    <h3>Kalendarz rekrutacji</h3>
                    <div class="show-hide-icon"></div>
                </div>
            </a>
        {/if}

        {if isset($job.job_employeer_place_direct)}
            <div class="jakdojadeMapa">   
                <a href="http://poznan.jakdojade.pl/?tc={$job.job_lat}:{$job.job_long}" target="_blank"><span>Sprawdź jak dojedziesz! <img width="100px" align="right" src="{$smarty.const.HOST}view/images/jakdojade.jpg" /></span></a>
            </div>
        {/if}
        
        <div class="notice-rate">
            <h3>STAWKA BRUTTO</h3>
            {if isset($job.job_hide_salary) && $job.job_hide_salary}
                <p>oferta bez podania stawki godzinowej</p>
            {elseif isset($job.job_volunteering) && $job.job_volunteering}
                <p>wolontariat, staż / praktyki bezpłatne</p>
            {else}
                
                <p class="rate">zł/godzinę</p>

                {if $job.job_rate_min==$job.job_rate_max}
                    {assign var=rate value="."|explode:$job.job_rate_min}

                    <p class="value"><strong>{$rate.0}<small>,{if isset($rate.1)}{$rate.1}{if strlen($rate.1)==1}0{elseif strlen($rate.1)==0}00{/if}{else}00{/if}</small></strong></p>
                {else}
                    {assign var=rate_min value="."|explode:$job.job_rate_min}
                    {assign var=rate_max value="."|explode:$job.job_rate_max} 

                    <p class="value"><small>OD</small> <strong>{$rate_min.0}<small>,{if isset($rate_min.1)}{$rate_min.1}{if strlen($rate_min.1)==1}0{elseif strlen($rate_min.1)==0}00{/if}{else}00{/if}</small></strong> &nbsp; <small>DO</small> <strong>{$rate_max.0}<small>,{if isset($rate_max.1)}{$rate_max.1}{if strlen($rate_max.1)==1}0{elseif strlen($rate_max.1)==0}00{/if}{else}00{/if}</small></strong></p>

                {/if}
            
            {/if}
        </div>

        {if $job.job_publish_date_end|strtotime > $smarty.now && (!$_user || ($_user && $_user->getUserType()==1))}
        <div class="notice-apply">
            {if !$_user || !$_model->checkIfUserAppliedToNormalJob($job.job_id, $_user->getUserId())}
            <h3>APLIKUJ</h3>
            <form action="" method="post" enctype="multipart/form-data" id="applyForm">
                <input type="hidden" name="apply_submit" value="1" />
                <input type="hidden" name="cv_id" value="0" id="cv_id" />
                <label>
                    <span class="input-box-label">Imię i nazwisko
                    <input type="text" name="apply_name" placeholder="Wpisz imię i nazwisko" value="{if $_user}{$_user->getUserName()}{if $_user->getUsersurname()} {$_user->getUsersurname()}{/if}{elseif isset($smarty.post.apply_name)}{$smarty.post.apply_name}{/if}" autocomplete="off">
                </label>
                <label>
                    <span class="input-box-label">Adres email <span class="red-text">(wymagane)</span></span>{if isset($errorArray.apply_email) && $errorArray.apply_email}<span class="label-error"></span>{/if}
                    {if $_user}<input type="hidden" name="apply_email" value="{$_user->getUserEmail()}" />{/if}
                    <input type="text" name="apply_email" {if $_user}disabled="disabled"{/if} placeholder="Wpisz adres email" value="{if $_user}{$_user->getUserEmail()}{elseif isset($smarty.post.apply_email)}{$smarty.post.apply_email}{/if}" autocomplete="off">

                </label>
                <label class="agree input-checkbox free-pay">
                    <input type="checkbox" name="warunki" value="1" {if isset($smarty.post.warunki) && $smarty.post.warunki} checked="checked"{/if}>
                    <div class="checkbox-bg" style="margin-right: 20px;">
                        <div class="check-bg"></div>
                    </div>
                    <small>{if isset($errorArray.warunki) && $errorArray.warunki}<span class="label-error" style="margin-left: 0px;"></span> {/if}Wyrażam zgodę na przetwarzanie przez administratora moich danych osobowych, które przekazałem za pomocą aplikacji dostępnych na niniejszej stronie internetowej, oraz na ich udostępnienie potencjalnym pracodawcom, w celu wykonania na moją rzecz usług pośrednictwa pracy.</small>
                </label>
                <div class="clear"></div>
                <div class="action-file-box">
                    <div class="btn-browser" style="width:241px;">
                        <span class="file-label">{if isset($errorArray.cv) && $errorArray.cv}<span class="label-error"></span>{/if} DOŁĄCZ PLIK CV <i class="icon-white icon-upload"></i></span>
                        <input type="file" name="cv">
                    </div>
                    <span class="btn-erase tooltip-item" data-original-title="Usuń"><i class="icon-trash icon-white"></i></span>
                </div>
                <div class="clear" style="height:10px;"></div>
                {if $_user && !empty($_user)}
                <div style="text-align:center;">lub</div>
                <div class="clear" style="height:10px;"></div>
                <span class="apply-submit" data-target="#cvList" data-toggle="modal" style="cursor: pointer; background: #39ADAD; text-align: center;">{if isset($errorArray.cv) && $errorArray.cv}<span class="label-error"></span>{/if} WYBIERZ JUŻ ISTNIEJĄCE CV <i class="icon-white icon-search"></i></span>
                <div class="clear" style="height:10px;"></div>
                {/if}
                <button onclick="$('#applyForm').submit();" class="btn btn-large btn-happinate" style="width: 290px;">APLIKUJ O PRACĘ <i class="icon-chevron-right icon-white"></i></button>
            </form>
            {else}
            <h2>JUŻ APLIKOWAŁAŚ/EŚ NA TO OGŁOSZENIE</h2>
            <h1>Dziekujemy!</h1>
            {/if}
        </div>
        {/if}
        {if $_user && $job.job_publish_date_end|strtotime > $smarty.now}
            {if isset($job.job_contact_name) || isset($job.job_contact_email) || isset($job.job_contact_phone)}
            <div class="notice-contact-info">
                <h3>DANE KONTAKTOWE</h3>         
                {if isset($job.job_contact_phone) && !empty($job.job_contact_phone)}
                <div class="show-phone-number">
                    <p class="phone-number" data-phone="{$job.job_contact_phone}"></p>
                    <div class="show-phone-number-mask">
                        <i class="icon-corner-phone"></i>
                        <p>POKAŻ NUMER TELEFONU</p>
                    </div>
                </div>
                {/if}
            </div>
            {/if}
        {/if}
    </div>

    <div class="clear"></div>
{/block}