{extends file="index.tpl"}

{block name=body}

    <div class="">

        <div class="job-title">
            <h2>Kalendarz rekrutacji {$job.job_position}</h2>
        </div>
        <div class="job-subtitle">
            <h3>Wybrany dzień: {$day}</3>
        </div>

        <div class="calendar-content-day">

            <table class="calendar-hours">
                {foreach from=$hours item=hour}
                    <tr>
                        <td class="hour-counter">
                            {$hour.label}
                        </td>
                        <td class="content {$hour.css}">
                            {if $hour.active == '1'}
                                <span>Wolnych miejsc: {$calendar.candidatePerHour-$hour.usersCounter}/{$calendar.candidatePerHour}</span>
                            {else}
                                <span>Termin niedostępny</span>
                            {/if}      
                        </td>
                        <td class="info">
                            {if $hour.active == '1'}
                                <form method="post">
                                    <input type="hidden" name="act" value="addme">
                                    <input type="hidden" name="data[date]" value="{$day}">
                                    <input type="hidden" name="data[hour]" value="{$hour.value}">
                                    <input type="hidden" name="data[job_id]" value="{$job.job_id}">  
                                    {if $usertype == 1}                                  
                                        <button type="submit" class="happi-btn">Kliknij aby zapisać się na daną godzinę</button>
                                    {/if}
                                </form>
                            {/if}
                        </td>
                    </tr>
                {/foreach}
            </table>


        </div>

    </div>

    <div class="clear"></div>
{/block}