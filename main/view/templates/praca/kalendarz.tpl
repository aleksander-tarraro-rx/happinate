{extends file="index.tpl"}

{block name=body}

    <div class="">
        <div class="job-title">
            <h2>Kalendarz rekrutacji {$job.job_position}</h2>
        </div>

        <div class="calendar-big">
        {foreach from=$structure item=months key=year}            
            
            <h3>Rok {$year}</h3>
            {foreach from=$months item=days key=month}
                
                <h4>{$month}</h4>
                {foreach from=$days item=day}
                    
                    <a href="{if $day.css != 'notactive'}/praca/kalendarz-dzien/{$job.job_id}/{$day.url}{else}javascript:void(0){/if}" class="{$day.css}" title="{$day.title}">
                        <span class="day">
                            {$day.label}
                        </span>     
                    </a>                           

                {/foreach}                           
                <div class="clear"></div>

            {/foreach}

        {/foreach}
            <div class="clear"></div>
        </div>

    </div>

    <div class="clear"></div>
{/block}