{extends file="index.tpl"}

{block name=body}

    <div class="left-column">
        <div class="job-title">
            <h2 style="padding:15px 0px 10px 0px;">{$job.job_position}</h2>
        </div>
        <hr>
        <div class="notice-description" style="padding: 20px; border: #39ADAD 1px solid; line-height: 150%;">
            {$job.job_tresc}
        </div>
    </div>
    <div class="right-column notice-more-column">
        {if $job.job_logo}
            <img src="{$job.job_logo}" class="img-rounded" style="margin-bottom: 10px; "/>
        {/if}
        <a href="{$job.job_url}" target="_blank" class="btn btn-success btn-large" style="width: 88%">APLIKUJ O PRACĘ</a>
    </div>

    <div class="clear"></div>
{/block}