function readURL(input) {
    $('#imagePreview').attr('src', '');
    $('.img-box-2').show();
    $('.img-box').hide();
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        $('#file_delete').val(false);
    }
}
function readCV(input) {
    $('#uploaded').show();
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            
            $('#uploaded').css('display', 'inline-block');
            if(input.files[0].type == 'application/pdf' || input.files[0].type == 'image/jpeg' || input.files[0].type == 'image/gif' || input.files[0].type == 'image/png' || input.files[0].type == 'application/msword' || input.files[0].type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || input.files[0].type == 'application/vnd.oasis.opendocument.text') {
                $('#uploaded').removeClass('label-warning').addClass('label-success');
                $('#uploaded').html('<i class="icon icon-file icon-white"></i> Udało Ci się wybrać plik CV : <b>'+input.files[0].name+'</b>');
            } else {
                $('#uploaded').removeClass('label-success').addClass('label-warning');
                $('#uploaded').html('<i class="icon icon-remove icon-white"></i> Proszę wybrać plik o innym formacie. Dostępne formaty to :<br/> <b>jpeg</b>, <b>pdf</b>, <b>gif</b>, <b>png</b>, <b>doc</b>, <b>docx</b>, <b>odt</b>');
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL2(input) {
    $('#imagePreview2').attr('src', '');
    $('.img-box-22').show();
    $('.img-box-222').hide();
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview2').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        $('#file_delete2').val(false);
    }
}
function deleteFile() {
    $('#file_delete').val(true);
    $('#imagePreview').attr('src', '');
    $('.img-box-2').hide();
    $('.img-box').hide();
    $('.action-file-box').css({ marginTop: 0 });
}
function deleteFile2() {
    $('#file_delete2').val(true);
    $('#imagePreview2').attr('src', '');
    $('.img-box-22').hide();
    $('.img-box-222').hide();
    $('.action-file-box').css({ marginTop: 0 });
}
function nameReplace(name,length){
	var oldName = name.slice(0,length);
	if(name.length<=length){
		return oldName;
	}else{
		var newName = oldName+'...';
		return newName;
	}
}

function addExperienceBox(element, id){
	var html = '<div class="experience-box">'+
					'<div class="input-box left">'+
						'<label>'+
							'<span class="input-box-label">Nazwa firmy</span>'+
							'<input type="text" name="newexperience['+id+'][]" placeholder="Wpisz nazwę firmy" value="" autocomplete="off">'+
						'</label>'+
					'</div>'+
					'<div class="input-box left" style="margin:0px auto;padding:0px;">'+
						'<label class="calendar input-box-half">'+
							'<span class="input-box-label">Okres zatrudnienia</span>'+
							'<input type="text" name="newexperience['+id+'][]" class="experience-date" placeholder="od" value="">'+
							'<i class="icon-calendar-input"></i>'+
						'</label>'+
						'<label class="calendar input-box-half right">'+
							'<span class="input-box-label"></span>'+
							'<input type="text" name="newexperience['+id+'][]" class="experience-date" placeholder="do" value="">'+
							'<i class="icon-calendar-input"></i>'+
						'</label>'+
					'</div>'+
					'<div class="clear"></div>'+
					'<div class="input-box left">'+
						'<label>'+
							'<span class="input-box-label">Stanowisko</span>'+
							'<input type="text" name="newexperience['+id+'][]" placeholder="Wpisz stanowisko" value="" autocomplete="off">'+
						'</label>'+
					'</div>'+
					'<a href="javascript:;" onClick="addExperienceBox(this,'+(id+1)+'); return false;" class="happi-btn add-btn"><strong>+</strong> DODAJ</a>'+
					'<a href="javascript:;" onClick="removeExperienceBox(this); return false;" class="happi-btn brown remove-btn"><i class="icon-trash icon-white"></i> USUŃ</a>'+
					'<div class="clear"></div>'+
				'</div>';
	$(html).insertAfter($('.experience-box:eq('+($('.experience-box').length-1)+')'));
	updateDataPickerExperience();
}

function removeExperienceBox(element, id){
	if($('.experience-box').length>1) {
            $(element).parent().remove();
        } else {
            $(element).parent().remove();
            addExperienceBox(element);
        }
        var html = '<input type="hidden" name="delexperience[]" value="'+id+'" />';
        $('.main-form').append(html);
	updateDataPickerExperience();
}

function addEducationBox(element, id){
	var html = '<div class="education-box">'+
					'<div class="input-box left">'+
						'<label>'+
							'<span class="input-box-label">Nazwa uczelni</span>'+
							'<input type="text" name="neweducation['+id+'][]" placeholder="Wpisz nazwę uczelni" value="" autocomplete="off">'+
						'</label>'+
					'</div>'+
					'<div class="input-box left" style="margin:0px auto;padding:0px;">'+
						'<label class="calendar input-box-half">'+
							'<span class="input-box-label">Okres nauczania</span>'+
							'<input type="text" name="neweducation['+id+'][]" class="experience-date" placeholder="od" value="">'+
							'<i class="icon-calendar-input"></i>'+
						'</label>'+
						'<label class="calendar input-box-half right">'+
							'<span class="input-box-label"></span>'+
							'<input type="text" name="neweducation['+id+'][]" class="experience-date" placeholder="do" value="">'+
							'<i class="icon-calendar-input"></i>'+
						'</label>'+
					'</div>'+
					'<div class="clear"></div>'+
					'<div class="input-box left">'+
						'<label>'+
							'<span class="input-box-label">Kierunek</span>'+
							'<input type="text" name="neweducation['+id+'][]" placeholder="Wpisz kierunek" value="" autocomplete="off">'+
						'</label>'+
					'</div>'+
					'<a href="javascript:;" onClick="addEducationBox(this, '+(id+1)+'); return false;" class="happi-btn add-btn"><strong>+</strong> DODAJ</a>'+
					'<a href="javascript:;" onClick="removeEducationBox(this); return false;" class="happi-btn brown remove-btn"><i class="icon-trash icon-white"></i> USUŃ</a>'+
					'<div class="clear"></div>'+
				'</div>';
	$(html).insertAfter($('.education-box:eq('+($('.education-box').length-1)+')'));
	updateDataPickerExperience();
}

function removeEducationBox(element, id){
	if($('.education-box').length>1) {
            $(element).parent().remove();
        } else {
            $(element).parent().remove();
            addExperienceBox(element);
        }
        var html = '<input type="hidden" name="deleducation[]" value="'+id+'" />';
        $('.main-form').append(html);
	updateDataPickerExperience();
}

function updateDataPickerExperience(){
	$('.experience-date').datepicker('destroy');
	$('.experience-date').datepicker({
		changeMonth: true,
		changeYear: true,
		showOtherMonths: true,
		selectOtherMonths: true,
		autoSize: true,
		dateFormat: 'yy-mm-dd',
		showOptions: { direction: "down" },
		firstDay: 1,
		dayNamesMin: [ "Ni", "Pn", "Wt", "Śr", "Cz", "Pt", "So" ],
		monthNames: [ "Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień" ],
		monthNamesShort: [ "Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru" ]
	});
}

$(document).ready(function() {
	
	$('input,textarea').placeholder();
	
	//	Popover tips run
	$('.help').hover(function(){
		$(this).popover('show');
	},function(){
		$(this).popover('hide');
	});
	
	// Tooltips item run
	$('.tooltip-item').tooltip({
		placement: 'top'
	});
	
	$('.driving-licence-check').each(function(index, element) {
		if($(element).find('input[type=checkbox]').attr('checked')=='checked'){
			$(element).addClass('active');
		}
	});
	
	$('.driving-licence-check').mousedown(function(){
		if($(this).find('input[type=checkbox]').attr('checked')=='checked'){
			$(this).removeClass('active');
		}else{
			$(this).addClass('active');
		}
	});
	
	// Select list
	if($('.select-box').length>0){
		$('.select-box').click(function(){
			var element = $(this);
			if($(element).is('.active')){
				$(element).removeClass('active');
				$(element).find('ul').slideUp('fast');
			}else{
				if($('.select-box.active').length>0){
					$('.select-box.active').removeClass('active').find('ul').slideUp('fast',function(){
						$(element).addClass('active');
						$(element).find('ul').slideDown('fast');
					});
				}else{
					$(element).addClass('active');
					$(element).find('ul').slideDown('fast');
				}
			}
		});
		$('#city').find('li').click(function(){
			var value = $(this).text();
			
			$(this).parent().parent().find('.select-value').text(value);
			$(this).parent().parent().find('input[name=city]').val(value);
		});
		$('#contract-type').find('li').click(function(){
			var value = $(this).text();
			
			$(this).parent().parent().find('.select-value').text(value);
			$(this).parent().parent().find('input[name=contract_type]').val(value);
		});
		$('#trade').find('li').click(function(){
			var value = $(this).text();
			
			$(this).parent().parent().find('.select-value').text(value);
			$(this).parent().parent().find('input[name=trade_type]').val(value);
		});
		$(document).click(function(e){
			if(!$(e.target).is('.select-box, .select-value, .icon-select-list, .input-area')){
				$('.select-box').find('ul').slideUp('fast');
			}
		});
	}
	
	// Input file
	/*$('.file-input-box').find('input').change(function(){
		var imgName = $(this).val().replace('C:\\fakepath\\','');
		$('.file-input-box').find('.img-box').html('<p title="'+imgName+'">'+nameReplace(imgName,35)+'</p>');
	});*/
	
	// Input cv
	$('.notice-apply').find('input[type=file]').change(function(){
		var imgName = $(this).val().replace('C:\\fakepath\\','');
		$('.notice-apply').find('.btn-browser').find('.file-label').text(nameReplace(imgName,25));
	});
	$('.notice-apply').find('.btn-erase').click(function(){
		$('.notice-apply').find('.btn-browser').find('.file-label').text('DOŁĄCZ CV');
		$('.notice-apply').find('input[type=file]').val('');
	});
	
	// Input cv profil candidate
	$('.candidate-profile-cv').find('input[type=file]').change(function(){
		var imgName = $(this).val().replace('C:\\fakepath\\','');
		$('.candidate-profile-cv').find('.btn-browser').find('.file-label').text(nameReplace(imgName,25));
	});
	$('.candidate-profile-cv').find('.btn-erase').click(function(){
		$('.candidate-profile-cv').find('.btn-browser').find('.file-label').text('DOŁĄCZ CV');
		$('.candidate-profile-cv').find('input[type=file]').val('');
	});
	
	// Add list item
	if($('.add-item').length>0){
		function addList(){
			var index = $(this).parent().parent().find('.add-item').length+1;
			var name = $(this).parent().parent().find('.add-item').children('input').attr('name');
                        
                        //alert(name);
                        
                        if(name!='newskills[]' && name!='newduties[]' && name!='newneeds[]') {
                            name = 'new'+name.replace(/\d+/g, '');;
                        }
                        
			var value = $(this).parent().parent().find('.add-item').children('input').attr('placeholder');
			var html =	'<div class="add-item">'+
							'<input type="text" name="'+name+'" placeholder="'+value+'" value="">'+
							'<span class="delete-item-btn tooltip-item" title="Usuń"><i class="icon-delete-item"></i></span>'+
                    		'<span class="add-item-btn tooltip-item" title="Dodaj"><i class="icon-add-item"></i></span>'+
						'</div>';
			$(this).parent().parent().find('.add-item:eq('+(index-2)+')').after(html);
			$(this).parent().parent().find('.add-item:last').children('.add-item-btn').bind('click',addList);
			$(this).parent().parent().find('.add-item:last').children('.delete-item-btn').bind('click',deleteList);
			$('.tooltip-item').tooltip({placement:'top'});
		}
		
		function deleteList(){
			var index = $(this).parent().parent().find('.add-item').length;
			var name = $(this).parent().parent().find('.add-item').children('input').attr('name');
			var html = '<input type="hidden" name="del'+name+'" value="'+index+'" />';
			$('.main-form').append(html);
			//if($(this).parent().parent().find('.add-item').length>1) 
                           $(this).parent().remove();
		}
		
		$('.add-item-btn').bind('click',addList);
		$('.delete-item-btn').bind('click',deleteList);
	}
		
	// Calendar
	$('.icon-calendar-input').click(function(){
		$(this).parent().find('input[type=text]').focus();
	});
	$('.start-date').datepicker({
		showOtherMonths: true,
		selectOtherMonths: true,
		dateFormat: 'yy-mm-dd',
		showOptions: { direction: "down" },
		minDate: 0,
		firstDay: 1,
		dayNamesMin: [ "Ni", "Pn", "Wt", "Śr", "Cz", "Pt", "So" ],
		monthNames: [ "Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień" ]
	});
	
	if($('.experience-date').length>0)updateDataPickerExperience();
		
});