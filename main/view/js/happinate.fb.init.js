(function(d, debug){
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/pl_PL/all" + (debug ? "/debug" : "") + ".js";
	ref.parentNode.insertBefore(js, ref);
}(document, true));

function pobierzDane(nazwa){
	return localStorage.getItem(nazwa);
}

function zapiszDane(nazwa, wartosc){
	localStorage.setItem(nazwa,wartosc);
}

function getFriends() {
    FB.api('/me/friends', function(response) {
        if(response.data) {
            var friendsList = [];
            $.each(response.data,function(index,friend) {
                friendsList[index] = friend.id;
            });
            zapiszDane('fb_frieds',friendsList);
            window.location = '/logowanie-przez-facebooka';
        } else {
            alert("Error!");
        }
    });
}

function initApp(){
	FB.api('/me', function(response) {
            zapiszDane('fb_imie',response.first_name);
            zapiszDane('fb_nazwisko',response.last_name);
            zapiszDane('fb_id',response.id);
            zapiszDane('fb_emal',response.email);
            getFriends();
	});
}

function fbLoginUser(){
	FB.login(function(response){
		if(response.authResponse){
			initApp();
		}else{
			console.log('nu nu nu');
		}
	},{
		scope : 'email'
	});
}

window.fbAsyncInit = function() {
	// init the FB JS SDK
	
	FB.init({
		appId      : '564144690330763',
		channelUrl : '/logowanie-przez-facebooka',
		status     : true,
		cookie     : true,
		xfbml      : true,
		frictionlessRequests: true
	});
			
};