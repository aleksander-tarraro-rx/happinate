function gotoHowWorks(e){
	$('#videoModal').modal('hide');
	setTimeout(function(){
		window.location = $(e).attr('data-href');
	},1000);
}

$(document).ready(function() {
	// Add off notice
	$('.one-notice.off').each(function(index, element) {
		$(element).append('<div class="offer-off"></div>');
	});
	
	// Hide success alert
	$('.alert.alert-success').delay(2000).slideUp('slow');
		
	// Contact map
	if($('.contact-map').length>0){
		var googleMapContent = '<div style="overflow:hidden;font-size:14px;color:#000;line-height:17px;"><p>happinate sp. z o.o.</p>'+
								'<p>ul. A. Baraniaka 88E (budynek F)</p>'+
								'<p>61-131 Poznań</p>'+
								'<div class="clear" style="height:5px;"></div>'+
								'<a href="https://maps.google.pl/maps?q=do+52.3998286,16.96590070000002(happinate)&hl=pl&t=m&z=15" target="_blank">google maps</a> | '+
								'<a href="http://poznan.jakdojade.pl/?tn=happinate&td=zapraszamy!!!&tc=52.3998286:16.96590070000002" target="_blank"> jak dojadę</a></div>';
		
		$('.contact-map').gmap().bind('init', function(ev, map) {
			$('.contact-map').gmap('addMarker', {'icon': 'view/images/marker.png', 'position': '52.3998286,16.96590070000002', 'bounds': true}).click(function() {
				$('.contact-map').gmap('openInfoWindow', {'content': googleMapContent}, this );
			});
			$('.contact-map').gmap('option', 'zoom', 15);
		});
	}
	
	// Login box
	$('.login-box').children('.happi-btn').click(function(){
		$('.register-box').fadeToggle(100);
	});
	$(document).click(function(e){
		if(!$(e.target).is($('.login-box').children()) && !$(e.target).is($('.register-box').children()) && !$(e.target).is($('.login-form').children()) && !$(e.target).is($('.input-text').children()) && !$(e.target).is($('.login-box').find('i'))){
			$('.register-box').fadeOut(100);
		}
	});
	
	// Video modal
	$('#videoModal').on('hidden', function () {
		$('#videoModal').find('.modal-body').html('');
	});
	
	$('#videoModal').on('show', function () {
		$('#videoModal').find('.modal-body').html('<iframe width="560" height="315" src="http://www.youtube.com/embed/yzA8D6_ZwQc?autoplay=1" frameborder="0" allowfullscreen></iframe>');
	});
	
	// Tytuły w karuzeli
	$('.karusela').find('.notice-title').each(function(index, element) {
		var oldText = $(element).children('a').text();
		$(element).children('a').attr('title',oldText).text(nameReplace(oldText,52));
	});
	
	$('.karusela-all').find('.notice-title').each(function(index, element) {
		var oldText = $(element).children('a').text();
		$(element).children('a').attr('title',oldText).text(nameReplace(oldText,40));
	});
	
    // $('.job-title').find('h2').each(function(index, element) {
    //     var oldText = $(element).html();
    //     $(element).html(nameReplace(oldText,40));
    // });
        
	$('.one-notice-text-box').click(function(){
		if(!$(this).is('.off')) window.location = $(this).find('a').attr('href');
		else return false;
	});
        
	$('.one-notice-logo').click(function(){
		if(!$(this).is('.off')) window.location = $(this).parent().find('a').attr('href');
		else return false;
	});
        
	$('.one-notic-pay').click(function(){
		if(!$(this).is('.off')) window.location = $(this).parent().find('a').attr('href');
		else return false;
	});
	// Filtry
	$('.filtry').find('li').click(function(){
		if($(this).is('.active')){
                    $(this).removeClass('active');
                    $(this).find('.erase-filtr').remove();
                    $(this).find('input').val(0);
		}else{
                    $(this).addClass('active').append('<span class="erase-filtr"><span class="erase-icon-bg"><i class="icon-ok-sign icon-white"></i></span></span>');
                    $(this).find('input').val(1);
		}
	});
	
	$('.filtry').find('small').click(function(){
		var klasa = $(this).parent().attr('class');
		$('.filtry').find('.'+klasa).find('li').removeClass('active');
		$('.filtry').find('.'+klasa).find('li').find('.erase-filtr').remove();
                $('.filtry').find('.'+klasa).find('input').val(0);
	});
	
	// Notice more box view
	$('.notice-more-info').click(function(){
		if($('.show-hide-icon').is('.active')){
			$('.show-hide-icon').removeClass('active');
			$('.show-hide-info').slideUp('fast');
		}else{
			$('.show-hide-icon').addClass('active');
			$('.show-hide-info').slideDown('fast');
		}
	});
	
	// Notice share
	$('.notice-share').hover(function(){
            $(this).children('ul').fadeIn('fast');	
	},function(){
            $(this).children('ul').fadeOut('fast');		
	});
	
	// Show phone number
	if($('.show-phone-number').length>0){
		$('.show-phone-number').click(function(){
			var number = $(this).children('p').attr('data-phone');
			$(this).children('p').css('text-align','center').text('tel.: '+number);
			$('.show-phone-number-mask').fadeOut(300);
		});
		
		var phoneNumber = $('.show-phone-number').find('.phone-number').attr('data-phone').slice(0,3);
		$('.show-phone-number').find('.phone-number').text(phoneNumber+' ...');
	}
	
	// Apla animation
	$('.apla-img').animate({marginLeft:50,opacity:1},'slow');
	$('.apla-haslo').animate({paddingTop:10,opacity:1},'slow');
	
	// How works
	$('.how-works-human').find('.human').click(function(){
		var index = $(this).index();
		$('.how-works-human').find('.active').removeClass('active');
		$(this).addClass('active');
		$('.how-works-description').find('.active').removeClass('active');
		$('.how-works-description').find('.human-description:eq('+index+')').addClass('active');
	});
	
	$('.how-works-header').find('.human-header').click(function(){
		var index = $(this).index();
		$('.how-works-human').find('.active').removeClass('active');
		$('.how-works-human').find('.human:eq('+index+')').addClass('active');
		$('.how-works-description').find('.active').removeClass('active');
		$('.how-works-description').find('.human-description:eq('+index+')').addClass('active');
	});

    $('.calendarUnreadTrigger').on('click', function(e) {
        e.preventDefault();
        var $calendar = $('#calendarUnread');
        if ($calendar.length) {
            $calendar.modal('show');
        }
    });
});

window.onload = function(){
	
	// Karuzela	
	if($('.karusela-mask').length>0){
		var heightKaruzela = $('.karusela').height();
		var heightMaskaKaruzela = 300;
		var heightOneNotice = 80;
		var karuzelaLength = ($('.karusela').children('.one-notice').length)-1;
				
		var lastNotice = $('.one-notice:eq('+karuzelaLength+')');
		lastNotice.insertBefore($('.one-notice:eq(0)'));
		$('.karusela').css('top',-heightOneNotice+'px');
		
		function karuzelaRotate(){
			clearTimeout(karuzelaTimeout);
			$('.karusela').animate({top:0},'slow',function(){
			
				var lastNotice = $('.one-notice:eq('+karuzelaLength+')');
				lastNotice.insertBefore($('.one-notice:eq(0)'));
				
				$('.karusela').css('top',-heightOneNotice+'px');
			});
			
			karuzelaTimeout = setTimeout(karuzelaRotate,3000);
		}
		
		$('.karusela-mask').hover(function(){
			clearTimeout(karuzelaTimeout);
		},function(){
			karuzelaRotate();
		});
		
		var karuzelaTimeout = setTimeout(karuzelaRotate,3000);
	}
	
	// Slider	
	if($('.slider').length>0){
            
	}
}