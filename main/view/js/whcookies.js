/*
 * Skrypt wyświetlający okienko z informacją o wykorzystaniu ciasteczek (cookies)
 * 
 * Więcej informacji: http://webhelp.pl/artykuly/okienko-z-informacja-o-ciasteczkach-cookies/
 * 
 */

function WHCreateCookie(name, value, days) {
    var date = new Date();
    date.setTime(date.getTime() + (days*24*60*60*1000));
    var expires = "; expires=" + date.toGMTString();
	document.cookie = name+"="+value+expires+"; path=/";
}
function WHReadCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
	}
	return null;
}

window.onload = WHCheckCookies;

function WHCheckCookies() {
    if(WHReadCookie('cookies_accepted') != 'T') {
        var html = document.getElementsByTagName('html')[0];
        html.style.marginTop = '48px';
        
        var message_container = document.createElement('div');
        message_container.id = 'cookies-message-container';
        var html_code = '<div id="cookies-message" style="padding: 7px 0px; font-size: 14px; line-height: 22px; text-align: center; position: fixed; top: 0px; background-color: #ededed; width: 100%; z-index: 999;">Ta strona używa ciasteczek (cookies), dzięki którym nasz serwis może działać lepiej. <a href="http://wszystkoociasteczkach.pl" target="_blank" style="color: #39ADAD;">Dowiedz się więcej</a>. <a href="javascript:WHCloseCookiesWindow();" id="accept-cookies-checkbox" name="accept-cookies" style="display: inline-block; width: 82px; margin-left: 210px; padding: 5px 10px 5px 10px; background: #39ADAD; border: 1px solid #268888; color: #FFF; font-size: 15px; cursor: pointer; -webkit-transition: opacity 100ms ease-out; -moz-transition: opacity 100ms ease-out; -ms-transition: opacity 100ms ease-out; -o-transition: opacity 100ms ease-out; transition: opacity 100ms ease-out; opacity: 1; font-family: \'PT Sans Narrow\', sans-serif; text-align: center;">Rozumiem</a></div>';
        message_container.innerHTML = html_code;
        document.body.appendChild(message_container);
    }
}

function WHCloseCookiesWindow() {
    WHCreateCookie('cookies_accepted', 'T', 365);
    document.getElementById('cookies-message-container').removeChild(document.getElementById('cookies-message'));
    var html = document.getElementsByTagName('html')[0];
    html.style.marginTop = '0px';
}
