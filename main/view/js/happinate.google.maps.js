var geocoder;
var polska = new google.maps.LatLng(52.45029085241015,18.947489018750048);
var marker;
var map;

function initializeGMap() {
	geocoder = new google.maps.Geocoder();
	var mapOptions = {
          zoom: 5,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          center: polska
        };

	map = new google.maps.Map(document.getElementById('map_canvas'),mapOptions);

	marker = new google.maps.Marker({
	  map:map,
	  //draggable:true,
	  position: polska
	});
	
	google.maps.event.addListener(marker, 'mouseup', function() {
		$('input[name=coords]').val(marker.getPosition().toString());
	});
	
	
	if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			$('input[name=coords]').val(pos.toString());
			map.setCenter(pos);
			map.setZoom(15);
			marker.setPosition(pos);
		});
	}
}

function codeAddress() {
	var address = $('#7').val()+', '+$('#8').val()+', '+$('#9').val();
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			map.setCenter(results[0].geometry.location);
			marker.setPosition(results[0].geometry.location);
			map.setZoom(15);
			$('input[name=coords]').val(marker.getPosition().toString());
		} else {
			//alert('Geocode was not successful for the following reason: ' + status);
		}
	});
}