$(document).ready(function() {
	init();
});


function init()
{
	if($('.calendar-day-edit').length > 0 )
	{
		getData('00','00','0000');
		$(".calendar-day-edit").click(function(){
			var day = $(this).attr('data-day');
			var year = $('.calendar-year').val();
			var month = $('.calendar-month').val();
			getData(day,month,year);
			return false;
		});
	}

	$("[rel='tooltip']").tooltip({html:true});
}


function getData(day,month,year)
{  
	$('.calendar-progress').show();
	$.post(
	    "/moje-konto/ajax-get-calendar-data",
	    {'day':day, 'month':month, 'year':year},
	    function(response) {

	    	$('.calendar-progress').hide();
	    	$('.calendar-dayinfo').html(response.header);
	    	$('.form-date').val(response.header);

			$('.calendar-day-posistions').empty();
			$('.calendar-day-posistions-content').empty();

			console.log(response);
	    	if(response.positions)
	    	{
	    		var i = 0;
	    		var active = "";
				$.each(response.positions, function(key, val){	

					if(i==0)
					{
						active="active";
					}
					else
					{
						active="";
					}
					var act = "<input type='hidden' name='act' value='del'>";
					var id = "<input type='hidden' name='id' value='"+key+"'>";
					var form = "<form method='post'>"+act+id+"<button type='submit' class='calendar-delete-position btn btn-danger'>Usuń</button></form>"
					$('.calendar-day-posistions').append( "<li class='"+active+"'><a href='#r"+i+"' data-toggle='tab'>" + val.time + "</a></li>" );
					$('.calendar-day-posistions-content').append( "<div class='tab-pane "+active+" calendar-position-info' id='r"+i+"'><p>"+val.information+" <br><br>"+form+"</p></div>" );
					i++;
				});	 
				$('.calendar-button-save').show();   		
	    	}

	        console.log(response);
	       
	    }, 'json' 
	);
}