jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "date-uk-pre": function (a) {
        var ukDatea = a.split('-');
        return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    },

    "date-uk-asc": function (a, b) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "date-uk-desc": function (a, b) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
});

function initialTable() {
    var oTable = $('.table.notice').dataTable({
        "aaSorting": [[1, "desc"]],
        "sPaginationType": "full_numbers",
        "iDisplayLength": 50,
        "bFilter": false,
        "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null
        ],
        "oLanguage": {
            "oPaginate": {
                "sFirst": "&laquo;",
                "sLast": "&raquo;",
                "sNext": "&rsaquo;",
                "sPrevious": "&lsaquo;"
            },
            "sSearch": '<div class="input-append search-entries-input">' +
            '<input type="text" placeholder="np. nazwa, stanowisko..." autocomplete="off">' +
            '<span class="btn"><i class="icon-search"></i></span>' +
            '</div>',
            "sEmptyTable": "Brak danych w tabeli",
            "sZeroRecords": "Brak danych do wyświetlenia",
            "sInfoEmpty": "",
            "sInfo": "Pozycje od _START_ do _END_ (wszystkich _TOTAL_)",
            "sInfoFiltered": "",
            "sLengthMenu": 'Pokaż <select class="input-small" style="margin:0px;">' +
            '<option value="50">50</option>' +
            '<option value="100">100</option>' +
            '<option value="200">200</option>' +
            '<option value="-1">Wszystkie</option>' +
            '</select> pozycji'
        }
    });

    $('.search-entries-input').parent().children('input[type=text]').remove();

    $('.table-select').find('.btn').click(function () {
        if ($(this).is('.active') && $('.table-select').find('.active').length > 1)$(this).removeClass('active');
        else $(this).addClass('active');

        var whatSort = 0;
        $('.table-select').find('.btn').each(function (index, element) {
            if (!$(element).is('.active')) whatSort++;
        });

        if (whatSort == 0)oTable.fnFilter('', 4);
        else {
            if ($('.table-select').find('.btn.active').index() == 0)oTable.fnFilter('aktywny', 4);
            else oTable.fnFilter('wstrzymany', 4);
        }
    });
}

function initialTableCandidate() {
    var oTable = $('.table.candidate').dataTable({
        "bPaginate": false,
        "sPaginationType": "full_numbers",
        "bFilter": false,
        "aaSorting": [[0, "asc"]],
        "oLanguage": {
            "oPaginate": {
                "sFirst": "&laquo;",
                "sLast": "&raquo;",
                "sNext": "&rsaquo;",
                "sPrevious": "&lsaquo;"
            },
            "sSearch": '<div class="input-append search-entries-input">' +
            '<input type="text" placeholder="np. imię, nazwisko..." autocomplete="off">' +
            '<span class="btn"><i class="icon-search"></i></span>' +
            '</div>',
            "sEmptyTable": 'Brak danych w tabeli',
            "sZeroRecords": 'Brak danych do wyświetlenia',
            "sInfoEmpty": '',
            "sInfo": '',
            "sInfoFiltered": '',
            "sLengthMenu": ''
        }
    });

    $('.search-entries-input').parent().children('input[type=text]').remove();
}

function initialTableNoticeCandidate() {
    var oTable = $('.table.candidate-notice').dataTable({
        "aaSorting": [[2, "desc"]],
        "bPaginate": false,
        "aoColumns": [
            null,
            null,
            {"sType": "date-uk"},
            null
        ],
        "oLanguage": {
            "sSearch": '<div class="input-append search-entries-input">' +
            '<input type="text" placeholder="np. nazwa, stanowisko..." autocomplete="off">' +
            '<span class="btn"><i class="icon-search"></i></span>' +
            '</div>',
            "sEmptyTable": "Brak danych w tabeli",
            "sZeroRecords": "Brak danych do wyświetlenia",
            "sInfoEmpty": "",
            "sInfo": "",
            "sInfoFiltered": "",
            "sLengthMenu": 'Pokaż <select class="input-small" style="margin:0px;">' +
            '<option value="50">50</option>' +
            '<option value="100">100</option>' +
            '<option value="200">200</option>' +
            '<option value="-1">Wszystkie</option>' +
            '</select> pozycji'
        }
    });
    $('.search-entries-input').parent().children('input[type=text]').remove();
}

function initialTableCV() {
    var oTable = $('.table.cv').dataTable({
        "aaSorting": [[1, "desc"]],
        "sPaginationType": "full_numbers",
        "aoColumns": [
            null,
            {"sType": "date-uk"},
            null,
            null
        ],
        "bFilter": false,
        "oLanguage": {
            "oPaginate": {
                "sFirst": "&laquo;",
                "sLast": "&raquo;",
                "sNext": "&rsaquo;",
                "sPrevious": "&lsaquo;"
            },
            "sEmptyTable": "Brak danych w tabeli",
            "sZeroRecords": "Brak danych do wyświetlenia",
            "sInfoEmpty": "",
            "sInfo": "Pozycje od _START_ do _END_ (wszystkich _TOTAL_)",
            "sInfoFiltered": "",
            "sLengthMenu": 'Pokaż <select class="input-small" style="margin:0px;">' +
            '<option value="50">50</option>' +
            '<option value="100">100</option>' +
            '<option value="200">200</option>' +
            '<option value="-1">Wszystkie</option>' +
            '</select> pozycji'
        }
    });
    $('.search-entries-input').parent().children('input[type=text]').remove();
}

function initialTableTag() {
    var oTable = $('.table.tag').dataTable({
        "sPaginationType": "full_numbers",
        "aoColumns": [
            null
        ],
        "bFilter": false,
        "oLanguage": {
            "oPaginate": {
                "sFirst": "&laquo;",
                "sLast": "&raquo;",
                "sNext": "&rsaquo;",
                "sPrevious": "&lsaquo;"
            },
            "sEmptyTable": "Brak danych w tabeli",
            "sZeroRecords": "Brak danych do wyświetlenia",
            "sInfoEmpty": "",
            "sInfo": "Pozycje od _START_ do _END_ (wszystkich _TOTAL_)",
            "sInfoFiltered": "",
            "sLengthMenu": 'Pokaż <select class="input-small" style="margin:0px;">' +
            '<option value="50">50</option>' +
            '<option value="100">100</option>' +
            '<option value="200">200</option>' +
            '<option value="-1">Wszystkie</option>' +
            '</select> pozycji'
        }
    });

    $('.search-entries-input').parent().children('input[type=text]').remove();
}

function initialTableFriends() {
    var oTable = $('.table.friends').dataTable({
        "sPaginationType": "full_numbers",
        "aoColumns": [
            null, null
        ],
        "bFilter": false,
        "oLanguage": {
            "oPaginate": {
                "sFirst": "&laquo;",
                "sLast": "&raquo;",
                "sNext": "&rsaquo;",
                "sPrevious": "&lsaquo;"
            },
            "sEmptyTable": "Brak danych w tabeli",
            "sZeroRecords": "Brak danych do wyświetlenia",
            "sInfoEmpty": "",
            "sInfo": "Pozycje od _START_ do _END_ (wszystkich _TOTAL_)",
            "sInfoFiltered": "",
            "sLengthMenu": 'Pokaż <select class="input-small" style="margin:0px;">' +
            '<option value="50">50</option>' +
            '<option value="100">100</option>' +
            '<option value="200">200</option>' +
            '<option value="-1">Wszystkie</option>' +
            '</select> pozycji'
        }
    });

    $('.search-entries-input').parent().children('input[type=text]').remove();
}
function initialTableRabat() {
    var oTable = $('.table.rabat').dataTable({
        "aaSorting": [[0, "desc"]],
        "sPaginationType": "full_numbers",
        "aoColumns": [
            null,
            null,
            null,
            null,
            null
        ],
        "bFilter": false,
        "oLanguage": {
            "oPaginate": {
                "sFirst": "&laquo;",
                "sLast": "&raquo;",
                "sNext": "&rsaquo;",
                "sPrevious": "&lsaquo;"
            },
            "sEmptyTable": "Brak danych w tabeli",
            "sZeroRecords": "Brak danych do wyświetlenia",
            "sInfoEmpty": "",
            "sInfo": "Pozycje od _START_ do _END_ (wszystkich _TOTAL_)",
            "sInfoFiltered": "",
            "sLengthMenu": 'Pokaż <select class="input-small" style="margin:0px;">' +
            '<option value="50">50</option>' +
            '<option value="100">100</option>' +
            '<option value="200">200</option>' +
            '<option value="-1">Wszystkie</option>' +
            '</select> pozycji'
        }
    });

    $('.search-entries-input').parent().children('input[type=text]').remove();
}
$(document).ready(function (e) {
    // Data table sorting search run
    initialTable();
    initialTableCandidate();
    initialTableNoticeCandidate();
    initialTableCV();
    initialTableTag();
    initialTableRabat();
    initialTableFriends();

});