$(document).ready(function (e) {
    $('.chosen-select').chosen({no_results_text: "Oops, nie znaleziono!"}).change(function () {
        updateUserSummary();
    });

    $('.chosen-select-one').chosen({no_results_text: "Oops, nie znaleziono!", max_selected_options: 1});

    $('.text-input.input-char-num').each(function (index, element) {
        var length = parseInt($(element).find('input').attr('maxlength'));
        $(element).find('.char-num').children('.value').text(length);
    });

    $('.text-input.input-char-num').keyup(function () {
        var length = parseInt($(this).find('input').attr('maxlength'));
        var inputLength = $(this).find('input').val().length;
        $(this).find('.char-num').children('.value').text((length - inputLength));
    });

    $('.data-input').find('input').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'yy-mm-dd',
        showOptions: {direction: "down"},
        minDate: 0,
        firstDay: 1,
        dayNamesMin: ["Ni", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
        monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"]
    });

    $('.data-input-2').find('input').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        showOptions: {direction: "down"},
        firstDay: 1,
        dayNamesMin: ["Ni", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
        monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
        yearRange: "-100:-16",
    });
    $('.data-input-3').find('input').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        showOptions: {direction: "down"},
        firstDay: 1,
        dayNamesMin: ["Ni", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
        monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
        yearRange: "-50:+10",
    });

    // Select list - new
    if ($('.select-input').length > 0) {
        $('.select-input').click(function () {
            var element = $(this);
            if ($(element).is('.active')) {
                //$(element).removeClass('active');
                //$(element).find('ul').slideUp('fast');
            } else {
                if ($('.select-input.active').length > 0) {
                    $('.select-input.active').removeClass('active').find('ul').slideUp('fast', function () {
                        $(element).addClass('active');
                        $(element).find('ul').slideDown('fast');
                    });
                } else {
                    $(element).addClass('active');
                    $(element).find('ul').slideDown('fast');
                }
            }
        });
        $('.select-input').find('li').click(function () {
            var value = $(this).text();

            if ($(this).parent().parent().parent().parent().is('.searchMainBox')) {
                var id = $(this).attr('id');
                $(this).parent().parent().find('.select-value').text(value);
                $(this).parent().parent().find('input').attr('name', 'province[' + id + ']');
            } else {
                $(this).parent().parent().find('.select-value').text(value);
                $(this).parent().parent().find('input').val(value);
            }

            /*  Show stawka  */
            if ($(this).parent().parent().is('.stawka')) {
                if (value == 'stawka godzinowa') {
                    $('.feature-row.stawka').slideDown();
                } else {
                    $('.feature-row.stawka').slideUp();
                }
            }


            /*  Show stawka  */
            if ($(this).parent().parent().is('.stawka')) {
                if (value == 'stawka miesięczna') {
                    $('.feature-row.miesieczna').slideDown();
                } else {
                    $('.feature-row.miesieczna').slideUp();
                }
            }
        });

        $(document).click(function (e) {
            if (!$(e.target).is('.select-input, .select-label, .select-value, .icon-select-list')) {
                $('.select-input.active').find('ul').slideUp('fast');
                $('.select-input.active').removeClass('active');
            }
        });
    }

    $('.user-list').hide();
    $('.user-toggle li').click(function () {
        $('.user-list').hide();
        $('.' + $(this).data('target')).show();
    });

    var updateUserSummary = function() {
        var $summary = $('#users-summary');
        $summary.empty();

        var count = $('.select-users-oneclick').parent().find('select option:selected').length;
        $('<li>', {'text': count + ' ulubionych kandydat(ów)'}).appendTo($summary);

        var count = $('.select-users-tags').parent().find('select option:selected').length;
        $('<li>', {'text': count + ' rodzaj(ów) prac'}).appendTo($summary);

        var count = $('.select-users-groups').parent().find('select option:selected').length;
        $('<li>', {'text': count + '  grup(y) kandydatów'}).appendTo($summary);
    };

    if ($('.position-map').length > 0) {
        initializeGMap();
    }

    updateUserSummary();
});