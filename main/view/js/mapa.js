var poz = new google.maps.LatLng(52.406374, 16.925168100000064);
var wawa = new google.maps.LatLng(52.2296756, 21.012228700000037);

var lodz = new google.maps.LatLng(51.783333, 19.466667);
var katowice = new google.maps.LatLng(50.25, 19);
var krakow = new google.maps.LatLng(50.061389, 19.938333);
var szczecin = new google.maps.LatLng(53.438056, 14.542222);
var gdansk = new google.maps.LatLng(54.366667, 18.633333);
var wroclaw = new google.maps.LatLng(51.11, 17.022222);
var bydgoszcz = new google.maps.LatLng(53.116667, 18);

var defBounds = poz;
var jsonURL = '/upload/mapa.json';
var jsonData = null;
var markers = null;
var infowindow = null;
var filtr = [];
var markerCluster;

function smoothZoom (map, level, cnt, mode) {
	if(mode) {	
		if (cnt >= level) return;
		else {
			var z = google.maps.event.addListener(map, 'zoom_changed', function(event){
				google.maps.event.removeListener(z);
				smoothZoom(map, level, cnt + 1, true);
			});
			setTimeout(function(){map.setZoom(cnt)}, 80);
		}
	} else {
		if (cnt <= level) return;
		else {
			var z = google.maps.event.addListener(map, 'zoom_changed', function(event) {
				google.maps.event.removeListener(z);
				smoothZoom(map, level, cnt - 1, false);
			});
			setTimeout(function(){map.setZoom(cnt)}, 80);
		}
	}
}

function searchMarkers(val){
	infowindow.close();
	
	$('article.active').removeClass('active');
	
	$('.gmap').gmap('set', 'bounds', null);
	
	if(val.length>0){
		markerCluster.clearMarkers();
		
		$('.gmap').gmap('find', 'markers', {'property': 'tags', 'value': val, 'operator': 'OR'}, function(marker, found) {
			if (found) {
				$('.gmap').gmap('addBounds', marker.position);
				$('#'+marker.mid).css('display','block');
				markerCluster.addMarker(marker);
			}else{
				$('#'+marker.mid).css('display','none');
			}
			marker.setVisible(found); 
		});
	}else{
            
		$.each($('.gmap').gmap('get', 'markers'), function(i, marker) {
			markerCluster.addMarker(marker);
			$('.gmap').gmap('addBounds', marker.position);
			marker.setVisible(true); 
			$('#'+marker.mid).css('display','block');
		});
	}
}

function loadMarkers(city,destroy){
	$('.gmap').gmap('destroy');
	
	markers = null;

	var html = '';
	
	$.each(jsonData.offers, function(j, o) {
		if(o.cid==city){
			html +=	'<article id="'+o.id+'" onclick="openOfferInfoSide('+o.id+')">'+
						'<p><strong>'+o.title+'</strong></p>'+
						'<a href="'+o.url+'" target="_blank">Aplikuj o pracę</a>'+
						'<p class="pln">'+o.amount+' zł/h</p>'+
					'</article>';
					
			var content ='<div class="mcontent"><p><i class="icon'+o.cat[0]+'"></i> <strong>'+o.title+'</strong></p>'+
						'<p>'+o.description+'</p>'+
						'<p class="btns"><a href="http://jakdojade.pl/?tn='+o.title+'&tc='+o.lat+':'+o.lng+'&cid='+o.cid+'" target="_blank" class="blue">Jak dojadę?</a> <a href="'+o.url+'" target="_blank">Aplikuj o pracę</a></p></div>';
			
			$('.gmap').gmap('addMarker', {
				'mid': o.id,
				'name': o.title,
				'tags': o.tags,
				'position': new google.maps.LatLng(o.lat, o.lng),
				'bounds':true,
				'content': content,
				'cat': o.cat[0],
				'animation': google.maps.Animation.DROP
			}).click( function() {
				openOfferInfo($(this)[0].mid);
			});
		}
	});
	
	$('.scroll').html(html);
	
	markers = $('.gmap').gmap('get', 'markers');
	
	defBounds = $('.gmap').gmap('get', 'map').getCenter();
		
	markerCluster = new MarkerClusterer($('.gmap').gmap('get', 'map'), markers)
	
	$('.gmap').gmap('set', 'MarkerClusterer', markerCluster);
	
	google.maps.event.addListener(markerCluster, "click", function(c) {
	    var currentZoom = $('.gmap').gmap('get', 'map').getZoom();
	    if(currentZoom >= 18){
			smoothZoom($('.gmap').gmap('get', 'map'),18,$('.gmap').gmap('get', 'map').getZoom(),true);
	    	
			var myLatlng = new google.maps.LatLng(c.getCenter().lat(), c.getCenter().lng());
			var m = c.getMarkers();
			var n = "";
			  
			for (var i = 0; i < m.length; i++ ){
				n += '<li class="icon'+m[i].cat+'"><a href="#" onclick="openOfferInfo('+m[i].mid+'); return false;">'+m[i].name+'</a></li>';
			}
			
			openMyInfoWindow(myLatlng, m.length, n);
		}
	});
	
	google.maps.event.addListener(markerCluster, "clusteringend", function(c) {
		//console.log('Tworzenie Clustera'); 
   	});
}

function openOfferInfo(id) {
        $.each(markers,function(i,marker){
            if(id==marker.mid){
                showInfo(marker);
            }
        });
	
	var eq = 0;
	
	$('.scroll article').each(function(i, article) {
		if($(article).css('display')=='block'){
			eq = $(article).index();
			return false;
		}
	});
	
	$('.scroll').animate({
		scrollTop: ($('#'+id).offset().top - $('.scroll article:eq('+eq+')').offset().top)
	}, 500);
}

function openOfferInfoSide(id) {
        //console.log(id);
        $.each(markers,function(i,marker){
            if(id==marker.mid){
                showInfo(marker);
                smoothZoom($('.gmap').gmap('get', 'map'),18,$('.gmap').gmap('get', 'map').getZoom(),true);
            }
        });
}

function showInfo(mk) {
	infowindow.close();
	
	$('article.active').removeClass('active');
	$('#'+mk.mid).addClass('active');
	$('.gmap').gmap('get', 'map').panTo(mk.position);
	
	defBounds = mk.position;
	
	infowindow.setPosition(mk.position);
	infowindow.setContent(mk.content);
	infowindow.open($('.gmap').gmap('get', 'map'));
	
	showDetails(mk);
}

function showDetails(mk) {
        var oferta;
        $.each(jsonData.offers,function(i,offer){
            if(offer.id==mk.mid)oferta = offer;
        });
    
	var name = oferta.employer[0].name;
	var watchUrl = oferta.employer[0].watchUrl;
	var wwwUrl = oferta.employer[0].wwwUrl;
	var imgUrl = oferta.employer[0].imgUrl;
	var contact = oferta.employer[0].contact;
        
	$('.watch .img img').attr('src',imgUrl);
	$('.watch .motto').text(name);
	$('.watch a').attr('href',watchUrl);
	
	$('.address a').attr('href',wwwUrl);
	$('.address p').html(contact);
}

function openMyInfoWindow(ll, m_length, c_list) {
	$('article.active').removeClass('active');
	$('.gmap').gmap('closeInfoWindow');
	infowindow.setPosition(ll);
	infowindow.setContent('<div class="mscrollcontent"><strong>Ofert pracy '+m_length+':</strong><ul>'+c_list+'</ul></div>');
	infowindow.open($('.gmap').gmap('get', 'map'));
}

function changeViewPosition(position){
	$('.gmap').gmap('get','map').panTo(position);
	$('.gmap').gmap('get','map').panBy(0,0);
}

$(document).ready(function(e) {
	$.getJSON(jsonURL+'?t='+$.now(), function(data) {
		jsonData = data;
	}); 
	
	$('input[name=s]').keyup(function(){
		searchMarkers($(this).val());
	});
	
	$('.filter').find('li').click(function(){
		filtr = [];
		
		if($(this).is('.on')){
			$(this).removeClass('on');
		}else{
			$(this).addClass('on');
		}
		
		if($(this).index()>0){
			if($('.filter ul li.on').length == ($('.filter ul li').length-1)){
				if(!$('.filter ul li:eq(0)').is('.on')){
					$('.filter ul li:eq(0)').addClass('on');
				}else{
					$('.filter ul li:eq(0)').removeClass('on');
				}
			}else if($('.filter ul li.on').length == 0){
				$('.filter ul li:eq(0)').addClass('on');
			}else{
				$('.filter ul li:eq(0)').removeClass('on');
			}
		}else{
			if($('.filter ul li:eq(0)').is('.on')){
				$('.filter ul li').each(function(i, li) {
					$(li).addClass('on');
				});
			}else{
				$('.filter ul li').each(function(i, li) {
					$(li).removeClass('on');
				});
			}
		}
		
		$('.filter').find('li').each(function(i, li) {
			if($(li).is('.on')){
				filtr.push($(li).attr('data-value'));
			}
		});
		
		if(filtr[0]=='all')filtr = [];
		searchMarkers(filtr);
	});
});

window.onhashchange = function () { 
	if(window.location.hash.match('lodz')){
		defBounds = lodz;
		$('a[href=#poznan]').removeClass('active');
        $('a[href=#warszawa]').removeClass('active');
		$('a[href=#lodz]').addClass('active');
		
		$('.offers').find('a').attr('href','http://happinate.com/oferty-pracy/1/-łódzkie');
		
		loadMarkers(4000);
	} else if(window.location.hash.match('warszawa')){
		defBounds = wawa;
		$('a[href=#poznan]').removeClass('active');
        $('a[href=#lodz]').removeClass('active');
		$('a[href=#warszawa]').addClass('active');
		
		$('.offers').find('a').attr('href','http://happinate.com/oferty-pracy/1/-mazowieckie');
		
		loadMarkers(3000);
	} else {
		defBounds = poz;
		$('a[href=#warszawa]').removeClass('active');
        $('a[href=#lodz]').removeClass('active');
		$('a[href=#poznan]').addClass('active');
		
		$('.offers').find('a').attr('href','http://happinate.com/oferty-pracy/1/-wielkopolskie');
		
		loadMarkers(1000);
	}
	
	$('.filter').find('li').each(function(i, li) {
		$(li).removeClass('on');
	});
	$('.filter ul li:eq(0)').addClass('on');
}

window.onresize = function(){
	changeViewPosition(defBounds);
}

window.onload = function(){
	google.maps.visualRefresh = true;

	var styles = [{
		featureType: "poi",
		stylers: [
			{ visibility: "off" }
		]
	}];
	
	$('.gmap').gmap({
		'zoom': 12,
		styles: styles,
		draggable: true,
		scrollwheel: true,
		'center': defBounds.toString(),
		//minZoom: 12,
  		//maxZoom: 20,
  		panControl: false,
  		zoomControl: true,
  		/*zoomControlOptions: {
	    	style: google.maps.ZoomControlStyle.SMALL
	  	},*/
		mapTypeControl: false,
		scaleControl: false,
		overviewMapControl: false,
  		mapTypeId: google.maps.MapTypeId.ROADMAP
	}).bind('init', function(event, map) {
		google.maps.event.addListener($('.gmap').gmap('get','map'), 'zoom_changed', function() {
			//console.log('zoom changed');
		});
	});
		
	infowindow = new google.maps.InfoWindow({zIndex: 9999, maxWidth: 350, pixelOffset: {width:0, height:-40}});
	google.maps.event.addListener(infowindow, 'closeclick', function() {
		infowindow.close();
		$('article.active').removeClass('active');
	});
	
	if (window.location.hash=='#lodz') {
		$('a[href=#poznan]').removeClass('active');
        $('a[href=#warszawa]').removeClass('active');
        $('a[href=#lodz]').addClass('active');
		setTimeout(function(){
			loadMarkers(4000);
		},500);
    } else if(window.location.hash=='#warszawa'){
        $('a[href=#lodz]').removeClass('active');
        $('a[href=#poznan]').removeClass('active');
		$('a[href=#warszawa]').addClass('active');
		setTimeout(function(){
			loadMarkers(3000);
		},500);
	}else{
        $('a[href=#lodz]').removeClass('active');
        $('a[href=#warszawa]').removeClass('active');
		$('a[href=#poznan]').addClass('active');
		setTimeout(function(){
			loadMarkers(1000);
		},500);
	}
}