$(document).ready(function() {
	// Platnosci
	$('.preview-formula.mini').find('.input-checkbox').mouseup(function(){
		$('.preview-formula.happi').find('input').removeAttr('checked');
		$('.preview-formula.happi').find('.check-bg').removeClass('active');
	});
	$('.preview-formula.happi').find('.input-checkbox').mouseup(function(){
		$('.preview-formula.mini').find('input').removeAttr('checked');
		$('.preview-formula.mini').find('.check-bg').removeClass('active');
	});
	$('.preview-formula.rabat').find('.input-checkbox').mouseup(function(){
		if($('.preview-formula.rabat').find('.check-bg').is('.active')==false){
			$('.preview-formula.rabat').find('.preview-formula-description').slideDown('fast');
		}else{
			$('.preview-formula.rabat').find('.preview-formula-description').slideUp('fast');
		}
	});
	$('.preview-pay-choice').children('div').mouseup(function(){
		if($(this).is('.pay-choice-phone')){
			//var price = parseInt($('.your-pay').find('input:eq(1)').val());
			//var sms = parseFloat($('.your-pay').find('input:eq(0)').val());
			//var total = $('.your-pay').find('input:eq(2)').val((price+sms));
			
			$('.your-pay').find('h2').text('Koszt smsa:');
			
			//$('.your-pay').find('.price').text((price+sms)+' zł');
			$('.preview-summation').find('.your-sms').slideDown('fast');
		}else{
			//var price = $('.your-pay').find('input:eq(1)').val();
			//var total = $('.your-pay').find('input:eq(2)').val(price);
			
			$('.your-pay').find('h2').text('Do zapłaty:');
			
			//$('.your-pay').find('.price').text(price+' zł');
			$('.preview-summation').find('.your-sms').find('input').val($('.preview-summation').find('.your-sms').find('input').attr('placeholder'));
			$('.preview-summation').find('.your-sms').slideUp('fast');
		}
		
		$('.preview-pay-choice').children('div').each(function(index, element) {
			$(element).removeClass('active');
		});
		$(this).addClass('active');
	});
	
	// Checkbox
	$('.input-checkbox').children('input').each(function(index, element) {
        if($(element).attr('checked')=='checked'){
			$(element).parent().find('.check-bg').addClass('active');
		}
        });
	$('.input-checkbox').mouseup(function(){
		if($(this).children('input').attr('checked')=='checked'){
			$(this).find('.check-bg').removeClass('active');
		}else{
			$(this).find('.check-bg').addClass('active');
		}
	});
	$('.free-pay').mouseup(function(){
		if($(this).children('input').attr('checked')!='checked'){
			$('.no-pay').children('input').removeAttr('checked');
			$('.no-pay').find('.check-bg').removeClass('active');
		}
	});
	$('.no-pay').mouseup(function(){
		if($(this).children('input').attr('checked')!='checked'){
			// Komunikat
			$('#formModalLabel').text('Publikowanie bez podania stawki');
			$('#formModal').find('.modal-body').html('<p style="line-height:25px;">Ogłoszenia bez podawania stawki cieszą się dużo mniejszym zainteresowaniem kandydatów i spływa na nie o wiele mniej zgłoszeń.</p><br><p>Czy mimo to nie chcesz publikować stawki?</p>');
			$('#formModal').find('.modal-footer .btn:eq(0)').text('Publikuj bez podania stawki');
			$('#formModal').find('.modal-footer .btn:eq(1)').text('Wróć i podaj stawkę');
			
			$('#formModal').find('.modal-footer').find('.btn').bind('click',function(){
				if($(this).is('.btn-primary')){
					$('.no-pay').children('input').removeAttr('checked');
					$('.no-pay').find('.check-bg').removeClass('active');
				}
				$('#formModal').modal('hide');
			});
			$('#formModal').modal('show');
			
			$(this).find('.check-bg').addClass('active');
			$('.free-pay').children('input').removeAttr('checked');
			$('.free-pay').find('.check-bg').removeClass('active');
		}
	});
	$('.job-time').mouseup(function(){
		if($(this).children('input').attr('checked')!='checked'){
			// Komunikat
			$(this).find('.check-bg').addClass('active');
			$('.job-time-flexible').children('input').removeAttr('checked');
			$('.job-time-flexible').find('.check-bg').removeClass('active');
		}
	});
	$('.job-time-flexible').mouseup(function(){
		if($(this).children('input').attr('checked')!='checked'){
			// Komunikat
			$(this).find('.check-bg').addClass('active');
			$('.job-time').children('input').removeAttr('checked');
			$('.job-time').find('.check-bg').removeClass('active');
		}
	});
	
	if($('.job-place-direct').children('input').attr('checked')){
			$('.direct-place').css('display','block');
	}
	
	$('.job-place-direct').mouseup(function(){
		if($(this).children('input').attr('checked')!='checked'){
			$('.direct-place').fadeIn('fast');
			$('.area-place').fadeOut('fast');
			$(this).find('.check-bg').addClass('active');
			$('.job-place-area').children('input').removeAttr('checked');
			$('.job-place-area').find('.check-bg').removeClass('active');
			
			$('.job-place-telework').children('input').removeAttr('checked');
			$('.job-place-telework').find('.check-bg').removeClass('active');
			
			$('.job-place-without').children('input').removeAttr('checked');
			$('.job-place-without').find('.check-bg').removeClass('active');
		}else{
			$('.direct-place').fadeOut('fast');
		}
	});
	
	if($('.job-place-area').children('input').attr('checked')){
			$('.area-place').css('display','block');
	}
	
	$('.job-place-area').mouseup(function(){
		if($(this).children('input').attr('checked')!='checked'){
			$('.direct-place').fadeOut('fast');
			$('.area-place').fadeIn('fast');
			$(this).find('.check-bg').addClass('active');
			$('.job-place-direct').children('input').removeAttr('checked');
			$('.job-place-direct').find('.check-bg').removeClass('active');
			
			$('.job-place-telework').children('input').removeAttr('checked');
			$('.job-place-telework').find('.check-bg').removeClass('active');
			
			$('.job-place-without').children('input').removeAttr('checked');
			$('.job-place-without').find('.check-bg').removeClass('active');
		}else{
			$('.area-place').fadeOut('fast');
		}
	});
	
	$('.job-place-telework').mouseup(function(){
		if($(this).children('input').attr('checked')!='checked'){
			$('.direct-place').fadeOut('fast');
			$('.area-place').fadeOut('fast');
			$(this).find('.check-bg').addClass('active');
			$('.job-place-direct').children('input').removeAttr('checked');
			$('.job-place-direct').find('.check-bg').removeClass('active');
			
			$('.job-place-area').children('input').removeAttr('checked');
			$('.job-place-area').find('.check-bg').removeClass('active');
			
			$('.job-place-without').children('input').removeAttr('checked');
			$('.job-place-without').find('.check-bg').removeClass('active');
		}
	});
	
	$('.job-place-without').mouseup(function(){
		if($(this).children('input').attr('checked')!='checked'){
			$('.direct-place').fadeOut('fast');
			$('.area-place').fadeOut('fast');
			
			// Komunikat
			$('#formModalLabel').text('Publikowanie bez podania adresu');
			$('#formModal').find('.modal-body').html('<p style="line-height:25px;">Ogłoszenia bez podanego miejsca / obszaru pracy cieszą się dużo mniejszym zainteresowaniem kandydatów i spływa na nie o wiele mniej zgłoszeń.</p><br><p>Czy mimo to nie chcesz podać adresu?</p>');
			$('#formModal').find('.modal-footer .btn:eq(0)').text('Publikuj bez podania adresu');
			$('#formModal').find('.modal-footer .btn:eq(1)').text('Wróć i podaj adres');
			
			$('#formModal').find('.modal-footer').find('.btn').bind('click',function(){
				if($(this).is('.btn-primary')){
					$('.direct-place').fadeIn('fast');
					$('.area-place').fadeOut('fast');
					$('.job-place-direct').find('.check-bg').addClass('active');
					$('.job-place-direct').children('input').attr('checked','checked');
					
					$('.job-place-area').children('input').removeAttr('checked');
					$('.job-place-area').find('.check-bg').removeClass('active');
					
					$('.job-place-without').children('input').removeAttr('checked');
					$('.job-place-without').find('.check-bg').removeClass('active');
				}
				$('#formModal').modal('hide');
			});
			$('#formModal').modal('show');
			
			$(this).find('.check-bg').addClass('active');
			$('.job-place-direct').children('input').removeAttr('checked');
			$('.job-place-direct').find('.check-bg').removeClass('active');
			
			$('.job-place-area').children('input').removeAttr('checked');
			$('.job-place-area').find('.check-bg').removeClass('active');
			
			$('.job-place-telework').children('input').removeAttr('checked');
			$('.job-place-telework').find('.check-bg').removeClass('active');
		}
	});
		
	$('#formModal').on('hidden', function () {
		$('#formModalLabel').text('');
		$('#formModal').find('.modal-body').html('');
		$('#formModal').find('.modal-footer .btn:eq(0)').text('');
		$('#formModal').find('.modal-footer .btn:eq(1)').text('');
		$('#formModal').find('.modal-footer').find('.btn').unbind('');
	});

});