(function($){
	$.fn.inputEmpty = function(){
		return this.each(function(){
			var val = $(this).attr('placeholder');
			
			$(this).focus(function(){
				if($(this).val()==val)$(this).val('');
			}).blur(function(){
				if($(this).val()=='')$(this).val(val);
			});
		})
	}	
})(jQuery)