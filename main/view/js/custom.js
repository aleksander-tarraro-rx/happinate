$(document).ready(function() {
    
    var aTop = 220;
    if($(this).scrollTop()>=aTop){
        $('.uzupelnij-box').css('position', 'fixed');
    } else {
        $('.uzupelnij-box').css('position', 'relative');
    }
    
    window.onresize = arguments.callee;
    
    $overlayer = $('.overlayer');
    $popup = $('.overlayerPopup');
    
    $windowWidth = $(window).width();
    $windowHeight = $(window).height();
    
    $popupWidth = $popup.width();
    $popupHeight = $popup.height();
    
    $marginLeft = ($windowWidth - $popupWidth) / 2;
    $marginTop = ($windowHeight - $popupHeight) / 2;
    
    if($overlayer) {
        $overlayer.animate({ opacity: 0.5 });
        $popup.css({ top: $marginTop }).animate({ left: $marginLeft });
        
        $overlayer.click(function() {
            $overlayer.fadeOut();
            $popup.fadeOut();
            $.post("/moje-konto", { popup: true } );
        });
        
        $popup.find('.close').click(function() {
            $overlayer.fadeOut();
            $popup.fadeOut();
            $.post("/moje-konto", { popup: true } );
        });
    }
    
    // Filtry
    $('#tagi span').click(function() {
       if($('#tagiMore li').is(':hidden')) { 
           $('#tagiMore li').slideDown();
           $(this).find('i').attr('class', 'icon-arrow-up');
       } else {
           $('#tagiMore li').slideUp();
           $(this).find('i').attr('class', 'icon-arrow-down');
       } 
    });
    $('#wojewodztwo span').click(function() {
       if($('#wojewodztwoMore li').is(':hidden')) { 
           $('#wojewodztwoMore li').slideDown();
           $('#wojewodztwo').find('i').attr('class', 'icon-arrow-up');
       } else {
           $('#wojewodztwoMore li').slideUp();
           $('#wojewodztwo').find('i').attr('class', 'icon-arrow-down');
       } 
    });
    $('#branza span').click(function() {
       if($('#branzaMore li').is(':hidden')) { 
           $('#branzaMore li').slideDown();
           $(this).find('i').attr('class', 'icon-arrow-up');
       } else {
           $('#branzaMore li').slideUp();
           $(this).find('i').attr('class', 'icon-arrow-down');
       } 
    });
    $('#umowa span').click(function() {
       if($('#umowaMore li').is(':hidden')) { 
           $('#umowaMore li').slideDown();
           $(this).find('i').attr('class', 'icon-arrow-up');
       } else {
           $('#umowaMore li').slideUp();
           $(this).find('i').attr('class', 'icon-arrow-down');
       } 
    });
    $('#time span').click(function() {
       if($('#timeMore li').is(':hidden')) { 
           $('#timeMore li').slideDown();
           $(this).find('i').attr('class', 'icon-arrow-up');
       } else {
           $('#timeMore li').slideUp();
           $(this).find('i').attr('class', 'icon-arrow-down');
       } 
    });
    
    $('#oferty-pracy .one-notice').each(function() {
        $(this).find('.fav').click(function() {
            var $id = $(this).attr('id');
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $.post("/praca/ajax", { favJob: false, id: $id } );
            } else {
                $(this).addClass('active');
                $.post("/praca/ajax", { favJob: true, id: $id } );
            }
        });
    });
    
    $('.job-title .fav').click(function() {
        var $id = $(this).attr('id');

        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $.post("/praca/ajax", { favJob: false, id: $id } );
        } else {
            $(this).addClass('active');
            $.post("/praca/ajax", { favJob: true, id: $id } );
        }
    });
    
    $('.candidate-notice .fav').click(function() {
        var $id = $(this).attr('id');

        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $.post("/praca/ajax", { favJob: false, id: $id } );
        } else {
            $(this).addClass('active');
            $.post("/praca/ajax", { favJob: true, id: $id } );
        }
    });
    
    
    // Profil
    $('.profile .bigBox .more').click(function() {
        if($(this).parent().hasClass('show')) {
            $(this).parent().removeClass('show');
            $(this).parent().find('.text').css('height', '200px');
            $(this).html('czytaj więcej <i class="icon icon-white icon-chevron-down"></i>');
        } else {
            $(this).parent().find('.text').css('height', 'auto');
            $(this).parent().addClass('show');
            $(this).html('schowaj <i class="icon icon-white icon-chevron-up"></i>');
        }
    });
    
    $('.profile .notice-watch a').click(function() {
        var attr = $(this).attr('id');
        var res = attr.split("-"); 
        var id = res[1];
        var watchUser = res[0];
        
        if(watchUser=='watch') {
            $.post("/profil/ajax", { watchUser: 1, id: id } );
            $(this).attr('id', 'unwatch-'+id);
            $(this).html('<i class="icon-minus icon-white"></i> &nbsp; Usuń z obserwowanych');
        } else if(watchUser=='unwatch') {
            $.post("/profil/ajax", { watchUser: 0, id: id } );
            $(this).attr('id', 'watch-'+id);
            $(this).html('<i class="icon-ok-sign icon-white"></i> &nbsp; Obserwuj pracodawcę');
        } else {
            return false;
        }
    });

    
    $('.job-title .notice-watch a').click(function() {
        var attr = $(this).attr('id');
        var res = attr.split("-"); 
        var id = res[1];
        var watchUser = res[0];
        
        if(watchUser=='watch') {
            $.post("/profil/ajax", { watchUser: 1, id: id } );
            $(this).attr('id', 'unwatch-'+id);
            $(this).html('<i class="icon-minus icon-white"></i> &nbsp; Usuń z obserwowanych');
        } else if(watchUser=='unwatch') {
            $.post("/profil/ajax", { watchUser: 0, id: id } );
            $(this).attr('id', 'watch-'+id);
            $(this).html('<i class="icon-ok-sign icon-white"></i> &nbsp; Obserwuj pracodawcę');
        } else {
            return false;
        }
    });
    
    $('.oferty-pracy button').click(function() {
        if($(this).parent().hasClass('show')) {
            $(this).parent().removeClass('show');
            $(this).parent().find('.karusela-all').css('height', '220px');
            $(this).html('Zobacz wszystkie oferty <i class="icon icon-white icon-chevron-down"></i>');
        } else {
            $(this).parent().find('.karusela-all').css('height', 'auto');
            $(this).parent().addClass('show');
            $(this).html('Schowaj <i class="icon icon-white icon-chevron-up"></i>');
        }
    });
    
    // Profil kandydata
    $('.middle-column .box2').each(function() {
       var button = $(this).find('.title');
       var text = $(this).find('.text');
       var watch = $(this).find('.watch');
        
       button.click(function() {
           
            var buttonText = $(this).find('span:not(.badge)').html();
            var buttonData = $(this).find('span:not(.badge)').attr('data');

            if($(this).hasClass('url')) {
                var url = $(this).find('a').attr('href');
                window.location = url;
            } else {
                if(text.hasClass('active')) {
                    text.slideUp().removeClass('active');
                    button.find('span:not(.badge)').html(buttonData);
                    button.find('span:not(.badge)').attr('data', buttonText);
                    button.find('i').removeClass('icon-chevron-up').addClass('icon-chevron-down');
                } else {
                    text.slideDown().addClass('active');
                    button.find('span:not(.badge)').html(buttonData);
                    button.find('span:not(.badge)').attr('data', buttonText);
                    button.find('i').removeClass('icon-chevron-down').addClass('icon-chevron-up');
                }
            }
       });
       
       if(watch.length > 0) {
           watch.find('.usun-z-obserwowanych').each(function() {
                $(this).click(function() {
                    var id = $(this).attr('id');
                    $.post("/profil/ajax", { watchUser: 0, id: id } );
                    $(this).parent().parent().fadeOut();
                });
           });
       }

    });

    // Zmiana koloru
    $('.left-column .zmien-kolor').each(function() {
        
        var box = $(this).find('.box');
        var color = box.find('.color');
        var fcolor = box.find('.fcolor');
        var id = box.parent().parent().attr('id');

        $(this).hover(function() {
             box.show();
        }, function() {
             box.hide();
        });

        color.click(function() {
             c = $(this).css('background-color');
             box.parent().parent().css('background-color', c);
             $.post( "/moje-konto/edytuj", { changeCVBG: 1, id: id, color: c } );
        });
        
        fcolor.click(function() {
             c = $(this).css('background-color');
             box.parent().parent().css('color', c);
             $.post( "/moje-konto/edytuj", { changeCVColor: 1, id: id, color: c } );
        });
    });
    
    // Rozwinięcie
    $('.left-column .header').each(function() {
        
        var box = $(this).parent().find('.box-do-rozwijania');
        var kolory = $(this).find('.zmien-kolor');
        
        $(this).click(function() {
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).parent().find('.box-do-rozwijania').slideDown();
                $(this).parent().find('.zmien-kolor').show();
                $(this).parent().find('.rozwin').hide();
                $(this).parent().find('.rozwin').show();
                box.slideUp();
                kolory.hide();
            } else {
                $(this).addClass('active');
                $(this).parent().find('.box-do-rozwijania').slideUp();
                $(this).parent().find('.zmien-kolor').hide();
                $(this).parent().find('.rozwin').show();
                $(this).parent().find('.rozwin').hide();
                box.slideDown();
                kolory.show();
            }
        });

    });
     
    // Zwiń
    $('.left-column .zwin').each(function() {
        
        var box = $(this).parent().parent().find('.box-do-rozwijania');
        var rozwin = $(this).parent().parent().find('.header .rozwin');
        var kolory = $(this).parent().parent().find('.header .zmien-kolor');
        
        $(this).click(function() {
            box.slideUp();
            rozwin.show();
            kolory.hide();
        });

    });
       
    // Buttons w doświadczemiu
    $('.left-column .doswiadczenie .item').each(function() {
        
        var item = $(this);
        
        var buttons = $(this).find('.buttons');
        var edit = $(this).find('.edit');
        var anuluj = $(this).find('.anuluj');
        var usun = $(this).find('.usun');
        
        var edytuj = $(this).find('.edytuj-doswiadczenie');
        var info = $(this).find('.info');
        
        var checkbox = $(this).find('.driving-licence-check');
        
        $(this).hover(function() {
             buttons.stop().animate({ opacity : 1 });
        }, function() {
             buttons.stop().animate({ opacity : .3 });
        });
        
        // Edycja
        edit.click(function() {
            item.parent().find('.item .edytuj-doswiadczenie').slideUp();
            item.parent().find('.dodaj-doswiadczenie').slideUp();
            item.parent().find('.dodaj').slideDown();
            item.parent().find('.item .info').slideDown();
            edytuj.slideDown();
            info.slideUp();
        });   
        
        // Anuluj
        anuluj.click(function() {
            edytuj.slideUp();
            info.slideDown();
        });
        
        // Usuń
        usun.click(function() {
            var id = usun.attr('id');
            var usunModal = $('#usun');
            var array = id.split('-');
            
            if(array[0] && array[1]) {
                if(array[0]==1) {
                    var label = 'Czy na pewno chcesz usunąć te doświadczenie?';
                    var url = '/moje-konto/edytuj/usun-doswiadczenie,'+array[1];
                } else if (array[0]==2) {
                    var label = 'Czy na pewno chcesz usunąć te wykształcenie?';
                    var url = '/moje-konto/edytuj/usun-wyksztalcenie,'+array[1];
                } else if (array[0]==3) {
                    var label = 'Czy na pewno chcesz usunąć ten język?';
                    var url = '/moje-konto/edytuj/usun-jezyk,'+array[1];
                } else if (array[0]==4) {
                    var label = 'Czy na pewno chcesz usunąć te szkolenie?';
                    var url = '/moje-konto/edytuj/usun-szkolenie,'+array[1];
                } else if (array[0]==5) {
                    var label = 'Czy na pewno chcesz usunąć tę umiejętność?';
                    var url = '/moje-konto/edytuj/usun-umiejetnosc,'+array[1];
                } else {
                    return false;
                }
                usunModal.find('h3').html(label);
                usunModal.find('a').attr('href', url);
            } else {
                return false;
            }
        });
        
        
        // checkbox
        $('.uprawnienia .driving-licence-check').click(function() {
            if($(this).hasClass('active')) {
                $(this).find('input').attr("checked", "checked");
            } else {
                $(this).find('input').removeAttr("checked");
            }
        });

    });
    
    // Dodawanie doświadczenia
    $('.doswiadczenie .dodaj').click(function() {
        $(this).stop().slideUp();
        $(this).parent().find('.dodaj-doswiadczenie').slideDown();
        $(this).parent().find('.item .edytuj-doswiadczenie').slideUp();
        $(this).parent().find('.item .info').slideDown();
    });
    // Dodawanie doświadczenia
    $('.dodaj-doswiadczenie .anuluj').click(function() {
        $(this).parent().parent().slideUp();
        $(this).parent().parent().parent().find('.dodaj').slideDown();
    });
    
    // Work now AJAX
    var worknow = $('.work-now').find('input[name=worknow]');
    var sms = $('.work-now').find('input[name=sms]');
    
    worknow.click(function() {
       if(worknow.is(':checked')) {
           var val = 1;
       } else {
           var val = 0;
       }
       $.post("/moje-konto", { worknow: val } );
    });
    
    sms.click(function() {
       if(sms.is(':checked')) {
           var val = 1;
       } else {
           var val = 0;
       }
       $.post("/moje-konto", { sms: val } );
    });
});


$(window).scroll(function(){
    var aTop = 220;
    if($(this).scrollTop()>=aTop){
        $('.uzupelnij-box').css('position', 'fixed');
    } else {
        $('.uzupelnij-box').css('position', 'relative');
    }
});
  
function getCV(_this) {
    var radio = _this.parent().parent().find('#radioButtons input[type="radio"]:checked').val();

    if($.isNumeric(radio)) {
        $('#cv_id').val(radio);
    }
};

function calculateMonthRate(t) {
    var payMonth = parseInt($(t).val());
    if(payMonth>0) {
        var payHour = (payMonth/168).toFixed(2);
        $(t).parent().parent().parent().find('#monthRate').html(payHour);
    } else {
        $(t).parent().parent().parent().find('#monthRate').html('0.00');
    }
}