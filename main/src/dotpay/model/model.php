<?php
namespace src\dotpay\model;

class dotpayModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }

    public function changeStatus($tid, $control) {
        $sql = "SELECT job_id, job_main_page FROM ".DB_PREF."job WHERE job_control = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($control));
        $r = $res->fetch();
        
        if($r['job_main_page']==2) {
            $this->workNow($r['job_id']);
        }
        
        $sql2 = "UPDATE ".DB_PREF."job SET job_status = 1, job_dotpay_tid = ?, job_control = NULL WHERE job_control = ? AND job_status = 0";
        $res2 = $this->database->prepare($sql2);

        if($res2->execute(array($tid, $control))) {
            return true;
        } else {
            return false;
        }       
    }
    
    public function resumeStatus($tid, $control) {
        $sql = "UPDATE ".DB_PREF."job SET job_status = 1, job_dotpay_tid = ?, job_control = NULL WHERE job_control = ? AND job_status = 3";
        $res = $this->database->prepare($sql);

        if($res->execute(array($tid, $control))) {
            return true;
        } else {
            return false;
        }       
    }
    
    public function delete($control) {
        $sql = "DELETE FROM ".DB_PREF."job job_control = ? AND job_status = 0";
        $res = $this->database->prepare($sql);

        if($res->execute(array($control))) {
            return true;
        } else {
            return false;
        }       
    }
    
    private function workNow($jobId) {
        $sql = "SELECT tag_id FROM ".DB_PREF."tag_job WHERE job_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($jobId ));
        $r = $res->fetchAll();
        
        $sql2 = "SELECT DISTINCT AES_DECRYPT(u.user_email, '".SALT."') as user_email FROM ".DB_PREF."user as u LEFT JOIN ".DB_PREF."user_tag as t ON u.user_id = t.tag_user_id WHERE u.user_worknow = 1 AND (";
        
        $execArray = array();
        $i = 1;
        $count = count($r);
        foreach($r as $tag) {
            $sql2 .= "t.tag_id = ?";
            if($count!=$i) {
                $sql2 .= " OR ";
            } else {
                $sql2 .= ")";
            }
            $i++;
            $execArray[] = $tag['tag_id'];
        }
        
        $res2 = $this->database->prepare($sql2);
        $res2->execute($execArray);
        $r2 = $res2->fetchAll();            

        foreach ($r2 as $user) {
            $msg = '<a href="http://'.$_SERVER['SERVER_NAME'].HOST.'praca/'.$jobId.'">Link do oferty</a>';
            $title = 'Tytuł';
            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($user['user_email'], $title, $msg);
        }
    }
}