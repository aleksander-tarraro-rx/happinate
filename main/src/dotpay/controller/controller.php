<?php

namespace src\dotpay;

use src\BaseController;

class dotpayController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* Index action */
    public function indexAction() {
        if(isset($_POST['t_status']) && $_POST['t_status']==2) {
            if($this->model->changeStatus($_POST['t_id'], $_POST['control'])) {
                echo "OK";
            }
        } elseif(isset($_POST['t_status']) && $_POST['t_status']==3) {
            if($this->model->delete($_POST['control'])) {
                echo "NOT OK";
            }           
        } else {
            header("Location:".HOST);
        }
    }
    
    public function resumeAction() {
        if(isset($_POST['t_status']) && $_POST['t_status']==2) {
            if($this->model->resumeStatus($_POST['t_id'], $_POST['control'])) {
                echo "OK";
            }       
        } else {
            header("Location:".HOST);
        }       
    }

}