<?php

namespace src\mojeKonto\model;

use app\helpers\Form;
use \PDO;

class mojeKontoModel {
    
    private $database;
    private $error;

    private $monthsNames = array(
        1 => "Styczeń",
        2 => "Luty",
        3 => "Marzec",
        4 => "Kwiecień",
        5 => "Maj",
        6 => "Czerwiec",
        7 => "Lipiec",
        8 => "Sierpień",
        9 => "Wrzesień",
        10 => "Październik",
        11 => "Listopad",
        12 => "Grudzień",
    );

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
    
    public function getEmployeerProfile() {
        $sql = "SELECT 
            user_profile_name, 
            user_logo,
            AES_DECRYPT(user_email, '".SALT."') as user_email, 
            AES_DECRYPT(user_nip, '".SALT."') as user_nip,
            AES_DECRYPT(user_regon, '".SALT."') as user_regon,
            AES_DECRYPT(user_pesel, '".SALT."') as user_pesel,
            AES_DECRYPT(user_phone, '".SALT."') as user_phone,
            user_adress_city,
            AES_DECRYPT(user_adress_zip_code, '".SALT."') as user_adress_zip_code,
            AES_DECRYPT(user_adress_street, '".SALT."') as user_adress_street,
            user_www,
            user_fanpage,
            user_profile_url,
            user_profile_img,
            user_info,
            user_profile_img_info,
            user_profile_yt_movie
            FROM ".DB_PREF."user ";
        if(isset($_SESSION['userToken'])) {
            $sql .= "WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
        } else {
            $sql .= "WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
        }
        $res = $this->database->prepare($sql);
        
        if(isset($_SESSION['userToken'])) {
            $res->execute(array($_SESSION['userToken']));
        } else {
            $res->execute(array($_SESSION['userTokenFB']));
        }

        if($res->rowCount()) {
            return $res->fetch();
        } else {
            return true;
        }       
    }
    
    public function getEmployeeProfile($id = false) {
        if(!$id) $id = $this->getUserId();
        
        $sql = "SELECT 
            u.user_id,
            u.user_logo,
            AES_DECRYPT(u.user_email, '".SALT."') as user_email, 
            AES_DECRYPT(u.user_name, '".SALT."') as user_name,
            AES_DECRYPT(u.user_surname, '".SALT."') as user_surname,
            AES_DECRYPT(u.user_pesel, '".SALT."') as user_pesel,
            AES_DECRYPT(u.user_phone, '".SALT."') as user_phone,
            u.user_adress_city,
            u.user_date_of_birth,
            AES_DECRYPT(u.user_adress_zip_code, '".SALT."') as user_adress_zip_code,
            AES_DECRYPT(u.user_adress_street, '".SALT."') as user_adress_street,
            u.user_student,
            u.user_uncapable,
            u.user_worknow,
            u.user_sms,
            u.user_main_tag_id,
            u.user_hobby,
            s.*
            FROM ".DB_PREF."user as u LEFT JOIN ".DB_PREF."user_other_skill as s ON s.skill_user_id = u.user_id  WHERE user_id = ?";

        $res = $this->database->prepare($sql);
        
        $res->execute(array($id));

        if($res->rowCount()) {
            $array = array();
            $array = $res->fetch();
            $array['skills'] = $this->getProfileSkills();
            $array['experiences'] = $this->getProfileExperience();
            $array['education'] = $this->getProfileEducation();
            return $array;
        } else {
            return true;
        }       
    }
    
    public function getProfileSkills() {
        $sql = "SELECT * FROM ".DB_PREF."user_skill WHERE skill_user_id = ?";
              
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }    
    
    public function getProfileExperience() {
        $sql = "SELECT * FROM ".DB_PREF."user_experience WHERE experience_user_id = ?";
              
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }
      
    public function getProfileEducation() {
        $sql = "SELECT * FROM ".DB_PREF."user_education WHERE education_user_id = ?";
              
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }
    
    public function checkProfileNameExists() {
        return true;
    }
    
    public function checkEmailNotExist($email) {
        $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."') AND user_id != ?;";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($email, $this->getUserId()));
        $res = $pre->fetch();

        if($pre->rowCount()!=0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
     
    public function updateTags($tags, $uid) {
        $sql = "DELETE FROM ".DB_PREF."user_tag WHERE tag_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($uid));
        
        foreach($tags as $tag) {
            $sql = "INSERT INTO ".DB_PREF."user_tag (tag_user_id, tag_id) VALUES (?, ?)";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($uid, $tag)); 
        }
 
        return true;
    }
       
    public function updateEmployeer($post, $file = 0) {
        $delFile = false;
        
        $uid = $this->getUserId();
        
        if($file>0) {
            $pathParts = pathinfo($file["name"]);
            $extension = $pathParts['extension'];

            $fileName = sha1($uid).'.'.$extension;
            $fileUploadPath = __DIR__ . '/../../../upload/userLogo/';

            if(!move_uploaded_file($file['tmp_name'], $fileUploadPath . $fileName)) {
                $fileName = '';
            } else {

                require __DIR__ . '/../../../app/libs/imageresizer/SimpleImage.php';
                $SimpleImage = new \SimpleImage();
                $SimpleImage->load($fileUploadPath . $fileName); 
                $SimpleImage->resizeToWidth(300); 
                $SimpleImage->save($fileUploadPath . $fileName);

            }
        } elseif($file == -1) {
            $delFile = true;
            $fileUploadPath = __DIR__ . '/../../../upload/userLogo/';
            $photo = $this->getUserPhoto($uid);
            if(file_exists($fileUploadPath . $photo)) {
                unlink($fileUploadPath . $photo);
            }
            $fileName = '';
        }
        
        $sql = "UPDATE ".DB_PREF."user SET 
                user_profile_name = ?, 
                user_email = AES_ENCRYPT(?, '".SALT."'), 
                user_adress_city = ?, 
                user_adress_zip_code = AES_ENCRYPT(?, '".SALT."'), 
                user_adress_street = AES_ENCRYPT(?, '".SALT."'), 
                user_nip = AES_ENCRYPT(?, '".SALT."'), 
                user_regon = AES_ENCRYPT(?, '".SALT."'), 
                user_phone = AES_ENCRYPT(?, '".SALT."'), 
                user_www = ?,
                user_fanpage = ?";
        if($delFile) $sql .= ", user_logo = NULL";
        if($file) $sql .= ", user_logo = '".$fileName."'";
        $sql .= " WHERE user_id = ?";
        
        #die($sql);
        
        $pre = $this->database->prepare($sql);

        if($pre->execute(array($post['profile_name'], $post['profile_email'], $post['profile_city'], $post['profile_zip_code'], $post['profile_street'], $post['profile_nip'], $post['profile_regon'], $post['profile_phone'], str_replace(array('https://','http://'),array('',''),$post['www']), str_replace(array('https://','http://'),array('',''),$post['fanpage']), $uid))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function updateEmployee($post, $file = false) {
        $uid = $this->getUserId();
        $delFile = false;
        
        if($file>0) {
            $pathParts = pathinfo($file["name"]);
            $extension = $pathParts['extension'];

            $fileName = sha1($uid).'.'.$extension;
            $fileUploadPath = __DIR__ . '/../../../upload/userLogo/';

            if(!move_uploaded_file($_FILES['photo']['tmp_name'], $fileUploadPath . $fileName)) {
                $fileName = '';
            } else {

                require __DIR__ . '/../../../app/libs/imageresizer/SimpleImage.php';
                $SimpleImage = new \SimpleImage();
                $SimpleImage->load($fileUploadPath . $fileName); 
                $SimpleImage->resizeToWidth(200); 
                $SimpleImage->resizeToHeight(200);
                $SimpleImage->save($fileUploadPath . $fileName);

            }
        } elseif($file == -1) {
            $delFile = true;
            $fileUploadPath = __DIR__ . '/../../../upload/userLogo/';
            $photo = $this->getUserPhoto($uid);
            if(file_exists($fileUploadPath . $photo)) {
                unlink($fileUploadPath . $photo);
            }
        }

        $sql = "UPDATE ".DB_PREF."user SET 
                user_name = AES_ENCRYPT(?, '".SALT."'), 
                user_surname = AES_ENCRYPT(?, '".SALT."'), 
                user_adress_city = ?,
                user_adress_street = AES_ENCRYPT(?, '".SALT."'),
                user_adress_zip_code = AES_ENCRYPT(?, '".SALT."'),
                user_phone = AES_ENCRYPT(?, '".SALT."'),
                user_worknow = ?,
                user_sms = ?,
                user_date_of_birth = ?";

        if(isset($fileName) && $fileName) {
           $sql .= ", user_logo = ? "; 
        } elseif($delFile) {
            $sql .= ", user_logo = '' "; 
        }

        $sql .= "WHERE user_id = ?";

        $pre = $this->database->prepare($sql);

        $array = array(
            $post['name'],
            $post['surname'],
            $post['city'],
            $post['street'],
            $post['postal'],
            $post['phone'],
            $post['worknow'],
            $post['sms'],
            $post['birth']
        );
        
        if(isset($fileName) && $fileName) {
           $array[] = $fileName; 
        }

        $array[] = $uid;

        if($pre->execute($array)) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
        
    }
    
    public function updateSkills($uptArray = false, $insDrray = false, $delArray = false) {
        
        if($delArray) {
            $delArray = Form::sanitizeArray($delArray);
            $sql = "DELETE FROM ".DB_PREF."user_skill WHERE skill_id = ?";
            foreach($delArray as $r=>$key) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($r));
                }
            }
        }

        if($insDrray) {
            $insDrray = Form::sanitizeArray($insDrray);
            $sql = "INSERT INTO ".DB_PREF."user_skill (skill_user_id, skill_text) VALUES (?,?)";
            foreach($insDrray as $r) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($this->getUserId(), $r));
                }
            }
        }
        
        if($uptArray) {
            $uptArray = Form::sanitizeArray($uptArray);
            $sql = "UPDATE ".DB_PREF."user_skill SET skill_text = ? WHERE skill_id = ?";
            foreach($uptArray as $r=>$key) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($key, $r));
                }
            }
        }
        
    } 
       
    
    public function deleteAccount() {
        $userId = $this->getUserId();
        
        $sql = "DELETE FROM ".DB_PREF."user WHERE user_id = ?";
        
        $pre = $this->database->prepare($sql);
        echo $userId;
        
        if($pre->execute(array($userId))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }    
    }
    
    public function getContractType($name = false) {
        if($name) {
            $sql = "SELECT type_id FROM ".DB_PREF."job_contract_types WHERE type_name = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($name));
            if($pre->rowCount()!=0) {
                $n = $pre->fetch();
                return $n['type_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }            
        } else {
            $sql = "SELECT * FROM ".DB_PREF."job_contract_types";
            $pre = $this->database->prepare($sql);
            if($pre->execute()) {
                return $pre->fetchAll();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }       
        }
    }    
    
    public function getTrades($name = false) {
        if($name) {
            $sql = "SELECT trade_id FROM ".DB_PREF."job_trade_types WHERE trade_name = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($name));
            if($pre->rowCount()!=0) {
                $n = $pre->fetch();
                return $n['trade_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }            
        } else {
            $sql = "SELECT * FROM ".DB_PREF."job_trade_types";
            $pre = $this->database->prepare($sql);
            $pre->execute();
            if($pre->rowCount()!=0) {
                return $pre->fetchAll();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }     
        }
    }
    
    public function getUserId($email = false) {
        if($email) {
            $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."')";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($email));            
                
            if($pre->rowCount()!=0) {
                $id = $pre->fetch();
                return $id['user_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }              
        } else {
            if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
                $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($_SESSION['userToken']));
            } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) { 
                $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($_SESSION['userTokenFB']));            
            } else {
                return false;
            }
            if($pre->rowCount()!=0) {
                $id = $pre->fetch();
                return $id['user_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }      
        }
    } 
    
    public function getUserPhoto($id) {

        $sql = "SELECT user_logo FROM ".DB_PREF."user WHERE user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));         
        
        $r = $pre->fetch();

        if($r['user_logo']) {
            return $r['user_logo'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }      
    }    
    
    public function getUserInfo($id) {

        $sql = "SELECT AES_DECRYPT(user_name, '".SALT."') as name, AES_DECRYPT(user_surname, '".SALT."') as surname, AES_DECRYPT(user_email, '".SALT."') as email FROM ".DB_PREF."user WHERE user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));         

        if($r = $pre->fetch()) {
            return $r;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }      
    }
    
    public function createURLText($string) {
        
        $string = strtolower($string);
        $polskie = array(',', ' - ',' ','ę', 'Ę', 'ó', 'Ó', 'Ą', 'ą', 'Ś', 's', 'ł', 'Ł', 'ż', 'Ż', 'Ź', 'ź', 'ć', 'Ć', 'ń', 'Ń','-',"'","/","?", '"', ":", 'ś', '!','.', '&', '&amp;', '#', ';', '[',']','domena.pl', '(', ')', '`', '%', '”', '„', '…');
        $miedzyn = array('-','-','-','e', 'e', 'o', 'o', 'a', 'a', 's', 's', 'l', 'l', 'z', 'z', 'z', 'z', 'c', 'c', 'n', 'n','-',"","","","","",'s','','', '', '', '', '', '', '', '', '', '', '', '', '');
        $string = str_replace($polskie, $miedzyn, $string);
        
        $string = preg_replace('/[^0-9a-z\-]+/', '', $string);
        $string = preg_replace('/[\-]+/', '-', $string);
        $string = trim($string, '-');

        $string = stripslashes($string);

        $string = urlencode($string);
        
        return $string;

    }

    public function getMyJobs() {
        $sql = "SELECT job_url_text, job_position, job_status, job_publish_date_end, job_id, job_views FROM ".DB_PREF."job WHERE job_status != 0 AND job_added_by = ? AND job_delete = 0;";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($this->getUserId()))) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }       
    }
    
    public function checkUserRightToEdit($id) {
        $sql = "SELECT count(job_id) as count FROM ".DB_PREF."job WHERE job_added_by = ? AND job_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId(), $id));
        $count = $pre->fetch();
        if($count['count']==1) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }
    
    public function pauseJob($id) {
        $sql = "UPDATE ".DB_PREF."job SET job_status = 2 WHERE job_id = ?";
        $pre = $this->database->prepare($sql);
        if($pre->execute(array($id))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }    
    
    public function resumeJob($id, $formula = 1, $control = false) {
        switch($formula) {
           case 1:
               
                $date = date("Y-m-d H:i:s");
                $date = strtotime($date);
                $date = strtotime("+14 day", $date);
                $endDate = date('Y-m-d H:i:s', $date);
                
                $sql = "UPDATE ".DB_PREF."job SET job_status = 1, job_publish_date_end = ?, job_created_date = NOW() WHERE job_id = ?";
                $pre = $this->database->prepare($sql);

                if($pre->execute(array($endDate, $id))) {
                    return true;
                } else {
                    $pdoError = $pre->errorInfo();
                    if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                    return false;
                } 
                
            break;
            case 2:

                 $date = date("Y-m-d H:i:s");
                 $date = strtotime($date);
                 $date = strtotime("+7 day", $date);
                 $endDate = date('Y-m-d H:i:s', $date);
                 
                 $sql = "UPDATE ".DB_PREF."job SET job_status = 0, job_main_page = 1, job_publish_date_end = ?, job_control = ?, job_created_date = NOW() WHERE job_id = ?";
                 $pre = $this->database->prepare($sql);

                 if($pre->execute(array($endDate, $control, $id))) {
                     return true;
                 } else {
                     $pdoError = $pre->errorInfo();
                     if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                     return false;
                 }

             break;
           case 3:

                 $date = date("Y-m-d H:i:s");
                 $date = strtotime($date);
                 $date = strtotime("+14 day", $date);
                 $endDate = date('Y-m-d H:i:s', $date);
                 
                 $sql = "UPDATE ".DB_PREF."job SET job_status = 0, job_main_page = 2, job_publish_date_end = ?, job_control = ?, job_created_date = NOW() WHERE job_id = ?";
                 $pre = $this->database->prepare($sql);

                 if($pre->execute(array($endDate, $control, $id))) {
                     return true;
                 } else {
                     $pdoError = $pre->errorInfo();
                     if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                     return false;
                 }
   
            break;
        }
       
    }
    
    public function deleteJob($id) {
        $sql = "UPDATE ".DB_PREF."job SET job_delete = 1 WHERE job_id = ?";
        $pre = $this->database->prepare($sql);
        if($pre->execute(array($id))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            print_r($pdoError);
            return false;
        }        
    }

    public function getJob($id) {
        $sql = "SELECT j.*, c.type_name as job_contract_type, t.trade_name as job_trade_type FROM ".DB_PREF."job as j LEFT JOIN ".DB_PREF."job_contract_types as c ON c.type_id = j.job_contract_type_id LEFT JOIN ".DB_PREF."job_trade_types as t ON t.trade_id = j.job_trade_type_id WHERE j.job_id = ? AND job_delete = 0 AND job_status>0 LIMIT 1";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        if($pre->rowCount()!=0) {
            $jobList = $pre->fetch();
            #$jobList['duties'] = $this->getDuties($jobList['job_id']);
            #$jobList['needs'] = $this->getNeeds($jobList['job_id']);
            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }
      
    public function getJobName($id) {
        $sql = "SELECT job_position FROM ".DB_PREF."job WHERE job_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        if($job = $pre->fetch()) {
            return $job['job_position'];
            #echo 'd';
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }
         
    public function getDuties($id) {
        $sql = "SELECT * FROM ".DB_PREF."job_duty WHERE duty_job_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }     
    }
         
    public function getNeeds($id) {
        $sql = "SELECT * FROM ".DB_PREF."job_need WHERE need_job_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }      
    }    
    
    public function updateJob($id, $post) {
        
        #echo "<pre>"; print_r($post); echo"</pre>"; die();
        
        $sql = "UPDATE ".DB_PREF."job SET job_additional_salary_info = ?, job_need = ?, job_duty = ?, job_offer = ?, job_contact_name = ?, job_contact_phone = ?, job_start_date = ?, job_work_hours_per_week = ?, job_work_shifts_per_day = ?, job_www = ?, job_fanpage = ? WHERE job_id = ? ";
        
        # execute array
        $executeArrayTmp = array(    
            $post['additional_salary_info'], 
            html_entity_decode ($post['need']), 
            html_entity_decode ($post['duty']), 
            html_entity_decode ($post['offer']), 
            $post['contact_name'], 
            $post['contact_phone'], 
            $post['start_date'], 
            $post['hours_per_week'], 
            $post['shifts'], 
            $post['www'], 
            $post['fanpage'], 
            $id
        );
               
        $executeArray = Form::sanitizeArray($executeArrayTmp);

        $pre = $this->database->prepare($sql);
        
        if($pre->execute($executeArray)) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    
    private function updateNeeds($id, $uptArray = false, $insDrray = false, $delArray = false) {
        
        if($delArray) {
            $delArray = Form::sanitizeArray($delArray);
            $sql = "DELETE FROM ".DB_PREF."job_need WHERE need_id = ?";
            foreach($delArray as $r=>$key) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($r));
                }
            }
        }

        if($insDrray) {
            $insDrray = Form::sanitizeArray($insDrray);
            $sql = "INSERT INTO ".DB_PREF."job_need (need_job_id, need_text) VALUES (?,?)";
            foreach($insDrray as $r) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($id, $r));
                }
            }
        }
        
        if($uptArray) {
            $uptArray = Form::sanitizeArray($uptArray);
            $sql = "UPDATE ".DB_PREF."job_need SET need_text = ? WHERE need_id = ?";
            foreach($uptArray as $r=>$key) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($key, $r));
                }
            }
        }
        
    }     
    
    private function updateDuties($id, $uptArray = false, $insDrray = false, $delArray = false) {
        
        if($delArray) {
            $delArray = Form::sanitizeArray($delArray);
            $sql = "DELETE FROM ".DB_PREF."job_duty WHERE duty_id = ?";
            foreach($delArray as $r=>$key) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($r));
                }
            }
        }

        if($insDrray) {
            $insDrray = Form::sanitizeArray($insDrray);
            $sql = "INSERT INTO ".DB_PREF."job_duty (duty_job_id, duty_text) VALUES (?,?)";
            foreach($insDrray as $r) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($id, $r));
                }
            }
        }
        
        if($uptArray) {
            $uptArray = Form::sanitizeArray($uptArray);
            $sql = "UPDATE ".DB_PREF."job_duty SET duty_text = ? WHERE duty_id = ?";
            foreach($uptArray as $r=>$key) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($key, $r));
                }
            }
        }
        
    } 

    public function getDotPayToken($formula, $max, $rabatText) {

        if($rabatText) {
            $rabat = $this->getRabatPercent($rabatText);
        } else {
            $rabat = 0;
        }
        
        // Formuła MINI 
        if($formula==1 && $max==0 && $rabat==0) {
            return 'ZV3NQR51647D8699Z6ZE6IWEG5RJRMT3';
        }
        
        // Formuła HAPPI 
        if($formula==2 && $max==0 && $rabat==0) {
            return '2Z8P8G8V6QY1GU7U1HWAIYWR6SUQT9S1';
        }
        
        // Formuła MINI + MAX 
        if($formula==1 && $max==1 && $rabat==0) {
            return 'JBLIMQ7RPWN9RSIJSRNQDI4H3WJM4IV9';
        } 
        
        // Formuła HAPPI + MAX 
        if($formula==2 && $max==1 && $rabat==0) {
            return 'WXX51G4U5WAU9U35T5WPMSJ1YTN65R2T';
        } 
          
        // Formuła MINI - 10%
        if($formula==1 && $max==0 && $rabat==10) {
            return '174FSI8VRD7VXGZEH9WW68UP8J425T7E';
        } 
          
        // Formuła MINI -25%
        if($formula==1 && $max==0 && $rabat==25) {
            return '2IYDZU3RA7CW7CQU57E62YYPBBHAGPRD';
        } 
          
        // Formuła MINI -50%
        if($formula==1 && $max==0 && $rabat==50) {
            return '2LTNWPY2JIPJ7E1C2A17V8GPA3UMSQL2';
        } 
         
        // Formuła HAPPI - 10%
        if($formula==2 && $max==0 && $rabat==10) {
            return 'A5GJ3F9ZYQX43U47FTQ9VVY7TT93WJI9';
        } 
          
        // Formuła HAPPI -25%
        if($formula==2 && $max==0 && $rabat==25) {
            return 'YUCW6UUW8MUBLRPBEPQI5U6KS2KAS2Y2';
        } 
          
        // Formuła HAPPI -50%
        if($formula==2 && $max==0 && $rabat==50) {
            return 'UI6H7N6PWM6QX64XDY52HD5IDI1K81H8';
        }  
        
        // Formuła MINI + MAX - 10%
        if($formula==1 && $max==1 && $rabat==10) {
            return 'V46ZA5CJ8G7LMSZ9V5VD4W9S2UL1P6AC';
        } 
          
        // Formuła MINI + MAX -25%
        if($formula==1 && $max==1 && $rabat==25) {
            return '	FDYSV2AJJ3325X66Y76Q4MJJ3HSYKTX9';
        } 
          
        // Formuła MINI + MAX -50%
        if($formula==1 && $max==1 && $rabat==50) {
            return '4M3RDJJCJ7HBYPSHT1LZSG5A5U8AWUSR';
        }      
        
        // Formuła HAPPI + MAX - 10%
        if($formula==2 && $max==1 && $rabat==10) {
            return '1FPWHY9F9319R6XZNA9A117TMA66JSMZ';
        } 
          
        // Formuła HAPPI + MAX -25%
        if($formula==2 && $max==1 && $rabat==25) {
            return 'JKGX7JP48B9ZYQT632ZY6UNFEMTPMW7G';
        } 
          
        // Formuła HAPPI + MAX -50%
        if($formula==2 && $max==1 && $rabat==50) {
            return '3I7192SHJ12ESVNG4JW4V479HB8WWBU6';
        }
        
        // Rabat 100%
        if($rabat==100) {
            return '7FS6F727ZAJYXEUMQGD326NDH4T4BUG8';
        }        
    }
    
    public function getUserJobList($employeer = false, $int = false) {
        if($employeer) {
            if($int) {
                $sql = "SELECT DISTINCT a.application_status, c.cv_src, c.cv_id, u.user_adress_city, u.user_id, a.application_date, j.job_added_by, a.application_status, j.job_position, j.job_id, j.job_url_text, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, AES_DECRYPT(u.user_phone, '".SALT."') as user_phone, AES_DECRYPT(u.user_adress_city, '".SALT."') as user_city, AES_DECRYPT(u.user_email, '".SALT."') as user_email FROM ".DB_PREF."application as a LEFT JOIN ".DB_PREF."job as j ON j.job_id = a.application_job_id LEFT JOIN ".DB_PREF."user as u ON u.user_id = a.application_user_id LEFT JOIN ".DB_PREF."user_cv as c ON c.cv_id = a.application_cv WHERE j.job_added_by = ? AND j.job_id = ? AND a.application_status < 3";
                $pre = $this->database->prepare($sql);

                $pre->execute(array($this->getUserId(), $int));            
            } else {
                $sql = "SELECT DISTINCT a.application_status, c.cv_src, c.cv_id, u.user_adress_city, u.user_id, j.job_added_by, a.application_date, a.application_status, j.job_position, j.job_id, j.job_url_text, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, AES_DECRYPT(u.user_phone, '".SALT."') as user_phone, AES_DECRYPT(u.user_adress_city, '".SALT."') as user_city, AES_DECRYPT(u.user_email, '".SALT."') as user_email FROM ".DB_PREF."application as a LEFT JOIN ".DB_PREF."job as j ON j.job_id = a.application_job_id LEFT JOIN ".DB_PREF."user as u ON u.user_id = a.application_user_id LEFT JOIN ".DB_PREF."user_cv as c ON c.cv_user_id = a.application_user_id WHERE j.job_added_by = ? AND j.job_publish_date_end >= NOW() AND u.user_active = 1 AND a.application_status < 3";
                $pre = $this->database->prepare($sql);

                $pre->execute(array($this->getUserId()));                  
            }
        } else {
            $sql = "SELECT a.application_date, a.application_status, j.job_position, j.job_id, j.job_employeer_name, j.job_url_text FROM ".DB_PREF."application as a LEFT JOIN ".DB_PREF."job as j ON j.job_id = a.application_job_id WHERE a.application_user_id = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($this->getUserId()));
        }
        if($pre->rowCount()!=0) {
            $jobList = $pre->fetchAll();
            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
   public function getOneClickUserJobList($int) {
        $sql = "SELECT DISTINCT u.user_adress_city, u.user_id, o.status, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, AES_DECRYPT(u.user_phone, '".SALT."') as user_phone, AES_DECRYPT(u.user_adress_city, '".SALT."') as user_city, AES_DECRYPT(u.user_email, '".SALT."') as user_email FROM ".DB_PREF."job_oneclick as o LEFT JOIN ".DB_PREF."user as u ON o.user_id = u.user_id WHERE o.job_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($int));    
        
        if($pre->rowCount()>0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function getAcceptedUserJobList($int ) {

        $sql = "SELECT c.cv_src, c.cv_id, u.user_id, a.application_date, j.job_added_by, a.application_status, j.job_position, j.job_id, j.job_url_text, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_email, '".SALT."') as user_email, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, AES_DECRYPT(u.user_phone, '".SALT."') as user_phone, AES_DECRYPT(u.user_adress_city, '".SALT."') as user_city FROM ".DB_PREF."application as a LEFT JOIN ".DB_PREF."job as j ON j.job_id = a.application_job_id LEFT JOIN ".DB_PREF."user as u ON u.user_id = a.application_user_id LEFT JOIN ".DB_PREF."user_cv as c ON c.cv_user_id = a.application_user_id WHERE j.job_added_by = ? AND j.job_id = ? AND a.application_status = 2 OR a.application_status = 3";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId(), $int));            

        if($pre->rowCount()!=0) {
            $jobList = $pre->fetchAll();
            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function countApplications($id) {
        $sql = "SELECT count(application_id) as count FROM ".DB_PREF."application WHERE application_job_id = ? AND application_status = 0";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $count = $pre->fetch();
        
        if($count['count']!=0) {
            return $count['count'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }        
    }
    
    public function countAcceptedApplications($id) {
        $sql = "SELECT count(application_id) as count FROM ".DB_PREF."application WHERE application_job_id = ? AND application_status = 2";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $count = $pre->fetch();
        
        if($count['count']!=0) {
            return $count['count'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }        
    }        
    public function countOneClick($id) {
        $sql = "SELECT count(id) as count FROM ".DB_PREF."job_oneclick WHERE job_id = ? AND status = 1";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $res = $pre->fetch();
        
        if($res['count']>0) {
            return $res['count'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }       
    }
    
    function getOneClickJobList($id = false) {
        if($id) {
            $listTmp = $this->getUserJobList(true, $id);
            $list = array();
            
            if($listTmp) {
                foreach($listTmp as $l) {
                    if($this->checkOneClick($l['user_id'], $l['job_added_by'])) {
                        $list[] = $l;
                    }
                }
                return $list;
            } else {
                return false;
            }
            
        } else {
            $sql = "SELECT o.oneclick_id, o.oneclick_last_job, u.user_id, j.job_position, c.cv_src, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_email, '".SALT."') as user_email, AES_DECRYPT(u.user_phone, '".SALT."') as user_phone, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname FROM ".DB_PREF."application_oneclick as o LEFT JOIN ".DB_PREF."user as u ON u.user_id = o.oneclick_employee_id LEFT JOIN ".DB_PREF."job as j ON j.job_id = o.oneclick_last_job LEFT JOIN ".DB_PREF."user_cv as c ON c.cv_id = o.oneclick_last_cv WHERE o.oneclick_employeer_id = ? AND oneclick_employee_id IS NOT NULL";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($this->getUserId()));  
            
            $res = $pre->fetchAll();
            
            if(count($res)>0) {
                return $res;
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            } 
        }
    }    
    
    public function checkOneClick($uid) {
        $sql = "SELECT oneclick_id FROM ".DB_PREF."application_oneclick WHERE oneclick_employeer_id = ? AND oneclick_employee_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId(), $uid));
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }
        
    public function getWaitingInvitation() {
        $sql = "SELECT oneclick_email FROM ".DB_PREF."application_oneclick WHERE oneclick_employeer_id = ? AND oneclick_token IS NOT NULL";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId()));

        if($pre->rowCount() > 0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }
    
    public function checkUserExists($id) {
        $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_id = ? AND user_type = 1";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }               
    }
        
    public function checkUserCV() {
        $sql = "SELECT count(cv_id) as count FROM ".DB_PREF."user_cv WHERE cv_user_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId()));
        $res = $pre->fetch();
        $count = $res['count'];

        if($count>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }               
    }
    
    public function addUserToOneClick($id, $token = false, $email = false, $job = null, $cv = null) {
        if($id) {
            
            $sql = "INSERT INTO ".DB_PREF."application_oneclick (oneclick_employeer_id, oneclick_employee_id, oneclick_last_job, oneclick_last_cv) VALUES (?,?,?,?)";

            $pre = $this->database->prepare($sql);

            if($pre->execute(array($this->getUserId(), $id, $job, $cv))) {
                return $this->database->lastInsertId();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;            
            }
        } else {
            $sql = "INSERT INTO ".DB_PREF."application_oneclick (oneclick_employeer_id, oneclick_token, oneclick_email) VALUES (?,?,?)";

            $pre = $this->database->prepare($sql);

            if($pre->execute(array($this->getUserId(), $token, $email))) {
                return $this->database->lastInsertId();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;            
            }    
        }
    }    
    
    public function removeUserToOneClick($id) {
        $sql = "DELETE FROM ".DB_PREF."application_oneclick WHERE oneclick_employeer_id = ? AND oneclick_employee_id = ?";

        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($this->getUserId(), $id))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            print_r($pdoError);
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;            
        }
      
    }
    
    public function getJobPosition($id) {
        $sql = "SELECT job_position, job_id, job_url_text FROM ".DB_PREF."job WHERE job_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }          
    }
      
    public function getUserEmail($id = false) {
        if($id) {
            $sql = "SELECT AES_DECRYPT(user_email, '".SALT."') as user_email FROM ".DB_PREF."user WHERE user_id = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($id));
            $res = $pre->fetch();   
        } else {
            $sql = "SELECT AES_DECRYPT(user_email, '".SALT."') as user_email FROM ".DB_PREF."user WHERE user_id = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($this->getUserId()));
            $res = $pre->fetch();
        }
        if(count($res)>0) {
            return $res['user_email'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }          
    }
    
    
    public function changeApplicationStatus($status, $uid, $jid, $note = false) {
        if($status) {
            if($note) {
                $sql = "UPDATE ".DB_PREF."application_note_user SET status = 2 WHERE user = ? AND note = ?";
            } else {
                $sql = "UPDATE ".DB_PREF."application SET application_status = 2, application_accepted_date = NOW() WHERE application_user_id = ? AND application_job_id = ?";
            }
        } else {
            if($note) {
                $sql = "UPDATE ".DB_PREF."application_note_user SET status = 1 WHERE user = ? AND note = ?";
            } else {
                $sql = "UPDATE ".DB_PREF."application SET application_status = 1, application_accepted_date = NOW() WHERE application_user_id = ? AND application_job_id = ?";
            }
        }
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($uid, $jid))) {
            $userEmail = $this->getUserEmail($uid);
            $job = $this->getJob($jid);
            
            if($status) {
                $body = '<h3>Kandydacie</h3><p>I-wszy etap selekcji przebiegł pomyślnie. Pracodawca rozważa Twoją kandydaturę na to stanowisko. Możliwe, że  zaprosi Ciebie na rozmowę kwalifikacyjną. </p>';
                $body .= '<p>Link do oferty: <a href="http://www.'.$_SERVER['SERVER_NAME'].HOST.'praca/'.$job['job_id'].'/'.$job['job_url_text'].'">'.$job['job_position'].'</a></p>';
                $title = 'happinate.com – pierwszy etap selekcji przebiegł pomyślnie';
            } else {
                $body = '<h3>Przykro nam</h3><p>Dziękujemy za złożoną aplikację na stanowisko '.$job['job_position'].' Selekcja została zakończona. Niestety tym razem, nie powiodło się!</p><p>Wejdź na happinate.com i sprawdź najświeższe oferty pracy </p>';
                $title = 'happinate.com - pierwszy etap selekcji został zakończony ';                
            }
            $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz</p>';

            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($userEmail, $title, $body);
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;            
        }
      
    }     
    
    public function oneClick($uid, $jid, $note = false, $friend = false) {
        if(!$note) {
            $sql = "UPDATE ".DB_PREF."application SET application_status = 2, application_accepted_date = NOW() WHERE application_user_id = ? AND application_job_id = ?";
            $pre = $this->database->prepare($sql);
        } else {
            $sql = "UPDATE ".DB_PREF."application_note_user SET status = 1 WHERE user = ? AND note = ?";
            $pre = $this->database->prepare($sql);
        }

        if($pre->execute(array($uid, $jid))) {
            
            $userEmail = $this->getUserEmail($uid);
            
            if($friend) {
                $body = '<h3>Kandydacie, gratulujemy!</h3><p>Kandydacie zostałeś zaakceptowany jako pracownik. Gratulujemy możesz stawić się w pracy. Pracuj i zarabiaj!</p>';
            } else {
                $body = '<h3>Kandydacie, gratulujemy!</h3><p>Kandydacie zostałeś przeniesiony z listy rezerwowych na listę główną. Gratulujemy możesz stawić się w pracy. Pracuj i zarabiaj!</p>';
            }
            $body .= '<p>Aby zobaczyć powiadomienie wejdź na swoje konto.</p>';
            $title = 'happinate.com - otrzymałeś pracę – zmiana statusu z listy rezerwowej na główną';

            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($userEmail, $title, $body);
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;            
        }  
    }    
    
    public function checkOneClickEmail($email) {
        $sql = "SELECT oneclick_id FROM ".DB_PREF."application_oneclick WHERE oneclick_employeer_id = ? AND oneclick_token = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId(), $email));
        
        $res = $pre->fetch();

        if($res['oneclick_id']) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function newOneClick($email, $msg) {

        
        if($this->checkEmailNotExist($email)) {
            $id = $this->getUserId($email);
            
            if(!$this->checkOneClick($id)) {
                if($this->addUserToOneClick($id)) {
                    return 1;
                } else {
                    return 0;
                }
            }
        } else { 
            $profil = $this->getEmployeerProfile();
            if(!$this->checkOneClickEmail($email)) {
                $token = sha1(time());

                $body = "<h3>Kandydacie!</h3><p>Nie wszyscy wygrywają rozmowy rekrutacyjne.</p>";
                $body.= "<p><a href='http://".$_SERVER['SERVER_NAME'].HOST."rejestracja/zaproszenie/".$token."'>Ty jesteś o krok dalej - pracodawca <strong>%s</strong> właśnie dodał Cię do bazy ulubionych pracowników.</a></p>";
                $body.= "<p><a href='http://".$_SERVER['SERVER_NAME'].HOST."rejestracja/zaproszenie/".$token."'>Gratulujemy - załóż konto na happinate.com i akceptuj oferty pracy wysyłane Ci przez pracodawców.</a></p>";
                $body.= "<p><a href='http://".$_SERVER['SERVER_NAME'].HOST."rejestracja/zaproszenie/".$token."'>Otwórz się na nowe możliwości, daj się polubić nowym pracodawcom.</a></p>";
                $body.= "<p><a href='http://".$_SERVER['SERVER_NAME'].HOST."rejestracja/zaproszenie/".$token."'>Dzięki happinate.com rozmowy rekrutacyjne to przeszłość.</a></p>";
                $body.= "<p><a href='http://".$_SERVER['SERVER_NAME'].HOST."rejestracja/zaproszenie/".$token."'>Z nami klikasz i zarabiasz!</a></p>";
                
                $body = sprintf($body, $profil['user_profile_name']);

                $title = sprintf("%s dodał Cię do ulubionych kandydatów.", $profil['user_profile_name']);

                $oid = $this->addUserToOneClick(false, $token, $email); 
                
                $_emailClass = $GLOBALS['_emailClass'];
                $_emailClass->addToList($email, $title, $body);
            
                return 2;
            } else {
                return 3;
            }
        }
    }
    
    public function countSendOneClick() {
        $sql = "SELECT * FROM ".DB_PREF."application_oneclick WHERE oneclick_employeer_id = ? AND oneclick_employee_id is NULL ";
              
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        if($pre->rowCount()!=0) {
            return $pre->rowCount();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        } 
    }
    
    public function getTags($admin = false) {
        if($admin) {
            $sql = "SELECT * FROM ".DB_PREF."tag WHERE tag_active = 0 ORDER BY tag_sort ASC";
        } else {
            $sql = "SELECT * FROM ".DB_PREF."tag WHERE tag_active = 1 ORDER BY tag_sort ASC";
        }
        $pre = $this->database->prepare($sql);
        $pre->execute();
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }         
    }
    
    public function getCities() {
        $sql = "SELECT * FROM ".DB_PREF."city";
        $pre = $this->database->prepare($sql);
        $pre->execute();
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }     
    }
    
        
    public function getCityId($name) {
        $sql = "SELECT city_id FROM ".DB_PREF."city WHERE city_text = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($name));
        
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['city_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }        
    
    public function getCityName($id) {
        $sql = "SELECT city_text FROM ".DB_PREF."city WHERE city_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['city_text'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
        
    public function getTagName($id) {
        $sql = "SELECT tag_name FROM ".DB_PREF."tag WHERE tag_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['tag_name'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function addTagsToUser($tag, $oneclickId) {
        $sql = "INSERT INTO ".DB_PREF."tag_oneclick (oneclick_id, tag_id) VALUES (?, ?)";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($oneclickId, $tag));

        if($pre->rowCount()!=0) {
            return true;
        } else {
            return false;
        }
    }
        
    public function addTagsToJob($tag, $jobId) {
        $tagID = $this->checkTag($tag);

        $sql = "INSERT INTO ".DB_PREF."tag_job (job_id, tag_id) VALUES (?, ?)";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($jobId, $tagID));

        if($pre->rowCount()!=0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function checkTag($tag) {
        $sql = "SELECT tag_id FROM ".DB_PREF."tag WHERE tag_name = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($tag));
        
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['tag_id'];
        } else {
            $pre->closeCursor();
            $sql = "INSERT INTO ".DB_PREF."tag (tag_name) VALUES (?)";
            $pre = $this->database->prepare($sql);

            $pre->execute(array(Form::sanitizeString($tag)));

            if($pre->rowCount()!=0) {
                return $this->database->lastInsertId();
            } else {
                return 0;
            }
        }
    }
    
    
    public function getUserTags($id = false) {
        
        if(!$id) $id = $this->getUserId();
        
        $sql = "SELECT tag_id FROM ".DB_PREF."user_tag WHERE tag_user_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        $res = $pre->fetchAll();

        if($pre->rowCount()!=0) {
            $array = array();
            foreach($res as $r) {
                $array[] = $r['tag_id'];
            }
            return $array;
        } else {
            return false;
        }
    }
        
    public function getUserOneclickTags($id = false) {
        
        if(!$id) $id = $this->getUserId();
        
        $sql = "SELECT t.tag_name FROM ".DB_PREF."tag_oneclick as o LEFT JOIN ".DB_PREF."tag as t ON o.tag_id = t.tag_id WHERE o.oneclick_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        $res = $pre->fetchAll();

        if($pre->rowCount()!=0) {
            $string = '';
            foreach($res as $r) {
                $string .= $r['tag_name']. ', ';
            }
            return $string;
        } else {
            return false;
        }
    }
        
    public function getUserOneclickNote($id = false) {

        $sql = "SELECT oneclick_note FROM ".DB_PREF."application_oneclick WHERE oneclick_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        $res = $pre->fetch();

        if($pre->rowCount()!=0) {
            return $res['oneclick_note'];
        } else {
            return false;
        }
    }

    /**
     * pobranie kalendarza użytkownika
     * @param  int $id id użytkownika
     * @return array dane z kalendarza
     */
    private function getUserCalendar()
    {   
        $_data = array();  
        $id = $this->getUserId();

        if(empty($id))
        {
            return $_data;
        }

        $sql = "SELECT * FROM ".DB_PREF."user_calendar WHERE user_id=?";
        $pre = $this->database->prepare($sql);        
        $pre->execute(array($id));
        $res = $pre->fetchAll();

        if(!empty($res))
        {
            foreach($res as $v)
            {
                $_temp = explode('-', $v['date']);
                $_data[$_temp[0]][$_temp[1]][$_temp[2]][] = $v;
            }
        }
        return $_data;
    }

    public function confimCalendar($_d=array())
    {
        // akceptacja
        $sql = "UPDATE ".DB_PREF."job_calendar_user SET confirmation='1' WHERE job_id=? AND user_id=? LIMIT 1";
        $pre = $this->database->prepare($sql);        
        $pre->execute(array($_d['jid'], $_d['uid']));       

        // aktualizacja kalendarza pracodawcy
        $id = $this->getUserId();
        $sql = "INSERT INTO happinate_user_calendar (user_id, date, time, information)VALUES(?,?,?,?)";
        $pre = $this->database->prepare($sql);        
        $pre->execute(array($id, $_d['date'], $_d['hour'], $_d['information']));
        
        // mail do uzytkownika 
        // mail do pracodawcy

    }

    public function getUsersCalendarByJid($jid)
    {
        $res = array();  
        if(empty($jid))
        {
            return $res;
        }

        $sql = "SELECT j.*,
                a.*,
                AES_DECRYPT(u.user_name, '".SALT."') as user_name, 
                AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, 
                AES_DECRYPT(u.user_phone, '".SALT."') as user_phone, 
                AES_DECRYPT(u.user_adress_city, '".SALT."') as user_city, 
                AES_DECRYPT(u.user_email, '".SALT."') as user_email         
                FROM ".DB_PREF."job_calendar_user AS j
                LEFT JOIN ".DB_PREF."user AS u ON u.user_id = j.user_id
                LEFT JOIN ".DB_PREF."job AS a ON a.job_id = j.job_id
                WHERE j.job_id=?";


        $pre = $this->database->prepare($sql);        
        $pre->execute(array($jid));
        $res = $pre->fetchAll();

        return $res;        
    }

    /**
     * pobieranie danych kolaendarza wgledem daty
     * @param  date $date data
     * @return array informacje z danego dnia
     */
    public function getUserCalendarByDate($date)
    {
        $_data = array();  
        $id = $this->getUserId();

        if(empty($id))
        {
            return $_data;
        }
        $sql = "SELECT * FROM ".DB_PREF."user_calendar WHERE user_id=? AND date=? ORDER BY time ASC";
        $pre = $this->database->prepare($sql);        
        $pre->execute(array($id, $date));
        $res = $pre->fetchAll();

        if(!empty($res))
        {
            foreach($res as $v)
            {
                $_data[$v['id']] = array('time' => $v['time'], 'information' => $v['information']);
            }
            
        }

        return $_data;        
    }    

    /**
     * sprawdz czy dany termin jest wolny
     * @param  string  $date
     * @param  string  $time 
     * @return boolean  true - wolny / false - zajety
     */
    public function isEmptyDateTime($date,$time)
    {
        $_data = array();  
        $id = $this->getUserId();

        if(empty($id))
        {
            return $_data;
        }
        $sql = "SELECT * FROM ".DB_PREF."user_calendar WHERE user_id=? AND date=? AND time=? ORDER BY time ASC";
        $pre = $this->database->prepare($sql);        
        $pre->execute(array($id, $date, $time));
        $res = $pre->fetchAll();

        if(!empty($res))
        {
            return false;
        }
        return true;        
    }

    /**
     * dodaj wpisa do kalendarza
     * @param array $data dane z formularza
     */
    public function addCalendarData($data)
    {
        if(empty($data['time']) OR empty($data['information']) OR empty($data['date']))
        {
            return "Podaj godzinę w formacie hh:mm:ss oraz treść wpisu.";
        }

        if(!$this->isTime($data['time']))
        {
            return "Godzina musi być w formacie hh:mm:ss";
        }

        if(!$this->isEmptyDateTime($data['date'], $data['time']))
        {
            return "Podany termin jest już zajęty, wybierz inny";
        }

        $id = $this->getUserId();

        $sql = "INSERT INTO happinate_user_calendar (user_id, date, time, information)VALUES(?,?,?,?)";
        $pre = $this->database->prepare($sql);        
        $pre->execute(array($id, $data['date'], $data['time'], $data['information']));
        //$res = $pre->fetchAll();

        return true;
    }

    /**
     * usuwanie wpisów
     * @param  int $id id wpisu
     * @return boolean true/false
     */
    public function delCalendarData($id)
    {
        if(empty($id))
        {
            return false;
        }

        $sql = "DELETE FROM happinate_user_calendar WHERE id=? LIMIT 1";
        $pre = $this->database->prepare($sql);        
        $pre->execute(array($id)); 
        return true;      
    }

    /**
     * funkcja sprawdza czy podany czas jest w odpowiednim formacie
     * @param  string  $time
     * @param  boolean $is24Hours format 24h/12h
     * @param  boolean $seconds w czasie podano sekundy
     * @return boolean true - ok / false - bledny
     */
    private function isTime($time, $is24Hours=true, $seconds=true)
    {
        $pattern = "/^".($is24Hours ? "([1-2][0-3]|[01]?[1-9])" : "(1[0-2]|0?[1-9])").":([0-5]?[0-9])".($seconds ? ":([0-5]?[0-9])" : "")."$/";
        if(preg_match($pattern, $time)) 
        {
            return true;
        }
        return false;
    }    

    /**
     * generuje tablice kalendarza z wpisami uzytkownika
     * @param  integer $month numer miesiaca (opcjonalnie)
     * @return array  tablica calendarza
     */
    public function generateSimpleCalendar($month=0, $year=0)
    {
        $userData = $this->getUserCalendar();

        $year = !empty($year) ? (int)$year : date("Y"); 
        $month = !empty($month) ? (int)$month : date("n");
        $daysOfMonth = date('t', mktime(0,0,0,$month,1,$year));
        $dayOfMonth = date('j');
        $dayOfWeek = date("w", mktime(0,0,0,$month,1,$year));
        $firstDayOfMonth = date("w", mktime(0,0,0,$month, 1, $year));

        if($dayOfWeek == 0) $dayOfWeek = 7;         // dzien tygodnia (dla niedzieli zwraca 0)
        if($firstDayOfMonth == 0) $firstDayOfMonth = 7; // 

        $_data = array();
        $j = $daysOfMonth + $firstDayOfMonth - 1;

        for($i = 0; $i < $j; $i++)
        {
            $label = $i - $firstDayOfMonth + 2;

            $k_month = str_pad($month, 2, "0", STR_PAD_LEFT);
            $k_label = str_pad($label, 2, "0", STR_PAD_LEFT);
            $info = array();
            if(!empty($userData[$year][$k_month][$k_label]))
            {
                $_temp = $userData[$year][$k_month][$k_label];
                foreach($_temp as $v)
                {
                    $info[$label][] = $v;  
                }
            }   

            $_data[ $this->monthsNames[$month] ][$i] = array(
                'label' => $label<1 ? '' : $label,
                'info' => !empty($info[$label]) ? $info[$label] : array(),
                'type' => '',
            );            
  
            if($i < $firstDayOfMonth - 1)                                   // pozostalosci z poprzedniego miesiaca
            {
                $_data[ $this->monthsNames[$month] ][$i]['type'] = 'empty-row';
            } 
            elseif(($i - $firstDayOfMonth + 2) == $dayOfMonth)              //dzisiaj
            {
                $_data[ $this->monthsNames[$month] ][$i]['type'] = !empty($info[$label]) ? 'full-row' : 'today-row';
            }
            elseif(($i - $firstDayOfMonth + 2) > $daysOfMonth)              // dni nastepnego miesiaca
            {
                $_data[ $this->monthsNames[$month] ][$i]['type'] = 'empty-row';            
            }
            else                                                           // reszta dni 
            {
                $_data[ $this->monthsNames[$month] ][$i]['type'] = !empty($info[$label]) ? 'full-row' : 'rest-row';           
            } 
         
        }   
        return $_data;
    }


   
    public function getJobTags($id) {
        $sql = "SELECT t.tag_name FROM ".DB_PREF."tag_job as j LEFT JOIN ".DB_PREF."tag as t ON j.tag_id = t.tag_id WHERE j.job_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        $res = $pre->fetchAll();

        if($pre->rowCount()!=0) {
            $string = '';
            $i = 0;
            foreach($res as $r) {
                if($i!=0) $string .= ', ';
                $string .= $r['tag_name'];
                $i++;
            }
            return $string;
        } else {
            return false;
        }
    }
    
    public function editOneclickTags($id, $tags) {
        $this->deleteOneclickTags($id);
        foreach ($tags as $tag) {
            $this->addTagsToUser($tag, $id);
        }
        return true;
    } 
    
    public function deleteOneclickTags($id) {
        $sql = "DELETE FROM ".DB_PREF."tag_oneclick WHERE oneclick_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
    }
    
    public function sendEmailToOneClick($jib) {
        $sql = "SELECT j.tag_id FROM ".DB_PREF."tag_job as j WHERE j.job_id = :jib";
        $pre = $this->database->prepare($sql);
        $pre->bindValue(':jib', $jib, \PDO::PARAM_INT);
        $pre->execute();
        
        $res = $pre->fetchAll();
        
        $array = array();
        $count = ceil(count($res)/2);
        
        foreach ($res as $r) {
            $sql = "SELECT AES_DECRYPT(u.user_email, '".SALT."') as email FROM ".DB_PREF."user as u LEFT JOIN ".DB_PREF."application_oneclick as o ON u.user_id = o.oneclick_employee_id LEFT JOIN ".DB_PREF."tag_oneclick as t ON o.oneclick_id = t.oneclick_id WHERE t.tag_id = ".$r['tag_id']." AND o.oneclick_employeer_id =".$this->getUserId();
            $pre = $this->database->prepare($sql);
            $pre->bindValue(":jib", $jib, \PDO::PARAM_INT);
            $pre->execute();
            
            $res2 = $pre->fetchAll();
            
            foreach($res2 as $r) {
                $array[] = $r['email'];
            }
        }
        
        $o = array_count_values($array);
        
        foreach($o as $email=>$k) {
            if($k>=$count) {
            
                $body = '<h3>Gratulujemy</h3><p>Zostałeś wybrany do opcji One Click!</p>';
                $body .= '<p>Link do ofery: <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'praca/'.$jib.'">KLIKNIJ</a></p>';
                $body .= '<p>Link do akceptacji: <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'/">ONECLICK!</a></p>';
                $title = 'happinate.com - opcja one click została udostępniona!';

                //Email::sendTo($email, $title, $body);
                $_emailClass = $GLOBALS['_emailClass'];
                $_emailClass->addToList($email, $title, $body);
            }
        }
    }
    
    public function getCVList() {
        $sql = "SELECT c.cv_id, c.cv_public, c.cv_date, c.cv_src, c.cv_name, a.application_job_id FROM ".DB_PREF."user_cv as c LEFT JOIN ".DB_PREF."application as a ON c.cv_application_id = a.application_id WHERE c.cv_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));

        $res = $pre->fetchAll();
        if($res) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }
    }
    
    
    public function checkCVExists($id) {
        $sql = "SELECT cv_id FROM ".DB_PREF."user_cv WHERE cv_id = ? AND cv_user_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id, $this->getUserId()));
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }               
    }    
    
    public function deleteCV($id) {
        $sql = "SELECT cv_src FROM ".DB_PREF."user_cv WHERE cv_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $res = $pre->fetch();

        $fileSrc = __DIR__ . "/../../../upload/userCV/".$res['cv_src'];
        
        if(file_exists($fileSrc)) {
            unlink($fileSrc);
        }

        $pre->closeCursor();
        
        $sql = "DELETE FROM ".DB_PREF."user_cv WHERE cv_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;  
        }
    }
    
    public function checkTagExists($id) {
        $sql = "SELECT tag_id FROM ".DB_PREF."tag WHERE tag_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }               
    }   
    
    public function addCV($cv) {
        
        $pathParts = pathinfo($cv['name']);
        $extension = $pathParts['extension'];

        $fileName = strtolower(str_replace('.'.$extension,'',$cv['name']));

        $cvName = sha1($fileName.time()).'.'.$extension;
        $fileUploadPath = __DIR__ . '/../../../upload/userCV/' . $cvName;

        if(!move_uploaded_file($cv['tmp_name'], $fileUploadPath)) {
            return false;
        }
        
        $sql = "INSERT INTO ".DB_PREF."user_cv (cv_user_id, cv_date, cv_src, cv_name, cv_public) VALUES (?, NOW(), ?, ?, 1);";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($this->getUserId(), $cvName, $pathParts['filename']))){
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function getStatistics() {
        $stats = array();
        
        // Users All
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_type = 1 OR user_type = 2");
        $pre->execute();
        $res = $pre->fetch();
        $stats['users']['all'] = $res['count'];
        $pre->closeCursor();
        
        // Users kandydaci
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_type = 1");
        $pre->execute();
        $res = $pre->fetch();
        $stats['users']['kandydaci'] = $res['count'];
        $pre->closeCursor();  
        
        // Users pracodawcy
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_type = 2");
        $pre->execute();
        $res = $pre->fetch();
        $stats['users']['pracodawcy'] = $res['count'];
        $pre->closeCursor();
                
        // Users active
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_active = 1 AND (user_type = 1 OR user_type = 2)");
        $pre->execute();
        $res = $pre->fetch();
        $stats['users']['aktywni'] = $res['count'];
        $stats['users']['nieaktywni'] = $stats['users']['all'] - $res['count'];
        $pre->closeCursor();
        
        // Users dzisiaj
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_register_date > ?");
        $pre->execute(array(date("Y-m-d").' 00:00:00'));
        $res = $pre->fetch();
        $stats['users']['dzisiaj'] = $res['count'];
        $pre->closeCursor();
                        
        // Jobs all
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."job WHERE job_publish_date_end < ? AND job_delete = 0");
        $pre->execute(array(date('Y-m-d H:i:s', strtotime("+30 days"))));
        $res = $pre->fetch();
        $stats['jobs']['all'] = $res['count'];
        $pre->closeCursor();  
        
        // Jobs aktywne
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."job WHERE job_publish_date > NOW() AND job_publish_date_end < NOW() AND job_delete = 0 AND job_status = 1");
        $pre->execute();
        $res = $pre->fetch();
        $stats['jobs']['aktywne'] = $res['count'];
        $pre->closeCursor();  
                        
        // Jobs aktywne
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."job WHERE job_publish_date > NOW() AND job_publish_date_end < NOW() AND job_delete = 0 AND job_dotpay_tid != ''");
        $pre->execute();
        $res = $pre->fetch();
        $stats['jobs']['zaplacone'] = $res['count'];
        $pre->closeCursor();  
                
        // Jobs today
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."job WHERE job_created_date > ?");
        $pre->execute(array(date("Y-m-d").' 00:00:00'));
        $res = $pre->fetch();
        $stats['jobs']['dzisiaj'] = $res['count'];
        $pre->closeCursor();  
                            
        // Application all
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."application");
        $pre->execute(array(date('Y-m-d H:i:s', strtotime("+30 days"))));
        $res = $pre->fetch();
        $stats['apps']['all'] = $res['count'];
        $pre->closeCursor();  
        
        // Application dzisiaj
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."application WHERE application_date > ?");
        $pre->execute(array(date("Y-m-d").' 00:00:00'));
        $res = $pre->fetch();
        $stats['apps']['dzisiaj'] = $res['count'];
        $pre->closeCursor();  
        
        // Oneclick all
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."application_oneclick");
        $pre->execute(array(date("Y-m-d").' 00:00:00'));
        $res = $pre->fetch();
        $stats['oneclick']['all'] = $res['count'];
        $pre->closeCursor();  
        
        // CV all
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user_cv");
        $pre->execute(array(date("Y-m-d").' 00:00:00'));
        $res = $pre->fetch();
        $stats['cv']['all'] = $res['count'];
        $pre->closeCursor();  
        
        // CV dzisiaj
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user_cv WHERE cv_date > ?");
        $pre->execute(array(date("Y-m-d").' 00:00:00'));
        $res = $pre->fetch();
        $stats['cv']['dzisiaj'] = $res['count'];
        $pre->closeCursor(); 
        
        // Tagi all
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."tag");
        $pre->execute();
        $res = $pre->fetch();
        $stats['tag']['all'] = $res['count'];
        $pre->closeCursor(); 
        
        // Tagi aktywne
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."tag WHERE tag_active = 1");
        $pre->execute();
        $res = $pre->fetch();
        $stats['tag']['aktywne'] = $res['count'];
        $stats['tag']['nieaktywne'] = $stats['tag']['all'] - $res['count'];
        $pre->closeCursor(); 

        return $stats;
    }
    
    public function tagAccept($id) {
        $sql = "UPDATE ".DB_PREF."tag SET tag_active = 1 WHERE tag_id = ?;";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id))){
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        } 
    }
    
    public function usunAccept($id) {
        $sql = "DELETE FROM ".DB_PREF."tag WHERE tag_id = ?;";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id))){
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        } 
    }
    
        
    public function getRabaty() {
        $sql = "SELECT rabat_id, rabat_date, rabat_name, rabat_percent, rabat_count, rabat_used, AES_DECRYPT(rabat_code, '".SALT."') as rabat_code FROM ".DB_PREF."job_rabat ORDER BY rabat_id ASC";
        $pre = $this->database->prepare($sql);

        $pre->execute();
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }       
    
    public function newRabat($name, $post) {
        $sql = "INSERT INTO ".DB_PREF."job_rabat (rabat_name, rabat_percent, rabat_code, rabat_count, rabat_date) VALUES (?, ?, AES_ENCRYPT(?, '".SALT."'), ?, NOW())";
        $pre = $this->database->prepare($sql);
        
        $code = $this->generateCode();
        
        $pre->execute(array($name, $post['procent'], $code, $post['count']));

        $id = $this->database->lastInsertId();
        if($id) {
            return '<p>'.$code.'</p>';
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function generateCode() {
        $znaki = '';
        $wynik = '';
        $znaki .= "ABCDEFGHIJKLMNPRSTUWZYXQ";
        $znaki .= "123456789";
        $dl = strlen($znaki) - 1;
        for($i =0; $i < 8; $i++)
        {
            $losuj = rand(0, $dl);
            $wynik .= $znaki[$losuj];
        }
        return $wynik; 
    }
    
    public function checkFriend($email) {
        
        $fid = $this->getUserId($email);
        $id = $this->getUserId();
        
        if($fid) {
            $sql = "SELECT id FROM ".DB_PREF."user_friend WHERE user_id = ? AND friend_id = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($id, $fid));
            $res = $pre->fetch();
        } else {
            return false;
        }
        
        if($pre->rowCount()>0){
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        } 
    }
    
    public function inviteFriend($email, $employeer = false) {

        if($employeer) {
                $body = '<h3>Pracodawco!</h3><p>Zapraszam Cię do skorzystania z serwisu happinate.com</p><ul><li>darmowe umieszczanie ofert pracy</li><li>unikatowe narzędzia ułatwiające proces rekrutacji kandydata</li><li>wizytówka firmy w google</li><ul>';
                $body .= '<p><a href="http://'.$_SERVER['SERVER_NAME'].HOST.'rejestracja">SPRAWDŹ</a></p>';
                $title = 'happinate.com - pracodawco';

                $_emailClass = $GLOBALS['_emailClass'];
                $_emailClass->addToList($email, $title, $body);
                return true;
        } else {
            if(!$this->checkEmailNotExist($email)) {
                $id = $this->getUserId();
                $token = sha1($this->generateCode());

                $sql = "INSERT INTO ".DB_PREF."user_friend (user_id, token, user_recommendation, email, accepted) VALUES (?, ?, 1, ?, 1)";
                $pre = $this->database->prepare($sql);
                $userInfo = $this->getUserInfo($id);
                
                if($pre->execute(array($id, $token, $email))){

                    // $body = '<h3>Zaproszenie</h3><p>'.$userInfo['name'].' '.$userInfo['surname'].' (<a href="mailto:'.$userInfo['email'].'">'.$userInfo['email'].'</a>) dodał Cię do listy znajomych na stronie happinate.com!</p><p>Aby przyjąć zaproszenie zarejestruj się używając linka poniżej.</p>';
//                     $body .= '<p><a href="http://'.$_SERVER['SERVER_NAME'].HOST.'rejestracja/znajomy/'.$token.'">ZAPROSZENIE</a></p>';

                    $body = "<p><a href='http://".$_SERVER['SERVER_NAME'].HOST.'rejestracja/znajomy/'.$token."'>".$userInfo['name']." ".$userInfo['surname']." właśnie wysłał(a) Ci zaproszenie do grona znajomych w serwisie największej bazy prac od zaraz www.happinate.com.</a></p>";
                    $body.= "<p><a href='http://".$_SERVER['SERVER_NAME'].HOST.'rejestracja/znajomy/'.$token."'>Zaloguj się już teraz lub załóż konto jeśli jeszcze go nie masz (zajmie Ci to 30 sekund)!</a></p>";
                    $body.= "<p><a href='http://".$_SERVER['SERVER_NAME'].HOST.'rejestracja/znajomy/'.$token."'>Szukajcie pracy razem i korzystajcie z możliwości jakie daje happinate.com</a></p>";

                    $title = $userInfo['name'] . ' '. $userInfo['surname'] . ' zaprosił(a) Cię do znajomych';

                    $_emailClass = $GLOBALS['_emailClass'];
                    $_emailClass->addToList($email, $title, $body);
                    return true;
                } else {
                    if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                    return false;
                }
            } else {
                $uid = $this->getUserId($email);
                $id = $this->getUserId();

                $sql = "INSERT INTO ".DB_PREF."user_friend (user_id, friend_id, accepted) VALUES (?, ?, 1);";
                $sql .= "INSERT INTO ".DB_PREF."user_friend (user_id, friend_id) VALUES (?, ?);";
                $pre = $this->database->prepare($sql);
                
                $userInfo = $this->getUserInfo($id);

                // $body = '<h3>Zaproszenie</h3><p>'.$userInfo['name'].' '.$userInfo['surname'].' (<a href="mailto:'.$userInfo['email'].'">'.$userInfo['email'].'</a>) dodał Cię do listy znajomych na stronie happinate.com!</p>';
                // $title = 'happinate.com - zaproszenie od znajomego';

                $body = "<p><a href='http://".$_SERVER['SERVER_NAME'].HOST.'rejestracja/znajomy/'.$token."'>".$userInfo['name']." ".$userInfo['surname']." właśnie wysłał(a) Ci zaproszenie do grona znajomych w serwisie największej bazy prac od zaraz www.happinate.com.</a></p>";
                $body.= "<p><a href='http://".$_SERVER['SERVER_NAME'].HOST.'rejestracja/znajomy/'.$token."'>Zaloguj się już teraz lub załóż konto jeśli jeszcze go nie masz (zajmie Ci to 30 sekund)!</a></p>";
                $body.= "<p><a href='http://".$_SERVER['SERVER_NAME'].HOST.'rejestracja/znajomy/'.$token."'>Szukajcie pracy razem i korzystajcie z możliwości jakie daje happinate.com</a></p>";

                $title = $userInfo['name'] . ' '. $userInfo['surname'] . ' zaprosił(a) Cię do znajomych';

                $_emailClass = $GLOBALS['_emailClass'];
                $_emailClass->addToList($email, $title, $body);

                if($pre->execute(array($id, $uid, $uid, $id))){
                    return true;
                } else {
                    $pdoError = $pre->errorInfo();
                    if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                    return false;
                } 
            }
        }
    }
    
    public function getFriendsList() {
        $sql = "SELECT f.id, u.user_id, AES_DECRYPT(u.user_email, '".SALT."') as user_email, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, f.accepted FROM ".DB_PREF."user_friend as f LEFT JOIN ".DB_PREF."user as u ON f.friend_id = u.user_id WHERE f.user_id = ? AND f.friend_id IS NOT NULL";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        $res = $pre->fetchAll();
        
        if($res){
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        } 
    }    
    
    public function checkIfFriendIsAccepted($fid) {
        $sql = "SELECT id FROM ".DB_PREF."user_friend WHERE user_id = ? AND friend_id = ? AND accepted = 1";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($fid, $this->getUserId()));
        
        if($pre->rowCount()>0){
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        } 
    }
    
    public function deleteFriend($id) {
        $sql = "DELETE FROM ".DB_PREF."user_friend WHERE (user_id = ? AND friend_id = ?) OR (user_id = ? AND friend_id = ?)";
        $pre = $this->database->prepare($sql);

        if($pre->execute(array($this->getUserId(), $id, $id, $this->getUserId(), ))){
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        } 
    }
        
    public function acceptFriend($id) {
        $sql = "UPDATE ".DB_PREF."user_friend SET accepted = 1 WHERE id = ?";
        $pre = $this->database->prepare($sql);

        if($pre->execute(array($id))){
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        } 
    }    

    public function getFav() {
        
        $id = $this->getUserId();
        
        if(!$id) return -1;
        
        $sql = "SELECT j.* FROM ".DB_PREF."job as j LEFT JOIN ".DB_PREF."user_fav_job as a ON a.fav_job_id = j.job_id WHERE a.fav_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetchAll();
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
    

    public function checkIfAddedToFav($job) {
        
        $id = $this->getUserId();
        
        if(!$id) return false;
        
        $sql = "SELECT fav_id FROM ".DB_PREF."user_fav_job WHERE fav_job_id = ? AND fav_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($job, $id));
        if($pre->rowCount()==0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
       
    public function checkCountCV() {
        
        $id = $this->getUserId();
        
        if(!$id) return false;
        
        $sql = "SELECT count(cv_id) as count FROM ".DB_PREF."user_cv WHERE cv_user_id = ? AND cv_public = 1";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        $res = $pre->fetch();

        if($res['count']<3) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function editNote($id, $note) {
        if($id && $note) {
            $sql = "UPDATE ".DB_PREF."application_oneclick SET oneclick_note = ? WHERE oneclick_id = ?";
            $pre = $this->database->prepare($sql);

            if($pre->execute(array($note, $id))) {
                return true;
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }  
        } else {
            return false;
        }
    }
    
    public function checkPassword($pass) {
        $sql = "SELECT count(user_id) as count FROM ".DB_PREF."user WHERE user_password = AES_ENCRYPT(md5(?),'".SALT."') AND user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($pass, $this->getUserId()));
        $res = $pre->fetch();
        
        if($res['count']>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
}
    }

    
    public function changePassword($pass) {
        $sql = "UPDATE ".DB_PREF."user SET user_password = AES_ENCRYPT(md5(?),'".SALT."') WHERE user_id = ?";
        $pre = $this->database->prepare($sql);

        if($pre->execute(array($pass, $this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function checkUniqueProfileUrl($url) {
        $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_id != ? AND user_profile_url = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId(), $url));

        if($pre->rowCount()==0) {
            return true;
        } else {
            return false;
        }  
    }
    
    public function updateEmployeerProfile($post, $file = false) {
        $userId = $this->getUserId();
        $delFile = false;
        if($file>0) {
            $pathParts = pathinfo($file["name"]);
            $info = getimagesize($file["tmp_name"]);
            
            #print_r($info);
            #die();
            $extension = $pathParts['extension'];

            $fileName = sha1($userId).'.'.$extension;
            $fileUploadPath = __DIR__ . '/../../../upload/userProfile/';

            if(!move_uploaded_file($file['tmp_name'], $fileUploadPath . $fileName)) {
                $fileName = '';
            } else {                    
                
                require __DIR__ . '/../../../app/libs/imageresizer/SimpleImage.php';
                $SimpleImage = new \SimpleImage();
                
                if($info[0]>570) {
                    $SimpleImage->load($fileUploadPath . $fileName); 
                    $SimpleImage->resizeToWidth(570); 
                    $SimpleImage->save($fileUploadPath . $fileName);
                }
                
                if($info[1]>400) {
                    $SimpleImage->load($fileUploadPath . $fileName); 
                    $SimpleImage->resizeToHeight(400); 
                    $SimpleImage->save($fileUploadPath . $fileName);
                }
            }
        } elseif($file == -1) {
            $delFile = true;
            $fileUploadPath = __DIR__ . '/../../../upload/userProfile/';
            if(file_exists($fileUploadPath . $post['profile_file'])) {
                unlink($fileUploadPath . $post['profile_file']);
            }
        }
        
        $sql = "UPDATE ".DB_PREF."user SET 
                user_profile_img_info = ?,
                user_profile_yt_movie = ?,
                user_info = ?";
        if($delFile) $sql .= ", user_profile_img = NULL";
        if($file) $sql .= ", user_profile_img = '".$fileName."'";
        $sql .= " WHERE user_id = ?";
        
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($post['profile_img_info'], $post['yt'], $post['info'], $userId))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function addExperience($post) {
        $sql = "INSERT INTO ".DB_PREF."user_experience (experience_user_id, experience_company, experience_start, experience_end, experience_till_now, experience_position, experience_opis) VALUES (?,?,?,?,?,?,?)";
        $pre = $this->database->prepare($sql);
        
        if($post['do']) {
            $do = $post['do'];
        } else {
            $do = null;
        }
        
        if($pre->execute(array($this->getUserId(),$post['firma'],$post['od'],$do,$post['teraz'],$post['stanowisko'],$post['opis']))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
        
    public function editExperience($post) {
        $sql = "UPDATE ".DB_PREF."user_experience SET experience_company = ?, experience_start = ?, experience_end = ?, experience_till_now = ?, experience_position = ?, experience_opis = ? WHERE experience_id = ? AND experience_user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($post['do']) {
            $do = $post['do'];
        } else {
            $do = null;
        }
        
        if($pre->execute(array($post['firma'],$post['od'],$do,$post['teraz'],$post['stanowisko'],$post['opis'],$post['id'],$this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
            
    public function deleteExperience($id) {
        $sql = "DELETE FROM ".DB_PREF."user_experience WHERE experience_id = ? AND experience_user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id,$this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
    
    public function getExperience($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_experience WHERE experience_user_id = ? ORDER BY experience_till_now DESC, experience_end DESC";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function addEducation($post) {
        $sql = "INSERT INTO ".DB_PREF."user_education (education_user_id, education_name, education_start, education_end, education_till_now, education_field) VALUES (?,?,?,?,?,?)";
        $pre = $this->database->prepare($sql);
        
        if($post['do']) {
            $do = $post['do'];
        } else {
            $do = null;
        }
        
        if($pre->execute(array($this->getUserId(),$post['szkola'],$post['od'],$do,$post['teraz'],$post['kierunek']))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
        
    public function editEducation($post) {
        $sql = "UPDATE ".DB_PREF."user_education SET education_name = ?, education_start = ?, education_end = ?, education_till_now = ?, education_field = ? WHERE education_id = ? AND education_user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($post['do']) {
            $do = $post['do'];
        } else {
            $do = null;
        }
        
        if($pre->execute(array($post['szkola'],$post['od'],$do,$post['teraz'],$post['kierunek'],$post['id'],$this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
            
    public function deleteEducation($id) {
        $sql = "DELETE FROM ".DB_PREF."user_education WHERE education_id = ? AND education_user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id,$this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
    
    public function getEducation($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_education WHERE education_user_id = ? ORDER BY education_till_now DESC, education_end DESC";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function addLang($post) {
        $sql = "INSERT INTO ".DB_PREF."user_lang (lang_user_id, lang_name, lang_level) VALUES (?,?,?)";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($this->getUserId(),$post['jezyk'],$post['poziom']))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
        
    public function editLang($post) {
        $sql = "UPDATE ".DB_PREF."user_lang SET lang_name = ?, lang_level = ? WHERE lang_id = ? AND lang_user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($post['jezyk'],$post['poziom'],$post['id'],$this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
            
    public function deleteLang($id) {
        $sql = "DELETE FROM ".DB_PREF."user_lang WHERE lang_id = ? AND lang_user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id,$this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
    
    public function getLang($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_lang WHERE lang_user_id = ? ORDER BY lang_name DESC";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }

    public function addTraining($post) {
        $sql = "INSERT INTO ".DB_PREF."user_training (training_user_id, training_end, training_name) VALUES (?,?,?)";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($this->getUserId(),$post['koniec'],$post['nazwa']))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
        
    public function editTraining($post) {
        $sql = "UPDATE ".DB_PREF."user_training SET training_end = ?, training_name = ? WHERE training_id = ? AND training_user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($post['koniec'],$post['nazwa'],$post['id'],$this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
            
    public function deleteTraining($id) {
        $sql = "DELETE FROM ".DB_PREF."user_training WHERE training_id = ? AND training_user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id,$this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
    
    public function getTraining($id = false) {
        $sql = "SELECT * FROM ".DB_PREF."user_training WHERE training_user_id = ? ORDER BY training_end DESC";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }

    public function addOtherSkill($post) {
        $sql = "INSERT INTO ".DB_PREF."user_skill (skill_user_id, skill_name, skill_opis) VALUES (?,?,?)";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($this->getUserId(),$post['nazwa'],$post['opis']))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
        
    public function editOtherSkill($post) {
        $sql = "UPDATE ".DB_PREF."user_skill SET skill_name = ?, skill_opis = ? WHERE skill_id = ? AND skill_user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($post['nazwa'],$post['opis'],$post['id'],$this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
            
    public function deleteOtherSkill($id) {
        $sql = "DELETE FROM ".DB_PREF."user_skill WHERE skill_id = ? AND skill_user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id,$this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
    
    public function getOtherSkill($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_skill WHERE skill_user_id = ? ORDER BY skill_name DESC";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function saveHobby($post) {
        $sql = "UPDATE ".DB_PREF."user SET user_hobby = ? WHERE user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($post['hobby'], $this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
         
    public function saveSkill($post) {
        $uid = $this->getUserId();

        if($this->checkSkill($uid)) {
            $sql = "UPDATE ".DB_PREF."user_other_skill SET skill_driving_a1 = ?, skill_driving_a = ?, skill_driving_b1 = ?, skill_driving_b = ?, skill_driving_c1 = ?, skill_driving_c = ?, skill_driving_d1 = ?, skill_driving_d = ?, skill_driving_be = ?, skill_driving_c1e = ?, skill_driving_ce = ?, skill_driving_d1e = ?, skill_driving_de = ?, skill_driving_t = ?, skill_welding_tig = ?, skill_welding_mig = ?, skill_welding_mag = ?, skill_sanel_owner = ?, skill_sanel_not_expired = ?, skill_electricity_to_1kv = ?, skill_electricity_more_1kv = ?, skill_forklift = ?, skill_forklift_big = ? WHERE skill_user_id = ?;";
        } else {
            $sql = "INSERT INTO ".DB_PREF."user_other_skill (skill_driving_a1, skill_driving_a, skill_driving_b1, skill_driving_b, skill_driving_c1, skill_driving_c, skill_driving_d1, skill_driving_d, skill_driving_be, skill_driving_c1e, skill_driving_ce, skill_driving_d1e, skill_driving_de, skill_driving_t, skill_welding_tig, skill_welding_mig, skill_welding_mag, skill_sanel_owner, skill_sanel_not_expired, skill_electricity_to_1kv, skill_electricity_more_1kv, skill_forklift, skill_forklift_big, skill_user_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        }
        
        $pre = $this->database->prepare($sql);

        $array = array();

        $i = 0;
        foreach($post as $p) {
            if($i>0) {
            $array[] = $p;
            }
            $i++;
        }

        $array[] = $uid;

        if($pre->execute($array)) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
          
    public function getSkill($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_other_skill WHERE skill_user_id";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function checkSkill($id) {
        $sql = "SELECT count(skill_user_id) as count FROM ".DB_PREF."user_other_skill WHERE skill_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        $r = $pre->fetch();
        if($r['count']>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
      
    public function saveTags($post) {
        $uid = $this->getUserId();

        $sql = "UPDATE ".DB_PREF."user SET user_main_tag_id = ? WHERE user_id = ?;";
        $pre = $this->database->prepare($sql);

        $array = array();
        $array = $post['main'];
        $array[] = $uid;
                    
        if($pre->execute($array)) {
            $this->updateTags($post['tags'], $uid);
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }   
    
          
    public function getWorkNowJobs() {
        $month = date("Y-m-d H:i:s");
        $sql = "SELECT AES_DECRYPT(u.user_email, '".SALT."') as user_email, u.user_profile_name, j.job_id, j.job_employeer_place_direct_city, j.job_position, j.job_url_text, j.job_added_by, j.job_created_date FROM ".DB_PREF."job as j LEFT JOIN ".DB_PREF."user as u ON j.job_added_by = u.user_id WHERE j.job_status = 1 AND j.job_publish_date < NOW() AND j.job_main_page = 2 AND j.job_publish_date_end > '".$month."' ORDER BY j.job_publish_date DESC LIMIT 0,3";
        $pre = $this->database->prepare($sql);

        $pre->execute();
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
                   
    public function getUserFav() {
        $sql = "SELECT DISTINCT AES_DECRYPT(u.user_email, '".SALT."') as user_email, u.user_profile_name, u.user_id FROM ".DB_PREF."application_oneclick as a LEFT JOIN ".DB_PREF."user as u ON a.oneclick_employeer_id = u.user_id WHERE a.oneclick_employee_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId()));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
                           
    public function getUserWatch() {
        $sql = "SELECT DISTINCT AES_DECRYPT(u.user_email, '".SALT."') as user_email, u.user_profile_name, u.user_id FROM ".DB_PREF."user_watch as a LEFT JOIN ".DB_PREF."user as u ON a.watch_id = u.user_id WHERE a.user_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId()));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
                          
    public function getUserFriend() {
        $sql = "SELECT DISTINCT AES_DECRYPT(u.user_email, '".SALT."') as user_email, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, user_logo, u.user_id FROM ".DB_PREF."user_friend as f LEFT JOIN ".DB_PREF."user as u ON f.friend_id = u.user_id WHERE f.user_id = ? AND f.friend_id IS NOT NULL AND f.accepted=1 ORDER BY RAND() LIMIT 0,6";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId()));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function checkIfUserAcceptedFriendInvitation($uid) {
        $sql = "SELECT * FROM ".DB_PREF."user_friend WHERE friend_id = ? AND user_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($this->getUserId(), $uid));
        
        $res = $pre->fetch();

        if(count($res)>0) {
            return $res['accepted'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
                               
    public function getUserCVColors() {
        $sql = "SELECT user_cv_color, user_bgcv_color FROM ".DB_PREF."user WHERE user_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId()));
        
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
                                         
    public function changeCvBg($id, $color) {
        
        $colors = $this->getUserCVColors();
        $bgTmp = $colors['user_bgcv_color'];
        $bg = explode(',', $bgTmp);
        
        $rgb = explode(',',trim(str_replace('rgb(', '', str_replace(')', '', $color))));
        $hex = $this->RGBToHex($rgb[0], $rgb[1], $rgb[2]);
        
        $bg[$id] = $hex;
        
        $str = '';
        $count = count($bg);
        $i = 1;
        
        foreach($bg as $b) {
            $str .= $b;
            if($count>$i) $str .= ',';
            $i++;
        }

        $sql = "UPDATE ".DB_PREF."user SET user_bgcv_color = ? WHERE user_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($str, $this->getUserId()));
        
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    public function changeCvColor($id, $color) {
        
        $colors = $this->getUserCVColors();
        $bgTmp = $colors['user_cv_color'];
        $bg = explode(',', $bgTmp);
        
        $rgb = explode(',',trim(str_replace('rgb(', '', str_replace(')', '', $color))));
        $hex = $this->RGBToHex($rgb[0], $rgb[1], $rgb[2]);
        
        $bg[$id] = $hex;
        
        $str = '';
        $count = count($bg);
        $i = 1;
        
        foreach($bg as $b) {
            $str .= $b;
            if($count>$i) $str .= ',';
            $i++;
        }

        $sql = "UPDATE ".DB_PREF."user SET user_cv_color = ? WHERE user_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($str, $this->getUserId()));
        
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function RGBToHex($r, $g, $b) {
        $hex = "#";
        $hex.= str_pad(dechex($r), 2, "0", STR_PAD_LEFT);
        $hex.= str_pad(dechex($g), 2, "0", STR_PAD_LEFT);
        $hex.= str_pad(dechex($b), 2, "0", STR_PAD_LEFT);

        return $hex;
    }
              
    public function userWorknow($v) {
        $sql = "UPDATE ".DB_PREF."user SET user_worknow = ? WHERE user_id = ?";
        $pre = $this->database->prepare($sql);

        if($pre->execute(array($v, $this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
                    
    public function userSms($v) {
        $sql = "UPDATE ".DB_PREF."user SET user_sms = ? WHERE user_id = ?";
        $pre = $this->database->prepare($sql);

        if($pre->execute(array($v, $this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
                          
    public function getVacancies($id, $note = false) {
        if($note) {
            $sql = "SELECT vacancies FROM ".DB_PREF."application_note WHERE id = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($id));
        } else {
            $sql = "SELECT job_vacancies FROM ".DB_PREF."job WHERE job_id = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($id));
        }
        if($pre->rowCount()>0) {
            $r = $pre->fetch();
            if($note) {
                return $r['vacancies'];
            } else {
                return $r['job_vacancies'];  
            }
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
                               
    public function getAcceptedOneClick($id, $note = false) {
        if($note) {
            $sql = "SELECT id FROM ".DB_PREF."application_note_user WHERE note = ? AND status = 1";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($id));
        } else {
            $sql = "SELECT job_id FROM ".DB_PREF."job_oneclick WHERE job_id = ? AND application_status = 1";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($id));
        }
        if($pre->rowCount()>0) {
            return $pre->rowCount();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }  
    }
                               
    public function getOneClick($limit = false) {
        $sql = "SELECT a.status, j.job_position, j.job_id, j.job_start_date FROM ".DB_PREF."job_oneclick as a LEFT JOIN ".DB_PREF."job as j ON a.job_id = j.job_id WHERE a.user_id = ?";
        
        if($limit) {
          $sql .= " AND a.status = 0 ";  
        }
        
        $sql .= " ORDER BY j.job_start_date DESC ";
        
        if($limit) {
          $sql .= " LIMIT 0,$limit";
        }
        
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        
        $array1 = $pre->fetchAll();
        $array2 = $this->getPowiadomienieOneclick(false, true, $limit);
                
        if($array1 && $array2) {
            $array = array_merge($array1, $array2);
        } elseif($array1) {
            $array = $array1;
        } elseif($array2) {
            $array = $array2;
        } else {
            $array = false;
        }
        
        #echo"<pre>";print_r($array);echo"</pre>";die();
        
        if($array) {
            usort($array, function($a, $b) {
                return $b['job_start_date'] - $a['job_start_date'];
            });
        }
        
        if($array) {
            return $array;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }      
    
    function aasort(&$array, $key) {
        $sorter=array();
        $ret=array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii]=$va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii]=$array[$ii];
        }
        $array=$ret;
    }


    public function oneClickFromReserve($jid, $uid) {
        $sql = "UPDATE ".DB_PREF."application SET application_status = 3, application_accepted_date = NOW() WHERE application_job_id = ? AND application_user_id = ?;";
        $sql .= "UPDATE ".DB_PREF."job_oneclick SET status = 1 WHERE job_id = ? AND user_id = ?;";
        $pre = $this->database->prepare($sql);

        $userEmail = $this->getUserEmail($uid);
        $userEEmail = $this->getUserEmail();
        $job = $this->getJob($jid);         

        $body = '<h3>Kandydacie, gratulujemy!</h3><p>Pracodawca przeniósł Cię z listy rezerwowych na główną listę pracowników!</p>';
        $body .= '<p>Oznacza to, że możesz rozpocząć pracę we wskazanym terminie. Jeśli jednak nie możesz podjąć pracy skontaktuj się pilnie z pracodawcą: <a href="mailto:'.$userEEmail.'">'.$userEEmail.'</a></p>';
        $body .= '<p>Link do oferty: <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'praca/'.$job['job_id'].'/'.$job['job_url'].'">'.$job['job_position'].'</a></p>';
        $title = 'happinate.com - 1-click pracodawca przeniósł Cię z listy rezerwowych na listę pracowników';

        $_emailClass = $GLOBALS['_emailClass'];
        $_emailClass->addToList($userEmail, $title, $body);
        #8die($userEmail);
        if($pre->execute(array($jid, $uid, $jid, $uid))) {

            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function oneClickOdrzuc($jid, $uid, $note = false) {
        if(!$note) {
            $sql = "UPDATE ".DB_PREF."application SET application_status = 4, application_accepted_date = NOW() WHERE application_job_id = ? AND application_user_id = ?;";
            $sql .= "UPDATE ".DB_PREF."job_oneclick SET status = 3 WHERE job_id = ? AND user_id = ?;";
            $pre = $this->database->prepare($sql);
            $array = array($jid, $uid, $jid, $uid);
        } else {
            $sql = "UPDATE ".DB_PREF."application_note_user SET status = 3 WHERE note = ? AND user = ?";
            $pre = $this->database->prepare($sql);
            $array = array($jid, $uid);
        }

        $userEmail = $this->getUserEmail($uid);
        $userEEmail = $this->getUserEmail();

        $body = '<h3>Kandydacie!</h3><p>Niestety pracodawca anulował ofertę pracy 1-click!</p>';
        $body .= '<p>W razie pytań skontaktuj się z pracodawcą: <a href="mailto:'.$userEEmail.'">'.$userEEmail.'</a></p>';
        $body .= '<p>Zobacz na liście swoich aplikacji OneClick.</p>';
        $body .= '<p>Wejdź na stronę happinate.com i szukaj innych ofert pracy</p>';
        $title = 'happinate.com - 1-click pracodawca przeniósł Cię z listy rezerwowych na listę pracowników';

        $_emailClass = $GLOBALS['_emailClass'];
        $_emailClass->addToList($userEmail, $title, $body);
        if($pre->execute($array)) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function getOneClickUsers() {
        $sql = "SELECT u.user_id, AES_DECRYPT(u.user_email, '".SALT."') as user_email, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname FROM ".DB_PREF."user as u LEFT JOIN ".DB_PREF."application_oneclick as a ON a.oneclick_employee_id = u.user_id WHERE a.oneclick_employeer_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId()));
        $res = $pre->fetchAll();
        
        if(!empty($res)) {
            return $res;
        } else {
            return 0;
        } 
    }
    
    public function powiadomieniaOnclick($post) {
        
        $sql = "INSERT INTO ".DB_PREF."application_note (title, employeer, note, date, rate_min, rate_max, vacancies, trade_id, group_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        $pre = $this->database->prepare($sql);
        
        if($post['paymant']=='stawka godzinowa') {
            
            if($post['rate']) {
                # rate_min
                $rateMin = $post['rate'];

                # rate_max
                $rateMax = $post['rate'];
            } else {
               # rate_min
                $rateMin = $post['rate_min'];

                # rate_max
                $rateMax = $post['rate_max'];
            }
            
        } elseif($post['paymant']=='stawka miesięczna') {
            
            $rate = round($post['rateMonth']/168, 2, PHP_ROUND_HALF_DOWN);
            
               # rate_min
                $rateMin = $rate;

                # rate_max
                $rateMax = $rate;
        } else {
            # rate_min
            $rateMin = $post['rate'];

            # rate_max
            $rateMax = $post['rate'];
        }
        
        if(isset($post['tags']) && !empty($post['tags']) && is_array($post['tags'])) {
            $tagString = '';
            $tagCount = count($post['tags']);
            $i = 1;
            foreach($post['tags'] as $tag) {
                $tagString .= $tag;
                if($i<$tagCount) $tagString .= ',';
                $i++;
            }
        } else {
            $tagString = null;
        }
        
        if(isset($post['groups']) && !empty($post['groups']) && is_array($post['groups'])) {
            $groupString = '';
            $groupCount = count($post['groups']);
            $i = 1;
            foreach($post['groups'] as $group) {
                $groupString .= $group;
                if($i<$groupCount) $groupString .= ',';
                $i++;
            }
        } else {
            $groupString = null;
        }
        
        $array = array(
            $post['position'],
            $this->getUserId(),
            $post['oneclick_info'],
            $post['date'],
            $rateMin,
            $rateMax,
            $post['vacancies'],
            $tagString,
            $groupString
        );

        if($pre->execute($array)) {
            $nid = $this->database->lastInsertId();
            
            $array = array();

            // Wysyłanie do pojedynczych oneclicków
            if(isset($post['oneclick']) && !empty($post['oneclick']) && is_array($post['oneclick'])) {
                foreach($post['oneclick'] as $oneclick) {
                    $array[] = $oneclick;
                }
            }
            
            // Wysyładnie do oneclicków z listy rodzajów pracy
            if(isset($post['tags']) && !empty($post['tags']) && is_array($post['tags'])) {
                foreach($post['tags'] as $tag) {
                    $tagsOneclick = $this->getOneClickTags($tag);
                    foreach($tagsOneclick as $oneclick) {
                        $array[] = $oneclick['oneclick_employee_id'];
                    }
                }
            }
                        
            // Wysyładnie do oneclicków z grupy
            if(isset($post['groups']) && !empty($post['groups']) && is_array($post['groups'])) {
                foreach($post['groups'] as $group) {
                    $groupOneclick = $this->getMemberFromGroup($group);
                    foreach($groupOneclick as $oneclick) {
                        $array[] = $oneclick['user_id'];
                    }
                }
            }
            
            $cleanArray = array_unique($array)  ;

            foreach($cleanArray as $oneclick) {
                $this->powiadomienieOneclick($nid, $oneclick, $post['oneclick_info']);
            }
            
            return true;
        }
    }
    
    public function powiadomienieOneclick($note, $user, $info) {
        $email = $this->getUserEmail($user);

        $sql = "INSERT INTO ".DB_PREF."application_note_user (note, user) VALUES (?, ?)";
        $pre = $this->database->prepare($sql);

        if($pre->execute(array($note, $user))) {

            $body = '<h3>Kandydacie, gratulujemy!</h3><p>Właśnie dostałeś ofertę pracy!</p>';
            $body .= '<p>Treść oferty OneClick:</p>';
            $body .= '<div style="padding: 10px; border: 1px solid #CCC; margin: 5px;">'.$info.'</div>';
            $body .= '<p>Link do oferty: <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'moje-konto/oneclick/'.$note.'">KLIKNIJ TUTAJ</a></p>';
            $title = 'happinate.com - dostałeś ofertę pracy 1-click';

            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($email, $title, $body);
            
            return true;
        } else {
            return false;
        } 
    }
    
    
    public function getPowiadomienieOneclick($id = false, $user = false, $all = true) {

        if($id) {
            if($user) {
                $sql = "SELECT n.*, u.status, u.friend FROM ".DB_PREF."application_note as n LEFT JOIN ".DB_PREF."application_note_user as u ON n.id = u.note WHERE n.id = ? AND u.user = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($id, $this->getUserId()));
                $res = $pre->fetch();
            } else {
                $sql = "SELECT u.note, u.friend, u.status, user.user_id, AES_DECRYPT(user.user_email, '".SALT."') as user_email, AES_DECRYPT(user.user_name, '".SALT."') as user_name, AES_DECRYPT(user.user_surname, '".SALT."') as user_surname, user.user_adress_city, AES_DECRYPT(user.user_phone, '".SALT."') as user_phone FROM ".DB_PREF."application_note_user as u LEFT JOIN ".DB_PREF."user as user ON u.user = user.user_id WHERE u.note = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($id));
                $res = $pre->fetchAll();
            }
        } else {
            if($user) {
                $sql = "SELECT n.title as title, n.id as job_id, u.status, n.date as job_start_date FROM ".DB_PREF."application_note_user as u LEFT JOIN ".DB_PREF."application_note as n ON u.note = n.id WHERE u.user = ?";
                if($all) { 
                    $sql .= " AND u.status = 0 ORDER BY n.date DESC LIMIT 0,3";
                } else {
                    $sql .= " ORDER BY n.date DESC LIMIT 0,3";
                }
           
                $pre = $this->database->prepare($sql);
                $pre->execute(array($this->getUserId()));
                $res = $pre->fetchAll();
            } else {
                $sql = "SELECT * FROM ".DB_PREF."application_note WHERE employeer = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($this->getUserId()));
                $res = $pre->fetchAll();
            }
        }
        
        if($res) {
            return $res;
        } else {
            return false;
        } 
    }
    
    public function getPowiadomienieOneclickWithUsers($id) {
        $array = array();
        
        $sql = "SELECT * FROM ".DB_PREF."application_note WHERE id = ? AND employeer = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id, $this->getUserId()));
        $res = $pre->fetch();
           
        if(!$res) return false;
        
        $array = $res;
        
        $sql = "SELECT user FROM ".DB_PREF."application_note_user WHERE note = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        $res = $pre->fetchAll();
        
        if($res) {
            $users = array();
            foreach($res as $r) {
                $users[] = $r['user'];
            }
        } else {
            $users = false;
        }
        
        $array['users'] = $users;
                
        if($array) {
            return $array;
        } else {
            return false;
        } 
    }
    
    public function countOneClickNotes($id) {

        $sql = "SELECT sum(case when status = 0 then 1 end) as '0', sum(case when status = 1 then 1 end) as '1', sum(case when status = 2 then 1 end) as '2', sum(case when status = 3 then 1 end) as '3' FROM ".DB_PREF."application_note_user WHERE note = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        $res = $pre->fetch();

        if($res) {
            return $res;
        } else {
            return false;
        } 
    }
    
    public function checkIfUserApplied($nid, $uid = false) {
        
        if($uid) {
            $sql = "SELECT count(id) as count FROM ".DB_PREF."application_note_user WHERE note = ? AND user = ?";
            $res = $this->database->prepare($sql);

            $res->execute(array($nid, $uid));
        } else {
            $sql = "SELECT count(id) as count FROM ".DB_PREF."application_note_user WHERE note = ? AND user = ? AND status != 0";
            $res = $this->database->prepare($sql);

            $res->execute(array($nid, $this->getUserId()));
        }
        $res = $res->fetch();
        
        if($res['count']>0) {
            return true;
        } else {
            return false;
        }         

    }
    
    public function checkVacancies($note, $count) {
        
        $sql = "SELECT COUNT(id) as count FROM ".DB_PREF."application_note_user WHERE note = ? AND status = 1";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($note));
        $res = $pre->fetch();

        if($res['count']>=$count) {
            return true;
        } else {
            return false;
        }  
    }
    
    public function powiadomienie($jid, $id, $vac = true, $friend = false) {
        if(!$friend) {
            if($vac) {
                $sql = "UPDATE ".DB_PREF."application_note_user SET status = 1 WHERE note = ? AND user = ?";
            } else {
                $sql = "UPDATE ".DB_PREF."application_note_user SET status = 2 WHERE note = ? AND user = ?";
            }
        } else {
            $sql = "UPDATE ".DB_PREF."application_note_user SET status = 5 WHERE note = ? AND user = ?";
        }
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($jid, $this->getUserId()))) {
            
            $email = $this->getUserEmail($id);
            
            if(!$friend) {
                $title = 'happinate.com - odpowiedź na Twoje powiadomienie 1-click';
                $body = '<h3>Pracodawco, otrzymałeś aplikację na Twoje powiadomienie 1-click.</h3>';
                $body .= '<p>Aby zobaczyć więcej szczegółów, zaloguj się na swoje konto oraz przejdź do zakładki "1-click".</p>';
                $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz!</p>';  
            } else {
                $title = 'happinate.com - kandydat poleca nowego pracownika na stanowisko 1-click';
                $body = '<h3>Pracodawco, nowe polecenie pracownika.</h3>';
                $body .= '<p>Twój ulubiony pracownik poleca Ci kandydata na ogłoszenie typu 1-click. <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'moje-konto/oneclick/'.$jid.'" target="_blank">Kliknij tutaj aby zobaczyć więcej szczegółów</a>.</p>';
                $body .= '<p>Aby zobaczyć więcej szczegółów, zaloguj się na swoje konto oraz przejdź do zakładki "1-click".</p>';
                $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz!</p>';  
            }

            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($email, $title, $body, 'default.html');
            
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }   
    
    public function powiadomienieOdrzuc($jid, $id, $friend = false) {
        
        if($this->checkIfUserApplied($jid)) return false;
        
        if($friend) {
            $sql = "DELETE FROM ".DB_PREF."application_note_user WHERE note = ? AND user = ?";
        } else {
            $sql = "UPDATE ".DB_PREF."application_note_user SET status = 4 WHERE note = ? AND user = ?";
        }
        
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($jid, $this->getUserId()))) {
            
            if($friend) return true;
            
            $email = $this->getUserEmail($id);
            
            $title = 'happinate.com - odpowiedź na Twoje powiadomienie 1-click';
            $body = '<h3>Pracodawco, jeden z Twoich zaproszonych kandydatów odrzucił ofertę 1-click.</h3>';
            $body .= '<p>Aby zobaczyć więcej szczegółów, zaloguj się na swoje konto oraz przejdź do zakładki "1-click".</p>';
            $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz!</p>';  

            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($email, $title, $body, 'default.html');
            
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }   
    
    public function countOneClickTags($id) {
        $sql = "SELECT count(o.oneclick_id) as count FROM ".DB_PREF."application_oneclick as o LEFT JOIN ".DB_PREF."tag_oneclick as t ON o.oneclick_id = t.oneclick_id WHERE t.tag_id = ? AND o.oneclick_employeer_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id, $this->getUserId()));
        $res = $pre->fetch();

        return $res['count'];
    }
    
    public function getOneClickTags($id) {
        $sql = "SELECT oneclick_employee_id FROM ".DB_PREF."application_oneclick as o LEFT JOIN ".DB_PREF."tag_oneclick as t ON o.oneclick_id = t.oneclick_id WHERE t.tag_id = ? AND o.oneclick_employeer_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id, $this->getUserId()));
        $res = $pre->fetchAll();

        return $res;
    }    
    
    public function addOneclickGroup($post) {
        $sql = "INSERT INTO ".DB_PREF."application_oneclick_group (employeer, name, opis) VALUES (?, ?, ?)";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($this->getUserId(), $post['name'], $post['opis']))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function deleteOneclickGroup($id) {
        $sql = "DELETE FROM ".DB_PREF."application_oneclick_group WHERE id = ? AND employeer = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id, $this->getUserId()))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }

    public function getOneclickGroup($id = false) {
        if($id) {
            $sql = "SELECT * FROM ".DB_PREF."application_oneclick_group WHERE id = ? AND employeer = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($id, $this->getUserId()));
            $res = $pre->fetch();
        } else {
            $sql = "SELECT * FROM ".DB_PREF."application_oneclick_group WHERE employeer = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($this->getUserId()));
            $res = $pre->fetchAll();
        }
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }

    public function getFilteredOneclickGroup($searchPhrase = '')
    {
        $qualifiedTableName = DB_PREF.'application_oneclick_group';
        $sql = "SELECT * FROM {$qualifiedTableName} WHERE employeer = ? AND ({$qualifiedTableName}.name LIKE ? OR {$qualifiedTableName}.opis LIKE ?)";
        $pre = $this->database->prepare($sql);
        $pre->execute(array(
            $this->getUserId(),
            "%{$searchPhrase}%",
            "%{$searchPhrase}%"
        ));

        return $pre->fetchAll();
    }


    // todo: czy ja z tego korzystam?
    public function getAllEmployerGroups()
    {
        if (empty($applicantIds)) {
            return array();
        }

        $sql = "SELECT * FROM ".DB_PREF."application_oneclick_group as _group WHERE _group.employeer = ?;";
        $preparedStatement = $this->database->prepare($sql);
        $userId = $this->getUserId();
        $preparedStatement->bindParam(1, $userId, PDO::PARAM_INT);
        $preparedStatement->execute();

        return $preparedStatement->fetchAll();
    }

    public function getOneClickGroupsForUsers($applicantIds)
    {
        if (empty($applicantIds)) {
            return array();
        }

        $sql = "SELECT _groupUser.group_id AS group_id, _groupUser.user AS user FROM ".DB_PREF."application_oneclick_group_user AS _groupUser LEFT JOIN ".DB_PREF."application_oneclick_group AS _group ON _group.id = _groupUser.group_id WHERE _group.employeer = ? AND user IN (".implode(',', $applicantIds).");";
        $preparedStatement = $this->database->prepare($sql);
        $userId = $this->getUserId();
        $preparedStatement->bindParam(1, $userId, PDO::PARAM_INT);
        $preparedStatement->execute();

        return $preparedStatement->fetchAll();
    }


    public function countOneclickInGroup($id) {
        $sql = "SELECT count(DISTINCT user) as count FROM ".DB_PREF."application_oneclick_group_user WHERE group_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        $res = $pre->fetch();

        if(count($res)>0) {
            return $res['count'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }      
    
    public function addMemberToGroup($id, $members) {
        
        foreach($members as $member) {
            $sql = "INSERT INTO ".DB_PREF."application_oneclick_group_user (group_id, user) VALUES (?, ?)";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($id, $member));
        } 
        
        return true;
    }
        
    public function getMemberFromGroup($id) {
        $sql = "SELECT DISTINCT AES_DECRYPT(u.user_email, '".SALT."') as user_email, AES_DECRYPT(u.user_phone, '".SALT."') as user_phone, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, u.user_id FROM ".DB_PREF."application_oneclick_group_user as o LEFT JOIN ".DB_PREF."user AS u ON u.user_id = o.user WHERE o.group_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        $res = $pre->fetchAll();

        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    } 
    
    
    public function deleteMemberFromGroup($id, $uid) {
        $sql = "DELETE FROM ".DB_PREF."application_oneclick_group_user WHERE group_id = ? AND user = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id, $uid))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }

    public function isMemberOfGroup($groupId, $userId)
    {
        $sql = "SELECT COUNT(id) AS isMemberOfGroup FROM ".DB_PREF."application_oneclick_group_user WHERE group_id = ? AND user = ?";
        $pre = $this->database->prepare($sql);

        $pre->bindParam(1, $groupId, PDO::PARAM_INT);
        $pre->bindParam(2, $userId, PDO::PARAM_INT);

        if($pre->execute()) {
            $allRows = $pre->fetchAll();
            $row = current($allRows);

            return (bool)$row['isMemberOfGroup'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }


    public function addMemberToGroupSecurely($groupId, $member)
    {
        $group = $this->getOneclickGroup($groupId);

        if (!empty($group) && intval($group['employeer']) == $this->getUserId() && !$this->isMemberOfGroup($groupId, $member)) {
            return $this->addMemberToGroup($groupId, array($member));
        }

        return false;
    }

    public function removeMemberFromGroupSecurely($groupId, $member)
    {
        $group = $this->getOneclickGroup($groupId);

        if (!empty($group) && $group['employeer'] == $this->getUserId()) {
            return $this->deleteMemberFromGroup($groupId, $member);
        }

        return false;
    }

    public function getCalendarEventsSmall($date) {

        $sql = "SELECT status FROM ".DB_PREF."application_note_user as u LEFT JOIN ".DB_PREF."application_note as n ON u.note = n.id WHERE u.user = ? AND n.date = ? ORDER BY CASE WHEN status = 0 THEN 1 WHEN status = 2 THEN 2 WHEN status = 1 THEN 3 WHEN status = 3 THEN 4 ELSE 5 END";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId(), $date));
        
        $res = $pre->fetch();

        if($res && count($res)>0) {
            if($res['status']==0) {
                return -1;
            } else {
                return $res['status'];
            }
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }

            
    public function getCalendarEvents($date) {

        $sql = "SELECT n.*, u.status FROM ".DB_PREF."application_note_user as u LEFT JOIN ".DB_PREF."application_note as n ON u.note = n.id WHERE u.user = ? AND n.date = ? AND u.status < 4";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId(), $date));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
            
    public function getUnreadCalendarEvents() {

        $sql = "SELECT n.* FROM ".DB_PREF."application_note_user as u LEFT JOIN ".DB_PREF."application_note as n ON u.note = n.id WHERE u.user = ? AND u.status = 0 AND n.date >= NOW()";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
            
    public function getUnreadFriendInvitations() {

        $sql = "SELECT DISTINCT u.user_id, f.id, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, AES_DECRYPT(u.user_email, '".SALT."') as user_email FROM ".DB_PREF."user_friend as f LEFT JOIN ".DB_PREF."user as u ON u.user_id = f.friend_id WHERE f.user_id = ? AND accepted = 0";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }

    public function getProfile() {
        $sql = "SELECT AES_DECRYPT(user_name, '".SALT."') as user_name, AES_DECRYPT(user_surname, '".SALT."') as user_surname, user_main_tag_id, user_id, user_type, user_hobby, user_cv_color, user_bgcv_color, user_date_of_birth, user_profile_yt_movie, user_profile_url, user_profile_img, user_profile_img_info, user_profile_name, user_info, AES_DECRYPT(user_email, '".SALT."') as user_email, user_logo, user_adress_city, AES_DECRYPT(user_adress_zip_code, '".SALT."') as user_adress_zip_code, AES_DECRYPT(user_adress_street, '".SALT."') as user_adress_street, user_www, user_fanpage,  AES_DECRYPT(user_phone, '".SALT."') as user_phone FROM ".DB_PREF."user WHERE user_id = ?";

        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        if($pre->rowCount()!=0) {
            return $pre->fetch();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }
    
    public function getJobList($id) {
        $month = date("Y-m-d H:i:s");
        $sql = "SELECT * FROM ".DB_PREF."job WHERE job_status = 1 AND job_added_by = ? AND job_publish_date_end > '".$month."' AND job_delete = 0";

        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }
    
    public function inviteFriendsToOneclick($id, $array) {
        $userId = $this->getUserId();
        
        $emailError = '';
        
        foreach($array as $a) {
            $uid = $this->getUserId($a);
            
            if($this->checkIfUserApplied($id, $uid)) {
                $emailError .= $a . ', ';
            } else {
                $sql = "INSERT INTO ".DB_PREF."application_note_user (note, user, status, friend) VALUES (?, ?, 0, ?)";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($uid, $id, $userId));
                
                $title = 'happinate.com - zaproszenie od znajomego do ogłoszenia typu 1-click';
                $body = '<h3>Kandydacie,</h3>';
                $body .= '<p>Twój znajomy '.$this->getUserEmail().' zaprasza Cię do oferty typu 1-click. <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'moje-konto/oneclick/'.$id.'"" target="_blank">Kliknij tutaj aby zobaczyć szczegóły</a>.</p>';
                $body .= '<p>Aby zobaczyć więcej szczegółów, zaloguj się na swoje konto oraz przejdź do zakładki "Oferty 1-click".</p>';
                $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz!</p>';  

                $_emailClass = $GLOBALS['_emailClass'];
                $_emailClass->addToList($a, $title, $body, 'default.html');
            }
        }
        
        if($emailError) {
           return $emailError;
        } else {
            return false;
        }
    }
    
    
    public function checkIfUserIsInvitedByFriend($nid, $uid = false) {
        
        if($uid) {
            $sql = "SELECT count(id) as count FROM ".DB_PREF."application_note_user WHERE note = ? AND user = ? AND friend IS NOT NULL";
            $res = $this->database->prepare($sql);

            $res->execute(array($nid, $uid));
        } else {
            $sql = "SELECT count(id) as count FROM ".DB_PREF."application_note_user WHERE note = ? AND user = ? AND friend IS NOT NULL";
            $res = $this->database->prepare($sql);

            $res->execute(array($nid, $this->getUserId()));
        }
        $res = $res->fetch();
        
        if($res['count']>0) {
            return true;
        } else {
            return false;
        }         

    }
    
    
    public function deleteUser($user_id) {
        $sql = "DELETE FROM ".DB_PREF."user_watch WHERE user_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($user_id));
        
        $sql = "DELETE FROM ".DB_PREF."user_tag WHERE tag_user_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($user_id));
        
        $sql = "DELETE FROM ".DB_PREF."user_skill WHERE skill_user_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($user_id));
        
        $sql = "DELETE FROM ".DB_PREF."user_other_skill WHERE skill_user_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($user_id));
        
        $sql = "DELETE FROM ".DB_PREF."user_lang WHERE lang_user_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($user_id));
        
        $sql = "DELETE FROM ".DB_PREF."user_friend WHERE user_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($user_id));
        
        $sql = "DELETE FROM ".DB_PREF."user_fav_job WHERE fav_user_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($user_id));
        
        $sql = "DELETE FROM ".DB_PREF."user_expirience WHERE expirience_user_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($user_id));
        
        $sql = "DELETE FROM ".DB_PREF."user_education WHERE education_user_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($user_id));
        
        $sql = "DELETE FROM ".DB_PREF."user_cv WHERE cv_user_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($user_id));
        
        $sql = "DELETE FROM ".DB_PREF."user WHERE user_id = ?";
        $res = $this->database->prepare($sql);
        $r = $res->execute(array($user_id));
        
        return true;
    }

    public function attachGroupsToEmployees($user, array $employees)
    {
        if (intval($user->getUserType()) != 2) {
            return $employees;
        }

        $employeeIds = array();
        foreach ($employees as $employee) {
            $employeeIds[] = intval($employee['user_id']);
        }

        $employeeIdsAsString = implode(',', $employeeIds);
        $sql = "SELECT oneclickGroup.id AS groupId, oneclickGroup.name AS groupName, oneclickGroupUser.user AS userId FROM ".DB_PREF."application_oneclick_group AS oneclickGroup LEFT JOIN ".DB_PREF."application_oneclick_group_user AS oneclickGroupUser ON oneclickGroup.id = oneclickGroupUser.group_id WHERE oneclickGroup.employeer = ? AND oneclickGroupUser.user IN ({$employeeIdsAsString});";

        $preparedStatement = $this->database->prepare($sql);
        $preparedStatement->execute(array($this->getUserId()));

        $groups = $preparedStatement->fetchAll();

        foreach ($groups as $group) {
            if (!isset($employees[$group['userId']]['groups'])) {
                $employees[$group['userId']]['groups'] = array();
            }
            $employees[$group['userId']]['groups'][$group['groupId']] = $group['groupName'];
        }
        foreach ($employees as $key => $employee) {
            if (isset($employee['groups'])) {
                $employees[$key]['groupsIds'] = array_keys($employees[$key]['groups']);
                $employees[$key]['groups'] = implode(', ', $employees[$key]['groups']);
            } else {
                $employees[$key]['groups'] = array();
            }
        }

        return $employees;
    }
}


