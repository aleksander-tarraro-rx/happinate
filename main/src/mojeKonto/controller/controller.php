<?php

namespace src\mojeKonto;

use app\helpers\FlashMsg;
use app\helpers\Form;
use src\BaseController;

class mojeKontoController extends BaseController {
    
    private $template;
    private $model;
    private $user;
    private $userType;

    public function __construct($template, $model, $user) {
        $this->template = $template;
        $this->model = $model;
        $this->user = $user;

        if(!$this->user || !$this->user->checkUser()) {
            FlashMsg::add('warning', 'Musisz się najpierw zalogować aby mieć dostęp do tej strony.');
            header("Location:".HOST."zaloguj-sie");
        }
        
        if($this->user) {
            $this->userType = $user->getUserType();

            $this->template->assign('tags', $user->getTags());
        }

        $this->template->assign('model', $this->model);
    }
    
    /* 
     * Mój profil
     * 
     * KANDYDAT / PRACODAWCA
     */
    public function indexAction($queryName = false) {
        // POPUP
        if(isset($_POST['popup'])) {
            $_SESSION['popup'] = true;
        }
        /*
         * PRACODAWCA 
         */
        if($this->userType==2) {
            if($this->user->employeerNotCompleted()) {
                $profil = $this->model->getEmployeerProfile();

                $userCalendar = $this->model->generateSimpleCalendar();
                $this->template->assign('userCalendar', $userCalendar);

                $this->template->assign('userProfile', $profil);

                // Friend Invitation
                if(isset($_POST['friendEmail']) && $_POST['friendEmail']) {
                    $error = '';
                    if(empty($_POST['friendEmail'])) {
                        $error .= '<p>- Proszę wpisać adres email.</p>';
                    } elseif(!Form::validEmail($_POST['friendEmail'])) {
                        $error .= '<p>- Proszę wpisać poprawny adres email.</p>';
                    }
                    if(!$error) {
                        if($this->model->inviteFriend($_POST['friendEmail'], true)) {
                            FlashMsg::add('success', 'Udało Ci się poprawnie wysłać polecenie.');
                            header("Location: " . HOST . "moje-konto");
                        } else {
                            FlashMsg::add('error', 'Nie udało Ci się poprawnie wysłać polecenie.');
                            header("Location: " . HOST . "moje-konto");
                        }
                    } else {
                        $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
                    }
                }

                if(isset($_POST['update-profile'])) {
                    $error = '';
                    $errorArray = array();
                    // Validation
                    $_POST = Form::sanitizeArray($_POST);
                    if(empty($_POST['profile_name'])) {
                        $error .= '<p>Proszę wpisać nazwę profilu.</p>';
                        $errorArray['profile_name'] = true;
                    } elseif(!Form::validNoSpecialChars($_POST['profile_name'])) {
                        $error .= '<p>Nazwa profilu musi się składać wyłącznie prostych znaków i liczb.</p>';
                        $errorArray['profile_name'] = true;
                    } elseif(strlen($_POST['profile_name']) < 1 || strlen($_POST['profile_name']) > 50) {
                        $error .= '<p>Nazwa profilu musi mieć od 1 do 50 znaków.</p>';
                        $errorArray['profile_name'] = true;
                    }
                    if(empty($_POST['profile_email'])) {
                        $error .= '<p>Proszę wpisać adres email.</p>';
                        $errorArray['profile_email'] = true;
                    } elseif(!Form::validEmail($_POST['profile_email'])) {
                        $error .= '<p>Proszę podać prawidłowy adres email.</p>';
                        $errorArray['profile_email'] = true;
                    } elseif($this->model->checkEmailNotExist($_POST['profile_email'])) {
                        $error .= '<p>Podany adres email znajduje się już w bazie danych.</p>';
                        $errorArray['profile_email'] = true;
                    }

                    if(!empty($_POST['profile_zip_code'])) {
                        if(!Form::validPolishZipCode($_POST['profile_zip_code'])) {
                            $error .= '<p>Proszę podać prawidłowy kod pocztowy (przykład 00-000)</p>';
                            $errorArray['profile_zip_code'] = true;
                        }
                    }
                    if(!empty($_POST['profile_nip'])) {
                        if(!is_numeric($_POST['profile_nip']) || !Form::validPolishNip($_POST['profile_nip'])) {
                            $error .= '<p>Proszę podać prawidłowy numer NIP</p>';
                            $errorArray['profile_nip'] = true;
                        }
                    }
                    if(!empty($_POST['profile_regon'])) {
                        if(!is_numeric($_POST['profile_regon']) || !Form::validPolishRegon($_POST['profile_regon'])) {
                            $error .= '<p>Proszę podać prawidłowy numer REGON</p>';
                            $errorArray['profile_regon'] = true;
                        }
                    }
                    if(!empty($_POST['profile_pesel'])) {
                        if(!is_numeric($_POST['profile_pesel']) || !Form::validPolishPesel($_POST['profile_pesel'])) {
                            $error .= '<p>Proszę podać prawidłowy numer pesel.</p>';
                            $errorArray['profile_pesel'] = true;
                        }
                    }
                    if(!empty($_POST['www'])) {
                        if(!Form::validUrl($_POST['www'])) {
                            $error .= '<p>Proszę podać prawidłowy adres strony www.</p>';
                            $errorArray['www'] = true;
                        }
                    }
                    if(!empty($_POST['fanpage'])) {
                        if(!Form::validUrl($_POST['fanpage'])) {
                            $error .= '<p>Proszę podać prawidłowy adres fanpage\'a.</p>';
                            $errorArray['fanpage'] = true;
                        }
                    }

                    if(isset($_FILES['file']) && $_FILES['file']['error']==0) {
                        $imgInfo = getimagesize($_FILES['file']['tmp_name']);

                        if(!($imgInfo['mime']=='image/jpeg' || $imgInfo['mime']=='image/png' || $imgInfo['mime']=='image/gif')) {
                            $error .= '<p>- przesłane logo musi być formatu png, jpg lub gif.</p>';
                            $errorArray['file'] = true;
                        }

                        if($imgInfo[0]>1600 || $imgInfo[1]>1200) {
                            $error .= '<p>- przesłane logo może mieć maksymalną rozdzielczość 1024px na 768px (szerokość / wysokość).</p>';
                            $errorArray['file'] = true;
                        }

                        if($_FILES['file']['size']>1024000) {
                            $error .= '<p>- przesłane logo waży zbyt dużo (maksymalna waga to 500kb).</p>';
                            $errorArray['file'] = true;
                        }

                        $file = $_FILES['file'];
                    } elseif(isset($_FILES['file']) && $_FILES['file']['error']==1) { 
                        $error .= '<p>- przesłane zdjęcie profilowe waży zbyt dużo (maksymalna waga to 1mb).</p>';
                        $errorArray['file2'] = true;
                    } else {
                        $file = 0;
                    }
                    if(isset($_POST['file_delete']) && $_POST['file_delete']=='true') {
                        $file = -1;
                    }

                    if(!$error) {
                        if($this->model->updateEmployeer($_POST, $file)) {
                            FlashMsg::add('success', 'Udało Ci się poprawnie edytować swój profil.');
                            header("Location: " . HOST . "moje-konto");
                        } else {
                            FlashMsg::add('error', 'Nie udało Ci się edytować profilu. Spróbuj ponownie później.');
                            header("Location: " . HOST . "moje-konto");
                        }
                    } else {
                        $this->template->assign('errorArray', $errorArray);
                        $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
                    }            
                }
                // Edycja profilu
                if(isset($_POST['profileEdit']) && $_POST['profileEdit']) {
                    $error = '';
                    $errorArray = array();
                    // Validation
                    $_POST = Form::sanitizeArray($_POST);

                    /*
                    if(empty($_POST['profile_url'])) {
                        $error .= '<p>Proszę wpisać unikalny adres profilu.</p>';
                        $errorArray['profile_url'] = true;
                    } elseif(!preg_match('/^[a-zA-Z0-9]+$/', $_POST['profile_url'])) {
                        $error .= '<p>Unikalny adres może składać się tylko z prostych znaków (bez polskich znaków), liczb oraz bez spacji.</p>';
                        $errorArray['profile_url'] = true;
                    } elseif(!$this->model->checkUniqueProfileUrl($_POST['profile_url'])) {
                        $error .= '<p>Taki unikalny adres jest już użyty.</p>';
                        $errorArray['profile_url'] = true;
                    }
                    */

                    if(!empty($_POST['yt']) && !Form::validYoutubeUrl($_POST['yt'])) {
                        $error .= '<p>Proszę wpisać prawidłowy link skopiowany ze strony Youtube.</p>';
                        $errorArray['yt'] = true;
                    }

                    if(isset($_FILES['file']) && $_FILES['file']['error']==0) {
                        $imgInfo = getimagesize($_FILES['file']['tmp_name']);

                        if(!($imgInfo['mime']=='image/jpeg' || $imgInfo['mime']=='image/png' || $imgInfo['mime']=='image/gif')) {
                            $error .= '<p>- przesłane zdjęcie profilowe musi być formatu png, jpg lub gif.</p>';
                            $errorArray['file2'] = true;
                        }

                        if($_FILES['file']['size']>1048576) {
                            $error .= '<p>- przesłane zdjęcie profilowe waży zbyt dużo (maksymalna waga to 1mb).</p>';
                            $errorArray['file2'] = true;
                        }

                        $file = $_FILES['file'];
                    } elseif(isset($_FILES['file']) && $_FILES['file']['error']==1) { 
                        $error .= '<p>- przesłane zdjęcie profilowe waży zbyt dużo (maksymalna waga to 1mb).</p>';
                        $errorArray['file2'] = true;
                    } else {
                        $file = 0;
                    }
                    if(isset($_POST['file_delete']) && $_POST['file_delete']=='true') {
                        $file = -1;
                    }

                    if(!$error) {
                        if($this->model->updateEmployeerProfile($_POST, $file)) {
                            FlashMsg::add('success', 'Udało Ci się poprawnie edytować swój profil.');
                            header("Location: " . HOST . "moje-konto");
                        } else {
                            FlashMsg::add('error', 'Nie udało Ci się edytować profilu. Spróbuj ponownie później.');
                            header("Location: " . HOST . "moje-konto");
                        }
                    } else {
                        $this->template->assign('errorArray', $errorArray);
                        $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
                    }            
                }

                if(isset($_POST['changePass']) && $_POST['changePass']) {
                    $error = '';
                    // Validation
                    $_POST = Form::sanitizeArray($_POST);

                    if(empty($_POST['password_old']) || empty($_POST['password_new']) || empty($_POST['password_new2'])) {
                        $error .= '<p>Proszę wypełnić wszystkie pola (stare hasło, nowe hasło i powtórz nowe hasło)</p>';
                    } else {
                        if(!$this->model->checkPassword($_POST['password_old'])) {
                            $error .= '<p>Twoje stare hasło jest niepoprawne.</p>';
                    }

                        if($_POST['password_new']!=$_POST['password_new2']) {
                            $error .= '<p>Proszę wpisać dwa identyczne nowe hasła.</p>';
                     }

                        if($_POST['password_new']!=$_POST['password_new2']) {
                            $error .= '<p>Proszę wpisać dwa identyczne nowe hasła.</p>';
                        }
                    }

                    if(!$error) {
                        if($this->model->changePassword($_POST['password_new'])) {
                            FlashMsg::add('success', 'Udało Ci się poprawnie zmienić hasło.');
                            header("Location: " . HOST . "moje-konto");
                        } else {
                            FlashMsg::add('error', 'Nie udało Ci się zmienić hasła. Spróbuj ponownie później.');
                            header("Location: " . HOST . "moje-konto");
                        }
                    } else {
                        $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
                    } 
                }

                $this->template->display('mojeKonto/pracodawca/index.tpl');
            } else {

                $profile = $this->model->getProfile(false, $queryName);

                $jobList = $this->model->getJobList($profile['user_id']);

                $this->template->assign('profile', $profile);
                $this->template->assign('jobList', $jobList);
                $this->template->display('mojeKonto/pracodawca/index2.tpl');
            }
            
        /*
         * KANDYDAT
         */
        } elseif($this->userType==1) {
            $profil = $this->model->getEmployeeProfile();
            $tagList = $this->model->getTags();
            $userTags = $this->model->getUserTags();    
            $userCalendar = $this->model->generateSimpleCalendar();
            $workNowJobs = $this->model->getWorkNowJobs();
            $userFav = $this->model->getUserFav();
            $userWatch = $this->model->getUserWatch();
            $userOneclick = $this->model->getOneClick(3);
            $userFriend = $this->model->getUserFriend();
            $calendarEvents = $this->model->getUnreadCalendarEvents();
            $friendInvitations = $this->model->getUnreadFriendInvitations();
            
           #echo "<pre>"; print_r($friendInvitations); echo "</pre>"; die();
            
            $this->template->assign('tagList',$tagList);
            $this->template->assign('userTags',$userTags);
            $this->template->assign('userProfile', $profil);
            $this->template->assign('workNowJobs', $workNowJobs);
            $this->template->assign('userFav', $userFav);
            $this->template->assign('userWatch', $userWatch);
            $this->template->assign('userFriend', $userFriend);
            $this->template->assign('userOneclick', $userOneclick);
            $this->template->assign('userCalendar', $userCalendar);
            $this->template->assign('calendarEvents',$calendarEvents);
            $this->template->assign('friendInvitations',$friendInvitations);
            
            #print_r($userOneclick);

            $userExperience = $this->model->getExperience($profil['user_id']);
            $userEducation = $this->model->getEducation($profil['user_id']);
            $userLangs = $this->model->getLang($profil['user_id']);
            $userTraining = $this->model->getTraining($profil['user_id']);
            $userSkill = $this->model->getSkill($profil['user_id']);
            $userOtherSkill = $this->model->getOtherSkill($profil['user_id']);

            $this->template->assign('tagList',$tagList);
            $this->template->assign('userTags',$userTags);
            $this->template->assign('userProfile', $profil);
            $this->template->assign('userCalendar', $userCalendar);

            $int = 0;

            if($userExperience) $int++;
            if($userEducation) $int++;
            if($userLangs) $int++;
            if($userTraining) $int++;
            if($userOtherSkill) $int++;

            $percent = ceil(($int/5)*100);
            $this->template->assign('completePercent', $percent);

            // AJAX
            if(isset($_POST['worknow'])) {
                $this->model->userWorknow($_POST['worknow']);
                die();
            }
            if(isset($_POST['sms'])) {
                $this->model->userSms($_POST['sms']);
                die();
            }
            
            // Zmiana hasła
            if(isset($_POST['changePass']) && $_POST['changePass']) {
                $error = '';
                // Validation
                $_POST = Form::sanitizeArray($_POST);

                if(empty($_POST['pass']) || empty($_POST['npass']) || empty($_POST['npass2'])) {
                    $error .= '<p>Proszę wypełnić wszystkie pola (stare hasło, nowe hasło i powtórz nowe hasło)</p>';
                } else {
                    if(!$this->model->checkPassword($_POST['pass'])) {
                        $error .= '<p>Twoje stare hasło jest niepoprawne.</p>';
                }

                    if($_POST['npass']!=$_POST['npass2']) {
                        $error .= '<p>Proszę wpisać dwa identyczne nowe hasła.</p>';
                    }
                }

                if(!$error) {
                    if($this->model->changePassword($_POST['npass'])) {
                        FlashMsg::add('success', 'Udało Ci się poprawnie zmienić hasło.');
                        header("Location: " . HOST . "moje-konto");
                    } else {
                        FlashMsg::add('error', 'Nie udało Ci się zmienić hasła. Spróbuj ponownie później.');
                        header("Location: " . HOST . "moje-konto");
                    }
                } else {
                    $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
                } 
            }
            
            //
            // Usuwanie konta
            //
            if(isset($_POST['deleteUser']) && $_POST['deleteUser']) {
                $error = '';
                $_POST = Form::sanitizeArray($_POST);
                
                if(empty($_POST['pass'])) {
                    $error .= '<p>Proszę wpisać hasło.</p>';
                } else {
                    if(!$this->model->checkPassword($_POST['pass'])) {
                        $error .= '<p>Twoje hasło jest niepoprawne.</p>';
                    }
                }
                
                if(!$error) {
                    if ($this->model->deleteUser($this->model->getUserId())) {
                        $_SESSION['userToken'] = NULL;
                        $_SESSION['userTokenFB'] = NULL;
                        $_SESSION['basket'] = NULL;
                        $_SESSION['resume'] = NULL;
                        $_SESSION['popup'] = NULL;
                        FlashMsg::add('success', 'Twoje konto zostało usunięte');
                        header("Location: " . HOST );
                        exit;
                    }
                } else {
                    $this->template->assign('error', '<p><strong>Podczas usuwania konta wystąpiły błędy:</strong></p>'.$error);
                }
            }
            
            //
            // Tagi
            //

            if(isset($_POST['update-tags'])) {
                $error = '';
                $errorArray = array();

                // Validation
                $_POST = Form::sanitizeArray($_POST);

                if(!isset($_POST['main']) || count($_POST['main'])<=0) {
                    $error .= '<p>- proszę wybrać jedną główną kategorię</p>';
                }

                if(!isset($_POST['tags']) || count($_POST['tags'])<2) {
                    $error .= '<p>- proszę wybrać przynajmniej 2 kategorie pozostałe</p>';
                }

                if(!$error) {
                    if($this->model->saveTags($_POST)) {
                        FlashMsg::add('success', 'Udało Ci się poprawnie zapisać kategorie.');

                        if (empty($_POST['tags']) || !is_array($_POST['tags']) || !isset($_GET['szukaj'])) {
                            header("Location: " . HOST . "moje-konto");
                        }

                        $tagNames = array();
                        foreach ($_POST['tags'] as $tagId) {
                            $tagName = $this->model->getTagName(intval($tagId));
                            $tagNames[] = strtolower(str_replace(' ', '_', $tagName));
                        }
                        asort($tagNames);
                        header("Location: " . HOST . "oferty-pracy/1/" . implode(',', $tagNames) . "------");
                    } else {
                        FlashMsg::add('error', 'Nie udało Ci się zapisać kategorii. Spróbuj ponownie później.');
                        header("Location: " . HOST . "moje-konto");
                    }
                } else {
                    $this->template->assign('error', $error);
                }
            }
            
            $this->template->display('mojeKonto/kandydat/index.tpl');
        /*
         * ADMINISTRATOR
         */ 
        } elseif($this->userType==5) {
            $stats = $this->model->getStatistics();

            $this->template->assign('stats', $stats);
            $this->template->display('mojeKonto/administrator/index.tpl');
        /*
         * MODERATOR
         */ 
        } elseif($this->userType==6) {
            $stats = $this->model->getStatistics();

            $this->template->assign('stats', $stats);
            $this->template->display('mojeKonto/moderator/index.tpl');
        }
    }
    
    /* 
     * Dodaj ogłoszenie
     * 
     * PRACODAWCA 
     */
    public function dodajOgloszenieAction($queryName = false) {
        header("Location: " . HOST . "daj-ogloszenie"); 
    }
    
    /* 
     * Moje Ogłoszenia
     * 
     * PRACODAWCA
     */
    public function mojeOgloszeniaAction($queryName = false) {
        /* Zabezpiecznie */
        if($this->userType==2) {

            // Friend Invitation
            if(isset($_POST['friendEmail']) && $_POST['friendEmail']) {
                $error = '';
                if(empty($_POST['friendEmail'])) {
                    $error .= '<p>- Proszę wpisać adres email.</p>';
                } elseif(!Form::validEmail($_POST['friendEmail'])) {
                    $error .= '<p>- Proszę wpisać poprawny adres email.</p>';
                }
                if(!$error) {
                    if($this->model->inviteFriend($_POST['friendEmail'], true)) {
                        FlashMsg::add('success', 'Udało Ci się poprawnie wysłać polecenie.');
                        header("Location: " . HOST . "moje-konto/moje-ogloszenia");
                    } else {
                        FlashMsg::add('error', 'Nie udało Ci się poprawnie wysłać polecenie.');
                        header("Location: " . HOST . "moje-konto/moje-ogloszenia");
                    }
                } else {
                    $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
                }
            }
        
            if(!$queryName) {
                $jobsList = $this->model->getMyJobs($queryName);

                #echo "<pre>"; print_r($jobList); echo "</pre>";

                $this->template->assign('jobsList', $jobsList);
                $this->template->display('mojeKonto/pracodawca/mojeOgloszenia.tpl');

            } elseif ($queryName && is_numeric($queryName)) {

                if(!empty($_POST['act']) && $_POST['act'] == 'confirm')
                {
                    if(!empty($_POST['jid']) && !empty($_POST['uid']))
                    {
                        $_d = array();
                        $_d['jid'] = (int)$_POST['jid'];
                        $_d['uid'] = (int)$_POST['uid'];
                        $_d['date'] = $_POST['date'];
                        $_d['hour'] = $_POST['hour'];
                        $_d['information'] = $_POST['information'];
                        $this->model->confimCalendar($_d);
                    }
                }

                $appList = $this->model->getUserJobList(true, $queryName);
                $appList = $this->appendGroupsToApplicants($appList, $this->user);

                $appOneClickList = $this->model->getOneClickUserJobList($queryName);
                
                $position = $this->model->getJobPosition($queryName);
                $calendarAppList = $this->model->getUsersCalendarByJid($queryName);

                $this->template->assign('calendarAppList', $calendarAppList);
                $this->template->assign('appList', $appList);
                $this->template->assign('appOneClickList', $appOneClickList);
                $this->template->assign('position', $position);
                $this->template->assign('appGroups', $this->model->getOneclickGroup());

                $this->template->display('mojeKonto/pracodawca/mojeOgloszeniaKandydaci.tpl');

            } else {
                $url = explode(',', $queryName);
                
                if(!$this->model->checkUserRightToEdit($url[1])) {
                    FlashMsg::add('warning', 'Nie masz uprawnień do zmiany tego ogłoszenia.');
                    header("Location:".HOST."moje-konto/moje-ogloszenia");                       
                }

                switch($url[0]) {
                    case 'wylacz':                       
                        if($this->model->pauseJob($url[1])) {
                            FlashMsg::add('success', 'Udało się poprawnie zmienić status ogłoszenie.');
                            header("Location:".HOST."moje-konto/moje-ogloszenia");       
                        } else {
                            FlashMsg::add('warning', 'Nie udało się zmienić statusu ogłoszenia.');
                            header("Location:".HOST."moje-konto/moje-ogloszenia");       
                        }
                    break;
                    case 'wlacz':
                        if($this->model->resumeJob($url[1])) {
                            FlashMsg::add('success', 'Udało się poprawnie zmienić status ogłoszenie.');
                            header("Location:".HOST."moje-konto/moje-ogloszenia");       
                        } else {
                            FlashMsg::add('warning', 'Nie udało się zmienić statusu ogłoszenia.');
                            header("Location:".HOST."moje-konto/moje-ogloszenia");       
                        }
                    break;
                    case 'usun':
                        if($this->model->deleteJob($url[1])) {
                            FlashMsg::add('success', 'Udało się poprawnie usunąć ogłoszenie.');
                            header("Location:".HOST."moje-konto/moje-ogloszenia");       
                        } else {
                            FlashMsg::add('warning', 'Nie udało się usunąć ogłoszenia.');
                            header("Location:".HOST."moje-konto/moje-ogloszenia");       
                        }
                    break;
                    case 'edytuj':
                        if(!$job = $this->model->getJob($url[1])) 
                            header('Location:'.HOST);


                        $profile = $this->model->getEmployeerProfile();
                        $contractType = $this->model->getContractType();
                        $trades = $this->model->getTrades();
                        $cities = $this->model->getCities();

                        $this->template->assign('profile', $profile);
                        $this->template->assign('contractType', $contractType);
                        $this->template->assign('trades', $trades);
                        $this->template->assign('cities', $cities);
  
                        if(isset($_POST['ogloszenie-edit'])) {
                            $error = '';
                            $errorArray = array();
                            $_POST = Form::sanitizeArray($_POST);

                            
                            # www
                            if(!empty($_POST['www']) && !Form::validUrl($_POST['www'])) {
                                $error .= '<p>- Proszę prawidłowy adres www.</p>';
                                $errorArray['www'] = true;
                            } else {
                                $_POST['www'] = str_replace(array('http://','https://'), array('',''), $_POST['www']);
                            }

                            # fanpage
                            if(!empty($_POST['fanpage']) && !Form::validUrl($_POST['fanpage'])) {
                                $error .= '<p>- Proszę podać prawidłowy adres fanpage.</p>';
                                $errorArray['fanpage'] = true;
                            } else {
                                $_POST['fanpage'] = str_replace(array('http://','https://'), array('',''), $_POST['fanpage']);
                            }

                           

                            if(!$error) {
                                if($this->model->updateJob($url[1], $_POST)) {
                                    FlashMsg::add('success', 'Udało Ci się poprawnie edytować ogłoszenie.');
                                    header("Location: " . HOST . "moje-konto/moje-ogloszenia/edytuj,".$url[1]);
                                } else {
                                    FlashMsg::add('error', 'Nie udało Ci się edytować ogłoszenia. Spróbuj ponownie później.');
                                    header("Location: " . HOST . "moje-konto/moje-ogloszenia/edytuj,".$url[1]);
                                }
                            } else {
                                $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
                                $this->template->assign('errorArray', $errorArray);
                            }       
                        }
                        
                        #echo "<pre>"; print_r($job); echo "</pre>";
                        
                        $this->template->assign('job', $job);
                        $this->template->display('mojeKonto/pracodawca/edytujOgloszenie.tpl'); 
                    break;
                    case 'sukces':
                        FlashMsg::add('success', 'Udało Ci się poprawnie wznowić ogłoszenie.');
                        header('Location:'.HOST.'moje-konto/moje-ogloszenia');
                    break;
                    case 'wznow':
                        if(!$this->model->getJob($url[1])) 
                            header('Location:'.HOST.'moje-konto/moje-ogloszenia');
                        
                        $id = $url[1];
                        
                        if(isset($_POST['formula']) && is_numeric($_POST['formula'])) {
                            switch($_POST['formula']) {
                                // Free
                                case 1:
                                    if($this->model->resumeJob($url[1])) {
                                        FlashMsg::add('success', 'Udało Ci się poprawnie wznowić ogłoszenie.');
                                        header("Location: " . HOST . "moje-konto/moje-ogloszenia/sukces,".$id);
                                    } else {
                                        FlashMsg::add('warning', 'Nie udało się wznowić ogłoszenia. Spróbuj ponownie później.');
                                        header("Location: " . HOST . "moje-konto/moje-ogloszenia/sukces,".$id);
                                    }
                                break;
                                // Mini
                                case 2:
                                    $control = sha1(time());
                                    
                                    if($this->model->resumeJob($id, 2, $control)) {
     
                                        $url  = "https://ssl.dotpay.pl/?pid=ZV3NQR51647D8699Z6ZE6IWEG5RJRMT3";
                                        $url .= "&type=0";
                                        $url .= "&URL=http://".$_SERVER['SERVER_NAME'] . HOST ."moje-konto/moje-ogloszenia/sukces,".$id;

                                        if($this->user) {
                                            $url .= "&email=".$this->user->getUserEmail();
                                        } else {
                                            $url .= "&email=".$_POST['email'];
                                        }
                                        
                                        $url .= "&control=".$control;
                                        $url .= "&back_button_url=http://".$_SERVER['SERVER_NAME'] . HOST ."moje-konto/moje-ogloszenia/wznow,".$id;

                                        header("Location:".$url);
                                    } else {
                                        FlashMsg::add('warning', 'Nie udało się wznowić ogłoszenia. Spróbuj ponownie później.');
                                        header("Location: " . HOST . "moje-konto/moje-ogloszenia/sukces,".$id);
                                    }

                                break;
                                // Happi
                                case 3:
                                    
                                    $control = sha1(time());
                                    
                                    if($this->model->resumeJob($id, 3, $control)) {
            
                                        $url  = "https://ssl.dotpay.pl/?pid=2Z8P8G8V6QY1GU7U1HWAIYWR6SUQT9S1";
                                        $url .= "&type=0";
                                        $url .= "&URL=http://".$_SERVER['SERVER_NAME'] . HOST ."moje-konto/moje-ogloszenia/sukces,".$id;

                                        if($this->user) {
                                            $url .= "&email=".$this->user->getUserEmail();
                                        } else {
                                            $url .= "&email=".$_POST['email'];
                                        }
                                        
                                        $url .= "&control=".$control;
                                        $url .= "&back_button_url=http://".$_SERVER['SERVER_NAME'] . HOST ."moje-konto/moje-ogloszenia/wznow,".$id;

                                        header("Location:".$url);
                                    } else {
                                        FlashMsg::add('warning', 'Nie udało się wznowić ogłoszenia. Spróbuj ponownie później.');
                                        header("Location: " . HOST . "moje-konto/moje-ogloszenia/sukces,".$id);
                                    }

                                break;
                            }
                        }
                        $this->template->display('mojeKonto/pracodawca/wznowOgloszenie.tpl');
                    break;
                    case 'oneclick':
                        if($this->model->oneClickFromReserve($url[1], $url[2])) {
                            FlashMsg::add('success', 'Udało Ci się zmienić status kandydata OneClick i przenieść go z listy rezerwowych.');
                            header("Location: " . HOST . "moje-konto/moje-ogloszenia/".$url[1]);
                        } else {
                            FlashMsg::add('warning', 'Nie udało się zaprosić kandydata. Spróbuj ponownie później.');
                            header("Location: " . HOST . "moje-konto/moje-ogloszenia/".$url[1]);
                        }
                    break;
                    case 'odrzuc':
                        if($this->model->oneClickOdrzuc($url[1], $url[2])) {
                            FlashMsg::add('success', 'Udało Ci się zmienić status kandydata OneClick i odrzucić go z listy.');
                            header("Location: " . HOST . "moje-konto/moje-ogloszenia/".$url[1]);
                        } else {
                            FlashMsg::add('warning', 'Nie udało się odrzucić kandydata. Spróbuj ponownie później.');
                            header("Location: " . HOST . "moje-konto/moje-ogloszenia/".$url[1]);
                        }
                    break;
                    default:
                        FlashMsg::add('warning', 'Nie masz uprawnień do zmiany tego ogłoszenia.');
                        header("Location:".HOST."moje-konto/moje-ogloszenia");                           
                    break;
                }
            }
            
        } else {

            if(isset($queryName) && $queryName=='oneclick') {
                $oneclick = $this->model->getOneClick(); 
                #echo"<pre>";print_r($oneclick);echo"</pre>";die();
                $this->template->assign('oneclick', $oneclick);
                
                $this->template->display('mojeKonto/kandydat/mojeOgloszeniaOneClick.tpl'); 
            } else {
                $jobList = $this->model->getUserJobList();
                $favJobList = $this->model->getFav(); 
                
                $this->template->assign('favJobList', $favJobList);
                $this->template->assign('jobList', $jobList);
                $this->template->display('mojeKonto/kandydat/mojeOgloszenia.tpl'); 
            }
        }
    }    
    
    /* 
     * Index action 
     * 
     * PRACODAWCA
     */
    public function moiKandydaciAction($queryName = false) {
        /* Zabezpiecznie */
        if($this->userType!=2) {
            FlashMsg::add('error', 'Nie masz uprawnień do przeglądania tej strony.');
            header("Location: " . HOST . "moje-konto"); 
        }

        if($queryName) {
            $tmp = explode(',', $queryName);
            if($tmp[0]!='nowy' && $tmp[0]!='edytuj-tagi') {
                if(!isset($tmp[1]) || !is_numeric($tmp[1]) || !$this->model->checkUserExists($tmp[1])) {
                    header("Location: " . HOST . "moje-konto/moi-kandydaci");
                }
            }
            switch($tmp[0]) {
                case "dodaj-do-ulubionych":
                    
                    if(count($tmp)!=4) header("Location: " . $_SERVER['HTTP_REFERER']);

                    if(!$this->model->checkOneClick($tmp[1])) {
                        if($this->model->addUserToOneClick($tmp[1], false, false, $tmp[2], $tmp[3])) {
                            FlashMsg::add('success', 'Udało się prawidłowo dodać osobę do ulubionych.');
                            header("Location: " . $_SERVER['HTTP_REFERER']);                         
                        } else {
                            FlashMsg::add('error', 'Nie udało się dodać osoby do ulubionych.');
                            header("Location: " . $_SERVER['HTTP_REFERER']);      
                        }
                    } else {
                        header("Location: " . $_SERVER['HTTP_REFERER']);
                    }
                break;
                case "usun-z-ulubionych":
                    if($this->model->checkOneClick($tmp[1])) {
                        if($this->model->removeUserToOneClick($tmp[1])) {
                            FlashMsg::add('success', 'Udało się prawidłowo usunąć osobę z ulubionych.');
                            header("Location: " . $_SERVER['HTTP_REFERER']);                         
                        } else {
                            FlashMsg::add('error', 'Nie udało się usunąć osoby z ulubionych.');
                            header("Location: " . $_SERVER['HTTP_REFERER']);                        
                        }
                    } else {
                        header("Location: " . $_SERVER['HTTP_REFERER']);
                    }
                break;
                case "przyjmij-kandydata":
                    if($this->model->changeApplicationStatus(true, $tmp[1], $tmp[2])) {
                        FlashMsg::add('success', 'Udało się prawidłowo zmienić status kandydata.');
                        header("Location: " . $_SERVER['HTTP_REFERER']);                         
                    } else {
                        FlashMsg::add('error', 'Nie udało się zmienić statusu kandydata.');
                        header("Location: " . $_SERVER['HTTP_REFERER']);                        
                    }
                break;
                case "odrzuc-kandydata":
                    if($this->model->changeApplicationStatus(false, $tmp[1], $tmp[2])) {
                        FlashMsg::add('success', 'Udało się prawidłowo zmienić status kandydata.');
                        header("Location: " . $_SERVER['HTTP_REFERER']);                         
                    } else {
                        FlashMsg::add('error', 'Nie udało się zmienić statusu kandydata.');
                        header("Location: " . $_SERVER['HTTP_REFERER']);                        
                    }
                break;
                case "one-click":
                    if($this->model->oneClick($tmp[1], $tmp[2])) {
                        FlashMsg::add('success', 'OneClick został uruchomiony!');
                        header("Location: " . $_SERVER['HTTP_REFERER']);                         
                    } else {
                        FlashMsg::add('error', 'Nie udało się uruchomić OneClicka.');
                        header("Location: " . $_SERVER['HTTP_REFERER']);                        
                    }
                break;
                case "nowy":
                    $error = '';
          
                    if($error) {
                        FlashMsg::add('warning', $error);
                        header("Location: " . HOST . "moje-konto/moi-kandydaci");   
                        exit();
                    }
                    
                    $emails = explode(',', $_POST['email']);
                    
                    foreach($emails as $email) {
                        if(!empty($email)) {
                            
                            if(!Form::validEmail($email)) {
                                $error .= "<p>".$email."</p>";
                            }
                            
                            $res = $this->model->newOneClick(trim($email), $_POST['message']);
                        }
                    }

                    FlashMsg::add('success', 'Na podane adresy email zostały wysłane zaproszenia.');
                    header("Location: " . $_SERVER['HTTP_REFERER']);   

                break;
                case "edytuj-tagi":
                    if($this->model->editOneclickTags($_POST['user'], $_POST['tags'])) {
                        FlashMsg::add('success', 'Udało się prawidłowo edytować rodzaje prac.');
                        header("Location: " . $_SERVER['HTTP_REFERER']);        
                    } else {
                        FlashMsg::add('error', 'Niestety nie udało się edytować rodzajów prac.');
                        header("Location: " . HOST . "moje-konto/moi-kandydaci"); 
                    }
                break;
                default:
                    FlashMsg::add('error', 'Nie masz uprawnień do przeglądania tej strony.');
                    header("Location: " . HOST . "moje-konto/moi-kandydaci"); 
                break;
            }
        } else {       
            $oneClickList = $this->model->getOneClickJobList();
            $waiting = $this->model->getWaitingInvitation();
            $tagList = $this->model->getTags();

            $orderedOneClickList = array();
            foreach ($oneClickList as $element) {
                $orderedOneClickList[$element['user_id']] = $element;
            }

            if (count($oneClickList)) {
                $orderedOneClickList = $this->model->attachGroupsToEmployees($this->user, $orderedOneClickList);
            }

            if(isset($_POST['editNote']) && isset($_POST['user']) && isset($_POST['note'])) {
                $_POST = Form::sanitizeArray($_POST);
                if($this->model->editNote($_POST['user'], $_POST['note'])) {
                    FlashMsg::add('success', 'Udało się prawidłowo edytować notatkę.');
                    header("Location: " . HOST . "moje-konto/moi-kandydaci"); 
                } else {
                    FlashMsg::add('error', 'Nie udało się edytować notatki. Proszę spróbować później.');
                    header("Location: " . HOST . "moje-konto/moi-kandydaci"); 
                }
            }

            $this->template->assign('tagList', $tagList);
            $this->template->assign('waiting',$waiting);
            $this->template->assign('oneClickList',$orderedOneClickList);

            $employerGroups = array();
            foreach ($this->model->getOneclickGroup() as $group) {
                $employerGroups[$group['id']] = $group;
            }
            $this->template->assign('employerGroups', $employerGroups);

            $this->template->display('mojeKonto/pracodawca/moiKandydaci.tpl');
        }
    }
    
    /**
     * strona moj kalendarz
     * @param  boolean $queryName 
     * @author ssdelejt@gmail.com
     */
    public function mojKalendarzAction() {
        /* Zabezpiecznie */

        if($this->userType!=1 AND $this->userType!=2) {
            FlashMsg::add('error', 'Nie masz uprawnień do przeglądania tej strony.');
            header("Location: " . HOST . "moje-konto"); 
        }

        // usuwanie
        if(!empty($_POST['act']) AND $_POST['act'] == "del")
        {
            $id = (int) $_POST['id'];
            $check = $this->model->delCalendarData($id);
            if($check==true)
            {
               FlashMsg::add('success', "Wybrany wpis został usunięty");
               header("Location: " . HOST . "moje-konto/moj-kalendarz");  
            }
        }

        // dodawanie
        if(!empty($_POST['act']) AND $_POST['act'] == 'add')
        {
            $data = Form::sanitizeArray($_POST);
            $check = $this->model->addCalendarData($data);
            if($check!==true)
            {
               FlashMsg::add('error', $check);
               header("Location: " . HOST . "moje-konto/moj-kalendarz");  
            }
            else
            {
               FlashMsg::add('success', "Pozycja została dodana");
               header("Location: " . HOST . "moje-konto/moj-kalendarz");                 
            }
        }

        // wypisujemy dane
        $year = 0;
        $month = 0;
        if(!empty($_POST['y']))
        {
            $year = (int)$_POST['y'];
        }
        if(!empty($_POST['m']))
        {
            $month = (int)$_POST['m'];
        }   

        // lista lat w selekcie
        $yearsList = array(date("Y"),date("Y")+1);     

        $userCalendar = $this->model->generateSimpleCalendar($month, $year);
        $this->template->assign('userCalendar',$userCalendar);
        $this->template->assign('month',!empty($month) ? $month : date("n") );
        $this->template->assign('year',!empty($year) ? $year : date("Y"));
        $this->template->assign('yearsList',$yearsList);

        $this->template->assign('userType',$this->userType);

        if($this->userType==1) 
        {            
            $this->template->display('mojeKonto/kandydat/mojKalendarz.tpl');
        }
        elseif($this->userType==2)
        {
            $this->template->display('mojeKonto/pracodawca/mojKalendarz.tpl');
        }
    }   




    /**
     * metoda wykorzystywana do wczytania lista wpisów z danego dnia (za posrednictwem jQuery.post)
     * @return json lista zajęc w postaci tablicy przepuszczonej przez jsona
     * @author ssdelejt@gmail.com
     */
    public function ajaxGetCalendarDataAction()
    {
        $userCalendarByDate = array();
        if(isset($_POST['day']) && isset($_POST['month']) && isset($_POST['year']) )
        {
            $day = str_pad($_POST['day'], 2, "0", STR_PAD_LEFT);
            $month = str_pad($_POST['month'], 2, "0", STR_PAD_LEFT);

            $data = $_POST['year'] ."-";
            $data .= $month . "-";
            $data .= $day;

            if($data == '0000-00-00')
            {
                $data = date("Y-m-d");
            }

            $res = $this->model->getUserCalendarByDate($data);
            if(!empty($res))
            {
                $userCalendarByDate['positions'] = $res;                
            }
            $userCalendarByDate['header'] = $data;
        }
        echo json_encode($userCalendarByDate);
    }


    
    /* 
     * Index action 
     * 
     * KANDYDAT
     */
    public function mojeCvAction($queryName = false) {
        /* Zabezpiecznie */
        if($this->userType!=1) {
            FlashMsg::add('error', 'Nie masz uprawnień do przeglądania tej strony.');
            header("Location: " . HOST . "moje-konto"); 
        }
        if($queryName) {
            $tmp = explode(',', $queryName);
            
            if($tmp[0]!='nowe') {
                if(!isset($tmp[1]) || !is_numeric($tmp[1]) || !$this->model->checkCVExists($tmp[1])) {
                    header("Location: " . HOST . "moje-konto/moje-cv");
                }
            }
            switch($tmp[0]) {
                case "usun":
                    if($this->model->deleteCV($tmp[1])) {
                        FlashMsg::add('success', 'Udało się prawidłowo usunąć CV.');
                        header("Location: " . $_SERVER['HTTP_REFERER']);                         
                    } else {
                        FlashMsg::add('error', 'Nie udało się CV.');
                        header("Location: " . $_SERVER['HTTP_REFERER']);                        
                    }               
                break;
                case "nowe":
                    $error = '';
                    $file = null;
                    
                    if($this->model->checkCountCV()) {

                        if(isset($_FILES['cv']) && $_FILES['cv']['error']==0) {
                            $allowed = array('application/pdf', 'image/jpeg', 'image/gif', 'image/png', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.oasis.opendocument.text');
                            if(!in_array($_FILES['cv']['type'],$allowed)) {
                                $error .= '<p>Przesłane CV musi być formatu pdf, jpg, png, gif lub doc.</p>';
                            }             
                            if($_FILES['cv']['size']>3048000) {
                                $error .= '<p>Przesłane logo waży zbyt dużo (maksymalna waga to 2mb.</p>';
                            }
                            $file = $_FILES['cv']; 
                        } else {
                            $error .= '<p>Proszę wgrać plik CV.</p>';
                        }
                        
                    } else {
                        $error .= '<p>Posiadasz już maksymalną ilość dodanych ręcznie CV. Maksymalnie możesz posiadać 3 CV.</p>';
                    }

                    if(!$error) {
                        $this->model->addCV($file);
                        FlashMsg::add('success', 'Udało się poprawnie dodać CV.');
                        header("Location:".HOST."moje-konto/moje-cv");       
                    } else {
                        FlashMsg::add('error', $error);
                        header("Location:".HOST."moje-konto/moje-cv"); 
                    }              
                break;
                default:
                    FlashMsg::add('error', 'Nie masz uprawnień do przeglądania tej strony.');
                    header("Location: " . HOST . "moje-konto/moi-kandydaci"); 
                break;
            }
        } else {     
            $cvList = $this->model->getCVList();

            $this->template->assign('cvList', $cvList);
            $this->template->display('mojeKonto/kandydat/mojeCv.tpl');
        }
    }    
    
    /* 
     * Index action 
     * 
     * KANDYDAT
     */
    public function usunAction($queryName = false) {
       if($this->model->deleteAccount()) {
            FlashMsg::add('success', 'Nie udało Ci się usunąć konta.');
            header("Location: " . HOST . "moje-konto");     
       } else {
            FlashMsg::add('error', 'Nie udało Ci się usunąć konta.');
            header("Location: " . HOST . "moje-konto"); 
       }
    }    
    
    /* 
     * ADMINISTRATOR
     */
    public function tagiAction($queryName = false) {
        /* Zabezpiecznie */
        if($this->userType!=5) {
            FlashMsg::add('error', 'Nie masz uprawnień do przeglądania tej strony.');
            header("Location: " . HOST . "moje-konto"); 
        }
        if($queryName) {
            $tmp = explode(',', $queryName);
            
            if(!isset($tmp[1]) || !is_numeric($tmp[1]) || !$this->model->checkTagExists($tmp[1])) {
                header("Location: " . HOST . "moje-konto/tagi");
            }
            
            switch($tmp[0]) {
                case "akceptuj":
                    if($this->model->tagAccept($tmp[1])) {
                        FlashMsg::add('success', 'Udało się prawidłowo zaakceptować tag.');
                        header("Location: " . $_SERVER['HTTP_REFERER']);                         
                    } else {
                        FlashMsg::add('error', 'Nie udało się zaakceptować tagu.');
                        header("Location: " . $_SERVER['HTTP_REFERER']);                        
                    }               
                break;
                case "usun":
                    if($this->model->usunAccept($tmp[1])) {
                        FlashMsg::add('success', 'Udało się prawidłowo usunąć tag.');
                        header("Location: " . $_SERVER['HTTP_REFERER']);                         
                    } else {
                        FlashMsg::add('error', 'Nie udało się usunąć tagu.');
                        header("Location: " . $_SERVER['HTTP_REFERER']);                        
                    }               
                break;
                default: 
                    header("Location: " . HOST . "moje-konto/tagi");
                break;
            }
        } else {
            $tagsActive = $this->model->getTags();
            $tagsNotActive = $this->model->getTags(true);

            $this->template->assign('tagsActive', $tagsActive);
            $this->template->assign('tagsNotActive', $tagsNotActive);
            $this->template->display('mojeKonto/administrator/tagi.tpl');
        }
    }    
    
    /* 
     * ADMINISTRATOR
     */
    public function rabatyAction($queryName = false) {
        /* Zabezpiecznie */
        if($this->userType!=5) {
            FlashMsg::add('error', 'Nie masz uprawnień do przeglądania tej strony.');
            header("Location: " . HOST . "moje-konto"); 
        }
        if($queryName) {
            $tmp = explode(',', $queryName);

            switch($tmp[0]) {
                case "generuj":
                    if(!isset($_POST['nazwa']) || !isset($_POST['procent']) || !isset($_POST['count']) || !isset($_POST['howmany'])) header("Location: " . HOST . "moje-konto/tagi");
                    
                    $error = '';
                    
                    if(empty($_POST['nazwa'])) {
                        $error .= '<p>- proszę podać nazwę rabatu</p>';
                    }
                    
                    if(empty($_POST['procent'])) {
                        $error .= '<p>- proszę wybrać wartość rabatu</p>';
                    } elseif($_POST['procent']!=(10 || 25 || 50 || 100)) {
                        $error .= '<p>- proszę wybrać poprawną wartość rabatu</p>';
                    }
                    
                    if(empty($_POST['count'])) {
                        $error .= '<p>- proszę wpisać ilość wykorzystań rabatu</p>';
                    } elseif(!is_numeric($_POST['count'])) {
                        $error .= '<p>- proszę wpisać poprawną ilość wykorzystań rabatu</p>';
                    }
                    
                     if(empty($_POST['howmany'])) {
                        $error .= '<p>- proszę wpisać ilość wygenerowanych rabatów</p>';
                    } elseif(!is_numeric($_POST['howmany'])) {
                        $error .= '<p>- proszę wpisać poprawną ilość wygenerowanych rabatów</p>';
                    }
                    
                    if(!$error) {
                        if($_POST['howmany']>1) {
                            $res = '';
                            for($i = 1; $i <= $_POST['howmany']; $i++) {
                                $name = $_POST['nazwa'] . ' #'.$i;
                                $res .= $this->model->newRabat($name, $_POST);
                            }
                            FlashMsg::add('success', '<p>Udało się prawidłowo wygenerować '.$_POST['howmany'].' rabatów</p><p>'.$res.'</p>');
                            header("Location: " . HOST . "moje-konto/rabaty");    
                        } else {
                            $res = $this->model->newRabat($_POST['nazwa'], $_POST);
                            FlashMsg::add('success', '<p>Udało się prawidłowo wygenerować rabat</p><p>'.$res.'</p>');
                            header("Location: " . HOST . "moje-konto/rabaty");      
                        }
                    } else {
                        FlashMsg::add('error', $error);
                        header("Location: " . HOST . "moje-konto/rabaty");      
                    }                    
                break;
                default: 
                    header("Location: " . HOST . "moje-konto/rabaty");
                break;
            }
        } else {
            $kodyRabatowe = $this->model->getRabaty();

            $this->template->assign('kodyRabatowe', $kodyRabatowe);
            $this->template->display('mojeKonto/administrator/kodyRabatowe.tpl');
        }
    }
    
        
    /* 
     * ZNAJOMI
     */
    public function znajomiAction($queryName = false) {
        /* Zabezpiecznie */
        if($this->userType!=1) {
            FlashMsg::add('error', 'Nie masz uprawnień do przeglądania tej strony.');
            header("Location: " . HOST . "moje-konto"); 
        }
        
        // Usuwanie
        if($queryName) {
            $exp = explode(',', $queryName);
            
            if($exp[0] && is_numeric($exp[1])) {
                switch($exp[0]) {
                    case 'usun':
                        if($this->model->deleteFriend($exp[1])) {
                            FlashMsg::add('success', 'Udało Ci się usunąć znajomego.');
                            header("Location: " . HOST . "moje-konto/znajomi");
                        } else {
                            FlashMsg::add('error', 'Nie udało się usunąć znajomego.');
                            header("Location: " . HOST . "moje-konto/znajomi");
                        }
                    break;
                    case 'akceptacja':
                        if($this->model->acceptFriend($exp[1])) {
                            FlashMsg::add('success', 'Udało Ci się zaakceptować znajomość.');
                            header("Location: " . HOST . "moje-konto/znajomi");
                        } else {
                            FlashMsg::add('error', 'Nie udało się zaakceptować znajomości.');
                            header("Location: " . HOST . "moje-konto/znajomi");
                        }
                    break;
                    case 'odrzuc':
                        if($this->model->refuseFriend($exp[1])) {
                            FlashMsg::add('success', 'Udało Ci się odrzucić znajomość.');
                            header("Location: " . HOST . "moje-konto/znajomi");
                        } else {
                            FlashMsg::add('error', 'Nie udało się odrzucić znajomości.');
                            header("Location: " . HOST . "moje-konto/znajomi");
                        }
                    break;
                    default:
                        header("Location: " . HOST . "moje-konto/znajomi");
                    break;
                }
            } else {
                header("Location: " . HOST . "moje-konto/znajomi"); 
            }

        }
        // Friend Invitation
        if(isset($_POST['inviteFriend']) && $_POST['inviteFriend']) {
            $error = '';
            if(empty($_POST['friendEmail'])) {
                $error .= '<p>- Proszę wpisać adres email.</p>';
            } elseif(!Form::validEmail($_POST['friendEmail'])) {
                $error .= '<p>- Proszę wpisać poprawny adres email.</p>';
            }
            if($this->model->checkFriend($_POST['friendEmail'])) {
                $error .= '<p>- Ten użytkownik jest już na Twojej liście znajomych.</p>';
            }
            if(!$error) {
                if($this->model->inviteFriend($_POST['friendEmail'])) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie wysłać zaproszenie.');
                    header("Location: " . HOST . "moje-konto/znajomi");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się poprawnie wysłać zaproszenie.');
                    header("Location: " . HOST . "moje-konto/znajomi");
                }
            } else {
                $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
            }
        }
            
        $friends = $this->model->getFriendsList();
        
        $this->template->assign('friends', $friends);
        $this->template->display('mojeKonto/kandydat/znajomi.tpl');
    }

        
    public function ajaxAction($queryName = false) {
        if(isset($_POST['favJob']) && isset($_POST['id'])) {
            if($_POST['favJob']) {
                $this->model->favJob();
            } else {
                $this->model->unfavJob();
            }
        } else {
            return false;
        }
    }
    
    public function edytujAction($queryName = false) {
        /* Zabezpiecznie */
        if($this->userType!=1) {
            FlashMsg::add('error', 'Nie masz uprawnień do przeglądania tej strony.');
            header("Location: " . HOST . "moje-konto"); 
        }
        
        if(isset($_POST['changeCVBG']) && isset($_POST['id']) && isset($_POST['color'])) {
            $this->model->changeCvBg($_POST['id'], $_POST['color']);
            die();
        }        
        
        if(isset($_POST['changeCVColor']) && isset($_POST['id']) && isset($_POST['color'])) {
            $this->model->changeCvColor($_POST['id'], $_POST['color']);
            die();
        }

        $profil = $this->model->getEmployeeProfile();
        $tagList = $this->model->getTags();
        $userTags = $this->model->getUserTags();    
        $userCalendar = $this->model->generateSimpleCalendar();

        $userExperience = $this->model->getExperience($profil['user_id']);
        $this->template->assign('userExperience', $userExperience);
                
        $userEducation = $this->model->getEducation($profil['user_id']);
        $this->template->assign('userEducation', $userEducation);
                        
        $userLangs = $this->model->getLang($profil['user_id']);
        $this->template->assign('userLangs', $userLangs);
                                
        $userTraining = $this->model->getTraining($profil['user_id']);
        $this->template->assign('userTraining', $userTraining);
                                        
        $userSkill = $this->model->getSkill($profil['user_id']);
        $this->template->assign('userSkill', $userSkill);
                                                
        $userOtherSkill = $this->model->getOtherSkill($profil['user_id']);
        $this->template->assign('userOtherSkill', $userOtherSkill);
        
        $this->template->assign('tagList',$tagList);
        $this->template->assign('userTags',$userTags);
        $this->template->assign('userProfile', $profil);
        $this->template->assign('userCalendar', $userCalendar);

        $int = 0;
        
        if($userExperience) $int++;
        if($userEducation) $int++;
        if($userLangs) $int++;
        if($userTraining) $int++;
        if($userOtherSkill) $int++;

        $percent = ceil(($int/5)*100);
        $this->template->assign('completePercent', $percent);

        // Kolorki
        $userCVColorsTmp = $this->model->getUserCVColors();
        $userCVColors = explode(',',$userCVColorsTmp['user_cv_color']);
        $userBgCVColors = explode(',',$userCVColorsTmp['user_bgcv_color']);
        
        #print_r($userCVColorsTmp);
        
        $this->template->assign('userCVColors', $userCVColors);
        $this->template->assign('userBgCVColors', $userBgCVColors);
        
        //
        // Edycja danych podstawowych
        //
        
        if(isset($_POST['edit-basic']) && $_POST['edit-basic']) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);

            if(isset($_FILES['photo']) && $_FILES['photo']['error']==0) {
                $imgInfo = getimagesize($_FILES['photo']['tmp_name']);

                if(!($imgInfo['mime']=='image/jpeg' || $imgInfo['mime']=='image/png' || $imgInfo['mime']=='image/gif')) {
                    $error .= '<p>- przesłane zdjęcie profilowe musi być formatu png, jpg lub gif.</p>';
                    $errorArray['file2'] = true;
                }

                if($_FILES['photo']['size']>1048576) {
                    $error .= '<p>- przesłane zdjęcie profilowe waży zbyt dużo (maksymalna waga to 1mb).</p>';
                    $errorArray['file2'] = true;
                }

                $file = $_FILES['photo'];
            } elseif(isset($_FILES['photo']) && $_FILES['photo']['error']==1) { 
                $error .= '<p>- przesłane zdjęcie profilowe waży zbyt dużo (maksymalna waga to 1mb).</p>';
                $errorArray['file2'] = true;
            } else {
                $file = 0;
            }
            
            if(isset($_POST['trash-file']) && $_POST['trash-file']) {
                $file = '-1';
            }

            if(!$error) {
                if($this->model->updateEmployee($_POST, $file)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie edytować swój profil.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się edytować profilu. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
            }            
        }
        
        //
        // Usuwanie
        //
        
        if(isset($queryName) && !empty($queryName)) {
            $exp = explode(',',$queryName);
            
            if(count($exp)!=2) header("Location: " . HOST . "moje-konto/edytuj");
            if(!is_numeric($exp[1])) header("Location: " . HOST . "moje-konto/edytuj");
                
            switch($exp[0]) {
                case 'usun-doswiadczenie':
                    if($this->model->deleteExperience($exp[1])) {
                        FlashMsg::add('success', 'Udało Ci się poprawnie usunąć doświadczenie.');
                        header("Location: " . HOST . "moje-konto/edytuj");
                    } else {
                        FlashMsg::add('error', 'Nie udało Ci się usunąć doświadczenia. Spróbuj ponownie później.');
                        header("Location: " . HOST . "moje-konto/edytuj");
                    }
                break;
                case 'usun-wyksztalcenie':
                    if($this->model->deleteEducation($exp[1])) {
                        FlashMsg::add('success', 'Udało Ci się poprawnie usunąć wykształcenie.');
                        header("Location: " . HOST . "moje-konto/edytuj");
                    } else {
                        FlashMsg::add('error', 'Nie udało Ci się usunąć wykształcenia. Spróbuj ponownie później.');
                        header("Location: " . HOST . "moje-konto/edytuj");
                    }
                break;
                case 'usun-jezyk':
                    if($this->model->deleteLang($exp[1])) {
                        FlashMsg::add('success', 'Udało Ci się poprawnie usunąć język.');
                        header("Location: " . HOST . "moje-konto/edytuj");
                    } else {
                        FlashMsg::add('error', 'Nie udało Ci się usunąć języku. Spróbuj ponownie później.');
                        header("Location: " . HOST . "moje-konto/edytuj");
                    }
                break;
                case 'usun-szkolenie':
                    if($this->model->deleteTraining($exp[1])) {
                        FlashMsg::add('success', 'Udało Ci się poprawnie usunąć szkolenie.');
                        header("Location: " . HOST . "moje-konto/edytuj");
                    } else {
                        FlashMsg::add('error', 'Nie udało Ci się usunąć szkolenia. Spróbuj ponownie później.');
                        header("Location: " . HOST . "moje-konto/edytuj");
                    }
                break;
                case 'usun-umiejetnosc':
                    if($this->model->deleteOtherSkill($exp[1])) {
                        FlashMsg::add('success', 'Udało Ci się poprawnie usunąć umiejętność.');
                        header("Location: " . HOST . "moje-konto/edytuj");
                    } else {
                        FlashMsg::add('error', 'Nie udało Ci się usunąć umiejętności. Spróbuj ponownie później.');
                        header("Location: " . HOST . "moje-konto/edytuj");
                    }
                break;
                default:
                    header("Location: " . HOST . "moje-konto/edytuj");
                break;
            }
        }
        
        //
        // Dodawnie doświadczenia
        //
        
        if(isset($_POST['doswiadczenie']) && !isset($_POST['id'])) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);

            if(!isset($_POST['od']) || empty($_POST['od'])) {
                $error .= '<p>- proszę wpisać datę rozpoczęcia pracy</p>';
                $errorArray['od'] = true;
            } elseif(!Form::validDate($_POST['od'])) {
                $error .= '<p>- proszę wpisać poprawną datę rozpoczęcia pracy</p>';
                $errorArray['od'] = true;
            }
            
            if(!isset($_POST['teraz']) || !$_POST['teraz']) {
                if(!isset($_POST['do']) || empty($_POST['do'])) {
                    $error .= '<p>- proszę wpisać datę zakończenia pracy</p>';
                    $errorArray['do'] = true;
                } elseif(!isset($_POST['teraz']) && !Form::validDate($_POST['do'])) {
                    $error .= '<p>- proszę wpisać poprawną datę zakończenia pracy</p>';
                    $errorArray['do'] = true;
                }
            }
            
            if(!isset($_POST['teraz']) || !$_POST['teraz']) {
                if(!isset($errorArray['od']) && !isset($errorArray['do'])) {
                    if(strtotime($_POST['od']) > strtotime($_POST['do'])) {
                        $error = '<p>- data zakończenia pracy nie może być mniejsza niż data rozpoczęcia pracy</p>';
                        $errorArray['od'] = true; 
                        $errorArray['do'] = true; 
                    }
                }
            }
                        
            if(!isset($_POST['stanowisko']) || empty($_POST['stanowisko'])) {
                $error .= '<p>- proszę wpisać obejmowane stanowisko</p>';
                $errorArray['stanowisko'] = true;
            }   
            
            if(!isset($_POST['firma']) || empty($_POST['firma'])) {
                $error .= '<p>- proszę wpisać w jakiej firmie</p>';
                $errorArray['firma'] = true;
            }   
            
            if(!isset($_POST['opis']) || empty($_POST['opis'])) {
                $error .= '<p>- proszę wpisać opis obejmowanego stanowisa</p>';
                $errorArray['opis'] = true;
            }
            
            if(!$error) {
                if($this->model->addExperience($_POST)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie dodać nowe doświadczenie.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się dodać doświadczenia. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>Podczas dodawania doświadczenia wystąpiły błędy:</strong></p>'.$error);
            } 
        }
       
        //
        // Edytowanie doświadczenia
        //
        
        if(isset($_POST['doswiadczenie']) && isset($_POST['id']) && is_numeric($_POST['id'])) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);

            if(!isset($_POST['od']) || empty($_POST['od'])) {
                $error .= '<p>- proszę wpisać datę rozpoczęcia pracy</p>';
                $errorArray['od'] = true;
            } elseif(!Form::validDate($_POST['od'])) {
                $error .= '<p>- proszę wpisać poprawną datę rozpoczęcia pracy</p>';
                $errorArray['od'] = true;
            }
            
            if(!isset($_POST['teraz']) || !$_POST['teraz']) {
                if(!isset($_POST['do']) || empty($_POST['do'])) {
                    $error .= '<p>- proszę wpisać datę zakończenia pracy</p>';
                    $errorArray['do'] = true;
                } elseif(!isset($_POST['teraz']) && !Form::validDate($_POST['do'])) {
                    $error .= '<p>- proszę wpisać poprawną datę zakończenia pracy</p>';
                    $errorArray['do'] = true;
                }
            }
            
            if(!isset($_POST['teraz']) || !$_POST['teraz']) {
                if(!isset($errorArray['od']) && !isset($errorArray['do'])) {
                    if(strtotime($_POST['od']) > strtotime($_POST['do'])) {
                        $error = '<p>- data zakończenia pracy nie może być mniejsza niż data rozpoczęcia pracy</p>';
                        $errorArray['od'] = true; 
                        $errorArray['do'] = true; 
                    }
                }
            }
                        
            if(!isset($_POST['stanowisko']) || empty($_POST['stanowisko'])) {
                $error .= '<p>- proszę wpisać obejmowane stanowisko</p>';
                $errorArray['stanowisko'] = true;
            }   
            
            if(!isset($_POST['firma']) || empty($_POST['firma'])) {
                $error .= '<p>- proszę wpisać w jakiej firmie</p>';
                $errorArray['firma'] = true;
            }   
            
            if(!isset($_POST['opis']) || empty($_POST['opis'])) {
                $error .= '<p>- proszę wpisać opis obejmowanego stanowisa</p>';
                $errorArray['opis'] = true;
            }
            
            if(!$error) {
                if($this->model->editExperience($_POST)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie dodać nowe doświadczenie.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się dodać doświadczenia. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>Podczas dodawania doświadczenia wystąpiły błędy:</strong></p>'.$error);
            } 
        }
        

        //
        // Dodawnie wykształcenia
        //
        
        if(isset($_POST['wyksztalcenie']) && !isset($_POST['id'])) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);

            if(!isset($_POST['od']) || empty($_POST['od'])) {
                $error .= '<p>- proszę wpisać datę rozpoczęcia szkoły</p>';
                $errorArray['od'] = true;
            } elseif(!Form::validDate($_POST['od'])) {
                $error .= '<p>- proszę wpisać poprawną datę rozpoczęcia szkoły</p>';
                $errorArray['od'] = true;
            }
            
            if(!isset($_POST['teraz']) || !$_POST['teraz']) {
                if(!isset($_POST['do']) || empty($_POST['do'])) {
                    $error .= '<p>- proszę wpisać datę zakończenia szkoły</p>';
                    $errorArray['do'] = true;
                } elseif(!Form::validDate($_POST['do'])) {
                    $error .= '<p>- proszę wpisać poprawną datę zakończenia szkoły</p>';
                    $errorArray['do'] = true;
                }
            }
            
            if(!isset($_POST['teraz']) || !$_POST['teraz']) {
                if(!isset($errorArray['od']) && !isset($errorArray['do'])) {
                    if(strtotime($_POST['od']) > strtotime($_POST['do'])) {
                        $error .= '<p>- data zakończenia szkoły nie może być mniejsza niż data rozpoczęcia szkoły</p>';
                        $errorArray['od'] = true; 
                        $errorArray['do'] = true; 
                    }
                }
            }
                        
            if(!isset($_POST['szkola']) || empty($_POST['szkola'])) {
                $error .= '<p>- proszę wpisać nazwę ukończonej szkoły</p>';
                $errorArray['szkola'] = true;
            }   

            if(!$error) {
                if($this->model->addEducation($_POST)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie dodać wykształcenie.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się dodać wykształcenia. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>Podczas dodawania wykształcenia wystąpiły błędy:</strong></p>'.$error);
            } 
        }
       
        //
        // Edytowanie wykształcenia
        //
        
        if(isset($_POST['wyksztalcenie']) && isset($_POST['id']) && is_numeric($_POST['id'])) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);

            if(!isset($_POST['od']) || empty($_POST['od'])) {
                $error .= '<p>- proszę wpisać datę rozpoczęcia szkoły</p>';
                $errorArray['od'] = true;
            } elseif(!Form::validDate($_POST['od'])) {
                $error .= '<p>- proszę wpisać poprawną datę rozpoczęcia szkoły</p>';
                $errorArray['od'] = true;
            }
            
            if(!isset($_POST['do']) || empty($_POST['do'])) {
                $error .= '<p>- proszę wpisać datę zakończenia szkoły</p>';
                $errorArray['do'] = true;
            } elseif(!Form::validDate($_POST['do'])) {
                $error .= '<p>- proszę wpisać poprawną datę zakończenia szkoły</p>';
                $errorArray['do'] = true;
            }
            
            if(!isset($errorArray['od']) && !isset($errorArray['do'])) {
                if(strtotime($_POST['od']) > strtotime($_POST['do'])) {
                    $error .= '<p>- data zakończenia szkoły nie może być mniejsza niż data rozpoczęcia szkoły</p>';
                    $errorArray['od'] = true; 
                    $errorArray['do'] = true; 
                }
            }
                        
            if(!isset($_POST['szkola']) || empty($_POST['szkola'])) {
                $error .= '<p>- proszę wpisać nazwę ukończonej szkoły</p>';
                $errorArray['szkola'] = true;
            }   

            if(!$error) {
                if($this->model->editEducation($_POST)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie edytować wykształcenie.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się edytować wykształcenia. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>Podczas edytowania wykształcenia wystąpiły błędy:</strong></p>'.$error);
            } 
        }

        //
        // Dodawnie języka
        //
        
        if(isset($_POST['jezyki']) && !isset($_POST['id'])) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);
         
            if(!isset($_POST['jezyk']) || empty($_POST['jezyk'])) {
                $error .= '<p>- proszę wpisać nazwę języka</p>';
                $errorArray['szkola'] = true;
            }   
            
            if(!isset($_POST['poziom']) || empty($_POST['poziom'])) {
                $error .= '<p>- proszę wpisać poziom języka</p>';
                $errorArray['kierunek'] = true;
            }   

            if(!$error) {
                if($this->model->addLang($_POST)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie dodać język.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się dodać języku. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>Podczas dodawania języka wystąpiły błędy:</strong></p>'.$error);
            } 
        }
       
        //
        // Edytowanie języka
        //
        
        if(isset($_POST['jezyki']) && isset($_POST['id']) && is_numeric($_POST['id'])) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);
      
            if(!isset($_POST['jezyk']) || empty($_POST['jezyk'])) {
                $error .= '<p>- proszę wpisać nazwę języka</p>';
                $errorArray['szkola'] = true;
            }   
            
            if(!isset($_POST['poziom']) || empty($_POST['poziom'])) {
                $error .= '<p>- proszę wpisać poziom języka</p>';
                $errorArray['kierunek'] = true;
            }   
            
            if(!$error) {
                if($this->model->editLang($_POST)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie edytować języku.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się edytować języku. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>Podczas edytowania języka wystąpiły błędy:</strong></p>'.$error);
            } 
        }

        //
        // Dodawnie szkolenia
        //
        
        if(isset($_POST['szkolenia']) && !isset($_POST['id'])) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);

            if(!isset($_POST['koniec']) || empty($_POST['koniec'])) {
                $error .= '<p>- proszę wpisać datę zakończenia szkolenia</p>';
                $errorArray['koniec'] = true;
            } elseif(!Form::validDate($_POST['koniec'])) {
                $error .= '<p>- proszę wpisać poprawną datę zakończenia szkolenia</p>';
                $errorArray['koniec'] = true;
            }

            if(!isset($_POST['nazwa']) || empty($_POST['nazwa'])) {
                $error .= '<p>- proszę wpisać nazwę szkolenia</p>';
                $errorArray['nazwa'] = true;
            }   

            if(!$error) {
                if($this->model->addTraining($_POST)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie dodać szkolenie.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się dodać szkolenia. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>Podczas dodawania szkolenia wystąpiły błędy:</strong></p>'.$error);
            } 
        }
       
        //
        // Edytowanie szkolenia
        //
        
        if(isset($_POST['szkolenia']) && isset($_POST['id']) && is_numeric($_POST['id'])) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);
            
            if(!isset($_POST['koniec']) || empty($_POST['koniec'])) {
                $error .= '<p>- proszę wpisać datę zakończenia szkolenia</p>';
                $errorArray['koniec'] = true;
            } elseif(!Form::validDate($_POST['koniec'])) {
                $error .= '<p>- proszę wpisać poprawną datę zakończenia szkolenia</p>';
                $errorArray['koniec'] = true;
            }

            if(!isset($_POST['nazwa']) || empty($_POST['nazwa'])) {
                $error .= '<p>- proszę wpisać nazwę szkolenia</p>';
                $errorArray['nazwa'] = true;
            }   

            if(!$error) {
                if($this->model->editTraining($_POST)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie edytować szkolenie.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się edytować szkolenia. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>Podczas edytowania szkolenia wystąpiły błędy:</strong></p>'.$error);
            } 
        }

        //
        // Dodawnie umiejętności
        //
        
        if(isset($_POST['umiejetnosci']) && !isset($_POST['id'])) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);
         
            if(!isset($_POST['nazwa']) || empty($_POST['nazwa'])) {
                $error .= '<p>- proszę wpisać nazwę umiejętności</p>';
                $errorArray['nazwa'] = true;
            }   

            if(!$error) {
                if($this->model->addOtherSkill($_POST)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie dodać umiejetność.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się dodać umiejętności. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>Podczas dodawania umiejętności wystąpiły błędy:</strong></p>'.$error);
            } 
        }
       
        //
        // Edytowanie umiejętności
        //
        
        if(isset($_POST['umiejetnosci']) && isset($_POST['id']) && is_numeric($_POST['id'])) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);
         
            if(!isset($_POST['nazwa']) || empty($_POST['nazwa'])) {
                $error .= '<p>- proszę wpisać nazwę umiejętności</p>';
                $errorArray['nazwa'] = true;
            }   
            
            if(!$error) {
                if($this->model->editOtherSkill($_POST)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie edytować umiejetność.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się edytować umiejetności. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>Podczas edytowania umiejetności wystąpiły błędy:</strong></p>'.$error);
            } 
        }
        
        //
        // Hobby
        //
        
        if(isset($_POST['zainteresowania'])) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);

            if(!isset($_POST['hobby']) || empty($_POST['hobby'])) {
                $error .= '<p>- proszę wpisać swoje zainteresowania</p>';
                $errorArray['hobby'] = true;
            }   

            if(!$error) {
                if($this->model->saveHobby($_POST)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie zapisać zainteresowania.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się zapisać zainteresowań. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto/edytuj");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>Podczas zapisywania zainteresowań wystąpiły błędy:</strong></p>'.$error);
            } 
        }
        
        //
        // Uprawnienia
        //
        
        if(isset($_POST['uprawnienia'])) {
            $error = '';
            $errorArray = array();
            
            // Validation
            $_POST = Form::sanitizeArray($_POST);          
            
            if($this->model->saveSkill($_POST)) {
                FlashMsg::add('success', 'Udało Ci się poprawnie zapisać uprawnienia.');
                header("Location: " . HOST . "moje-konto/edytuj");
            } else {
                FlashMsg::add('error', 'Nie udało Ci się zapisać uprawnień. Spróbuj ponownie później.');
                header("Location: " . HOST . "moje-konto/edytuj");
            }

        }

        $this->template->display('mojeKonto/kandydat/edytuj.tpl');
    }
    
    function oneclickAction($queryname = false) {

        if($this->userType==2) {

            if($queryname && !is_numeric($queryname)) {
                $exp = explode(',', $queryname);
                switch($exp[0]) {
                    case 'nowe':
                        $oneclick = $this->model->getOneClickUsers();
                        $this->template->assign('oneclick', $oneclick);
                        
                        $groups = $this->model->getOneclickGroup();
                        $this->template->assign('groups', $groups);

                        if(isset($_POST['oneclick'])) {
                            $error = '';
                            $errorArray = array();

                            // Validation
                            $_POST = Form::sanitizeArray($_POST);

                            if(empty($_POST['position'])) {
                                $error .= '<p>- Proszę wpisać nazwę powiadomienia.</p>';
                                $errorArray['position'] = true;
                            } elseif(strlen($_POST['position']) < 3 || strlen($_POST['position']) > 120) {
                                $error .= '<p>- Nazwa powiadomienia musi mieć od 3 do 120 znaków.</p>';
                                $errorArray['position'] = true;
                            }

                            if(empty($_POST['vacancies'])) {
                                $error .= '<p>- Proszę wpisać liczbę wakatów.</p>';
                                $errorArray['vacancies'] = true;
                            } elseif(!is_numeric($_POST['vacancies'])) {
                                $error .= '<p>- Ilość wakatów musibyć liczbą.</p>';
                                $errorArray['vacancies'] = true;
                            }           

                            if(empty($_POST['date'])) {
                                $error .= '<p>- Proszę wpisać datę rozpoczęcia pracy.</p>';
                                $errorArray['date'] = true;
                            }

                            # stawka
                            if(isset($_POST['rate_min'])) {
                                (float) $_POST['rate_min'] = floatval($_POST['rate_min']);
                            }
                            if(isset($_POST['rate_max'])) {
                                (float) $_POST['rate_max'] = floatval($_POST['rate_max']);
                            }
                            if(isset($_POST['rate'])) {
                                (float) $_POST['rate'] = floatval($_POST['rate']);
                            }
                            if(isset($_POST['rateMonth'])) {
                                (float) $_POST['rateMonth'] = floatval($_POST['rateMonth']);
                            }

                            if(empty($_POST['paymant'])) {
                                $error .= '<p>- Proszę wybrać stawkę.</p>';
                                $errorArray['paymant'] = true;
                            } elseif($_POST['paymant']=='stawka godzinowa') {
                                if((!isset($_POST['rate_min']) || !isset($_POST['rate_max']) || empty($_POST['rate_min']) || empty($_POST['rate_max'])) && (!isset($_POST['rate']) || empty($_POST['rate']))) {
                                    $error .= '<p>- Proszę podać stawkę.</p>';
                                    $errorArray['rate_min'] = true; 
                                    $errorArray['rate_max'] = true; 
                                    $errorArray['rate'] = true;  
                                } elseif(!is_float($_POST['rate_min']) || !is_float($_POST['rate_max']) || !is_float($_POST['rate'])){ 
                                    $error .= '<p>- Proszę podać prawidłową wartość stawki minimalnej i maksymalnej.</p>';
                                    $errorArray['rate_min'] = true; 
                                    $errorArray['rate_max'] = true;   
                                    $errorArray['rate'] = true;  
                                } elseif($_POST['rate_min']>$_POST['rate_max']) {
                                    $error .= '<p>- Kwota maksymalna nie może być mniejsza od kwoty minimalnej..</p>';
                                    $errorArray['rate_min'] = true; 
                                    $errorArray['rate_max'] = true;     
                                }
                            } elseif($_POST['paymant']=='stawka miesięczna') {
                                if(!isset($_POST['rateMonth']) || empty($_POST['rateMonth'])) {
                                    $error .= '<p>- Proszę podać stawkę miesięczną.</p>';
                                    $errorArray['rateMonth'] = true; 
                                } elseif(!is_float($_POST['rateMonth'])){ 
                                    $error .= '<p>- Proszę podać prawidłową wartość stawki miesięcznej.</p>';
                                    $errorArray['rateMonth'] = true;   
                                }
                            }

                            if(!isset($_POST['oneclick_info']) || empty($_POST['oneclick_info'])) {
                                $error .= '<p>- proszę wpisać informacje dla kandydatów</p>';
                                $errorArray['oneclick_info'] = true;
                            }

                            if (empty($_POST['oneclick']) || !is_array($_POST['oneclick']) && empty($_POST['groups']) && empty($_POST['tags'])) {
                                $error .= '<p>- proszę wybrać kandydatów do powiadomienia</p>';
                                $errorArray['users'] = true;
                            }

                            if(!$error) {
                                if($this->model->powiadomieniaOnclick($_POST)) {
                                    FlashMsg::add('success', 'Udało Ci się poprawnie wysłać.');
                                    header("Location: " . HOST . "moje-konto/oneclick");
                                } else {
                                    FlashMsg::add('error', 'Nie udało Ci się zapisać zainteresowań. Spróbuj ponownie później.');
                                    header("Location: " . HOST . "moje-konto/oneclick");
                                }
                            } else {
                                $this->template->assign('errorArray', $errorArray);
                                $this->template->assign('error', '<p><strong>Podczas wysyłania oferty OneClick wystąpiły błędy:</strong></p>'.$error);
                            } 
                        }

                        $this->template->display('mojeKonto/pracodawca/oneclickPowiadomienie.tpl');
                    break;
                    case "odrzuc-kandydata":
                        if($this->model->oneClickOdrzuc($exp[1], $exp[2], true)) {
                            FlashMsg::add('success', 'Udało się prawidłowo zmienić status kandydata.');
                            header("Location: " . $_SERVER['HTTP_REFERER']);                         
                        } else {
                            FlashMsg::add('error', 'Nie udało się zmienić statusu kandydata.');
                            header("Location: " . $_SERVER['HTTP_REFERER']);                        
                        }
                    break;
                    case "akceptuj-kandydata":
                        if(isset($exp[3])) {
                            if($this->model->oneClick($exp[1], $exp[2], true, true)) {
                                FlashMsg::add('success', 'OneClick został uruchomiony!');
                                header("Location: " . $_SERVER['HTTP_REFERER']);                         
                            } else {
                                FlashMsg::add('error', 'Nie udało się uruchomić OneClicka.');
                                header("Location: " . $_SERVER['HTTP_REFERER']);                        
                            }
                        } else {
                            if($this->model->oneClick($exp[1], $exp[2], true)) {
                                FlashMsg::add('success', 'OneClick został uruchomiony!');
                                header("Location: " . $_SERVER['HTTP_REFERER']);                         
                            } else {
                                FlashMsg::add('error', 'Nie udało się uruchomić OneClicka.');
                                header("Location: " . $_SERVER['HTTP_REFERER']);                        
                            } 
                        }
                    break;
                    case "copy":
                        
                        if(!isset($exp[1]) || !is_numeric($exp[1])) header("Location: " . HOST . "moje-konto/oneclick");
                        
                        $oneclick = $this->model->getOneClickUsers();
                        $this->template->assign('oneclick', $oneclick);                        
                        
                        $groups = $this->model->getOneclickGroup();
                        $this->template->assign('groups', $groups);
                        
                        $powiadomienie = $this->model->getPowiadomienieOneclickWithUsers($exp[1]);
                        $this->template->assign('powiadomienie', $powiadomienie);

                        if(!$powiadomienie) header("Location: " . HOST . "moje-konto/oneclick");

                        if(isset($_POST['oneclick'])) {
                            $error = '';
                            $errorArray = array();

                            // Validation
                            $_POST = Form::sanitizeArray($_POST);

                            if(empty($_POST['position'])) {
                                $error .= '<p>- Proszę wpisać nazwę powiadomienia.</p>';
                                $errorArray['position'] = true;
                            } elseif(strlen($_POST['position']) < 3 || strlen($_POST['position']) > 120) {
                                $error .= '<p>- Nazwa powiadomienia musi mieć od 3 do 120 znaków.</p>';
                                $errorArray['position'] = true;
                            }

                            if(empty($_POST['vacancies'])) {
                                $error .= '<p>- Proszę wpisać liczbę wakatów.</p>';
                                $errorArray['vacancies'] = true;
                            } elseif(!is_numeric($_POST['vacancies'])) {
                                $error .= '<p>- Ilość wakatów musibyć liczbą.</p>';
                                $errorArray['vacancies'] = true;
                            }           

                            if(empty($_POST['date'])) {
                                $error .= '<p>- Proszę wpisać datę rozpoczęcia pracy.</p>';
                                $errorArray['date'] = true;
                            }

                            # stawka
                            if(isset($_POST['rate_min'])) {
                                (float) $_POST['rate_min'] = floatval($_POST['rate_min']);
                            }
                            if(isset($_POST['rate_max'])) {
                                (float) $_POST['rate_max'] = floatval($_POST['rate_max']);
                            }
                            if(isset($_POST['rate'])) {
                                (float) $_POST['rate'] = floatval($_POST['rate']);
                            }
                            if(isset($_POST['rateMonth'])) {
                                (float) $_POST['rateMonth'] = floatval($_POST['rateMonth']);
                            }

                            if(empty($_POST['paymant'])) {
                                $error .= '<p>- Proszę wybrać stawkę.</p>';
                                $errorArray['paymant'] = true;
                            } elseif($_POST['paymant']=='stawka godzinowa') {
                                if((!isset($_POST['rate_min']) || !isset($_POST['rate_max']) || empty($_POST['rate_min']) || empty($_POST['rate_max'])) && (!isset($_POST['rate']) || empty($_POST['rate']))) {
                                    $error .= '<p>- Proszę podać stawkę.</p>';
                                    $errorArray['rate_min'] = true; 
                                    $errorArray['rate_max'] = true; 
                                    $errorArray['rate'] = true;  
                                } elseif(!is_float($_POST['rate_min']) || !is_float($_POST['rate_max']) || !is_float($_POST['rate'])){ 
                                    $error .= '<p>- Proszę podać prawidłową wartość stawki minimalnej i maksymalnej.</p>';
                                    $errorArray['rate_min'] = true; 
                                    $errorArray['rate_max'] = true;   
                                    $errorArray['rate'] = true;  
                                } elseif($_POST['rate_min']>$_POST['rate_max']) {
                                    $error .= '<p>- Kwota maksymalna nie może być mniejsza od kwoty minimalnej..</p>';
                                    $errorArray['rate_min'] = true; 
                                    $errorArray['rate_max'] = true;     
                                }
                            } elseif($_POST['paymant']=='stawka miesięczna') {
                                if(!isset($_POST['rateMonth']) || empty($_POST['rateMonth'])) {
                                    $error .= '<p>- Proszę podać stawkę miesięczną.</p>';
                                    $errorArray['rateMonth'] = true; 
                                } elseif(!is_float($_POST['rateMonth'])){ 
                                    $error .= '<p>- Proszę podać prawidłową wartość stawki miesięcznej.</p>';
                                    $errorArray['rateMonth'] = true;   
                                }
                            }

                            if(!isset($_POST['oneclick']) || empty($_POST['oneclick']) || !is_array($_POST['oneclick'])) {
                                $error .= '<p>- proszę wybrać kandydatów do powiadomienia</p>';
                                $errorArray['oneclick'] = true;
                            }   

                            if(!isset($_POST['oneclick_info']) || empty($_POST['oneclick_info'])) {
                                $error .= '<p>- proszę wpisać informacje dla kandydatów</p>';
                                $errorArray['oneclick_info'] = true;
                            }   

                            if(!$error) {
                                if($this->model->powiadomieniaOnclick($_POST)) {
                                    FlashMsg::add('success', 'Udało Ci się poprawnie wysłać .');
                                    header("Location: " . HOST . "moje-konto/oneclick");
                                } else {
                                    FlashMsg::add('error', 'Nie udało Ci się zapisać zainteresowań. Spróbuj ponownie później.');
                                    header("Location: " . HOST . "moje-konto/oneclick");
                                }
                            } else {
                                $this->template->assign('errorArray', $errorArray);
                                $this->template->assign('error', '<p><strong>Podczas wysyłania oferty OneClick wystąpiły błędy:</strong></p>'.$error);
                            } 
                        }

                        $this->template->display('mojeKonto/pracodawca/oneclickPowiadomienieCopy.tpl');
                    break;
                    default:
                        header("Location: " . HOST);
                    break;
                }
                
            } elseif($queryname && is_numeric($queryname)) { 
                $appList = $this->model->getPowiadomienieOneclick($queryname);
                $this->template->assign('appList', $appList);
                $this->template->assign('queryname', $queryname);
                $this->template->display('mojeKonto/pracodawca/oneclickKandydaci.tpl');
            } else {
                $oneclickList = $this->model->getPowiadomienieOneclick();
              
                $this->template->assign('oneclickList', $oneclickList);

                $this->template->display('mojeKonto/pracodawca/oneclick.tpl');
            }
            
        } elseif($this->userType==1 && $queryname && is_numeric($queryname)) {

            if(!$powiadomienie = $this->model->getPowiadomienieOneclick($queryname, true)) header("Location: " . HOST);

            if($this->model->checkIfUserApplied($powiadomienie['id'])) {
                $applied = true;
            } else {
                $applied = false;
            }
            
            if(!$this->model->checkVacancies($powiadomienie['id'], $powiadomienie['vacancies'])) {
                $vacancies = true;
            } else {
                $vacancies = false;
            }
            
            if(isset($_POST['inviteFriend']) && isset($_POST['id']) && is_numeric($_POST['id']) && isset($_POST['friends']) && is_array($_POST['friends'])) {
                if($res = $this->model->inviteFriendsToOneclick($_POST['id'], $_POST['friends'])) {
                    FlashMsg::add('warning', 'Niektórzy Twoi znajomi otrzymali już tę ofertę ('.$res.') i nie możesz jej ponownie wysłać.');
                    header("Location: " . HOST . "moje-konto/oneclick/".$_POST['id']);
                } else {
                    FlashMsg::add('success', 'Udało Ci się wysłać zaproszenie do wszystkich przyjaciół.');
                    header("Location: " . HOST . "moje-konto/oneclick/".$_POST['id']);
                }
            }
           
            
            $this->template->assign('applied', $applied); 
            $this->template->assign('vacancies', $vacancies); 

            $this->template->assign('powiadomienie', $powiadomienie);
            $this->template->display('mojeKonto/kandydat/oneclick.tpl');
        } elseif($this->userType==1 && $queryname && !is_numeric($queryname)) {
            
            $exp = explode(',', $queryname);
            
            if(!$powiadomienie = $this->model->getPowiadomienieOneclick($exp[0], true)) header("Location: " . HOST);
                       
            if(isset($exp[1]) && $exp[1]=='sukces') {
                $this->template->display('mojeKonto/kandydat/oneclick-sukces.tpl');
            } elseif(isset($exp[1]) && $exp[1]=='rezerwowy') {
                $this->template->display('mojeKonto/kandydat/oneclick-rezerwowy.tpl');
            } elseif(isset($exp[1]) && $exp[1]=='akceptuj') {
                
                if($this->model->checkIfUserIsInvitedByFriend($powiadomienie['id'])) {
                    $friend = true;
                } else {
                    $friend = false;
                }

                if($this->model->checkIfUserApplied($powiadomienie['id'])) {
                    $applied = true;
                } else {
                    $applied = false;
                }

                if(!$this->model->checkVacancies($powiadomienie['id'], $powiadomienie['vacancies'])) {
                    $vacancies = true;
                } else {
                    $vacancies = false;
                }
                
                $this->template->assign('applied', $applied); 
                $this->template->assign('vacancies', $vacancies); 
            
                if(!$applied) {
                    if($vacancies) {
                        if($this->model->powiadomienie($exp[0], $powiadomienie['employeer'], true, $friend)) {
                            header("Location:".HOST."moje-konto/oneclick/".$exp[0].",sukces");
                        } else {
                            FlashMsg::add('error', 'Coś poszło nie tak. Spróbuj ponownie później.');
                            header("Location:".HOST."moje-konto/oneclick/".$exp[0]);
                        } 
                    } else {
                        if($this->model->powiadomienie($exp[0], $powiadomienie['employeer'], false, $friend)) {
                            header("Location:".HOST."moje-konto/oneclick/".$exp[0].",rezerwowy");
                        } else {
                            FlashMsg::add('error', 'Coś poszło nie tak. Spróbuj ponownie później.');
                            header("Location:".HOST."moje-konto/oneclick/".$exp[0]);
                        } 
                    }
                } else {
                    FlashMsg::add('warning', 'Już aplikowałeś na to stanowisko.');
                    header("Location:".HOST);
                }
            } elseif(isset($exp[1]) && $exp[1]=='odrzuc') {
                
                if($this->model->checkIfUserIsInvitedByFriend($powiadomienie['id'])) {
                    $friend = true;
                } else {
                    $friend = false;
                }
                
                #die($friend);
                
                $this->model->powiadomienieOdrzuc($exp[0], $powiadomienie['employeer'], $friend);
                $this->template->display('mojeKonto/kandydat/oneclick-odrzucony.tpl');
            } else {
                header("Location: " . HOST);
            }
        } else {
            header("Location: " . HOST);
        }
    }

    public function mojeGrupyAction($queryname = false) {
        if($queryname) {
            $url = explode(',',$queryname);

            if(count($url) < 2) header("Location: " . HOST);

            switch($url[0]) {
                case 'usun':
                    if(!is_numeric($url[1])) header("Location: " . HOST);
                    if($this->model->deleteOneclickGroup($url[1])) {
                        FlashMsg::add('success', 'Udało Ci się poprawnie usunąć grupę .');
                        header("Location: " . HOST . "moje-konto/moje-grupy");
                    } else {
                        FlashMsg::add('error', 'Nie udało Ci się usunąć grupy. Spróbuj ponownie później.');
                        header("Location: " . HOST . "moje-konto/moje-grupy");
                    }
                break;
                case 'podglad':
                    if(!is_numeric($url[1])) header("Location: " . HOST);
                    $group = $this->model->getOneclickGroup($url[1]);
                    $members = $this->model->getMemberFromGroup($url[1]);

                    $this->template->assign('group', $group);
                    $this->template->assign('members', $members);
                    $this->template->display('mojeKonto/pracodawca/mojeGrupyPodglad.tpl');
                break;
                case 'nowy':
                    if(!is_numeric($url[1])) header("Location: " . HOST);
                    $oneclick = $this->model->getOneClickUsers();
                    $group = $this->model->getOneclickGroup($url[1]);
                    
                     if(isset($_POST['member'])) {
                        $error = '';

                        // Validation
                        $_POST = Form::sanitizeArray($_POST);

                        if(!isset($_POST['oneclick']) || empty($_POST['oneclick'])) {
                            $error .= '<p>- Proszę wybrać kandydatów.</p>';
                            $errorArray['position'] = true;
                        }

                        if(!$error) {
                            if($this->model->addMemberToGroup($url[1], $_POST['oneclick'])) {
                                FlashMsg::add('success', 'Udało Ci się poprawnie dodać nowych członków do tej grupy.');
                                header("Location: " . HOST . "moje-konto/moje-grupy/podglad,".$url[1]);
                            } else {
                                FlashMsg::add('error', 'Nie udało Ci się  dodać nowych członków do tej grupy. Spróbuj ponownie później.');
                                header("Location: " . HOST . "moje-konto/moje-grupy/podglad,".$url[1]);
                            }
                        } else {
                            $this->template->assign('errorArray', $errorArray);
                            $this->template->assign('error', '<p><strong>Podczas dodawania grupy wystąpiły błędy:</strong></p>'.$error);
                        } 
                    }
                    
                    $this->template->assign('group', $group);
                    $this->template->assign('oneclick', $oneclick); 
                    $this->template->display('mojeKonto/pracodawca/mojeGrupyNowyCzlonek.tpl');
                break;
                case 'usun-kandydata':
                    if(!is_numeric($url[1])) header("Location: " . HOST);
                    if(!isset($url[2]) || !is_numeric($url[2])) header("Location: " . HOST);

                    if($this->model->deleteMemberFromGroup($url[1], $url[2])) {
                        FlashMsg::add('success', 'Udało Ci się poprawnie usunąć kandydata .');
                        header("Location: " . HOST . "moje-konto/moje-grupy/podglad,".$url[1]);
                    } else {
                        FlashMsg::add('error', 'Nie udało Ci się usunąć kandydata. Spróbuj ponownie później.');
                        header("Location: " . HOST . "moje-konto/moje-grupy/podglad,".$url[1]);
                    }
                break;
                case 'filtr':
                    if (!$this->isXmlHttpRequest()) {
                        header('Location:' . HOST);
                    }

                    $url[1] = str_replace(';', '', $url[1]);
                    if (empty($url[1])) {
                        $groups = $this->model->getOneclickGroup();
                    } else {
                        $groups = $this->model->getFilteredOneclickGroup($url[1]);
                    }
                    $this->template->assign('groups', $groups);
                    $this->template->display('mojeKonto/pracodawca/mojeFiltrowaneGrupy.tpl');
                    break;
                default:
                    header("Location: " . HOST); 
                break;
            }
        } else {
            if(isset($_POST['addGroup'])) {
                $error = '';

                // Validation
                $_POST = Form::sanitizeArray($_POST);

                if(empty($_POST['name'])) {
                    $error .= '<p>- Proszę wpisać nazwę grupy.</p>';
                    $errorArray['position'] = true;
                } elseif(strlen($_POST['name']) < 3 || strlen($_POST['name']) > 120) {
                    $error .= '<p>- Nazwa grupy musi mieć od 3 do 120 znaków.</p>';
                    $errorArray['position'] = true;
                }

                if(!$error) {
                    if($this->model->addOneclickGroup($_POST)) {
                        FlashMsg::add('success', 'Udało Ci się poprawnie dodać nową grupę .');
                        isset($_POST['returnTo'])
                            ? header("Location: " . HOST . $_POST['returnTo'])
                            : header("Location: " . HOST . "moje-konto/moje-grupy")
                        ;
                    } else {
                        FlashMsg::add('error', 'Nie udało Ci się dodać nowej grupy. Spróbuj ponownie później.');
                        header("Location: " . HOST . "moje-konto/moje-grupy");
                    }
                } else {
                    $this->template->assign('errorArray', $errorArray);
                    $this->template->assign('error', '<p><strong>Podczas dodawania grupy wystąpiły błędy:</strong></p>'.$error);
                } 
            }

            if (isset($_POST['deleteGroup'])) {
                $groupsIds = json_decode($_POST['groupsIds']);
                if (is_array($groupsIds)) {
                    foreach ($groupsIds as $groupId) {
                        if (($groupId = intval($groupId)) > 0) {
                            $this->model->deleteOneclickGroup($groupId);
                        }
                        FlashMsg::add('success', 'Wskazane grupy zostały usunięte.');
                    }
                } else {
                    FlashMsg::add('error', 'Coś poszło nie tak. Spróbuj ponownie później.');
                }

                if (isset($_POST['returnTo'])) {
                    header('Location: ' . HOST . $_POST['returnTo']);
                }
            }
            
            $groups = $this->model->getOneclickGroup();
            $this->template->assign('groups', $groups);
            $this->template->display('mojeKonto/pracodawca/mojeGrupy.tpl');
        }
    }

    /**
     * AJAXowa akcja do zapisywania/wypisywania kandydata do grupy.
     *
     * @param $queryName
     */
    public function grupyAction($queryName)
    {
        list ($action, $userId) = explode(',', $queryName);

        $validActions = array('usun-kandydata', 'dodaj-kandydata');

        if (!$this->isXmlHttpRequest()) {
            header('Location: ' . HOST);
        }

        if (
            strtolower($_SERVER['REQUEST_METHOD']) !== 'post'
            || !in_array($action, $validActions)
            || !is_numeric($userId)
        ) {
            die(json_encode(array('success' => 0)));
        }

        $groupIds = $_POST['groups'];
        if (!is_array($groupIds)) {
            $groupIds = array();
        } else {
            foreach ($groupIds as $key => $gId) {
                $gId = intval($gId);
                if ($gId > 0) {
                    $groupIds[$key] = $gId;
                } else {
                    unset($groupIds[$key]);
                }
            }
        }

        if ('dodaj-kandydata' === $action) {
            foreach ($groupIds as $id) {
                $this->model->addMemberToGroupSecurely($id, $userId);
            }
        } else {
            foreach ($groupIds as $id) {
                $this->model->removeMemberFromGroupSecurely($id, $userId);
            }
        }

        die(json_encode(array('success' => 1)));
    }

    function createCalendarAction() {

        if(!isset($_POST['date']) || empty($_POST['date'])) die('błąd');
        
        $exp = explode('-', $_POST['date']);
        
        if(count($exp)!=2) die('błąd');
        
        $actualDate = date("d-m-Y");
        
        $month = $exp[0];
        $year = $exp[1];
        
        if($month==12) {
            $nextMonth = 1;
            $nextYear = $year+1;
        } else {
            $nextMonth = $month+1;
            $nextYear = $year;
        }
        
        if($month==1) {
            $prevMonth = 12;
            $prevYear = $year-1;
        } else {
            $prevMonth = $month-1;
            $prevYear = $year;
        }

        $first_day = mktime(0,0,0, $month, 1, $year) ;
        $day_of_week = date('D', $first_day) ; 
        $days_in_month = cal_days_in_month(0, $month, $year) ;
        
        $title = date('F', $first_day);

        $arrayToFind = array('January','February','March','April','May','June','July','August','September','October','November','December');
        $arrayToReplace = array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień');
        
        $title = str_replace($arrayToFind, $arrayToReplace, $title);

        switch($day_of_week){ 
           case "Mon": $blank = 0; break; 
           case "Tue": $blank = 1; break; 
           case "Wed": $blank = 2; break; 
           case "Thu": $blank = 3; break; 
           case "Fri": $blank = 4; break; 
           case "Sat": $blank = 5; break; 
           case "Sun": $blank = 6; break; 
        }
        
        echo '<div class="miesiace"><a href="javascript:;" onclick="createCalendar(\''.$prevMonth.'-'.$prevYear.'\');"><i class="icon icon-chevron-left"></i></a> '.$title.' '.$year.'  <a href="javascript:;" onclick="createCalendar(\''.$nextMonth.'-'.$nextYear.'\')"><i class="icon icon-chevron-right"></i></a></div>';
        
        echo '<table>';
        echo '<tr><th>Pn</th><th>Wt</th><th>Śr</th><th>Cz</th><th>Pi</th><th>So</th><th>Ni</th></tr>';
        
        $day_count = 1; 
        
        echo "<tr>";
        
        while($blank>0){ 
            echo "<td class='blank'></td>"; 
            $blank = $blank-1; 
            $day_count++;
        }  
        
        $day_num = 1;
                        
        while($day_num <= $days_in_month ) { 
            
            $calendarDate = $day_num.'-'.$month.'-'.$year;
            
            if($actualDate==$calendarDate) {
                echo '<td class="today"><div><a href="#calendarDay" data-toggle="modal" onclick="createOneDayCalendar(\''.$day_num.'-'.$month.'-'.$year.'\')">' .$day_num. '</a>';
                if($status) {
                    switch($status) {
                        case -1:
                            echo '<span class="unread"></span>';
                        break;
                        case 1:
                            echo '<span class="notice"></span>';
                        break;
                        case 2:
                            echo '<span class="meeting"></span>';
                        break;
                        default:
                            echo '<span class="unread"></span>';
                        break;
                    }
                } else {
                    echo '<span></span>';
                }
                echo '</div></td>';
            } else {
                echo '<td><div><a href="#calendarDay" data-toggle="modal" onclick="createOneDayCalendar(\''.$day_num.'-'.$month.'-'.$year.'\')">' .$day_num. '</a>';
                $status = $this->model->getCalendarEventsSmall($year.'-'.$month.'-'.$day_num);
                if($status) {
                    switch($status) {
                        case -1:
                            echo '<span class="unread"></span>';
                        break;
                        case 1:
                            echo '<span class="notice"></span>';
                        break;
                        case 2:
                            echo '<span class="meeting"></span>';
                        break;
                        default:
                            echo '<span></span>';
                        break;
                    }
                } else {
                    echo '<span></span>';
                }
                echo '</div></td>'; 
            }
            $day_num++; 

            $day_count++;

            if ($day_count > 7){
                echo "</tr><tr>";
                $day_count = 1;
            }
        }  
                        
        while ( $day_count >1 && $day_count <=7 ) { 
            echo "<td class='blank'></td>"; 
            $day_count++; 
        } 

        echo "</tr></table>";         
    }
    
    function createOneDayCalendarAction() {

        if(!isset($_POST['date']) || empty($_POST['date'])) die('błąd');
        
        $exp = explode('-', $_POST['date']);
        
        if(count($exp)!=3) die('błąd');
        
        $day = $exp[0];
        $month = $exp[1];
        $year = $exp[2];
        
        $first_day = mktime(0,0,0, $month, 1, $year) ;
        
        $monthName = date('F', $first_day);

        $arrayToFind = array('January','February','March','April','May','June','July','August','September','October','November','December');
        $arrayToReplace = array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień');
        
        $monthName = str_replace($arrayToFind, $arrayToReplace, $monthName);

        $events = $this->model->getCalendarEvents($year . '-'.  $month . '-' . $day);

        if($events) {
            $title = $day . ' '.  $monthName . ' ' . $year . ' rozpoczynasz pracę. <br>Kandydacie pamiętaj tego dnia zgłoś się do pracy!';
        } else {
            $title = $day . ' '.  $monthName . ' ' . $year;
        }
        
        if($events) {
            $body = '<table>';
            
            foreach($events as $event) {
                
                $body  .= '<tr>';
                
                switch($event['status']) {
                    case 0:
                        $body .= '<td style="width: 30px; height: 30px;"><span class="label label-info"><i class="icon icon-white icon-question-sign"></i></span></td>';
                    break;
                    case 1:
                        $body .= '<td style="width: 30px; height: 30px;"><span class="label label-success"><i class="icon icon-white icon-ok-sign"></i></span></td>';
                    break;
                    case 2:
                        $body .= '<td style="width: 30px; height: 30px;"><span class="label label-warning"><i class="icon icon-white icon-flag"></i></span></td>';
                    break;
                    case 3:
                        $body .= '<td style="width: 30px; height: 30px;"><span class="label label-important"><i class="icon icon-white icon-remove"></i></span></td>';
                    break;
                }
                
                $body .= '<td style="width: 370px;"><a href="/moje-konto/oneclick/'.$event['id'].'">'.$event['title'].'</a></td>';
                
                $body .= '<td><a class="btn btn-small btn-happinate" target="_blank" href="/profil/p/'.md5($event['employeer']).'">zobacz profil pracodawcy</a></td>';

                $body .= '</tr>';

            }
                
            $body .= '</table>';
            
            $body .= '<div class="legend" style="margin-top: 20px; border-top: 1px solid #CCC;">
                        <h3>Legenda:</h3>
                        <p><span class="label label-info"><i class="icon icon-white icon-question-sign"></i></span> - otrzymałeś zaproszenie</p>
                        <p><span class="label label-success"><i class="icon icon-white icon-ok-sign"></i></span> - jesteś gotowy do pracy</p>
                        <p><span class="label label-warning"><i class="icon icon-white icon-flag"></i></span> - znajdujesz się na liście rezerwowych</p>
                        <p><span class="label label-important"><i class="icon icon-white icon-remove"></i></span> - zostałeś odrzucony przez pracodawcę</p>
                      </div>';
        } else {
            $body = 'brak powiadomień na ten dzień';
        }

        $array = array($title, $body);
        
        echo json_encode($array);
    }
    
    function edytujProfilAction() {
        
        if($this->userType!=2) {
            header("Location:".HOST);
        }
        
        $profil = $this->model->getEmployeerProfile();

        $userCalendar = $this->model->generateSimpleCalendar();
        $this->template->assign('userCalendar', $userCalendar);

        $this->template->assign('userProfile', $profil);

        // Friend Invitation
        if(isset($_POST['friendEmail']) && $_POST['friendEmail']) {
            $error = '';
            if(empty($_POST['friendEmail'])) {
                $error .= '<p>- Proszę wpisać adres email.</p>';
            } elseif(!Form::validEmail($_POST['friendEmail'])) {
                $error .= '<p>- Proszę wpisać poprawny adres email.</p>';
            }
            if(!$error) {
                if($this->model->inviteFriend($_POST['friendEmail'], true)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie wysłać polecenie.');
                    header("Location: " . HOST . "moje-konto");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się poprawnie wysłać polecenie.');
                    header("Location: " . HOST . "moje-konto");
                }
            } else {
                $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
            }
        }

        if(isset($_POST['update-profile'])) {
            $error = '';
            $errorArray = array();
            // Validation
            $_POST = Form::sanitizeArray($_POST);
            if(empty($_POST['profile_name'])) {
                $error .= '<p>Proszę wpisać nazwę profilu.</p>';
                $errorArray['profile_name'] = true;
            } elseif(!Form::validNoSpecialChars($_POST['profile_name'])) {
                $error .= '<p>Nazwa profilu musi się składać wyłącznie prostych znaków i liczb.</p>';
                $errorArray['profile_name'] = true;
            } elseif(strlen($_POST['profile_name']) < 1 || strlen($_POST['profile_name']) > 50) {
                $error .= '<p>Nazwa profilu musi mieć od 1 do 50 znaków.</p>';
                $errorArray['profile_name'] = true;
            }
            if(empty($_POST['profile_email'])) {
                $error .= '<p>Proszę wpisać adres email.</p>';
                $errorArray['profile_email'] = true;
            } elseif(!Form::validEmail($_POST['profile_email'])) {
                $error .= '<p>Proszę podać prawidłowy adres email.</p>';
                $errorArray['profile_email'] = true;
            } elseif($this->model->checkEmailNotExist($_POST['profile_email'])) {
                $error .= '<p>Podany adres email znajduje się już w bazie danych.</p>';
                $errorArray['profile_email'] = true;
            }

            if(!empty($_POST['profile_zip_code'])) {
                if(!Form::validPolishZipCode($_POST['profile_zip_code'])) {
                    $error .= '<p>Proszę podać prawidłowy kod pocztowy (przykład 00-000)</p>';
                    $errorArray['profile_zip_code'] = true;
                }
            }
            if(!empty($_POST['profile_nip'])) {
                if(!is_numeric($_POST['profile_nip']) || !Form::validPolishNip($_POST['profile_nip'])) {
                    $error .= '<p>Proszę podać prawidłowy numer NIP</p>';
                    $errorArray['profile_nip'] = true;
                }
            }
            if(!empty($_POST['profile_regon'])) {
                if(!is_numeric($_POST['profile_regon']) || !Form::validPolishRegon($_POST['profile_regon'])) {
                    $error .= '<p>Proszę podać prawidłowy numer REGON</p>';
                    $errorArray['profile_regon'] = true;
                }
            }
            if(!empty($_POST['profile_pesel'])) {
                if(!is_numeric($_POST['profile_pesel']) || !Form::validPolishPesel($_POST['profile_pesel'])) {
                    $error .= '<p>Proszę podać prawidłowy numer pesel.</p>';
                    $errorArray['profile_pesel'] = true;
                }
            }
            if(!empty($_POST['www'])) {
                if(!Form::validUrl($_POST['www'])) {
                    $error .= '<p>Proszę podać prawidłowy adres strony www.</p>';
                    $errorArray['www'] = true;
                }
            }
            if(!empty($_POST['fanpage'])) {
                if(!Form::validUrl($_POST['fanpage'])) {
                    $error .= '<p>Proszę podać prawidłowy adres fanpage\'a.</p>';
                    $errorArray['fanpage'] = true;
                }
            }

            if(isset($_FILES['file']) && $_FILES['file']['error']==0) {
                $imgInfo = getimagesize($_FILES['file']['tmp_name']);

                if(!($imgInfo['mime']=='image/jpeg' || $imgInfo['mime']=='image/png' || $imgInfo['mime']=='image/gif')) {
                    $error .= '<p>- przesłane logo musi być formatu png, jpg lub gif.</p>';
                    $errorArray['file'] = true;
                }

                if($imgInfo[0]>1600 || $imgInfo[1]>1200) {
                    $error .= '<p>- przesłane logo może mieć maksymalną rozdzielczość 1024px na 768px (szerokość / wysokość).</p>';
                    $errorArray['file'] = true;
                }

                if($_FILES['file']['size']>1024000) {
                    $error .= '<p>- przesłane logo waży zbyt dużo (maksymalna waga to 500kb).</p>';
                    $errorArray['file'] = true;
                }

                $file = $_FILES['file'];
            } elseif(isset($_FILES['file']) && $_FILES['file']['error']==1) { 
                $error .= '<p>- przesłane zdjęcie profilowe waży zbyt dużo (maksymalna waga to 1mb).</p>';
                $errorArray['file2'] = true;
            } else {
                $file = 0;
            }
            if(isset($_POST['file_delete']) && $_POST['file_delete']=='true') {
                $file = -1;
            }

            if(!$error) {
                if($this->model->updateEmployeer($_POST, $file)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie edytować swój profil.');
                    header("Location: " . HOST . "moje-konto");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się edytować profilu. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
            }            
        }
        // Edycja profilu
        if(isset($_POST['profileEdit']) && $_POST['profileEdit']) {
            $error = '';
            $errorArray = array();
            // Validation
            $_POST = Form::sanitizeArray($_POST);

            /*
            if(empty($_POST['profile_url'])) {
                $error .= '<p>Proszę wpisać unikalny adres profilu.</p>';
                $errorArray['profile_url'] = true;
            } elseif(!preg_match('/^[a-zA-Z0-9]+$/', $_POST['profile_url'])) {
                $error .= '<p>Unikalny adres może składać się tylko z prostych znaków (bez polskich znaków), liczb oraz bez spacji.</p>';
                $errorArray['profile_url'] = true;
            } elseif(!$this->model->checkUniqueProfileUrl($_POST['profile_url'])) {
                $error .= '<p>Taki unikalny adres jest już użyty.</p>';
                $errorArray['profile_url'] = true;
            }
            */

            if(!empty($_POST['yt']) && !Form::validYoutubeUrl($_POST['yt'])) {
                $error .= '<p>Proszę wpisać prawidłowy link skopiowany ze strony Youtube.</p>';
                $errorArray['yt'] = true;
            }

            if(isset($_FILES['file']) && $_FILES['file']['error']==0) {
                $imgInfo = getimagesize($_FILES['file']['tmp_name']);

                if(!($imgInfo['mime']=='image/jpeg' || $imgInfo['mime']=='image/png' || $imgInfo['mime']=='image/gif')) {
                    $error .= '<p>- przesłane zdjęcie profilowe musi być formatu png, jpg lub gif.</p>';
                    $errorArray['file2'] = true;
                }

                if($_FILES['file']['size']>1048576) {
                    $error .= '<p>- przesłane zdjęcie profilowe waży zbyt dużo (maksymalna waga to 1mb).</p>';
                    $errorArray['file2'] = true;
                }

                $file = $_FILES['file'];
            } elseif(isset($_FILES['file']) && $_FILES['file']['error']==1) { 
                $error .= '<p>- przesłane zdjęcie profilowe waży zbyt dużo (maksymalna waga to 1mb).</p>';
                $errorArray['file2'] = true;
            } else {
                $file = 0;
            }
            if(isset($_POST['file_delete']) && $_POST['file_delete']=='true') {
                $file = -1;
            }

            if(!$error) {
                if($this->model->updateEmployeerProfile($_POST, $file)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie edytować swój profil.');
                    header("Location: " . HOST . "moje-konto");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się edytować profilu. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto");
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
            }            
        }

        if(isset($_POST['changePass']) && $_POST['changePass']) {
            $error = '';
            // Validation
            $_POST = Form::sanitizeArray($_POST);

            if(empty($_POST['password_old']) || empty($_POST['password_new']) || empty($_POST['password_new2'])) {
                $error .= '<p>Proszę wypełnić wszystkie pola (stare hasło, nowe hasło i powtórz nowe hasło)</p>';
            } else {
                if(!$this->model->checkPassword($_POST['password_old'])) {
                    $error .= '<p>Twoje stare hasło jest niepoprawne.</p>';
            }

                if($_POST['password_new']!=$_POST['password_new2']) {
                    $error .= '<p>Proszę wpisać dwa identyczne nowe hasła.</p>';
             }

                if($_POST['password_new']!=$_POST['password_new2']) {
                    $error .= '<p>Proszę wpisać dwa identyczne nowe hasła.</p>';
                }
            }

            if(!$error) {
                if($this->model->changePassword($_POST['password_new'])) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie zmienić hasło.');
                    header("Location: " . HOST . "moje-konto");
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się zmienić hasła. Spróbuj ponownie później.');
                    header("Location: " . HOST . "moje-konto");
                }
            } else {
                $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
            } 
        }

        $this->template->display('mojeKonto/pracodawca/index.tpl');
    }

    private function isXmlHttpRequest()
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    private function appendGroupsToApplicants($applicants, $user)
    {
        $orderedApplicants = array();

        if (empty($applicants)) {
            return $orderedApplicants;
        }

        foreach ($applicants as $applicant) {
            $orderedApplicants[$applicant['user_id']] = $applicant;
            if (!isset($applicant['groups'])) {
                $orderedApplicants[$applicant['user_id']]['groups'] = array();
            }
        }

        $userToOneClickGroups = $this->model->getOneclickGroupsForUsers(array_keys($orderedApplicants));

        foreach ($userToOneClickGroups as $userToOneClickGroup) {
            $orderedApplicants[$userToOneClickGroup['user']]['groups'][] = $userToOneClickGroup['group_id'];
        }

        return $orderedApplicants;
    }
}