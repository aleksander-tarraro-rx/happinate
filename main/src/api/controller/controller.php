<?php

namespace src\api;

use app\helpers\Form;
use src\BaseController;

/*
 * CONTROLLER 
 * 
 * api
 * 
 * !IMPORTANT
 * 
 * This is only used by Android App to get some informations from database.
 * It needs [API_KEY] to work.
 * It's need to be improved or redesinged.
 * All returned data are in JSON.
 * 
 */

class apiController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* 
     * INDEX ACTION
     * 
     * /api
     * 
     * DOES NOTHING
     * 
     * redirect to home page
     */
    public function indexAction($queryName = false) {
        header("Location:" . HOST);
    }
    
    /* 
     * getOffers
     * 
     * /api/getOffers/[$queryName]
     * 
     * /api/getOffers/[PAGE NUMBER] - [API_KEY] - [TRADE1] , [TRADE2] - [CONTRACT1] , [CONTRACT2] - [TIME1] , [TIME2] - [CITY1] , [CITY1] - [MIN_RATE]
     * 
     * get all offers from database with [$queryName] as a filter
     * [$queryName] is needed to work, and it's exploded to an array
     * 
     * [$queryName][0]      = actual page number
     * [$queryName][1]      = [API_KEY]
     * [$queryName][2-6]    = filters
     * [$queryName][7]      = [User ID]
     * 
     */
    public function getOffersAction($queryName = false) {
        if(!$queryName) { 
            // If $queryName is empty
            die('Access denied!');
        } else {
            $url = explode('-', $queryName);
            // Check [API_KEY]
            if(!isset($url[1]) || $url[1]!=API_KEY) {
                die('Access denied!');
            }
            // Filters
            if(isset($url[2])) {
                $filter = $url[2] . '-' . $url[3] . '-' . $url[4] . '-' . $url[5] . '-' . $url[6];
            } else {
                $filter = false;
            }
            // [USER_ID]
            if(isset($url[7])) {
                $userId = $url[7];
            } else {
                $userId = false;
            }
        }
        
        // Job on one page
        $jobOnPage = 20;
        // Actual page
        $page = $queryName[0];

        if(!empty($page)) {
            $from = $page * $jobOnPage - $jobOnPage;
        } else {
            $from = 0;
            $page = 1;
        }        
        
        // Count all pages with specific filters and return integer
        $countJobs = $this->model->countJobs($filter);
        $pageCount = ceil($countJobs/$jobOnPage);
        
        // Get all jobs with specific filters and limited with [$from] / [$jobOnPage]
        $offers = $this->model->getJob($from, $jobOnPage, $filter);
        
        #echo "<pre>"; print_r($offers); echo"</pre>";
        
        // Start array
        $json = array();
        $json['pageCount'] = $pageCount;
        
        // Convert jobs array to json array
        $i = 0;
        if($offers) {
            foreach ($offers as $offer) {
                if($userId && $this->model->checkIfOneclick($offer['job_id'], $userId)) { 
                    $json['offers'][$i]['id'] = $offer['job_id'];
                    $json['offers'][$i]['title'] = $offer['job_position'];
                    $json['offers'][$i]['city'] = $offer['job_employeer_place_direct_city'];

                    if($offer['job_volunteering']) {
                        $json['offers'][$i]['pay'] = "0.00 zł/g"; 
                    } elseif($offer['job_hide_salary']) {
                        $json['offers'][$i]['pay'] = "?? zł/g";
                    } else {
                        $json['offers'][$i]['pay'] = ceil($offer['job_rate_max'] + $offer['job_rate_min']) / 2 . " zł/g";
                    }

                    $json['offers'][$i]['contractType'] = $offer['job_contract_type_id'];
                    $json['offers'][$i]['tradeType'] = $offer['job_trade_type_id'];

                    if($userId) {
                        $json['offers'][$i]['fav'] = $this->model->checkIfFaved($offer['job_id'], $userId);
                    } else {
                        $json['offers'][$i]['fav'] = false; 
                    }
                    
                    $json['offers'][$i]['oneclick'] = true;
                    
                    if($offer['job_main_page']==2) {
                        $json['offers'][$i]['wyroznienie'] = false;
                        $json['offers'][$i]['pracujteraz'] = true;
                    } elseif($offer['job_main_page']==1) {
                        $json['offers'][$i]['wyroznienie'] = true;
                        $json['offers'][$i]['pracujteraz'] = false;
                    } else {
                        $json['offers'][$i]['wyroznienie'] = false;
                        $json['offers'][$i]['pracujteraz'] = false;
                    }
                    $i++;
                }
            }
        } 
        
        // Convert jobs array to json array
        if($offers) {
            foreach ($offers as $offer) {
                if(!$userId || !$this->model->checkIfOneclick($offer['job_id'], $userId)) { 
                    $json['offers'][$i]['id'] = $offer['job_id'];
                    $json['offers'][$i]['title'] = $offer['job_position'];
                    $json['offers'][$i]['city'] = $offer['job_employeer_place_direct_city'];

                    if($offer['job_volunteering']) {
                        $json['offers'][$i]['pay'] = "0.00 zł/g"; 
                    } elseif($offer['job_hide_salary']) {
                        $json['offers'][$i]['pay'] = "?? zł/g";
                    } else {
                        $json['offers'][$i]['pay'] = ceil($offer['job_rate_max'] + $offer['job_rate_min']) / 2 . " zł/g";
                    }

                    $json['offers'][$i]['contractType'] = $offer['job_contract_type_id'];
                    $json['offers'][$i]['tradeType'] = $offer['job_trade_type_id'];

                    if($userId) {
                        $json['offers'][$i]['fav'] = $this->model->checkIfFaved($offer['job_id'], $userId);
                    } else {
                        $json['offers'][$i]['fav'] = false;                         
                    }
                    
                    $json['offers'][$i]['oneclick'] = false;

                    if($offer['job_main_page']==2) {
                        $json['offers'][$i]['wyroznienie'] = false;
                        $json['offers'][$i]['pracujteraz'] = true;
                    } elseif($offer['job_main_page']==1) {
                        $json['offers'][$i]['wyroznienie'] = true;
                        $json['offers'][$i]['pracujteraz'] = false;
                    } else {
                        $json['offers'][$i]['wyroznienie'] = false;
                        $json['offers'][$i]['pracujteraz'] = false;
                    }
                    $i++;
                }
            }
        } 
        
        if(!$offers) {
            $json['offers'] = '';
        }
        
        // Echo json or null
        echo(json_encode($json));
    }
    
    /* 
     * getOffer
     * 
     * /api/getOffer/[$queryName]
     * 
     * /api/getOffer/[API_KEY] - [JOB_ID] - [USER_ID]
     * 
     * get one offer from database with [$queryName] as a filter
     * [$queryName] is needed to work, and it's exploded to an array
     * 
     * [$queryName][0]      = [API_KEY]
     * [$queryName][1]      = [JOB_ID]
     * [$queryName][2]      = [USER_ID]
     * 
     */
    public function getOfferAction($queryName = false) {
        if(!$queryName) { 
            // If $queryName is empty
            die('Access denied!');
        } else {
            $url = explode('-', $queryName);
            // Check [API_KEY]
            if(!isset($url[0]) || $url[0]!=API_KEY) {
                die('Access denied!');
            }
            // [USER_ID]
            if(isset($url[2])) {
                $userId = $url[2];
            } else {
                $userId = false;
            }
        }
        
        // Get one job with [JOB_ID]
        $job = $this->model->getOneJob($url[1]);
        
        if($job) {
            $json['offer']['id'] = $job['job_id'];
            $json['offer']['title'] = $job['job_position'];
            
            if($job['job_duty']) {
                $json['offer']['duties'] = html_entity_decode($job['job_duty']);
            } else {
                $json['offer']['duties'] = ''; 
            }
            if($job['job_need']) {
                $json['offer']['skills'] = html_entity_decode($job['job_need']);
            } else {
                $json['offer']['skills'] = '';
            }
            if($job['job_offer']) {
                $json['offer']['offer'] = html_entity_decode($job['job_offer']);
            } else {
                $json['offer']['offer'] = '';
            }
            
            if($job['job_volunteering']) {
                $json['offer']['payMin'] = " 0.00 zł/g";
                $json['offer']['payMax'] = " 0.00 zł/g";
            } elseif($job['job_hide_salary']) {
                $json['offer']['payMin'] = "?? zł/g";
                $json['offer']['payMax'] = "?? zł/g";
            } else {
                $json['offer']['payMin'] = $job['job_rate_min'] . " zł/g";
                $json['offer']['payMax'] = $job['job_rate_max'] . " zł/g";
            }
            
            if($job['job_additional_salary_info']) {
                $json['offer']['payinfo'] = $job['job_additional_salary_info']; 
            } else {
                $json['offer']['payinfo'] = '';
            }
                        
            if($job['job_employeer_place_direct_city']) {
                $json['offer']['city'] = $job['job_employeer_place_direct_city'];
            } else {
                $json['offer']['city'] = '';
            }
            
            if($job['job_employeer_name']) {
                $json['offer']['employerName'] = $job['job_employeer_name']; 
            } else {
                $json['offer']['employerName'] = ''; 
            }

            if($job['job_employeer_place_direct']) {
                $json['offer']['jobPlaceType'] = 1; 
            } elseif($job['job_employeer_place_area']) {
                $json['offer']['jobPlaceType'] = 2; 
            } elseif($job['job_employeer_place_telework']) {
                $json['offer']['jobPlaceType'] = 3; 
            } elseif($job['job_employeer_place_without']) {
                $json['offer']['jobPlaceType'] = 4; 
            }
            
            if($job['job_employeer_place_direct_address']) {
                $json['offer']['street'] = $job['job_employeer_place_direct_address']; 
            } else {
                $json['offer']['street'] = ''; 
            }       
                        
            if($job['job_employeer_place_direct_address']) {
                $json['offer']['jobStreet'] = $job['job_employeer_place_direct_address']; 
            } else {
                $json['offer']['jobStreet'] = ''; 
            }      
            
            if($job['job_employeer_place_direct_zip_code']) {
                $json['offer']['jobZipCode'] = $job['job_employeer_place_direct_zip_code']; 
            } else {
                $json['offer']['jobZipCode'] = ''; 
            }     
            
            if($job['job_employeer_place_direct_city']) {
                $json['offer']['jobCity'] = $job['job_employeer_place_direct_city']; 
            } else {
                $json['offer']['jobCity'] = ''; 
            }      
                       
            if($job['job_contact_email']) {
                $json['offer']['contactEmail'] = $job['job_contact_email']; 
            } else {
                $json['offer']['contactEmail'] = ''; 
            }      
                       
            if($job['job_contact_phone']) {
                $json['offer']['contactPhone'] = $job['job_contact_phone']; 
            } else {
                $json['offer']['contactPhone'] = ''; 
            }      
                       
            if($job['job_contract_type_id']) {
                $json['offer']['contractType'] = $job['job_contract_type_id']; 
            } else {
                $json['offer']['contractType'] = ''; 
            }      
                       
            if($job['job_trade_type_id']) {
                $json['offer']['tradeType'] = $job['job_trade_type_id']; 
            } else {
                $json['offer']['tradeType'] = ''; 
            }      
                                   
            if($job['job_lat']) {
                $json['offer']['lat'] = $job['job_lat']; 
            } else {
                $json['offer']['lat'] = ''; 
            }      
                                   
            if($job['job_trade_type_id']) {
                $json['offer']['tradeType'] = $job['job_trade_type_id']; 
            } else {
                $json['offer']['tradeType'] = ''; 
            }      
                                   
            if($job['job_long']) {
                $json['offer']['long'] = $job['job_long']; 
            } else {
                $json['offer']['long'] = ''; 
            }      
            
            // If [USER_ID] is set, check if this job is in favourite jobs
            if($userId) {
                $json['offer']['fav'] = $this->model->checkIfFaved($job['job_id'], $userId);
                $json['offer']['oneclick'] = $this->model->checkIfOneclick($job['job_id'], $userId);
                if($json['offer']['oneclick'] && $job['job_additional_oneclick_info'] ) {
                    $json['offer']['duties'] .= html_entity_decode('<p>Dodatkowe informacje od pracodawcy dla OneClick!</p><p>'.$job['job_additional_oneclick_info'].'</p>');
                }
            } else {
                $json['offer']['fav'] = false; 
                $json['offer']['oneclick'] = false; 
            } 
            
            if($job['job_main_page']===1) {
                $json['offer']['wyroznienie'] = true;
                $json['offer']['pracujteraz'] = false;
            } elseif($job['job_main_page']==2) {
                $json['offer']['wyroznienie'] = false;
                $json['offer']['pracujteraz'] = true;
            } else {
                $json['offer']['wyroznienie'] = false;
                $json['offer']['pracujteraz'] = false;
            }
        } else {
            $json['offer'] = '';
        }
        
        // Echo json or null
        echo(json_encode($json));
    }
    
    /* 
     * login
     * 
     * /api/login/[$queryName]
     * 
     * /api/login/[API_KEY]
     * 
     * [EMAIL] and [PASSWORD] are send with [$_POST]
     * [$queryName] is needed to work, and it's exploded to an array
     * 
     * [$queryName][0]      = [API_KEY]
     * 
     */    
    public function loginAction($queryName = false) {
        if(!$queryName) { 
            die('Access denied!');
        } else {
            $url = explode('-', $queryName);
            if(!isset($url[0]) || $url[0]!=API_KEY) {
                die('Access denied!');
            }
        }
        
        $error = array();

        $_POST = Form::sanitizeArray($_POST);

        if(!isset($_POST['username']) || empty($_POST['username'])) {
            $error[] = "brak loginu";
        }
        
        if(!isset($_POST['password']) || empty($_POST['password'])) {
            $error[] = "brak hasła";
        }    
        
        if(!isset($_POST['push']) || empty($_POST['push'])) {
            $error[] = "brak pusha";
        }
        
        if(!$error) {
            $loginRes = $this->model->login($_POST['username'], $_POST['password'], $_POST['push']);
            if($loginRes > 0) {
                $json = array('status'=>3, 'info'=>'sukces', 'id'=>$loginRes);
                echo(json_encode($json));
            } elseif($loginRes == -1) {
                $json = array('status'=>2, 'info'=>'konto nieaktywne');
                echo(json_encode($json));
            } else {
                $json = array('status'=>1, 'info'=>'dane niepoprawne');
                echo(json_encode($json));
            }
        } else {
            $json = array('status'=>0, 'info'=>$error);
            echo(json_encode($json));
        }
    }
    
    /* 
     * getCV
     * 
     * /api/getCV/[$queryName]
     * 
     * /api/getCV/[API_KEY]
     * 
     * get list of CV from database based on [USER_ID]
     * [$queryName] is needed to work, and it's exploded to an array
     * 
     * [$queryName][0]      = [API_KEY]
     * [$queryName][1]      = [USER_ID]
     * 
     */  
    public function getCVAction($queryName = false) {
        if(!$queryName) { 
            die('Access denied!');
        } else {
            $url = explode('-', $queryName);
            if(!isset($url[0]) || $url[0]!=API_KEY) {
                die('Access denied!');
            }
        }
        
        // Get user CV
        $cv = $this->model->getCV($url[1]);
        
        if($cv>0) {
            $json = array('cvs' => $cv);
        } elseif($cv == -1) {
            $json = array('Error' => 1, 'Msg' => 'Brak użytkownika o takim ID.');
        } else {
            $json = array('Error' => 1, 'Msg' => 'Nie masz dodanych żadnych CV.');
        }
        
        echo(json_encode($json));
    }
    
    /* 
     * applyToJob
     * 
     * /api/applyToJob/[$queryName]
     * 
     * /api/applyToJob/[API_KEY]-[USER_ID]-[JOB_ID]-[CV_ID]
     * 
     * get list of CV from database based on [USER_ID]
     * [$queryName] is needed to work, and it's exploded to an array
     * 
     * [$queryName][0]      = [API_KEY]
     * [$queryName][1]      = [USER_ID]
     * [$queryName][2]      = [JOB_ID]
     * [$queryName][3]      = [CV_ID]
     * 
     */  
    public function applyToJobAction($queryName = false) {
        if(!$queryName) { 
            die('Access denied!');
        } else {
            $url = explode('-', $queryName);
            if(!isset($url[0]) || $url[0]!=API_KEY) {
                die('Access denied!');
            }
            if(count($url)!=4) {
                die('Access denied!');
            }
        }
        
        // Apply to job
        $res = $this->model->applyToJob($url[1], $url[2], $url[3]);
        
        if($res>0) {
            $json = array('Success' => 1, 'Msg' => 'Poprawnie złożone aplikacje.');
        } elseif($res == -1) {
            $json = array('Error' => 1, 'Msg' => 'Brak użytkownika o takim ID.');
        } elseif($res == -2) {
            $json = array('Error' => 1, 'Msg' => 'Już aplikowałeś na tę ofertę.');
        } else {
            $json = array('Error' => 1, 'Msg' => 'Inny błąd.');
        }
        
        echo(json_encode($json));
    }
    
     public function applyToOneclickAction($queryName = false) {
        if(!$queryName) { 
            die('Access denied!');
        } else {
            $url = explode('-', $queryName);
            if(!isset($url[0]) || $url[0]!=API_KEY) {
                die('Access denied!');
            }
            if(count($url)!=3) {
                die('Access denied!');
            }
        }
        
        
        // Apply to job
        $res = $this->model->applyToOneclick($url[1], $url[2]);
        
        if($res>0) {
            $json = array('Success' => 1, 'Msg' => 'Poprawnie złożone aplikacje.');
        } elseif($res == -1) {
            $json = array('Error' => 1, 'Msg' => 'Brak użytkownika o takim ID.');
        } elseif($res == -2) {
            $json = array('Error' => 1, 'Msg' => 'Już aplikowałeś na tę ofertę.');
        } else {
            $json = array('Error' => 1, 'Msg' => 'Inny błąd.');
        }
        
        echo(json_encode($json));
    }
    /* 
     * getApplications
     * 
     * /api/getApplications/[$queryName]
     * 
     * /api/getApplications/[API_KEY]
     * 
     * get list of CV from database based on [USER_ID]
     * [$queryName] is needed to work, and it's exploded to an array
     * 
     * [$queryName][0]      = [API_KEY]
     * [$queryName][1]      = [USER_ID]
     * [$queryName][2]      = [JOB_ID]
     * [$queryName][3]      = [CV_ID]
     * 
     */ 
    public function getApplicationsAction($queryName = false) {
        if(!$queryName) { 
            die('Access denied!');
        } else {
            $url = explode('-', $queryName);
            if(!isset($url[1]) || $url[1]!=API_KEY) {
                die('Access denied!');
            }
        }
        
        // Applications on page
        $jobOnPage = 20;
        $page = $queryName[0];

        if(!empty($page)) {
            $from = $page * $jobOnPage - $jobOnPage;
        } else {
            $from = 0;
            $page = 1;
        }        
        
        // Get all applications based on [USER_ID] and from [$from]
        $array = $this->model->getApplications($from, $url[2]);
                
        $countJobs = $this->model->countApplications($url[2]);
        $pageCount = ceil($countJobs/$jobOnPage);
        
        $json = array();
        $i = 0;
        $json['pageCount'] = $pageCount;
        
        if($array) {
            foreach ($array as $offer) {
                $json['offers'][$i]['id'] = $offer['job_id'];
                $json['offers'][$i]['title'] = $offer['job_position'];
                $json['offers'][$i]['city'] = $this->model->getCityName($offer['job_city_id']);
                
                if($offer['job_volunteering']) {
                    $json['offers'][$i]['pay'] = "0.00 zł/g"; 
                } elseif($offer['job_hide_salary']) {
                    $json['offers'][$i]['pay'] = "?? zł/g";
                } else {
                    $json['offers'][$i]['pay'] = ceil($offer['job_rate_max'] + $offer['job_rate_min']) / 2 . " zł/g";
                }

                if($offer['job_contract_type_id']) {
                    $json['offers'][$i]['contractType'] = $offer['job_contract_type_id'];
                } else {
                    $json['offers'][$i]['contractType'] = "";
                }
                if($offer['job_trade_type_id']) {
                    $json['offers'][$i]['tradeType'] = $offer['job_trade_type_id'];
                } else {
                    $json['offers'][$i]['tradeType'] = "";
                }

                $i++;
            }
        } else {
            $json['offers'] = NULL;
        }
        
        if($array>0) {
            echo(json_encode($json));
        } elseif($array == -1) {
            $json = array('Error' => 1, 'Msg' => 'Brak użytkownika o takim ID.');
            echo(json_encode($json));
        } else {
            $json = array('Error' => 1, 'Msg' => 'Lista złożonych aplikacji jest pusta.');
            echo(json_encode($json));
        }

    }
    /*
     * favJob
     * 
     * /api/favJob/[$queryName]
     * 
     * /api/favJob/[API_KEY]
     * 
     * add job with [JOB_ID] to favourite 
     * [$queryName] is needed to work, and it's exploded to an array
     * 
     * [$queryName][0]      = [API_KEY]
     * [$queryName][1]      = [USER_ID]
     * [$queryName][2]      = [JOB_ID]
     * [$queryName][3]      = [CV_ID]
     * 
     */ 
    public function favJobAction($queryName = false) {
        if(!$queryName) { 
            die('Access denied!');
        } else {
            $url = explode('-', $queryName);
            if(!isset($url[0]) || $url[0]!=API_KEY) {
                die('Access denied!');
            }
        }
        
        $array = $this->model->favJob($url[1], $url[2]);
        
        if($array>0) {
            $json = array('Success' => 1, 'Msg' => 'Poprawnie dodano ofertę do ulubionych.');
        } elseif($array == -1) {
            $json = array('Error' => 1, 'Msg' => 'Brak użytkownika o takim ID.');
        } elseif($array == -2) {
            $json = array('Error' => 1, 'Msg' => 'Już dodałeś tę ofertę do ulubionych.');
        } else {
            $json = array('Error' => 1, 'Msg' => 'Inny błąd.');
        }
        
        echo(json_encode($json));
    }
                
    public function unfavJobAction($queryName = false) {
        if(!$queryName) { 
            die('Access denied!');
        } else {
            $url = explode('-', $queryName);
            if(!isset($url[0]) || $url[0]!=API_KEY) {
                die('Access denied!');
            }
        }
        
        $array = $this->model->unfavJob($url[1], $url[2]);
        
        if($array>0) {
            $json = array('Success' => 1, 'Msg' => 'Poprawnie usunięto ofertę z ulubionych.');
        } elseif($array == -1) {
            $json = array('Error' => 1, 'Msg' => 'Brak użytkownika o takim ID.');
        } elseif($array == -2) {
            $json = array('Error' => 1, 'Msg' => 'Nie lubisz tej oferty.');
        } else {
            $json = array('Error' => 1, 'Msg' => 'Inny bład.');
        }
        
        echo(json_encode($json));
    }
        
    public function favListAction($queryName = false) {
       if(!$queryName) { 
            die('Access denied!');
        } else {
            $url = explode('-', $queryName);
            if(!isset($url[1]) || $url[1]!=API_KEY) {
                die('Access denied!');
            }
        }
        
        $jobOnPage = 20;
        $page = $queryName[0];

        if(!empty($page)) {
            $from = $page * $jobOnPage - $jobOnPage;
        } else {
            $from = 0;
            $page = 1;
        }        

        $array = $this->model->getFav($from, $url[2]);
                
        $countJobs = $this->model->countFav($url[2]);
        $pageCount = ceil($countJobs/$jobOnPage);
        
        $json = array();
        $i = 0;
        $json['pageCount'] = $pageCount;
        
        if($array) {
            foreach ($array as $offer) {
                $json['offers'][$i]['id'] = $offer['job_id'];
                $json['offers'][$i]['title'] = $offer['job_position'];
                $json['offers'][$i]['city'] = $this->model->getCityName($offer['job_city_id']);
                
                if($offer['job_volunteering']) {
                    $json['offers'][$i]['pay'] = "0.00 zł/g"; 
                } elseif($offer['job_hide_salary']) {
                    $json['offers'][$i]['pay'] = "?? zł/g";
                } else {
                    $json['offers'][$i]['pay'] = ceil($offer['job_rate_max'] + $offer['job_rate_min']) / 2 . " zł/g";
                }

                if($offer['job_contract_type_id']) {
                    $json['offers'][$i]['contractType'] = $offer['job_contract_type_id'];
                } else {
                    $json['offers'][$i]['contractType'] = "";
                }
                if($offer['job_trade_type_id']) {
                    $json['offers'][$i]['tradeType'] = $offer['job_trade_type_id'];
                } else {
                    $json['offers'][$i]['tradeType'] = "";
                }

                $i++;
            }
        } else {
            $json['offers'] = NULL;
        }
        
        if($array>0) {
            echo(json_encode($json));
        } elseif($array == -1) {
            $json = array('Error' => 1, 'Msg' => 'Brak użytkownika o takim ID.');
            echo(json_encode($json));
        } else {
            $json = array('Error' => 1, 'Msg' => 'Lista ulubionych ofert jest pusta.');
            echo(json_encode($json));
        }

    }
    
    /*
    public function getInactiveCandidatesAction() {
        $r = $this->model->getInactiveCandidates();
        
        echo "<pre>"; print_r($r); echo "</pre>";
    }
    
    public function getUntagedCandidatesAction() {
        $r = $this->model->getUntaggedCandidates();
        
        echo "<pre>"; print_r($r); echo "</pre>";
    }     
    */
        
    public function mapaAction() {
        $mapaSrc = __DIR__.'/../../../upload/mapa.json';
        $file = file_get_contents($mapaSrc);
        echo $file;
    } 
}