<?php

namespace src\api\model;

use PDO;

class apiModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
    
    public function getJob($from = false, $to = false, $filter = false) {
        $month = date("Y-m-d H:i:s");
        
        if($filter) {
            $explode = explode('-',$filter);
            $trade = explode(',', $explode[0]);
            $contract = explode(',', $explode[1]);
            $time = explode(',', $explode[3]);
            $city = explode(',', $explode[2]);
            $rate = $explode[4];
        }
        
        #echo "<pre>"; print_r($explode); echo "</pre>";
        
        $sql = "SELECT DISTINCT j.job_main_page, j.job_employeer_place_direct_city, j.job_trade_type_id, j.job_contract_type_id, j.job_city_id, j.job_id, j.job_added_by, j.job_rate_min, j.job_rate_max, j.job_logo, j.job_volunteering, j.job_hide_salary, j.job_position, j.job_url_text, j.job_publish_date_end FROM ".DB_PREF."job as j LEFT JOIN ".DB_PREF."tag_job as t ON j.job_id = t.job_id WHERE j.job_status = 1 AND j.job_publish_date < NOW() AND job_publish_date_end > '".$month."'";
        
        // Miasta
        if($filter && !empty($city[0])) {
            $i = 1;
            $cityCount = count($city);
            foreach($city as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_province_id = :province".$c;
                if($i==$cityCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }        
        
        // Trade
        if($filter && !empty($trade[0])) {
            
            $i = 1;
            $tradeCount = count($trade);
            $sql .= " AND ( ";
            #echo $tradeCount;
            foreach($trade as $c=>$k) {
                $sql .= "t.tag_id IN ( :tag".$i." ) ";
                if($i<$tradeCount) {
                    $sql .= " OR ";
                }
                $i++;
            }
            $sql .= " ) ";
        }   
        
        // Trade
        if($filter && !empty($contract[0])) {
            $i = 1;
            $contractCount = count($contract);
            foreach($contract as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_contract_type_id = :contact".$c;
                if($i==$contractCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }        

        // Time
        if($filter && !empty($time[0])) {
            $i = 1;
            $timeCount = count($time);
            foreach($time as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_time = :time".$c;
                if($i==$timeCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }
        
        // Min. wynagrodzenie
        if($filter && !empty($rate)) {
            $sql .= " AND j.job_rate_min >= :rate ";
        }       
        $sql .= " AND j.job_delete = 0  AND j.job_publish_date_end > NOW() ORDER BY j.job_created_date DESC LIMIT :from, :to;";
        
        $pre = $this->database->prepare($sql);
        
        // Miasta
        if($filter && !empty($city[0])) {
            foreach($city as $c=>$k) {
                $pre->bindValue(':province'.$c, $this->getProvinceId($k), PDO::PARAM_INT);
            }
        }
        // Branża
        if($filter && !empty($trade[0])) {
            foreach($trade as $c=>$k) {
                $pre->bindValue(':tag'.++$c, $this->getTagId($k), PDO::PARAM_INT);
            }
        }
        // Kontrakt
        if($filter && !empty($contract[0])) {
            foreach($contract as $c=>$k) {
                $pre->bindValue(':contact'.$c, $this->getContractId($k), PDO::PARAM_INT);
            }
        }
        // Time
        if($filter && !empty($time[0])) {
            foreach($time as $c=>$k) {
                $pre->bindValue(':time'.$c, $k, PDO::PARAM_STR);
            }
        }
        // Time
        if($filter && !empty($rate)) {
            $pre->bindValue(':rate', $rate, PDO::PARAM_INT);
        }
        #die($sql);
        $pre->bindValue(':from', $from, PDO::PARAM_INT);
        $pre->bindValue(':to', $to, PDO::PARAM_INT);
        $pre->execute();
        
        if($pre->rowCount()!=0) {
            $jobList = $pre->fetchAll();
            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function getCityName($id) {
        $sql = "SELECT city_text FROM ".DB_PREF."city WHERE city_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['city_text'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
    
    public function countJobs($filter) {
        $month = date("Y-m-d H:i:s");
        if($filter) {
            $explode = explode('-',$filter);
            $trade = explode(',', $explode[0]);
            $contract = explode(',', $explode[1]);
            $time = explode(',', $explode[3]);
            $city = explode(',', $explode[2]);
            $rate = $explode[4];        
        }
        $sql = "SELECT DISTINCT count(j.job_id) as count FROM ".DB_PREF."job as j LEFT JOIN ".DB_PREF."tag_job as t ON j.job_id = t.job_id WHERE j.job_status = 1 AND j.job_publish_date < NOW() AND job_publish_date_end > '".$month."'";
        
        // Miasta
        if($filter && !empty($city[0])) {
            $i = 1;
            $cityCount = count($city);
            foreach($city as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_province_id = :city".$c;
                if($i==$cityCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }        
        
        // Trade
        if($filter && !empty($trade[0])) {
            
            $i = 1;
            $tradeCount = count($trade);
            $sql .= " AND ( ";
            #echo $tradeCount;
            foreach($trade as $c=>$k) {
                $sql .= "t.tag_id IN ( :tag".$i." ) ";
                if($i<$tradeCount) {
                    $sql .= " OR ";
                }
                $i++;
            }
            $sql .= " ) ";
        }   
        
        // Trade
        if($filter && !empty($contract[0])) {
            $i = 1;
            $contractCount = count($contract);
            foreach($contract as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_contract_type_id = :contact".$c;
                if($i==$contractCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }        

        // Time
        if($filter && !empty($time[0])) {
            $i = 1;
            $timeCount = count($time);
            foreach($time as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_time = :time".$c;
                if($i==$timeCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }
        
        // Min. wynagrodzenie
        if($filter && !empty($rate)) {
            $sql .= " AND j.job_rate_min >= :rate ";
        }       
        $sql .= " AND j.job_delete = 0";
        
        $pre = $this->database->prepare($sql);
        
        // Miasta
        if($filter && !empty($city[0])) {
            foreach($city as $c=>$k) {
                $pre->bindValue(':city'.$c, $this->getProvinceId($k), PDO::PARAM_INT);
            }
        }
        // Branża
        if($filter && !empty($trade[0])) {
            foreach($trade as $c=>$k) {
                $pre->bindValue(':tag'.++$c, $this->getTagId($k), PDO::PARAM_INT);
            }
        }
        // Kontrakt
        if($filter && !empty($contract[0])) {
            foreach($contract as $c=>$k) {
                $pre->bindValue(':contact'.$c, $this->getContractId($k), PDO::PARAM_INT);
            }
        }
        // Time
        if($filter && !empty($time[0])) {
            foreach($time as $c=>$k) {
                $pre->bindValue(':time'.$c, $k, PDO::PARAM_STR);
            }
        }
        // Time
        if($filter && !empty($rate)) {
            $pre->bindValue(':rate', $rate, PDO::PARAM_INT);
        }
        #$sql = "SELECT count(job_id) as count FROM ".DB_PREF."job WHERE job_status = 1 AND job_publish_date < NOW() AND job_delete = 0";
        
        $pre->execute();
        if($pre->rowCount()!=0) {
            $r = $pre->fetch();
            return $r['count'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
       

    public function getProvinceId($name) {
        $sql = "SELECT province_id FROM ".DB_PREF."province WHERE LOWER(REPLACE(province_text, ' ', '_')) = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($name));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['province_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
   
    public function getCityId($name) {
        $sql = "SELECT city_id FROM ".DB_PREF."city WHERE LOWER(REPLACE(city_text, ' ', '_')) = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($name));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['city_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function getTradeId($name) {
        $sql = "SELECT trade_id FROM ".DB_PREF."job_trade_types WHERE LOWER(REPLACE(trade_name, ' | ', '_')) = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($name));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['trade_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
        
    public function getTagId($name) {
        $sql = "SELECT tag_id FROM ".DB_PREF."tag WHERE LOWER(REPLACE(tag_name, ' | ', '_')) = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($name));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['tag_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    
    public function getContractId($name) {
        $sql = "SELECT type_id FROM ".DB_PREF."job_contract_types WHERE LOWER(REPLACE(type_name, ' ', '_')) = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($name));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['type_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function getOneJob($id) {

        $sql = "SELECT j.*, c.type_name as job_contract_type, t.trade_name as job_trade_type FROM ".DB_PREF."job as j LEFT JOIN ".DB_PREF."job_contract_types as c ON c.type_id = j.job_contract_type_id LEFT JOIN ".DB_PREF."job_trade_types as t ON t.trade_id = j.job_trade_type_id WHERE j.job_publish_date < NOW() AND j.job_id = ? LIMIT 1";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        if($pre->rowCount()!=0) {
            $jobList = $pre->fetch();
            $jobList['duties'] = $this->getDuties($jobList['job_id']);
            $jobList['needs'] = $this->getNeeds($jobList['job_id']);
            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }
    
    public function getDuties($id) {
        $sql = "SELECT duty_text FROM ".DB_PREF."job_duty WHERE duty_job_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }     
    }
         
    public function getNeeds($id) {
        $sql = "SELECT need_text FROM ".DB_PREF."job_need WHERE need_job_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }      
    }
    
    public function login($login, $pass, $push) {
        $sql = "SELECT user_active, user_id, user_email, user_push FROM ".DB_PREF."user WHERE AES_DECRYPT(user_email, '".SALT."') = ? AND user_password = AES_ENCRYPT(md5(?), '".SALT."');";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($login, $pass));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            if ($res['user_push'] != $push) {
                $this->updatePush($res['user_id'], $push);
            }
            if($res['user_active']) {
                return $res['user_id'];
            } else {
                return -1;
            }
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }
    }
    
    public function getUserId($email) {
        $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."')";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($email));            
        
        if($pre->rowCount()!=0) {
            $id = $pre->fetch();
            return $id['user_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function getCV($id) {

        if(!$id) return -1;
        
        $sql = "SELECT cv_id, cv_name FROM ".DB_PREF."user_cv WHERE cv_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetchAll();
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
        
    public function checkIfApplied($id, $uid) {
        $sql = "SELECT application_id FROM ".DB_PREF."application WHERE application_job_id = ? AND application_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id, $uid));
        if($pre->rowCount()==0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
    
    public function applyToJob($id, $job, $cv = false) {

        if(!$id) return -1;
        
        if(!$this->checkIfApplied($job, $id)) return -2;
            
        $sql = "INSERT INTO ".DB_PREF."application (application_user_id, application_job_id, application_date, application_cv) VALUES (?, ?, NOW(), ?)";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id, $job, $cv))) {
            $jobEmail = $this->getJobContactEmail($jid);
            
            $title = 'happinate.com - odpowiedź na Twoje ogłoszenie';
            $body = '<h3>Pracodawco, otrzymałeś aplikację na Twoje ogłoszenie.</h3>';
            $body .= '<p>Aby zobaczyć więcej szczegółów, zaloguj się na swoje konto oraz przejdź do zakładki "Moje ogłoszenia".</p>';
            $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz!</p>';  

            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($jobEmail, $title, $body, 'default.html');
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
    
        
    public function getJobContactEmail($id) {
        $sql = "SELECT AES_DECRYPT(user_email,'".SALT."') as email FROM ".DB_PREF."user as u LEFT JOIN ".DB_PREF."job as j ON u.user_id = j.job_added_by WHERE j.job_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($id));
        $res = $res->fetch();
        if(count($res)==1) {
            return $res['email'];
        } else {
            return false;
        }  
    }
    
    public function applyToOneclick($id, $job) {

        if(!$id) return -1;
        
        $sql = "UPDATE ".DB_PREF."job_oneclick SET status = 1 WHERE job_id = ? AND user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($job, $id))) {
            $jobEmail = $this->getJobContactEmail($job);
                        
            $title = 'happinate.com - potwierdzenie na ofertę 1-click';
            $body = '<h3>Pracodawco, kandydat, którym jesteś zainteresowany, potwierdził swoją gotowość do pracy.</h3>';
            $body .= '<p>Zaloguj się na swoje konto. W zakładce "Moje ogłoszenia" sprawdź aktualny status potwierdzeń.</p>';
            $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz!</p>';  

            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($jobEmail, $title, $body, 'default.html');
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
    
    public function getApplications($from, $id) {

        if(!$id) return -1;
        
        $sql = "SELECT j.* FROM ".DB_PREF."job as j LEFT JOIN ".DB_PREF."application as a ON a.application_job_id = j.job_id WHERE a.application_user_id = ? LIMIT ".$from.", 20";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetchAll();
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
        
         
    public function countApplications($id) {

        $sql = "SELECT count(application_id) as count FROM ".DB_PREF."application WHERE application_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['count'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
        
         
    public function checkIfAddedToFav($id, $uid) {
        $sql = "SELECT fav_id FROM ".DB_PREF."user_fav_job WHERE fav_job_id = ? AND fav_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id, $uid));
        if($pre->rowCount()==0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
          
    public function favJob($id, $job) {
        
        if(!$id) return -1;
        
        if(!$this->checkIfAddedToFav($job, $id)) return -2;
            
        $sql = "INSERT INTO ".DB_PREF."user_fav_job (fav_user_id, fav_job_id) VALUES (?, ?)";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id, $job))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
              
    public function unfavJob($id, $job) {
        
        if(!$id) return -1;
        
        if($this->checkIfAddedToFav($job, $id)) return -2;
            
        $sql = "DELETE FROM ".DB_PREF."user_fav_job WHERE fav_user_id = ? AND fav_job_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id, $job))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
    
     
    public function getFav($from, $id) {

        if(!$id) return -1;
        
        $sql = "SELECT j.* FROM ".DB_PREF."job as j LEFT JOIN ".DB_PREF."user_fav_job as a ON a.fav_job_id = j.job_id WHERE a.fav_user_id = ? LIMIT ".$from.", 20";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetchAll();
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
                 
    public function countFav($id) {

        $sql = "SELECT count(fav_id) as count FROM ".DB_PREF."user_fav_job WHERE fav_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['count'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
        
    public function checkIfFaved($jid, $uid) {
        $sql = "SELECT fav_id FROM ".DB_PREF."user_fav_job WHERE fav_user_id = ? AND  fav_job_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($uid, $jid));
        if($pre->rowCount()>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }
             
    public function checkIfOneclick($jid, $uid) {
        $sql = "SELECT id FROM ".DB_PREF."job_oneclick WHERE job_id = ? AND  user_id = ? AND status = 0";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($jid, $uid));
        if($pre->rowCount()>0) {
            
            $sql = "SELECT job_start_date, job_vacancies FROM ".DB_PREF."job WHERE job_id = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($jid));
            $res = $pre->fetch();
            
            $pre->closeCursor();
            
            $sql2 = "SELECT count(id) as count FROM ".DB_PREF."job_oneclick WHERE job_id = ? AND status = 1";
            $pre2 = $this->database->prepare($sql2);
            $pre2->execute(array($jid));
            $count = $pre2->fetch();

            if($res['job_vacancies']<=$count['count']) {
                return false;
            } else {
                if($res['job_start_date']!='0000-00-00') {
                    $st_dt = new \DateTime($res['job_start_date']);
                    $end_dt = new \DateTime(date("Y-m-d"));

                    if($st_dt < $end_dt) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            } 
            
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }
            
    public function getInactiveCandidates() {
        $sql = "SELECT AES_DECRYPT(user_email,'".SALT."') as email FROM ".DB_PREF."user WHERE user_active = 0";
        $pre = $this->database->prepare($sql);
        $pre->execute();
        $res = $pre->fetchAll();
        if($res) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
            
    public function getUntaggedCandidates() {
        $sql = "SELECT AES_DECRYPT(user_email,'".SALT."') as email, user_id FROM ".DB_PREF."user WHERE user_type = 1";
        $pre = $this->database->prepare($sql);
        $pre->execute();
        $res = $pre->fetchAll();
        if($res) {
            $array = array();
            foreach($res as $r) {
                if(!$this->countTags($r['user_id'])) {
                    $array[] = $r['email'];
                }
            }
            return $array;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    private function countTags($id) {
        $sql = "SELECT count(id) as count FROM ".DB_PREF."user_tag WHERE tag_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        $res = $pre->fetch();
        if($res['count']>2) {
            return true;
        } else {
            return false;
        }
    }
    
    private function updatePush($user_id, $user_push) {
        $sql = "UPDATE ".DB_PREF."user SET user_push = ? WHERE user_id = ?";
        $pre = $this->database->prepare($sql);
        $res = $pre->execute(array($user_push, $user_id));
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
}