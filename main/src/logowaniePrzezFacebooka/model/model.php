<?php

namespace src\logowaniePrzezFacebooka\model;

use app\helpers\Form;

class logowaniePrzezFacebookaModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }

    public function login($fbId) {
        $sql = "SELECT user_id, user_fb_id FROM ".DB_PREF."user WHERE user_fb_id = AES_ENCRYPT(?, '".SALT."') LIMIT 1";
        $res = $this->database->prepare($sql);
        $res->execute(array($fbId));
        $r = $res->fetch();
        
        if($res->rowCount()>0) {
            $userSalt = md5(microtime());
            $sql = "UPDATE ".DB_PREF."user SET user_salt = ?, user_ip = AES_ENCRYPT(?, '".SALT."') WHERE user_id = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($userSalt, $_SERVER['REMOTE_ADDR'], $r['user_id']));
            $_SESSION['userTokenFB'] = sha1($r['user_id'].$r['user_fb_id'].$userSalt);
            return true;
        } else {
            $pdoError = $this->database->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function checkEmailExist($email) {
        $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."') LIMIT 1";
        $res = $this->database->prepare($sql);
        $res->execute(array($email));
        $r = $res->fetch();
        
        if($res->rowCount()>0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function updateProfileWithFBInfo($info, $exists = false) {

        if($exists) {
            $sql = "UPDATE ".DB_PREF."user SET user_fb_id = AES_ENCRYPT(?, '".SALT."') WHERE user_email = AES_ENCRYPT(?, '".SALT."')";
            $res = $this->database->prepare($sql);

            $res->execute(array($info['id'], $info['email']));
            if($res->rowCount()>0) {
                return true;
            } else {
                $pdoError = $this->database->errorInfo();
                $this->error->sqlError($pdoError[2]);
                return false;
            }                   
        } else {
        
            if($info['picture']['data']['url']) {

                $extension = pathinfo($info['picture']['data']['url'], PATHINFO_EXTENSION);

                $fileName = sha1($info['id']).'.'.$extension;

                require __DIR__ . '/../../../app/libs/imageresizer/SimpleImage.php';
                $SimpleImage = new \SimpleImage();
                $SimpleImage->load($info['picture']['data']['url']); 
                $SimpleImage->resize(100, 100); 
                $SimpleImage->save(__DIR__. '/../../../upload/userLogo/' . $fileName);

            } else {
                $fileName = false;
            }
        
            $sql = "UPDATE ".DB_PREF."user SET user_fb_id = AES_ENCRYPT(?, '".SALT."'), user_name = AES_ENCRYPT(?, '".SALT."'), user_surname = AES_ENCRYPT(?, '".SALT."'), user_additional_mail = AES_ENCRYPT(?, '".SALT."'), user_adress_city  = ?, user_logo  = ? WHERE user_id = ?";
            $res = $this->database->prepare($sql);
            
            if(isset($fbInfo['location'])) {
                $city = explode(',', $fbInfo['location']['name']);
            } else {
                $city[0] = false;
            }
        
            $array = array(
                $info['id'],
                $info['first_name'],
                $info['last_name'],
                $info['email'],
                $city[0],
                $fileName,
                $this->getUserId()
            );

            #echo"<pre>";print_r($info);echo"</pre>";die();
            
            if($res->execute($array)) {
                return true;
            } else {
                $pdoError = $this->database->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }            
        }

    }
   public function getUserId() {

        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($_SESSION['userToken']));
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) { 
            $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($_SESSION['userTokenFB']));            
        } else {
            return false;
        }
        if($pre->rowCount()!=0) {
            $id = $pre->fetch();
            return $id['user_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }      
        
    } 
    public function registerViaFacebook($fbInfo) {

        if($fbInfo['picture']['data']['url']) {
            
            $extension = pathinfo($fbInfo['picture']['data']['url'], PATHINFO_EXTENSION);
            
            $fileName = sha1($fbInfo['id']).'.'.$extension;
            
            require __DIR__ . '/../../../app/libs/imageresizer/SimpleImage.php';
            $SimpleImage = new \SimpleImage();
            $SimpleImage->load($fbInfo['picture']['data']['url']); 
            $SimpleImage->resizeToWidth(240); 
            $SimpleImage->save(__DIR__. '/../../../upload/userLogo/' . $fileName);
            
        } else {
            $fileName = false;
        }
            
        if(isset($fbInfo['location'])) {
            $city = explode(',', $fbInfo['location']['name']);
        } else {
            $city[0] = false;
        }
        
        $password = Form::generatePassword();
        
        $sql = "INSERT INTO ".DB_PREF."user (user_password, user_fb_id, user_email, user_name, user_surname, user_type, user_register_date, user_logo, user_adress_city, user_active) VALUES (AES_ENCRYPT(md5(?), '".SALT."'),AES_ENCRYPT(?, '".SALT."'),AES_ENCRYPT(?, '".SALT."'),AES_ENCRYPT(?, '".SALT."'),AES_ENCRYPT(?, '".SALT."'), ?, ?, ?, ?, 1);";
        $pre = $this->database->prepare($sql);
        if($pre->execute(array($password, $fbInfo['id'], $fbInfo['email'], $fbInfo['first_name'], $fbInfo['last_name'], 1, date("Y-m-d H:i:s"), $fileName, $city[0]))) {

            $title = 'happinate.com - witamy!';
            $body = '<h3>Witamy w happinate.com! </h3>';
            $body .= '<p>Cieszymy się, że dołączyłeś do Nas. Zaloguj się na Naszej stronie: <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'" target="_blank">happinate.com</a></p>';
            $body .= '<p>Wybierz rodzaje prac, które Cię interesują, a oferty same przyjdą do Ciebie.</p>';
            $body .= '<p>Twoje tymczasowe hasło to: '.$password.'</p>';
            $body .= '<p>Możesz je zmienić w swoim profilu</p>';
            $body .= '<p>Pozdrawiamy, happinate.com - <br/>praca od zaraz!</p>';
            
            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($fbInfo['email'], $title, $body);
            
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
}