<?php
namespace src\logowaniePrzezFacebooka;

use app\helpers\FlashMsg;
use src\BaseController;

require __DIR__ . '/../../../app/libs/facebook/facebook.php';

class logowaniePrzezFacebookaController extends BaseController {
    
    private $template;
    private $model;
    private $user;

    public function __construct($template, $model, $user) {
        $this->template = $template;
        $this->model = $model;
        $this->user = $user;
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        
        /* Facebook Login */
        $facebook = new \Facebook(array(
          'appId'  => FB_ID,
          'secret' => FB_SECRET_KEY,
        ));
        
        $user = $facebook->getUser();

        if ($user) {
            try {
                if($this->user && $this->user->checkUser()) {
                    $user_profile = $facebook->api('/me?fields=id,name,picture.type(large),work,education,address,email,first_name,last_name,location');
 
                    if($this->model->updateProfileWithFBInfo($user_profile)) {
                        FlashMsg::add('success', 'Udało się połączyć dane z konta Facebookowego!');
                        header("Location: " . HOST. "moje-konto");
                    } else {
                        FlashMsg::add('error', 'Nie udało się połączyć danych z konta Facebookowego!');
                        header("Location: " . HOST. "moje-konto");
                    }
                } else {
                    $user_profile = $facebook->api('/me?fields=id,name,picture.type(large),work,education,address,email,first_name,last_name,location');

                    if($this->model->login($user)) {
                        FlashMsg::add('success', 'Udało sie poprawnie zalogować!');
                        header("Location: " . HOST. "moje-konto");
                    } elseif(!$this->model->checkEmailExist($user_profile['email'])) {
                        if($this->model->registerViaFacebook($user_profile) && $this->model->login($user)) {
                            FlashMsg::add('success', 'Udało sie poprawnie zalogować!');
                            header("Location: " . HOST. "moje-konto");                            
                        } else {
                            FlashMsg::add('error', 'Nie udało się zalogować poprzez Facebooka!');
                            header("Location: " . HOST);                            
                        }
                    } else {
                        if($this->model->updateProfileWithFBInfo($user_profile, true)) {
                            if($this->model->login($user)) {
                                FlashMsg::add('success', 'Udało sie poprawnie zalogować!');
                                header("Location: " . HOST. "moje-konto");                            
                            } else {
                                FlashMsg::add('error', 'Nie udało się zalogować poprzez Facebooka!');
                                header("Location: " . HOST);                            
                            }
                        } else {
                            FlashMsg::add('error', 'Nie udało się połączyć danych z konta Facebookowego!');
                            header("Location: " . HOST);
                        }                        
                    }
                }
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = null;
            }
        }
    }
}