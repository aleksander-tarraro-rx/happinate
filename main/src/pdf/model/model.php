<?php

namespace src\pdf\model;

class pdfModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
    
    public function getEmployeeProfile($id = false) {
        $sql = "SELECT 
            u.user_id,
            u.user_logo,
            AES_DECRYPT(u.user_email, '".SALT."') as user_email, 
            AES_DECRYPT(u.user_name, '".SALT."') as user_name,
            AES_DECRYPT(u.user_surname, '".SALT."') as user_surname,
            AES_DECRYPT(u.user_pesel, '".SALT."') as user_pesel,
            AES_DECRYPT(u.user_phone, '".SALT."') as user_phone,
            u.user_adress_city,
            u.user_date_of_birth,
            AES_DECRYPT(u.user_adress_zip_code, '".SALT."') as user_adress_zip_code,
            AES_DECRYPT(u.user_adress_street, '".SALT."') as user_adress_street,
            u.user_student,
            u.user_uncapable,
            u.user_worknow,
            u.user_sms,
            u.user_main_tag_id,
            u.user_hobby,
            s.*
            FROM ".DB_PREF."user as u LEFT JOIN ".DB_PREF."user_other_skill as s ON s.skill_user_id = u.user_id";
        
        if($id) {
            $sql .= " WHERE user_id = ?";
        } else {
            if(isset($_SESSION['userTokenFB']) && !empty($_SESSION['userTokenFB'])) {
                $sql .= " WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            } else {
                $sql .= " WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            }
        }
        
        $res = $this->database->prepare($sql);
        if($id) {
            $res->execute(array($id));
        } else {
            if(isset($_SESSION['userTokenFB']) && !empty($_SESSION['userTokenFB'])) {
                $res->execute(array($_SESSION['userTokenFB']));
            } else {
                $res->execute(array($_SESSION['userToken']));
            } 
        }
        

        if($res->rowCount()) {
            $array = array();
            $array = $res->fetch();
            return $array;
        } else {
            return true;
        }       
    }
    
    public function getExperience($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_experience WHERE experience_user_id = ? ORDER BY experience_till_now DESC, experience_end DESC";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    
    public function getEducation($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_education WHERE education_user_id = ? ORDER BY education_till_now DESC, education_end DESC";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }

    public function getLang($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_lang WHERE lang_user_id = ? ORDER BY lang_name DESC";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function getTraining($id = false) {
        $sql = "SELECT * FROM ".DB_PREF."user_training WHERE training_user_id = ? ORDER BY training_end DESC";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function getSkill($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_other_skill WHERE skill_user_id";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function getOtherSkill($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_skill WHERE skill_user_id = ? ORDER BY skill_name DESC";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
                          
    public function getUserCVColors($id) {
        $sql = "SELECT user_cv_color, user_bgcv_color FROM ".DB_PREF."user WHERE user_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
}