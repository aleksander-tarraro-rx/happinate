<?php

namespace src\pdf;

/*
 * CONTROLLER 
 * 
 * api
 * 
 * !IMPORTANT
 * 
 * This is only used by Android App to get some informations from database.
 * It needs [API_KEY] to work.
 * It's need to be improved or redesinged.
 * All returned data are in JSON.
 * 
 */

use app\helpers\FlashMsg;
use src\BaseController;

class pdfController extends BaseController {
    
    private $template;
    private $model;
    private $user;
    private $userType;

    public function __construct($template, $model, $user) {
        $this->template = $template;
        $this->model = $model;
        $this->user = $user;

        if($this->user)
            $this->userType = $user->getUserType();
        
        $this->template->assign('model', $this->model);
    }
    
    /* 
     * INDEX ACTION
     * 
     * /api
     * 
     * DOES NOTHING
     * 
     * redirect to home page
     */
    public function indexAction($queryName = false) {
        /* Zabezpiecznie */
        if($this->userType!=1) {
            FlashMsg::add('error', 'Nie masz uprawnień do przeglądania tej strony.');
            header("Location: " . HOST . "moje-konto"); 
        }
        
        require __DIR__ . '/../../../app/libs/dompdf/dompdf_config.inc.php';
      
        $html = file_get_contents('http://'.$_SERVER['SERVER_NAME'].HOST.'pdf/create/'.$this->user->getUserId());

        $dompdf = new \DOMPDF();
        $dompdf->load_html($html, 'utf-8');
        $dompdf->render();
        $dompdf->stream("cv - happinate.pdf");
    }
    
    public function createAction($id = false) {

        $profil = $this->model->getEmployeeProfile($id);
        $this->template->assign('profile', $profil);

        $userExperience = $this->model->getExperience($profil['user_id']);
        $this->template->assign('userExperience', $userExperience);

        $userEducation = $this->model->getEducation($profil['user_id']);
        $this->template->assign('userEducation', $userEducation);

        $userLangs = $this->model->getLang($profil['user_id']);
        $this->template->assign('userLangs', $userLangs);

        $userTraining = $this->model->getTraining($profil['user_id']);
        $this->template->assign('userTraining', $userTraining);

        $userSkill = $this->model->getSkill($profil['user_id']);
        $this->template->assign('userSkill', $userSkill);

        $userOtherSkill = $this->model->getOtherSkill($profil['user_id']);
        $this->template->assign('userOtherSkill', $userOtherSkill);

        $userCVColorsTmp = $this->model->getUserCVColors($profil['user_id']);
        $userCVColors = explode(',',$userCVColorsTmp['user_cv_color']);
        $userBgCVColors = explode(',',$userCVColorsTmp['user_bgcv_color']);

        $this->template->assign('userCVColors', $userCVColors);
        $this->template->assign('userBgCVColors', $userBgCVColors);
        
        $this->template->display('pdf/cv.tpl');
    }
    
}