<?php
namespace src\profil;

use app\helpers\FlashMsg;
use src\BaseController;

class profilController extends BaseController {
    
    private $template;
    private $model;
    private $user;
    private $userType;
    private $userId;

    public function __construct($template, $model, $user) {
        $this->template = $template;
        $this->model = $model;
        $this->user = $user;
        
        if($this->user) {
            $this->userType = $user->getUserType();
            $this->userId = $user->getUserId();
            $this->template->assign('uid', $this->userId);
            $this->template->assign('userType', $this->userType);
        }
        
        $this->template->assign('user', $this->user);
        $this->template->assign('model', $this->model);
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        if($this->userType==2) {
            $profile = $this->model->getProfile($this->user->getUserId());
            $this->template->assign('profile', $profile);
            
            $jobList = $this->model->getJobList($profile['user_id']);
            $this->template->assign('jobList', $jobList);
            
            $this->template->display('profil/index.tpl');
        } elseif($this->userType==1) {
            
            $profile = $this->model->getProfile($this->user->getUserId());
            $this->template->assign('profile', $profile);
            
            $userTags = $this->model->getUserTags($profile['user_id']);
            $this->template->assign('userTags', $userTags);    
            
            $userExperience = $this->model->getExperience($profile['user_id']);
            $this->template->assign('userExperience', $userExperience);

            $userEducation = $this->model->getEducation($profile['user_id']);
            $this->template->assign('userEducation', $userEducation);

            $userLangs = $this->model->getLang($profile['user_id']);
            $this->template->assign('userLangs', $userLangs);

            $userTraining = $this->model->getTraining($profile['user_id']);
            $this->template->assign('userTraining', $userTraining);

            $userSkill = $this->model->getSkill($profile['user_id']);
            $this->template->assign('userSkill', $userSkill);

            $userOtherSkill = $this->model->getOtherSkill($profile['user_id']);
            $this->template->assign('userOtherSkill', $userOtherSkill);
            
            $userCVColorsTmp = $this->model->getUserCVColors($profile['user_id']);
            $userCVColors = explode(',',$userCVColorsTmp['user_cv_color']);
            $userBgCVColors = explode(',',$userCVColorsTmp['user_bgcv_color']);

            $this->template->assign('userCVColors', $userCVColors);
            $this->template->assign('userBgCVColors', $userBgCVColors);
            
            $this->template->assign('profil', true);
            $this->template->assign('myownprofile', true);
            
            $this->template->display('profil/cv.tpl');
        } else {
            header("Location:".HOST);
        }
    }    
    
    /* Index action */
    public function pAction($queryName = false) {
        if(!$queryName) {
            header("Location:".HOST);
        } else {
            if(isset($_POST['inviteFriend']) && isset($_POST['friends']) && !empty($_POST['friends'])) {
                if($this->model->sendProfilToFriend($queryName, $_POST['friends'])) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie wysłać polecenie.');
                    header("Location: " . HOST . "profil/p/".$queryName);
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się poprawnie wysłać polecenie.');
                    header("Location: " . HOST . "profil/p/".$queryName);
                }
            }
            
            $profile = $this->model->getProfile(false, $queryName);
            
            if(!$profile) header("Location:".HOST);
            
            if($profile['user_type']==1) header("Location:".HOST."profil/k/".$queryName);
            
            $jobList = $this->model->getJobList($profile['user_id']);

            $watchers = $this->model->getWatchers($profile['user_id']);
            
            $this->template->assign('watchers', $watchers);

            $this->template->assign('profile', $profile);
            $this->template->assign('jobList', $jobList);
            $this->template->display('profil/index.tpl');
        }
    }
    
    
    /* Index action */
    public function kAction($queryName = false) {
        if(!$queryName) {
            header("Location:".HOST);
        } else {
            if($this->user && $this->userType==1) {
                $friendList = $this->model->getListEmp($this->userId);
                $this->template->assign('friendList', $friendList);
            }
            
            if(isset($_POST['inviteFriend']) && isset($_POST['cand']) && !empty($_POST['cand'])) {
                if($this->model->sendProfilToEmployeer($queryName, $_POST)) {
                    FlashMsg::add('success', 'Udało Ci się poprawnie wysłać polecenie.');
                    header("Location: " . HOST . "profil/p/".$queryName);
                } else {
                    FlashMsg::add('error', 'Nie udało Ci się poprawnie wysłać polecenie.');
                    header("Location: " . HOST . "profil/p/".$queryName);
                }
            }
            
            $profile = $this->model->getProfile(false, $queryName);
            
            if(!$profile) header("Location:".HOST);
            
            if($profile['user_type']==2)  header("Location:".HOST."profil/p/".$queryName);
            
            
            if(is_object($this->user) && $profile['user_id'] == $this->user->getUserId()) {
                $this->template->assign('myownprofile', true);
            } else {
                $this->template->assign('myownprofile', false);
            }
            
            $this->template->assign('profile', $profile);
            
            $userExperience = $this->model->getExperience($profile['user_id']);
            $this->template->assign('userExperience', $userExperience);

            $userEducation = $this->model->getEducation($profile['user_id']);
            $this->template->assign('userEducation', $userEducation);

            $userLangs = $this->model->getLang($profile['user_id']);
            $this->template->assign('userLangs', $userLangs);

            $userTraining = $this->model->getTraining($profile['user_id']);
            $this->template->assign('userTraining', $userTraining);

            $userSkill = $this->model->getSkill($profile['user_id']);
            $this->template->assign('userSkill', $userSkill);

            $userOtherSkill = $this->model->getOtherSkill($profile['user_id']);
            $this->template->assign('userOtherSkill', $userOtherSkill);
            
            $userCVColorsTmp = $this->model->getUserCVColors($profile['user_id']);
            $userCVColors = explode(',',$userCVColorsTmp['user_cv_color']);
            $userBgCVColors = explode(',',$userCVColorsTmp['user_bgcv_color']);

            $this->template->assign('userCVColors', $userCVColors);
            $this->template->assign('userBgCVColors', $userBgCVColors);
            
            $this->template->display('profil/cv.tpl');
        }
    }
    
    /* Index action */
    public function ajaxAction($queryName = false) {
        
        if(isset($_POST['watchUser']) && isset($_POST['id']) && is_numeric($_POST['id'])) {
            $this->model->watchUser($_POST['watchUser'], $_POST['id'], $this->userId);
        }
        
        die();
    }
    
    
}