<?php

namespace src\profil\model;

class profilModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
    
    public function getProfile($id = false, $token = false) {
        if($id) {
            $sql = "SELECT AES_DECRYPT(user_name, '".SALT."') as user_name, AES_DECRYPT(user_surname, '".SALT."') as user_surname, user_main_tag_id, user_id, user_type, user_hobby, user_cv_color, user_bgcv_color, user_date_of_birth, user_profile_yt_movie, user_profile_url, user_profile_img, user_profile_img_info, user_profile_name, user_info, AES_DECRYPT(user_email, '".SALT."') as user_email, user_logo, user_adress_city, AES_DECRYPT(user_adress_zip_code, '".SALT."') as user_adress_zip_code, AES_DECRYPT(user_adress_street, '".SALT."') as user_adress_street, user_www, user_fanpage,  AES_DECRYPT(user_phone, '".SALT."') as user_phone FROM ".DB_PREF."user WHERE user_id = ?";
              
            $pre = $this->database->prepare($sql);
            $pre->execute(array($id));
            if($pre->rowCount()!=0) {
                return $pre->fetch();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }   
        } elseif($token) {
            $sql = "SELECT AES_DECRYPT(user_name, '".SALT."') as user_name, AES_DECRYPT(user_surname, '".SALT."') as user_surname, user_id, user_type, user_hobby, user_cv_color, user_bgcv_color, user_date_of_birth, user_profile_yt_movie, user_profile_url, user_profile_img, user_profile_img_info, user_profile_name, user_info, AES_DECRYPT(user_email, '".SALT."') as user_email, user_logo, user_adress_city, AES_DECRYPT(user_adress_zip_code, '".SALT."') as user_adress_zip_code, AES_DECRYPT(user_adress_street, '".SALT."') as user_adress_street, user_www, user_fanpage,  AES_DECRYPT(user_phone, '".SALT."') as user_phone FROM ".DB_PREF."user WHERE (md5(user_id) = ? OR user_profile_url = ?)";
              
            $pre = $this->database->prepare($sql);
            $pre->execute(array($token, $token));
            if($pre->rowCount()!=0) {
                $r = $pre->fetch();
                if($r['user_profile_url'] && ($token!=$r['user_profile_url'])) { 
                    header("Location:".HOST."profil/p/".$r['user_profile_url']);
                } else {
                    return $r;
                }
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }   
        } else {
            return false;
        }
    }
    
    public function getJobList($id) {
        $month = date("Y-m-d H:i:s");
        $sql = "SELECT * FROM ".DB_PREF."job WHERE job_status = 1 AND job_added_by = ? AND job_publish_date_end > '".$month."' AND job_delete = 0";

        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }
    

    public function getExperience($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_experience WHERE experience_user_id = ? ORDER BY experience_till_now DESC, experience_end DESC";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    
    public function getEducation($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_education WHERE education_user_id = ? ORDER BY education_till_now DESC, education_end DESC";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }

    public function getLang($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_lang WHERE lang_user_id = ? ORDER BY lang_name DESC";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function getTraining($id = false) {
        $sql = "SELECT * FROM ".DB_PREF."user_training WHERE training_user_id = ? ORDER BY training_end DESC";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function getSkill($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_other_skill WHERE skill_user_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function getOtherSkill($id) {
        $sql = "SELECT * FROM ".DB_PREF."user_skill WHERE skill_user_id = ? ORDER BY skill_name DESC";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
                          
    public function getUserCVColors($id) {
        $sql = "SELECT user_cv_color, user_bgcv_color FROM ".DB_PREF."user WHERE user_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
                              
    public function getUserTags($id) {
        $sql = "SELECT tag_id FROM ".DB_PREF."user_tag WHERE tag_user_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
                                  
    public function getTagName($id) {
        $sql = "SELECT tag_name FROM ".DB_PREF."tag WHERE tag_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return $res['tag_name'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
                          
    public function watchUser($val, $id, $uid) {
        if($val) {
            $sql = "INSERT INTO ".DB_PREF."user_watch (user_id, watch_id) VALUES (?, ?)";
        } else {
            $sql = "DELETE FROM ".DB_PREF."user_watch WHERE user_id = ? AND watch_id = ?";
        }
        
        $pre = $this->database->prepare($sql);

        if($pre->execute(array($uid, $id))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }    
                          
    public function checkWatch($id, $uid) {
        $sql = "SELECT id FROM ".DB_PREF."user_watch WHERE user_id = ? AND watch_id = ?";
        
        $pre = $this->database->prepare($sql);
        $pre->execute(array($uid, $id));
        
        if($pre->rowCount()>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function getWatchers($id) {
        $sql = "SELECT count(*) as `count` FROM ".DB_PREF."user_watch WHERE watch_id = ?";
        
        $res = $this->database->prepare($sql);
        $res->execute(array($id));
        
        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['count'];
        } else {
            return 0;
        }
    }
    
    public function getUserEmail() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT AES_DECRYPT(user_email, '".SALT."') as user_email FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));          
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT AES_DECRYPT(user_email, '".SALT."') as user_email FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));        
        } else {
            return false;
        }

        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_email'];
        }
    }
    
    public function sendProfilToFriend($q, $array) {
        
            $friendEmail = $this->getUserEmail();
            
            $body = '<h3>Kandydacie</h3><p>Twój znajomy '.$friendEmail.' przesyła Tobie ciekawą wizytówkę pracodawcy. Sprawdź ją i bądź na bieżąco z ofertami prac!</p>';
            $body .= '<p>Link do wizytówki: <a href="http://www.'.$_SERVER['SERVER_NAME'].HOST.'profil/p/'.$q.'">WIZYTÓWKA PRACODAWCY</a></p>';
            $title = 'happinate.com – znajomy przesyła Ci wizytówkę pracodawcy';

            $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz</p>';

            $_emailClass = $GLOBALS['_emailClass'];
            
            foreach($array as $email) {
                $_emailClass->addToList($email, $title, $body);
            } 
            
            return true;
    }
    
    public function getListEmp($id) {
        $sql = "SELECT AES_DECRYPT(u.user_email, '".SALT."') as user_email, u.user_profile_name FROM ".DB_PREF."application_oneclick as o LEFT JOIN ".DB_PREF."user as u ON u.user_id = o.oneclick_employeer_id WHERE o.oneclick_employee_id = ?";
        
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        
        if($pre->rowCount()>0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function sendProfilToEmployeer($q, $array) {
        
            $friendEmail = $this->getUserEmail();
            
            $body1 = '<h3>Pracodawca</h3><p>Użytkownik '.$friendEmail.' poleca Tobie kandydata '.$array['name'].' '.$array['email'].', sprawdź jego doświadczenie zawodowe klikając w poniższy link!</p>';
            $body1 .= '<p>Link do wizytówki: <a href="http://www.'.$_SERVER['SERVER_NAME'].HOST.'profil/k/'.$q.'">WIZYTÓWKA KANDYDATA</a></p>';
            if(isset($_POST['info'])) {
                $body1 .= '<p>Wiadomość od użytkownika:</p>';
                $body1 .= '<p>'.$_POST['info'].'</p>';
            }
            $title1 = 'happinate.com – pracodawco '.$friendEmail.' poleca kandydata';
            $body1 .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz</p>';
            
            $body2 = '<h3>Kandydacie</h3><p>Twój znajomy '.$friendEmail.' polecił Twoją osobę pracodawcom!</p>';
            $title2 = 'happinate.com – Twój znajomy '.$friendEmail.' polecił Ciebie pracodawcy';
            $body2 .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz</p>';

            $_emailClass = $GLOBALS['_emailClass'];
            
            foreach($array['cand'] as $email) {
                $_emailClass->addToList($email, $title1, $body1);
            } 
            
            $_emailClass->addToList($array['email'], $title2, $body2);
            
            return true;
    }
}
