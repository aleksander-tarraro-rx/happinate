<?php
namespace src\cennik;

use src\BaseController;

class cennikController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        $this->template->display('cennik/index.tpl');
    }
}