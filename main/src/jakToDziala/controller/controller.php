<?php
namespace src\jakToDziala;

use src\BaseController;

class jakToDzialaController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        $this->template->display('jakToDziala/index.tpl');
    }
}