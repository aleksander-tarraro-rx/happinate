<?php
namespace src\android;

use src\BaseController;

class androidController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        header("Location:https://play.google.com/store/apps/details?id=com.moodup.happinate");
    }    

}