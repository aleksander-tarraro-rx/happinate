<?php
namespace src\pomoc;

use src\BaseController;

class pomocController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        $this->template->display('pomoc/index.tpl');
    }
}