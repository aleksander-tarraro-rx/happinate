<?php
namespace src\qrcode;

use src\BaseController;

class qrcodeController extends BaseController {
    
    private $template;

    public function __construct($template) {
        $this->template = $template;
    }
    
    /* Index action */
    public function indexAction() {
        header('Location:'.HOST);
    }
    
    
    /* Index action */
    public function qponyAction() {
        $this->template->display('analitycs.tpl');
        header('Location:'.HOST);
    }
}