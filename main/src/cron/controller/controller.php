<?php

namespace src\cron;

/*
 * CONTROLLER 
 * 
 * api
 * 
 * !IMPORTANT
 * 
 * This is only used by Android App to get some informations from database.
 * It needs [API_KEY] to work.
 * It's need to be improved or redesinged.
 * All returned data are in JSON.
 * 
 */

class cronController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* 
     * INDEX ACTION
     */
    public function indexAction($queryName = false) {
        header("Location:" . HOST);
    }
    
    /* 
     * email
     */  
    public function dailyStatisticsAction($queryName = false) {
        if(!$queryName) { 
            // If $queryName is empty
            die('Access denied!');
        } else {
            if(!isset($queryName) || $queryName!=API_KEY) {
                die('Access denied!');
            }
        }
        
        $dailyStatistic = $this->model->getDailyStatistic();
        
        $body = '<h3>Statystyki ogólne:</h3>';
        $body .= '<p><b>&mdash; Zarejestrowani użytkownicy: </b>'.$dailyStatistic['uzytkownicy']['alltime']['wszyscy'].'</p>';
        $body .= '<p><b>&mdash; Zarejestrowani pracodawcy: </b>'.$dailyStatistic['uzytkownicy']['alltime']['pracodawcy'].'</p>';
        $body .= '<p><b>&mdash; Zarejestrowani kandydaci: </b>'.$dailyStatistic['uzytkownicy']['alltime']['pracownicy'].'</p>';
        $body .= '<p><b>&mdash; Ogłoszenia: </b>'.$dailyStatistic['ogloszenia']['alltime']['wszystkie'].'</p>';
        $body .= '<p><b>&mdash; Liczba aplikacji: </b>'.$dailyStatistic['aplikacje']['alltime'].'</p>';
        $body .= '<p><b>&mdash; Średnio aplikacji na ogłoszenie: </b>'.$dailyStatistic['srednia']['alltime'].'</p>';
        
        $body .= '<h3>Statystyki z wczoraj ('.date('Y-m-d').'):</h3>';
        $body .= '<p><b>&mdash; Zarejestrowani użytkownicy: </b>'.$dailyStatistic['uzytkownicy']['wczoraj']['wszyscy'].'</p>';
        $body .= '<p><b>&mdash; Zarejestrowani pracodawcy: </b>'.$dailyStatistic['uzytkownicy']['wczoraj']['pracodawcy'].'</p>';
        $body .= '<p><b>&mdash; Zarejestrowani kandydaci: </b>'.$dailyStatistic['uzytkownicy']['wczoraj']['pracownicy'].'</p>';
        $body .= '<p><b>&mdash; Ogłoszenia: </b>'.$dailyStatistic['ogloszenia']['wczoraj']['wszystkie'].'</p>';
        $body .= '<p><b>&mdash; Liczba aplikacji: </b>'.$dailyStatistic['aplikacje']['wczoraj'].'</p>';
        $body .= '<p><b>&mdash; Średnio aplikacji na ogłoszenie: </b>'.$dailyStatistic['srednia']['wczoraj'].'</p>';
        
        
        $body .= '<h1>MIŁEGO DNIA :)</h1>';

        $_emailClass = $GLOBALS['_emailClass'];
        $_emailClass->addToList('ola.tokarewicz@gmail.com', 'happinate - statystyki', $body);
        $_emailClass->addToList('jagodapiescicka@gmail.com', 'happinate - statystyki', $body);

    }
    
    /* 
     * email
     */
    public function emailAction($queryName = false) {
        if(!$queryName) { 
            // If $queryName is empty
            die('Access denied!');
        } else {
            $url = explode('-', $queryName);
            // Check [API_KEY]
            if(!isset($url[0]) || $url[0]!=API_KEY) {
                die('Access denied!');
            }
        }
        
        $this->model->sendAllEmails();
    }
     
    /* Mapa */
    public function mapaAction($queryName = false) {
        
        if(!$queryName) { 
            // If $queryName is empty
            die('Access denied!');
        } else {
            if(!isset($queryName) || $queryName!=API_KEY) {
                die('Access denied!');
            }
        }
        
        $mapa = $this->model->getMapaData();

        //echo "<pre>"; var_dump($mapa); echo "</pre>"; die();
        
        $json = json_encode($mapa);

        $fileSRC = __DIR__ . "/../../../upload/mapa.json";
        
        if(file_exists($fileSRC)) {
            unlink($fileSRC);
            $fp = fopen($fileSRC, 'w');
            fwrite($fp, $json);
            fclose($fp);
        } else { 
            $fp = fopen($fileSRC, 'w');
            fwrite($fp, $json);
            fclose($fp);
        }

    }
}