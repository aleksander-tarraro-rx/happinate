<?php

namespace src\cron\model;

use app\helpers\Email;

class cronModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
    
    public function getDailyStatistic() {
        $from = date("Y-m-d", time() - 60 * 60 * 24)." 00:00:01";
        $to = date("Y-m-d", time() - 60 * 60 * 24)." 23:59:59";
        
        $stats = array();
        
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_type = 1 OR user_type = 2");
        $pre->execute();
        $res = $pre->fetch();
        $stats['uzytkownicy']['alltime']['wszyscy'] = $res['count'];
        $pre->closeCursor();

        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_type = 2");
        $pre->execute();
        $res = $pre->fetch();
        $stats['uzytkownicy']['alltime']['pracodawcy'] = $res['count'];
        $pre->closeCursor();
        
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_type = 1");
        $pre->execute();
        $res = $pre->fetch();
        $stats['uzytkownicy']['alltime']['pracownicy'] = $res['count'];
        $pre->closeCursor();
                
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."application");
        $pre->execute();
        $res = $pre->fetch();
        $stats['aplikacje']['alltime'] = $res['count'];
        $pre->closeCursor();  
        
        // Users All
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE (user_type = 1 OR user_type = 2) AND user_register_date > '".$from."' AND user_register_date > '".$to."'");
        $pre->execute();
        $res = $pre->fetch();
        $stats['uzytkownicy']['wczoraj']['wszyscy'] = $res['count'];
        $pre->closeCursor();
        
        // Users All
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_type = 2 AND user_register_date > '".$from."' AND user_register_date > '".$to."'");
        $pre->execute();
        $res = $pre->fetch();
        $stats['uzytkownicy']['wczoraj']['pracodawcy'] = $res['count'];
        $pre->closeCursor();
        
        // Users All
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_type = 1 AND user_register_date > '".$from."' AND user_register_date > '".$to."'");
        $pre->execute();
        $res = $pre->fetch();
        $stats['uzytkownicy']['wczoraj']['pracownicy'] = $res['count'];
        $pre->closeCursor();
        
        // Jobs all
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."job WHERE job_delete = 0");
        $pre->execute();
        $res = $pre->fetch();
        $stats['ogloszenia']['alltime']['wszystkie'] = $res['count'];
        $pre->closeCursor();  
        
        // Jobs all
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."job WHERE job_publish_date_end < ? AND job_delete = 0 AND job_created_date > '".$from."' AND job_created_date > '".$to."'");
        $pre->execute(array(date('Y-m-d H:i:s', strtotime("+30 days"))));
        $res = $pre->fetch();
        $stats['ogloszenia']['wczoraj']['wszystkie'] = $res['count'];
        $pre->closeCursor();  

        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."application WHERE application_date > '".$from."' AND application_date > '".$to."'");
        $pre->execute();
        $res = $pre->fetch();
        $stats['aplikacje']['wczoraj'] = $res['count'];
        $pre->closeCursor();
        
        $stats['srednia']['alltime'] = $stats['aplikacje']['alltime'] / $stats['ogloszenia']['alltime']['wszystkie'];
        
        if($stats['ogloszenia']['wczoraj']['wszystkie']) {
            $stats['srednia']['wczoraj'] = $stats['aplikacje']['wczoraj'] / $stats['ogloszenia']['wczoraj']['wszystkie'];
        } else {
            $stats['srednia']['wczoraj'] = 0;
        }
        
        if(!$stats['srednia']['wczoraj']) {
             $stats['srednia']['wczoraj'] = 0;
        }
        return $stats;
    }
    
    public function sendAllEmails($limit = 50) {
        $pre = $this->database->prepare("SELECT * FROM ".DB_PREF."email ORDER BY date ASC LIMIT 0, ". $limit);
        $pre->execute();
        $res = $pre->fetchAll();
        $pre->closeCursor();
        
        #echo "<pre>"; print_r($res); echo "</pre>"; die();
        
        foreach($res as $r) {
            Email::sendTo($r['email'], $r['title'], $r['body'], $r['template'], false, $r['fromEmail'], $r['fromName']);
            $pre = $this->database->prepare("DELETE FROM ".DB_PREF."email WHERE id = ". $r['id']);
            $pre->execute();
        }
    }
    
    public function getMapaData() {
        $jobs = $this->getAllJobs();
        if($jobs) {
            $j = 0;
            foreach($jobs as $job) { 
                $array['offers'][$j]['id'] = $job['job_id'];
                $array['offers'][$j]['title'] = $job['job_position'];
                
                if($job['job_need']) {
                    $array['offers'][$j]['description'] = html_entity_decode($job['job_need']);
                } else {
                    $array['offers'][$j]['description'] = html_entity_decode('<small>brak opisu</small>');
                }
                $array['offers'][$j]['amount'] = ceil(($job['job_rate_min'] + $job['job_rate_max'])/2);
                
                $array['offers'][$j]['url'] = HOST.'praca/'.$job['job_id'].'/'.$job['job_url_text'];
                
                $tags = $this->getAllTags($job['job_id']);
                
                if($tags && is_array($tags)) {
                    foreach($tags as $tag) {
                        $array['offers'][$j]['tags'][] = $tag['tag_name'];
                    }
                } else {
                    $array['offers'][$j]['tags'][] = array();
                }

                
                $array['offers'][$j]['lat'] = $job['job_lat'];
                $array['offers'][$j]['lng'] = $job['job_long'];
                
                if(trim(mb_strtolower($job['job_employeer_place_direct_city'])) == 'warszawa') {
                    $array['offers'][$j]['cid'] = '3000';
                } else if (trim(mb_strtolower($job['job_employeer_place_direct_city'])) == 'łódź') {
                    $array['offers'][$j]['cid'] = '4000';
                } else {
                    $array['offers'][$j]['cid'] = '1000';
                }
                
                $array['offers'][$j]['cat'] = array($job['job_trade_type_id']);
                
                $employee = $this->getEmployeeInfo($job['job_added_by']);
                
                if($job['job_employeer_name']) {
                    $array['offers'][$j]['employer'][0]['name'] = $job['job_employeer_name'];
                } elseif($employee['user_profile_name']) {
                    $array['offers'][$j]['employer'][0]['name'] = $employee['user_profile_name'];
                } else {
                    $array['offers'][$j]['employer'][0]['name'] = 'brak nazwy pracodawcy';
                }
                
                if($employee['user_profile_url']) {
                    $array['offers'][$j]['employer'][0]['watchUrl'] = HOST.'profil/p/'.$employee['user_profile_url'];
                } else {
                    $array['offers'][$j]['employer'][0]['watchUrl'] = HOST.'profil/p/'.md5($job['job_added_by']);
                }
                 
                if($employee['user_profile_url']) {
                    $array['offers'][$j]['employer'][0]['wwwUrl'] = HOST.'profil/p/'.$employee['user_profile_url'];
                } else {
                    $array['offers'][$j]['employer'][0]['wwwUrl'] = HOST.'profil/p/'.md5($job['job_added_by']);
                }

                if(!empty($job['job_logo'])) {
                    $array['offers'][$j]['employer'][0]['imgUrl'] = HOST.'upload/jobLogo/'.$job['job_logo'];
                } elseif(!empty($employee['user_logo'])) {
                    $array['offers'][$j]['employer'][0]['imgUrl'] = HOST.'upload/userLogo/'.$employee['user_logo'];
                } else {
                    $array['offers'][$j]['employer'][0]['imgUrl'] = '';
                }
                
                if($job['job_employeer_place_direct_address'] || $job['job_employeer_place_direct_city'] || $job['job_employeer_place_direct_zip_code']) {
                    $contact = $job['job_employeer_place_direct_address'] . "<br/>";
                    $contact .= $job['job_employeer_place_direct_zip_code'] . " " . $job['job_employeer_place_direct_city'] . "<br/>";
                } else {
                    $contact = $employee['user_adress_street'] . "<br/>";
                    $contact .= $employee['user_adress_zip_code'] . " " . $employee['user_adress_city'] . "<br/>";
                }
                
                if($job['job_contact_phone']) {
                    $contact .= 'tel.: '. $job['job_contact_phone'];
                } elseif($employee['user_phone']) {
                    $contact .= 'tel.: '. $employee['user_phone'];
                } else {
                    $contact .= '<small>brak telefonu</small>';
                }
      
                $array['offers'][$j]['employer'][0]['contact'] = $contact;

                $j++;
            }
        } else {
            return false;
        }
        
        return $array;
    }
    
    private function getAllJobs() {
        $sql = "SELECT job_id, job_added_by, job_main_page, job_employeer_name, job_contact_phone, job_logo, job_employeer_place_direct_address, job_employeer_place_direct_city, job_employeer_place_direct_zip_code, job_contact_phone, job_position, job_need, job_employeer_place_direct_city, job_rate_min, job_rate_max, job_url_text, job_lat, job_long, job_trade_type_id FROM ".DB_PREF."job WHERE (LOWER(job_employeer_place_direct_city) = 'warszawa' OR LOWER(job_employeer_place_direct_city) = 'poznań' OR LOWER(job_employeer_place_direct_city) = 'poznan' OR LOWER(job_employeer_place_direct_city) = 'łódź' OR LOWER(job_employeer_place_direct_city) = 'lodz') AND job_delete = 0 AND job_publish_date_end > ? AND job_lat IS NOT NULL AND job_long IS NOT NULL";
        $pre = $this->database->prepare($sql);
        $pre->execute(array(date('Y-m-d H:i:s')));
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            return false;
        }
    }
            
    private function getEmployeeInfo($id) {
        $sql = "SELECT user_profile_name, user_www, user_logo, user_profile_url, user_adress_city, AES_DECRYPT(user_adress_zip_code, '".SALT."') as user_adress_zip_code, AES_DECRYPT(user_adress_street, '".SALT."') as user_adress_street, AES_DECRYPT(user_phone, '".SALT."') as user_phone FROM ".DB_PREF."user WHERE user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return $res;
        } else {
            return false;
        }
    }
        
    private function getAllTags($id) {
        $sql = "SELECT t.tag_name FROM ".DB_PREF."tag_job as j LEFT JOIN ".DB_PREF."tag as t ON t.tag_id = j.tag_id WHERE j.job_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            return false;
        }
    }
            
}