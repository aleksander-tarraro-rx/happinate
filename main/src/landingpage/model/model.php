<?php

namespace src\landingpage\model;

class landingpageModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
    
    public function register($email, $pass) {

        $sql = "INSERT INTO ".DB_PREF."user (user_email, user_password, user_type, user_register_date, user_register_token) VALUES (AES_ENCRYPT(?, '".SALT."'), AES_ENCRYPT(md5(?), '".SALT."'), ?, ?, ?);";
        $pre = $this->database->prepare($sql);
        $registerToken =  sha1($email.SALT);
        if($pre->execute(array($email, $pass, 1, date("Y-m-d H:i:s"), $registerToken))) {

            $title = 'happinate.com - witamy!';
            $body = '<h3>Witamy w happinate.com!</h3>';
            $body .= '<p>Cieszymy się, że dołączyłeś do Nas!</p>';
            $body .= '<p>Kliknij w <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'rejestracja/potwierdzenie/'.$registerToken.'" target="_blank">link</a> i aktywuj swoje konto.</p>';
            $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz</p>';
            
            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($email, $title, $body);
            
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function checkEmailNotExist($email) {
        $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."');";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($email));
        if($pre->rowCount()!=0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
}