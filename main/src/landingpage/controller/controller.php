<?php

namespace src\landingpage;

use app\helpers\Form;
use src\BaseController;

class landingpageController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }

    public function indexAction($queryName = false) {
        header("Location:" . HOST);
    }
    
    public function znajdz3sposobyAction($queryName = false) {
        if($queryName && ($queryName=='dziekanat' || $queryName=='3-sposoby' || $queryName=='brak-kasy' || $queryName=='facebook')) {
            
            $this->template->assign('googleAnalytics', $queryName);

        }
        
        if(isset($_POST['submit'])) {
            $error = '';
            // Validation
            if(empty($_POST['email'])) {
                $error .= '<p>Proszę wpisać adres email.</p>';
            } elseif(!Form::validEmail($_POST['email'])) {
                $error .= '<p>Proszę podać prawidłowy adres email.</p>';
            } elseif($id = $this->model->checkEmailNotExist($_POST['email'])) {
                $error .= '<p>Podany adres email znajduje się już w bazie danych.</p>';
            }
            if(empty($_POST['password1'])) {
                $error .= '<p>Proszę wpisać hasło.</p>';
            } elseif(strlen($_POST['password1'])<6) {
                $error .= '<p>Hasło musi posiadać więcej niż 6 znaków.</p>';
            }
            if($_POST['password1']!==$_POST['password2']) {
                $error .= '<p>Podane hasła muszą być identyczne.</p>';
            }

            if(!$error) {
                if($this->model->register($_POST['email'], $_POST['password1'])) {
                    header("Location: " . HOST . "rejestracja/sukces");
                }
            } else {
                $this->template->assign('error', $error);
            } 
        }
        
        $this->template->display('landingpage/znajdz3sposoby.tpl');
    }
    
}