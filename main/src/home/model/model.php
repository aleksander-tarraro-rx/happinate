<?php
namespace src\home\model;

class homeModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
    
    public function getMainPageJobs() {
        $month = date("Y-m-d H:i:s");
        $sql = "SELECT job_id, job_main_page, job_employeer_place_direct_city, job_added_by, job_rate_min, job_rate_max, job_volunteering, job_hide_salary, job_position, job_url_text, job_publish_date_end FROM ".DB_PREF."job WHERE job_status = 1 AND job_delete = 0 AND job_publish_date_end > '".$month."' ORDER BY job_created_date DESC LIMIT 0, 20";
        $res = $this->database->prepare($sql);
        $res->execute();

        if($res->rowCount()) {
            return $res->fetchAll();
        } else {
            return true;
        }     
    }
    
    public function getProvinces($name = false) {
        if($name) {
            $sql = "SELECT province_id FROM ".DB_PREF."province WHERE province_text = ?";

            $pre = $this->database->prepare($sql);
            $pre->execute(array($name));
            if($pre->rowCount()!=0) {
                $res = $pre->fetch();
                return $res['province_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return 0;
            }    
        } else {
            $sql = "SELECT * FROM ".DB_PREF."province";

            $pre = $this->database->prepare($sql);
            $pre->execute();
            if($pre->rowCount()!=0) {
                return $pre->fetchAll();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return 0;
            }   
        }
    }
}