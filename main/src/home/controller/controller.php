<?php
namespace src\home;

use src\BaseController;

class homeController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        $mainPageJobs = $this->model->getMainPageJobs();
        $provinces = $this->model->getProvinces();

        $this->template->assign('mainPageJobs', $mainPageJobs);
        $this->template->assign('provinces', $provinces);
        $this->template->display('home/index.tpl');
    }
}