<?php

namespace src\mapa\model;

class mapaModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
    
    public function getAllTags() {
        $sql = "SELECT tag_id, tag_name FROM ".DB_PREF."tag WHERE tag_active = 1 ORDER BY tag_sort ASC";
        $pre = $this->database->prepare($sql);
        $pre->execute();
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
}

