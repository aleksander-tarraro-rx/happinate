<?php
namespace src\mapa;

use src\BaseController;

class mapaController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        $tags = $this->model->getAllTags();

        $this->template->assign('tags', $tags);
        $this->template->display('mapa/index.tpl');
    }
}