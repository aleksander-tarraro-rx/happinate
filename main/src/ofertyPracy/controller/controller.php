<?php

namespace src\ofertyPracy;

use app\helpers\Form;
use src\BaseController;

class ofertyPracyController extends BaseController {
    
    private $template;
    private $model;
    private $contractTypeList;
    private $tradeTypeList;
    private $cities;
    private $provinces;
    private $tags;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
        $this->contractTypeList = $this->model->getContractType();
        $this->tradeTypeList = $this->model->getTrades();
        $this->cities = $this->model->getCities();
        $this->provinces = $this->model->getProvinces();
        $this->tags = $this->model->getTags();
        
        $this->template->assign('contractTypeList', $this->contractTypeList);
        $this->template->assign('tradeTypeList', $this->tradeTypeList);
        $this->template->assign('citiesList', $this->cities);
        $this->template->assign('provinceList', $this->provinces);
        $this->template->assign('tagsList', $this->tags);
        $this->template->assign('_model', $this->model);
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        
        header("Location:".HOST."oferty-pracy/1/");

    }
    
    /* Integer action */
    public function integerAction($int, $queryName = false) {
        $this->template->assign('queryName', $queryName);
        
        $error = false;
        
        if(isset($_POST['filter'])) {
            
            if(isset($_POST['rate-from'])) {
                $_POST['rate-from'] = str_replace(',','.',$_POST['rate-from']);
            }
            
            if(!empty($_POST['rate-from']) && !is_numeric($_POST['rate-from'])) {
                $error = '<p>Proszę podać prawidłową kwotę w filtrze</p>';
            }
            
            $url = HOST."oferty-pracy/1/";
            
            $tags = 1;
            if(isset($_POST['tags']) && !empty($_POST['tags'])) {
                foreach($_POST['tags'] as $tr=>$key) {
                    if($key) { 
                        if($tags != 1) $url .= ",";
                        $url .= $this->model->getTagName($tr);
                        $tags++;
                    }
                }
            }
            
            $url.="-";
                        
            $city = 1;
            if(isset($_POST['province']) && !empty($_POST['province']) && is_array($_POST['province'])) {
                foreach($_POST['province'] as $tr=>$key) {
                    if($key) { 
                        if($city != 1) $url .= ",";
                        $url .= $this->model->getProvinceName($tr);
                        $city++;
                    }
                }  
            }
            
            $url.="-";
            
            $t = 1;
            if(isset($_POST['trade']) && !empty($_POST['trade'])) {
                foreach($_POST['trade'] as $tr=>$key) {
                    if($key) { 
                        if($t != 1) $url .= ",";
                        $url .= $this->model->getTradeName($tr);
                        $t++;
                    }
                }
            }
            
            $url.="-";
            
            $type = 1;
            if(isset($_POST['type']) && !empty($_POST['type'])) {
                foreach($_POST['type'] as $tr=>$key) {
                    if($key) { 
                        if($type != 1) $url .= ",";
                        $url .= $this->model->getContractName($tr);
                        $type++;
                    }
                }
            }
            
            $url.="-";
      
            if(isset($_POST['time'][1]) && $_POST['time'][1]) $url .= "stały";
            if(isset($_POST['time'][1]) && $_POST['time'][1] && isset($_POST['time'][2]) && $_POST['time'][2]) $url .= ",";
            if(isset($_POST['time'][2]) && $_POST['time'][2]) $url .= "elastyczny";

            if(isset($_POST['rate-from']) && $_POST['rate-from']) { 
                $url .= "-" . $_POST['rate-from'];
            } else {
                $url .= "-";
            }

            if(isset($_POST['search']) && $_POST['search']) { 
                $url .= "-" . $_POST['search'];
            } else {
                $url .= "-";
            }
 
            
            if(!$error) header("Location:".$url);
        }
        
        if($error) $this->template->assign('error', $error);  
        
        if($queryName) {
            $explode = explode('-',$queryName);
            
            if(isset($explode[0])) { $tags = explode(',', $explode[0]); } else { $tags = false; }
            if(isset($explode[1])) { $city = explode(',', $explode[1]); } else { $city = false; }
            if(isset($explode[2])) { $trade = explode(',', $explode[2]); } else { $trade = false; }
            if(isset($explode[3])) { $contract = explode(',', $explode[3]); } else { $contract = false; }
            if(isset($explode[4])) { $time = explode(',', $explode[4]); } else { $time = false; }
            
            if(isset($explode[5])) {
                $rate = $explode[5];
            } else {
                $rate = 0;
            }
            if(isset($explode[6])) {
                $search = Form::sanitizeString($explode[6]);
            } else {
                $search = 0;
            }       
            $this->template->assign("tags", $tags);
            $this->template->assign("cities", $city);
            $this->template->assign("trade", $trade);
            $this->template->assign("contract", $contract);
            $this->template->assign("time", $time);
            $this->template->assign("rateFrom", $rate);
            $this->template->assign("search", $search);
        } 
        
        // Stronicowanie
        $jobOnPage = 30;
        $page = $int;

        if(!empty($page)) {
            $from = $page * $jobOnPage - $jobOnPage;
        } else {
            $from = 0;
            $page = 1;
        }            
        
        
        
        $countJobs = $this->model->countJobs($queryName);
        
        if(!$countJobs) {
            $jobList = $this->model->getJob(false, false, false, $from, $jobOnPage, false);
            $countJobs = $this->model->countJobs(false);
            $noJobs = true;
        } else {
            $jobList = $this->model->getJob(false, false, false, $from, $jobOnPage, $queryName);
            $noJobs = false;
        }
        
        $pageCount = ceil($countJobs/$jobOnPage);

        if($pageCount != 0 && $int>$pageCount) {
            header("Location:".HOST."oferty-pracy");
        }

        $this->template->assign('noJobs', $noJobs);
        $this->template->assign('jobList', $jobList);
        $this->template->assign('countJobs', $countJobs);
        $this->template->assign('pageCount', $pageCount);
        $this->template->display('ofertyPracy/index.tpl');
    }
}