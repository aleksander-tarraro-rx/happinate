<?php

namespace src\ofertyPracy\model;

use PDO;
use app\helpers\Form;

class ofertyPracyModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
  
    public function getContractType() {
        $sql = "SELECT * FROM ".DB_PREF."job_contract_types";
        $pre = $this->database->prepare($sql);
        $pre->execute();
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }            
    }    
    
    public function getTrades() {
        $sql = "SELECT * FROM ".DB_PREF."job_trade_types";
        $pre = $this->database->prepare($sql);
        $pre->execute();
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }     
    }    
    
    public function getCities() {
        $sql = "SELECT * FROM ".DB_PREF."city";
        $pre = $this->database->prepare($sql);
        $pre->execute();
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }     
    }
      
    public function getJob($page = false, $type = false, $trade = false, $from = false, $to = false, $filter = false) {
        if($filter) {
            $explode = explode('-',$filter);
            
            if(isset($explode[0])) { $tags = explode(',', $explode[0]); } else { $tags = false; }
            if(isset($explode[1])) { $city = explode(',', $explode[1]); } else { $city = false; }
            if(isset($explode[2])) { $trade = explode(',', $explode[2]); } else { $trade = false; }
            if(isset($explode[3])) { $contract = explode(',', $explode[3]); } else { $contract = false; }
            if(isset($explode[4])) { $time = explode(',', $explode[4]); } else { $time = false; }
            
            if(isset($explode[5])) {
                $rate = $explode[5];
            } else {
                $rate = 0;
            }
            if(isset($explode[6])) {
                $search = Form::sanitizeString($explode[6]);
            } else {
                $search = 0;
            } 
        }
        
        $month = date("Y-m-d H:i:s", strtotime("-30 days"));
        
        $sql = "SELECT DISTINCT u.user_profile_name, j.job_employeer_place_direct_city, j.job_main_page, j.job_city_id, j.job_id, j.job_created_date, j.job_added_by, j.job_rate_min, j.job_rate_max, j.job_logo, j.job_volunteering, j.job_hide_salary, j.job_position, j.job_url_text, j.job_publish_date, j.job_publish_date_end FROM ".DB_PREF."job as j LEFT JOIN ".DB_PREF."tag_job as t ON t.job_id = j.job_id LEFT JOIN ".DB_PREF."user as u ON j.job_added_by = u.user_id WHERE j.job_status = 1 AND j.job_publish_date < NOW() AND j.job_publish_date_end > '".$month."'";
        
        // Miasta
        if($filter && !empty($city[0])) {
            $i = 1;
            $cityCount = count($city);
            foreach($city as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_province_id = :city".$c;
                if($i==$cityCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }        
                
        // Tagi
        if($filter && !empty($tags[0])) {
            $i = 1;
            $tagCount = count($tags);
            foreach($tags as $c=>$k) {
                if($i==1) $sql .= " AND ( (";
                $sql .= " t.tag_id = :tag".$c;
                if($i==$tagCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }      
        // Główne rodzaje pracy
        if($filter && !empty($tags[0])) {
            $i = 1;
            $tagCount = count($tags);
            foreach($tags as $c=>$k) {
                if($i==1) $sql .= " OR (";
                $sql .= " j.job_trade_type_id = :trade".$c;
                if($i==$tagCount) {
                    $sql .= " ) )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }          
        
        // Trade
        if($filter && !empty($trade[0])) {
            $i = 1;
            $tradeCount = count($trade);
            foreach($trade as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_trade_type_id = :trade".$c;
                if($i==$tradeCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }   
        
        // Trade
        if($filter && !empty($contract[0])) {
            $i = 1;
            $contractCount = count($contract);
            foreach($contract as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_contract_type_id = :contact".$c;
                if($i==$contractCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }        

        // Time
        if($filter && !empty($time[0])) {
            $i = 1;
            $timeCount = count($time);
            foreach($time as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_time = :time".$c;
                if($i==$timeCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }
        
        // Min. wynagrodzenie
        if($filter && !empty($rate)) {
            $sql .= " AND j.job_rate_min >= :rate ";
        }            

        // Search
        if($filter && !empty($search)) {
            $sql .= " AND (LOWER(j.job_position) LIKE :search OR LOWER(j.job_employeer_place_direct_city) LIKE :search OR LOWER(u.user_profile_name) LIKE :search) ";
        }       

        $sql .= " AND j.job_delete = 0 ORDER BY j.job_created_date DESC";
        $sql .= " LIMIT :from, :to";
        
        #die($sql);
        
        $pre = $this->database->prepare($sql);
        
        // Tagi
        if($filter && !empty($tags[0])) {
            foreach($tags as $c=>$k) {
                $pre->bindValue(':tag'.$c, $this->getTagId($k), PDO::PARAM_INT);
            }
        } 
        
        if($filter && !empty($tags[0])) {
            foreach($tags as $c=>$k) {
                $pre->bindValue(':trade'.$c, $this->getTagId($k), PDO::PARAM_INT);
            }
        } 
        
        // Miasta
        if($filter && !empty($city[0])) {
            foreach($city as $c=>$k) {
                $pre->bindValue(':city'.$c, $this->getProvinceId($k), PDO::PARAM_INT);
            }
        }
        
        // Branża
        if($filter && !empty($trade[0])) {
            foreach($trade as $c=>$k) {
                $pre->bindValue(':trade'.$c, $this->getTradeId($k), PDO::PARAM_INT);
            }
        }
        
        // Kontrakt
        if($filter && !empty($contract[0])) {
            foreach($contract as $c=>$k) {
                $pre->bindValue(':contact'.$c, $this->getContractId($k), PDO::PARAM_INT);
            }
        }
        // Time
        if($filter && !empty($time[0])) {
            foreach($time as $c=>$k) {
                $pre->bindValue(':time'.$c, $k, PDO::PARAM_STR);
            }
        }
        // Time
        if($filter && !empty($rate)) {
            $pre->bindValue(':rate', $rate, PDO::PARAM_INT);
        }
        // Search
        if($filter && !empty($search)) {
            $searchVal = strtolower('%'.Form::sanitizeString($search).'%');
            $pre->bindValue(':search', $searchVal, PDO::PARAM_STR);
        }
        
        $pre->bindValue(':from', $from, PDO::PARAM_INT);
        $pre->bindValue(':to', $to, PDO::PARAM_INT);

        $pre->execute();
 
        if($pre->rowCount()!=0) {
            $jobList = $pre->fetchAll();

            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function createURLText($string) {
        
        $string = strtolower($string);
        $polskie = array(',', ' - ',' ','ę', 'Ę', 'ó', 'Ó', 'Ą', 'ą', 'Ś', 's', 'ł', 'Ł', 'ż', 'Ż', 'Ź', 'ź', 'ć', 'Ć', 'ń', 'Ń','-',"'","/","?", '"', ":", 'ś', '!','.', '&', '&amp;', '#', ';', '[',']','domena.pl', '(', ')', '`', '%', '”', '„', '…');
        $miedzyn = array('-','-','-','e', 'e', 'o', 'o', 'a', 'a', 's', 's', 'l', 'l', 'z', 'z', 'z', 'z', 'c', 'c', 'n', 'n','-',"","","","","",'s','','', '', '', '', '', '', '', '', '', '', '', '', '');
        $string = str_replace($polskie, $miedzyn, $string);
        
        $string = preg_replace('/[^0-9a-z\-]+/', '', $string);
        $string = preg_replace('/[\-]+/', '-', $string);
        $string = trim($string, '-');

        $string = stripslashes($string);

        $string = urlencode($string);
        
        return $string;

    }
 
    public function jobFeedXml() {
        
        $url = "http://startpraca.pl/feeds/happinate.com/";
        $xmlString = file_get_contents($url);
        $xml = new \SimpleXMLElement($xmlString, LIBXML_NOCDATA);

        
        $array = array();
        $i = 0;
        
        foreach ($xml->offer as $element) {

            $array[$i]['job_id'] = ''.$element->id.'';
            $array[$i]['job_position'] = ''.$element->nazwa_stanowiska.'';
            $array[$i]['job_logo'] = ''.$element->logo.'';
            $array[$i]['job_city'] = ''.$element->miasta.'';
            $array[$i]['job_url_text'] = $this->createURLText($element->nazwa_stanowiska);
            $array[$i]['job_publish_date'] = ''.$element->data_publikacji.'';
            $array[$i]['job_created_date'] = ''.$element->data_publikacji.'';
            $array[$i]['job_publish_date_end'] = ''.$element->data_konca_publikacji.'';
            $array[$i]['job_url'] = ''.$element->url.'';
            
            $i++;
        }
        #print_r($array);
        return $array;
    }
    
    public function getUserLogo($id) {
        $sql = "SELECT user_logo FROM ".DB_PREF."user WHERE user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $logo = $pre->fetch();
            return $logo['user_logo'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function countJobs($filter) {
        if($filter) {
            $explode = explode('-',$filter);
            
            if(isset($explode[0])) { $tags = explode(',', $explode[0]); } else { $tags = false; }
            if(isset($explode[1])) { $city = explode(',', $explode[1]); } else { $city = false; }
            if(isset($explode[2])) { $trade = explode(',', $explode[2]); } else { $trade = false; }
            if(isset($explode[3])) { $contract = explode(',', $explode[3]); } else { $contract = false; }
            if(isset($explode[4])) { $time = explode(',', $explode[4]); } else { $time = false; }
            
            if(isset($explode[5])) {
                $rate = $explode[5];
            } else {
                $rate = 0;
            }
            if(isset($explode[6])) {
                $search = Form::sanitizeString($explode[6]);
            } else {
                $search = 0;
            } 
        }
        
        $month = date("Y-m-d H:i:s", strtotime("-30 days"));
        
        $sql = "SELECT DISTINCT count(j.job_id) as count FROM ".DB_PREF."job as j LEFT JOIN ".DB_PREF."tag_job as t ON j.job_id = t.job_id WHERE j.job_status = 1 AND j.job_publish_date < NOW() AND job_publish_date_end > '".$month."'";
        
        // Miasta
        if($filter && !empty($city[0])) {
            $i = 1;
            $cityCount = count($city);
            foreach($city as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_province_id = :city".$c;
                if($i==$cityCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }          
                 
        // Tagi
        if($filter && !empty($tags[0])) {
            $i = 1;
            $tagCount = count($tags);
            foreach($tags as $c=>$k) {
                if($i==1) $sql .= " AND ( (";
                $sql .= " t.tag_id = :tag".$c;
                if($i==$tagCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }    
        
        // Główne rodzaje pracy
        if($filter && !empty($tags[0])) {
            $i = 1;
            $tagCount = count($tags);
            foreach($tags as $c=>$k) {
                if($i==1) $sql .= " OR (";
                $sql .= " j.job_trade_type_id = :trade".$c;
                if($i==$tagCount) {
                    $sql .= " ) )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }       
        // Trade
        if($filter && !empty($trade[0])) {
            $i = 1;
            $tradeCount = count($trade);
            foreach($trade as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_trade_type_id = :trade".$c;
                if($i==$tradeCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }   
        
        // Trade
        if($filter && !empty($contract[0])) {
            $i = 1;
            $contractCount = count($contract);
            foreach($contract as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_contract_type_id = :contact".$c;
                if($i==$contractCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }        

        // Time
        if($filter && !empty($time[0])) {
            $i = 1;
            $timeCount = count($time);
            foreach($time as $c=>$k) {
                if($i==1) $sql .= " AND (";
                $sql .= " j.job_time = :time".$c;
                if($i==$timeCount) {
                    $sql .= " )";
                } else {
                    $sql .= " OR ";
                }
                $i++;
            }
        }
        
        // Min. wynagrodzenie
        if($filter && !empty($rate)) {
            $sql .= " AND j.job_rate_min >= :rate ";
        }     

        // Search
        if($filter && !empty($search)) {
            $sql .= " AND LOWER(j.job_position) LIKE :search ";
        }   
        
        $sql .= " AND j.job_delete = 0";

        $pre = $this->database->prepare($sql);
          
        // Tagi
        if($filter && !empty($tags[0])) {
            foreach($tags as $c=>$k) {
                $pre->bindValue(':tag'.$c, $this->getTagId($k), PDO::PARAM_INT);
            }
        } 
        
        if($filter && !empty($tags[0])) {
            foreach($tags as $c=>$k) {
                $pre->bindValue(':trade'.$c, $this->getTagId($k), PDO::PARAM_INT);
            }
        } 

        // Miasta
        if($filter && !empty($city[0])) {
            foreach($city as $c=>$k) {
                $pre->bindValue(':city'.$c, $this->getProvinceId($k), PDO::PARAM_INT);
            }
        }
        
        // Branża
        if($filter && !empty($trade[0])) {
            foreach($trade as $c=>$k) {
                $pre->bindValue(':trade'.$c, $this->getTradeId($k), PDO::PARAM_INT);
            }
        }
        
        // Kontrakt
        if($filter && !empty($contract[0])) {
            foreach($contract as $c=>$k) {
                $pre->bindValue(':contact'.$c, $this->getContractId($k), PDO::PARAM_INT);
            }
        }
        // Time
        if($filter && !empty($time[0])) {
            foreach($time as $c=>$k) {
                $pre->bindValue(':time'.$c, $k, PDO::PARAM_STR);
            }
        }
        // Time
        if($filter && !empty($rate)) {
            $pre->bindValue(':rate', $rate, PDO::PARAM_INT);
        }
        
        // Search
        if($filter && !empty($search)) {
            $searchVal = strtolower('%'.Form::sanitizeString($search).'%');
            $pre->bindValue(':search', $searchVal, PDO::PARAM_STR);
        }

        $pre->execute();
        
        if($pre->rowCount()!=0) {
            $r = $pre->fetch();
            return $r['count'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    
    
    public function getTradeName($id) {
        $sql = "SELECT trade_name FROM ".DB_PREF."job_trade_types WHERE trade_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return str_replace(" | ", "_",strtolower($res['trade_name']));
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }     
    }    
    
    public function getContractName($id) {
        $sql = "SELECT type_name FROM ".DB_PREF."job_contract_types WHERE type_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return str_replace(" ", "_",strtolower($res['type_name']));
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }     
    }    
    
    public function getCityName($id) {
        $sql = "SELECT city_text FROM ".DB_PREF."city WHERE city_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return str_replace(" ", "_",strtolower($res['city_text']));
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }    
       
    public function getTagName($id) {
        $sql = "SELECT tag_name FROM ".DB_PREF."tag WHERE tag_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return str_replace(" ", "_",strtolower($res['tag_name']));
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }    
    
    public function getProvinceName($id) {
        $sql = "SELECT province_text FROM ".DB_PREF."province WHERE province_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return str_replace(" ", "_",strtolower($res['province_text']));
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }     
    
    public function getCleanCityName($id) {
        $sql = "SELECT city_text FROM ".DB_PREF."city WHERE city_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['city_text'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }    
    
    public function getCityId($name) {
        $sql = "SELECT city_id FROM ".DB_PREF."city WHERE LOWER(REPLACE(city_text, ' ', '_')) = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($name));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['city_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
        
    public function getProvinceId($name) {
        $sql = "SELECT province_id FROM ".DB_PREF."province WHERE LOWER(REPLACE(province_text, ' | ', '_')) = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($name));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['province_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
   
            
    public function getTagId($name) {
        $sql = "SELECT tag_id FROM ".DB_PREF."tag WHERE LOWER(REPLACE(tag_name, ' ', '_')) = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($name));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['tag_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
   
    
    public function getTradeId($name) {
        $sql = "SELECT trade_id FROM ".DB_PREF."job_trade_types WHERE LOWER(REPLACE(trade_name, ' | ', '_')) = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($name));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['trade_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    
    public function getContractId($name) {
        $sql = "SELECT type_id FROM ".DB_PREF."job_contract_types WHERE LOWER(REPLACE(type_name, ' ', '_')) = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($name));
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['type_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
        
    public function getProvinces($name = false) {
        if($name) {
            $sql = "SELECT province_id FROM ".DB_PREF."province WHERE province_text = ?";

            $pre = $this->database->prepare($sql);
            $pre->execute(array($name));
            if($pre->rowCount()!=0) {
                $res = $pre->fetch();
                return $res['province_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return 0;
            }    
        } else {
            $sql = "SELECT * FROM ".DB_PREF."province";

            $pre = $this->database->prepare($sql);
            $pre->execute();
            if($pre->rowCount()!=0) {
                return $pre->fetchAll();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return 0;
            }   
        }
    }
    
    public function checkIfAddedToFav($job) {
        
        $id = $this->getUserId();
        
        if(!$id) return false;
        
        $sql = "SELECT fav_id FROM ".DB_PREF."user_fav_job WHERE fav_job_id = ? AND fav_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($job, $id));
        if($pre->rowCount()==0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
          
    public function favJob($job) {
        
        $id = $this->getUserId();
        
        if(!$id) return false;
        
        if(!$this->checkIfAddedToFav($job, $id)) return -2;
            
        $sql = "INSERT INTO ".DB_PREF."user_fav_job (fav_user_id, fav_job_id) VALUES (?, ?)";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id, $job))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
              
    public function unfavJob($id, $job) {
        
        $id = $this->getUserId();
        
        if(!$id) return false;
        
        if($this->checkIfAddedToFav($job, $id)) return -2;
            
        $sql = "DELETE FROM ".DB_PREF."user_fav_job WHERE fav_user_id = ? AND fav_job_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id, $job))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
    
    public function getUserId($email = false) {
        if($email) {
            $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."')";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($email));            
                
            if($pre->rowCount()!=0) {
                $id = $pre->fetch();
                return $id['user_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }              
        } else {
            if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
                $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($_SESSION['userToken']));
            } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) { 
                $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($_SESSION['userTokenFB']));            
            } else {
                return false;
            }
            if($pre->rowCount()!=0) {
                $id = $pre->fetch();
                return $id['user_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }      
        }
    }
    
    public function getTags($admin = false) {
        if($admin) {
            $sql = "SELECT * FROM ".DB_PREF."tag WHERE tag_active = 0 ORDER BY tag_sort ASC";
        } else {
            $sql = "SELECT * FROM ".DB_PREF."tag WHERE tag_active = 1 ORDER BY tag_sort ASC";
        }
        $pre = $this->database->prepare($sql);
        $pre->execute();
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }         
    }
}