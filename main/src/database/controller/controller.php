<?php

namespace src\database;

use src\BaseController;

class databaseController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        echo "<h3>Database Convert</h3>";
        
        $list = $this->model->getUserList();
        
        echo "<pre>"; print_r($list); echo "</pre>";
        
        #$this->model->deleteDuplicatesId();
        
        foreach($list as $profile) {
            $this->model->AesEncrypt($profile['user_id'], 'user_email');
            $this->model->AesEncrypt($profile['user_id'], 'user_name');
            $this->model->AesEncrypt($profile['user_id'], 'user_surname');
            $this->model->AesEncrypt($profile['user_id'], 'user_adress_zip_code');
            $this->model->AesEncrypt($profile['user_id'], 'user_adress_street');
            $this->model->AesEncrypt($profile['user_id'], 'user_contact_zip_code');
            $this->model->AesEncrypt($profile['user_id'], 'user_contact_street');
            $this->model->AesEncrypt($profile['user_id'], 'user_phone');
        }
    }    
    
}