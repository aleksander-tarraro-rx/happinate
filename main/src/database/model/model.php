<?php

namespace src\database\model;

class databaseModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
    
    public function getUserList() {
        $sql = "SELECT * FROM happinate_user";
        $pre = $this->database->prepare($sql);

        $pre->execute();
        
        if($pre->rowCount()!=0) {
            $jobList = $pre->fetchAll();
            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            $this->error->sqlError($pdoError[2]);
            return false;
        }           
    }
    
      
    public function getJobNeeds() {
        $sql = "SELECT * FROM dbo_job_skills";
        $pre = $this->database->prepare($sql);

        $pre->execute();
        
        if($pre->rowCount()!=0) {
            $jobList = $pre->fetchAll();
            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            $this->error->sqlError($pdoError[2]);
            return false;
        }           
    }
       
    public function getJobDuty() {
        $sql = "SELECT * FROM dbo_job_skills_extra";
        $pre = $this->database->prepare($sql);

        $pre->execute();
        
        if($pre->rowCount()!=0) {
            $jobList = $pre->fetchAll();
            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            $this->error->sqlError($pdoError[2]);
            return false;
        }           
    }
    
    
    public function setType($id) {
        $sql = "SELECT RoleId FROM klaudiusz_webuser_webpages_usersinroles WHERE UserId = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            $sql = "UPDATE klaudiusz_webuser_userprofile SET user_type = ? WHERE UserId = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($res['RoleId'], $id));
        } else {
            $res = $pre->fetch();
            $sql = "UPDATE klaudiusz_webuser_userprofile SET user_type = ? WHERE UserId = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array(1, $id));
        }           
    }    
    
    public function setFacebook($id) {
        $sql = "SELECT ProviderUserId FROM klaudiusz_webuser_webpages_oauthmembership WHERE UserId = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            $sql = "UPDATE klaudiusz_webuser_userprofile SET user_fb_id = ? WHERE UserId = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($res['ProviderUserId'], $id));
        } else {
            $sql = "UPDATE klaudiusz_webuser_userprofile SET user_fb_id = NULL WHERE UserId = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($id));
        } 
    }
    
    public function AesEncrypt($id, $field) {
        $sql = "SELECT ".$field." FROM happinate_user WHERE user_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            $sql = "UPDATE happinate_user SET ".$field." = AES_ENCRYPT(?, '".SALT."') WHERE user_id = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($res[$field], $id));
        }    
    }    
        
    public function setNull($id, $field) {
        $sql = "SELECT ".$field." FROM klaudiusz_webuser_userprofile WHERE UserId = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $res = $pre->fetch();
        if(empty($res[$field])) {
            $sql = "UPDATE klaudiusz_webuser_userprofile SET ".$field." = NULL WHERE UserId = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($id));
        }    
    }    
    
    public function deleteDuplicates() {
        $sql = "DELETE c1 FROM klaudiusz_webuser_userprofile c1, klaudiusz_webuser_userprofile c2 WHERE c1.user_email = c2.user_email AND c1.UserId < c2.UserId ";
        $pre = $this->database->prepare($sql);

        $pre->execute();
    }  
    
    public function deleteDuplicatesId() {
        $sql = "DELETE c1 FROM klaudiusz_webuser_userprofile c1, klaudiusz_webuser_userprofile c2 WHERE c1.UserId = c2.UserId AND c1.UserId < c2.UserId ";
        $pre = $this->database->prepare($sql);

        $pre->execute();
    }
    
    
    public function activeUser($id) {

        $sql = "UPDATE klaudiusz_webuser_userprofile SET user_active = 1 WHERE UserId = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));  
    }    
        
    public function generatePasswordToken($id) {
        $token = sha1(time().rand(1, 10000));
        $sql = "UPDATE klaudiusz_webuser_userprofile SET user_password = ?, user_password_reminder_token = ?, user_password_reminder_exp_date = '2020-12-20 00:00:00' WHERE UserId = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($token, $token, $id));  
    }    
        
    public function checkNeed($id, $nid) {
        $sql = "SELECT * FROM dbo_jobs WHERE job_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        if($pre->rowCount()==0) {

            $sql = "DELETE FROM dbo_job_skills WHERE need_id = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($nid));
        }
    }            
    
    public function checkDuty($id, $nid) {
        $sql = "SELECT * FROM dbo_jobs WHERE job_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        if($pre->rowCount()==0) {

            $sql = "DELETE FROM dbo_job_skills_extra WHERE duty_id = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($nid));
        }
    }    

}