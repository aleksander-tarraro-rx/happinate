<?php
namespace src\rejestracja\model;

use app\helpers\Email;

class rejestracjaModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }

    public function register($email, $pass, $type = 1, $token = false, $tags = false, $friendToken = false, $main = null) {

        $sql = "INSERT INTO ".DB_PREF."user (user_email, user_password, user_type, user_register_date, user_register_token, user_main_tag_id) VALUES (AES_ENCRYPT(?, '".SALT."'), AES_ENCRYPT(md5(?), '".SALT."'), ?, ?, ?, ?);";
        $pre = $this->database->prepare($sql);
        $registerToken =  sha1($email.SALT);
        if($pre->execute(array($email, $pass, $type, date("Y-m-d H:i:s"), $registerToken, $main))) {
            $uid = $this->database->lastInsertId();
            if($token) {
                $sql2 = "UPDATE ".DB_PREF."application_oneclick SET oneclick_employee_id = ?, oneclick_token = null, oneclick_email = null WHERE oneclick_token = ?";
                $pre2 = $this->database->prepare($sql2); 
                $pre2->execute(array($uid, $token));
            }
            if($friendToken) {
                
                $sql4 = "SELECT user_id FROM ".DB_PREF."user_friend WHERE token = ?";
                $pre4 = $this->database->prepare($sql4); 
                $pre4->execute(array($friendToken));                
                $r = $pre4->fetch();
                
                $sql3 = "UPDATE ".DB_PREF."user_friend SET friend_id = ?, token = null, email = null, accepted = 1 WHERE token = ?";
                $pre3 = $this->database->prepare($sql3); 
                $pre3->execute(array($uid, $friendToken));

                $sql5 = "INSERT INTO ".DB_PREF."user_friend (user_id, friend_id, user_recommendation, accepted) VALUES (?,?,0,1)";
                $pre5 = $this->database->prepare($sql5); 
                $pre5->execute(array($uid, $r['user_id']));
                
            }
            if($tags) {
                $this->addTagToUser($uid, $tags);
            }

            $title = 'Witamy w happinate!';
            
            $url = 'http://'.$_SERVER['SERVER_NAME'].HOST.'rejestracja/potwierdzenie/'.$registerToken;
            
            $body = '<h3>Witamy w Happinate!</h3>';
            $body.= '<p class="lead">Największej bazie prac od zaraz!<br>Cieszymy się, że dołączyłeś do nas.</p>';
            $body.= '<p>Aby aktywować swoje konto kliknij poniższy link:</p>';
            $body.= '<p style="text-align:center;padding:10px 0 20px;"><a href="'.$url.'" target="_blank"  style="display:inline-block;padding: 5px 15px 7px 15px;background: #39ADAD;border: 1px solid #268888;color: #FFF;font-size: 15px;cursor:pointer;-webkit-transition: opacity 100ms ease-out;-moz-transition: opacity 100ms ease-out;-ms-transition: opacity 100ms ease-out;-o-transition: opacity 100ms ease-out;transition: opacity 100ms ease-out;opacity:1;font-family:sans-serif;text-align:center;text-decoration:none">Aktywuj konto</a></p>';
            $body.= '<p>Pozdrawiamy,<br>Zespół Happinate</p>';

            Email::sendTo($email, $title, $body, 'new.html');
            
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
     
    public function addTagToUser($user, $tags) {
        foreach($tags as $tag) {
            $sql = "INSERT INTO ".DB_PREF."user_tag (tag_user_id, tag_id) VALUES (?, ?);";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($user, $tag));
        }
    }
    
    public function getTags($admin = false) {
        if($admin) {
            $sql = "SELECT * FROM ".DB_PREF."tag WHERE tag_active = 0 ORDER BY tag_sort ASC";
        } else {
            $sql = "SELECT * FROM ".DB_PREF."tag WHERE tag_active = 1 ORDER BY tag_sort ASC";
        }
        $pre = $this->database->prepare($sql);
        $pre->execute();
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }         
    }
           
    public function checkEmailNotExist($email) {
        $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."');";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($email));
        if($pre->rowCount()!=0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function checkRegisterToken($token) {
        #die($token);
        $sql = "SELECT user_id, user_email FROM ".DB_PREF."user WHERE user_register_token = ? AND user_active = 0";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($token));
        $r = $pre->fetch();
        if(!empty($r['user_id'])) {
            
            $sql = "UPDATE ".DB_PREF."user SET user_active = 1, user_register_token = null WHERE user_id = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($r['user_id']));
            
            $userSalt = md5(microtime());

            $sql = "UPDATE ".DB_PREF."user SET user_salt = ?, user_ip = AES_ENCRYPT(?, '".SALT."') WHERE user_id = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($userSalt, $_SERVER['REMOTE_ADDR'], $r['user_id']));
            $_SESSION['userToken'] = sha1($r['user_id'].$r['user_email'].$userSalt);

            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }    
    }
    
    public function checkInvitation($token, $kandydat = false) {
        if(!$kandydat) {
            $sql = "SELECT oneclick_email FROM ".DB_PREF."application_oneclick WHERE oneclick_token = ?;";
        } else {
            $sql = "SELECT email FROM ".DB_PREF."user_friend WHERE token = ?;";
        }
        $pre = $this->database->prepare($sql);
        $pre->execute(array($token));
        
        if($r = $pre->fetch()) {
            if(!$kandydat) {
                return $r['oneclick_email'];
            } else {
                return $r['email'];
            }
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function sendAgainActivationLink($email) {
        $sql = "SELECT user_register_token FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."')";
        $pre = $this->database->prepare($sql); 
        $pre->execute(array($email));
        $res = $pre->fetch();

        $title = 'happinate.com - witamy!';
        $body = '<h3>Witamy w happinate.com!</h3>';
        $body .= '<p>Cieszymy się, że dołączyłeś do Nas!</p>';
        $body .= '<p>Kliknij w <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'rejestracja/potwierdzenie/'.$res['user_register_token'].'" target="_blank">link</a> i aktywuj swoje konto.</p>';
        $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz</p>';

        $_emailClass = $GLOBALS['_emailClass'];
        $_emailClass->addToList($email, $title, $body);

        return true;
    }
    
}