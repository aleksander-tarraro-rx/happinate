<?php

namespace src\rejestracja;

use app\helpers\Form;
use app\helpers\FlashMsg;
use src\BaseController;

class rejestracjaController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model, $user) {
        $this->template = $template;
        $this->model = $model;
        $this->user = $user;

        if($this->user) {
            FlashMsg::add('warning', 'Musisz się najpierw wylogować aby mieć dostęp do tej strony.');
            header("Location:".HOST);
        }
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        $this->template->display('rejestracja/index.tpl');
    }
    
    /* Rejestracja action */
    public function sukcesAction($queryName = false) {
        $this->template->display('rejestracja/sukces.tpl');
    }    
    
    /* Rejestracja action */
    public function potwierdzenieAction($queryName = false) {
        if($queryName) {
            if($this->model->checkRegisterToken($queryName)) {
                FlashMsg::add('success', 'Udało Ci się aktywować konto.');

                // ładowanie zweryfikowanego użytkownika
                global $_database;
                require_once HAPPINATE_ROOT_DIR . '/app/src/user.class.php';
                $_user = new \app\src\User($_database);
                $_user->checkUser();

                if (intval($_user->getUserType()) === 2 /*pracodawca*/) {
                    header("Location: " . HOST . "moje-konto/moje-ogloszenia");
                } else {
                    header("Location: " . HOST . "moje-konto");
                }
            } else {
                FlashMsg::add('error', 'Niestety proces aktywacji konta nie powiódł się.');
                header("Location: " . HOST);
            }
        } else {
            header("Location: " . HOST);
        }
    }

    public function zaproszenieAction($queryName = false) {
        if(!isset($queryName) || empty($queryName))
            header("Location: ".HOST);
        
        if(!$email = $this->model->checkInvitation($queryName)) 
            header("Location: ".HOST);
        
        $tagList = $this->model->getTags();
        $this->template->assign('tagList',$tagList);
        $this->template->assign('email', $email);
        
        if(isset($_POST['register-pass'])) {
            $error = '';
            // Validation
            if(empty($_POST['register-pass'])) {
                $error .= '<p>Proszę wpisać hasło.</p>';
            } elseif(strlen($_POST['register-pass'])<6) {
                $error .= '<p>Hasło musi posiadać więcej niż 6 znaków.</p>';
            }
            if($_POST['register-pass']!==$_POST['register-pass-2']) {
                $error .= '<p>Podane hasła muszą być identyczne.</p>';
            }
            if(!isset($_POST['main']) || count($_POST['main'])<=0) {
                $error .= '<p>Proszę wybrać jedną główną kategorię</p>';
            }
            if(!isset($_POST['tags']) || count($_POST['tags'])<2) {
                $error .= '<p>Proszę wybrać przynajmniej 2 kategorie pozostałe</p>';
            }
            if(!isset($_POST['checkbox1']) || $_POST['checkbox1']!=1) {
                $error .= '<p>Proszę przeczytać i zaakceptować regulamin serwisu.</p>';
            }
            if(!isset($_POST['checkbox2']) || $_POST['checkbox2']!=1) {
                $error .= '<p>Proszę wyrazić zgodę na przetwarzanie danych osobowych.</p>';
            }
            if(!$error) {
                if($this->model->register($email, $_POST['register-pass'], 1, $queryName, $_POST['tags'], false, $_POST['main'][0])) {
                    header("Location: " . HOST . "rejestracja/sukces");
                }
            } else {
                $this->template->assign('error', $error);
            }
        }
        $this->template->display('rejestracja/kandydatZaproszenie.tpl');
    }

    
    public function znajomyAction($queryName = false) {
        if(!isset($queryName) || empty($queryName))
            header("Location: ".HOST);
        
        $this->template->assign('queryName', $queryName);
        
        if(!$email = $this->model->checkInvitation($queryName, true)) 
            header("Location: ".HOST);
        
        $tagList = $this->model->getTags();

        $this->template->assign('tagList',$tagList);
        $this->template->assign('email', $email);
        
        if(isset($_POST['register'])) {
            $error = '';
            // Validation
            if(empty($_POST['register-pass'])) {
                $error .= '<p>Proszę wpisać hasło.</p>';
            } elseif(strlen($_POST['register-pass'])<6) {
                $error .= '<p>Hasło musi posiadać więcej niż 6 znaków.</p>';
            }
            if($_POST['register-pass']!==$_POST['register-pass-2']) {
                $error .= '<p>Podane hasła muszą być identyczne.</p>';
            }
            if(!isset($_POST['main']) || count($_POST['main'])<=0) {
                $error .= '<p>Proszę wybrać jedną główną kategorię</p>';
            }
            if(!isset($_POST['tags']) || count($_POST['tags'])<2) {
                $error .= '<p>Proszę wybrać przynajmniej 2 kategorie pozostałe</p>';
            }
            if(!isset($_POST['checkbox1']) || $_POST['checkbox1']!=1) {
                $error .= '<p>Proszę przeczytać i zaakceptować regulamin serwisu.</p>';
            }
            if(!isset($_POST['checkbox2']) || $_POST['checkbox2']!=1) {
                $error .= '<p>Proszę wyrazić zgodę na przetwarzanie danych osobowych.</p>';
            }
            if(!$error) {
                if($this->model->register($email, $_POST['register-pass'], 1, false, $_POST['tags'], $queryName, $_POST['main'][0])) {
                    header("Location: " . HOST . "rejestracja/sukces");
                }
            } else {
                $this->template->assign('error', $error);
                
            }
        }
        $this->template->display('rejestracja/zaproszenie.tpl');
    } 
    
    public function kandydatAction() {
        $tagList = $this->model->getTags();
        $this->template->assign('tagList',$tagList);

        if(isset($_POST['register-email'])) {
            $error = '';
            // Validation
            if(empty($_POST['register-email'])) {
                $error .= '<p>Proszę wpisać adres email.</p>';
            } elseif(!Form::validEmail($_POST['register-email'])) {
                $error .= '<p>Proszę podać prawidłowy adres email.</p>';
            } elseif($id = $this->model->checkEmailNotExist($_POST['register-email'])) {
                $error .= '<p>Podany adres email znajduje się już w bazie danych.</p>';
            }
            if(empty($_POST['register-pass'])) {
                $error .= '<p>Proszę wpisać hasło.</p>';
            } elseif(strlen($_POST['register-pass'])<6) {
                $error .= '<p>Hasło musi posiadać więcej niż 6 znaków.</p>';
            }
            if($_POST['register-pass']!==$_POST['register-pass-2']) {
                $error .= '<p>Podane hasła muszą być identyczne.</p>';
            }
            if(!isset($_POST['main']) || count($_POST['main'])<=0) {
                $error .= '<p>Proszę wybrać jedną główną kategorię</p>';
            }
            if(!isset($_POST['tags']) || count($_POST['tags'])<2) {
                $error .= '<p>Proszę wybrać przynajmniej 2 kategorie pozostałe</p>';
            }
            if(!isset($_POST['checkbox1']) || $_POST['checkbox1']!=1) {
                $error .= '<p>Proszę przeczytać i zaakceptować regulamin serwisu.</p>';
            }
            if(!isset($_POST['checkbox2']) || $_POST['checkbox2']!=1) {
                $error .= '<p>Proszę wyrazić zgodę na przetwarzanie danych osobowych.</p>';
            }
 
            if(!$error) {
                
                if($this->model->register($_POST['register-email'], $_POST['register-pass'], 1, false, $_POST['tags'], false, $_POST['main'])) {
                    header("Location: " . HOST . "rejestracja/sukces");
                }
                
            } else {
                $this->template->assign('error', $error);
            } 
        }
        $this->template->display('rejestracja/kandydat.tpl');
    }
    
    public function pracodawcaAction() {
        if(isset($_POST['register-email'])) {
            $error = '';
            // Validation
            if(empty($_POST['register-email'])) {
                $error .= '<p>Proszę wpisać adres email.</p>';
            } elseif(!Form::validEmail($_POST['register-email'])) {
                $error .= '<p>Proszę podać prawidłowy adres email.</p>';
            } elseif($id = $this->model->checkEmailNotExist($_POST['register-email'])) {
                $error .= '<p>Podany adres email znajduje się już w bazie danych.</p>';
            }
            if(empty($_POST['register-pass'])) {
                $error .= '<p>Proszę wpisać hasło.</p>';
            } elseif(strlen($_POST['register-pass'])<6) {
                $error .= '<p>Hasło musi posiadać więcej niż 6 znaków.</p>';
            }
            if($_POST['register-pass']!==$_POST['register-pass-2']) {
                $error .= '<p>Podane hasła muszą być identyczne.</p>';
            }
            if(!$error) {
                if($this->model->register($_POST['register-email'], $_POST['register-pass'], 2)) {
                    header("Location: " . HOST . "rejestracja/sukces");
                }
            } else {
                $this->template->assign('error', $error);
            } 
        }
        $this->template->display('rejestracja/pracodawca.tpl');
    }
    
    /* Rejestracja action */
    public function wyslijPonownieLinkAction($queryName = false) {
        
        if(isset($_POST['reminder-submit'])) {
            $error = '';
            // Validation
            if(empty($_POST['reminderemail'])) {
                $error .= '<p>Proszę wpisać adres email.</p>';
            } elseif(!Form::validEmail($_POST['reminderemail'])) {
                $error .= '<p>Proszę podać prawidłowy adres email.</p>';
            } elseif(!$this->model->checkEmailNotExist($_POST['reminderemail'])) {
                $error .= '<p>Podany adres email nie znajduje się jeszcze w bazie danych.</p>';
            }
            if(!$error) {
                if($this->model->sendAgainActivationLink($_POST['reminderemail'])) {
                    FlashMsg::add('success', 'Na podany adres email została wysłana wiadomość z linkiem aktywacyjnym.');
                    header("Location:".HOST."rejestracja/sukces");                   
                } else {
                    FlashMsg::add('warning', 'Nie udało się wysłać linka z linkiem aktywacyjnym. Proszę spróbować ponownie później.');
                    header("Location:".HOST."rejestracja/wyslij-ponownie-link");                    
                }
            } else {
                $this->template->assign('error', $error);
            } 
        }
        
        $this->template->display('rejestracja/wyslijPonownieLink.tpl');
    }    
    
}