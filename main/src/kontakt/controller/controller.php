<?php

namespace src\kontakt;

use app\helpers\Form;
use app\helpers\FlashMsg;
use src\BaseController;

class kontaktController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        if(isset($_POST['contact_submit'])) {
            $error = '';
            // Validation
            if(empty($_POST['name'])) {
                $error .= '<p>Proszę wpisać imię i nazwisko.</p>';
            }
            if(empty($_POST['email'])) {
                $error .= '<p>Proszę wpisać adres email.</p>';
            } elseif(!Form::validEmail($_POST['email'])) {
                $error .= '<p>Proszę podać prawidłowy adres email.</p>';
            }
            if(empty($_POST['message'])) {
                $error .= '<p>Proszę wpisać wiadomość.</p>';
            }
            if(!$error) {
                $msgBody  = '<h1>Wiadomość wygenerowana automatycznie ze strony Happinate.</h1>';
                $msgBody .= '<h3>Wiadomość od użytkownika : '.$_POST['name'].' (<a href="mailto:'.$_POST['email'].'">'.$_POST['email'].'</a>).</h3>';
                $msgBody .= '<p><strong>Treść wiadomości</strong></p>';
                $msgBody .= '<div style="padding: 10px; margin: 5px; border: #CCC thin solid;">'.$_POST['message'].'</div>';
                
                
                $_emailClass = $GLOBALS['_emailClass'];
                $_emailClass->addToList('tadeuszhyzy@gmail.com', 'Wiadomość ze strony happinate.com', $msgBody);
                $_emailClass->addToList('piotr.chabzda@gmail.com', 'Wiadomość ze strony happinate.com', $msgBody);
                FlashMsg::add('success', 'Dziękujemy za wyslanie wiadomości.');
                header("Location: " . HOST . "kontakt");
            } else {
                $this->template->assign('error', $error);
            }
        }
        $this->template->display('kontakt/index.tpl');
    }
}