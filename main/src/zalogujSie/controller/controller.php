<?php
namespace src\zalogujSie;

use app\helpers\Form;
use app\helpers\FlashMsg;
use src\BaseController;

class zalogujSieController extends BaseController {
    
    private $template;
    private $model;
    private $user;

    public function __construct($template, $model, $user) {
        $this->template = $template;
        $this->model = $model;
        $this->user = $user;
        
        if($this->user) {
            FlashMsg::add('success', 'Jesteś już zalogowany!');
            header("Location: " . HOST. "moje-konto");
        }
    }
    
    /* Index action */
    public function indexAction($queryName = false) {

        if(isset($_POST['login-submit'])) {
            $error = '';
            // Validation
            if(empty($_POST['login-email'])) {
                $error .= '<p>Proszę wpisać adres email.</p>';
            } elseif(!Form::validEmail($_POST['login-email'])) {
                $error .= '<p>Proszę podać prawidłowy adres email.</p>';
            }
            if(empty($_POST['login-pass'])) {
                $error .= '<p>Proszę wpisać hasło.</p>';
            }
            
            $_POST = Form::sanitizeArray($_POST);
            
            if(!$error) {
                $loginRes = $this->model->login($_POST['login-email'], $_POST['login-pass']);
                if($loginRes == 1) {
                    FlashMsg::add('success', 'Udało sie poprawnie zalogować!');
                    header("Location: " . HOST. "moje-konto");
                } elseif($loginRes == 3) {
                    FlashMsg::add('success', 'Udało sie poprawnie zalogować!');
                    header("Location: " . HOST. "moje-konto/moje-ogloszenia");
                } elseif($loginRes == 2) {
                    $this->template->assign('error', 'Twoje konto nie jest jeszcze aktywne. Proszę sprawdzić pocztę (również folder SPAM) lub wysłać potwierdzenie klikając na poniższy link.<p>- Ponowna aktywacja</p>');  
                } else {
                    $this->template->assign('error', 'Dane są niepoprawne. Proszę spróbować ponownie.');  
                }
            } else {
                $this->template->assign('error', $error);
            } 
        }
        $this->template->display('zalogujSie/index.tpl');
    }
    
    public function redirectAction($queryName = false) {
        
        $redirectURL = str_replace('-','/',$queryName);

        if(isset($_POST['login-submit'])) {
            $error = '';
            // Validation
            if(empty($_POST['login-email'])) {
                $error .= '<p>Proszę wpisać adres email.</p>';
            } elseif(!Form::validEmail($_POST['login-email'])) {
                $error .= '<p>Proszę podać prawidłowy adres email.</p>';
            }
            if(empty($_POST['login-pass'])) {
                $error .= '<p>Proszę wpisać hasło.</p>';
            }
            
            $_POST = Form::sanitizeArray($_POST);
            
            if(!$error) {
                $loginRes = $this->model->login($_POST['login-email'], $_POST['login-pass']);
                if($loginRes == 1) {
                    FlashMsg::add('success', 'Udało sie poprawnie zalogować!');
                    header("Location: " . HOST . $redirectURL);
                } elseif($loginRes == 2) {
                    $this->template->assign('error', 'Twoje konto nie jest jeszcze aktywne. Proszę sprawdzić pocztę (również folder SPAM) lub wysłać potwierdzenie klikając na poniższy link.<p>- Ponowna aktywacja</p>');  
                } else {
                    $this->template->assign('error', 'Dane są niepoprawne. Proszę spróbować ponownie.');  
                }
            } else {
                $this->template->assign('error', $error);
            } 
        }
        $this->template->display('zalogujSie/index.tpl');
    }
}