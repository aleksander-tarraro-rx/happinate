<?php
namespace src\zalogujSie\model;

class zalogujSieModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }

    public function login($login, $pass) {
        $sql = "SELECT user_type, user_active, user_id, user_email FROM ".DB_PREF."user WHERE AES_DECRYPT(user_email, '".SALT."') = ? AND user_password = AES_ENCRYPT(md5(?), '".SALT."');";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($login, $pass));
        if($pre->rowCount()!=0) {
            
            $res = $pre->fetch();
            if($res['user_active']) {
                $userSalt = md5(microtime());
                $sql = "UPDATE ".DB_PREF."user SET user_salt = ?, user_ip = AES_ENCRYPT(?, '".SALT."') WHERE user_id = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($userSalt, $_SERVER['REMOTE_ADDR'], $res['user_id']));
                $_SESSION['userToken'] = sha1($res['user_id'].$res['user_email'].$userSalt);
            } else {
                return 2;
            }
            if($res['user_type']==2) {
                return 3;
            } else {
                return 1;
            }
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }
    }
}