<?php

namespace src\praca;

use app\helpers\Form;
use app\helpers\FlashMsg;
use src\BaseController;

class pracaController extends BaseController {
    
    private $template;
    private $model;
    private $user;

    private static $monthsNames = array(
        1 => "Styczeń",
        2 => "Luty",
        3 => "Marzec",
        4 => "Kwiecień",
        5 => "Maj",
        6 => "Czerwiec",
        7 => "Lipiec",
        8 => "Sierpień",
        9 => "Wrzesień",
        10 => "Październik",
        11 => "Listopad",
        12 => "Grudzień",
    );

    private static $dayNames = array(
        "Monday" => "Poniedziałek",
        "Tuesday" => "Wtorek",
        "Wednesday" => "Środa",
        "Thursday" => "Czwartek",
        "Friday" => "Piątek",
        "Saturday" => "Sobota",
        "Sunday" => "Niedziela",
    );


    public function __construct($template, $model, $user) {
        $this->template = $template;
        $this->model = $model;
        $this->user = $user;
        $this->template->assign('_model', $this->model);
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        header("Location:".HOST);
    }    
    
    /* Integer action */
    public function integerAction($int, $queryName = false) {

        if(!$job = $this->model->getJob($int)) 
            header('Location:'.HOST.'oferty-pracy');
        
        if($this->user && $this->user->getUserType()==1) {
            $this->template->assign('userCVList', $this->user->getCVList());
        }
        
        // Wysyłanie ogłoszenia
        if(isset($_POST['inviteFriend']) && $_POST['inviteFriend'])  {
            if(!isset($_POST['url']) || empty($_POST['url']) || !isset($_POST['friends']) || empty($_POST['friends'])) {
                header('Location:'.HOST.'praca/'.$job['job_id'].'/'.$job['job_url_text']);
            } 
            
            if($this->model->sendJobToFriends($_POST['url'], $_POST['friends'])) {
                FlashMsg::add('success', 'Udało Ci się wysłać zaproszenie.');
                header('Location:'.HOST.'praca/'.$job['job_id'].'/'.$job['job_url_text']);
            } else {
                FlashMsg::add('warning', 'Niestety nie udało Ci się wysłać oferty do znajomych.');
                header('Location:'.HOST.'praca/'.$job['job_id'].'/'.$job['job_url_text']);
            }
        }
        
        
        if(isset($_POST['apply_submit'])) {
            $error = '';
            $errorArray = array();
            // Validation
            if(empty($_POST['apply_email'])) {
                $error .= '<p>Proszę wpisać adres email.</p>';
                $errorArray['apply_email'] = true;
            } elseif(!Form::validEmail($_POST['apply_email'])) {
                $error .= '<p>Proszę podać prawidłowy adres email.</p>';
                $errorArray['apply_email'] = true;
            } elseif($this->model->checkIfUserApplied($int, false, $_POST['apply_email'])) {
                $error .= '<p>Już aplikowałeś na tę pracę. Zaloguj się na konto by zobaczyć szczegóły.</p>';
                $errorArray['apply_email'] = true;
            }
            
            if(!isset($_POST['warunki']) || empty($_POST['warunki']) || !$_POST['warunki']) {
                $error .= '<p>Proszę wyrazić zgode na przetwarzanie danych osobowych.</p>';
                $errorArray['warunki'] = true;
            }
            
            $file = null;
            if(isset($_FILES['cv']) && $_FILES['cv']['error']==0) {
                $allowed = array('application/pdf', 'image/jpeg', 'image/gif', 'image/png', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.oasis.opendocument.text');
                if(!in_array($_FILES['cv']['type'], $allowed)) {
                    $error .= '<p>Przesłane CV musi być formatu pdf, jpg, png, gif lub doc.</p>';
                    $errorArray['cv'] = true;
                }             
                if($_FILES['cv']['size']>3048000) {
                    $error .= '<p>Przesłane logo waży zbyt dużo (maksymalna waga to 2mb.</p>';
                    $errorArray['cv'] = true;
                }
                $file = $_FILES['cv']; 
            } elseif($_POST['cv_id']) {
              
            } else {
                $error .= '<p>Proszę dodać plik CV.</p>';
                $errorArray['cv'] = true;
            }
            
            if(!$error) {
                if($this->model->applyToJob($int, $_POST, $file)) {
                    FlashMsg::add('success', 'Udało się poprawnie złożyć aplikację.');
                    header("Location:".HOST."praca/".$int);       
                } else {
                    FlashMsg::add('warning', 'Niestety nie udało Ci się złożyć aplikacji. Spróbuj ponownie później.');
                    header("Location:".HOST."praca/".$int);       
                }
            } else {
                $this->template->assign('errorArray', $errorArray);
                $this->template->assign('error', $error);
            } 
        }


        // kalendarz
        $_calendar = $this->model->getCalendar($job['job_id']);
        if(!empty($_calendar))
        {
            $this->template->assign('isCalendar', true);
        }
        else
        {
           $this->template->assign('isCalendar', false); 
        }        

        #echo"<pre>";print_r($job);echo"</pre>";
        $this->template->assign('job', $job);
        $this->template->display('praca/index.tpl');
    }
    
    public function oneClickAction($queryName = false) {  
        
        $exp = explode(',', $queryName);
        
        if(!$job = $this->model->getJob($exp[0], false)) {
            FlashMsg::add('warning', 'Brak ogłoszenia o danym ID.');
            header("Location:".HOST);    
        }
        
        $id = $this->model->checkUser($exp[0]);
        
        if($this->model->checkIfUserApplied($job['job_id'],$id)) {
            $applied = true;
        } else {
            $applied = false;
        }
        
        $this->template->assign('applied', $applied); 
        
        if($id==-1) {
            FlashMsg::add('warning', 'Zaloguj się aby mieć dostęp do tej strony.');
            header("Location:".HOST."zaloguj-sie/redirect/praca-oneClick-".$exp[0]);    
        } elseif(!$id) {
            FlashMsg::add('warning', 'Nie masz dostępu do tej części strony.');
            header("Location:".HOST);       
        }
        
        if(!$this->model->checkVacancies($exp[0], $job['job_vacancies'])) {
            $vacancies = true;
        } else {
            $vacancies = false;
        }
        
        $this->template->assign('vacancies', $vacancies); 
        
        if(isset($exp[1]) && $exp[1]=='sukces') {
            $this->template->display('praca/oneclick-sukces.tpl');
        } elseif(isset($exp[1]) && $exp[1]=='rezerwowy') {
            $this->template->display('praca/oneclick-rezerwowy.tpl');
        } elseif(isset($exp[1]) && $exp[1]=='akceptuj') {
            if(!$applied) {
                if($vacancies) {
                    if($this->model->oneClick($exp[0], $id)) {
                        header("Location:".HOST."praca/oneclick/".$exp[0].",sukces");
                    } else {
                        FlashMsg::add('error', 'Coś poszło nie tak. Spróbuj ponownie później.');
                        header("Location:".HOST."praca/oneclick/".$exp[0]);
                    } 
                } else {
                    if($this->model->oneClick($exp[0], $id, false)) {
                        header("Location:".HOST."praca/oneclick/".$exp[0].",rezerwowy");
                    } else {
                        FlashMsg::add('error', 'Coś poszło nie tak. Spróbuj ponownie później.');
                        header("Location:".HOST."praca/oneclick/".$exp[0]);
                    } 
                }
            } else {
                FlashMsg::add('warning', 'Już aplikowałeś na to stanowisko.');
                header("Location:".HOST);
            }
        } else {
            $this->template->assign('job', $job);
            $this->template->display('praca/oneclick.tpl');
        }

    }
    /*
    public function agencjaAction($queryName = false) {
        $job = $this->model->jobFeedXml($queryName);
        $this->template->assign('job', $job);

        $this->template->display('praca/agencja.tpl'); 
    }    
    */
    public function ajaxAction($queryName = false) {
        if(isset($_POST['favJob']) && isset($_POST['id'])) {
            if($_POST['favJob']=='true') {
                $this->model->favJob($_POST['id']);
            } else {
                $this->model->unfavJob($_POST['id']);
            }
        } else {
            return false;
        }
    }

    /**
     * strona glowna kalendarza w ofercie
     * @param  integer $queryName id oferty
     */
    public function kalendarzAction($queryName=0)
    {

        $queryName = (int)$queryName;
        if(empty($queryName))
        {
            header("Location:".HOST);
        }

        if(!$job = $this->model->getJob($queryName))
        {
            header('Location:'.HOST);             
        }

        // kalendarz
        $_calendar = $this->model->getCalendar($job['job_id']);
        if(!empty($_calendar))
        {

            $structure = self::getCalendarStructure($_calendar);

            // echo "<pre>";
            // print_r($structure);
            // echo "</pre>";

            $this->template->assign('structure', $structure);
            $this->template->assign('calendar', $_calendar);
        }
        else
        {
            header("Location:".HOST); 
        }        

        $this->template->assign('job', $job);
        $this->template->display('praca/kalendarz.tpl');
    }


    /**
     * strona danego dnia
     * @param  integer $queryName id oferty
     */
    public function kalendarzDzienAction()
    {

        if(empty($_REQUEST['url']))
        {
            header("Location:".HOST);
        }

        $_temp = explode('/', $_REQUEST['url']);

        if(!empty($_POST['act']) && $_POST['act'] == "addme")
        {
            $res = array();        
            $res = $this->model->setCalendarCandidate($_POST['data']);   
            FlashMsg::add($res['class'], $res['msg']);       
            header("Location:".HOST.$_temp[0]."/".$_temp[1]."/".$_temp[2]."/".$_temp[3]);
            exit;
        }

        if(count($_temp) != 4)
        {
            header("Location:".HOST);
            exit;
        }

        $job_id = (int)$_temp[2];
        $day = $_temp[3];

        if(empty($job_id) OR empty($day))
        {
            header("Location:".HOST);
            exit;
        }

        if(!$job = $this->model->getJob($job_id))
        {
            header('Location:'.HOST);   
            exit;          
        }

        $_calendar = $this->model->getCalendar($job_id);
        if(!self::isValidDay($_calendar, $day))
        {
            header("Location:".HOST);
            exit;
        }

        $_d = array();
        $_d['job_id'] = $job_id;
        $_d['date'] = $day;
        
        $_calendarCandidate = $this->model->getCalendarCandidate($_d);
        
        if(!empty($_calendarCandidate))
        {
            foreach($_calendarCandidate as $k => $v)
            {
                $usersCounterByHour[$v['hour']] = !empty($usersCounterByHour[$v['hour']]) ? $usersCounterByHour[$v['hour']] + 1 : 1;
            }            
        }
        $hours = array();
        for($i=6; $i<=22; $i++)
        {
            $hours[$i]['active'] = '1'; 
            $hours[$i]['label'] = $i . "<sup>00</sup>";
            $hours[$i]['value'] = $i;
            $hours[$i]['usersCounter'] = !empty($usersCounterByHour[$i]) ? $usersCounterByHour[$i] : 0;

            if($i < $_calendar['hoursFrom'] OR $i > $_calendar['hoursTo'])
            {
                $hours[$i]['css'] = "notactive";
                $hours[$i]['active'] = '0';   
            }
            else
            {
                $hours[$i]['css'] = "free";
            }

            if($hours[$i]['usersCounter'] >= $_calendar['candidatePerHour'] )
            {
                $hours[$i]['css'] = "notactive";
                $hours[$i]['active'] = '0';                
            }
           
        }

        $this->template->assign('usertype', $this->user->getUserType());
        $this->template->assign('calendar', $_calendar);
        $this->template->assign('hours', $hours);
        $this->template->assign('job', $job);
        $this->template->assign('day', $day);
        $this->template->display('praca/kalendarz-dzien.tpl');
    }


    /**
     * sprawdzenie czy dzien w url jest prawidlowy 
     * @param  array  $_calendar 
     * @param  string  $day  format yyyy-mm-dd  
     * @return boolean true/false
     */
    private static function isValidDay($_calendar, $day)
    {
        if(empty($_calendar))
        {
            return false;
        }
        $date = $_calendar['start'];
        $end_date = $_calendar['stop'];
        while (strtotime($date) <= strtotime($end_date)) 
        {
            if($date == $day)
            {
                return true;
            }
            $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
        }
        return false;
    }

    /**
     * generuje strukturę dużego kalendarza
     * @param  array $_calendar rekord z bazy
     * @return array tablica zawierajaca strukture w podzaile na lata miesiace i dni z wykluczeniami
     */
    private static function getCalendarStructure($_calendar)
    {
        if(empty($_calendar['start']) OR empty($_calendar['stop']))
        {
            return false;
        }
 
        $_temp = array();
        $date = $_calendar['start'];
        $end_date = $_calendar['stop'];
 
        while (strtotime($date) <= strtotime($end_date)) 
        {
            $_d = explode('-', $date);
            $month = self::$monthsNames[(int)$_d[1]];
            $css = 'active';

            if($_calendar['exludeSunSut'])
            {
                if(self::isWeekend($date))
                {
                    $css = "notactive";
                }
            }
        
            if(strtotime(date("Y-m-d")) >= strtotime($date))
            {
                $css = "notactive";
            }

            $_temp[ $_d[0] ][ $month ][ $_d[2] ] = array( 
                "label" => $_d[2],
                "css" => $css,
                "title" => self::$dayNames[ date('l',strtotime($date)) ],
                "url" => $_d[0] . "-" . $_d[1] . "-" . $_d[2], 
            );

            $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
        }

        return $_temp;
    }
    

    /**
     * pomocnicza metoda sprawdzaja czy podana data to weekend
     * @param  string  $date data w formacie yyyy-mm-dd
     * @return boolean true/false
     */
    private static function isWeekend($date) 
    {
        return (date('N', strtotime($date)) >= 6);
    }    
}