<?php

namespace src\praca\model;

use app\helpers\Form;

class pracaModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
 
    public function jobFeedXml($id = false) {
        $array = array();
        
        if($id) {
            $url = "http://startpraca.pl/feeds/happinate.com/";
            $xmlString = file_get_contents($url);
            $xml = new \SimpleXMLElement($xmlString, LIBXML_NOCDATA);

            foreach ($xml->offer as $element) {
                if($element->id == $id) {
                    $array['job_id'] = ''.$element->id.'';
                    $array['job_position'] = ''.$element->nazwa_stanowiska.'';
                    $array['job_trade'] = ''.$element->branze.'';
                    $array['job_region'] = ''.$element->region.'';
                    $array['job_city'] = ''.$element->miasta.'';
                    $array['job_logo'] = ''.$element->logo.'';
                    $array['job_tresc'] = ''.$element->tresc_ogloszenia.'';
                    $array['job_publish_date'] = ''.$element->data_publikacji.'';
                    $array['job_created_date'] = ''.$element->data_publikacji.'';
                    $array['job_publish_date_end'] = ''.$element->data_konca_publikacji.'';
                    $array['job_url'] = ''.$element->url.'';
                }
            }
        } else {
            $url = "http://startpraca.pl/feeds/happinate.com/";
            $xmlString = file_get_contents($url);
            $xml = new \SimpleXMLElement($xmlString, LIBXML_NOCDATA);

            $array = array();
            foreach ($xml->offer as $element) {
                $i = 0;

                $array[$i]['job_id'] = ''.$element->id.'';
                $array[$i]['job_position'] = ''.$element->nazwa_stanowiska.'';
                $array[$i]['job_city_id'] = ''.$element->id.'';
                $array[$i]['job_rate_min'] = ''.$element->id.'';
                $array[$i]['job_rate_max'] = ''.$element->id.'';
                $array[$i]['job_logo'] = ''.$element->id.'';
                $array[$i]['job_volunteering'] = ''.$element->id.'';
                $array[$i]['job_hide_salary'] = ''.$element->id.'';

                $array[$i]['job_url_text'] = $this->createURLText($element->nazwa_stanowiska);
                $array[$i]['job_publish_date'] = ''.$element->data_publikacji.'';
                $array[$i]['job_created_date'] = ''.$element->data_publikacji.'';
                $array[$i]['job_publish_date_end'] = ''.$element->data_konca_publikacji.'';
                $array[$i]['job_url'] = ''.$element->url.'';

                $i++;
            }
        }

        return $array;
    }
    
    public function getJob($id, $view = true) {
        $sql = "SELECT j.*, c.type_name as job_contract_type, t.tag_name as job_tag_type FROM ".DB_PREF."job as j LEFT JOIN ".DB_PREF."job_contract_types as c ON c.type_id = j.job_contract_type_id LEFT JOIN ".DB_PREF."tag as t ON t.tag_id = j.job_trade_type_id WHERE j.job_publish_date < NOW() AND j.job_id = ? LIMIT 1";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        
        if($pre->rowCount()!=0) {
            $jobList = $pre->fetch();
            if($view) $this->view($id, $jobList['job_views']);
            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }
    
    public function view($id, $count) {
        $count++;
        $sql = "UPDATE ".DB_PREF."job SET job_views = ? WHERE job_id = ?";
        $pre = $this->database->prepare($sql);

        if($pre->execute(array($count, $id))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }
    
    public function getDuties($id) {
        $sql = "SELECT duty_text FROM ".DB_PREF."job_duty WHERE duty_job_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }     
    }
         
    public function getNeeds($id) {
        $sql = "SELECT need_text FROM ".DB_PREF."job_need WHERE need_job_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }      
    }
           
    public function getUser() {
        $sql = "SELECT 
            AES_DECRYPT(user_name, '".SALT."') as user_name, 
            AES_DECRYPT(user_surname, '".SALT."') as user_surname,
            AES_DECRYPT(user_email, '".SALT."') as user_email
            FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
        $res = $this->database->prepare($sql);
        $res->execute(array($_SESSION['userToken']));
        if($res->rowCount()) {
            return $res->fetch();
        } else {
            return true;
        }     
    }

    /**
     * pobierz dane do kalendarza rekrutacji
     * @param  int $jid id oferty
     * @return array dane do kolendarza
     */
    public function getCalendar($jid)
    {
        $sql = "SELECT * FROM happinate_job_calendar WHERE job_id=?";
        $res = $this->database->prepare($sql);
        $res->execute(array($jid));

        if($res->rowCount())
        {
            return $res->fetch();
        } 
        return array();       
    }

    /**
     * pobierz dane o zarezerowanycg godzinach 
     * @param  array $data 
     * @return array
     */
    public function getCalendarCandidate($data)
    {
        $sql = "SELECT * FROM happinate_job_calendar_user WHERE job_id=? AND date=?";
        $pre = $this->database->prepare($sql);        
        $pre->execute(array($data['job_id'], $data['date']));
        $res = $pre->fetchAll();
        return $res;
    }    


    /**
     * zapisz kandydata na spotkanie
     * @param array $data dane z formularza
     */
    public function setCalendarCandidate($data)
    {
        $check = $this->checkCandidate($data['job_id'], $data['date'], $data['hour']);
        $res = array();
        if($check !== 4)
        {
            switch($check)
            {
                case 1:
                    $res = array(
                        'msg' => 'Musisz być zalogowany, aby zapisać się na spotkanie',
                        'class' => 'error',
                    );
                    break;
                case 2:
                    $res = array(
                        'msg' => 'Jesteś już zapisany na spotkanie.',
                        'class' => "error",
                    );
                    break;
                case 3:
                    $res = array(
                        'msg' => 'W Twoim kalendarzu istnieje już wpis dotyczący tej godziny, wybierz inną.',
                        'class' => "error",
                    );
                    break;
            }
        }
        else
        {
            // zapis do tabeli ogloszenia
            $sql = "INSERT INTO ".DB_PREF."job_calendar_user (job_id, user_id, date, hour)";
            $sql .= "VALUES(?,?,?,?)";
            $res = $this->database->prepare($sql);
            $res->execute(array($data['job_id'], $this->getUserId(), $data['date'], $data['hour']));    

            // zapis do tabeli kandydata
            $information = '';
            $jobData = $this->getJob((int)$data['job_id']);
            $information .= $jobData['job_position'] . ' ' . 
                            $jobData['job_employeer_place_direct_address'] . ' '.
                            $jobData['job_employeer_place_direct_city'] . ' ';
            $time = str_pad($data['hour'], 2, "0", STR_PAD_LEFT);
            $time .= ":00:00";
  
            $sql = "INSERT INTO ".DB_PREF."user_calendar(user_id, date, time, information)";
            $sql .= "VALUES(?,?,?,?)";
            $res = $this->database->prepare($sql);
            $res->execute(array($this->getUserId(), $data['date'], $time, $information)); 
            $res = array(
                'msg' => 'Zostałeś zapisany na spotkanie.',
                'class' => "success",
            );
        }
        return $res;

    }

    /**
     * sprawdz czy kandydat nie zpisal sie juz na spotkanie w tym ogloszeniu i czy ma wolny termin 
     * @param  int $job_id
     * @return [type]         [description]
     */
    private function checkCandidate($job_id, $date, $time)
    {
        if(!$id = $this->getUserId())
        {
            return 1;
        }

        $result = 4;

        // czy zapisał sie juz na spotkanie
        $sql = "SELECT * FROM ".DB_PREF."job_calendar_user WHERE job_id=? AND user_id=?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($job_id, $id));
        $res = $pre->fetch();
        if(!empty($res)) 
        {
            $result = 2;
        }

        if($result == 4)
        {
            // czy mamy wony termin w naszym kalendarzu
            $time = str_pad($time, 2, "0", STR_PAD_LEFT);
            $time .= ":00:00";
            $sql = "SELECT * FROM ".DB_PREF."user_calendar WHERE user_id=? AND date=? AND time=?";
            $pre = $this->database->prepare($sql);        
            $pre->execute(array($id, $date, $time));
            $res = $pre->fetchAll();
            if(!empty($res))
            {
                $result = 3;
            }            
        }

        return $result;
    }
  



    public function checkUserExists($email) {
        $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."')";
        $res = $this->database->prepare($sql);
        $res->execute(array($email));
        $res = $res->fetch();
        if(count($res)==1) {
            return $res['user_id'];
        } else {
            return false;
        }           
    }
    
    public function register($email, $name) {
        $sql = "INSERT INTO ".DB_PREF."user (user_email, user_password, user_type, user_register_date, user_register_token, user_name, user_surname, user_active) VALUES (AES_ENCRYPT(?, '".SALT."'), AES_ENCRYPT(md5(?), '".SALT."'), ?, ?, ?, AES_ENCRYPT(?, '".SALT."'), AES_ENCRYPT(?, '".SALT."'), 1);";
        $pre = $this->database->prepare($sql);
        $pass = Form::generatePassword();
        $registerToken = sha1($email.SALT);
        $tmp = explode(' ', $name);
        if(!isset($tmp[1])) {
            $tmp[1] = '';
        }
        if($pre->execute(array($email, $pass, 1, date("Y-m-d H:i:s"), $registerToken, $tmp[0], $tmp[1]))) {
            $uid = $this->database->lastInsertId();

            $title = 'happinate.com - aplikacja';
            $body = '<p>Twój login to: '.$email.'</p><p>Hasło: '.$pass.'</p>';
            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($email, $title, $body);

            return $uid;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }    
    
    public function checkUser($jid) {
        if(!$id = $this->getUserId()){
            return -1;
        }
        
        $sql = "SELECT count(id) as count FROM ".DB_PREF."job_oneclick WHERE job_id = ? AND user_id = ?";
        $res = $this->database->prepare($sql);
        
        $res->execute(array($jid, $id));
        
        $res = $res->fetch();
        
        if($res['count']>0) {
            return $id;
        } else {
            return false;
        }         
    }
        
    public function checkIfUserApplied($jid, $id, $email = false) {
        
        if($id) {
            $sql = "SELECT count(id) as count FROM ".DB_PREF."job_oneclick WHERE job_id = ? AND user_id = ? AND status != 0";
            $res = $this->database->prepare($sql);

            $res->execute(array($jid, $id));

            $res = $res->fetch();

            if($res['count']>0) {
                return true;
            } else {
                return false;
            }         
        } elseif($email) {
            
            $uid = $this->getUserId($email);
           
            $sql = "SELECT count(application_id) as count FROM ".DB_PREF."application WHERE application_job_id = ? AND application_user_id = ?";
            $res = $this->database->prepare($sql);

            $res->execute(array($jid, $uid));

            $res = $res->fetch();

            if($res['count']>0) {
                return true;
            } else {
                return false;
            } 
        } else {
            return false;
        }
    }
            
    public function checkIfUserAppliedToNormalJob($jid, $id) {
        $sql = "SELECT count(application_id) as count FROM ".DB_PREF."application WHERE application_job_id = ? AND application_user_id = ?";
        $res = $this->database->prepare($sql);
        
        $res->execute(array($jid, $id));
        
        $r = $res->fetch();

        if($r['count']>0) {
            return true;
        } else {
            return false;
        }         
    }
    
    public function getUserId($email = false) {
        if($email) {
            $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."')";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($email));            
                
            if($pre->rowCount()!=0) {
                $id = $pre->fetch();
                return $id['user_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }              
        } else {

            if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
                $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($_SESSION['userToken']));
            } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) { 
                $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($_SESSION['userTokenFB']));            
            } else {
                return false;
            }
            if($pre->rowCount()!=0) {
                $id = $pre->fetch();
                return $id['user_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }      
        }
    }
    
    public function getJobContactEmail($id) {
        $sql = "SELECT AES_DECRYPT(user_email,'".SALT."') as email FROM ".DB_PREF."user as u LEFT JOIN ".DB_PREF."job as j ON u.user_id = j.job_added_by WHERE j.job_id = ?";
        $res = $this->database->prepare($sql);
        $res->execute(array($id));
        $res = $res->fetch();
        if(count($res)==1) {
            return $res['email'];
        } else {
            return false;
        }  
    }

    public function applyToJob($jid, $data, $cv = false, $vac = true) {
        if($cv) {
            $pathParts = pathinfo($cv['name']);
            $extension = $pathParts['extension'];
            
            $fileName = strtolower(str_replace('.'.$extension,'',$cv['name']));
        }

        if(is_array($data)) {
            if(!$uid = $this->checkUserExists($data['apply_email'])) {
                
                $uid = $this->register($data['apply_email'], $data['apply_name']);
            } 
            
        } else {
            $uid = $data;
        }
        
        if(is_array($data)){
            if($data['cv_id']) {
                $sql = "INSERT INTO ".DB_PREF."application (application_user_id, application_job_id, application_date, application_cv) VALUES (?, ?, NOW(), ?);";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($uid, $jid, $data['cv_id']));
            } else {
                if($cv) {
                    $cvName = sha1($fileName.time()).'.'.$extension;
                    $fileUploadPath = __DIR__ . '/../../../upload/userCV/' . $cvName;

                    if(!move_uploaded_file($cv['tmp_name'], $fileUploadPath)) {
                        return false;
                    }
                    
                    $sql2 = "INSERT INTO ".DB_PREF."user_cv (cv_user_id, cv_date, cv_src, cv_name) VALUES (?, NOW(), ?, ?);";
                    $pre2 = $this->database->prepare($sql2);
                    $pre2->execute(array($uid, $cvName, $fileName));
                    
                    $cid = $this->database->lastInsertId();
                }
                $sql = "INSERT INTO ".DB_PREF."application (application_user_id, application_job_id, application_date, application_cv) VALUES (?, ?, NOW(), ?);";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($uid, $jid, $cid));
            }
        } else {
            if($vac) {
                $sql = "INSERT INTO ".DB_PREF."application (application_user_id, application_job_id, application_date, application_status) VALUES (?, ?, NOW(), 3);";
            } else {
                $sql = "INSERT INTO ".DB_PREF."application (application_user_id, application_job_id, application_date, application_status) VALUES (?, ?, NOW(), 4);";
            }
            $pre = $this->database->prepare($sql);
            $pre->execute(array($uid, $jid)); 
        }
        if($aid = $this->database->lastInsertId()) {
            $jobEmail = $this->getJobContactEmail($jid);
            $pre->closeCursor();
            
            $title = 'happinate.com - odpowiedź na Twoje ogłoszenie';
            $body = '<h3>Pracodawco, otrzymałeś aplikację na Twoje ogłoszenie.</h3>';
            $body .= '<p>Aby zobaczyć więcej szczegółów, zaloguj się na swoje konto oraz przejdź do zakładki "Moje ogłoszenia".</p>';
            $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz!</p>';  

            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($jobEmail, $title, $body, 'default.html');
            return $aid;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
     
    public function getUserLogo($id) {
        $sql = "SELECT user_logo FROM ".DB_PREF."user WHERE user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            $logo = $pre->fetch();
            return $logo['user_logo'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function getCityName($id) {
        $sql = "SELECT city_text FROM ".DB_PREF."city WHERE city_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['city_text'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    } 
    
    public function checkVacancies($jobID, $count) {
        
        $sql = "SELECT COUNT(id) as count FROM ".DB_PREF."job_oneclick WHERE job_id = ? AND status = 1";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($jobID));
        $res = $pre->fetch();
        #echo $res['count'];
        if($res['count']>=$count) {
            return true;
        } else {
            return false;
        }  
    }
     
    public function oneClick($jid, $id, $vac = true) {
        if($vac) {
            $sql = "UPDATE ".DB_PREF."job_oneclick SET status = 1 WHERE job_id = ? AND user_id = ?";
        } else {
            $sql = "UPDATE ".DB_PREF."job_oneclick SET status = 2 WHERE job_id = ? AND user_id = ?";
        }
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($jid, $id))) {
            $this->applyToJob($jid, $id, false, $vac);
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }   
    
    public function sendJobToFriends($url, $user) {
        if(!$url || !$user) return false;
        
        foreach($user as $u) {
            //Email::sendTo($u, , );
            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($u, 'happinate.com - zaproszenie do pracy razem', '<p>Twój znajomy zaprasza Cię do pracy razem.</p><p>Zobacz jaką ofertę pracy Tobie poleca <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'praca/'.$url.'" target="_blank">Link do oferty!</a></p>');
        }
        return true;
    }
    

    public function checkIfAddedToFav($job) {
        
        $id = $this->getUserId();
        
        if(!$id) return false;
        
        $sql = "SELECT fav_id FROM ".DB_PREF."user_fav_job WHERE fav_job_id = ? AND fav_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($job, $id));
        if($pre->rowCount()==0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
          
    public function favJob($job) {
        
        $id = $this->getUserId();
        
        if(!$id) return false;
        
        if(!$this->checkIfAddedToFav($job, $id)) return -2;
            
        $sql = "INSERT INTO ".DB_PREF."user_fav_job (fav_user_id, fav_job_id) VALUES (?, ?)";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id, $job))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
              
    public function unfavJob($job) {
        
        $id = $this->getUserId();
        
        if(!$id) return false;
        
        if($this->checkIfAddedToFav($job, $id)) return -2;
            
        $sql = "DELETE FROM ".DB_PREF."user_fav_job WHERE fav_user_id = ? AND fav_job_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id, $job))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  
    
    public function getUserProfileName($id){
        $sql = "SELECT user_profile_name FROM ".DB_PREF."user WHERE user_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id))) {
            $res = $pre->fetch();
            return $res['user_profile_name'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
                          
    public function checkWatch($id, $uid) {
        $sql = "SELECT id FROM ".DB_PREF."user_watch WHERE user_id = ? AND watch_id = ?";
        
        $pre = $this->database->prepare($sql);
        $pre->execute(array($uid, $id));
        
        if($pre->rowCount()>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }

}