<?php

namespace src\dajOgloszenie\model;

use app\helpers\Form;

class dajOgloszenieModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }

    public function getEmployeerProfile() {
        $sql = "SELECT 
            user_profile_name, 
            user_logo,
            AES_DECRYPT(user_email, '".SALT."') as user_email, 
            AES_DECRYPT(user_nip, '".SALT."') as user_nip,
            AES_DECRYPT(user_regon, '".SALT."') as user_regon,
            AES_DECRYPT(user_pesel, '".SALT."') as user_pesel,
            AES_DECRYPT(user_phone, '".SALT."') as user_phone,
            user_adress_city,
            AES_DECRYPT(user_adress_zip_code, '".SALT."') as user_adress_zip_code,
            AES_DECRYPT(user_adress_street, '".SALT."') as user_adress_street,
            user_www,
            user_fanpage
            FROM ".DB_PREF."user ";
        if(isset($_SESSION['userToken'])) {
            $sql .= "WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
        } else {
            $sql .= "WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
        }
        $res = $this->database->prepare($sql);
        
        if(isset($_SESSION['userToken'])) {
            $res->execute(array($_SESSION['userToken']));
        } else {
            $res->execute(array($_SESSION['userTokenFB']));
        }

        if($res->rowCount()) {
            return $res->fetch();
        } else {
            return true;
        }       
    }
    
    public function getEmployeeProfile() {
        $sql = "SELECT 
            u.user_id,
            u.user_logo,
            AES_DECRYPT(u.user_email, '".SALT."') as user_email, 
            AES_DECRYPT(u.user_name, '".SALT."') as user_name,
            AES_DECRYPT(u.user_surname, '".SALT."') as user_surname,
            AES_DECRYPT(u.user_pesel, '".SALT."') as user_pesel,
            AES_DECRYPT(u.user_phone, '".SALT."') as user_phone,
            u.user_adress_city,
            u.user_date_of_birth,
            AES_DECRYPT(u.user_adress_zip_code, '".SALT."') as user_adress_zip_code,
            AES_DECRYPT(u.user_adress_street, '".SALT."') as user_adress_street,
            u.user_student,
            u.user_uncapable,
            s.*
            FROM ".DB_PREF."user as u LEFT JOIN ".DB_PREF."user_other_skill as s ON s.skill_user_id = u.user_id";
        
        if(isset($_SESSION['userTokenFB']) && !empty($_SESSION['userTokenFB'])) {
            $sql .= " WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
        } else {
            $sql .= " WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
        }
        
        $res = $this->database->prepare($sql);
        
        if(isset($_SESSION['userTokenFB']) && !empty($_SESSION['userTokenFB'])) {
            $res->execute(array($_SESSION['userTokenFB']));
        } else {
            $res->execute(array($_SESSION['userToken']));
        } 
        

        if($res->rowCount()) {
            $array = array();
            $array = $res->fetch();
            $array['skills'] = $this->getProfileSkills();
            $array['experiences'] = $this->getProfileExperience();
            $array['education'] = $this->getProfileEducation();
            return $array;
        } else {
            return true;
        }       
    }
    
    public function getProfileSkills() {
        $sql = "SELECT * FROM ".DB_PREF."user_skill WHERE skill_user_id = ?";
              
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }    
    
    public function getProfileExperience() {
        $sql = "SELECT * FROM ".DB_PREF."user_experience WHERE experience_user_id = ?";
              
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }
      
    public function getProfileEducation() {
        $sql = "SELECT * FROM ".DB_PREF."user_education WHERE education_user_id = ?";
              
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }
    
    public function checkProfileNameExists() {
        return true;
    }
    
    public function checkEmailNotExist($email) {
        $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."') AND user_id != ?;";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($email, $this->getUserId()));
        $res = $pre->fetch();

        if($pre->rowCount()!=0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function updateEmployeer($post, $file = 0) {
        $delFile = false;
        if($file>0) {
            $pathParts = pathinfo($file["name"]);
            $extension = $pathParts['extension'];

            $fileName = sha1($post['profile_email']).'.'.$extension;
            $fileUploadPath = __DIR__ . '/../../../upload/userLogo/';
            if(!move_uploaded_file($file['tmp_name'], $fileUploadPath . $fileName)) {
                $fileName = '';
            }
        } elseif($file == -1) {
            $delFile = true;
            $fileUploadPath = __DIR__ . '/../../../upload/profileLogo/';
            if(file_exists($fileUploadPath . $post['profile_file'])) {
                unlink($fileUploadPath . $post['profile_file']);
            }
        }
        
        $sql = "UPDATE ".DB_PREF."user SET 
                user_profile_name = ?, 
                user_email = AES_ENCRYPT(?, '".SALT."'), 
                user_adress_city = ?, 
                user_adress_zip_code = AES_ENCRYPT(?, '".SALT."'), 
                user_adress_street = AES_ENCRYPT(?, '".SALT."'), 
                user_nip = AES_ENCRYPT(?, '".SALT."'), 
                user_regon = AES_ENCRYPT(?, '".SALT."'), 
                user_pesel = AES_ENCRYPT(?, '".SALT."'), 
                user_phone = AES_ENCRYPT(?, '".SALT."'), 
                user_www = ?,
                user_fanpage = ?";
        if($delFile) $sql .= ", user_logo = NULL";
        if($file) $sql .= ", user_logo = '".$fileName."'";
        $sql .= " WHERE sha1(concat(user_id,user_email,user_salt)) = ?";
        
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($post['profile_name'], $post['profile_email'], $post['profile_city'], $post['profile_zip_code'], $post['profile_street'], $post['profile_nip'], $post['profile_regon'], $post['profile_pesel'], $post['profile_phone'], str_replace(array('https://','http://'),array('',''),$post['www']), str_replace(array('https://','http://'),array('',''),$post['fanpage']), $_SESSION['userToken']))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function updateTags($tags) {
        
    }
    
    public function updateEmployee($post, $file = 0, $file2 = 0) {
        $delFile = false;
        if($file>0) {
            $pathParts = pathinfo($file["name"]);
            $extension = $pathParts['extension'];

            $fileName = sha1($post['email']).'.'.$extension;
            $fileUploadPath = __DIR__ . '/../../../upload/userLogo/';
            if(!move_uploaded_file($file['tmp_name'], $fileUploadPath . $fileName)) {
                $fileName = '';
            }
        } elseif($file == -1) {
            $delFile = true;
            $fileUploadPath = __DIR__ . '/../../../upload/profileLogo/';
            if(file_exists($fileUploadPath . $post['profile_file'])) {
                unlink($fileUploadPath . $post['profile_file']);
            }
        }
        
        $sql = "UPDATE ".DB_PREF."user SET 
                user_email = AES_ENCRYPT(?, '".SALT."'), 
                user_name = AES_ENCRYPT(?, '".SALT."'), 
                user_surname = AES_ENCRYPT(?, '".SALT."'), 
                user_pesel = AES_ENCRYPT(?, '".SALT."'),
                user_date_of_birth = ?, 
                user_adress_city = ?,
                user_adress_zip_code = AES_ENCRYPT(?, '".SALT."'), 
                user_adress_street = AES_ENCRYPT(?, '".SALT."'), 
                user_phone = AES_ENCRYPT(?, '".SALT."'), 
                user_student = ?,
                user_uncapable = ?";
        if($delFile) $sql .= ", user_logo = NULL";
        if($file) $sql .= ", user_logo = '".$fileName."'";
        
        if(isset($_SESSION['userTokenFB']) && !empty($_SESSION['userTokenFB'])) {
            $sql .= " WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ?";
        } else {
            $sql .= " WHERE sha1(concat(user_id,user_email,user_salt)) = ?";
        } 
        
        // Other skills
        /*$otherSkill = array($post['driving_a'],$post['driving_a1'],$post['driving_b'],$post['driving_b1'],$post['driving_c'],$post['driving_c1'],$post['driving_d'],$post['driving_d1'],$post['driving_be'],$post['driving_c1e'],$post['driving_ce'],$post['driving_d1e'],$post['driving_de'],$post['driving_t'],$post['sanel_owner'],$post['sanel_not_expired'],$post['welding_tig'],$post['welding_mig'],$post['welding_mag'],$post['electricity_to_1kv'],$post['electricity_more_1kv'],$post['forklift'],$post['forklift_big']);
        $this->updateOtherSkills($otherSkill);
        
        // Skills
        if(isset($post['skills']) && !empty($post['skills'])) 
            $this->updateSkills($post['skills']);
        if(isset($post['newskills']) && !empty($post['newskills'])) 
            $this->updateSkills(false, $post['newskills']);           
        if(isset($post['delskills']) && !empty($post['delskills'])) 
            $this->delskills(false, false, $post['delskills']);
        
        // Experience
        if(isset($post['experience']) && !empty($post['experience'])) 
            $this->updateExperience($post['experience']); 
        if(isset($post['newexperience']) && !empty($post['newexperience']))
            $this->updateExperience(false, $post['newexperience']);     
        if(isset($post['delexperience']) && !empty($post['delexperience'])) 
            $this->updateExperience(false, false, $post['delexperience']);
         
        // Education
        if(isset($post['education']) && !empty($post['education']))
            $this->updateEducation($post['education']); 
        if(isset($post['neweducation']) && !empty($post['neweducation'])) 
            $this->updateEducation(false, $post['neweducation']);     
        if(isset($post['deleducation']) && !empty($post['deleducation'])) {
            $this->updateEducation(false, false, $post['deleducation']);
        
        }
         * 
         */
        $pre = $this->database->prepare($sql);
        
        $array = array($post['email'], $post['name'], $post['surname'], $post['pesel'], $post['date_of_birth'], $post['adress_city'], $post['adress_zip_code'], $post['adress_street'], $post['phone'], $post['student'], $post['uncapable']);
         
        if(isset($_SESSION['userTokenFB']) && !empty($_SESSION['userTokenFB'])) {
            $array[] = $_SESSION['userTokenFB'];
        } else {
            $array[] = $_SESSION['userToken'];
        }
        
        if($pre->execute($array)) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function updateSkills($uptArray = false, $insDrray = false, $delArray = false) {
        
        if($delArray) {
            $delArray = Form::sanitizeArray($delArray);
            $sql = "DELETE FROM ".DB_PREF."user_skill WHERE skill_id = ?";
            foreach($delArray as $r=>$key) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($r));
                }
            }
        }

        if($insDrray) {
            $insDrray = Form::sanitizeArray($insDrray);
            $sql = "INSERT INTO ".DB_PREF."user_skill (skill_user_id, skill_text) VALUES (?,?)";
            foreach($insDrray as $r) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($this->getUserId(), $r));
                }
            }
        }
        
        if($uptArray) {
            $uptArray = Form::sanitizeArray($uptArray);
            $sql = "UPDATE ".DB_PREF."user_skill SET skill_text = ? WHERE skill_id = ?";
            foreach($uptArray as $r=>$key) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($key, $r));
                }
            }
        }
        
    } 
        
    public function updateExperience($uptArray = false, $insDrray = false, $delArray = false) {
        
        if($delArray) {
            $delArray = Form::sanitizeArray($delArray);
            $sql = "DELETE FROM ".DB_PREF."user_experience WHERE experience_id = ?";
            foreach($delArray as $r) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($r));
                }
            }
        }

        if($insDrray) {
            $insDrray = Form::sanitizeArray($insDrray);
            
            $sql = "INSERT INTO ".DB_PREF."user_experience (experience_user_id, experience_company, experience_start, experience_end, experience_position) VALUES (?,?,?,?,?)";
            foreach($insDrray as $r) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($this->getUserId(), $r[0], $r[1], $r[2], $r[3]));
                }
            }
        }
        
        if($uptArray) {
            $uptArray = Form::sanitizeArray($uptArray);
            
            $sql = "UPDATE ".DB_PREF."user_experience SET experience_company = ?, experience_start = ?, experience_end = ?, experience_position = ? WHERE experience_id = ?";
            foreach($uptArray as $r=>$key) {
                if(!empty($key)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($key[0],$key[1],$key[2],$key[3], $r));
                }
            }
        }
        
    }        
    
    public function updateEducation($uptArray = false, $insDrray = false, $delArray = false) {
        
        if($delArray) {
            $delArray = Form::sanitizeArray($delArray);
            $sql = "DELETE FROM ".DB_PREF."user_education WHERE education_id = ?";
            foreach($delArray as $r) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($r));
                }
            }
        }

        if($insDrray) {
            $insDrray = Form::sanitizeArray($insDrray);
            
            $sql = "INSERT INTO ".DB_PREF."user_education (education_user_id, education_name, education_start, education_end, education_field) VALUES (?,?,?,?,?)";
            foreach($insDrray as $r) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($this->getUserId(), $r[0], $r[1], $r[2], $r[3]));
                }
            }
        }
        
        if($uptArray) {
            $uptArray = Form::sanitizeArray($uptArray);
            
            $sql = "UPDATE ".DB_PREF."user_education SET education_name = ?, education_start = ?, education_end = ?, education_field = ? WHERE education_id = ?";
            foreach($uptArray as $r=>$key) {
                if(!empty($key)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($key[0],$key[1],$key[2],$key[3], $r));
                }
            }
        }
        
    } 
    
    public function updateOtherSkills($array) {
        $sql = "SELECT count(skill_user_id) as count FROM ".DB_PREF."user_other_skill WHERE skill_user_id = ? LIMIT 1";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        $array = Form::sanitizeArray($array);
        $res = $pre->fetch();
        $array[] = $this->getUserId();
        if($res['count']==1) {
            $sql = "UPDATE ".DB_PREF."user_other_skill SET skill_driving_a = ?, skill_driving_a1 = ?, skill_driving_b = ?, skill_driving_b1 = ?, skill_driving_c = ?, skill_driving_c1 = ?, skill_driving_d = ?, skill_driving_d1 = ?, skill_driving_be = ?, skill_driving_c1e = ?, skill_driving_ce = ?, skill_driving_d1e = ?, skill_driving_de = ?, skill_driving_t = ?, skill_sanel_owner = ?, skill_sanel_not_expired = ?, skill_welding_tig = ?, skill_welding_mig = ?, skill_welding_mag = ?, skill_electricity_to_1kv = ?, skill_electricity_more_1kv = ?, skill_forklift = ?, skill_forklift_big = ? WHERE skill_user_id = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute($array);
        } else {
            $sql = "INSERT INTO ".DB_PREF."user_other_skill (skill_driving_a, skill_driving_a1, skill_driving_b, skill_driving_b1, skill_driving_c, skill_driving_c1, skill_driving_d, skill_driving_d1, skill_driving_be, skill_driving_c1e, skill_driving_ce, skill_driving_d1e, skill_driving_de, skill_driving_t, skill_sanel_owner, skill_sanel_not_expired, skill_welding_tig, skill_welding_mig, skill_welding_mag, skill_electricity_to_1kv, skill_electricity_more_1kv, skill_forklift, skill_forklift_big, skill_user_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            $pre = $this->database->prepare($sql);
            if($pre->execute($array)) {
                return true;
            }
        }
    }
    
    public function delete() {
        $userId = $this->getUserId();
        
        $sql = "DELETE FROM ".DB_PREF."user WHERE user_id = :id;";
        
        $pre = $this->database->prepare($sql);
        
        $pre->bindParam(':id', $userId, \PDO::PARAM_INT);
        
        if($pre->execute()) {
            return true;
        }
        $error = $this->database->errorInfo();
    }
    
    public function getContractType($name = false) {
        if($name) {
            $sql = "SELECT type_id FROM ".DB_PREF."job_contract_types WHERE type_name = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($name));
            if($pre->rowCount()!=0) {
                $n = $pre->fetch();
                return $n['type_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }            
        } else {
            $sql = "SELECT * FROM ".DB_PREF."job_contract_types";
            $pre = $this->database->prepare($sql);
            if($pre->execute()) {
                return $pre->fetchAll();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }       
        }
    }    
    
    public function getTrades($name = false) {
        if($name) {
            $sql = "SELECT trade_id FROM ".DB_PREF."job_trade_types WHERE trade_name = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($name));
            if($pre->rowCount()!=0) {
                $n = $pre->fetch();
                return $n['trade_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }            
        } else {
            $sql = "SELECT * FROM ".DB_PREF."job_trade_types";
            $pre = $this->database->prepare($sql);
            $pre->execute();
            if($pre->rowCount()!=0) {
                return $pre->fetchAll();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }     
        }
    }
    
    public function getUserId($email = false) {
        if($email) {
            $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."')";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($email));            
                
            if($pre->rowCount()!=0) {
                $id = $pre->fetch();
                return $id['user_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }              
        } else {

            if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
                $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($_SESSION['userToken']));
            } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) { 
                $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($_SESSION['userTokenFB']));            
            } else {
                return false;
            }
            if($pre->rowCount()!=0) {
                $id = $pre->fetch();
                return $id['user_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }      
        }
    }
    
    public function createURLText($string) {
        
        $string = strtolower($string);
        $polskie = array(',', ' - ',' ','ę', 'Ę', 'ó', 'Ó', 'Ą', 'ą', 'Ś', 's', 'ł', 'Ł', 'ż', 'Ż', 'Ź', 'ź', 'ć', 'Ć', 'ń', 'Ń','-',"'","/","?", '"', ":", 'ś', '!','.', '&', '&amp;', '#', ';', '[',']','domena.pl', '(', ')', '`', '%', '”', '„', '…');
        $miedzyn = array('-','-','-','e', 'e', 'o', 'o', 'a', 'a', 's', 's', 'l', 'l', 'z', 'z', 'z', 'z', 'c', 'c', 'n', 'n','-',"","","","","",'s','','', '', '', '', '', '', '', '', '', '', '', '', '');
        $string = str_replace($polskie, $miedzyn, $string);
        
        $string = preg_replace('/[^0-9a-z\-]+/', '', $string);
        $string = preg_replace('/[\-]+/', '-', $string);
        $string = trim($string, '-');

        $string = stripslashes($string);

        $string = urlencode($string);
        
        return $string;

    }
 
    public function register($email, $pass, $type = 1, $contact_name = false, $contact_phone = false, $street = false, $city = false) {
        
        $sql = "INSERT INTO ".DB_PREF."user (user_profile_name, user_phone, user_adress_street, user_adress_city, user_email, user_password, user_type, user_register_date, user_register_token) VALUES (?, AES_ENCRYPT(?, '".SALT."'), AES_ENCRYPT(?, '".SALT."'), ?, AES_ENCRYPT(?, '".SALT."'), AES_ENCRYPT(md5(?), '".SALT."'), ?, ?, ?);";
        $pre = $this->database->prepare($sql);
        $registerToken =  sha1($email.SALT);
        if($pre->execute(array($contact_name, $contact_phone, $street, $city, $email, $pass, $type, date("Y-m-d H:i:s"), $registerToken))) {
            $uid = $this->database->lastInsertId();

            $title = 'happinate.com - witamy!';
            $body = '<h3>Witamy w happinate.com!</h3>';
            $body .= '<p>Cieszymy się, że dołączyłeś do Nas!</p>';
            $body .= '<p>Kliknij w <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'rejestracja/potwierdzenie/'.$registerToken.'" target="_blank">link</a> i aktywuj swoje konto.</p>';
            $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz</p>';
            
            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($email, $title, $body);
            
            return $uid;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }

    private function getUserLogo() {
        $sql = "SELECT user_logo FROM ".DB_PREF."user WHERE user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));            

        if($pre->rowCount()!=0) {
            $id = $pre->fetch();
            return $id['user_logo'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }
    
    public function addJob($post, $control = false, $free = false, $wyroznienie = false) {

        if(isset($post['logo']) && $post['logo'] && $post['logo']!='user') {
            $tmpFile = __DIR__ . '/../../../upload/tmp/' . $post['logo'];
            $file = __DIR__ . '/../../../upload/jobLogo/' . $post['logo'];
            if(file_exists($tmpFile)) {
                if(copy($tmpFile,$file)) {
                    unlink($tmpFile);
                }
            } else {
                $post['logo'] = NULL;
            }
        } elseif(isset($post['logo']) && $post['logo']=='user') {
            $post['logo'] = $this->getUserLogo();
            if($post['logo']) {
                $tmpFile = __DIR__ . '/../../../upload/userLogo/' . $post['logo'];
                $file = __DIR__ . '/../../../upload/jobLogo/' . $post['logo'];
                if(file_exists($tmpFile)) {
                    copy($tmpFile,$file);
                } else {
                    $post['logo'] = NULL;
                }
            } else {
                $post['logo'] = NULL;
            }
        } else {
            $post['logo'] = NULL;
        }
        
        if($control) {
            $sql = "INSERT INTO ".DB_PREF."job (job_employeer_name, job_added_by, job_contract_type_id, job_trade_type_id, job_start_date, job_time, job_work_hours_per_week, job_work_shifts_per_day, job_logo, job_www, job_fanpage, job_publish_date, job_publish_date_end, job_position, job_url_text, job_rate_min, job_rate_max, job_volunteering, job_hide_salary, job_additional_salary_info, job_employeer_place_direct, job_employeer_place_direct_address, job_employeer_place_direct_city, job_employeer_place_area, job_employeer_place_telework, job_employeer_place_without, job_main_page, job_status, job_city_id, job_created_date, job_vacancies, job_control, job_lat, job_long, job_need, job_duty, job_offer, job_province_id, job_additional_oneclick_info) 
                    VALUES (:employeer_name, :id, :contract_type, :trade_type, :start_date, :time, :hours_per_week, :shifts, :logo, :www, :fanpage, :publish_date, :publish_date_end, :position, :url, :rate_min, :rate_max, :volunteering, :hide_salary, :additional_salary_info, :employeer_place_direct, :street, :city, :employeer_place_area, :employeer_place_telework, :employeer_place_without, :wyroznienie, 0, :city_id, NOW(), :vacancies, :control, :lat, :long, :need, :duty, :offer, :province, :additional_oneclick_info);";
        } elseif($free) {
            $sql = "INSERT INTO ".DB_PREF."job (job_employeer_name, job_added_by, job_contract_type_id, job_trade_type_id, job_start_date, job_time, job_work_hours_per_week, job_work_shifts_per_day, job_logo, job_www, job_fanpage, job_publish_date, job_publish_date_end, job_position, job_url_text, job_rate_min, job_rate_max, job_volunteering, job_hide_salary, job_additional_salary_info, job_employeer_place_direct, job_employeer_place_direct_address, job_employeer_place_direct_city, job_employeer_place_area, job_employeer_place_telework, job_employeer_place_without, job_main_page, job_status, job_city_id, job_created_date, job_vacancies, job_control, job_lat, job_long, job_need, job_duty, job_offer, job_province_id, job_additional_oneclick_info) 
                    VALUES (:employeer_name, :id, :contract_type, :trade_type, :start_date, :time, :hours_per_week, :shifts, :logo, :www, :fanpage, :publish_date, :publish_date_end, :position, :url, :rate_min, :rate_max, :volunteering, :hide_salary, :additional_salary_info, :employeer_place_direct, :street, :city, :employeer_place_area, :employeer_place_telework, :employeer_place_without, 0, 1, :city_id, NOW(), :vacancies, :control, :lat, :long, :need, :duty, :offer, :province, :additional_oneclick_info);";            
        }

        $pre = $this->database->prepare($sql);
        
        $null = null;
               
        #echo"<pre>";print_r($post);echo"</pre>";
        #die();
        # id

        if(isset($post['userEmail'])) {
            $uid = $this->getUserId($post['userEmail']);
            $pre->bindValue(':id', $uid, \PDO::PARAM_INT);
        } else {
            #die();
            if(isset($post['email'])) {
                $uid = $this->register($post['email'], $post['pass'], 2, $post['contact_name'], $post['contact_phone'], $post['street'], $post['city']);
                $pre->bindValue(':id', $uid, \PDO::PARAM_INT);
            } else {
                $pre->bindValue(':id', $this->getUserId(), \PDO::PARAM_INT);
            }
        }
        
        # contract_type
        if(!empty($post['contract'])) {
            $contract_type_id = $this->getContractType($post['contract']);
            $pre->bindValue(':contract_type', $contract_type_id, \PDO::PARAM_INT);
        } else {
            $pre->bindValue(':contract_type', $null, \PDO::PARAM_NULL);
        }        

        # city_id
        if($city_id = $this->getCityId($post['city'])) {
            $pre->bindValue(':city_id', $city_id, \PDO::PARAM_INT);
        } else {
            $pre->bindValue(':city_id', $null, \PDO::PARAM_NULL);
        }
        
        # trade_type
        if(!empty($post['trade'])) {
            $trade_type_id = $this->getTags($post['trade']);
            $pre->bindValue(':trade_type', $trade_type_id, \PDO::PARAM_INT);
        } else {
            $pre->bindValue(':trade_type', $null, \PDO::PARAM_NULL);
        }
        
        # start_date
        $pre->bindValue(':start_date', $post['start_date'], \PDO::PARAM_STR);
                
        # time
        $pre->bindValue(':time', $post['time'], \PDO::PARAM_STR);
        
        # hours_per_week
        $pre->bindValue(':hours_per_week', $post['hours_per_week'], \PDO::PARAM_INT);
                
        # shifts
        $pre->bindValue(':shifts', $post['shifts'], \PDO::PARAM_STR);

        # logo
        $pre->bindValue(':logo', $post['logo'], \PDO::PARAM_STR);
        
        # www
        $pre->bindValue(':www', $post['www'], \PDO::PARAM_STR);
        
        # fanpage
        $pre->bindValue(':fanpage', $post['fanpage'], \PDO::PARAM_STR);
        
        # publish_date
        $publishDate = date("Y-m-d H:i:s");
        $pre->bindValue(':publish_date', $publishDate, \PDO::PARAM_STR);

        # vacancies
        $pre->bindValue(':vacancies', $post['vacancies'], \PDO::PARAM_INT);
        #die($post['vacancies']);
        
        # publish_date_end
        if($post['formula']==3) {
            $dateTmp = strtotime($publishDate);
            $date = strtotime("+14 day", $dateTmp);
            $endDate = date('Y-m-d H:i:s', $date);
        } elseif($post['formula']==2) {
            $dateTmp = strtotime($publishDate);
            $date = strtotime("+7 day", $dateTmp);
            $endDate = date('Y-m-d H:i:s', $date);
        } elseif($post['formula']==1) {
            $dateTmp = strtotime($publishDate);
            $date = strtotime("+14 day", $dateTmp);
            $endDate = date('Y-m-d H:i:s', $date);
        }  
        $pre->bindValue(':publish_date_end', $endDate, \PDO::PARAM_STR);
        
        # position
        $pre->bindValue(':position', $post['position'], \PDO::PARAM_STR);
        
        # url
        $url = $this->createURLText($post['position']);
        $pre->bindValue(':url', $url, \PDO::PARAM_STR);
        
        if($post['paymant']=='stawka godzinowa') {
            
            if($post['rate']) {
                # rate_min
                $pre->bindValue(':rate_min', $post['rate'], \PDO::PARAM_INT);

                # rate_max
                $pre->bindValue(':rate_max', $post['rate'], \PDO::PARAM_INT);
            } else {
                 # rate_min
                $pre->bindValue(':rate_min', $post['rate_min'], \PDO::PARAM_INT);

                # rate_max
                $pre->bindValue(':rate_max', $post['rate_max'], \PDO::PARAM_INT);
            }
            
        } elseif($post['paymant']=='stawka miesięczna') {
            
            $rate = round($post['rateMonth']/168, 2, PHP_ROUND_HALF_DOWN);
            echo $rate;
            # rate_min
            $pre->bindValue(':rate_min', $rate, \PDO::PARAM_INT);

            # rate_max
            $pre->bindValue(':rate_max', $rate, \PDO::PARAM_INT);
        } else {
            # rate_min
            $pre->bindValue(':rate_min', $post['rate_min'], \PDO::PARAM_INT);

            # rate_max
            $pre->bindValue(':rate_max', $post['rate_max'], \PDO::PARAM_INT);
        }
        
        # volunteering
        if($post['paymant']=='wolontariat, staż / praktyki bezpłatne') {
            $volunteering = true;
        } else {
            $volunteering = false;
        }
        $pre->bindValue(':volunteering', $volunteering, \PDO::PARAM_INT);
        
        # hide_salary
        if($post['paymant']=='publikuj bez podawania stawki') {
            $hide_salary = true;
        } else {
            $hide_salary = false;
        }
        $pre->bindValue(':hide_salary', $hide_salary, \PDO::PARAM_INT);
             
        # additional_salary_info
        $pre->bindValue(':additional_salary_info', $post['additional_salary_info'], \PDO::PARAM_STR);

        # employeer_place_direct 
        if(empty($post['place'])) {
            $employeer_place_direct = true;
        } else {
            $employeer_place_direct = false;
        }
        $pre->bindValue(':employeer_place_direct', $employeer_place_direct, \PDO::PARAM_INT);
        
        # street
        $pre->bindValue(':street', $post['street'], \PDO::PARAM_STR);
                          
        # city
        $pre->bindValue(':city', $post['city'], \PDO::PARAM_STR);
        
        if(isset($post['employeer_name'])) {
            $pre->bindValue(':employeer_name', $post['employeer_name'], \PDO::PARAM_STR);
        } else {
            $pre->bindValue(':employeer_name', $null, \PDO::PARAM_NULL);
        }
        
        # employeer_place_area 
        if($post['place']=='praca w terenie') {
            $employeer_place_area = true;
        } else {
            $employeer_place_area = false;
        }
        $pre->bindValue(':employeer_place_area', $employeer_place_area, \PDO::PARAM_INT);
                   
        # employeer_place_telework 
        if($post['place']=='praca zdalna') {
            $employeer_place_telework = true;
        } else {
            $employeer_place_telework = false;
        }
        $pre->bindValue(':employeer_place_telework', $employeer_place_telework, \PDO::PARAM_INT);
                   
        # employeer_place_without 
        if($post['place']=='bez podawania adresu') {
            $employeer_place_without = true;
        } else {
            $employeer_place_without = false;
        }
        $pre->bindValue(':employeer_place_without', $employeer_place_without, \PDO::PARAM_INT);
     
        # control
        if($control) {
            $pre->bindValue(':wyroznienie', $wyroznienie, \PDO::PARAM_INT);
            $pre->bindValue(':control', $control, \PDO::PARAM_STR);
        } else {
            $pre->bindValue(':control', $null, \PDO::PARAM_NULL);
        }
        
        # lat & long
        $latlong = explode(', ', str_replace(')', '', str_replace('(', '', $post['coords'])));
        if(count($latlong)==2) {
            $pre->bindValue(':lat', $latlong[0], \PDO::PARAM_STR);
            $pre->bindValue(':long', $latlong[1], \PDO::PARAM_STR);
        } else {
            $pre->bindValue(':lat', $null, \PDO::PARAM_NULL);
            $pre->bindValue(':long', $null, \PDO::PARAM_NULL);
        }
     
        # contact_name
        $pre->bindValue(':need', $post['need'], \PDO::PARAM_STR);
        
        # contact_email
        $pre->bindValue(':duty', $post['duty'], \PDO::PARAM_STR);
                        
        # contact_email
        $pre->bindValue(':offer', $post['offer'], \PDO::PARAM_STR);
        
        # province
        $province = $this->getProvinces($post['province']);
        $pre->bindValue(':province', $province, \PDO::PARAM_INT);
        
        # province
        if(isset($post['oneclick_info'])) {
            $pre->bindValue(':additional_oneclick_info', $post['oneclick_info'], \PDO::PARAM_STR);
        } else {
            $pre->bindValue(':additional_oneclick_info', $null, \PDO::PARAM_NULL);
        }
        
        if($pre->execute()) {
            $lastId = $this->database->lastInsertId();
            $this->insertJobTags($lastId, $post['tags']);
            
            // if($post['trade']) {
            //     $trade_type_id = $this->getTags($post['trade']);
            //     $userTradeObserators = $this->getTradeObservators($trade_type_id);
            //     $url = 'http://happinate.com/praca/' . $lastId. '/' . $this->createURLText($post['position']);
            //     $jobOfferTitle = $post['position'];
            //     if ($userTradeObserators) {
            //         $title = 'Nowa oferta pracy';
            //         $body = '<h3>Nowa oferta pracy</h3>';
            //         $body.= '<p class="lead">Twoja kategoria główna to '.$post['trade'].'.</p>';
            //         $body.= '<p>Właśnie pojawiła się nowa oferta pracy:</p>';
            //         $body.= '<p style="padding:10px 0 20px;"><a href="'.$url.'" target="_blank">'.$jobOfferTitle.'</a></p>';
            //         $body.= '<p>Pozdrawiamy,<br>Zespół Happinate</p>';
            //         foreach ($userTradeObserators as $user) {
            //             $_emailClass = $GLOBALS['_emailClass'];
            //             $_emailClass->addToList($user['user_email'], $title, $body, 'new.html');
            //         }
            //     }
            // }

            // kalendarz rekrutacji
            if(MOD_CALENDAR) {
                if(isset($post['calendar']) && is_array($post['calendar']) && !empty($post['calendar']['start']) && !empty($post['calendar']['stop'])) {
                    $_d = array();
                    $_d['jid'] = $lastId;
                    $_d['uid'] = $this->getUserId($post['userEmail']);
                    $_d['calendar'] = $post['calendar'];
                    $this->insertCalendarData($_d);
                }
            }
        
            if(isset($post['oneclickAll']) && $post['oneclickAll']) {
                $this->oneClickStart($lastId, $post['oneclick_info'], false, $post['tags']);
            } elseif(isset($post['oneclick']) && !empty($post['oneclick'])) {
                $this->oneClickStart($lastId, $post['oneclick_info'], $post['oneclick']);
            }
            
            $_SESSION['basket'] = NULL;
            
            return $lastId;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }  


    /**
     * zapis danych do klanedarze zlecenia
     * @param  array $data id ogloszenia i dane do kalendarza 
     * @return boolena  true/false
     */
    private function insertCalendarData($data)
    {
        if(empty($data['calendar']) OR empty($data['uid']))
        {
            return false;
        }

        $sql = "INSERT INTO happinate_job_calendar(job_id, user_id, start, stop, exludeSunSut, hoursFrom, hoursTo, candidatePerHour)
                VALUES(:job_id, :user_id, :start, :stop, :exludeSunSut, :hoursFrom, :hoursTo, :candidatePerHour);";

        $pre = $this->database->prepare($sql);
  
        $start = !empty($data['calendar']['start']) ? $data['calendar']['start'] : '';
        $stop = !empty($data['calendar']['stop']) ? $data['calendar']['stop'] : '';
        $exludeSunSut = !empty($data['calendar']['exludeSunSut']) ? $data['calendar']['exludeSunSut'] : '0';
        $hoursFrom = !empty($data['calendar']['hoursFrom']) ? $data['calendar']['hoursFrom'] : '7';
        $hoursTo = !empty($data['calendar']['hoursTo']) ? $data['calendar']['hoursTo'] : '16';
        $candidatePerHour = !empty($data['calendar']['candidatePerHour']) ? $data['calendar']['candidatePerHour'] : '1';
        
        $pre->bindValue(':job_id', $data['jid']);
        $pre->bindValue(':user_id', $data['uid']);        
        $pre->bindValue(':start', $start);
        $pre->bindValue(':stop', $stop);
        $pre->bindValue(':exludeSunSut', $exludeSunSut);
        $pre->bindValue(':hoursFrom', $hoursFrom);
        $pre->bindValue(':hoursTo', $hoursTo);
        $pre->bindValue(':candidatePerHour', $candidatePerHour);

        if($pre->execute()) 
        {
            return true;
        }
        return false;
    }


    
    private function oneClickStart($jid, $info, $array = false, $tags = false) {
        if($array) {
            foreach($array as $element) {
                $elementArray = array('user_id'=>$element, 'user_email'=>$this->getUserEmail($element));
                $this->addOneClick($jid, $elementArray, $info);
            }   
        } else {
            $list = $this->getOneClickUsersByTags($tags);
            
            #echo "<pre>"; print_r($list); echo "</pre>"; die();
            
            foreach($list as $element) {
                $elementArray = array('user_id'=>$element['user_id'], 'user_email'=>$element['user_email']);
                $this->addOneClick($jid, $elementArray, $info);
            }
        }
    }
    
    private function addOneClick($jid, $element, $info) {
        
        $sql = "INSERT INTO ".DB_PREF."job_oneclick (job_id, user_id) VALUES (?, ?)";
        $pre = $this->database->prepare($sql);
        
        #echo "<pre>"; print_r($element); echo "</pre>"; die();
        if($pre->execute(array($jid, $element['user_id']))) {
            $this->pushToApp($jid, $element['user_id']);
            
            $body = '<h3>Gratulujemy!</h3><p>Pracodawca zaoferował Tobie pracę!</p>';
            $body .= '<p><a href="http://'.$_SERVER['SERVER_NAME'].HOST.'praca/oneClick/'.$jid.'">Zobacz ofertę pracy</a></p>';
            if($info) {
                $body .= '<p>Dodatkowe informacje od pracodawcy:</p>';
                $body .= '<p style="padding: 10px; border: #ccc 1px solid;">'.$info.'</p>';
            }
            $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz</p>';
            $title = 'happinate.com – oferta pracy 1-click';

            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($element['user_email'], $title, $body);
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
         
    }
    
    private function pushToApp($job, $user) {
        $sql = "SELECT user_push FROM ".DB_PREF."user WHERE user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($user));
        $res = $pre->fetch();

        if($res['user_push']) {
            $regIds = array();
	
            array_push($regIds, $res['user_push']);
            
            // Set POST variables
            $url = 'https://android.googleapis.com/gcm/send';
            
            $message = array(
                "notification" => false, 
                "message" => 'Nowe powiadomienie od pracodawców!' , 
                "title"=>'happinate!', 
                "offerId"=>$job, 
                "type"=>1
            );
            
            $fields = array(
                'registration_ids' => $regIds,
                'data' => $message,
            );
            
            $headers = array(
                'Authorization: key=' . GOOGLE_API_KEY,
                'Content-Type: application/json'
            );
            
            
            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Execute post
            $result = curl_exec($ch);
            if ($result === FALSE) {
                //die('Curl failed: ' . curl_error($ch));
            }

            // Close connection
            curl_close($ch);

            return true;
        } else {
            return false;
        }
        
    }
    
    public function insertJobTags($jid, $tags) {
        foreach($tags as $tag) {
            $sql = "INSERT INTO ".DB_PREF."tag_job (job_id, tag_id) VALUES (?, ?)";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($jid, $tag));
        }
    }
    
    public function rabat($rabat) {
        $sql = "SELECT rabat_used FROM ".DB_PREF."job_rabat WHERE rabat_code = AES_ENCRYPT(?, '".SALT."')";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($rabat));
        
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            $rabatUsed = $res['rabat_used']+1;

            $sql2 = "UPDATE ".DB_PREF."job_rabat SET rabat_used = ".$rabatUsed." WHERE rabat_code = AES_ENCRYPT(?, '".SALT."')";
            $pre = $this->database->prepare($sql2);

            if($pre->execute(array($rabat))) {
                return true;
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            }
        } else {
            return false;
        }

    }
    
    public function paidResumeJob($id, $post, $control = false) {

        if($control) {
            $sql = "UPDATE ".DB_PREF."job SET job_publish_date = ?, job_publish_date_end = ?, job_main_page = ?, job_control = ?, job_status = 3 WHERE job_id = ?;";
        } else {
            $sql = "UPDATE ".DB_PREF."job SET job_publish_date = ?, job_publish_date_end = ?, job_main_page = ?, job_status = 3 WHERE job_id = ?";         
        }
        
        if($post['formula']==2) {
            $dateTmp = strtotime(date("Y-m-d H:i:s"));
            $date = strtotime("+14 day", $dateTmp);
            $endDate = date('Y-m-d H:i:s', $date);
        } else {
            $dateTmp = strtotime(date("Y-m-d H:i:s"));
            $date = strtotime("+3 day", $dateTmp);
            $endDate = date('Y-m-d H:i:s', $date);
        }
        
        if($post['max']) {
            $job_main_page = 1;
        } else {
            $job_main_page = 0;
        }
        
        # execute array
        $executeArrayTmp = array(
            date("Y-m-d H:i:s"),
            $endDate, 
            $job_main_page
        );
        
        if($control) {
            $executeArrayTmp[] = $control;
        }
        
        $executeArrayTmp[] = $id;
        
        $executeArray = Form::sanitizeArray($executeArrayTmp);
        
        #echo "<pre>"; print_r($executeArray); echo "</pre>";
        
        $pre = $this->database->prepare($sql);
        
        if($pre->execute($executeArray)) {
            $_SESSION['resume'] = null;
            return true;

        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }   
    
    public function addDutiesToJob($id, $array) {
        $array = Form::sanitizeArray($array);
        $sql = "INSERT INTO ".DB_PREF."job_duty (duty_job_id, duty_text) VALUES (?,?)";
        foreach($array as $r) {
            $pre = $this->database->prepare($sql);
            $pre->execute(array($id, $r));
        }
    }
        
    public function addNeedsToJob($id, $array) {
        $array = Form::sanitizeArray($array);
        $sql = "INSERT INTO ".DB_PREF."job_need (need_job_id, need_text) VALUES (?,?)";
        foreach($array as $r) {
            $pre = $this->database->prepare($sql);
            $pre->execute(array($id, $r));
        }
    }
    
    public function checkRabat($rabat) {
        $sql = "SELECT rabat_id FROM ".DB_PREF."job_rabat WHERE rabat_code = AES_ENCRYPT(?, '".SALT."') AND rabat_used < rabat_count;";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($rabat));
        if($pre->rowCount()!=0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    
    public function getRabatPercent($rabat) {
        $sql = "SELECT rabat_percent FROM ".DB_PREF."job_rabat WHERE rabat_code = AES_ENCRYPT(?, '".SALT."');";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($rabat))) {
            $r = $pre->fetch();
            return $r['rabat_percent'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
        
    public function getRabatId($rabat) {
        $sql = "SELECT rabat_id FROM ".DB_PREF."job_rabat WHERE rabat_code = AES_ENCRYPT(?, '".SALT."');";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($rabat))) {
            $r = $pre->fetch();
            return $r['rabat_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return null;
        }
    }
    
    public function getMyJobs() {
        $sql = "SELECT job_url_text, job_position, job_status, job_publish_date_end, job_id, job_views FROM ".DB_PREF."job WHERE job_status != 0 AND job_added_by = ? AND job_delete = 0;";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($this->getUserId()))) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }       
    }
    
    public function checkUserRightToEdit($id) {
        $sql = "SELECT count(job_id) as count FROM ".DB_PREF."job WHERE job_added_by = ? AND job_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId(), $id));
        $count = $pre->fetch();
        if($count['count']==1) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }   
    }
    
    public function pauseJob($id) {
        $sql = "UPDATE ".DB_PREF."job SET job_status = 2 WHERE job_id = ?";
        $pre = $this->database->prepare($sql);
        if($pre->execute(array($id))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }    
    
    public function resumeJob($id) {
        $sql = "UPDATE ".DB_PREF."job SET job_status = 1 WHERE job_id = ?";
        $pre = $this->database->prepare($sql);
        if($pre->execute(array($id))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }
    
    public function deleteJob($id) {
        $sql = "UPDATE ".DB_PREF."job SET job_delete = 1 WHERE job_id = ?";
        $pre = $this->database->prepare($sql);
        if($pre->execute(array($id))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            print_r($pdoError);
            return false;
        }        
    }

    public function getJob($id) {
        $sql = "SELECT j.*, c.type_name as job_contract_type, t.trade_name as job_trade_type FROM ".DB_PREF."job as j LEFT JOIN ".DB_PREF."job_contract_types as c ON c.type_id = j.job_contract_type_id LEFT JOIN ".DB_PREF."job_trade_types as t ON t.trade_id = j.job_trade_type_id WHERE j.job_id = ? AND job_delete = 0 AND job_status>0 LIMIT 1";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        if($pre->rowCount()!=0) {
            $jobList = $pre->fetch();
            $jobList['duties'] = $this->getDuties($jobList['job_id']);
            $jobList['needs'] = $this->getNeeds($jobList['job_id']);
            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }
      
    public function getJobName($id) {
        $sql = "SELECT job_position FROM ".DB_PREF."job WHERE job_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        
        if($job = $pre->fetch()) {
            return $job['job_position'];
            #echo 'd';
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }
         
    public function getDuties($id) {
        $sql = "SELECT * FROM ".DB_PREF."job_duty WHERE duty_job_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }     
    }
         
    public function getNeeds($id) {
        $sql = "SELECT * FROM ".DB_PREF."job_need WHERE need_job_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($id));
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }      
    }    
    
    public function updateJob($id, $post, $file = 0) {

        $fileName = '';
        if($file>0) {
            $pathParts = pathinfo($file["name"]);
            $extension = $pathParts['extension'];

            $fileName = sha1(time()).'.'.$extension;
            $fileUploadPath = __DIR__ . '/../../../upload/jobLogo/';
            if(!move_uploaded_file($file['tmp_name'], $fileUploadPath . $fileName)) {
                $fileName = '';
            } else {
                $imgInfo = getimagesize($fileUploadPath . $fileName);
                if($imgInfo[0]>200 || $imgInfo[1]>100) {
                    require __DIR__ . '/../../../app/libs/imageresizer/SimpleImage.php';
                    $SimpleImage = new \SimpleImage();
                    if($imgInfo[0]>$imgInfo[1]) {
                        $SimpleImage->load($fileUploadPath . $fileName); 
                        $SimpleImage->resizeToWidth(200); 
                        $SimpleImage->save($fileUploadPath . $fileName);
                    } else {
                        $SimpleImage->load($fileUploadPath . $fileName); 
                        $SimpleImage->resizeToHeight(100); 
                        $SimpleImage->save($fileUploadPath . $fileName);                         
                    }
                }
            }
        } elseif($file == -1) {
            $fileUploadPath = __DIR__ . '/../../../upload/jobLogo/';
            if(file_exists($fileUploadPath . $post['logo'])) {
                unlink($fileUploadPath . $post['logo']);
            }
        }
        
        $sql = "UPDATE ".DB_PREF."job SET job_start_date = ?, job_time = ?, job_work_hours_per_week = ?, job_work_shifts_per_day = ?, job_logo = ?, job_www = ?, job_fanpage = ?, job_url_text = ?, job_rate_min = ?, job_rate_max = ?, job_volunteering = ?, job_hide_salary = ?, job_additional_salary_info = ?, job_employeer_place_direct = ?, job_employeer_place_direct_address = ?, job_employeer_place_direct_city = ?, job_employeer_place_direct_zip_code = ?, job_employeer_place_direct_place_tips = ?, job_employeer_place_area = ?, job_employeer_place_area_tips = ?,  	job_employeer_place_telework = ?,  	job_employeer_place_without = ?, job_contact_name = ?, job_contact_email = ?, job_contact_phone = ?, job_vacancies = ? WHERE job_id = ? ";
        
        if($post['employeer_place_direct']=='on') {
            $employeer_place_direct = true;
        } else {
            $employeer_place_direct = false;
        }
        
        if($post['employeer_place_area']=='on') {
            $employeer_place_area = true;
        } else {
            $employeer_place_area = false;
        }
        
        if($post['employeer_place_telework']=='on') {
            $employeer_place_telework = true;
        } else {
            $employeer_place_telework = false;
        }
         
        if($post['employeer_place_without']=='on') {
            $employeer_place_without = true;
        } else {
            $employeer_place_without = false;
        }

        if($post['hide_salary']=='on') {
            $hide_salary = true;
        } else {
            $hide_salary = false;
        }
         
        if($post['volunteering']=='on') {
            $volunteering = true;
        } else {
            $volunteering = false;
        }

        # execute array
        $executeArrayTmp = array(    
            $post['start_date'],
            $post['time'], 
            $post['work_hours_per_week'], 
            $post['work_shifts_per_day'], 
            $fileName,
            $post['www'], 
            $post['fanpage'], 
            $this->createURLText($post['position']),
            $post['rate_min'], 
            $post['rate_max'], 
            $volunteering, 
            $hide_salary, 
            $post['additional_salary_info'], 
            $employeer_place_direct, 
            $post['employeer_place_direct_address'], 
            $post['employeer_place_direct_city'], 
            $post['employeer_place_direct_zip_code'], 
            $post['employeer_place_direct_place_tips'], 
            $employeer_place_area, 
            $post['employeer_place_area_tips'], 
            $employeer_place_telework, 
            $employeer_place_without, 
            $post['contact_name'], 
            $post['contact_email'], 
            $post['contact_phone'],
            $post['vacancies'],
            $id
        );
               
        $executeArray = Form::sanitizeArray($executeArrayTmp);
        
        #echo "<pre>"; print_r($executeArray); echo "</pre>";
        #
        // Needs
        if(isset($post['needs']) && !empty($post['needs'])) 
            $this->updateNeeds($id, $post['needs']);
        if(isset($post['newneeds']) && !empty($post['newneeds'])) 
            $this->updateNeeds($id, false, $post['newneeds']);           
        if(isset($post['delneeds']) && !empty($post['delneeds'])) 
            $this->updateNeeds($id, false, false, $post['delneeds']);
        
        // Duties
        if(isset($post['duties']) && !empty($post['duties'])) 
            $this->updateDuties($id, $post['duties']); 
        if(isset($post['newduties']) && !empty($post['newduties']))
            $this->updateDuties($id, false, $post['newduties']);     
        if(isset($post['delduties']) && !empty($post['delduties'])) 
            $this->updateDuties($id, false, false, $post['delduties']);
                
        $pre = $this->database->prepare($sql);
        
        if($pre->execute($executeArray)) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    
    public function updateNeeds($id, $uptArray = false, $insDrray = false, $delArray = false) {
        
        if($delArray) {
            $delArray = Form::sanitizeArray($delArray);
            $sql = "DELETE FROM ".DB_PREF."job_need WHERE need_id = ?";
            foreach($delArray as $r=>$key) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($r));
                }
            }
        }

        if($insDrray) {
            $insDrray = Form::sanitizeArray($insDrray);
            $sql = "INSERT INTO ".DB_PREF."job_need (need_job_id, need_text) VALUES (?,?)";
            foreach($insDrray as $r) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($id, $r));
                }
            }
        }
        
        if($uptArray) {
            $uptArray = Form::sanitizeArray($uptArray);
            $sql = "UPDATE ".DB_PREF."job_need SET need_text = ? WHERE need_id = ?";
            foreach($uptArray as $r=>$key) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($key, $r));
                }
            }
        }
        
    }     
    
    public function updateDuties($id, $uptArray = false, $insDrray = false, $delArray = false) {
        
        if($delArray) {
            $delArray = Form::sanitizeArray($delArray);
            $sql = "DELETE FROM ".DB_PREF."job_duty WHERE duty_id = ?";
            foreach($delArray as $r=>$key) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($r));
                }
            }
        }

        if($insDrray) {
            $insDrray = Form::sanitizeArray($insDrray);
            $sql = "INSERT INTO ".DB_PREF."job_duty (duty_job_id, duty_text) VALUES (?,?)";
            foreach($insDrray as $r) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($id, $r));
                }
            }
        }
        
        if($uptArray) {
            $uptArray = Form::sanitizeArray($uptArray);
            $sql = "UPDATE ".DB_PREF."job_duty SET duty_text = ? WHERE duty_id = ?";
            foreach($uptArray as $r=>$key) {
                if(!empty($r)) {
                    $pre = $this->database->prepare($sql);
                    $pre->execute(array($key, $r));
                }
            }
        }
        
    } 

    public function getDotPayToken($formula, $max = 0, $rabatText = 0) {
        /*
        if($rabatText) {
            $rabat = $this->getRabatPercent($rabatText);
        } else {
            $rabat = 0;
        }
        */
        // Formuła MINI 
        if($formula==2) {
            return 'ZV3NQR51647D8699Z6ZE6IWEG5RJRMT3';
        }
        
        // Formuła HAPPI 
        if($formula==3) {
            return '4M3RDJJCJ7HBYPSHT1LZSG5A5U8AWUSR';
        }
        /*
        // Formuła MINI + MAX 
        if($formula==1 && $max==1 && $rabat==0) {
            return 'JBLIMQ7RPWN9RSIJSRNQDI4H3WJM4IV9';
        } 
        
        // Formuła HAPPI + MAX 
        if($formula==2 && $max==1 && $rabat==0) {
            return 'WXX51G4U5WAU9U35T5WPMSJ1YTN65R2T';
        } 
          
        // Formuła MINI - 10%
        if($formula==1 && $max==0 && $rabat==10) {
            return '174FSI8VRD7VXGZEH9WW68UP8J425T7E';
        } 
          
        // Formuła MINI -25%
        if($formula==1 && $max==0 && $rabat==25) {
            return '2IYDZU3RA7CW7CQU57E62YYPBBHAGPRD';
        } 
          
        // Formuła MINI -50%
        if($formula==1 && $max==0 && $rabat==50) {
            return '2LTNWPY2JIPJ7E1C2A17V8GPA3UMSQL2';
        } 
         
        // Formuła HAPPI - 10%
        if($formula==2 && $max==0 && $rabat==10) {
            return 'A5GJ3F9ZYQX43U47FTQ9VVY7TT93WJI9';
        } 
          
        // Formuła HAPPI -25%
        if($formula==2 && $max==0 && $rabat==25) {
            return 'YUCW6UUW8MUBLRPBEPQI5U6KS2KAS2Y2';
        } 
          
        // Formuła HAPPI -50%
        if($formula==2 && $max==0 && $rabat==50) {
            return 'UI6H7N6PWM6QX64XDY52HD5IDI1K81H8';
        }  
        
        // Formuła MINI + MAX - 10%
        if($formula==1 && $max==1 && $rabat==10) {
            return 'V46ZA5CJ8G7LMSZ9V5VD4W9S2UL1P6AC';
        } 
          
        // Formuła MINI + MAX -25%
        if($formula==1 && $max==1 && $rabat==25) {
            return '	FDYSV2AJJ3325X66Y76Q4MJJ3HSYKTX9';
        } 
          
        // Formuła MINI + MAX -50%
        if($formula==1 && $max==1 && $rabat==50) {
            return '4M3RDJJCJ7HBYPSHT1LZSG5A5U8AWUSR';
        }      
        
        // Formuła HAPPI + MAX - 10%
        if($formula==2 && $max==1 && $rabat==10) {
            return '1FPWHY9F9319R6XZNA9A117TMA66JSMZ';
        } 
          
        // Formuła HAPPI + MAX -25%
        if($formula==2 && $max==1 && $rabat==25) {
            return 'JKGX7JP48B9ZYQT632ZY6UNFEMTPMW7G';
        } 
          
        // Formuła HAPPI + MAX -50%
        if($formula==2 && $max==1 && $rabat==50) {
            return '3I7192SHJ12ESVNG4JW4V479HB8WWBU6';
        }
        
        // Rabat 100%
        if($rabat==100) {
            return '7FS6F727ZAJYXEUMQGD326NDH4T4BUG8';
        }     */   
    }
    
    public function getUserJobList($employeer = false, $int = false) {
        if($employeer) {
            if($int) {
                $sql = "SELECT c.cv_src, u.user_adress_city, u.user_id, a.application_date, j.job_added_by, a.application_status, j.job_position, j.job_id, j.job_url_text, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, AES_DECRYPT(u.user_phone, '".SALT."') as user_phone, AES_DECRYPT(u.user_adress_city, '".SALT."') as user_city FROM ".DB_PREF."application as a LEFT JOIN ".DB_PREF."job as j ON j.job_id = a.application_job_id LEFT JOIN ".DB_PREF."user as u ON u.user_id = a.application_user_id LEFT JOIN ".DB_PREF."user_cv as c ON c.cv_user_id = a.application_user_id WHERE j.job_added_by = ? AND j.job_id = ? AND a.application_status = 0";
                $pre = $this->database->prepare($sql);

                $pre->execute(array($this->getUserId(), $int));            
            } else {
                $sql = "SELECT c.cv_src, u.user_adress_city, u.user_id, j.job_added_by, a.application_date, a.application_status, j.job_position, j.job_id, j.job_url_text, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, AES_DECRYPT(u.user_phone, '".SALT."') as user_phone, AES_DECRYPT(u.user_adress_city, '".SALT."') as user_city FROM ".DB_PREF."application as a LEFT JOIN ".DB_PREF."job as j ON j.job_id = a.application_job_id LEFT JOIN ".DB_PREF."user as u ON u.user_id = a.application_user_id LEFT JOIN ".DB_PREF."user_cv as c ON c.cv_user_id = a.application_user_id WHERE j.job_added_by = ? AND j.job_publish_date_end >= NOW() AND u.user_active = 1";
                $pre = $this->database->prepare($sql);

                $pre->execute(array($this->getUserId()));                  
            }
        } else {
            $sql = "SELECT a.application_date, a.application_status, j.job_position, j.job_id, j.job_employeer_name, j.job_url_text FROM ".DB_PREF."application as a LEFT JOIN ".DB_PREF."job as j ON j.job_id = a.application_job_id WHERE a.application_user_id = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($this->getUserId()));
        }
        if($pre->rowCount()!=0) {
            $jobList = $pre->fetchAll();
            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function getAcceptedUserJobList($int ) {

        $sql = "SELECT u.user_id, a.application_date, j.job_added_by, a.application_status, j.job_position, j.job_id, j.job_url_text, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, AES_DECRYPT(u.user_phone, '".SALT."') as user_phone, AES_DECRYPT(u.user_adress_city, '".SALT."') as user_city FROM ".DB_PREF."application as a LEFT JOIN ".DB_PREF."job as j ON j.job_id = a.application_job_id LEFT JOIN ".DB_PREF."user as u ON u.user_id = a.application_user_id WHERE j.job_added_by = ? AND j.job_id = ? AND a.application_status = 2";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId(), $int));            

        if($pre->rowCount()!=0) {
            $jobList = $pre->fetchAll();
            return $jobList;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function countApplications($id) {
        $sql = "SELECT count(application_id) as count FROM ".DB_PREF."application WHERE application_job_id = ? AND application_status = 0";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $count = $pre->fetch();
        
        if($count['count']!=0) {
            return $count['count'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }        
    }
    
    public function countAcceptedApplications($id) {
        $sql = "SELECT count(application_id) as count FROM ".DB_PREF."application WHERE application_job_id = ? AND application_status = 2";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $count = $pre->fetch();
        
        if($count['count']!=0) {
            return $count['count'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }        
    }        
    public function countOneClick($id) {
        $sql = "SELECT o.oneclick_id FROM ".DB_PREF."application_oneclick AS o LEFT JOIN ".DB_PREF."application AS a ON o.oneclick_employee_id = a.application_user_id WHERE a.application_job_id = ? AND a.application_status = 0";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return count($res);
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }       
    }
    
    public function getOneClickJobList($id = false) {
        if($id) {
            $listTmp = $this->getUserJobList(true, $id);
            $list = array();
            if($listTmp) {
                foreach($listTmp as $l) {
                    if($this->checkOneClick($l['user_id'], $l['job_added_by'])) {
                        $list[] = $l;
                    }
                }
                return $list;
            } else {
                return false;
            }
            
        } else {
            $sql = "SELECT o.oneclick_id, u.user_id, u.user_adress_city, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_email, '".SALT."') as user_email, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname, AES_DECRYPT(u.user_adress_city, '".SALT."') as user_address_city, AES_DECRYPT(u.user_phone, '".SALT."') as user_phone FROM ".DB_PREF."application_oneclick as o LEFT JOIN ".DB_PREF."user as u ON u.user_id = o.oneclick_employee_id WHERE o.oneclick_employeer_id = ? AND u.user_active = 1";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($this->getUserId()));  
            
            $res = $pre->fetchAll();

            if(count($res)>0) {
                return $res;
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;
            } 
        }
        
    }    
    
    public function checkOneClick($uid = false) {
            if($uid) {
            $sql = "SELECT oneclick_id FROM ".DB_PREF."application_oneclick WHERE oneclick_employeer_id = ? AND oneclick_employee_id = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($this->getUserId(), $uid));
            $res = $pre->fetchAll();
        } else {
            $sql = "SELECT oneclick_id FROM ".DB_PREF."application_oneclick WHERE oneclick_employeer_id = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($this->getUserId()));
            $res = $pre->fetchAll();
        }
        if(count($res)>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }        
    }
    
    public function checkUserExists($id) {
        $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_id = ? AND user_type = 1";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }               
    }
    
    public function addUserToOneClick($id, $email = false) {
        if($id) {
            $sql = "INSERT INTO ".DB_PREF."application_oneclick (oneclick_employeer_id, oneclick_employee_id) VALUES (?,?)";

            $pre = $this->database->prepare($sql);

            if($pre->execute(array($this->getUserId(), $id))) {
                return $this->database->lastInsertId();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;            
            }
        } else {
            $sql = "INSERT INTO ".DB_PREF."application_oneclick (oneclick_employeer_id, oneclick_token) VALUES (?,?)";

            $pre = $this->database->prepare($sql);

            if($pre->execute(array($this->getUserId(), $email))) {
                return $this->database->lastInsertId();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return false;            
            }    
        }
    }    
    
    public function removeUserToOneClick($id) {
        $sql = "DELETE FROM ".DB_PREF."application_oneclick WHERE oneclick_employeer_id = ? AND oneclick_employee_id = ?";

        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($this->getUserId(), $id))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            print_r($pdoError);
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;            
        }
      
    }
    
    public function getJobPosition($id) {
        $sql = "SELECT job_position, job_id, job_url_text FROM ".DB_PREF."job WHERE job_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }          
    }
      
    public function getUserEmail($id = false) {
        if($id) {
            $sql = "SELECT AES_DECRYPT(user_email, '".SALT."') as user_email FROM ".DB_PREF."user WHERE user_id = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($id));
            $res = $pre->fetch();   
        } else {
            $sql = "SELECT AES_DECRYPT(user_email, '".SALT."') as user_email FROM ".DB_PREF."user WHERE user_id = ?";
            $pre = $this->database->prepare($sql);

            $pre->execute(array($this->getUserId()));
            $res = $pre->fetch();
        }
        if(count($res)>0) {
            return $res['user_email'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }          
    }
    
    
    public function changeApplicationStatus($status, $uid, $jid) {
        if($status) {
            $sql = "UPDATE ".DB_PREF."application SET application_status = 2, application_accepted_date = NOW() WHERE application_user_id = ? AND application_job_id = ?";
        } else {
            $sql = "UPDATE ".DB_PREF."application SET application_status = 1, application_accepted_date = NOW() WHERE application_user_id = ? AND application_job_id = ?";
        }
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($uid, $jid))) {
            $userEmail = $this->getUserEmail($uid);
            $job = $this->getJob($jid);

            if($status) {
                $body = '<h3>Kandydacie</h3><p>I-wszy etap selekcji przebiegł pomyślnie. Pracodawca rozważa Twoją kandydaturę na to stanowisko. Możliwe, że  zaprosi Ciebie na rozmowę kwalifikacyjną. </p>';
                $body .= '<p>Link do oferty: <a href="http://www.'.$_SERVER['SERVER_NAME'].HOST.'praca/'.$job['job_id'].'/'.$job['job_url'].'">'.$job['job_position'].'</a></p>';
                $title = 'happinate.com – pierwszy etap selekcji przebiegł pomyślnie';
            } else {
                $body = '<h3>Przykro nam</h3><p>Dziękujemy za złożoną aplikację na stanowisko '.$job['job_position'].' Selekcja została zakończona. Niestety tym razem, nie powiodło się!</p><p>Wejdź na happinate.com i sprawdź najświeższe oferty pracy </p>';
                $title = 'happinate.com - pierwszy etap selekcji został zakończony ';                
            }
            $body .= '<p>Pozdrawiamy, happinate.com <br/>praca od zaraz</p>';


            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($userEmail, $title, $body);
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            print_r($pdoError);
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;            
        }
      
    }     
    
    public function oneClick($uid, $jid) {
        $sql = "UPDATE ".DB_PREF."application SET application_status = 2, application_accepted_date = NOW() WHERE application_user_id = ? AND application_job_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($uid, $jid))) {
            
            $userEmail = $this->getUserEmail($uid);
            $job = $this->getJob($jid);         
            
            $body = '<h3>Kandydacie, gratulujemy!</h3><p>Właśnie dostałeś ofertę pracy!</p>';
            $body .= '<p>Link do oferty: <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'praca/'.$job['job_id'].'/'.$job['job_url'].'">'.$job['job_position'].'</a></p>';
            $title = 'happinate.com - dostałeś ofertę pracy 1-click';

            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($userEmail, $title, $body);
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            print_r($pdoError);
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;            
        }  
    }    
    
    public function checkOneClickEmail($email) {
        $sql = "SELECT oneclick_id FROM ".DB_PREF."application_oneclick WHERE oneclick_employeer_id = ? AND oneclick_token = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId(), $email));
        
        $res = $pre->fetch();

        if($res['oneclick_id']) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function newOneClick($email, $msg, $tags) {

        if($this->checkEmailNotExist($email)) {
            $id = $this->getUserId($email);
            
            if(!$this->checkOneClick($id)) {
                if($oid = $this->addUserToOneClick($id)) {
                    foreach($tags as $tag) {
                        $this->addTagsToUser($tag, $oid);
                    }
                    return 1;
                } else {
                    return 0;
                }
            }
        } else { 
            if(!$this->checkOneClickEmail($email)) {
                $token = sha1(time());

                $body = "<h3>Gratulujemy!</h3> <p>Pracodawca <strong>".$this->getUserEmail()."</strong> chce Cię dodać do swoich ulubionych kandydatów. Zaakceptuj to zaproszenie a będziesz dostawać najnowsze oferty pracy. Dzięki opcji 1-click możesz od razu umówić się do pracy we wskazanym przez pracodawcę terminie - bez wysyłania CV, bez rekrutacji!</a>";
                $body .= '<p><a style="width: 200px; text-align: center; margin: 0 auto; display: block; padding: 5px 10px; border: 1px solid #268888; background: #39ADAD; color: #FFF; text-decoration: none;" href="http://'.$_SERVER['SERVER_NAME'].HOST.'rejestracja/zaproszenie/'.$token.'">AKCEPTUJE ZAPROSZENIE</a></p>';
                if($msg) $body .= '<p></br>Wiadomość od użytkownika: "'.$msg.'"</p>';
                $body .= "<p>Miłego dnia,</p>";
                $body .= "<p>Zespół happinate.com</p>";
                $body .= "<p>praca dorywcza od zaraz!</p>";

                $title = 'happinate.com- potwierdź zaproszenie od pracodawcy';

                $oid = $this->addUserToOneClick(false, $token, $email); 

                foreach($tags as $tag) {
                    $this->addTagsToUser($tag, $oid);
                }
                
                //Email::sendTo($email, $title, $body);  
                $_emailClass = $GLOBALS['_emailClass'];
                $_emailClass->addToList($email, $title, $body);
                return 2;
            } else {
                return 3;
            }
        }
    }
    
    public function countSendOneClick() {
        $sql = "SELECT * FROM ".DB_PREF."application_oneclick WHERE oneclick_employeer_id = ? AND oneclick_employee_id is NULL ";
              
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        if($pre->rowCount()!=0) {
            return $pre->rowCount();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        } 
    }
    
    public function getTags($name = false) {
        if($name) {
            $sql = "SELECT tag_id FROM ".DB_PREF."tag WHERE tag_name = ?";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($name));
            if($pre->rowCount()!=0) {
                $res = $pre->fetch();
                return $res['tag_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return 0;
            }  
        } else {
            $sql = "SELECT * FROM ".DB_PREF."tag WHERE tag_active = 1 ORDER BY tag_sort ASC";
            $pre = $this->database->prepare($sql);
            $pre->execute();
            if($pre->rowCount()!=0) {
                return $pre->fetchAll();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return 0;
            }    
        }
     
    }
        
    public function getProvinces($name = false) {
        if($name) {
            $sql = "SELECT province_id FROM ".DB_PREF."province WHERE province_text = ?";

            $pre = $this->database->prepare($sql);
            $pre->execute(array($name));
            if($pre->rowCount()!=0) {
                $res = $pre->fetch();
                return $res['province_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return 0;
            }    
        } else {
            $sql = "SELECT * FROM ".DB_PREF."province";

            $pre = $this->database->prepare($sql);
            $pre->execute();
            if($pre->rowCount()!=0) {
                return $pre->fetchAll();
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
                return 0;
            }   
        }
    }
    
    public function getCities() {
        $sql = "SELECT * FROM ".DB_PREF."city";
        $pre = $this->database->prepare($sql);
        $pre->execute();
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }     
    }
    
        
    public function getCityId($name) {
        $sql = "SELECT city_id FROM ".DB_PREF."city WHERE city_text = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($name));
        
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['city_id'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }        
    
    public function getCityName($id) {
        $sql = "SELECT city_text FROM ".DB_PREF."city WHERE city_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['city_text'];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function addTagsToUser($tag, $oneclickId) {
        $tagID = $this->checkTag($tag);

        $sql = "INSERT INTO ".DB_PREF."tag_oneclick (oneclick_id, tag_id) VALUES (?, ?)";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($oneclickId, $tagID));

        if($pre->rowCount()!=0) {
            return true;
        } else {
            return false;
        }
    }
        
    public function addTagsToJob($tag, $jobId) {
        $tagID = $this->checkTag($tag);

        $sql = "INSERT INTO ".DB_PREF."tag_job (job_id, tag_id) VALUES (?, ?)";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($jobId, $tagID));

        if($pre->rowCount()!=0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function checkTag($tag) {
        $sql = "SELECT tag_id FROM ".DB_PREF."tag WHERE tag_name = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($tag));
        
        if($pre->rowCount()!=0) {
            $res = $pre->fetch();
            return $res['tag_id'];
        } else {
            $pre->closeCursor();
            $sql = "INSERT INTO ".DB_PREF."tag (tag_name) VALUES (?)";
            $pre = $this->database->prepare($sql);

            $pre->execute(array(Form::sanitizeString($tag)));

            if($pre->rowCount()!=0) {
                return $this->database->lastInsertId();
            } else {
                return 0;
            }
        }
    }
    
    
    public function getUserTags($id) {
        $sql = "SELECT t.tag_name FROM ".DB_PREF."tag_oneclick as o LEFT JOIN ".DB_PREF."tag as t ON o.tag_id = t.tag_id WHERE o.oneclick_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        $res = $pre->fetchAll();

        if($pre->rowCount()!=0) {
            $string = '';
            $i = 0;
            foreach($res as $r) {
                if($i!=0) $string .= ', ';
                $string .= $r['tag_name'];
                $i++;
            }
            return $string;
        } else {
            return false;
        }
    }    
    
    public function getJobTags($id) {
        $sql = "SELECT t.tag_name FROM ".DB_PREF."tag_job as j LEFT JOIN ".DB_PREF."tag as t ON j.tag_id = t.tag_id WHERE j.job_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
        $res = $pre->fetchAll();

        if($pre->rowCount()!=0) {
            $string = '';
            $i = 0;
            foreach($res as $r) {
                if($i!=0) $string .= ', ';
                $string .= $r['tag_name'];
                $i++;
            }
            return $string;
        } else {
            return false;
        }
    }
    
    public function editOneclickTags($id, $tags) {
        $this->deleteOneclickTags($id);
        die($id);
        foreach ($tags as $tag) {
            $this->addTagsToUser($tag, $id);
        }
        return true;
    } 
    
    public function deleteOneclickTags($id) {
        $sql = "DELETE FROM ".DB_PREF."tag_oneclick WHERE oneclick_id = ?";
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($id));
    }

    public function getCVList() {
        $sql = "SELECT c.cv_id, c.cv_public, c.cv_date, c.cv_src, c.cv_name, a.application_job_id FROM ".DB_PREF."user_cv as c LEFT JOIN ".DB_PREF."application as a ON c.cv_application_id = a.application_id WHERE c.cv_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));

        $res = $pre->fetchAll();
        if($res) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return 0;
        }
    }
    
    
    public function checkCVExists($id) {
        $sql = "SELECT cv_id FROM ".DB_PREF."user_cv WHERE cv_id = ? AND cv_user_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id, $this->getUserId()));
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }               
    }    
    
    public function deleteCV($id) {
        $sql = "SELECT cv_src FROM ".DB_PREF."user_cv WHERE cv_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $res = $pre->fetch();

        $fileSrc = HOST . "upload/userCV/".$res['cv_src'];
        
        if(file_exists($fileSrc)) {
            unlink($fileSrc);
        }
        $pre->closeCursor();
        
        $sql = "DELETE FROM ".DB_PREF."user_cv WHERE cv_id = ?";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;  
        }
    }
    
    public function checkTagExists($id) {
        $sql = "SELECT tag_id FROM ".DB_PREF."tag WHERE tag_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($id));
        $res = $pre->fetch();
        
        if(count($res)>0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }               
    }   
    
    public function addCV($post, $cv) {
        
        $pathParts = pathinfo($cv['name']);
        $extension = $pathParts['extension'];

        $fileName = strtolower(str_replace('.'.$extension,'',$cv['name']));

        $cvName = sha1($fileName.time()).'.'.$extension;
        $fileUploadPath = __DIR__ . '/../../../upload/userCV/' . $cvName;

        if(!move_uploaded_file($cv['tmp_name'], $fileUploadPath)) {
            return false;
        }

        $sql = "INSERT INTO ".DB_PREF."user_cv (cv_user_id, cv_date, cv_src, cv_name, cv_public) VALUES (?, NOW(), ?, ?, ?);";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($this->getUserId(), $cvName, $post['name'], $post['public']))){
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function getStatistics() {
        $stats = array();
        
        // Users All
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_type = 1 OR user_type = 2");
        $pre->execute();
        $res = $pre->fetch();
        $stats['users']['all'] = $res['count'];
        $pre->closeCursor();
        
        // Users kandydaci
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_type = 1");
        $pre->execute();
        $res = $pre->fetch();
        $stats['users']['kandydaci'] = $res['count'];
        $pre->closeCursor();  
        
        // Users pracodawcy
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_type = 2");
        $pre->execute();
        $res = $pre->fetch();
        $stats['users']['pracodawcy'] = $res['count'];
        $pre->closeCursor();
                
        // Users active
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_active = 1 AND (user_type = 1 OR user_type = 2)");
        $pre->execute();
        $res = $pre->fetch();
        $stats['users']['aktywni'] = $res['count'];
        $stats['users']['nieaktywni'] = $stats['users']['all'] - $res['count'];
        $pre->closeCursor();
        
        // Users dzisiaj
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user WHERE user_register_date > ?");
        $pre->execute(array(date("Y-m-d").' 00:00:00'));
        $res = $pre->fetch();
        $stats['users']['dzisiaj'] = $res['count'];
        $pre->closeCursor();
                        
        // Jobs all
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."job WHERE job_publish_date_end < ? AND job_delete = 0");
        $pre->execute(array(date('Y-m-d H:i:s', strtotime("+30 days"))));
        $res = $pre->fetch();
        $stats['jobs']['all'] = $res['count'];
        $pre->closeCursor();  
        
        // Jobs aktywne
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."job WHERE job_publish_date > NOW() AND job_publish_date_end < NOW() AND job_delete = 0 AND job_status = 1");
        $pre->execute();
        $res = $pre->fetch();
        $stats['jobs']['aktywne'] = $res['count'];
        $pre->closeCursor();  
                        
        // Jobs aktywne
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."job WHERE job_publish_date > NOW() AND job_publish_date_end < NOW() AND job_delete = 0 AND job_dotpay_tid != ''");
        $pre->execute();
        $res = $pre->fetch();
        $stats['jobs']['zaplacone'] = $res['count'];
        $pre->closeCursor();  
                
        // Jobs today
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."job WHERE job_created_date > ?");
        $pre->execute(array(date("Y-m-d").' 00:00:00'));
        $res = $pre->fetch();
        $stats['jobs']['dzisiaj'] = $res['count'];
        $pre->closeCursor();  
                            
        // Application all
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."application");
        $pre->execute(array(date('Y-m-d H:i:s', strtotime("+30 days"))));
        $res = $pre->fetch();
        $stats['apps']['all'] = $res['count'];
        $pre->closeCursor();  
        
        // Application dzisiaj
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."application WHERE application_date > ?");
        $pre->execute(array(date("Y-m-d").' 00:00:00'));
        $res = $pre->fetch();
        $stats['apps']['dzisiaj'] = $res['count'];
        $pre->closeCursor();  
        
        // Oneclick all
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."application_oneclick");
        $pre->execute(array(date("Y-m-d").' 00:00:00'));
        $res = $pre->fetch();
        $stats['oneclick']['all'] = $res['count'];
        $pre->closeCursor();  
        
        // CV all
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user_cv");
        $pre->execute(array(date("Y-m-d").' 00:00:00'));
        $res = $pre->fetch();
        $stats['cv']['all'] = $res['count'];
        $pre->closeCursor();  
        
        // CV dzisiaj
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."user_cv WHERE cv_date > ?");
        $pre->execute(array(date("Y-m-d").' 00:00:00'));
        $res = $pre->fetch();
        $stats['cv']['dzisiaj'] = $res['count'];
        $pre->closeCursor(); 
        
        // Tagi all
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."tag");
        $pre->execute();
        $res = $pre->fetch();
        $stats['tag']['all'] = $res['count'];
        $pre->closeCursor(); 
        
        // Tagi aktywne
        $pre = $this->database->prepare("SELECT COUNT(*) as count FROM ".DB_PREF."tag WHERE tag_active = 1");
        $pre->execute();
        $res = $pre->fetch();
        $stats['tag']['aktywne'] = $res['count'];
        $stats['tag']['nieaktywne'] = $stats['tag']['all'] - $res['count'];
        $pre->closeCursor(); 

        return $stats;
    }
    
    public function tagAccept($id) {
        $sql = "UPDATE ".DB_PREF."tag SET tag_active = 1 WHERE tag_id = ?;";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id))){
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        } 
    }
    
    public function usunAccept($id) {
        $sql = "DELETE FROM ".DB_PREF."tag WHERE tag_id = ?;";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($id))){
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        } 
    }
    
        
    public function getRabaty() {
        $sql = "SELECT rabat_id, rabat_date, rabat_name, rabat_percent, rabat_count, rabat_used, AES_DECRYPT(rabat_code, '".SALT."') as rabat_code FROM ".DB_PREF."job_rabat ORDER BY rabat_id ASC";
        $pre = $this->database->prepare($sql);

        $pre->execute();
        $res = $pre->fetchAll();
        
        if(count($res)>0) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }       
    
    public function newRabat($name, $post) {
        $sql = "INSERT INTO ".DB_PREF."job_rabat (rabat_name, rabat_percent, rabat_code, rabat_count, rabat_date) VALUES (?, ?, AES_ENCRYPT(?, '".SALT."'), ?, NOW())";
        $pre = $this->database->prepare($sql);
        
        $code = $this->generateCode();
        
        $pre->execute(array($name, $post['procent'], $code, $post['count']));

        $id = $this->database->lastInsertId();
        if($id) {
            return '<p>'.$code.'</p>';
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
    public function generateCode() {
        $znaki .= "ABCDEFGHIJKLMNPRSTUWZYXQ";
        $znaki .= "123456789";
        $dl = strlen($znaki) - 1;
        for($i =0; $i < 8; $i++)
        {
            $losuj = rand(0, $dl);
            $wynik .= $znaki[$losuj];
        }
        return $wynik; 
    }
    
    public function checkUserEmailForMod($email) {
        $sql = "SELECT user_type FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."')";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($email));
        $res = $pre->fetch();
        
        if(!empty($res)) {
            return $res['user_type'];
        } else {
            return 0;
        }
    }
    
    public function getOneClickUsers() {
        $sql = "SELECT u.user_id, AES_DECRYPT(u.user_email, '".SALT."') as user_email, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname FROM ".DB_PREF."user as u LEFT JOIN ".DB_PREF."application_oneclick as a ON a.oneclick_employee_id = u.user_id WHERE a.oneclick_employeer_id = ?";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($this->getUserId()));
        $res = $pre->fetchAll();
        
        if(!empty($res)) {
            return $res;
        } else {
            return 0;
        } 
    }
        
    public function getOneClickUsersByTags($tags) {
        $sql = "SELECT DISTINCT u.user_id, AES_DECRYPT(u.user_email, '".SALT."') as user_email FROM ".DB_PREF."tag_oneclick as t LEFT JOIN ".DB_PREF."application_oneclick as a ON t.oneclick_id = a.oneclick_id LEFT JOIN ".DB_PREF."user as u ON u.user_id = a.oneclick_employee_id WHERE a.oneclick_employeer_id = ? ";
        
        $i = 1;
        $count = count($tags);


        $sql .= " AND (";
        
        foreach($tags as $tag) {
            $sql .= " t.tag_id = " . $tag;
            if($i<$count) {
                $sql .= ' OR ';
            }
            $i++;
        }
        $sql .= ")";
        
        $pre = $this->database->prepare($sql);
        
        $pre->execute(array($this->getUserId()));
        $res = $pre->fetchAll();
        
        #die($sql);
        
        if(!empty($res)) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        } 
    }
    
    public function getTradeObservators($trade_id) {
        $sql = "SELECT DISTINCT u.user_id, AES_DECRYPT(u.user_email, '".SALT."') as user_email FROM ".DB_PREF."user as u WHERE u.user_main_tag_id = ? AND u.user_type = 1";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($trade_id));
        $res = $pre->fetchAll();
        
        if(!empty($res)) {
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
    public function getTradeInfo($trade_id) {
        $sql = "SELECT tag_name FROM ".DB_PREF."tag WHERE tag_id = ? AND tag_active = 1";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($trade_id));
        $res = $pre->fetchAll();
        
        if (isset($res[0])) {
            return $res[0];
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
}

