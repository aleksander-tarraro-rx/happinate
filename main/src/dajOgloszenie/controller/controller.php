<?php

namespace src\dajOgloszenie;

use app\helpers\Form;
use src\BaseController;

class dajOgloszenieController extends BaseController {
    
    private $template;
    private $model;
    private $user;
    private $userType;

    public function __construct($template, $model, $user) {
        $this->template = $template;
        $this->model = $model;
        $this->user = $user;
        
        if($this->user)
            $this->userType = $user->getUserType();
        
        $this->template->assign('model', $this->model);
        $this->template->assign('_user', $this->user);
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        
        $contractType = $this->model->getContractType();
        $cities = $this->model->getCities();
        $tagList = $this->model->getTags();
        $provinces = $this->model->getProvinces();

        $this->template->assign('tagList',$tagList);
        $this->template->assign('contractType', $contractType);
        $this->template->assign('cities', $cities);
        $this->template->assign('provinces', $provinces);
        
        if(isset($_SESSION['basket']['step']) && $_SESSION['basket']['step']>1) {
            switch($_SESSION['basket']['step']) {
                case 2:
                    if(isset($_POST['step'])) {
                        if($_POST['step']==1) {
                            $_SESSION['basket']['step'] = 1;
                            header("Location:".HOST."daj-ogloszenie");
                        } else {
                            $error = '';
                            $errorArray = array();
                            $_POST = Form::sanitizeArray($_POST);
                            
                            if(isset($_POST['hours_per_week']) && !empty($_POST['hours_per_week']) && !is_numeric($_POST['hours_per_week'])) {
                                $error .= '<p>- Proszę wpisać prawidłową ilość godzin w ciągu tygodnia.</p>';
                                $errorArray['hours_per_week'] = true;
                            } elseif($_POST['hours_per_week']>168) {
                                $error .= '<p>- Cały tydzień posiada tylko 168 godzin.</p>';
                                $errorArray['hours_per_week'] = true;                                
                            }
                                                                                    
                            if(isset($_POST['www']) && !empty($_POST['www']) && !Form::validUrl($_POST['www'])) {
                                $error .= '<p>- Proszę wpisać prawidłowy adres www.</p>';
                                $errorArray['www'] = true;
                            }
                            
                            if(isset($_POST['fanpage']) && !empty($_POST['fanpage']) && !Form::validUrl($_POST['fanpage'])) {
                                $error .= '<p>- Proszę wpisać prawidłowy adres fanpage.</p>';
                                $errorArray['fanpage'] = true;
                            }

                            // kalendarz
					
                            if(MOD_CALENDAR) {
                                if(isset($_POST['calendar']) && is_array($_POST['calendar']) && (!empty($_POST['calendar']['start']) || !empty($_POST['calendar']['stop']))) {
                                    if(!empty($_POST['calendar']['start']) && !Form::validDate($_POST['calendar']['start'])) {

                                        $error .= '<p>- Proszę podać prawidłową datę rozpoczęcia rekrutacji.</p>';
                                        $errorArray['calendar']['start'] = true;                                
                                    }
                                    elseif(!empty($_POST['calendar']['start']) && Form::validDate($_POST['calendar']['start']))
                                    {
                                        $calendar_start = strtotime($_POST['calendar']['start']);
                                    }

                                    if(!empty($_POST['calendar']['stop']) && !Form::validDate($_POST['calendar']['stop'])) {
                                        $error .= '<p>- Proszę podać prawidłową datę zakończenia rekrutacji.</p>';
                                        $errorArray['calendar']['stop'] = true;
                                    }
                                    elseif(!empty($_POST['calendar']['stop']) && Form::validDate($_POST['calendar']['stop']))
                                    {
                                        $calendar_stop = strtotime($_POST['calendar']['stop']);
                                    }                            

                                    if(!empty($_POST['calendar']['exludeSunSut']) && (empty($calendar_start) || empty($calendar_stop)))  
                                    {
                                        $error .= '<p>- Proszę podać prawidłową datę rozpoczęcia i zakończenia rekrutacji.</p>';
                                        $errorArray['calendar']['exludeSunSut'] = true;
                                        $errorArray['calendar']['start'] = true;
                                        $errorArray['calendar']['stop'] = true;
                                    }

                                    if(!empty($_POST['calendar']['hoursFrom']) || (empty($calendar_start) || empty($calendar_stop))) 
                                    {
                                        $hour = (int)$_POST['calendar']['hoursFrom'];
                                        if(empty($hour))
                                        {
                                            $error .= '<p>- Godzina rozpoczęcia rekrutacji musi być liczbą. Musisz podać datę rozpoczęcia i zakończenia rekrutacji.</p>';
                                            $errorArray['calendar']['hoursFrom'] = true;                                    
                                        }
                                    }

                                    if(!empty($_POST['calendar']['hoursTo']) || (empty($calendar_start) || empty($calendar_stop))) {
                                        $hour = (int)$_POST['calendar']['hoursTo'];
                                        if(empty($hour))
                                        {
                                            $error .= '<p>- Godzina zakończenia rekrutacji musi być liczbą. Musisz podać datę rozpoczęcia i zakończenia rekrutacji.</p>';
                                            $errorArray['calendar']['hoursTo'] = true;                                    
                                        }
                                    }  

                                    if(!empty($_POST['calendar']['candidatePerHour']) || (empty($calendar_start) || empty($calendar_stop))) {
                                        $candidatePerHour = (int)$_POST['calendar']['candidatePerHour'];
                                        if(empty($candidatePerHour))
                                        {
                                            $error .= '<p>- Liczba kandydatów na godzinę musi być liczbą.</p>';
                                            $errorArray['calendar']['candidatePerHour'] = true;                                    
                                        }
                                    }

                                    if(!empty($calendar_start) && !empty($calendar_stop))
                                    {
                                        if($calendar_start > $calendar_stop)
                                        {
                                            $error .= '<p>- Data rozpoczęcia rekrutacji nie może być mniejsza niż data zakończenia.</p>';
                                            $errorArray['calendar']['hoursTo'] = true;                                     
                                        }

                                        $calendar_today = strtotime(date("Y-m-d"));
                                        if($calendar_start < $calendar_today)
                                        {
                                            $error .= '<p>- Data rozpoczęcia rekrutacji nie może być mniejsza niż data dzisiejsza.</p>';
                                            $errorArray['calendar']['hoursFrom'] = true;                                     
                                        }                                    

                                    } 
                                }
                            }

                            if((isset($_POST['oneclick']) || isset($_POST['oneclickAll'])) && (!isset($_POST['start_date']) || empty($_POST['start_date']))) {
                                $error .= '<p>- Proszę uzupełnić datę rozpoczęcia pracy.</p>';
                                $errorArray['start_date'] = true;   
                            }
                            
                            # file
                            $fileName = false;
                            if(isset($_FILES['logo']) && $_FILES['logo']['error']==0) {
                                $imgInfo = getimagesize($_FILES['logo']['tmp_name']);

                                if(!($imgInfo['mime']=='image/jpeg' || $imgInfo['mime']=='image/png' || $imgInfo['mime']=='image/gif')) {
                                    $error .= '<p>- przesłane logo musi być formatu png, jpg lub gif.</p>';
                                    $errorArray['logo'] = true;
                                }

                                if($imgInfo[0]>1024 || $imgInfo[1]>768) {
                                    $error .= '<p>- przesłane logo może mieć maksymalną rozdzielczość 1024px na 768px (szerokość / wysokość).</p>';
                                    $errorArray['logo'] = true;
                                }

                                if($_FILES['logo']['size']>512000) {
                                    $error .= '<p>- przesłane logo waży zbyt dużo (maksymalna waga to 500kb).</p>';
                                    $errorArray['logo'] = true;
                                }

                                if(isset($_FILES['logo']) && $_FILES['logo']['error']==0) {
                                    
                                    $pathParts = pathinfo($_FILES['logo']['name']);
                                    $extension = $pathParts['extension'];

                                    $fileName = sha1(time()).'.'.$extension;
                                    $_POST['logo'] = $fileName;
                                    
   
                                    $fileUploadPath = __DIR__ . '/../../../upload/tmp/';
                                            
                                    if(!move_uploaded_file($_FILES['logo']['tmp_name'], $fileUploadPath . $fileName)) {
                                        $fileName = '';
                                    } else {
                                        if($imgInfo[0]>200 || $imgInfo[1]>100) {
                                            require __DIR__ . '/../../../app/libs/imageresizer/SimpleImage.php';
                                            $SimpleImage = new \SimpleImage();
                                            if($imgInfo[0]>$imgInfo[1]) {
                                                $SimpleImage->load($fileUploadPath . $fileName); 
                                                $SimpleImage->resizeToWidth(200); 
                                                $SimpleImage->save($fileUploadPath . $fileName);
                                            } else {
                                                $SimpleImage->load($fileUploadPath . $fileName); 
                                                $SimpleImage->resizeToHeight(100); 
                                                $SimpleImage->save($fileUploadPath . $fileName);                         
                                            }
                                        }
                                        $_POST['trashFile'] = false;
                                    }

                                }
                            } elseif(isset($_POST['logo']) && $_POST['logo'] == 'user' && $_POST['trashFile']==0) {
                                $fileName = 'user';
                            } elseif(isset($_POST['logo'])) {
                                $fileName = $_POST['logo'];
                            } else {
                                $fileName = false;
                            }

                            
                            if(isset($_POST['trashFile']) && $_POST['trashFile']) {
                                if(!empty($_POST['logo']) && $_POST['logo']!='user') {
                                    $file =  __DIR__ . '/../../../upload/tmp/' . $_POST['logo'];
                                    if(file_exists($file)) {
                                        unlink($file);
                                    }
                                }
                                $fileName = '';
                                $_POST['logo'] = null;
                                $_FILES = NULL;
                            } 
							
                            if(!$error) {
                                foreach($_POST as $key=>$val) {
                                    $_SESSION['basket'][$key] = $val;
                                }
                                $_SESSION['basket']['step'] = 3;
                                header("Location:".HOST."daj-ogloszenie");
                            } else {
                                $this->template->assign('errorArray', $errorArray);
                                $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
                            } 
                        }
                        
                    }
                    $oneclick = $this->model->getOneClickUsers();
                    
                    $this->template->assign('oneclick', $oneclick);
                    $this->template->display('dajOgloszenie/step2.tpl');
                break;
                case 3:
                    if(isset($_POST['step'])) {
                        if($_POST['step']==2) {
                            $_SESSION['basket']['step'] = 2;
                            header("Location:".HOST."daj-ogloszenie");
                        } else {
                            $error = '';
                            $errorArray = array();
                            $_POST = Form::sanitizeArray($_POST);
                            
                            if(!$this->user) {
                                if(!isset($_POST['email']) || empty($_POST['email'])) {
                                    $error .= '<p>- Proszę wpisać adres email.</p>';
                                    $errorArray['email'] = true;
                                } elseif(!Form::validEmail($_POST['email'])) {
                                    $error .= '<p>- Proszę wpisać poprawny adres email.</p>';
                                    $errorArray['email'] = true;
                                } elseif($this->model->getUserId($_POST['email'])) {
                                    $error .= '<p>- Podany adres email znajduje się już w bazie danych.</p>';
                                    $errorArray['email'] = true;
                                }

                                if(!isset($_POST['pass']) || empty($_POST['pass'])) {
                                    $error .= '<p>- Proszę wpisać hasło.</p>';
                                    $errorArray['pass'] = true;
                                } elseif(strlen($_POST['pass'])<5) {
                                    $error .= '<p>- Hasło musi się składać z minimum 5 znaków.</p>';
                                    $errorArray['pass'] = true;
                                } elseif(!isset($_POST['pass2']) || empty($_POST['pass2'])) {
                                    $error .= '<p>- Proszę powtórzyć hasło.</p>';
                                    $errorArray['pass2'] = true; 
                                } elseif($_POST['pass'] != $_POST['pass2']) {
                                    $error .= '<p>- Obydwa hasła muszą być identyczne.</p>';
                                    $errorArray['pass'] = true; 
                                    $errorArray['pass2'] = true; 
                                }
                            }
                            if(!$error) {
                                foreach($_POST as $key=>$val) {
                                    $_SESSION['basket'][$key] = $val;
                                }
                                $_SESSION['basket']['step'] = 4;
                                header("Location:".HOST."daj-ogloszenie");
                            } else {
                                $this->template->assign('errorArray', $errorArray);
                                $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
                            } 
                        }
                    }
                    $this->template->display('dajOgloszenie/step3.tpl');
                break;
                case 4:
                    switch($_SESSION['basket']['formula']) {
                        case 1:
                            if($id = $this->model->addJob($_SESSION['basket'], false, 1)) {
                                header("Location:".HOST."daj-ogloszenie/sukces/".$id);
                            }
                        break;
                        case 2:
                            $_SESSION['basket']['step'] = 3;
                            $control = sha1(time());

                            if(!$id = $this->model->addJob($_SESSION['basket'], $control, false, 1)) {
                                FlashMsg::add('warning', 'Niestety nie udało się doadć ogłoszenia..');
                                header("Location:".HOST."daj-ogloszenie");
                            }                                        

                            $url  = "https://ssl.dotpay.pl/?pid=ZV3NQR51647D8699Z6ZE6IWEG5RJRMT3";
                            $url .= "&type=0";
                            $url .= "&URL=http://".$_SERVER['SERVER_NAME'] . HOST ."daj-ogloszenie/sukces/".$id;

                            if($this->user) {
                                $url .= "&email=".$this->user->getUserEmail();
                            } else {
                                $url .= "&email=".$_POST['email'];
                            }
                            $url .= "&control=".$control;
                            $url .= "&back_button_url=http://".$_SERVER['SERVER_NAME'] . HOST ."daj-ogloszenie";

                            header("Location:".$url);
                        break;
                        case 3:
                            $_SESSION['basket']['step'] = 3;
                            $control = sha1(time());

                            if(!$id = $this->model->addJob($_SESSION['basket'], $control, false, 2)) {
                                FlashMsg::add('warning', 'Niestety nie udało się doadć ogłoszenia..');
                                header("Location:".HOST."moje-konto/daj-ogloszenie");
                            }                                        

                            $url  = "https://ssl.dotpay.pl/?pid=2Z8P8G8V6QY1GU7U1HWAIYWR6SUQT9S1";
                            $url .= "&type=0";
                            $url .= "&URL=http://".$_SERVER['SERVER_NAME'] . HOST ."daj-ogloszenie/sukces/".$id;
                            if($this->user) {
                                $url .= "&email=".$this->user->getUserEmail();
                            } elseif(isset($_POST['email'])) {
                                $url .= "&email=".$_POST['email'];
                            }
                            $url .= "&control=".$control;
                            $url .= "&back_button_url=http://".$_SERVER['SERVER_NAME'] . HOST ."daj-ogloszenie";

                            header("Location:".$url);
                        break;
                        default:
                            header("Location:".HOST."daj-ogloszenie");
                        break;
                    }
                    $this->template->display('dajOgloszenie/step3.tpl');
                break;
                default:
                    header("Location:".HOST."daj-ogloszenie");
                break;
            }
        } else {
            if(isset($_POST['step']) && $_POST['step']==1) {
                $error = '';
                $errorArray = array();
                $_POST = Form::sanitizeArray($_POST);

                # userEmail
                if($this->userType==6 || $this->userType==5) {
                    if(empty($_POST['userEmail'])) {
                        $error .= '<p>- Proszę wpisać użytkownika (adres email).</p>';
                        $errorArray['userEmail'] = true;
                    } elseif(!Form::validEmail($_POST['userEmail'])) {
                        $error .= '<p>- Proszę wpisać prawidłowy adres email użytkownika.</p>';
                        $errorArray['userEmail'] = true;  
                    } else {
                        if($res = $this->model->checkUserEmailForMod($_POST['userEmail'])) {
                            if($res!=2) {
                                $error .= '<p>- Ten użytkownik nie jest zarejestrowany jako pracodawca.</p>';
                                $errorArray['userEmail'] = true;
                            }
                        } else {
                            $error .= '<p>- Nie ma takiego użytkownika o takim adresie email.</p>';
                            $errorArray['userEmail'] = true;
                        }
                    }
                }

                # name
                if(empty($_POST['position'])) {
                    $error .= '<p>- Proszę wpisać nazwę stanowiska.</p>';
                    $errorArray['position'] = true;
                } elseif(strlen($_POST['position']) < 3 || strlen($_POST['position']) > 64) {
                    $error .= '<p>- Nazwa stanowiska musi mieć od 3 do 64 znaków.</p>';
                    $errorArray['position'] = true;
                }

                # tags
                if(!isset($_POST['tags']) || empty($_POST['tags'])) {
                    $error .= '<p>- Proszę wybrać minimum 2 rodzaje wykonywanej pracy.</p>';
                    $errorArray['tags'] = true;
                } else {
                    $tagsCount = count($_POST['tags']);

                    if($tagsCount<1) {
                        $error .= '<p>- Proszę wybrać minimum 1 rodzaj wykonywanej pracy.</p>';
                        $errorArray['tags'] = true;   
                    }

                    if($tagsCount>=20) {
                        $error .= '<p>- Maksymalna ilość tagów nie może przekraczać 20.</p>';
                        $errorArray['tags'] = true;   
                    }

                }

                # branża
                if(empty($_POST['trade'])) {
                    $error .= '<p>- Proszę wybrać branże.</p>';
                    $errorArray['trade'] = true;
                }

                # stawka
                if(isset($_POST['rate_min'])) {
                    (float) $_POST['rate_min'] = floatval($_POST['rate_min']);
                }
                if(isset($_POST['rate_max'])) {
                    (float) $_POST['rate_max'] = floatval($_POST['rate_max']);
                }
                if(isset($_POST['rate'])) {
                    (float) $_POST['rate'] = floatval($_POST['rate']);
                }
                if(isset($_POST['rateMonth'])) {
                    (float) $_POST['rateMonth'] = floatval($_POST['rateMonth']);
                }
                
                if(empty($_POST['paymant'])) {
                    $error .= '<p>- Proszę wybrać stawkę.</p>';
                    $errorArray['paymant'] = true;
                } elseif($_POST['paymant']=='stawka godzinowa') {
                    if((!isset($_POST['rate_min']) || !isset($_POST['rate_max']) || empty($_POST['rate_min']) || empty($_POST['rate_max'])) && (!isset($_POST['rate']) || empty($_POST['rate']))) {
                        $error .= '<p>- Proszę podać stawkę.</p>';
                        $errorArray['rate_min'] = true; 
                        $errorArray['rate_max'] = true; 
                        $errorArray['rate'] = true;  
                    } elseif(!is_float($_POST['rate_min']) || !is_float($_POST['rate_max']) || !is_float($_POST['rate'])){ 
                        $error .= '<p>- Proszę podać prawidłową wartość stawki minimalnej i maksymalnej.</p>';
                        $errorArray['rate_min'] = true; 
                        $errorArray['rate_max'] = true;   
                        $errorArray['rate'] = true;  
                    } elseif($_POST['rate_min']>$_POST['rate_max']) {
                        $error .= '<p>- Kwota maksymalna nie może być mniejsza od kwoty minimalnej..</p>';
                        $errorArray['rate_min'] = true; 
                        $errorArray['rate_max'] = true;     
                    }
                } elseif($_POST['paymant']=='stawka miesięczna') {
                    if(!isset($_POST['rateMonth']) || empty($_POST['rateMonth'])) {
                        $error .= '<p>- Proszę podać stawkę miesięczną.</p>';
                        $errorArray['rateMonth'] = true; 
                    } elseif(!is_float($_POST['rateMonth'])){ 
                        $error .= '<p>- Proszę podać prawidłową wartość stawki miesięcznej.</p>';
                        $errorArray['rateMonth'] = true;   
                    }
                }

                # dodatkowe informacje o wynagrodzeniu
                if(!empty($_POST['additional_salary_info']) && count($_POST['additional_salary_info'])>300) {
                    $error .= '<p>- Pole z dodatkowymi informacjami o wynagrodzeniu może zawierać tylko 300 znaków.</p>';
                    $errorArray['additional_salary_info'] = true;                
                }

                # wymagania
                if(!empty($_POST['need']) && count($_POST['need'])>300) {
                    $error .= '<p>- Pole z wymaganiami może zawierać tylko 300 znaków.</p>';
                    $errorArray['need'] = true;                
                }

                # obowiązki
                if(empty($_POST['duty'])) {
                    $error .= '<p>- Proszę wpisać obowiązki.</p>';
                    $errorArray['duty'] = true;
                } elseif(count($_POST['duty'])>300) {
                    $error .= '<p>- Pole z obowiązkami może zawierać tylko 300 znaków.</p>';
                    $errorArray['duty'] = true;                
                }

                # oferujemy
                if(!empty($_POST['offer']) && count($_POST['offer'])>300) {
                    $error .= '<p>- Pole z ofertą może zawierać tylko 300 znaków.</p>';
                    $errorArray['offer'] = true;                
                }
                
                # województwa
                if(empty($_POST['province'])) {
                    $error .= '<p>- Proszę wybrać województwo.</p>';
                    $errorArray['province'] = true;
                }

                # miasto
                if(empty($_POST['city'])) {
                    $error .= '<p>- Proszę wpisać miasto.</p>';
                    $errorArray['city'] = true;
                }
                
                # street 
                if(empty($_POST['street'])) {
                    $error .= '<p>- Proszę wpisać ulicę oraz numer lokalu.</p>';
                    $errorArray['street'] = true;
                }
                
                # osoba
                if(!$this->user) {
                    if(empty($_POST['contact_email'])) {
                        $error .= '<p>- Proszę wpisać adres email.</p>';
                        $errorArray['contact_email'] = true;
                    } elseif(!Form::validEmail($_POST['contact_email'])) {
                        $error .= '<p>- Proszę wpisać prawidłowy adres email.</p>';
                        $errorArray['contact_email'] = true;  
                    }
                    if(empty($_POST['contact_name'])) {
                        $error .= '<p>- Proszę wpisać nazwę pracodawcy.</p>';
                        $errorArray['contact_name'] = true;
                    }
                }

                
                if(!$error) {
                    foreach($_POST as $key=>$val) {
                        $_SESSION['basket'][$key] = $val;
                    }
                    $_SESSION['basket']['step'] = 2;
                    header("Location:".HOST."daj-ogloszenie");
                } else {
                    $this->template->assign('errorArray', $errorArray);
                    $this->template->assign('error', '<p><strong>W formularzu wystąpiły błedy:</strong></p>'.$error);
                }
            }
            
            $this->template->display('dajOgloszenie/step1.tpl');
            
        }        
    }    
    
    public function sukcesAction($queryName = false) {
        if(!isset($queryName) || !is_numeric($queryName)) {
            header("Location:".HOST."moje-konto/dodaj-ogloszenie");
        }
        
        $this->template->assign('job_id', $queryName);
        $this->template->display('dajOgloszenie/sukces.tpl');  
    }
}