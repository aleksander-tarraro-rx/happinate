<?php
namespace src\informacje;

use src\BaseController;

class informacjeController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        header("Location:".HOST);
    }    

    public function oNasAction($queryName = false) {
        $this->template->display('informacje/oNas.tpl');
    }

    public function wartosciAction($queryName = false) {
        $this->template->display('informacje/wartosci.tpl');
    }

    public function dlaMediowAction($queryName = false) {
        $this->template->display('informacje/dlaMediow.tpl');
    }

    public function faqAction($queryName = false) {
        $this->template->display('informacje/faq.tpl');
    }

    public function mapaKategoriiAction($queryName = false) {
        $this->template->display('informacje/mapaKategorii.tpl');
    }

    public function mapaMiejscowosciAction($queryName = false) {
        $this->template->display('informacje/mapaMiejscowosci.tpl');
    }

    public function politykaPrywatnosciAction($queryName = false) {
        $this->template->display('informacje/politykaPrywatnosci.tpl');
    }

    public function regulaminAction($queryName = false) {
        $this->template->display('informacje/regulamin.tpl');
    }

    public function warunkiPlatnosciAction($queryName = false) {
        $this->template->display('informacje/warunkiPlatnosci.tpl');
    }

    public function daneRejestroweAction($queryName = false) {
        $this->template->display('informacje/daneRejestrowe.tpl');
    }

    public function dotpayAction($queryName = false) {
        $this->template->display('informacje/dotpay.tpl');
    }

    public function konkursAction($queryName = false) {
        $this->template->display('informacje/konkurs.tpl');
    }
    
}