<?php

namespace src;

use src\mojeKonto\model\mojeKontoModel;

class BaseController
{
    public function preExecute()
    {
        global $_database;
        global $_error;
        global $_template;
        global $_user;
        if (
            is_object($_user)
            && method_exists($_user, 'getUserType')
            && 1 === intval($_user->getUserType())
        ) {

            require_once HAPPINATE_ROOT_DIR . '/src/mojeKonto/model/model.php';
            $model = new mojeKontoModel($_database, $_error);

            $_template->assign('calendarEvents', $model->getUnreadCalendarEvents());
        }
    }
}