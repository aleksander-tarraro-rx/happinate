<?php

namespace src\activate\model;

/*
 * MODEL 
 * 
 * activate
 */

class activateModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
    
    /*
     * activate($email)
     *
     * activate user with email adress 
     * 
     * @return boolean
     */
    public function activate($email) {
        $sql = "UPDATE ".DB_PREF."user SET user_active = 1 WHERE user_email = AES_ENCRYPT(?, '".SALT."')";
        $pre = $this->database->prepare($sql);

        $pre->execute(array($email));
        
        if($pre->rowCount()!=0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }           
    }
}