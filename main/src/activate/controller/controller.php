<?php

namespace src\activate;

use app\helpers\FlashMsg;
use src\BaseController;

/*
 * CONTROLLER 
 * 
 * activate
 * 
 * !IMPORTANT
 * 
 * This is only for internal use. Random users shouldn't get this URL.
 * It's need to be removed, improved or redesinged.
 * 
 */

class activateController extends BaseController {
    
    private $template;
    private $model;

    public function __construct($template, $model) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* 
     * INDEX ACTION
     * 
     * /activate
     * 
     * DOES NOTHING
     * 
     * redirect to home page
     */
    public function indexAction($queryName = false) {
        header("Location: " . HOST);    
    }
    
    /* 
     * USER ACTION
     * 
     * /activate/user/[$email]
     * 
     * $queryName must be valid email adress
     */
    public function userAction($queryName = false) {
        if($queryName) {
            if($this->model->activate($queryName)) {
                FlashMsg::add('success', 'Udało się aktywować konto '.$queryName.'.');
                header("Location: " . HOST); 
            } else {
                FlashMsg::add('error', 'Nie udało się aktywować konto '.$queryName.'.');
                header("Location: " . HOST);                 
            }
        } else {
            header("Location: " . HOST);    
        }
    }
}