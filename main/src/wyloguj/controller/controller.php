<?php
namespace src\wyloguj;

use app\helpers\FlashMsg;

require __DIR__ . '/../../../app/libs/facebook/facebook.php';

class wylogujController {
    
    private $template;
    private $model;
    private $user;

    public function __construct($template, $model, $user) {
        $this->template = $template;
        $this->model = $model;
        $this->user = $user;
        if(!$this->user) {
            FlashMsg::add('warning', 'Musisz się najpierw zalogować aby mieć dostęp do tej strony.');
            header("Location:".HOST."zaloguj-sie");
        }
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        $facebook = new \Facebook(array(
          'appId'  => FB_ID,
          'secret' => FB_SECRET_KEY
        ));
        $facebook->destroySession();
        $_SESSION['userToken'] = NULL;
        $_SESSION['userTokenFB'] = NULL;
        $_SESSION['basket'] = NULL;
        $_SESSION['resume'] = NULL;
        $_SESSION['popup'] = NULL;
        FlashMsg::add('success', 'Udało Ci się poprawnie wylogować!');
        header("Location: " . HOST);
    }
}