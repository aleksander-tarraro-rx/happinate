<?php

namespace src\przypomnijHaslo\model;

use app\helpers\Email;

class przypomnijHasloModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }

    public function remindPassword($email) {
        $startDate = time();
        $tommorow = date('Y-m-d H:i:s', strtotime('+1 day', $startDate));
        $reminderToken =  sha1($email.$tommorow.SALT);
        
        $sql = "UPDATE ".DB_PREF."user SET user_password_reminder_token = ?, user_password_reminder_exp_date = ? WHERE user_email = AES_ENCRYPT(?, '".SALT."');";
        $pre = $this->database->prepare($sql);
        if($pre->execute(array($reminderToken, $tommorow, $email))) {

            $title = 'happinate.com - nowe hasło ';
            $body = '<p>Aby zmienić hasło kliknij w <a href="http://'.$_SERVER['SERVER_NAME'].HOST.'przypomnij-haslo/zmiana-hasla/'.$email.','.$reminderToken.'" target="_blank">link</a></p>';
            $body .= '<p>Pozdrawiamy, happinate.com - <br/>praca od zaraz!</p>';
            
            $_emailClass = $GLOBALS['_emailClass'];
            $_emailClass->addToList($email, $title, $body);
            
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
        
    public function checkEmailNotExist($email) {
        $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."');";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($email));
        if($pre->rowCount()!=0) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
           
    public function checkReminder($string) {
        $array = explode(',', $string);
        $sql = "SELECT user_password_reminder_exp_date FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."') AND user_password_reminder_token = ? AND user_password_reminder_exp_date>NOW();";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($array[0], $array[1]));
        if($pre->rowCount()==1) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
             
    public function changePassword($email, $password) {
        $sql = "UPDATE ".DB_PREF."user SET user_password = AES_ENCRYPT(md5(?), '".SALT."'), user_password_reminder_token = NULL, user_password_reminder_exp_date = NULL WHERE user_email = AES_ENCRYPT(?, '".SALT."');";
        $pre = $this->database->prepare($sql);
        if($pre->execute(array($password, $email))) {
            return true;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }
    }
    
}