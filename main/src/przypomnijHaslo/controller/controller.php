<?php

namespace src\przypomnijHaslo;

use app\helpers\Form;
use app\helpers\FlashMsg;
use src\BaseController;

class przypomnijHasloController extends BaseController {
    
    private $template;
    private $model;
    private $user;

    public function __construct($template, $model, $user) {
        $this->template = $template;
        $this->model = $model;
        $this->user = $user;
        if($this->user) {
            FlashMsg::add('warning', 'Aby przypomnieć sobie hasło najpierw wyloguj się z konta.');
            header("Location:".HOST);
        }
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        if(isset($_POST['reminder-submit'])) {
            $error = '';
            // Validation
            if(empty($_POST['reminder-email'])) {
                $error .= '<p>Proszę wpisać adres email.</p>';
            } elseif(!Form::validEmail($_POST['reminder-email'])) {
                $error .= '<p>Proszę podać prawidłowy adres email.</p>';
            } elseif(!$this->model->checkEmailNotExist($_POST['reminder-email'])) {
                $error .= '<p>Podany adres email nie znajduje się jeszcze w bazie danych.</p>';
            }
            if(!$error) {
                if($this->model->remindPassword($_POST['reminder-email'])) {
                    FlashMsg::add('success', 'Na podany adres email została wysłana wiadomość z dalszymi instrukcjami aby zmienić hasło.');
                    header("Location:".HOST);                   
                } else {
                    FlashMsg::add('warning', 'Nie udało się wysłać linka z przypominaniem hasła. Proszę spróbować ponownie później.');
                    header("Location:".HOST);                    
                }
            } else {
                $this->template->assign('error', $error);
            } 
        }
        $this->template->display('przypomnijHaslo/index.tpl');
    }    
    
    /* Index action */
    public function zmianaHaslaAction($queryName = false) {
        if($queryName) {
            if($this->model->checkReminder($queryName)) {
                
                $array = explode(',', $queryName);
                $this->template->assign('user_email', $array[0]);
                if(isset($_POST['reminder-submit'])) { 
                    $error = '';
                    // Validation
                    if(empty($_POST['reminder-password'])) {
                        $error .= '<p>Proszę wpisać hasło.</p>';
                    } elseif(strlen($_POST['reminder-password'])<6) {
                        $error .= '<p>Hasło musi posiadać więcej niż 6 znaków.</p>';
                    }
                    if(!$error) {
                        if($this->model->changePassword($array[0], $_POST['reminder-password'])) {
                            FlashMsg::add('success', 'Udało się poprawnie zmienić hasło. Możesz się teraz zalogować na nowe hasło.');
                            header("Location: " . HOST . "zaloguj-sie");
                        } else {
                            FlashMsg::add('success', 'Nie udało się zmienić hasło. Proszę spróbować ponownie.');
                            header("Location: " . HOST . "przypomnij-haslo/zmiana-hasla/".$array[0]."+".$array[1]);                        
                        }
                    } else {
                        FlashMsg::add('error', $error);
                        header("Location: " . HOST . "przypomnij-haslo/zmiana-hasla/".$array[0]."+".$array[1]);
                    }
                }
            } else {
                FlashMsg::add('warning', 'Dane do zmiany hasła są niepoprawne lub link aktywacyjny wygasł. Proszę spróbować ponownie.');
                header("Location:".HOST);                     
            }
            $this->template->display('przypomnijHaslo/zmianaHasla.tpl');
        } else {
            header("Location:".HOST);            
        }
    }
}