<?php


namespace src;


class ErrorController extends BaseController
{
    private $template;

    private $user;

    public function __construct($template, $user)
    {
        $this->template = $template;
        $this->user = $user;
    }

    public function show404()
    {
        $this->template->display('error404.tpl');
    }
}