<?php

namespace src\listaPracodawcow\model;

class listaPracodawcowModel {
    
    private $database;
    private $error;

    public function __construct($database, $error) {
        $this->database = $database;
        $this->error = $error;
    }
                       
    public function getEmplyoeers() {
        $sql = "SELECT user_id, user_profile_name, user_logo FROM ".DB_PREF."user WHERE user_type = 2 AND user_profile_name IS NOT null AND user_active = 1 ORDER BY LOWER(user_profile_name) ASC";
        
        $pre = $this->database->prepare($sql);
        $pre->execute();
        
        if($pre->rowCount()>0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2], __FILE__, ''.__LINE__.'');
            return false;
        }  
    }
    
}