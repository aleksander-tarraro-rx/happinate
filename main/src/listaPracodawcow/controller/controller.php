<?php

namespace src\listaPracodawcow;

use src\BaseController;

class listaPracodawcowController extends BaseController {
    
    private $template;
    private $model;


    public function __construct($template, $model, $user) {
        $this->template = $template;
        $this->model = $model;
    }
    
    /* Index action */
    public function indexAction($queryName = false) {
        $list = $this->model->getEmplyoeers();

        $this->template->assign('list',$list);
        $this->template->display('listaPracownikow/index.tpl');
    }    
    
}