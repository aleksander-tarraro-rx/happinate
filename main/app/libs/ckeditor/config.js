/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {

	config.toolbarGroups = [
		{ name: 'document',     groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'forms' },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'tools' },
		{ name: 'others' }
	];

	// The default plugins included in the basic setup define some buttons that
	// we don't want too have in a basic editor. We remove them here.
	config.removeButtons = 'Cut,Copy,Paste,Undo,Redo,Anchor,Underline,Strike,Subscript,Superscript';

	// Let's have it basic on dialogs as well.
	config.removeDialogTabs = 'link:advanced';
        
        // Resize
        config.resize_enabled = true;
        config.resize_maxHeight = 600;
        config.resize_minHeight = 100;
        
        config.height = 100;
        
        config.wordcount = {

            // Whether or not you want to show the Word Count
            showWordCount: true,

            // Whether or not you want to show the Char Count
            showCharCount: true,

             // Whether or not to include Html chars in the Char Count
             countHTML: false,

            // Option to limit the characters in the Editor
            charLimit: 'unlimited',

            // Option to limit the words in the Editor
            wordLimit: 'unlimited'
            
        };
};
