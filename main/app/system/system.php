<?php 
    /* Guardian Angel */
        if(!defined('GUARDIAN_ANGEL') || !GUARDIAN_ANGEL) { exit; }

    /* DEBUG niszczenie sesji */
        if(DEBUG && isset($_GET['clearSession'])) {
            session_destroy();
            header("Location: " . HOST);
        }
        
    /* Czyszczenie FlashMsg */
        if(isset($_SESSION['flashMsg']) && !empty($_SESSION['flashMsg'])) {
            $_template->assign('flashMsg', $_SESSION['flashMsg']);
            \app\helpers\FlashMsg::Clear();
        }
    
    /* Sprawdzenie użytkownika */
        if((isset($_SESSION['userToken']) && !empty($_SESSION['userToken'])) || (isset($_SESSION['userTokenFB']) && !empty($_SESSION['userTokenFB']))) {
            require_once __DIR__ . '/../src/user.class.php';
            $_user = new app\src\User($_database);
            $_user->checkUser();
        } else {
            $_user = false;
        }
        
        $_template->assign('_user', $_user);

    /* GET */
        $error404 = false;
        $integerAction = false;
        if(isset($_GET['url']) && !empty($_GET['url'])) {
            // Default
            $queryName = '';
            $actionName = 'indexAction';
            // Url array
            $urlArray = explode('/', $_GET['url']);
            $controllerTmp = explode('-', $urlArray[0]);
            $controllerString = '';
            
            $i = 0;
            foreach($controllerTmp as $ctl) {
                if($i>0) {
                    $controllerString .= ucfirst($ctl);
                } else {
                    $controllerString .= strtolower($ctl);
                }
                $i++;
            }

            $controllerName = $controllerString.'Controller';
            $modelName = $controllerString.'Model';
            
            // Action
            if(isset($urlArray[1]) && !empty($urlArray[1])) {
                if(is_numeric($urlArray[1])) {
                    $actionName = 'integerAction';
                    $integerAction = true;
                } else {
                    $actionTmp = explode('-', $urlArray[1]);
                    $actionString = '';
                    $i = 0;
                    foreach($actionTmp as $act) {
                        if($i>0) {
                            $actionString .= ucfirst($act);
                        } else {
                            $actionString .= strtolower($act);
                        }
                        $i++;
                    }
                    $actionName = $actionString.'Action';
                    if($actionName=='indexAction') {
                        header("Location: " . HOST . $urlArray[0]);
                    }                   
                }
            }

            // Query
            if(isset($urlArray[2]) && !empty($urlArray[2])) $queryName = $urlArray[2];
            
            // Model
            $modelSrc = __DIR__ . '/../../src/'.$controllerString.'/model/model.php';
            if(file_exists($modelSrc)) {
                require_once $modelSrc;
                $modelNamespace = 'src\\'.$controllerString.'\\model\\'.$modelName;
                $model = new $modelNamespace($_database, $_error);  
            } else {
                $model = NULL;
                $modelName = '';
                $modelNamespace = '';
            }
           
            // Controller
            $controllerSrc = __DIR__ . '/../../src/'.$controllerString.'/controller/controller.php';
            if(file_exists($controllerSrc)) {
                require_once $controllerSrc;
                $controllerNamespace = 'src\\'.$controllerString.'\\'.$controllerName;
                $controller = new $controllerNamespace($_template, $model, $_user); 
                if(!method_exists($controller, $actionName)) {
                    $error404 = true;
                    $actionName = 'no action found';
                }
            } else {
                $controllerName = 'no controller found';
                $actionName = 'no action found';
                $controllerNamespace = '';
                $error404 = true;
            }
        } else {
            // Model
            $modelName = strtolower(INDEX).'Model';
            require_once __DIR__ . '/../../src/'.strtolower(INDEX).'/model/model.php';
            $modelNamespace = 'src\\'.strtolower(INDEX).'\\model\\'.$modelName;
            
            $model = new $modelNamespace($_database, $_error);      
            
            // Controller
            $controllerName = strtolower(INDEX).'Controller';
            require_once __DIR__ . '/../../src/'.strtolower(INDEX).'/controller/controller.php';
            
            $controllerNamespace = 'src\\'.strtolower(INDEX).'\\'.$controllerName;
            
            $controller = new $controllerNamespace($_template, $model, $_user); 
            
            $actionName = 'indexAction';
            $queryName = '';
        }

    /* Smarty Controller && Action */
        $_template->assign('controllerName', $controllerName);
        $_template->assign('actionName', $actionName);
        
        if(DEBUG) {
            $_template->assign('controllerNamespace', $controllerNamespace);
            $_template->assign('queryName', $queryName);

            $_template->assign('modelNamespace', $modelNamespace);
            $_template->assign('modelName', $modelName);
            
            $endTime = microtime(true);
            $totalTime = round(($endTime - $startTime) / 60, 4);
            $_template->assign('totalTime', $totalTime);
            
            if(isset($_FILES)) {
                $_template->assign('_files', $_FILES);
            }
        }
 
    /* Controller Action */    
        if(!$error404) {
            if (method_exists($controller, 'preExecute')) {
                $controller->preExecute();
            }

            if($integerAction) {
                $_template->assign('integerAction', $urlArray[1]);
                $controller->$actionName($urlArray[1], $queryName);
            } else {
                $controller->$actionName($queryName);
            }
            
        } else {
            require_once HAPPINATE_ROOT_DIR . '/src/ErrorController.php';
            $controller = new \src\ErrorController($_template, $_user);
            if (method_exists($controller, 'preExecute')) {
                $controller->preExecute();
            }
            $controller->show404();
        } 
        
        