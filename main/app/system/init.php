<?php 
    /* Guardian Angel */
        if(!defined('GUARDIAN_ANGEL') || !GUARDIAN_ANGEL) { exit; }	
        
    /* Liczenie czasu do debugera */
        if(DEBUG) $startTime = microtime(true);
             		
    /* Sprawdzenie środowiska i właczenie logowania błędów */
        error_reporting(E_ALL);
        DEBUG ? ini_set('display_errors','ON') : ini_set('display_errors','OFF');
        LOG ? ini_set('log_errors', 'ON') : ini_set('log_errors', 'OFF');
        
    /* Smarty */
        require_once __DIR__ . '/../libs/smarty/Smarty.class.php';
        
        $_template = new Smarty();
        
        $_template->setTemplateDir(__DIR__ . '/../../view/templates/');
        $_template->setCompileDir(__DIR__ . '/../tmp/template_c');
        $_template->setCacheDir(__DIR__ . '/../tmp/template_cache');
        
    /* Email */
        require __DIR__ . '/../libs/phpmailer/class.phpmailer.php';
        global $_email;
        $_email = new PHPMailer(true);
        
    /* Logi */
        require_once __DIR__ . '/../src/log.class.php';
        $_logs = new Log();

    /* Obsługa błędów */
        require_once __DIR__ . '/../src/error.class.php';
        $_error = new Error($_logs, $_template);
        
        set_error_handler(array($_error, 'handler'));
    
    /* Baza danych */
        require_once __DIR__ . '/../src/database.class.php';
        $database = new Database($_error);
        $_database = $database->connect();
        
    /* Sesja */
        if($_database) {
            require_once __DIR__ . '/../src/session.class.php';
            $_session = new Session($_database, SESSION_LIFE);
            session_set_save_handler(array($_session, 'open'),array($_session, 'close'),array($_session, 'read'),array($_session, 'write'),array($_session, 'destroy'),array($_session, 'gc'));
        }
        
        ob_start();
        session_start();
   
    /* Helpery */
        require_once __DIR__ . '/../helpers/form.php';
        require_once __DIR__ . '/../helpers/flashMsg.php';
        require_once __DIR__ . '/../helpers/email.php';
        require_once HAPPINATE_ROOT_DIR . '/src/BaseController.php';
        
        $_emailClass = new \app\helpers\Email($_database);
        global $_emailClass;