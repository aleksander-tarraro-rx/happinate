<?php

/*
 * Klasa Database 
 */

class Database {
    
    private $error;
    
    /*
     * Konstruktor 
     */
    public function __construct($error) {
        $this->error = $error;
    }
    
    /* 
     * Tworzy obiekt PDO i konfiguruje podstawowe ustawienia 
     * 
     * @return object
     */
    public function connect() {
        try {
            $_database = new PDO('mysql:dbname='.DB_NAME.';host='.DB_HOST.';port='.DB_PORT.';', DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
            $_database->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $_database->setAttribute(PDO::ATTR_ORACLE_NULLS , PDO::NULL_EMPTY_STRING);
            return $_database;
        } catch(PDOException $exception) {
            $this->error->sqlError($exception->getMessage());
            return false;
        }
    }

    
}