<?php

/* 
 * Session 
 */

class Session {

    private $database;
    private $maxlifetime;
    
    /* 
     * Konstruktor 
     */
    public function __construct($database, $maxlifetime = 3600) {
        $this->database = $database;
        $this->maxlifetime = $maxlifetime;
    }

    /* 
     * Zamykanie sesji 
     * 
     * @return null
     */
    public function close() {
        $this->gc($this->maxlifetime);
    }
    
    /* 
     * Usuwanie sesji 
     * 
     * @return null
     */
    public function destroy($sid) {
        $pre = $this->database->prepare("DELETE FROM ".DB_PREF."session WHERE session_id = ?");
        $pre->execute(array($sid));
    }

    /* 
     * Globalcountdown sesji 
     * 
     * @return null
     */
    public function gc($maxlife) {
        $pre = $this->database->prepare("DELETE FROM ".DB_PREF."session WHERE datetime < ?");
        $pre->execute(array(date("Y-m-d H:i:s", time() - $maxlife)));
    }

    /* 
     * Otworzenie sesji 
     * 
     * @return boolean
     */
    public function open() {
        return true;
    }
    
    /* 
     * Odczyt sesji 
     * 
     * @return string / null
     */
    public function read($sid) {
        
        $pre = $this->database->prepare("SELECT value FROM ".DB_PREF."session WHERE session_id = ?");
        $pre->execute(array($sid));
        if($r = $pre->fetch()) {
            $val = $r['value'];
            return $val;
        } else {
            $pre = $this->database->prepare("INSERT INTO ".DB_PREF."session (`session_id` ,`datetime`) VALUES (?, ?)");
            $pre->execute(array($sid, date("Y-m-d H:i:s")));
        }
    }
    
    /* 
     * Zapis sesji 
     * 
     * @return null
     */
    public function write($sid, $values) {
        $pre = $this->database->prepare("SELECT value FROM ".DB_PREF."session WHERE session_id = ?");
        $pre->execute(array($sid));
        $r = $pre->fetch(PDO::FETCH_ASSOC);
        if(count($r)!=1) {
              $session_val = $r['value'];       
              if($session_val != $values) {
                  $pre = $this->database->prepare("UPDATE ".DB_PREF."session SET value = ?, datetime = ? WHERE session_id = ?");
                  $pre->execute(array($values,date("Y-m-d H:i:s"),$sid));
              }
        } else {
              $pre = $this->database->prepare("UPDATE ".DB_PREF."session SET value = ?, datetime = ? WHERE session_id = ?");
              $pre->execute(array($values,date("Y-m-d H:i:s"),$sid));         
        }
    }
}