<?php

namespace app\src;

use app\helpers\FlashMsg;

/* 
 * User 
 */

class User {

    private $database;
    
    public $id = false;
    
    /* 
     * Konstruktor 
     */
    public function __construct($database) {
        $this->database = $database;
        
        $this->id = $this->getUserId();
    }

    /* 
     * Sprawdza poprawność Tokenu
     * 
     * @return boolean
     */
    public function checkUser() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? AND user_ip = AES_ENCRYPT(?, '".SALT."') LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken'], $_SERVER['REMOTE_ADDR']));            
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? AND user_ip = AES_ENCRYPT(?, '".SALT."') LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB'], $_SERVER['REMOTE_ADDR']));
        } else {
            return false;
        }

        if($res->rowCount()<=0) {
            $_SESSION['userToken'] = NULL;
            $_SESSION['userTokenFB'] = NULL;
            FlashMsg::add('warning', 'Zostałeś przymusowo wylogowany ponieważ ktoś inny zalogował się na Twoje konto. Zmiań hasło dla pewności!');
            header("Location:".HOST);
        } else {
            return true;
        }
    }
    
    /* 
     * Pobiera adres email użytkownika
     * 
     * @return string
     */    
    public function getUserEmail() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT AES_DECRYPT(user_email, '".SALT."') as user_email FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));          
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT AES_DECRYPT(user_email, '".SALT."') as user_email FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));        
        } else {
            return false;
        }

        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_email'];
        }
    }    
    
    /* 
     * Pobiera typ użytkownika
     * 
     * @return integer
     */     
    public function getUserType() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT user_type FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));        
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT user_type FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));    
        } else {
            return false;
        }

        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_type'];
        }
    }  
    
    /* 
     * Pobiera logo użytkownika
     * 
     * @return string
     */         
    public function getUserLogo() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT user_logo FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));        
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT user_logo FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));    
        }

        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_logo'];
        }
    }  
    
    /* 
     * Pobiera miasto użytkownika
     * 
     * @return string
     */     
    public function getUserCity() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT user_city FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));        
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT user_city FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));    
        }

        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_city'];
        }
    }  
    
    /* 
     * Pobiera kod pocztowy użytkownika
     * 
     * @return string
     */       
    public function getUserZipcode() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT AES_DECRYPT(user_adress_zip_code, '".SALT."') as user_adress_zip_code FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));        
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT AES_DECRYPT(user_adress_zip_code, '".SALT."') as user_adress_zip_code FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));    
        }

        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_adress_zip_code'];
        }
    }  
     
    /* 
     * Pobiera telefon użytkownika jako int
     * 
     * @return string
     */     
    public function getUserPhone() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT AES_DECRYPT(user_phone, '".SALT."') as user_phone FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));        
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT AES_DECRYPT(user_phone, '".SALT."') as user_phone FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));    
        }

        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_phone'];
        }
    }  
    
    /* 
     * Pobiera typ użytkownika jako string
     * 
     * @return string
     */  
    public function getUserTypeName() {
        $sql = "SELECT user_type_name FROM ".DB_PREF."user WHERE type_type_id = ? LIMIT 1";
        $res = $this->database->prepare($sql);
        $res->execute(array($this->getUserType()));
        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_type_name'];
        }
    }  
    
    /* 
     * Pobiera nazwę użytkownika
     * 
     * @return string
     */         
    public function getUserName() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT AES_DECRYPT(user_name, '".SALT."') as user_name FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));        
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT AES_DECRYPT(user_name, '".SALT."') as user_name FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));    
        }
        
        $r = $res->fetch();

        if($r['user_name']) {
            return $r['user_name'];
        }
    }  
    
    /* 
    * Pobiera nazwisko użytkownika
    * 
    * @return string
    */        
    public function getUserSurname() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT AES_DECRYPT(user_surname, '".SALT."') as user_surname FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));        
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT AES_DECRYPT(user_surname, '".SALT."') as user_surname FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));    
        }
        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_surname'];
        }
    }  
    
    /* 
     * Pobiera nazwę profilu użytkownika
     * 
     * @return string
     */        
    public function getUserProfilename() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT user_profile_name FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));        
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT user_profile_name FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));    
        }
        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_profile_name'];
        }
    }  
 
    /* 
     * Pobiera miasto użytkownika
     * 
     * @return string
     */        
    public function getUserAdressCity() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT user_adress_city FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));        
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT user_adress_city FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));    
        }
        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_adress_city'];
        }
    }  
 
    /* 
     * Pobiera adres użytkownika
     * 
     * @return string
     */        
    public function getUserAdressStreet() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT AES_DECRYPT(user_adress_street,'".SALT."') as user_adress_street FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));        
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT AES_DECRYPT(user_adress_street,'".SALT."') as user_adress_street FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));    
        }
        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_adress_street'];
        }
    }  
    
    /* 
     * Pobiera stronę www użytkownika
     * 
     * @return string
     */        
    public function getUserWWW() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT user_www FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));        
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT user_www WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));    
        }
        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_www'];
        }
    }  
        
    /* 
     * Pobiera fanpage użytkownika
     * 
     * @return string
     */        
    public function getUserFanpage() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT user_fanpage FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));        
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT user_fanpage FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));    
        }
        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_fanpage'];
        }
    }  
    
    /* 
     * Pobiera Facebook id użytkownika
     * 
     * @return integer
     */        
    public function getUserFBId() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT AES_DECRYPT(user_fb_id, '".SALT."') as user_fb_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));          
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT AES_DECRYPT(getUserFBId, '".SALT."') as getUserFBId FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));        
        }

        if($res->rowCount()>0) {
            $r = $res->fetch();
            return $r['user_fb_id'];
        }
    }
    
    /* 
     * Sprawdz czy użytkownik posiada tagi
     * 
     * @return boolean
     */        
    public function checkIfUserHasTags() {
        if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
            $sql = "SELECT count(t.id) as count FROM ".DB_PREF."user_tag as t LEFT JOIN ".DB_PREF."user as u ON u.user_id = t.tag_user_id WHERE sha1(concat(u.user_id,u.user_email,u.user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userToken']));          
        } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) {
            $sql = "SELECT count(t.id) as count FROM ".DB_PREF."user_tag as t LEFT JOIN ".DB_PREF."user as u ON u.user_id = t.tag_user_id WHERE sha1(concat(u.user_id,u.user_fb_id,u.user_salt)) = ? LIMIT 1";
            $res = $this->database->prepare($sql);
            $res->execute(array($_SESSION['userTokenFB']));        
        }
            
        $r = $res->fetch();

        if($r['count']>1) {
            return true;
        } else {
            return false;
        }
    }
    
    /* 
     * Pobiera wszystkie tagi
     * 
     * @return array
     */  
    public function getTags() {
        $sql = "SELECT * FROM ".DB_PREF."tag WHERE tag_active = 1";
        
        $pre = $this->database->prepare($sql);
        $pre->execute();
        if($pre->rowCount()!=0) {
            return $pre->fetchAll();
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2]);
            return 0;
        }         
    }    
    
    /* 
     * Pobiera listę znajomych użytkownika
     * 
     * @return array
     */ 
    public function getFriendsList() {
        $sql = "SELECT u.user_id, AES_DECRYPT(u.user_email, '".SALT."') as user_email, AES_DECRYPT(u.user_name, '".SALT."') as user_name, AES_DECRYPT(u.user_surname, '".SALT."') as user_surname FROM ".DB_PREF."user_friend as f LEFT JOIN ".DB_PREF."user as u ON f.friend_id = u.user_id WHERE f.user_id = ? AND f.friend_id IS NOT NULL";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        $res = $pre->fetchAll();
        
        if($res){
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2]);
            return false;
        } 
    }
    
    /* 
     * Pobiera id użytkownika
     * 
     * @return integer
     */ 
    public function getUserId($email = false) {
        if($email) {
            $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_email = AES_ENCRYPT(?, '".SALT."')";
            $pre = $this->database->prepare($sql);
            $pre->execute(array($email));            
                
            if($pre->rowCount()!=0) {
                $id = $pre->fetch();
                return $id['user_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2]);
                return false;
            }              
        } else {
            if(isset($_SESSION['userToken']) && $_SESSION['userToken']) {
                $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_email,user_salt)) = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($_SESSION['userToken']));
            } elseif(isset($_SESSION['userTokenFB']) && $_SESSION['userTokenFB']) { 
                $sql = "SELECT user_id FROM ".DB_PREF."user WHERE sha1(concat(user_id,user_fb_id,user_salt)) = ?";
                $pre = $this->database->prepare($sql);
                $pre->execute(array($_SESSION['userTokenFB']));            
            } else {
                return false;
            }
            if($pre->rowCount()!=0) {
                $id = $pre->fetch();
                return $id['user_id'];
            } else {
                $pdoError = $pre->errorInfo();
                if($pdoError[1]!=0) $this->error->sqlError($pdoError[2]);
                return false;
            }      
        }
    }
    
    /* 
     * Pobiera listę CV użytkownika
     * 
     * @return array
     */ 
    public function getCVList() {
        $sql = "SELECT * FROM ".DB_PREF."user_cv WHERE cv_user_id = ?";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        $res = $pre->fetchAll();
        
        if($res){
            return $res;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2]);
            return false;
        } 
    }
    
  /* 
     * Sprawdz czy użytkownik posiada tagi
     * 
     * @return boolean
     */        
    public function employeerNotCompleted() {
        $sql = "SELECT user_id FROM ".DB_PREF."user WHERE user_id = ? "
                . "AND user_profile_name IS NOT NULL "
                . "AND user_logo IS NOT NULL "
                . "AND user_adress_city IS NOT NULL "
                . "AND user_adress_zip_code IS NOT NULL "
                . "AND user_adress_street IS NOT NULL "
                . "AND user_www IS NOT NULL "
                . "AND user_fanpage IS NOT NULL "
                . "AND user_info IS NOT NULL "
                . "AND user_profile_img IS NOT NULL "
                . "AND user_profile_img_info IS NOT NULL "
                . "AND user_phone IS NOT NULL";
        $pre = $this->database->prepare($sql);
        $pre->execute(array($this->getUserId()));
        
        if($pre->rowCount()>0){
            return false;
        } else {
            $pdoError = $pre->errorInfo();
            if($pdoError[1]!=0) $this->error->sqlError($pdoError[2]);
            return true;
        } 
    }
    
    
}