<?php

namespace app\helpers;

/*
 * app\helpers\Form
 */

class Form {
    
    /*
     * Sprawdzenie poprawności adresu email
     * 
     * @return boolean
     */
    public static function validEmail($email) {
        $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
        return (bool) preg_match($regex, $email);
    }   
          
    /*
     * Sprawdzenie poprawności linki Youtube
     * 
     * @return boolean
     */
    public static function validYoutubeUrl($url) {
        $regex = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i'; 
        return (bool) preg_match($regex, $url);
    }   
        
    /*
     * Sprawdzenie poprawności stringa bez znaków specjalnych
     * 
     * @return boolean
     */
    public static function validNoSpecialChars($string) {
        $regex = '/^[a-zA-Z0-9]$/'; 
        return (bool) !preg_match($regex, $string);
    }   
    
    /*
     * Sprawdzenie czy wartość to boolean
     * 
     * @return boolean
     */
    public static function validBoolean($boolean) {
        return (bool) is_bool($boolean);
    }    
    
    /*
     * Sprawdzenie czy wartość to integer
     * 
     * @return boolean
     */
    public static function validInteger($int) {
        return (bool) is_numeric($int);
    }    
    
    /*
     * Sprawdzenie poprawności url
     * 
     * @return boolean
     */
    public static function validUrl($url) {
        if(preg_match("/\b(?:(?:https?):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $url)) {
            return true;
        } else {
            return false;
        }
    }
        
    /*
     * Sprawdzenie poprawności polskich kodów pocztowych
     * 
     * @return boolean
     */
    public static function validPolishZipCode($code) {
        $regex = '/^([0-9]{2})-([0-9]{3})$/'; 
        return (bool) preg_match($regex, $code);
    }
            
    /*
     * Sprawdzenie poprawności polskiego numeru NIP
     * 
     * @return boolean
     */
    public static function validPolishNip($nip) {
	$str = preg_replace("/[^0-9]+/","",$nip);
	if (strlen($str) != 10)	{
		return false;
	}
 
	$arrSteps = array(6, 5, 7, 2, 3, 4, 5, 6, 7);
	$intSum=0;
	for ($i = 0; $i < 9; $i++) {
		$intSum += $arrSteps[$i] * $nip[$i];
	}
	$int = $intSum % 11;
 
	$intControlNr=($int == 10)?0:$int;
	if ($intControlNr == $nip[9]) {
		return true;
	}
	return false;
    }
                
    /*
     * Sprawdzenie poprawności polskiego numeru Pesel
     * 
     * @return boolean
     */
    public static function validPolishPesel($pesel) {
	if (!preg_match('/^[0-9]{11}$/',$pesel)) {
		return false;
	}
 
	$arrSteps = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3);
	$intSum = 0;
	for ($i = 0; $i < 10; $i++) {
		$intSum += $arrSteps[$i] * $pesel[$i];
	}
	$int = 10 - $intSum % 10;
	$intControlNr = ($int == 10)?0:$int;
	if ($intControlNr == $pesel[10]) {
		return true;
	}
	return false;
    }
                
    /*
     * Sprawdzenie poprawności polskiego numeru Regon
     * 
     * @return boolean
     */
    public static function validPolishRegon($regon) {

	if (strlen($regon) != 9) {
		return false;
	}
 
	$arrSteps = array(8, 9, 2, 3, 4, 5, 6, 7);
	$intSum=0;
	for ($i = 0; $i < 8; $i++)
	{
		$intSum += $arrSteps[$i] * $regon[$i];
	}
	$int = $intSum % 11;
	$intControlNr=($int == 10)?0:$int;
	if ($intControlNr == $regon[8]) {
		return true;
	}
	return false;
    }
    
    /*
     * Czyszczenie stringa z tagów html
     * 
     * @param string $string 
     * @return string
     */
    public static function sanitizeString($string) { 
        return htmlspecialchars($string, ENT_NOQUOTES);
    }     
    
    /*
     * Czyszczenie tablicy z tagów html
     * 
     * @param array $array 
     * @return array
     */
    public static function sanitizeArray($array) { 
        $postSanitized = array();
        foreach($array as $p => $k) {
            if(!is_array($k)) {
                $postSanitized[$p] = htmlspecialchars($k, ENT_NOQUOTES);
            } else {
                $postSanitized[$p] = $k;
            }
            
        }
        
        return $postSanitized;
    }    
    
    /*
     * Generowanie losowego hasła
     * 
     * @param string $lenght (default = 10)
     * @return boolean
     */    
    public static function generatePassword($lenght = 10) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $lenght; $i++) {
           $index = rand(0, $count - 1);
           $result .= mb_substr($chars, $index, 1);
        }

        return $result;       
    }

    /**
     * Sprawdzenie poprawności daty 
     * 
     * @param string $date (yyyy-mm-dd)
     * @return boolean
     */
    public static function validDate($date) {
        if(empty($date)) {
            return false;
        }
        
        $_temp = explode("-", $date);

        if(count($_temp)<>3) {
            return false;
        }

        foreach($_temp as $k => $v) {
            $_temp[$k] = (int)ltrim($v, '0');
        }

        if(!checkdate((int)$_temp[1], (int)$_temp[2],(int)$_temp[0])) {
            return false;
        }

        return true;
    }
}