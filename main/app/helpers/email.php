<?php

namespace app\helpers;

/*
 * app\helpers\Email
 */

class Email {
    
    private $database;
    
    /* 
     * Konstruktor 
     */
    public function __construct($database) {
        $this->database = $database;
    }

    /*
     * Wysyłka emaila inicjowana z CRONA
     * 
     * @param $adresses (string), $title (string), $body (string), $layout (string), $attachment (file), $fromEmail (string), $fromName (string)
     * @return boolean
     */
    static public function sendTo($adresses, $title, $body, $layout = 'default.html', $attachment = false, $fromEmail = 'happinate@happinate.com', $fromName = 'happinate - praca od zaraz') {
        $email = $GLOBALS['_email'];

        $email->IsSMTP();
        
        try {
            $email->Host         = EMAIL_HOST;
            $email->CharSet      = 'utf-8';
            $email->SMTPAuth     = true;
            $email->SMTPSecure   = EMAIL_SSL;
            $email->Username     = EMAIL_USER;
            $email->Password     = EMAIL_PASS;
            $email->Port         = EMAIL_PORT;
            #$email->SMTPDebug = 1;

            echo $adresses;
            $email->SetFrom($fromEmail, $fromName);
            //$email->AddAddress($adresses, $adresses);
            $email->AddAddress('noreply@happinate.com');
            $email->AddBCC($adresses);
            $email->Subject = $title;

            if($attachment) {
                $email->AddAttachment(__DIR__ . '/../../upload/tmp/'.$attachment);
                $body .= "<p>W załączniku znajduje się CV osoby składającej aplikację.</p>";
            }

            $html = str_replace('{body}', $body, file_get_contents(__DIR__.'/../../view/email/'.$layout));
            $html = str_replace('{title}', $title, $html);
            $email->MsgHTML($html);

            $email->AltBody = $body;
            $email->Send();
        } catch (phpmailerException $e) {
            echo $e->errorMessage();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }    
    /*
     * Wysyłka emaila inicjowana z CRONA
     * 
     * @param $adresses (string), $title (string), $body (string), $layout (string), $attachment (file), $fromEmail (string), $fromName (string)
     * @return boolean
     */
    public function addToList($adresses, $title, $body, $layout = 'default.html', $attachment = false, $fromEmail = 'happinate@happinate.com', $fromName = 'happinate - praca od zaraz') {
        $sql = "INSERT INTO ".DB_PREF."email (title, email, body, template, fromName, fromEmail, date) VALUES (?, ?, ?, ?, ?, ?, NOW())";
        $pre = $this->database->prepare($sql);
        
        if($pre->execute(array($title, $adresses, $body, $layout, $fromName, $fromEmail))) {
            return true;
        } else {
            return false;
        }
 
    }
}