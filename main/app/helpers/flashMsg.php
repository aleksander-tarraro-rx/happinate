<?php

namespace app\helpers;

/*
 * app\helpers\FlashMsg
 */

class FlashMsg {

    /*
     * Dodawanie nowego typu FlashMsg do sesji
     */
    public static function add($type, $content) {
        $_SESSION['flashMsg'][$type] = $content;
    }
    
     /*
     * Odczyt FlashMsg z sesji
     * 
     * @return string
     */
    public static function get($type) {
        return $_SESSION['flashMsg'][$type];
    }
    
     /*
     * Usuwanie FlashMsg z sesji
     */
    public static function clear() {
        unset($_SESSION['flashMsg']);
    }
   
}