<?php
/* Check protection */
if(!defined('GUARDIAN_ANGEL') || !GUARDIAN_ANGEL) { exit; }

/* Basic configuration */
define('HOST', '/');
define('DEBUG', false);
define('LOG', true);
define('INDEX', 'home');
define('REVISION', 79);

/* Database configuration */
define('DB_HOST', '127.0.0.1');
define('DB_PORT', '');
define('DB_USER', 'root');
define('DB_PASS', 'password');
define('DB_NAME', 'happinate');
define('DB_PREF', '');

/* Facebook configuration */
define('FB_ID', '111111111111111');
define('FB_SECRET_KEY', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

/* Email configuration */
define('EMAIL_HOST', 'poczta.webio.pl');
define('EMAIL_PORT', 465);
define('EMAIL_USER', 'happinate@happinate.com');
define('EMAIL_PASS', 'aaaaaaaaaaa');
define('EMAIL_SSL', 'ssl');

/* Security salt */
define('SALT', 'aaaaaaaaaaaaaaaaaaaaaaaa');

/* Session max life */
define('SESSION_LIFE', 3600);

/* DotPay */
define('DOTPAY_SMS_ID', 69028);
define('DOTPAY_SMS_CODE', 'TC.3D');
define('DOTPAY_SMS_TYPE', 'C1');
define('DOTPAY_SMS_DEL', 1);

/* Api Key */
define('API_KEY', 'aaaaaaaaaaaaaaaaaaaaaaaaa');

/* Google API Key */
define('GOOGLE_API_KEY', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

/* Modules */
define('MOD_CALENDAR', false);
