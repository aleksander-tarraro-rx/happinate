<?php 

    /* Guardian Angel to prevent external */
        define('GUARDIAN_ANGEL', true);

    /* Constant for root dir for easing class-loading difficulties */
        define('HAPPINATE_ROOT_DIR', __DIR__);

    /* Include configuration file */
        include_once __DIR__ . '/app/config/config.php';

    /* Include init file */	
        include_once __DIR__ . '/app/system/init.php';

    /* Include system file */
        include_once __DIR__ . '/app/system/system.php';
    
    /* Ob_flush */   
        ob_flush();
